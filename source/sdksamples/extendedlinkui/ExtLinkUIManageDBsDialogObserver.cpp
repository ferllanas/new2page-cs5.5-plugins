//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIManageDBsDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IBooleanControlData.h"
#include "IDialogController.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ITextControlData.h"

// General includes:
#include "CDialogObserver.h"

// Project includes:
#include "ExtLinkUIID.h"
#include "SDKFileHelper.h"


/** Implements IObserver based on the partial implementation CDialogObserver.

	
	@ingroup extendedlinkui
*/
class ExtLinkUIManageDBsDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		ExtLinkUIManageDBsDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~ExtLinkUIManageDBsDialogObserver() {}

		/**
			Called by the application to allow the observer to attach to the subjects
			to be observed, in this case the dialog's info button widget. If you want
			to observe other widgets on the dialog you could add them here.
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange,
			ISubject* theSubject,
			const PMIID& protocol,
			void* changedBy
		);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(ExtLinkUIManageDBsDialogObserver, kExtLinkUIManageDBsDialogObserverImpl)

/* AutoAttach
*/
void ExtLinkUIManageDBsDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	ASSERT(panelControlData);
	if(!panelControlData) {
		return;
	}

	// Attach to other widgets you want to handle dynamically here.
	AttachToWidget(kExtLinkUIFindFolderButtonWidgetID, IBooleanControlData::kDefaultIID, panelControlData);
	AttachToWidget(kExtLinkUISharedFolderTextWidgetID, ITextControlData::kDefaultIID, panelControlData);
}

/* AutoDetach
*/
void ExtLinkUIManageDBsDialogObserver::AutoDetach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
	ASSERT(panelControlData);
	if(!panelControlData) {
		return;
	}

	// Detach from other widgets you want to handle dynamically here.
	DetachFromWidget(kExtLinkUIFindFolderButtonWidgetID, IBooleanControlData::kDefaultIID, panelControlData);
	DetachFromWidget(kExtLinkUISharedFolderTextWidgetID, ITextControlData::kDefaultIID, panelControlData);
}



/* Update
*/
void ExtLinkUIManageDBsDialogObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID& protocol, void* changedBy)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);
	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		ASSERT(controlView);
		if(!controlView) {
			break;
		}
		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		
		
		// Check for Cancel button.
		if (theSelectedWidget == kCancelButton_WidgetID && theChange == kTrueStateMessage) {
			break;
		}
		// Check for OK button.
		if (theSelectedWidget == kOKButtonWidgetID && theChange == kTrueStateMessage) {
			break;
		}
		
		InterfacePtr<IDialogController> dlgController(this, UseDefaultIID());
		if (dlgController == nil) {
			ASSERT_FAIL("IDialogController is invalid");
			break;
		}

		if (theSelectedWidget == kExtLinkUIFindFolderButtonWidgetID && theChange == kTrueStateMessage)
		{
			SDKFolderChooser folderChooser; 
			folderChooser.ShowDialog(); 
			if (folderChooser.IsChosen()) { 
				SDKFileHelper helper(folderChooser.GetIDFile());
				PMString currentText = helper.GetPath();
				currentText.SetTranslatable(kFalse);
				dlgController->SetTextControlData(kExtLinkUISharedFolderTextWidgetID, currentText);
			}
		}
		
	} while (kFalse);
}



//  Code generated by DollyXs code generator
