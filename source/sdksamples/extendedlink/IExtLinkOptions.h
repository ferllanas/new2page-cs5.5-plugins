//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/IExtLinkOptions.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IExtLinkOptions_H_DEFINED__
#define __IExtLinkOptions_H_DEFINED__
#include "ExtLinkID.h"

/**
	Option data for IExtLinkOptions
*/
class ExtLinkOptionsData
{
public:
	typedef object_type data_type;
	
	ExtLinkOptionsData()
	{
		fConnectionName = PMString(); 
		fImageFolderPath = PMString(); 
		fDataSourceName = PMString(); 
		fDBTableName = PMString(); 
	}

	/** Copy-constructor 
	 * 	@param rhs IN Another ExtLinkOptionsData class.
	*/
	ExtLinkOptionsData(const ExtLinkOptionsData& rhs)
	{
		fConnectionName = rhs.fConnectionName; 
		fImageFolderPath = rhs.fImageFolderPath; 
		fDataSourceName = rhs.fDataSourceName; 
		fDBTableName = rhs.fDBTableName; 
	}
	
	virtual ~ExtLinkOptionsData() {}

	/** Operator assignment 
	 * 	@param rhs IN Right-hand side argument to the assignment operator.
	 * 	@return Reference to this object.
	 */
	ExtLinkOptionsData& operator=(const ExtLinkOptionsData& rhs)
	{
		this->fConnectionName = rhs.fConnectionName;
		this->fImageFolderPath = rhs.fImageFolderPath;
		this->fDataSourceName = rhs.fDataSourceName;
		this->fDBTableName = rhs.fDBTableName;
		return *this;
	}
	
	/** Operator equal*/
	bool16   operator==(const ExtLinkOptionsData& inData) const
	{
		return	(fConnectionName == inData.fConnectionName && 
				 fImageFolderPath == inData.fImageFolderPath &&
				 fDataSourceName == inData.fDataSourceName &&
				 fDBTableName == inData.fDBTableName);
	}
	
	/** 
		Set an ODBC connection's name, this is currently not used
	*/
	virtual void SetConnectionName(const PMString& name) {if (fConnectionName != name) fConnectionName = name;}

	/** 
		Get an ODBC connection's name, this is currently not used
	*/
	virtual const PMString& GetConnectionName() {return fConnectionName;}

	/** 
		Set the image folder path.  For image assets, our database only stores the filename of the image, then we come to this folder path to look for the image
	*/
	virtual void SetImageFolderPath(const PMString& imageFolderPath) {if (fImageFolderPath != imageFolderPath) fImageFolderPath = imageFolderPath;}

	/** 
		Get the image folder path.  For image assets, our database only stores the filename of the image, then we come to this folder path to look for the image
	*/
	virtual const PMString& GetImageFolderPath() {return fImageFolderPath;}

	/** 
		Set the ODBC data source name.  This is the DSN you set up in your ODBC utility outside InDesign.
	*/
	virtual void SetDataSourceName(const PMString& dsn) {if (fDataSourceName != dsn) fDataSourceName = dsn;}

	/** 
		Get the ODBC data source name.  This is the DSN you set up in your ODBC utility outside InDesign.
	*/
	virtual const PMString& GetDataSourceName() {return fDataSourceName;}

	/**
		Set up database table which contains the data we can handle, this is currently not used
	*/
	virtual void SetDBTableName(const PMString& tableName) {if (fDBTableName != tableName) fDBTableName = tableName;}
	/**
		Get up database table which contains the data we can handle, this is currently not used
	*/
	virtual const PMString& GetDBTableName() {return fDBTableName;}
private:
	PMString fConnectionName;
	PMString fImageFolderPath;
	PMString fDataSourceName;
	PMString fDBTableName;
};

/** persistent interface added in to the session workspace, holding options for this plug-in.

	@ingroup extendedlink
*/

class IExtLinkOptions : public IPMUnknown
{
public:

	enum { kDefaultIID = IID_IEXTLINKOPTIONS };

	/** Append an option to the persistent list. If the index you specify is outwith the length, then it'll get appended to the list
		@param newOption [IN] new ExtLinkOptionsData to add to existing list of options
		@param indexWhere [IN] where you want it in the list of options, zero based
	*/
	virtual void AddOption(const ExtLinkOptionsData& newOption, const int32 indexWhere)=0; 

	/**	Return option at given index in list
		@param index [IN] zero-based index
		@return ExtLinkOptionsData giving option at specified position
	 */
	virtual ExtLinkOptionsData GetNthOption(const int32 index) =0; 

	/** Retrieves current list of options as clone
		@param outList OUT contains copy of list of options
	*/
	virtual void GetOptionListCopy(K2Vector<ExtLinkOptionsData>& outList)=0;
	
	/**	Return the flag whether refresh resource status automatically
	 */
	virtual bool16 IsAutoRefreshStatus() const = 0;
	
	/**	Set flag whether refresh resource status automatically
		@param val [IN] true auto refresh, false otherwise
	 */
	virtual void AutoRefreshStatus(bool16 val) = 0;
};

#endif // __IExtLinkOptions_H_DEFINED__


