//========================================================================================
//  
//  $File: //depot/indesign_4.0/highprofile/source/sdksamples/printmemorystream/N2PSQLXferBytes.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: jbond $
//  
//  $DateTime: 2005/07/08 10:44:00 $
//  
//  $Revision: #2 $
//  
//  $Change: 350974 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"			    

// Interface includes:
// none. 

// Project includes:
#include "N2PSQLXferBytes.h"
#include "N2PsqlID.h" // for kN2PsqlPluginName

/* Constructor
*/
N2PSQLXferBytes::N2PSQLXferBytes(void) 
: 	fMyBuffer(nil),
	fCurrentMaxBuf(ePrtMemInitialMaxBuf),
	fAbsolutePositionInBuffer(0),
	fCountBytesStored(0),
	fStreamState(kStreamStateGood)
{
	fMyBuffer = new uint8 [fCurrentMaxBuf];
	ASSERT(fMyBuffer);
	memset(fMyBuffer, 0,fCurrentMaxBuf);
	TRACEFLOW(kN2PsqlPluginName, "N2PSQLXferBytes constructor\n");

}

/* Destructor
*/
N2PSQLXferBytes::~N2PSQLXferBytes(void)
{
	TRACEFLOW(kN2PsqlPluginName, "N2PSQLXferBytes destructor\n");
	this->dumpState();
	if (fMyBuffer)
	{
		delete [] fMyBuffer;
		fMyBuffer = nil;
	}
}

/* resizeBuffer
*/
void N2PSQLXferBytes::resizeBuffer(uint32 newSize)
{
	do
	{
		bool16 preconditions = (newSize > ePrtMemInitialMaxBuf);
		if (preconditions == kFalse)
		{
			ASSERT_FAIL("preconditions are not met");
			break;
		}

		// create a new temporary buffer that is bigger. 
		// (Don't just resize by a little amount...)
		uint32 newTargetBufferSize = ePrtMemInitialMaxBuf + newSize;
		uint8* tempBuffer = new uint8 [newTargetBufferSize];
		if (tempBuffer == nil)
		{
			ASSERT_FAIL("new operator failed to allocate for tempBuffer");
			break;
		}
		// initialize the temp buffer data to 0
		memset(tempBuffer, 0, newSize);
		ASSERT(tempBuffer);
		// copy the data from existing buffer to temp buffer 
		memcpy(tempBuffer, this->fMyBuffer, this->fCurrentMaxBuf);

		// delete the buffer (member variable)
		delete [] fMyBuffer;
		// reassign member pointers and size
		this->fMyBuffer = tempBuffer;
		this->fCurrentMaxBuf = newTargetBufferSize;

		// Postconditions: fMyBuffer is not nil
		ASSERT(tempBuffer);

		// It is able to store newTargetBufferSize bytes
	} while (kFalse);
}


/* Read
*/
uint32 N2PSQLXferBytes::Read(void* buffer, uint32 pnum)
{
	// TODO - test 
	// ipaterso: I've not tested this at all, as I've only use a Write stream
	// with this code
	uint32 numToTransfer = 0;
	do
	{
		if (buffer == nil)
		{
			ASSERT_FAIL("buffer is nil!");
			break;
		}
		
		uint32 numLeftInBuffer = this->fCountBytesStored - this->fAbsolutePositionInBuffer;
	
		if (pnum > numLeftInBuffer)
		{
			numToTransfer = numLeftInBuffer;
			fStreamState = kStreamStateEOF;
		}
		else
		{
			numToTransfer = pnum;
		}
		// Something like:
		// memcpy (buffer, <somewhere in buffer>, numToTransfer);
		bool16 canRead = (fAbsolutePositionInBuffer + numToTransfer) < fCurrentMaxBuf; 
		if (canRead)
		{
			memcpy(buffer, fMyBuffer + fAbsolutePositionInBuffer, numToTransfer);
			this->fAbsolutePositionInBuffer += numToTransfer;	
		}
		else
		{
			ASSERT_FAIL("canRead is false, our buffer isn't big enough");
		}

	} while (false);
	return numToTransfer;

}


/* Write
*/
uint32 N2PSQLXferBytes::Write(void* buffer, uint32 pnum)
{

	uint32 numToTransfer = 0;
	do
	{
		if (buffer == nil)
		{
			ASSERT_FAIL("buffer is nil!");
			break;
		}

		numToTransfer = pnum;
		// We want to add this data at fAbsolutePositionInBuffer...
		uint32 endOfWriteBytes = (this->fAbsolutePositionInBuffer + numToTransfer);
		bool16 noOverflow = endOfWriteBytes < fCurrentMaxBuf; 
		if (noOverflow == kFalse)
		{
			this->resizeBuffer(endOfWriteBytes + 1); 
		}
		noOverflow = endOfWriteBytes < fCurrentMaxBuf; 
		ASSERT(noOverflow);

		if (numToTransfer > 0 && noOverflow)
		{
			memcpy(this->fMyBuffer + this->fAbsolutePositionInBuffer, buffer, numToTransfer);
			this->fAbsolutePositionInBuffer += numToTransfer;
			if (fAbsolutePositionInBuffer > this->fCountBytesStored)
			{
				this->fCountBytesStored = this->fAbsolutePositionInBuffer;
			}
		}
	} while (false);
	return numToTransfer;
}

/* Seek
*/      
uint32 N2PSQLXferBytes::Seek(int32 numberOfBytes, SeekFromWhere fromHere)
{
	if (fStreamState == kStreamStateEOF)
	{
		fStreamState = kStreamStateGood;
	}
	switch (fromHere)
	{
	case kSeekFromStart:
		this->fAbsolutePositionInBuffer = numberOfBytes;
		break;
	case kSeekFromCurrent:
		this->fAbsolutePositionInBuffer = this->fAbsolutePositionInBuffer + numberOfBytes;
		break;
	case kSeekFromEnd:
		this->fAbsolutePositionInBuffer = this->fCountBytesStored + numberOfBytes;
		break;
	}
	return fAbsolutePositionInBuffer;
}

/* Flush
*/
void N2PSQLXferBytes::Flush(void)
{
	// Do nothing. 
	// I guess we'd only care about this in file-based implementation,
	// at which point we'd flush to disk.
}

/* GetStreamState
*/  
StreamState N2PSQLXferBytes::GetStreamState(void)
{
	return this->fStreamState;
}

/* SetEndOfStream
*/  
void N2PSQLXferBytes::SetEndOfStream(void)
{
	this->fCountBytesStored = this->fAbsolutePositionInBuffer;
}

/* getBufferPtr
*/
const uint8* N2PSQLXferBytes::getBufferPtr(void) const
{
	return this->fMyBuffer;
}

/* dumpState
*/
void N2PSQLXferBytes::dumpState(void) const
{
	TRACEFLOW(kN2PsqlPluginName, "N2PSQLXferBytes::dumpState()\n");
	TRACEFLOW(kN2PsqlPluginName, "-- Abs. position in buffer = 0x%x\n", this->fAbsolutePositionInBuffer);
	TRACEFLOW(kN2PsqlPluginName, "-- Bytes stored = 0x%x\n", this->fCountBytesStored);
	TRACEFLOW(kN2PsqlPluginName, "-- Current maxbuf = 0x%x\n\n", this->fCurrentMaxBuf);
	PMString data;
	const int32 nbytes = 2048;
	data.SetXString(reinterpret_cast<const char*>(this->getBufferPtr()), nbytes);
	TRACEFLOW(kN2PsqlPluginName, "First %d bytes: \n%s\n\n\n", nbytes, data.GrabCString());
}

// End, N2PSQLXferBytes.cpp.

