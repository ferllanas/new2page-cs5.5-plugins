/*
//	File:	CheckInOutSuite.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "IRangeData.h"
#include "IApplication.h"
#include "ITextModel.h"
#include "ITextModelCmds.h"
#include "ICommand.h"
#include "IComposeScanner.h"
#include "IDocument.h"
#include "IDataBase.h"
#include "IFrameList.h"
#include "ILayoutUIUtils.h"
#include "IStyleNameTable.h"
#include "IWindow.h"
#include "IWindowUtils.h"
#include "IWorkspace.h"
#include "IHierarchy.h"
#include "ITextFrameColumn.h"
#include "IGeometry.h"
#include "IPageItemTypeUtils.h"
#include "ILayoutControlData.h"
#include "ILayerList.h"
#include "ILayoutCmdData.h"
#include "IBoolData.h"
#include "ISpreadList.h"
#include "ISpread.h"
#include "IGraphicFrameData.h"
#include "ILockPosition.h"
#include "IIntData.h"
#include "IItemLockData.h"
#include "IFrameEdgePrefsCmdData.h"
#include "IInCopyDocUtils.h"
#include "IInCopyStoryList.h"
#include "ITextParcelList.h"
#include "ITextParcelListComposer.h"
#include "IParcelList.h"
#include "IPageItemUtils.h"
#include "INewPageItemCmdData.h"
#include "SplineID.h"		// kSplineItemBoss
#include "TransformUtils.h"		
#include "ITOPSplineData.h"	
#include "PageItemScrapID.h"		

//Para aplicar color al frame
#include "IColorData.h"		
#include "ISwatchList.h"	
#include "ISwatchUtils.h"	
#include "IPersistUIDData.h"		

#include "IDialogMgr.h"
#include "IDialog.h"
#include "IDialogController.h"
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "ErrorUtils.h"
//#include "FrameUtils.h"
#include "CAlert.h"
#include "SDKLayoutHelper.h"

#include "IDocumentLayer.h"

#include "ITextModel.h"
#include "ITextFrameColumn.h"
#include "IParcelList.h"
#include "IParcel.h"
#include "IFrameList.h"
#include "IFrameListComposer.h"
#include "ITextParcelList.h"
#include "ITextParcelListComposer.h"
#include "IWaxStrand.h"
#include "IWaxIterator.h"
#include "IWaxLine.h"
#include "IHierarchy.h"
#include "ITextTarget.h"
#include "IConcreteSelection.h"
#include "ILayoutTarget.h"
#include "IGraphicFrameData.h"
#include "ITextColumnSizer.h"
#include "IGalleyInfoUtils.h"
#include "ITextColumnSizer.h"
#include "ICreateMCFrameData.h"
#include "IMetaDataAccess.h"
#include "ICommandMgr.h"
#include "IDocumentCommands.h"
#include "StandOffID.h"
#include "IUIDData.h"
#include "IRenderingObject.h"

#include "IPMUnknownData.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "ISpread.h"
#include "IStandOffData.h"
#include "IPageItemTypeUtils.h"

#include "IGeometry.h"
#include "IGraphicAttributeUtils.h"
#include "IGraphicAttrIndeterminateData.h"
#include "IGraphicAttrRealNumber.h"//atributos de rectangulo
#include "IGraphicFrameData.h"
//#include "IGraphicMetaDataUtils.h"
#include "IGraphicMetaDataObject.h"
#include "IGraphicStateRenderObjects.h"
#include "IGraphicStateUtils.h"
#include "IGraphicStyleDescriptor.h"////////para cambiar el tamaÒo del marco del rectangulo////////////
#include "IGeometry.h"

#include "N2PFrOvtID.h"
#include "IN2PCTUtilities.h"

#ifdef MACINTOSH
	#include"../../../source/Interlasa/N2PSQL/UpdateStorysAndDBUtilis.h"
#endif

#ifdef WINDOWS
	#include"..\N2PSQL\UpdateStorysAndDBUtilis.h"
#endif
/*#include "N2PSQLUtilities.h"


#include "..\..\..\source\Interlasa\N2PCheckInOut\ICheckInOutSuite.h"

#include "..\..\..\source\Interlasa\N2Padornmentissue\IN2PAdIssueUtils.h"
#include "..\..\..\source\Interlasa\N2Padornmentissue\N2PAdIssueID.h"

#include "..\..\..\source\Interlasa\N2PLogInOut\IN2PSQLUtils.h"
#include "..\..\..\source\Interlasa\N2PLogInOut\N2PRegisterUsers.h"
#include "..\..\..\source\Interlasa\N2PLogInOut\IRegisterUsersUtils.h"

/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/


class N2PCTUtilities : public CPMUnknown<IN2PCTUtilities>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	N2PCTUtilities (IPMUnknown *boss);

	virtual bool16 MuestraNotasConFrameOverset(StructIdFramesElementosXIdNota* VectorNota,const int32 TotNotas);
	
	virtual bool16 EsStoryOverset(InterfacePtr<ITextModel> textModel);
	
	virtual  bool16 OcultaNotasConFrameOverset();

	virtual int LengthXMP(PMString PathStruct);
	
	virtual bool16 ObtenerTextFrameOrStOnXMP(StructOfTextFrameConOverset *ListTextFrOrSt,int32 CantidadDeTextFrameOverset);
	
	virtual bool16 CanFlowTextOversetOnNewFrame( UID UIDTextModelofElementoNota, UIDRef& FrameWhitOversetText );
 
 	virtual bool16 DeleteXMP(PMString Path);
 	
 	virtual bool16 GuardarTextFrameOrStOnXMP(StructOfTextFrameConOverset *ListTextFrOrSt,int32 CantidadDeTextFrameOverset);
 	
 	virtual bool16 GetTamanoInterlineado(InterfacePtr<IFrameList> frameList,PMReal& LeadingOfLasParcel);
 	
 	
 	virtual ErrorCode EstimateTextDepth(const InterfacePtr<ITextParcelList>& textParcelList, 
 										ParcelKey fromParcelKey, 
 										int32 numberOfParcels, 
 										PMReal& estimatedDepth, 
 										int32& NumeroDeLineaAntesDelOverset,
 										int32& UltimoCaracterMostrado
 										);

	virtual ErrorCode EstimateStoryDepth(const InterfacePtr<ITextModel>& textModel, PMReal& estimatedDepth, int32& NumeroDeLineaAntesDelOverset);

	virtual bool16 GETStoryParams_OfUIDTextModel( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumLinesFaltantes,
 												int32& NumLinesRestantes,
 												int32& NumCarFaltantes,
 												int32& NumCarRestantes,
 												int32& NumCarOfTextModel);
 												
 	virtual ErrorCode CountWordsAndLines(UID UIDTextModelofElementoNota, const InterfacePtr<ITextParcelList>& textParcelList, ParcelKey fromParcelKey, 
											int32 numberOfParcels, PMReal& estimatedDepth,
											int32& NumeroDeLineaAntesDelOverset,int32& UltimoCaracterMostrado);

	private:
	
		IParcel* QueryParcelContaining(ITextModel* textModel, const TextIndex at);
		
		//ITextFrame* QueryTextFrameContaining(ITextModel* textModel, const TextIndex at);
		
		
		UIDRef CreateAndThreadTextFrame(const UIDRef& fromGraphicFrameUIDRef);
		
		bool16 CanThreadTextFrames(IDataBase* database, const UID& fromGraphicFrameUID, const UID& toGraphicFrameUID);
		
		
		ErrorCode ThreadTextFrames(IDataBase* database, const UID& fromGraphicFrameUID, const UID& toGraphicFrameUID);
		
		UID GetTextContentUID(const UIDRef& graphicFrameUIDRef);
		
		UIDRef GetGraphicFrameRef(const InterfacePtr<ITextFrameColumn>& textFrame, const bool16 isTOPFrameAllowed);
		
		
		 PMRect Get_Coordinates_Reales(const UIDRef& pageItemRef);
		
		
		
		 
		 
		 void SaveXMP(PMString PathStruct, PMString ads, int pos);
		 
		 PMString GetXMP(PMString PathStruct,int ads);
		 
		 
		 
		 bool16 SetIgnoredTextWrap(const UIDRef& FrameUIDRef);
		 
		 UID BuscarOcrearColor(const PMString& swatchName, 
								  const PMReal rCyan, 
								  const PMReal rMagenta, 
								  const PMReal rAmarillo,
								  const PMReal rNegro);
		void crearColores();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PCTUtilities, kN2PCTUtilitiesImpl)


/* HelloWorld Constructor
*/
N2PCTUtilities::N2PCTUtilities(IPMUnknown* boss) 
: CPMUnknown<IN2PCTUtilities>(boss)
{
}


 bool16 N2PCTUtilities::MuestraNotasConFrameOverset(StructIdFramesElementosXIdNota* VectorNota,const int32 contNotasInVector)
 {
 
 	do
 	{
 		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
		
		StructOfTextFrameConOverset *ListTextFrOrSt= new StructOfTextFrameConOverset[ contNotasInVector * 4 ]; 
 		int32 CantidadDeTextFrameOverset=0;
 	
 		if(contNotasInVector>0)
 		{
 			//db->BeginTransaction();
	 		InterfacePtr<ICommandMgr> cmdManager(document,IID_ICOMMANDMGR);
 			bool16 olUndo = cmdManager->SetUndoSupport(kFalse);
 		
 			for(int32 numNota=0 ; numNota < contNotasInVector ; numNota++)
			{		
				UID UIDTextModelofElementoNota;
				UIDRef FrameWhitOversetText;
			
				UIDTextModelofElementoNota = VectorNota[numNota].UIDFrTituloNote;
				if(this->CanFlowTextOversetOnNewFrame( UIDTextModelofElementoNota, FrameWhitOversetText ))
				{
					ListTextFrOrSt[ CantidadDeTextFrameOverset ].UIDTextModel = UIDTextModelofElementoNota.Get();
					ListTextFrOrSt[ CantidadDeTextFrameOverset ].UIDTextFrameOrSt = (FrameWhitOversetText.GetUID()).Get();
					CantidadDeTextFrameOverset++;
				}
			
				UIDTextModelofElementoNota = VectorNota[numNota].UIDFrContentNote;
				if(this->CanFlowTextOversetOnNewFrame( UIDTextModelofElementoNota, FrameWhitOversetText ))
				{
					ListTextFrOrSt[ CantidadDeTextFrameOverset ].UIDTextModel = UIDTextModelofElementoNota.Get();
					ListTextFrOrSt[ CantidadDeTextFrameOverset ].UIDTextFrameOrSt = (FrameWhitOversetText.GetUID()).Get();
					CantidadDeTextFrameOverset++;
				}
				
				UIDTextModelofElementoNota = VectorNota[numNota].UIDFrBalazoNote;
				if(this->CanFlowTextOversetOnNewFrame( UIDTextModelofElementoNota, FrameWhitOversetText ))
				{
					ListTextFrOrSt[ CantidadDeTextFrameOverset ].UIDTextModel = UIDTextModelofElementoNota.Get();
					ListTextFrOrSt[ CantidadDeTextFrameOverset ].UIDTextFrameOrSt = (FrameWhitOversetText.GetUID()).Get();
					CantidadDeTextFrameOverset++;
				}
			
			
				UIDTextModelofElementoNota = VectorNota[numNota].UIDFrPieFotoNote;
				if(this->CanFlowTextOversetOnNewFrame( UIDTextModelofElementoNota, FrameWhitOversetText ))
				{
					ListTextFrOrSt[ CantidadDeTextFrameOverset ].UIDTextModel = UIDTextModelofElementoNota.Get();
					ListTextFrOrSt[ CantidadDeTextFrameOverset ].UIDTextFrameOrSt = (FrameWhitOversetText.GetUID()).Get();
					CantidadDeTextFrameOverset++;
				}
			}
			cmdManager->SetUndoSupport(kTrue);
			this->GuardarTextFrameOrStOnXMP(ListTextFrOrSt,CantidadDeTextFrameOverset);
		
			//db->EndTransaction();
	 	}	
	 	
	 	//db->SetModified(kFalse);
	 	
	 	
	 	
 	}while(false);

 	return kTrue;
 }
 
 
  bool16 N2PCTUtilities::OcultaNotasConFrameOverset()
 {
 
 	do
	{	
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
			
			
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
				break;
		}
		
		
		UIDList frameList(db);
		
 		int32 NumTxtFmOverset=this->LengthXMP("N2PTxtFmeOversetArray");
		if(NumTxtFmOverset<=0)
			break;
		
		StructOfTextFrameConOverset *ListTextFrOrSt = new StructOfTextFrameConOverset[NumTxtFmOverset]; 
 		
 		this->ObtenerTextFrameOrStOnXMP(ListTextFrOrSt,NumTxtFmOverset);
 		
 		for(int32 numReg=0;numReg<NumTxtFmOverset;numReg++)
 		{
			CAlert::InformationAlert("Entro al for");
 			//Obtiene el UID del Frame que se encuentra en el XMP
 			UID UIDFrameOverset = ListTextFrOrSt[ numReg ].UIDTextFrameOrSt;
 			
 			
			
			//Convierte a UIDRef
			UIDRef UIDRefOfElementoNota(db,UIDFrameOverset);
			
			//Verifica que es valido
			UID fromMultiColumnItemUID = this->GetTextContentUID(UIDRefOfElementoNota);
			if (fromMultiColumnItemUID == kInvalidUID) 
			{
				continue;
			}
			
			//Adiciona a la lista  de frames a borrar
			frameList.Append(UIDFrameOverset);
			
 		}
 		
 		////Borra lista de Frames con overset
 		
 		
 		
 		//db->BeginTransaction();
 		InterfacePtr<ICommandMgr> cmdManager(document,IID_ICOMMANDMGR);
 		bool16 olUndo = cmdManager->SetUndoSupport(kFalse);
 		
 		//
 			InterfacePtr<ICommand> deleteCmd(CmdUtils::CreateCommand(kDeleteCmdBoss));
 			ASSERT(deleteCmd);
 			if(!deleteCmd)
 			{
 				break;
 			}
 			deleteCmd->SetItemList(frameList);
 			deleteCmd->SetName("Loco");
 			deleteCmd->SetUndoability(ICommand::kRegularUndo);
 			ErrorCode status = CmdUtils::ProcessCommand(deleteCmd);
 			ASSERT_MSG(status==kSuccess,"kDeleteCmdBoss failed");
 		//
 		cmdManager->SetUndoSupport(kTrue);
 		
		this->DeleteXMP("N2PTxtFmeOversetArray");
 		
 	}while(false);
 	
 	return kTrue;
 }
 
 
 
 
 bool16 N2PCTUtilities::ObtenerTextFrameOrStOnXMP(StructOfTextFrameConOverset *ListTextFrOrSt,int32 CantidadDeTextFrameOverset)
 {
 	for(int32 numNota=0 ; numNota < CantidadDeTextFrameOverset ; numNota++)
	{		
			PMString CadenaRegistro="";
			
			CadenaRegistro = this->GetXMP("N2PTxtFmeOversetArray",numNota+1);
			
			
			PMString elemento=CadenaRegistro.Substring(0,CadenaRegistro.IndexOfString(","))->GrabCString();
			
			//
			
			ListTextFrOrSt[ numNota ].UIDTextModel = elemento.GetAsNumber();
			
			CadenaRegistro.Remove(0,CadenaRegistro.IndexOfString(",")+1);
			
			//
			
			ListTextFrOrSt[ numNota ].UIDTextFrameOrSt=CadenaRegistro.GetAsNumber();
			
			
	}	
	return(kTrue);
 }

 
 bool16 N2PCTUtilities::GuardarTextFrameOrStOnXMP(StructOfTextFrameConOverset *ListTextFrOrSt,int32 CantidadDeTextFrameOverset)
 {
 	bool16 retval=kFalse;
 	
 	for(int numTxtFmOverset=0; numTxtFmOverset<CantidadDeTextFrameOverset;numTxtFmOverset++)
 	{
 		PMString XMPReg="";
 		XMPReg.AppendNumber(ListTextFrOrSt[ numTxtFmOverset ].UIDTextModel);
 		XMPReg.Append(",");
 		XMPReg.AppendNumber(ListTextFrOrSt[ numTxtFmOverset ].UIDTextFrameOrSt);
 		this->SaveXMP("N2PTxtFmeOversetArray",XMPReg, numTxtFmOverset);
 	}
 
 	return(retval);
 }
 
 
 
 
 

 
 bool16 N2PCTUtilities::CanFlowTextOversetOnNewFrame( UID UIDTextModelofElementoNota, UIDRef& FrameWhitOversetText )
 {
 	bool16 retval=kFalse;
 	do
 	{
 			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			//
			UIDRef UIDRefOfElementoNota(db,UIDTextModelofElementoNota);
			
			//Obtiene el Texmodel apartir del numero de UID
			InterfacePtr<ITextModel> mytextmodel(db,UIDTextModelofElementoNota, UseDefaultIID()); 
			if(mytextmodel==nil)
			{
				
				continue;
			}
			
			if(!EsStoryOverset(mytextmodel))
			{
				continue;
			}
			
			//Obtiene el Iparcel del Texmodel 
			InterfacePtr<IParcel> parcel(this->QueryParcelContaining(mytextmodel,0));
			if (!parcel)
			{
				continue;
			}	
			
			//Obtiene el TextFrame que contiene el caracter numero 1
			InterfacePtr<ITextFrameColumn> FrameContentModel;
			
			FrameContentModel.reset(parcel->QueryFrame());
			if (!FrameContentModel)
			{
				continue;
			}	
			
			//Verifica que este texto sea un texto multicolumna
			if(!Utils<ITextUtils>()->IsMultiColumnFrame(FrameContentModel))
			{
			
				InterfacePtr<ITextFrameColumn> frameConteiner(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel),UseDefaultIID());
				
				if(!Utils<ITextUtils>()->IsMultiColumnFrame(frameConteiner))
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}
	
				//Obtiene el texto Multicolumna que contiene el Texmodel
				InterfacePtr<ITextColumnSizer> textColumnSizerFlow(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel), UseDefaultIID());
				if (textColumnSizerFlow == nil)
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}	
				int32 NumDColumns=textColumnSizerFlow->GetNumberOfColumns();
				PMReal AnchoDColumns=textColumnSizerFlow->GetFixedWidth();
				
				//Obtiene el UIDRef del GraphicFrame(Frame Principal del Textstory)
				UIDRef refFrame = GetGraphicFrameRef(frameConteiner, kTrue);
				
				
				FrameWhitOversetText = this->CreateAndThreadTextFrame(refFrame);
				
				
				retval=kTrue;
			}	
 	}while(false);
 	return(retval)	;	
 }
 
 
 /*
*/
UIDRef N2PCTUtilities::CreateAndThreadTextFrame(const UIDRef& fromGraphicFrameUIDRef)
{
	UIDRef result = UIDRef::gNull;

	// Wrap the commands in a sequence.
	CmdUtils::SequencePtr seq();
	ErrorCode status = kFailure;

	do {
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
			
			
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		//Para sacar segun las coordenadas de la pagina
		PMRect boundsInParentCoo=this->Get_Coordinates_Reales(fromGraphicFrameUIDRef);
		
		
		
		// Create a new text frame.
		// Determine the bounds of the new text frame.
		// Make it the same size as the given frame, positioned alongside at a 10 point offset.
		InterfacePtr<IGeometry> geometry(fromGraphicFrameUIDRef, UseDefaultIID());
		ASSERT(geometry);
		if (!geometry) 
		{
			
			break;
		}
		PMMatrix inner2parent = ::InnerToParentMatrix(geometry);
		PMRect boundsInParentCoords = geometry->GetStrokeBoundingBox(inner2parent);
		//Saca copia
		
		boundsInParentCoords.MoveRel(boundsInParentCoords.Width() , boundsInParentCoords.Height());
		
		// Parent the new text frame on the same spread layer as the given text frame.
		InterfacePtr<IHierarchy> hierarchy(fromGraphicFrameUIDRef, UseDefaultIID());
		ASSERT(hierarchy);
		if (!hierarchy) {
			
			break;
		}
		UIDRef parentUIDRef = UIDRef(fromGraphicFrameUIDRef.GetDataBase(), hierarchy->GetLayerUID());

		// Get the number of columns in the given text frame.
		InterfacePtr<ITextColumnSizer> frameColumnSizer(fromGraphicFrameUIDRef.GetDataBase(), this->GetTextContentUID(fromGraphicFrameUIDRef), UseDefaultIID());
		ASSERT(frameColumnSizer);
		if (!frameColumnSizer) {
			
			break;
		}
		int32 numberOfColumns = frameColumnSizer->GetNumberOfColumns();
		PMReal AnchoDColumns=frameColumnSizer->GetFixedWidth();
		
		
		
		// Determine if the given text frame is vertical.
		InterfacePtr<ITextFrameColumn> textFrame(frameColumnSizer, UseDefaultIID());
		if (!textFrame) {
		
			break;
		}
		
		//
		UIDRef UIDRefOfElementoNota(db,textFrame->GetTextModelUID());
		
		//
		int32 CantidadLinesOverset = Utils<ITextUtils>()->CountOversetLines(UIDRefOfElementoNota);
		
		InterfacePtr<IFrameList> frameList(textFrame->QueryFrameList());
		if (!frameList) {
			
			break;
		}
		InterfacePtr<ITextParcelList> textParcelList(frameList,UseDefaultIID());
		if (!textParcelList) {
			
			break;
		}
		bool16 isVertical = textParcelList->GetIsVertical();

		
		
 		
 		
 		PMReal TamanoDeInterlineado = 0.0;
 		if(!this->GetTamanoInterlineado(frameList,TamanoDeInterlineado))
 			break;
 		//Pone el tamaño de la columna para el nuevo textframe
		boundsInParentCoords.SetHeight(TamanoDeInterlineado * CantidadLinesOverset);
		boundsInParentCoords.SetWidth(AnchoDColumns);
		
 			// Create the new frame based on the properties of the given text frame determined above.
			SDKLayoutHelper layoutHelper;
			UIDRef toGraphicFrameUIDRef = layoutHelper.CreateTextFrame(parentUIDRef, boundsInParentCoords, 1, isVertical);
			if (toGraphicFrameUIDRef == UIDRef::gNull) {
			
				break;
			}

			// Link the out-port of the given text frame to the in-port of the new text frame.
			if (this->CanThreadTextFrames(fromGraphicFrameUIDRef.GetDataBase(), fromGraphicFrameUIDRef.GetUID(), toGraphicFrameUIDRef.GetUID()) == kFalse) {
			
				break;
			}
			if (this->ThreadTextFrames(fromGraphicFrameUIDRef.GetDataBase(), fromGraphicFrameUIDRef.GetUID(), toGraphicFrameUIDRef.GetUID()) != kSuccess) {
			
				break;
			}
		
			// If we get here we have successfully created a new text frame and linked it to the given text frame.
			//seq.SetState(kSuccess);
			status = kSuccess;
			result = toGraphicFrameUIDRef;
		
		
			/*///Hace ajuste del Frame al texto
			InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
			// Set the FitContentToFrameCmd's ItemList:
			fitCmd->SetItemList(UIDList(toGraphicFrameUIDRef));
			// Process the FitContentToFrameCmd:
			//CAlert::ErrorAlert("Ajusta Frame a contenido");
			if (CmdUtils::ProcessCommand(fitCmd) != kSuccess)
			{
				ASSERT_FAIL("Error al intentar ajustar imagen");
			}*/
		
		
		
	
			UID colorUID = BuscarOcrearColor("N2PColor Red",0,100,100,0);
			if(colorUID==nil)
			{
				break;
			}
		
			InterfacePtr<IPersistUIDData> fillRenderAttr((IPersistUIDData*)::CreateObject(kGraphicStyleFillRenderingAttrBoss,IID_IPERSISTUIDDATA));
			fillRenderAttr->SetUID(colorUID);
				
			//obtengo un comando
			InterfacePtr<ICommand>	gfxApplyCmd(CmdUtils::CreateCommand(kGfxApplyAttrOverrideCmdBoss));//se crea el comado para aplicar color al frame o rectangulo
				
			//pones el UIRef del frame al que se le va a plicar el color
			gfxApplyCmd->SetItemList(UIDList(toGraphicFrameUIDRef));//

			//se obtiene la interfaz de IPMUnknownData para el comando
			InterfacePtr<IPMUnknownData> pifUnknown(gfxApplyCmd,UseDefaultIID());

			//pones el color
			pifUnknown->SetPMUnknown(fillRenderAttr);
		
			ErrorCode err = CmdUtils::ProcessCommand(gfxApplyCmd);
						
	} while(false);
			if (status!=kSuccess)
				ErrorUtils::PMSetGlobalErrorCode(status);
	return result;
}


 bool16 N2PCTUtilities::EsStoryOverset(InterfacePtr<ITextModel> textModel)
 {	
 	bool16 overset=kFalse;
 	do
 	{
 		InterfacePtr<IFrameList> frameList(textModel->QueryFrameList());
 		if(!frameList)
 		{
 			break;
 		}
 		
 		overset=Utils<ITextUtils>()->IsOverset(frameList);
 	}while(false);
 	return overset;
 }
 
 
 
 /*
*/
IParcel* N2PCTUtilities::QueryParcelContaining(ITextModel* textModel, const TextIndex at)
{
	InterfacePtr<IParcel> result;
	do {
		// Find the parcel list that displays the TextIndex.
		InterfacePtr<ITextParcelList> textParcelList(textModel->QueryTextParcelList(at));
		if (!textParcelList) {
			// Likely it belongs to a note or deleted text or 
			// another story thread that does not use parcels.
			// Or perhaps we are not in the layout view (i.e.
			// we are in a story or galley view).
			break;
		}
		ParcelKey firstDamagedParcelKey = textParcelList->GetFirstDamagedParcel();
		if (firstDamagedParcelKey.IsValid() == kTrue){
			// Recompose the text in this parcel list.
			InterfacePtr<ITextParcelListComposer> textParcelListComposer(textParcelList, UseDefaultIID());
			ASSERT(textParcelListComposer);
			if (!textParcelListComposer) {
				break;
			}
			textParcelListComposer->RecomposeThruTextIndex(kInvalidParcelIndex/*recompose all*/);
		}

		// Find the parcel that displays the TextIndex.
		ParcelKey parcelKey = textParcelList->GetParcelContaining(at);
		if (parcelKey.IsValid() == kFalse) {
			// Parcel is not displayed. Could be that the parcel
			// that displays this TextIndex is an overset text
			// cell in a table for example.
			break;
		}
		InterfacePtr<IParcelList> parcelList(textParcelList, UseDefaultIID());
		ASSERT(parcelList);
		if (!parcelList) {
			break;
		}
		InterfacePtr<IParcel> parcel(parcelList->QueryParcel(parcelKey));
		ASSERT(parcel);
		if (!parcel) {
			break;
		}
		result = parcel;
	} while(false);
	return result.forget();
}






/*
*/
bool16 N2PCTUtilities::CanThreadTextFrames(IDataBase* database, const UID& fromGraphicFrameUID, const UID& toGraphicFrameUID)
{
	bool16 result = kFalse;
	do {
		UID fromMultiColumnItemUID = this->GetTextContentUID(UIDRef(database, fromGraphicFrameUID));
		if (fromMultiColumnItemUID == kInvalidUID) {
			break;
		}

		// Check if the story underlying fromGraphicFrameUID is locked.
		InterfacePtr<IMultiColumnTextFrame> fromMCF(database, fromMultiColumnItemUID, UseDefaultIID());
		ASSERT(fromMCF != nil);
		if (fromMCF == nil) {
			break;
		}
		InterfacePtr<ITextModel> fromTextModel(fromMCF->QueryTextModel());
		ASSERT(fromTextModel != nil);
		if (fromTextModel == nil) {
			break;
		}
		InterfacePtr<IItemLockData> fromTextLockData(fromTextModel, UseDefaultIID());
		ASSERT(fromTextLockData != nil);
		if (fromTextLockData == nil) {
			break;
		}
		if (fromTextLockData) {
			if (fromTextLockData->GetInsertLock() == kTrue || fromTextLockData->GetAttributeLock() == kTrue) {
				// A lock exists so don't link.
				break;
			}
		}

		// Check if the story underlying toGraphicFrameUID is locked.
		UID toMultiColumnItemUID = this->GetTextContentUID(UIDRef(database, toGraphicFrameUID));
		if (toMultiColumnItemUID == kInvalidUID) {
			break;
		}
		InterfacePtr<IMultiColumnTextFrame> toMCF(database, toMultiColumnItemUID, UseDefaultIID());
		ASSERT(toMCF != nil);
		if (toMCF == nil) {
			break;
		}
		InterfacePtr<ITextModel> toTextModel(toMCF->QueryTextModel());
		ASSERT(toTextModel != nil);
		if (toTextModel == nil) {
			break;
		}
		InterfacePtr<IItemLockData> toTextLockData(toTextModel, UseDefaultIID());
		ASSERT(toTextLockData != nil);
		if (toTextLockData == nil) {
			break;
		}
		if (toTextLockData) {
			if (toTextLockData->GetInsertLock() == kTrue || toTextLockData->GetAttributeLock() == kTrue) {
				// A lock exists so don't link.
				break;
			}
		}

		// If we get here there are no text locks on the stories involved in the link operation.

		// If the story underlying toGraphicFrameUID is empty we can link the frame
		// anywhere in the frame list.
		/*if (toTextModel->TotalLength() == 1) {
			result = kTrue;
			break;
		}*/

		// The story underlying toGraphicFrameUID is not empty.

		// Check we are appending toGraphicFrameUID onto the end of the frame list underlying fromGraphicFrameUID.
		InterfacePtr<IFrameList> fromFrameList(fromMCF->QueryFrameList());
		ASSERT(fromFrameList);
		if (!fromFrameList) {
			break;
		}
		InterfacePtr<IFrameList> toFrameList(toMCF->QueryFrameList());
		ASSERT(toFrameList);
		if (!toFrameList) {
			break;
		}
		if (!fromMCF->GetIsLastMCF()) {
			// Frame being linked from is not the last one in its frame list.
			break;
		}
		if (!toMCF->GetIsFirstMCF()) {
			// Frame being linked to is not the first one in its frame list.
			break;
		}

		// If we get here we are linking the last frame in the frame list underlying fromGraphicFrameUID
		// onto the first frame in the frame list underlying fromGraphicFrameUID.
		// We have an end to start link.
		result = kTrue;

	} while(false);
	return result;
}



/*
*/
ErrorCode N2PCTUtilities::ThreadTextFrames(IDataBase* database, const UID& fromGraphicFrameUID, const UID& toGraphicFrameUID)
{
	ErrorCode status = kFailure;
	CmdUtils::SequencePtr seq();

	do {
		UID fromMultiColumnItemUID =this->GetTextContentUID(UIDRef(database, fromGraphicFrameUID));
		if (fromMultiColumnItemUID== kInvalidUID) {
			break;
		}
		InterfacePtr<ITextColumnSizer> fromTextColumnSizer(database, fromMultiColumnItemUID, UseDefaultIID());
		ASSERT(fromTextColumnSizer != nil);
		if (fromTextColumnSizer == nil) {
			break;
		}
		InterfacePtr<ITextFrameColumn> fromTextFrame(fromTextColumnSizer, UseDefaultIID());
		ASSERT(fromTextFrame != nil);
		if (fromTextFrame == nil) {
			break;
		}
		InterfacePtr<ITextModel> fromTextModel(fromTextFrame->QueryTextModel());
		ASSERT(fromTextModel != nil);
		if (fromTextModel == nil) {
			break;
		}

		UID toMultiColumnItemUID = this->GetTextContentUID(UIDRef(database, toGraphicFrameUID));
		if (toMultiColumnItemUID== kInvalidUID) {
			break;
		}
		InterfacePtr<ITextColumnSizer> toTextColumnSizer(database, toMultiColumnItemUID, UseDefaultIID());
		ASSERT(toTextColumnSizer != nil);
		if (toTextColumnSizer == nil) {
			break;
		}
		InterfacePtr<ITextFrameColumn> toTextFrame(toTextColumnSizer, UseDefaultIID());
		ASSERT(toTextFrame != nil);
		if (toTextFrame == nil) {
			break;
		}
		InterfacePtr<ITextModel> toTextModel(toTextFrame->QueryTextModel());
		ASSERT(toTextModel != nil);
		if (toTextModel == nil) {
			break;
		}

		// If there is text content in the story underlying the text frame
		// given by toGraphicFrameUID, append this text to the end of the story
		// underlying the text frame given by parameter fromGraphicFrameUID.
		if (toTextModel->TotalLength() > 1) {
			Utils<ITextUtils> textUtils;
			ASSERT(textUtils);
			if (!textUtils) {
				break;
			}
			InterfacePtr<ICommand> moveStoryRangeCmd(textUtils->QueryMoveStoryFromAllToEndCommand(::GetUIDRef(toTextModel), ::GetUIDRef(fromTextModel)));
			ASSERT(moveStoryRangeCmd);
			if (!moveStoryRangeCmd) {
				break;
			}
			status = CmdUtils::ProcessCommand(moveStoryRangeCmd);
			ASSERT_MSG(status == kSuccess, "ITextUtils::QueryMoveStoryFromAllToEndCommand failed");
			if (status != kSuccess) {
				break;
			}
			if (toTextModel->TotalLength() > 1) {
				ASSERT_FAIL("toTextModel->TotalLength() should be 1, the content should have been merged");
			}
		}

		// Link the text flow across kMultiColumnItemBoss objects.
		// Requires an item list containing pairs of UID that refer to the
		// objects to be linked together in from(out-port)/to(in-port) order.
	    InterfacePtr<ICommand> textLinkCmd(CmdUtils::CreateCommand(kTextLinkCmdBoss));
		ASSERT(textLinkCmd != nil);
		if (textLinkCmd == nil) {
			break;
		}
		UIDList itemList(database);
		itemList.Append(fromMultiColumnItemUID);
		itemList.Append(toMultiColumnItemUID);
		// Note that the story underlying the object referenced by toMultiColumnItemUID
		// must be empty. You need to move any text content it has before linking the
		// frames as illustrated by the code above.
		textLinkCmd->SetItemList(itemList);
		status = CmdUtils::ProcessCommand(textLinkCmd);
		ASSERT_MSG(status == kSuccess, "kTextLinkCmdBoss failed");

	} while (false);
			if (status!=kSuccess)
				ErrorUtils::PMSetGlobalErrorCode(status);
	return status;
}



/*
*/
UIDRef N2PCTUtilities::GetGraphicFrameRef(const InterfacePtr<ITextFrameColumn>& textFrame, const bool16 isTOPFrameAllowed)
{
	UIDRef result = UIDRef::gNull;

	do {
		ASSERT(textFrame);
		if (!textFrame) {
			break;
		}

		// Check for a regular text frame by going up
		// the hierarchy till we find a parent object
		// that aggregates IGraphicFrameData. This is
		// the graphic frame that contains the text content.
		UIDRef graphicFrameDataUIDRef = UIDRef::gNull;
		InterfacePtr<IHierarchy> child(textFrame, UseDefaultIID());
		if (child == nil) {
			break;
		}
		do 
		{
			InterfacePtr<IHierarchy> parent(child->QueryParent());
			if (parent == nil) {
				break;
			}
			InterfacePtr<IGraphicFrameData> graphicFrameData(parent, UseDefaultIID());
			if (graphicFrameData != nil)
			{
				// We have a text frame.
				graphicFrameDataUIDRef = ::GetUIDRef(graphicFrameData);
				break;
			}
			child = parent;
		} while(child != nil);

		if (graphicFrameDataUIDRef == UIDRef::gNull) {
			break;
		}

		InterfacePtr<ITOPSplineData> topSplineData(graphicFrameDataUIDRef, UseDefaultIID());
		if (topSplineData) {
			// We have a text on a path frame.
			if (isTOPFrameAllowed == kTrue) {
				// Return the text on a path frame
				result = graphicFrameDataUIDRef;
			}
			else 
			{
				// Return the graphic frame associated with the text on a path frame.
				UID mainSplineItemUID = topSplineData->GetMainSplineItemUID();
				ASSERT(mainSplineItemUID != kInvalidUID);
				result = UIDRef(graphicFrameDataUIDRef.GetDataBase(), mainSplineItemUID);
			}
		}
		else {
			// We have a normal graphic frame.
			result = graphicFrameDataUIDRef;
		}

	} while(false);

	return result;

}

/*
*/
UID N2PCTUtilities::GetTextContentUID(const UIDRef& graphicFrameUIDRef)
{
	UID result = kInvalidUID;
	InterfacePtr<IGraphicFrameData> graphicFrameData(graphicFrameUIDRef, UseDefaultIID());
	if (graphicFrameData)
	{
		result = graphicFrameData->GetTextContentUID();
	}
	return result;
}



bool16 N2PCTUtilities::SetIgnoredTextWrap(const UIDRef& frameUIDRef)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<ICommand> SetIgnoreWrapCmdBoss(CmdUtils::CreateCommand(kSetIgnoreWrapCmdBoss)); 
		if(SetIgnoreWrapCmdBoss == nil) 
		{ 
			ASSERT_FAIL("SetIgnoreWrapCmdBoss = nil "); 
			return kFalse; 
		} 
		
		InterfacePtr<IBoolData> SetIgnoreWrapCmdBossData(SetIgnoreWrapCmdBoss, IID_IIGNOREWRAP); 
		if(SetIgnoreWrapCmdBossData == nil) 
		{ 
			ASSERT_FAIL("SetIgnoreWrapCmdBossData = nil "); 
			return kFalse; 
		} 
		SetIgnoreWrapCmdBossData->Set(kFalse); 
		SetIgnoreWrapCmdBoss->SetItemList(UIDList(frameUIDRef)); 
		if (CmdUtils::ProcessCommand(SetIgnoreWrapCmdBoss)!= kSuccess) 
		{ 
			ASSERT_FAIL("Error Command Textwrap"); 
			return kFalse; 
		} 
	
		 retval=kTrue;
	}while(false);
	return(retval);
}



void N2PCTUtilities::crearColores()
{
	//llamada a la funcion buscaOcreaColor mandando el nombre del color y la mescla de los colores para generar elm deseado
	BuscarOcrearColor("N2PColor Red",0,100,100,0);
	
}


/**
	ESTA BUNCION SU FUNCION PRINCIPAL ES LA BUSQUEDA DE UN COLOR POR SU NOMBRE EN CASO
	DE QUE NO EXISTA SE DEBERA CREAR USANDO LA MEZCLA DE COLORES QUE SE LE MANDA
*/
UID N2PCTUtilities::BuscarOcrearColor(const PMString& swatchName, 
								  const PMReal rCyan, 
								  const PMReal rMagenta, 
								  const PMReal rAmarillo,
								  const PMReal rNegro)
{

	//para indicar el tipo de color CMYK
	//Trace("ResolveCMYKColorSwatch\n"); // Make debugging easier.

	//UID del color
	UID colorUID = kInvalidUID;
	do
	{
		//Documento
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			// How did we POSSIBLY get here when our menu item should have been disabled if we
			// have no document and have no selection?
			ASSERT_FAIL("TSCore::ResolveCMYKColorSwatch: document invalid");
			break;
		}	

		//obtien la lista de colores en el documento actual
		InterfacePtr<ISwatchList> swatchList(document->GetDocWorkSpace(), IID_ISWATCHLIST);
		if (swatchList == nil)
		{
			ASSERT_FAIL("TSCore::ResolveRGBColorSwatch: swatchList invalid");
			break;
		}

		
		PMString NombreColor=swatchName;
		NombreColor.Translate();
		NombreColor.SetTranslatable(kTrue);
		//busqueda del color por nombre y regresaa el UIDRef
		UIDRef colorUIDRef = swatchList->FindSwatch(NombreColor);
		
		if (colorUIDRef != nil)//en caso de que se encontro
		{
			colorUID = colorUIDRef.GetUID();//obtenge el UID del color
		}
		else//en caso de que no se haya encontrado se crea
		{
			// Must create the color.  Didn't find it in swatch list.
			
			// First create our command and get at the data:
		    InterfacePtr<ICommand> newColorCmd(CmdUtils::CreateCommand(kNewColorCmdBoss));

			// We'll be adding to the swatch list:
			InterfacePtr<IUIDData> swatchListData(newColorCmd, IID_IUIDDATA);
			if (swatchListData == nil)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: swatchListData invalid");
				break;
			}		

			//se adiciona la lista de colores al comando
			swatchListData->Set(swatchList);

			// Create the rendering object:
		    InterfacePtr<IRenderingObject> renderingObject(newColorCmd, IID_IRENDERINGOBJECT);
		    if (renderingObject == nil)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: renderingObject invalid");
				break;
			}		
			
		    // Fill out the name:
			renderingObject->SetSwatchName(NombreColor);

		    // Supply the actual color via the colorData interface:
		    InterfacePtr<IColorData> colorData(newColorCmd, IID_ICOLORDATA);
		    
		    const int32 colorSpace = (kPMCsCalCMYK);	//CMYK color space.

		    // Create the color array:
			ColorArray rgbColor;
	
			rgbColor.push_back(rCyan); // Cyan (0.0...1.0)
			rgbColor.push_back(rMagenta); // MAgenta (0.0...1.0)
			rgbColor.push_back(rAmarillo); // Yellow (0.0...1.0)
			rgbColor.push_back(rNegro); // Black (0.0...1.0)
			
			//pone el array del colore
		    colorData->SetColorData(colorSpace, rgbColor);

			//procesa el comando
			ErrorCode error = CmdUtils::ProcessCommand(newColorCmd);
			if (error != kSuccess)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: newColorCmd failed");
				break;
			}

			// Now get our UID out of the list and add it to the swatch:
		    const UIDList* colorUIDList = newColorCmd->GetItemList();
		    if (colorUIDList == nil)
		    {
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: UIDList invalid");
				break;
			}
		    
		    // We're only going to add the very first color here, not the whole list:
		    colorUID = colorUIDList->First();

			// As of build 325 we have to manually add to the Swatch list,
			// it's no longer part of the NewColorCmd:
			InterfacePtr<ICommand> addSwatchesCmd(CmdUtils::CreateCommand(kAddSwatchesCmdBoss));
			InterfacePtr<IUIDData> addSwatchesCmdListData(addSwatchesCmd, IID_IUIDDATA);
			
			if (addSwatchesCmdListData == nil || addSwatchesCmd == nil)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: addSwatches invalid");
				break;
			}
			
			addSwatchesCmd->SetName("New Color");				
			
			addSwatchesCmdListData->Set(swatchList);
			addSwatchesCmd->SetItemList(UIDList(colorUIDList->GetDataBase(), colorUID));

			// Finally, add the color to the swatch list via this command:
			error = CmdUtils::ProcessCommand(addSwatchesCmd);
			if (error != kSuccess)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: addSwatchesCmd failed");
				break;
			}

			// Now let's try to find the color from the swatch list:
			colorUIDRef = swatchList->FindSwatch(NombreColor);

			if (colorUIDRef == nil)
			{
				ASSERT_FAIL("TSCore::CreateStyle: even after new color, cannot find swatch");
				break;
			}
			
			colorUID = colorUIDRef.GetUID();
		}
	} while (false);
	return colorUID;
}



 /*
 */
 int N2PCTUtilities::LengthXMP(PMString PathStruct)
{
	int total=0;
	do 
	{
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		total = metaDataAccess->Count("http://ns.adobe.com/xap/1.0/",PathStruct);
	}while(false);
	return total;
}


void N2PCTUtilities::SaveXMP(PMString PathStruct, PMString ads, int pos)
{
	do 
	{
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (doc == nil) 
		{
			
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		if (pos == 0)
		{
			
			metaDataAccess->AppendArrayItem("http://ns.adobe.com/xap/1.0/",PathStruct,ads,metadata_alt);
			metaDataAccess->SetArrayItem("http://ns.adobe.com/xap/1.0/",PathStruct,1,ads);
		}
		else
		{
			
			metaDataAccess->SetArrayItem("http://ns.adobe.com/xap/1.0/",PathStruct,pos+1,ads);
		}
			
			
	}while(false);
}

PMString N2PCTUtilities::GetXMP(PMString PathStruct,int ads)
{
	PMString elemento;
	do 
	{
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		metaDataAccess->GetArrayItem("http://ns.adobe.com/xap/1.0/",PathStruct,ads,elemento);
	}while(false);
	return elemento;
}


bool16 N2PCTUtilities::DeleteXMP(PMString Path)
 {
 		
 	do
 	{
 		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
 		////Borra de XMP lista de Frame con Overset
 		InterfacePtr<IMetaDataAccess> metaDataAccess(document, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		metaDataAccess->Remove("http://ns.adobe.com/xap/1.0/",Path);
		
 	}while(false);
 	return(kTrue);
 		
 }
 
 
bool16 N2PCTUtilities::GetTamanoInterlineado(InterfacePtr<IFrameList> frameList,PMReal& LeadingOfLasParcel)
{
	bool16 retval=kFalse;
	do
	{
		
		InterfacePtr<ITextParcelList> textParcelList(frameList, UseDefaultIID());
		ASSERT(textParcelList != nil);
		if (textParcelList == nil) {
			break;
		}
		
		InterfacePtr<IParcelList> parcelList(frameList, UseDefaultIID());
		ASSERT(parcelList != nil);
		if (parcelList == nil) {
			break;
		}
		
		ParcelKey fromParcelKey = parcelList->GetNthParcelKey(0);
		int32 numberOfParcels=parcelList->GetParcelCount();
		PMReal estimatedDepth=0.0;
		
		int32 fromParcelIndex = parcelList->GetParcelIndex(fromParcelKey);
		ASSERT(fromParcelIndex >= 0);
		if (fromParcelIndex < 0) {
			break;
		}
		ASSERT(numberOfParcels > 0 && numberOfParcels <= parcelList->GetParcelCount());
		if(numberOfParcels <= 0 || numberOfParcels > parcelList->GetParcelCount()) {
			break;
		}

		// Make sure the text is composed.
		ParcelKey firstDamagedParcelKey = textParcelList->GetFirstDamagedParcel();
		if (firstDamagedParcelKey.IsValid() == kTrue){
			InterfacePtr<ITextParcelListComposer> textParcelListComposer(textParcelList, UseDefaultIID());
			ASSERT(textParcelListComposer);
			if (!textParcelListComposer) {
				break;
			}
			textParcelListComposer->RecomposeThruTextIndex(fromParcelIndex + numberOfParcels - 1);
		}
		
		// Scan the wax and use the position of the first and last line
		// in each parcel that displays text to estimate the overall depth.
		InterfacePtr<IWaxStrand> waxStrand(textParcelList->GetWaxStrandRef(), UseDefaultIID());
		if (waxStrand == nil) {
			break;
		}
		K2::scoped_ptr<IWaxIterator> firstIterator(waxStrand->NewWaxIterator());
		K2::scoped_ptr<IWaxIterator> lastIterator(waxStrand->NewWaxIterator());
		ASSERT(firstIterator != nil && lastIterator != nil);
		if (firstIterator == nil || lastIterator == nil) 
		{
			break;
		}
		
		int32 LastFrameConTexto=0;
		PMString MsgBox="";
		PMRect BoundparcelLastParcel;
		PMReal YPosicionFirstLine=0.0;
		PMReal YPosicionLastLine=0.0;
		int32 IndexStartTextLastParcel=0;
		int32 IndexEndTextLastParcel=0;
		//
		for (int32 i = fromParcelIndex; i < fromParcelIndex + numberOfParcels; i++) 
		{
			
			// Get the range of text in this parcel.
			ParcelKey key = parcelList->GetNthParcelKey(i);
			TextIndex start = textParcelList->GetTextStart(key);
			
			//Para Obtener el altpo del ultimo parcel
			BoundparcelLastParcel=parcelList->GetParcelBounds(key);
			
			
		
			int32 span = textParcelList->GetTextSpan(key);
			if (span <= 0) 
			{
				// Some parcels may not be able to display any text.
				// But we can't assume that the story is displayed
				// in its entirety by preceeding parcels. It is possible
				// that a parcel was not big enough to accept text and
				// so text flowed into a subsequent and larger parcel.
				// To handle this continue and consider the depth
				// of other parcels in the list.
				ASSERT_FAIL("No se fluyo sobre este span<0");
				continue;
			}
			TextIndex end = start + span - 1;
			
			
			
			// Get the wax lines that display the first and last characters in the parcel.
			IWaxLine* firstLine = firstIterator->GetFirstWaxLine(start);
			IWaxLine* lastLine = lastIterator->GetFirstWaxLine(end);	
			ASSERT(firstLine != nil && lastLine != nil);
			if (firstLine == nil || lastLine == nil) 
			{
				ASSERT_FAIL("no tiene una linea inicial y una linea final");
				continue;
			}
				
			////////////////////////////////////////////////////////////
			
				LastFrameConTexto=i;//este sera el ultimo frame contexto
			
				
				//Toma el tamaño de la Ultima linea o el Leading	
				LeadingOfLasParcel=lastLine->GetLineHeight();
		
				// Estimate the depth of the text.	
				estimatedDepth = lastLine->GetYPosition() - (firstLine->GetYPosition() - firstLine->GetTOFLineHeight());
				
				YPosicionFirstLine = firstLine->GetYPosition();
				
				YPosicionLastLine = lastLine->GetYPosition();
			
				IndexStartTextLastParcel=start;
			
				IndexEndTextLastParcel=end;
			
			///////////////////////////////////////////////////////
			
			
		}
		
			MsgBox="";
			MsgBox.Append("LastFrameConTexto: ");
			MsgBox.AppendNumber(LastFrameConTexto);
			
			MsgBox.Append("\nAlto de Parcel: ");
			MsgBox.AppendNumber(BoundparcelLastParcel.Height());
			MsgBox.Append("\nStart  de Parcel: ");
			MsgBox.AppendNumber(IndexStartTextLastParcel);
			MsgBox.Append("\nStart  de Parcel: ");
			MsgBox.AppendNumber(IndexEndTextLastParcel);
			MsgBox.Append("\nStart Posicion de primera linea Parcel: ");
			MsgBox.AppendNumber(YPosicionFirstLine);
			
			MsgBox.Append("\nEnd Posicion de primera linea Parcel: ");
			MsgBox.AppendNumber(YPosicionLastLine);
			MsgBox.Append("\nLeading of lastLine: ");
			MsgBox.AppendNumber(LeadingOfLasParcel);
			MsgBox.Append("\nDepth of Text Parcel: ");
			MsgBox.AppendNumber(estimatedDepth);
			
			// Sia un cabia mas lineas en el ultimo parcel
			if((BoundparcelLastParcel.Height()-estimatedDepth)>LeadingOfLasParcel)
			{
					PMReal NumLineasQueCabe=(BoundparcelLastParcel.Height()-estimatedDepth)/LeadingOfLasParcel;
					
					NumLineasQueCabe=NumLineasQueCabe + ( ( ( numberOfParcels - ( LastFrameConTexto + 1) ) * BoundparcelLastParcel.Height() ) / LeadingOfLasParcel );
					MsgBox.Append("\n\nUnder: ");
					MsgBox.AppendNumber(NumLineasQueCabe);
					MsgBox.Append(" Lines ");
			}
			else
			{
				//si no fue el parcel
				if( ( LastFrameConTexto + 1) < numberOfParcels )
				{
					PMReal NumLineasQueCabe=( ( ( numberOfParcels - ( LastFrameConTexto + 1) ) * BoundparcelLastParcel.Height() ) / LeadingOfLasParcel );
					MsgBox.Append("\n\nUnder: ");
					MsgBox.AppendNumber(NumLineasQueCabe);
					MsgBox.Append(" Lines ");
				}
				
			}
			
		retval=kTrue;
	}while(false);
	return retval;
}
  
  
  
/*
*/
ErrorCode N2PCTUtilities::EstimateStoryDepth(const InterfacePtr<ITextModel>& textModel, PMReal& estimatedDepth,int32& NumeroDeLineaAntesDelOverset)
{
	
	ErrorCode status = kFailure;
	estimatedDepth = 0.0;
	do {
		InterfacePtr<IFrameList> frameList(textModel->QueryFrameList());
		ASSERT(frameList != nil);
		if (frameList == nil) {
			break;
		}
		
		InterfacePtr<IParcelList> parcelList(frameList, UseDefaultIID());
		ASSERT(parcelList != nil);
		if (parcelList == nil) {
			break;
		}
		InterfacePtr<ITextParcelList> textParcelList(frameList, UseDefaultIID());
		ASSERT(textParcelList != nil);
		if (textParcelList == nil) {
			break;
		}
		
		
		UID UIDTextModelofElementoNota = frameList->GetTextModelUID();
		int32 NumLinesFaltantes=0;
		int32 UltimoCaracterMostrado=0;
		status = this->CountWordsAndLines(UIDTextModelofElementoNota, textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumLinesFaltantes, UltimoCaracterMostrado);
				
		//status = this->EstimateTextDepth(textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumeroDeLineaAntesDelOverset,Ultimocaractermostrado);
	} while (false);
	return status;
}


/*
*/
ErrorCode N2PCTUtilities::EstimateTextDepth(const InterfacePtr<ITextParcelList>& textParcelList, 
											ParcelKey fromParcelKey, 
											int32 numberOfParcels, 
											PMReal& estimatedDepth,
											int32& NumeroDeLineaAntesDelOverset,
											int32& UltimoCaracterMostrado
											)
{
	ErrorCode status = kFailure;
	do {
		// Validate parameters.
		ASSERT(textParcelList != nil);
		if (textParcelList == nil) {
			break;
		}
		InterfacePtr<IParcelList> parcelList(textParcelList, UseDefaultIID());
		ASSERT(parcelList != nil);
		if (parcelList == nil) {
			break;
		}
		int32 fromParcelIndex = parcelList->GetParcelIndex(fromParcelKey);
		ASSERT(fromParcelIndex >= 0);
		if (fromParcelIndex < 0) {
			break;
		}
		ASSERT(numberOfParcels > 0 && numberOfParcels <= parcelList->GetParcelCount());
		if(numberOfParcels <= 0 || numberOfParcels > parcelList->GetParcelCount()) {
			break;
		}

		// Make sure the text is composed.
		ParcelKey firstDamagedParcelKey = textParcelList->GetFirstDamagedParcel();
		if (firstDamagedParcelKey.IsValid() == kTrue){
			InterfacePtr<ITextParcelListComposer> textParcelListComposer(textParcelList, UseDefaultIID());
			ASSERT(textParcelListComposer);
			if (!textParcelListComposer) {
				break;
			}
			textParcelListComposer->RecomposeThruTextIndex(fromParcelIndex + numberOfParcels - 1);
		}
		
		// Scan the wax and use the position of the first and last line
		// in each parcel that displays text to estimate the overall depth.
		InterfacePtr<IWaxStrand> waxStrand(textParcelList->GetWaxStrandRef(), UseDefaultIID());
		if (waxStrand == nil) {
			break;
		}
		
		/////////////////////////////////
		for (int32 j = fromParcelIndex; j < fromParcelIndex + numberOfParcels; j++) 
		{
		
		 	K2::scoped_ptr<IWaxIterator> waxIterator(waxStrand->NewWaxIterator());
    	    if (waxIterator == nil) 
    	    {
    	         break;
    	    }
    	    
    	    ParcelKey key = parcelList->GetNthParcelKey(j);
			TextIndex start = textParcelList->GetTextStart(key);
			int32 span = textParcelList->GetTextSpan(key);
			if (span <= 0) 
			{
				// Some parcels may not be able to display any text.
				// But we can't assume that the story is displayed
				// in its entirety by preceeding parcels. It is possible
				// that a parcel was not big enough to accept text and
				// so text flowed into a subsequent and larger parcel.
				// To handle this continue and consider the depth
				// of other parcels in the list.
				ASSERT_FAIL("No se fluyo sobre este span<0");
				continue;
			}
			TextIndex end = start + span - 1;
			
			
    	    IWaxLine* waxLine = waxIterator->GetFirstWaxLine(start);
    	    
    	    int32   i = 0;
    	    
    	    //SNIPLOG("#, TextOrigin, GetTextSpan, Leading(pts)");
   	     	while (waxLine != nil && waxLine->TextOrigin() < end) 
    	     {
    	     	PMString kk="numLine: ";
    	     	kk.AppendNumber(i);
    	     	kk.Append("\nSpan Line:");
    	     	kk.AppendNumber(waxLine->GetTextSpan());
    	     	kk.Append("\nY inicial:");
    	     	kk.AppendNumber(waxLine->GetYPosition());
    	     	kk.Append("\nY Advence:");
    	     	kk.AppendNumber(waxLine->GetYAdvance());
    	     	kk.Append("\nY Advence:");
    	     	kk.AppendNumber(waxLine->GetYAdvance());
    	     	
    	     	//CAlert::InformationAlert(kk);
       	      	/*SNIPLOG("%d, %d, %d, %2.1f pts.", 
                	i, 
                 	waxLine->TextOrigin(),
                 	
                 	waxLine->GetYAdvance());*/
            		waxLine = waxIterator->GetNextWaxLine();
             	i++;
        	 }
		}
		///////////////////////////////////
		
		/*K2::scoped_ptr<IWaxIterator> firstIterator(waxStrand->NewWaxIterator());
		K2::scoped_ptr<IWaxIterator> lastIterator(waxStrand->NewWaxIterator());
		ASSERT(firstIterator != nil && lastIterator != nil);
		if (firstIterator == nil || lastIterator == nil) 
		{
			break;
		}
		
		int32 LastFrameConTexto=0;
		PMString MsgBox="";
		PMReal LeadingOfLasParcel=0.0;
		PMRect BoundparcelLastParcel;
		PMReal YPosicionFirstLine=0.0;
		PMReal YPosicionLastLine=0.0;
		int32 IndexStartTextLastParcel=0;
		
		
		
		//
		for (int32 i = fromParcelIndex; i < fromParcelIndex + numberOfParcels; i++) 
		{
			
			// Get the range of text in this parcel.
			ParcelKey key = parcelList->GetNthParcelKey(i);
			TextIndex start = textParcelList->GetTextStart(key);
			
			//Para Obtener el altpo del ultimo parcel
			BoundparcelLastParcel=parcelList->GetParcelBounds(key);
			
			
		
			int32 span = textParcelList->GetTextSpan(key);
			if (span <= 0) 
			{
				// Some parcels may not be able to display any text.
				// But we can't assume that the story is displayed
				// in its entirety by preceeding parcels. It is possible
				// that a parcel was not big enough to accept text and
				// so text flowed into a subsequent and larger parcel.
				// To handle this continue and consider the depth
				// of other parcels in the list.
				ASSERT_FAIL("No se fluyo sobre este span<0");
				continue;
			}
			TextIndex end = start + span - 1;
			
			
			
			// Get the wax lines that display the first and last characters in the parcel.
			IWaxLine* firstLine = firstIterator->GetFirstWaxLine(start);
			IWaxLine* lastLine = IWaxIterator
			->GetFirstWaxLine(end);	
			
			
			
			ASSERT(firstLine != nil && lastLine != nil);
			if (firstLine == nil || lastLine == nil) 
			{
				ASSERT_FAIL("no tiene una linea inicial y una linea final");
				continue;
			}
				
			////////////////////////////////////////////////////////////
			
				LastFrameConTexto=i;//este sera el ultimo frame contexto
			
				
				//Toma el tamaño de la Ultima linea o el Leading	
				LeadingOfLasParcel=lastLine->GetLineHeight();
		
				// Estimate the depth of the text.	
				estimatedDepth = lastLine->GetYPosition() - (firstLine->GetYPosition() - firstLine->GetTOFLineHeight());
				
				YPosicionFirstLine = firstLine->GetYPosition();
				
				YPosicionLastLine = lastLine->GetYPosition();
			
				IndexStartTextLastParcel=start;
			
				UltimoCaracterMostrado=end;
			
			///////////////////////////////////////////////////////
			
			
		}
		
			
			
			PMReal NumLineasQueCabe;
			// Sia un cabia mas lineas en el ultimo parcel
			if((BoundparcelLastParcel.Height()-estimatedDepth)>LeadingOfLasParcel)
			{
					NumLineasQueCabe = (BoundparcelLastParcel.Height()-estimatedDepth)/LeadingOfLasParcel;
					
					NumLineasQueCabe=NumLineasQueCabe + ( ( ( numberOfParcels - ( LastFrameConTexto + 1) ) * BoundparcelLastParcel.Height() ) / LeadingOfLasParcel );
					//MsgBox.Append("\n\nUnder: ");
					MsgBox.AppendNumber(NumLineasQueCabe);
					//MsgBox.Append(" Lines ");
					NumeroDeLineaAntesDelOverset = MsgBox.GetAsNumber();
			}
			else
			{
				//si no fue el parcel
				if( ( LastFrameConTexto + 1) < numberOfParcels )
				{
					NumLineasQueCabe = ( ( ( numberOfParcels - ( LastFrameConTexto + 1) ) * BoundparcelLastParcel.Height() ) / LeadingOfLasParcel );
					//MsgBox.Append("\n\nUnder: ");
					MsgBox="";
					MsgBox.AppendNumber(NumLineasQueCabe);
					//MsgBox.Append(" Lines ");
					
					NumeroDeLineaAntesDelOverset = MsgBox.GetAsNumber();
				}
				
			}*/
			
			
			/*MsgBox="";
			MsgBox.Append("LastFrameConTexto: ");
			MsgBox.AppendNumber(LastFrameConTexto);
			
			MsgBox.Append("\nAlto de Parcel: ");
			MsgBox.AppendNumber(BoundparcelLastParcel.Height());
			MsgBox.Append("\nStart  de Parcel: ");
			MsgBox.AppendNumber(IndexStartTextLastParcel);
			MsgBox.Append("\nStart  de Parcel: ");
			MsgBox.AppendNumber(IndexEndTextLastParcel);
			MsgBox.Append("\nStart Posicion de primera linea Parcel: ");
			MsgBox.AppendNumber(YPosicionFirstLine);
			
			MsgBox.Append("\nEnd Posicion de primera linea Parcel: ");
			MsgBox.AppendNumber(YPosicionLastLine);
			MsgBox.Append("\nLeading of lastLine: ");
			MsgBox.AppendNumber(LeadingOfLasParcel);
			MsgBox.Append("\nDepth of Text Parcel: ");
			MsgBox.AppendNumber(estimatedDepth);*/
			
			
		//	CAlert::InformationAlert(MsgBox);
		
		status = kSuccess;

	} while (false);
	return status;
}


PMRect N2PCTUtilities::Get_Coordinates_Reales(const UIDRef& pageItemRef)
{
	PMRect coor;
	do
	{
			
							
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("layout pointer nil");
			break;
		}


		//Obtien la jerarquia de la capa
		InterfacePtr<IHierarchy> layerHier(layout->QueryActiveLayer());
		if (layerHier == nil)
		{
			ASSERT_FAIL("IHierarchy pointer nil");
			break;
		}

		// Get the UIDRef of the layer:
		UIDRef layerRef(::GetUIDRef(layerHier));
								
		// Get the bounding box for the current page
		IDataBase* db1 = layerRef.GetDataBase();
		UIDRef pageUIDRef = UIDRef(db1,layout->GetPage());
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry pointer nil");
			break;
		}


		/////////////////////////////

		//frameList.Append(pageItemRef.GetUID());	
							
		InterfacePtr<IGeometry> geometry(pageItemRef,IID_IGEOMETRY);
		if(geometry==nil)
		{ 
			ASSERT_FAIL("geometry pointer nil");
			break;
		}

		PMMatrix identity;
		PMRect bBoxInner = geometry->GetStrokeBoundingBox(identity);
		
		PBPMRect bBoxPasteboard = geometry->GetStrokeBoundingBox(::InnerToPasteboardMatrix(geometry));
		TransformPasteboardRectToInner(pageGeometry, &bBoxPasteboard);
		coor=bBoxPasteboard;
						
	}while(false);
	return coor;	
}



 /*
 	Obtiene el las cooordenadas reales del frame primario que contiene el textmodel de un elemento 
 	de la nota a partir del Uid del TextFrame.
 	
 */
 bool16 N2PCTUtilities::GETStoryParams_OfUIDTextModel( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumLinesFaltantes,
 												int32& NumLinesRestantes,
 												int32& NumCarFaltantes,
 												int32& NumCarRestantes,
 												int32& NumCarOfTextModel)
 {
 	bool16 retval=kFalse;
 	do
 	{
 			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			//
			UIDRef UIDRefOfElementoNota(db,UIDTextModelofElementoNota);
			
			//Obtiene el Texmodel apartir del numero de UID
			InterfacePtr<ITextModel> mytextmodel(db,UIDTextModelofElementoNota, UseDefaultIID()); 
			if(mytextmodel==nil)
			{
				
				continue;
			}
			
			NumCarOfTextModel=mytextmodel->TotalLength();
			
			InterfacePtr<IFrameList> frameList(mytextmodel->QueryFrameList());
			ASSERT(frameList != nil);
			if (frameList == nil) {
				break;
			}
		
			InterfacePtr<IParcelList> parcelList(frameList, UseDefaultIID());
			ASSERT(parcelList != nil);
			if (parcelList == nil) {
				break;
			}
			InterfacePtr<ITextParcelList> textParcelList(frameList, UseDefaultIID());
			ASSERT(textParcelList != nil);
			if (textParcelList == nil) {
				break;
			}
		
						
			//Obtiene el Iparcel del Texmodel 
			InterfacePtr<IParcel> parcel(this->QueryParcelContaining(mytextmodel,0));
			if (!parcel)
			{
				continue;
			}	
			
			//Obtiene el TextFrame que contiene el caracter numero 1
			InterfacePtr<ITextFrameColumn> FrameContentModel;
			
			FrameContentModel.reset(parcel->QueryFrame());
			if (!FrameContentModel)
			{
				continue;
			}	
			
			//Verifica que este texto sea un texto multicolumna
			if(!Utils<ITextUtils>()->IsMultiColumnFrame(FrameContentModel))
			{
			
				InterfacePtr<ITextFrameColumn> frameConteiner(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel),UseDefaultIID());
				
				if(!Utils<ITextUtils>()->IsMultiColumnFrame(frameConteiner))
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}
	
				//Obtiene el texto Multicolumna que contiene el Texmodel
				InterfacePtr<ITextColumnSizer> textColumnSizerFlow(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel), UseDefaultIID());
				if (textColumnSizerFlow == nil)
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}	
				
				
				//Obtiene el UIDRef del GraphicFrame(Frame Principal del Textstory)
				UIDRef refFrame = GetGraphicFrameRef(frameConteiner, kTrue);
				
				PMReal estimatedDepth=0;
  			    int32 UltimoCaracterMostrado=0;
  			    this->CountWordsAndLines(UIDTextModelofElementoNota, textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumLinesFaltantes, UltimoCaracterMostrado);
				//this->EstimateTextDepth(textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumLinesFaltantes, UltimoCaracterMostrado);
				
				NumCarRestantes = NumCarOfTextModel - UltimoCaracterMostrado;
				//Para sacar segun las coordenadas de la pagina
				boundsInParentCoo=this->Get_Coordinates_Reales(refFrame);
				
				NumLinesRestantes = Utils<ITextUtils>()->CountOversetLines(UIDRefOfElementoNota);
				
				retval=kTrue;
			}	
 	}while(false);
 	return(retval);	
 }
 
 
 
 

 										
 /*
*/
ErrorCode N2PCTUtilities::CountWordsAndLines(UID UIDTextModelofElementoNota, const InterfacePtr<ITextParcelList>& textParcelList, ParcelKey fromParcelKey, 
											int32 numberOfParcels, PMReal& estimatedDepth,
											int32& NumeroDeLineaAntesDelOverset,int32& UltimoCaracterMostrado)
{
	ErrorCode status = kFailure;
	do {
	
		
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("layout pointer nil");
			break;
		}


		//Obtien la jerarquia de la capa
		InterfacePtr<IHierarchy> layerHier(layout->QueryActiveLayer());
		if (layerHier == nil)
		{
			ASSERT_FAIL("IHierarchy pointer nil");
			break;
		}

		// Get the UIDRef of the layer:
		UIDRef layerRef(::GetUIDRef(layerHier));
								
		// Get the bounding box for the current page
		IDataBase* db1 = layerRef.GetDataBase();
		UIDRef pageUIDRef = UIDRef(db1,layout->GetPage());
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry pointer nil");
			break;
		}
		
		////////////////////////////////
	
		// Validate parameters.
		ASSERT(textParcelList != nil);
		if (textParcelList == nil) {
			break;
		}
		
		/*InterfacePtr<ITiler>  myTiler(textParcelList, UseDefaultIID());
		//ASSERT(myTiler != nil);
		if (myTiler == nil) 
		{
			CAlert::InformationAlert("myTiler == nil");
			break;
		}*/
		
		
		
		InterfacePtr<IParcelList> parcelList(textParcelList, UseDefaultIID());
		ASSERT(parcelList != nil);
		if (parcelList == nil) 
		{
			break;
		}
		
		
		
		
		
		
		
		int32 fromParcelIndex = parcelList->GetParcelIndex(fromParcelKey);
		ASSERT(fromParcelIndex >= 0);
		if (fromParcelIndex < 0) {
			break;
		}
		ASSERT(numberOfParcels > 0 && numberOfParcels <= parcelList->GetParcelCount());
		if(numberOfParcels <= 0 || numberOfParcels > parcelList->GetParcelCount()) {
			break;
		}

		// Make sure the text is composed.
		ParcelKey firstDamagedParcelKey = textParcelList->GetFirstDamagedParcel();
		if (firstDamagedParcelKey.IsValid() == kTrue){
			InterfacePtr<ITextParcelListComposer> textParcelListComposer(textParcelList, UseDefaultIID());
			ASSERT(textParcelListComposer);
			if (!textParcelListComposer) {
				break;
			}
			textParcelListComposer->RecomposeThruTextIndex(fromParcelIndex + numberOfParcels - 1);
		}
		
		// Scan the wax and use the position of the first and last line
		// in each parcel that displays text to estimate the overall depth.
		InterfacePtr<IWaxStrand> waxStrand(textParcelList->GetWaxStrandRef(), UseDefaultIID());
		if (waxStrand == nil) {
			break;
		}
		
		
		K2Vector<int32> ParcelKeyToAdd;
		int32 UltimoParcelWithText=-1;
		bool16 isParcelSinTexto=kFalse;
		PMReal YLastLine=0;
		PMReal YHeightLastLine=0;
		PMReal Alto, YSigLine,  YSigHeightLine;
		Alto=0.0;
		PMString kk="";
		for (int32 j = fromParcelIndex; j < fromParcelIndex + numberOfParcels; j++) 
		{
			
			
		 	
    	    
    	    ParcelKey key = parcelList->GetNthParcelKey(j);
    	    PMRect BoundparcelLastParcel=parcelList->GetParcelBounds(key);
    	    
    	    
    	    
    	    //Toma las coordenadas del TextFrame
			TextIndex start = textParcelList->GetTextStart(key);
			int32 span = textParcelList->GetTextSpan(key);
			if (span <= 0) 
			{
				// Some parcels may not be able to display any text.
				// But we can't assume that the story is displayed
				// in its entirety by preceeding parcels. It is possible
				// that a parcel was not big enough to accept text and
				// so text flowed into a subsequent and larger parcel.
				// To handle this continue and consider the depth
				// of other parcels in the list.
				//CAlert::InformationAlert("No se fluyo sobre este span<0");
				ParcelKeyToAdd.push_back(j);
				isParcelSinTexto=kTrue;
				continue;
			}
			else
			{
				UltimoParcelWithText=j;
				//Borra los anteriores pues no se van a contar para estimar 
				//la cantidad de caracteres que falatn para el CopyFit
				if(isParcelSinTexto==kTrue)	
				{
					if(ParcelKeyToAdd.Length()==1)
					{
						ParcelKeyToAdd.pop_back();
					}
					else
					{
						
						if(ParcelKeyToAdd.Length()>1)
						{
							ParcelKeyToAdd.clear();
						}
					}
					isParcelSinTexto=kFalse;
				}
			}
			
			TextIndex end = start + span - 1;
			UltimoCaracterMostrado = end;
			
			K2::scoped_ptr<IWaxIterator> waxIterator(waxStrand->NewWaxIterator());
    	    if (waxIterator == nil) 
    	    {
    	         break;
    	    }
    	    IWaxLine* waxLine = waxIterator->GetFirstWaxLine(start);
    	    
    	    int32   i = 0;
    	    
    	    //SNIPLOG("#, TextOrigin, GetTextSpan, Leading(pts)");
   	     	while (waxLine != nil && waxLine->TextOrigin() < end) 
    	     {
    	     	if(i==0)
    	     	{
    	     		
    	     		YLastLine = waxLine->GetYPosition();
    	     		YHeightLastLine= waxLine->GetLineHeight();
    	     		if( YLastLine > (BoundparcelLastParcel.Top() + YHeightLastLine))
    	     		{
    	     			Alto = Alto + ((-1)*(BoundparcelLastParcel.Top() - YSigLine));
    	     		}
    	     	}
    	     	else
    	     	{
    	     		YSigLine = waxLine->GetYPosition();
    	     		YSigHeightLine= waxLine->GetLineHeight();
    	     		
    	     		if( YSigLine == (YLastLine + YSigHeightLine))
    	     		{
    	     			YLastLine=YSigLine;
    	     			YHeightLastLine=	YSigHeightLine;
    	     			//ok
    	     		}
    	     		else
    	     		{
    	     			
    	     			Alto = Alto + ((-1)*(YLastLine - YSigLine));
    	     			YLastLine=YSigLine;
    	     			YHeightLastLine=	YSigHeightLine;
    	     		}
    	     		
    	     		
    	     	}
    	     	
            	waxLine = waxIterator->GetNextWaxLine();
             	i++;
        	 }
		}
		
		PMReal LastPosicionLineOnParcelReal=0;
		
		
		ParcelKey LastParcelkeyWhithText = parcelList->GetNthParcelKey(UltimoParcelWithText);
    	PMRect BoundparcelLastParcelWithText=parcelList->GetParcelBounds(LastParcelkeyWhithText);
	
		
		
		
		PMReal CantidadDeLineasParaCompletarElUltimoParcelConTexto = 0;
		if(YLastLine>0)
		{
			CantidadDeLineasParaCompletarElUltimoParcelConTexto =  (BoundparcelLastParcelWithText.Height() - ((BoundparcelLastParcelWithText.Height()/2) + YLastLine))/YHeightLastLine;
			LastPosicionLineOnParcelReal=(BoundparcelLastParcelWithText.Height()/2) + YLastLine;
			//::Round(CantidadDeLineasParaCompletarElUltimoParcelConTexto);
		}
		else
		{
			CantidadDeLineasParaCompletarElUltimoParcelConTexto =  ((BoundparcelLastParcelWithText.Height()/2) - YLastLine)/YHeightLastLine;
			LastPosicionLineOnParcelReal=(BoundparcelLastParcelWithText.Height()/2) + YLastLine;
			//::Round(CantidadDeLineasParaCompletarElUltimoParcelConTexto);
		}
		
		
		//Adiciona el numero de Parcel que se enconttro la ultima linea leida
		ParcelKeyToAdd.push_back(UltimoParcelWithText);
		
		//Ciclo para observar si se encuentra en los Parcel faltates y en el ultimo Parcel que se encontro texto,
		//algun Pageitem con TextWrap
		for(int32 zz=0;zz<ParcelKeyToAdd.Length();zz++)
		{
			PMString coordenadasvbv="";
			ParcelKey key = parcelList->GetNthParcelKey(ParcelKeyToAdd[zz]);
			PMRect Boundparcel=parcelList->GetParcelBounds(key);
			
			
			 
			// Se suma la natidad de Lineas que caben en el siguiente Parcel siempre y cuando 
			// sea diferente al numero del  ultimo Parcel que tenia Texto
			if(UltimoParcelWithText!=ParcelKeyToAdd[zz])
				CantidadDeLineasParaCompletarElUltimoParcelConTexto = CantidadDeLineasParaCompletarElUltimoParcelConTexto  +  (Boundparcel.Height()/YHeightLastLine);
			
			
			//
			
			
			
			/////
			InterfacePtr<IGeometry> geometry(db1,parcelList->GetParcelFrameUID(key),IID_IGEOMETRY);
			if(geometry==nil)
			{ 
				
				ASSERT_FAIL("geometry pointer nil");
				break;
			}
			
			
			PMMatrix identity;
			
			PMRect bBoxInner = geometry->GetStrokeBoundingBox(identity);
		
			
			PBPMRect bBoxPasteboard = geometry->GetStrokeBoundingBox(::InnerToPasteboardMatrix(geometry));
			
			
			TransformPasteboardRectToInner(pageGeometry, &bBoxPasteboard);
			
			
			////////////////////
			
			InterfacePtr<ILayoutControlData> layout1(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layout1 == nil)
			{
				ASSERT_FAIL("ILayoutControlData pointer nil");
				break;
			}
			
			InterfacePtr<ISpread> spread(layout1->GetSpreadRef(), UseDefaultIID());
			if(spread==nil)
			{
				break;
			}
			
			// Iterate over all the pages on the spread.
			int32 numberOfPages = spread->GetNumPages();
			for ( int32 i = 0; i < numberOfPages; i++ )
			{
				//contpag++;//contador de paginas
				UIDList pageItemList(db1);
				spread->GetItemsOnPage
				(
					i, 
					&pageItemList, 
					kFalse, // don't include the page object itself
					kFalse // include items that lie on the pasteboard
				);
				
				//UID pagerUID=spread->GetNthPageUID(i);
				//UIDRef pageUIDRef(db1,pagerUID);

				
					
				// Iterate the page items and save off the UIDs of frames.
				int32 pageItemListLength = pageItemList.Length();
				for ( int32 j = 0; j < pageItemListLength; j++ )
				{
					UIDRef pageItemRef = pageItemList.GetRef(j);
					
					
					InterfacePtr<IStandOffData> myStandOffData(pageItemRef, IID_ISTANDOFFDATA);
					if(myStandOffData==nil)
					{
						continue;
					}
					
					IStandOff::mode modoTextWrap = myStandOffData->GetMode();
					if(modoTextWrap==IStandOff::kNone)
					{
						continue;
					}
				
					InterfacePtr<IGraphicFrameData> graphicFrameData(pageItemRef, UseDefaultIID());
					if(graphicFrameData==nil)
					{
						continue;
					}
					else
					{
						if(!graphicFrameData->IsGraphicFrame())
						{
							//UIDTextModelofElementoNota
							UID fromMutiColumnItemUID = graphicFrameData->GetTextContentUID();
							if(fromMutiColumnItemUID==kInvalidUID)
							{
								continue;
							}
							
							InterfacePtr<ITextColumnSizer> fromTextColumnSizer(db1, fromMutiColumnItemUID, UseDefaultIID());
							if(fromTextColumnSizer==nil)
							{
								continue;
							}
							InterfacePtr<ITextFrameColumn> fromTextFrame(fromTextColumnSizer, UseDefaultIID());
							if(fromTextFrame==nil)
							{
								continue;
							}
							
							InterfacePtr<ITextModel> fromTextModel(fromTextFrame->QueryTextModel());
							if(fromTextModel==nil)
							{
								continue;
							}
							
							IFrameList* Listffame = fromTextModel->QueryFrameList();
							UID UIDTextModel = Listffame->GetTextModelUID();
							if(UIDTextModel==UIDTextModelofElementoNota)
								continue;
						}
						
					}
					/*if (Utils<IPageItemTypeUtils>()->IsGraphicFrame(pageItemRef) == kTrue || 
							(graphicFrameData != nil && 
							 graphicFrameData->IsGraphicFrame() == kTrue && 
							 graphicFrameData->HasContent() != kTrue))
					{*/
						
						InterfacePtr<IGeometry> geometryPageItem(pageItemRef , IID_IGEOMETRY);
						if(geometryPageItem==nil)
						{ 
							ASSERT_FAIL("geometry pointer nil");
							break;
						}
						
						//Obtiene la geometria Real del GraphicFrame
						PMMatrix identityPageItem;
						PMRect bBoxInnerPageItem = geometry->GetStrokeBoundingBox(identityPageItem);
						PBPMRect bBoxPasteboardPageItem = geometryPageItem->GetStrokeBoundingBox(::InnerToPasteboardMatrix(geometryPageItem));
						TransformPasteboardRectToInner(pageGeometry, &bBoxPasteboardPageItem);
						
							//Si se existe interseccion entre los dos GraficsFrames
						if(Intersect(bBoxPasteboard, bBoxPasteboardPageItem))
						{
												
							
							if( (UltimoParcelWithText==ParcelKeyToAdd[zz]) && ((bBoxPasteboard.Top() + LastPosicionLineOnParcelReal) < bBoxPasteboardPageItem.Top())  )
							{
								PMRect IntersectPMRect = Intersection(bBoxPasteboard, bBoxPasteboardPageItem);
						
								//si el largo de la interseccion es igual al largo del Parcel
					
								if(IntersectPMRect.Width()==bBoxPasteboard.Width() || ( (IntersectPMRect.Width()-bBoxPasteboard.Width()) < 12))
								{
									//Entonces ocupa varias lineas completas
									//Se restan las lineas que ocupa el graphicframe sobre el actual Parcel
									CantidadDeLineasParaCompletarElUltimoParcelConTexto=CantidadDeLineasParaCompletarElUltimoParcelConTexto - (IntersectPMRect.Height()/YHeightLastLine);
									
								}
								else
								{
									//Si no, Entonces parte de algunas lineas no estaran completas
									//Se debe de determinar cuantos caracteres caben en esa parte de linea sobrante
								}
							}
							else
							{
								if(UltimoParcelWithText!=ParcelKeyToAdd[zz])
								{
									PMRect IntersectPMRect = Intersection(bBoxPasteboard, bBoxPasteboardPageItem);
						
									//si el largo de la interseccion es igual al largo del Parcel
					
									if(IntersectPMRect.Width()==bBoxPasteboard.Width() || ( (IntersectPMRect.Width()-bBoxPasteboard.Width()) < 12))
									{
										//Entonces ocupa varias lineas completas
										//Se restan las lineas que ocupa el graphicframe sobre el actual Parcel
										CantidadDeLineasParaCompletarElUltimoParcelConTexto=CantidadDeLineasParaCompletarElUltimoParcelConTexto - (IntersectPMRect.Height()/YHeightLastLine);
										
									}
									else
									{
										//Si no, Entonces parte de algunas lineas no estaran completas
											//Se debe de determinar cuantos caracteres caben en esa parte de linea sobrante
									}
								}
								
							}
							
						}
					//}
				}
			} 
		}
		
		//Verificar si  despues de la ultima linea observada
		PMString nn="";
		/*nn.Append("UltimoParcelWithText= ");
		nn.AppendNumber(UltimoParcelWithText);
		
		nn.Append("\n BoundparcelLastParcelWithText= ");
		nn.AppendNumber(BoundparcelLastParcelWithText.Height());
		nn.Append("\n YLastLine= ");
    	nn.AppendNumber(YLastLine);
    	
    	nn.Append("\n YHeightLastLine= ");
    	nn.AppendNumber(YHeightLastLine);*/
		
		nn.AppendNumber(CantidadDeLineasParaCompletarElUltimoParcelConTexto);
		NumeroDeLineaAntesDelOverset = nn.GetAsNumber(); 
		//CAlert::InformationAlert(nn);
		
		status = kSuccess;

	} while (false);
	return status;
}
