//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/basicdragdrop/N2PSQLCustomFlavorHelper.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #6 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

//Interface includes:
#include "CDragDropTargetFlavorHelper.h"
#include "IControlView.h"
#include "ILayoutUIUtils.h"
#include "ILayoutControlData.h"
#include "ICommand.h"
#include "IDataExchangeHandler.h"
#include "IGeometry.h"
#include "IPageItemLayerData.h"
#include "IPageItemScrapUtils.h"
#include "IPasteboardUtils.h"
#include "IDocument.h"
#include "IPageItemScrapData.h"
#include "ISpread.h"
#include "ILayoutCmdData.h"
#include "ISysFileListData.h"
#include "IPlaceGun.h"
#include "IPlaceBehavior.h"
#include "ILayoutControlViewHelper.h"
#include "IUIDData.h"
#include "IHierarchy.h"
#include "IPageItemTypeUtils.h"
#include "IGraphicFrameData.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IListBoxController.h"
#include "ILayoutUIUtils.h"
#include "ILayoutUIUtils.h"

// General includes:
#include "PersistUtils.h"
#include "ErrorUtils.h"
#include "Utils.h"
#include "DataObjectIterator.h"
#include "CmdUtils.h"
#include "LayerID.h"
#include "CAlert.h"
#include "SDKFileHelper.h"
#include "IPalettePanelUtils.h"

// Project includes:
#include "N2PSQLID.h"
#include "N2PSQLUtilities.h"
#include "N2PSQLPhotoListBoxHelper.h"
#include "UpdateStorysAndDBUtilis.h"

#ifdef WINDOWS
	#include "..\N2PCheckInOut\N2PCheckInOutID.h"

	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	
	#include "..\N2PAdornmentIssue\IN2PAdIssueUtils.h"
	#include "..\N2PAdornmentIssue\N2PAdIssueID.h"
#endif

#ifdef MACINTOSH
	#include "N2PCheckInOutID.h"

	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	
	#include "IN2PAdIssueUtils.h"
	#include "N2PAdIssueID.h"
#endif





/** Extends the DND target capabilities of the layout widget to
	accept our custom flavor object.

	@ingroup basicdragdrop
	
*/
class N2PSQLCustomFlavorHelper : public CDragDropTargetFlavorHelper
{
public:
	/**
		Constructor.
		@param boss IN the boss class the interface is aggregated onto.
	*/
	N2PSQLCustomFlavorHelper(IPMUnknown* boss);
	/**
		Destructor.
	*/
	virtual	~N2PSQLCustomFlavorHelper();

	/**
		Determines whether we can handle the flavors in the drag.
		@param target IN the target the mouse is currently over.
		@param dataIter IN iterator providing access to the data objects within the drag.
		@param fromSource IN the source of the drag.
		@param controller IN the drag drop controller mediating the drag.
		@return a target response (either won't accept or drop will copy).

		@see DragDrop::TargetResponse
	*/
	virtual DragDrop::TargetResponse	
						CouldAcceptTypes(const IDragDropTarget* target, DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController* controller) const;

	/**
		performs the actual drag. Because we know that our custom flavor is masquerading and is 
		really a page item we must take a copy of the page item, then move this copy to the drop zone.
		We know we have been dropped on a widget (because we are called as part of that widget responding to
		a drop of the custom flavor).
		@param target IN the target for this drop.
		@param controller IN the drag drop controller that is mediating the drag.
		@param action IN what the drop means (i.e. copy, move etc)
		@return kSuccess if the drop is executed without error, kFailure, otherwise.
	*/
	virtual ErrorCode		ProcessDragDropCommand(IDragDropTarget*, IDragDropController*, DragDrop::eCommandType);

	DECLARE_HELPER_METHODS()
};


//--------------------------------------------------------------------------------------
// Class LayoutDDTargetFileFlavorHelper
//--------------------------------------------------------------------------------------
CREATE_PMINTERFACE(N2PSQLCustomFlavorHelper, kN2PSQLCustomFlavorHelperImpl)
DEFINE_HELPER_METHODS(N2PSQLCustomFlavorHelper)

N2PSQLCustomFlavorHelper::N2PSQLCustomFlavorHelper(IPMUnknown* boss) :
	CDragDropTargetFlavorHelper(boss), HELPER_METHODS_INIT(boss)
{
}

N2PSQLCustomFlavorHelper::~N2PSQLCustomFlavorHelper()
{
}

/* Determine if we can handle the drop type */
DragDrop::TargetResponse 
N2PSQLCustomFlavorHelper::CouldAcceptTypes(const IDragDropTarget* target, DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController* controller) const
{
	
	// Check the available external flavors to see if we can handle any of them
	if (dataIter != nil)
	{
		// Test for swatches in the drag
		DataExchangeResponse response;
		
		response = dataIter->FlavorExistsWithPriorityInAllObjects(N2PSQLCustomFlavorFile);
		if (response.CanDo())
			return DragDrop::TargetResponse(response, DragDrop::kDropWillCopy);
		
		
	}
	
	return DragDrop::kWontAcceptTargetResponse;
}

/* process the drop, this method called if CouldAcceptTypes returns valid response */
ErrorCode	
N2PSQLCustomFlavorHelper::ProcessDragDropCommand
	(
	IDragDropTarget* 			target, 
	IDragDropController* 		controller,
	DragDrop::eCommandType		action
	)
{	

	
	ErrorCode stat = kFailure;
	IAbortableCmdSeq* sequence = CmdUtils::BeginAbortableCmdSeq();
	if (sequence == nil)
	{
		ASSERT_FAIL("Cannot create command sequence?");
		return stat;
	}
	sequence->SetName(PMString("ProcessDragDropCommand", PMString::kDontTranslateDuringCall));

	do
	{
		if (action != DragDrop::kDropCommand) 
		{
			ASSERT_FAIL("CProcessDragDropCommand");			
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			ASSERT_FAIL("document");	
			break;
		}
		//	CAlert::InformationAlert("X009");
		// Find the target view for the drop and the mouse position.
		InterfacePtr<IControlView> layoutView(target, UseDefaultIID());
		ASSERT(layoutView);
		if (!layoutView) 
		{
			ASSERT_FAIL("layoutView");	
			break;
		}
		InterfacePtr<ILayoutControlData> layoutData(target, UseDefaultIID());
		ASSERT(layoutData);
		if (!layoutData) 
		{
			ASSERT_FAIL("layoutData");	
			break;
		}
		GSysPoint where = controller->GetDragMouseLocation();
		PMPoint currentPoint = Utils<ILayoutUIUtils>()->GlobalToPasteboard(layoutView, where);

		// Determine the target spread to drop the items into.
		InterfacePtr<ISpread> targetSpread(Utils<IPasteboardUtils>()->QueryNearestSpread(layoutView, currentPoint));
		ASSERT(targetSpread);
		if (!targetSpread) 
		{
			ASSERT_FAIL("targetSpread");	
			break;
		}
		UIDRef targetSpreadUIDRef = ::GetUIDRef(targetSpread);

		// Change spread if necessary.
		if (targetSpreadUIDRef != layoutData->GetSpreadRef()) {
			InterfacePtr<ICommand> setSpreadCmd(CmdUtils::CreateCommand(kSetSpreadCmdBoss));
			ASSERT(setSpreadCmd);
			if (!setSpreadCmd) 
			{
				ASSERT_FAIL("setSpreadCmd");
				break;
			}
			InterfacePtr<ILayoutCmdData> setSpreadLayoutCmdData(setSpreadCmd, UseDefaultIID());
			ASSERT(setSpreadLayoutCmdData);
			if (!setSpreadLayoutCmdData) 
			{
				ASSERT_FAIL("setSpreadLayoutCmdData");
				break;
			}
			setSpreadLayoutCmdData->Set(::GetUIDRef(layoutData->GetDocument()), layoutData);
			setSpreadCmd->SetItemList(UIDList(targetSpreadUIDRef));
			 CmdUtils::ProcessCommand(setSpreadCmd);
			//ASSERT_MSG(stat == kSuccess, "Failed to change spread");
		}

		// Get a list of the page items in the drag.
		ErrorCode Dstat = controller->InternalizeDrag(kNoExternalFlavor, N2PSQLCustomFlavorFile);
		ASSERT_MSG(Dstat == kSuccess, "Cannot internalize the dragged item?\n");
		if (Dstat != kSuccess) 
		{
			ASSERT_FAIL("InternalizeDrag");
			break;
		}
		InterfacePtr<IDataExchangeHandler> handler(controller->QueryTargetHandler());
		ASSERT(handler);
		if (!handler) 
		{
			ASSERT_FAIL("handler");
			//stat = kFailure;
			break;
		}
		
		InterfacePtr<ISysFileListData> 	sysFileListData(handler, IID_ISYSFILELISTDATA);
		ASSERT(sysFileListData);
		if (!sysFileListData)
		{
			ASSERT_FAIL("sysFileListData");
			break;
		}
		
		IDFile xFile;
		for(int32 numFile=0; numFile<sysFileListData->Length();numFile++)
		{
			xFile = sysFileListData->GetSysFileItem(numFile);
		}
		
		
		SDKFileHelper fileHelper(xFile);
		ASSERT(fileHelper.IsExisting());
		if(!fileHelper.IsExisting())
		{
			ASSERT_FAIL("fileHelper");
			break;
		}
		
		IDataBase* database=::GetDataBase(targetSpread);
		UIDRef docUIDRef(database, database->GetRootUID());
		
		//CAlert::InformationAlert("X09");

		UIDRef loadedImage = N2PSQLUtilities::ImportImageAndLoadPlaceGun(	docUIDRef , xFile);
		if (loadedImage == UIDRef::gNull)
		{
			ASSERT_FAIL("loadedImage == UIDRef::gNull");
			break;
		}
		
		UIDRef parent(targetSpreadUIDRef.GetDataBase(), layoutData->GetActiveLayerUID());
		
		PMPointList points;
		points.push_back(currentPoint);
		points.push_back(currentPoint);
		
		InterfacePtr<IPlaceGun> placeGun(docUIDRef, UseDefaultIID());
		ASSERT(placeGun);
		if(!placeGun)
		{
			ASSERT_FAIL("placeGun");
			break;
		}
		
		if(placeGun->CountPlaceGunUIDs()<=0)
		{
			ASSERT_FAIL("CountPlaceGunUIDs");
			break;
		}
		
		 InterfacePtr<IPlaceBehavior> sourceBehavior(docUIDRef.GetDataBase(), 
		         placeGun->GetFirstPlaceGunItemUID(), 
                 UseDefaultIID());
         ASSERT(sourceBehavior);
         if(!sourceBehavior) 
         {
			ASSERT_FAIL("sourceBehavior");
             break;
         }
         ICursorMgr::eCursorModifierState cursorModState = (ICursorMgr::eCursorModifierState) ICursorMgr::kNoModifiers;
 
         InterfacePtr<ILayoutControlViewHelper> layoutControlViewHelper(layoutView, UseDefaultIID());// IID_ILAYOUTCONTROLVIEWHELPER);
         ASSERT(layoutControlViewHelper);
         if(!layoutControlViewHelper)
         {
			ASSERT_FAIL("layoutControlViewHelper");
            break;
         }
			
		
		
         InterfacePtr<IPlaceBehavior> targetBehavior(static_cast<IPlaceBehavior*>(layoutControlViewHelper->
                         QueryHitTestPageItemNew(currentPoint, kSolidTOPMPHitTestHandlerBoss, IID_IPLACEBEHAVIOR)));
         if(targetBehavior==nil)
		{
			break;
		}
		
         UIDList mylistCS5 = sourceBehavior->ProcessPlace(targetBehavior, parent, points, cursorModState, cursorModState, targetSpread); 
         UIDRef placedItem = UIDRef(database, mylistCS5[0]); 
         if (placedItem.GetUID() != kInvalidUID)
         {
			
             InterfacePtr<ICommand> clearPlaceGun(CmdUtils::CreateCommand(kClearPlaceGunCmdBoss));
             ASSERT(clearPlaceGun);
             InterfacePtr<IUIDData> docUIDData(clearPlaceGun, UseDefaultIID());
            ASSERT(docUIDData);
             if(!docUIDData) 
			 {
                 break;
             }
             docUIDData->Set(docUIDRef);
             
             if(CmdUtils::ProcessCommand(clearPlaceGun)!=kSuccess)
             {
             	break;
             }
         }
					
		
		
		
		UID UIdParentOfItemPlaced=placedItem.GetUID();
		
		
		//Obtiene la interface IHierarchy para obtener el padre de pageitem que fue colocado
		InterfacePtr<IHierarchy> hierarchyOfPlacedItem(placedItem,  UseDefaultIID());
		ASSERT(hierarchyOfPlacedItem);
		if(!hierarchyOfPlacedItem)
		{	
			ASSERT_FAIL(" N2PSQLCustomFlavorHelper::ProcessDragDropCommand  hierarchyOfPlacedItem");
			break;
		}
		
		//Interfaz para cambiar el adornament(adorno) que muestra el tipo Estado del elemento de la nota
		InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
		(
			kN2PAdIssueUtilsBoss,	// Object boss/class
			IN2PAdIssueUtils::kDefaultIID
		)));
		
		if(N2PAdIssueUtil==nil)
		{
			break;
		}
			
		//Obtiene las prefercnais del Servidor local
		PreferencesConnection PrefConections;
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));

		//CAlert::InformationAlert("X10");

		if(UpdateStorys==nil)
		{
			ASSERT_FAIL(" N2PSQLCustomFlavorHelper::ProcessDragDropCommand  UpdateStorys");
			break;
		}
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			ASSERT_FAIL(" N2PSQLCustomFlavorHelper::ProcessDragDropCommand  GetPreferencesConnectionForNameConection");
			break;
		}
		
		//Obtiene el nombre de la aplicacion en la que estamos trabajando y el nombre o Id_ del usuari que se logeo
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL(" N2PSQLCustomFlavorHelper::ProcessDragDropCommand  wrkSpcPrefs");
			break;
		}		
		
			
		PMString ID_Usuario = wrkSpcPrefs->GetIDUserLogedString();
		PMString Applicacion= wrkSpcPrefs->GetNameApp() ;
		if(Applicacion.NumUTF16TextChars()<1)
		{
			break;
		}
			
		//Obtiene el UID del padre del pageitem que fue colocado
		for(int32 i=0;i<hierarchyOfPlacedItem->GetChildCount();i++)
		{
			//UIdParentOfItemPlaced = ;
			UIDRef ChildOfPlacetItem(placedItem.GetDataBase(), hierarchyOfPlacedItem->GetChildUID(i));
			
			//Comando paa el Autofit de la pagina al frame 
			// Create a FitContentToFrameCmd:
			InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kFitContentToFrameCmdBoss));
			if(fitCmd==nil)
			{
				break;
			}
			// Set the FitContentToFrameCmd's ItemList:
			fitCmd->SetItemList(UIDList(ChildOfPlacetItem));
			// Process the FitContentToFrameCmd:
			
			// stat = CmdUtils::ProcessCommand(fitCmd);
       		 //ASSERT(stat == kSuccess);	
   			
   			InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPhotoPanelWidgetID)) ;
			if(panel==nil)
			{
				break;
			}

			InterfacePtr<IControlView>		iControViewList( panel->FindWidget(kN2PsqlPhotoListBoxWidgetID), UseDefaultIID() );
			if(iControViewList==nil)
			{
				break;
			}
			
			N2PSQLPhotoListBoxHelper N2PSQLPhotoListHelper(iControViewList, kN2PSQLPluginID,kN2PsqlPhotoListBoxWidgetID, kN2PsqlPhotoPanelWidgetID);
			
			InterfacePtr<IListBoxController> iListController(iControViewList,IID_ILISTBOXCONTROLLER); // kDefaultIID not def'd
			ASSERT_MSG(iListController != nil, "listCntl nil");
			if(iListController == nil) 
			{
				break;
			}
			
			PMString UIDOfFramePlaced="";
			UIDOfFramePlaced.AppendNumber(UIdParentOfItemPlaced.Get());
			PMString IdElePhoto="";
			PMString IdPadreElePhoto="";
			PMString StatusPhoto="";
			PMString FechaUltMod="";
			
			PMString DSNName="Default";
			PMString ProvieneDServidorLocal="0";
			PMString N2PSQLIDPag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);	
			/*PMString DSNName= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID); //DSN Actual
			PMString N2PSQLIDPag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);							//Folio de  pagina donde fue drageada la foto
			
			//Para verificvar si proviene de un servidor local
			PMString ProvieneDServidorLocal="";
			if(PrefConections.NameConnection==DSNName)
				ProvieneDServidorLocal="0"; //indicamos que esta nota proviene del servidor local
			else
			{
				//Obtiene las preferencias para la conexion al servidor remoto
				if(!UpdateStorys->GetPreferencesConnectionForNameConection(DSNName,PrefConections))
				{
					ASSERT_FAIL(" N2PSQLCustomFlavorHelper::ProcessDragDropCommand  vGetPreferencesConnectionForNameConection 2");	
					break;
				}
						
		
				ProvieneDServidorLocal="1"; //indicamos que esta nota proviene de otro servid
			}
				*/
						
			K2Vector<int32> multipleSelection ;
			iListController->GetSelected( multipleSelection ) ;
			const int kSelectionLength =  multipleSelection.Length() ;
			if (kSelectionLength> 0 )
			{
				
				int indexSelected = multipleSelection[0];
				N2PSQLPhotoListHelper.ObtenerTextoDeSeleccionActual(kN2PsqlIdElePhotoLabelWidgetID,IdElePhoto, indexSelected);
				N2PSQLPhotoListHelper.ObtenerTextoDeSeleccionActual(kN2PsqlIdPadreElePhotoLabelWidgetID,IdPadreElePhoto, indexSelected);
				N2PSQLPhotoListHelper.ObtenerTextoDeSeleccionActual(kN2PsqlStatusPhotoLabelWidgetID,StatusPhoto, indexSelected);
				N2PSQLPhotoListHelper.ObtenerTextoDeSeleccionActual(kN2PsqlFechaUltModPhotoLabelWidgetID,FechaUltMod, indexSelected);
				
				//CAlert::InformationAlert("X20");
				PMString CmdStoreProcString = "CALL FOTO_COLOCA_Y_LIGA_A_PAGINA (" + 
											IdElePhoto + " , " + N2PSQLIDPag +  " , " + ProvieneDServidorLocal + " , '" + ID_Usuario + "' ,  @A , @X)"; //ELECT @OUTString AS Id_Elemento ,@NuveNotaValores AS Fecha_ult_act"; 
				
				
				PMString StringConection="";
		
				StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
					(
						kN2PSQLUtilsBoss,	// Object boss/class
						IN2PSQLUtils::kDefaultIID
					)));
				if(SQLInterface==nil)
				{
					break;
				}
			
				K2Vector<PMString> QueryVector;
				
				//CAlert::InformationAlert(CmdStoreProcString);
				if(!SQLInterface->SQLQueryDataBase(StringConection,CmdStoreProcString,QueryVector))
				{
					ASSERT_FAIL(" N2PSQLCustomFlavorHelper::ProcessDragDropCommand  SQLQueryDataBase 2");	
					stat = kFailure;
					break;
				}
				
				PMString GuiaNota="";
				//Solo trae nuevos datos cuando se hizo una copia de esta foto	
				if(QueryVector.Length()>0)
				{
					
					
					for(int32 j=0;j<QueryVector.Length();j++)
					{
						GuiaNota = SQLInterface->ReturnItemContentbyNameColumn("GuiaNota",QueryVector[j]);
					}
				}
				else
					break;
				
		
				if(N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP("N2P_ListaUpdate",IdPadreElePhoto, IdElePhoto,
												 UIDOfFramePlaced, "Foto",
												 DSNName, StatusPhoto, kFalse,
												 FechaUltMod,kTrue) ==kTrue)
				{
				
					//Coloca las dimenciones del frame en donde fue colocado la foto
					
					//Muetra icono de que ya ha sido colocada la foto
					N2PSQLPhotoListHelper.Mostrar_IconFotoColocada(indexSelected, kN2PsqlCmpListElementPenWidgetID,kTrue);
					
					UIDList ListFrame=UIDList(placedItem);
					N2PAdIssueUtil->UpdateUIDListLabelfor( ListFrame,GuiaNota, kTrue);
						
					stat = kSuccess;
             		ASSERT(stat == kSuccess);
				}
				else
				{
					PMString FotoYaColocada=kFotoYaFueColocadaStringKey;
					FotoYaColocada.Translate();
					FotoYaColocada.SetTranslatable(kTrue);
					CAlert::InformationAlert(FotoYaColocada);	
					stat = kFailure;
				}       
				
			}
		}
		
			
	} while(false);

	if (stat == kSuccess) 
	{
		
		// Everything completed so end the command sequence
		CmdUtils::EndCommandSequence(sequence);
		
		
	} else
	{
		
		// Abort the sequence and roll back the changes.
		CmdUtils::AbortCommandSequence(sequence);
	
	}
	
	return stat;
}

// End, N2PSQLCustomFlavorHelper.cpp.

