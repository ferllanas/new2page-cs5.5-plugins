/*
 *  N2PWSDLCltUtils.cpp
 *  N2PWSDLClient
 *
 *  Created by ADMINISTRADOR on 25/04/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "N2PwsdlcltID.h"
#include "N2PWSDLCltUtils.h"

#include "CAlert.h"
//#include "webservices Normal/N2PLocalwsdlBinding.nsmap"
//#include "webservices Normal/soapN2PLocalwsdlBindingProxy.h"

#include "webservices/N2PLocalwsdlBinding.nsmap"
#include "webservices/soapN2PLocalwsdlBindingProxy.h"

#include "WPN2PWSDLCltUtils.h"
#include "WPN2PID.h"


class N2PWSDLCltUtils : public CPMUnknown<IN2PWSDLCltUtils>
{
public:
	/** Constructor.
	 param boss boss object on which this interface is aggregated.
	 */
	N2PWSDLCltUtils (IPMUnknown *boss);
	
	const bool16 realizaConneccion(PMString usuario,
							 PMString ip, 
							 PMString cliente,
							 PMString aplicacion, 
							 PMString URL);
	const bool16 realizaDesconeccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL);
	
	const K2Vector<PMString> MySQLConsulta(PMString query,PMString URL);

	const bool16 sendPost(PMString URL, PMString publicacion, PMString idsNotas);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
 */
CREATE_PMINTERFACE(N2PWSDLCltUtils, kN2PwsdlcltUtilsImpl)


/* HelloWorld Constructor
 */
N2PWSDLCltUtils::N2PWSDLCltUtils(IPMUnknown* boss) 
: CPMUnknown<IN2PWSDLCltUtils>(boss)
{
	// do nothing.
	
	/* NOTE: 
	 There used to be code that would set the preference value by default.
	 That was removed in favor of the workspace responders.
	 
	 The responders get called, and they will try to setup the value
	 for this custom preference.  It is one of the responders that will
	 set the correct initial value for the cached preference, fPrefState.
	 
	 If the global workspace is already created from a previous 
	 application session, the ReadWrite() method will set the 
	 cached preference, fPrefState. The same ReadWrite() will be called when 
	 an existing document is opened.
	 
	 Refer to CstPrfNewWSResponder.cpp and N2PSQLUtils.cpp
	 for details on how to set the workspace defaults. 
	 */
}



const bool16 N2PWSDLCltUtils::realizaConneccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL)
{
	bool16 retval=kFalse;
	ns1__STConnection STConnectionZZ;
	
	ns1__ValidaConnectionResponse param_1;
	
	STConnectionZZ.username = usuario.GrabCString();
	STConnectionZZ.ip = ip.GrabCString();
	STConnectionZZ.cliente = cliente.GrabCString();
	STConnectionZZ.aplicacion = aplicacion.GrabCString();
	
	
	N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	N2PLocal.soap_endpoint = URL.GrabCString();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	N2PLocal.ValidaConnection(&STConnectionZZ,param_1);
	if(N2PLocal.error)
	{
		N2PLocal.soap_stream_fault(std::cerr);
		CAlert::InformationAlert(N2PLocal.soap_fault_string());
		
	}
	else
	{
		PMString  XCS="";
		XCS.Append(param_1.return_->errorstring.c_str());//;

		if(XCS=="OK")
		{
			retval=kTrue;
		
		}
		else
			CAlert::InformationAlert(XCS);
	}
	
	return retval;
}

const bool16 N2PWSDLCltUtils::realizaDesconeccion(PMString usuario,PMString ip, PMString cliente,PMString aplicacion,PMString URL)
{
	bool16 retval=kFalse;
	ns1__STConnection STConnectionZZ;
	ns1__disconnectResponse param_1;
	
	STConnectionZZ.username = usuario.GrabCString();
	STConnectionZZ.ip = ip.GrabCString();
	STConnectionZZ.cliente = cliente.GrabCString();
	STConnectionZZ.aplicacion = aplicacion.GrabCString();
	
	
	N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	N2PLocal.soap_endpoint = URL.GrabCString();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	N2PLocal.disconnect(&STConnectionZZ, param_1);
	//.ValidaConnection(&STConnectionZZ,param_1);
	if(N2PLocal.error)
	{
		N2PLocal.soap_stream_fault(std::cerr);
		CAlert::InformationAlert(N2PLocal.soap_fault_string());
		//CAlert::InformationAlert(N2PLocal.soap_fault_detail());
	}
	else
	{
		PMString  XCS="";
		XCS.Append(param_1.return_->errorstring.c_str());//;
		//CAlert::InformationAlert(XCS);
		
		if(XCS=="OK")
		{
			retval=kTrue;
			
		}
		else
			CAlert::InformationAlert(XCS);
	}
	return retval;
}



const K2Vector<PMString> N2PWSDLCltUtils::MySQLConsulta(PMString query,PMString URL)
{
	
	
	K2Vector<PMString> retval;
	std::string consulta= query.GrabCString();
	ns1__EjecutaQueryResponse param_1;
	
	
	
	N2PLocalwsdlBindingProxy N2PLocal(SOAP_C_UTFSTRING);
	N2PLocal.soap_endpoint = URL.GrabCString();//"http://www.publish88.com/p88appleiPad/p88Serverwsdl.php";
	N2PLocal.EjecutaQuery( consulta , param_1);
	if(N2PLocal.error)
	{
		N2PLocal.soap_stream_fault(std::cerr);
		CAlert::InformationAlert(N2PLocal.soap_fault_string());
	}
	else
	{
		CAlert::InformationAlert("A");
		int32 cantidad1 = param_1.return_->__size;
		CAlert::InformationAlert("B");
		int32 cantidad2 = param_1.return_[0].__ptr[0]->__size;
		PMString cantidad3= param_1.return_[0].__ptr[0]->__ptr[0]->nomcampo.c_str();
		
		PMString SD="";
		SD.AppendNumber(cantidad1);
		SD.Append(",");
		SD.AppendNumber(cantidad2);
		SD.Append(",");
		SD.Append(cantidad3);
		CAlert::InformationAlert(query+","+SD);
		//ns1__campo* mycampos= param_1.return_[0].__ptr[0]->__ptr[0];
		CAlert::InformationAlert("C");
		//std::string nomcampo=mycampos->nomcampo;
		CAlert::InformationAlert("D");
		//for(int32 i=0;i<mycampos; i++)
		//std::string mystring = param_1.return_[0].__ptr[0]->__ptr[0]->nomcampo;
		
		//int32 cd=mystring.length();
		PMString XSWQ("Prueba regreso algo");//="";//mystring;
		CAlert::InformationAlert(XSWQ);
		//XSWQ.insert(mystring);
		if(param_1.return_->__size> 0)
		{
			for(int32 i=0; i <param_1.return_->__size; i++)
			{
				for(int32 j=0; j< param_1.return_[i].__size ; j++)
				{
					for(int32 k=0;k< param_1.return_[i].__ptr[j]->__size ; k++)
					{
						//int32 bb=param_1.return_[i].__ptr[j]->__ptr[k]->__size;
						PMString Campo = param_1.return_[i].__ptr[j]->__ptr[k]->nomcampo.c_str();
						PMString Valor = param_1.return_[i].__ptr[j]->__ptr[k]->valor.c_str();
						
						CAlert::InformationAlert(Campo+":"+Valor);
					}
				}
			}	
		}
		//if(param_1.return_.size()> 0)
		//if(param_1.return_.size> 0)
		//if(param_1.return_.size> 0)
		//if(param_1.return_->size()> 0)
		
	}
	
	return retval;
}
 


const bool16 N2PWSDLCltUtils::sendPost(PMString URL, PMString publicacion, PMString idsNotas)
{
	bool16 retval=kFalse;
	
	do{
	
		InterfacePtr<IWPN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IWPN2PWSDLCltUtils*> (CreateObject
																					  (
																					   kWPN2PwsdlcltUtilsBoss,	// Object boss/class
																					   IWPN2PWSDLCltUtils::kDefaultIID
																					   )));
	
		if(!	N2PWSDLCltUtil)
		{
			CAlert::InformationAlert("salio");
			break;
		}
		PMString ResultString="OK";
		//URL="http://localhost:8888/PilotoWpN2P/webservices/soapws";
		
		retval=N2PWSDLCltUtil->sendPost(URL, publicacion, idsNotas );//realizaConneccion("ferllanas",
		
		//			 "192.6.2.2", 
		//			 "",
		//			 "InDesign", 
		//			 "http://localhost/N2PWebservices/N2PServerwsdl.php");
		
		/*if(!retval)
		{
		
			CAlert::InformationAlert(ResultString);
			//retval=kFalse;
		
		}*/
	}while(false);
	
	return retval;
}
 
