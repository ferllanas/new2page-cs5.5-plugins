//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/cellpanel/CellTable.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #2 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __CELLTABLE_H__
#define __CELLTABLE_H__

#include "PMString.h"

/**
	CellData stores the string data for each cell on the table.	
	
	@ingroup cellpanel
	
*/
class CellData {
	public:
		/**
			Constructor.
		*/
		CellData();
		/**
			Destructor.
		*/
		~CellData();
	
		/** Set a string in cell.
			@param str A string data to set up.
		*/
		bool				SetString(const PMString& str);
		/** Getting a string data from a cell.
		*/
		const PMString&		GetString(void);
		
	protected:
	private:
		PMString		string;
};


/** CellTable
	This is a table of cell data class.
	The table has many cell datas, And two access function.
	
	@ingroup cellpanel
	
*/
class CellTable {
	public:
		/**
			Constructor.
			@param numColumn The number of columns for the new table.
			@param numRow The number of rows for the new table.
		*/
		CellTable(int32 numColumn, int32 numRow);
		
		/**
			Destructor.
		*/
		~CellTable();
		
		/** Set a string in cell of the table.
			@param str PMString class data.
			@param column The column number of the cell to set.
			@@param row The row number of the cell to set.
		*/
		bool			SetString(const PMString& str, int32 column, int32 row);
		
		/** Getting a string data from a cell.
			@param column The number of column for getting a string data from a cell.
			@param row The number of row for getting a string data from a cell.
		*/
		const PMString&	GetString(int32 column, int32 row);
				
	private:
	int32 maxColumn, maxRow;
	CellData* table;
		
};

#endif __CELLTABLE_H__

//End, CellTable.h
