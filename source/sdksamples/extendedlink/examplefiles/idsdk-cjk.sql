-- MySQL dump 10.11
--
-- Host: localhost    Database: idsdk-cjk
-- ------------------------------------------------------
-- Server version	5.0.67

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `idsdk-cjk`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `idsdk-cjk` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `idsdk-cjk`;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `inventory` (
  `sku` char(6) NOT NULL,
  `name` varchar(50) default NULL,
  `price` double default NULL,
  `quantity` int(11) default NULL,
  `image` varchar(255) default NULL,
  `description` varchar(1023) default NULL,
  `version` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
INSERT INTO `inventory` VALUES ('AB-307','è¿·ä½ è‡ªåŠ¨å¼€å…³é›¨ä¼ž',13.75,101,'MiniAutoUmbrellaAB-307.gif','æ—¶å°š43åº¦å¼§é¢è¿·ä½ è‡ªåŠ¨å¼€å…³é›¨ä¼žã€‚çº¢æœ¨ä¼žæŸ„ã€‚é»‘è‰²ç»¸å¸ƒä¼žé¢ã€‚æŠ˜å åŽé•¿åº¦12è‹±å¯¸ã€‚ä¼žé¢ä¸Šå°æœ‰ä¸¤ç§é¢œè‰²æž„æˆçš„Adobeå¾½æ ‡','2009-03-27 03:39:44'),('AB-220','ä¸–ç•Œæ—…è¡Œã‚¢ãƒ€ãƒ—ã‚¿ãƒ¼',24,48,'WorldTravelAdapterAB-220.gif','ã“ã®ã‚¹ã‚¤ã‚¹ã®ãƒ‡ã‚£ã‚µã‚¤ãƒ³ã•ã‚Œã¦ã„ãŸä¸–ç•Œæ—…è¡Œã‚¢ãƒ€ãƒ—ã‚¿ãƒ¼ã¯150ãƒ¶ä»¥ä¸Šã®å›½ã«é©ç”¨ã§ãã€é›»åœ§å¤‰æ›ãªã—ä¾¿åˆ©ãªã‚‚ã®ã§ã‚ã‚‹ã€‚ç„¡æœŸé™ä¿è¨¼ã€‚ã‚¢ãƒ‰ãƒ“ãƒ­ã‚´ãŒç™½ã§æŠ¼å°ã•ã‚Œã‚‹ã€‚','2009-03-27 05:01:59'),('AB-221','ë ˆë“œ ë¯¸ë‹ˆ ê´‘ë§ˆìš°ìŠ¤',10.5,96,'RedMouseAB-221.gif','ì´ ìŠ¬ë¦¼í•œ ë””ìžì¸ì˜ ìžë™ê°ê¹€ì‹ ë¯¸ë‹ˆ ê´‘ë§ˆìš°ìŠ¤ë¡œ ì±…ìƒì˜ ë¶„ìœ„ê¸°ë¥¼ ë°”ê¿” ì£¼ì„¸ìš”. í¬ë¡¬ ì•¡ì„¼íŠ¸ê°€ ë‹ë³´ì´ëŠ” ê¸€ë¡œì‹œ ë ˆë“œ.','2007-12-13 23:58:17');
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-03-27  5:04:58
