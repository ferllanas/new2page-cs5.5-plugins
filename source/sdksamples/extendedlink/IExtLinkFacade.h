//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/IExtLinkFacade.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IExtLinkFacade_h__
#define __IExtLinkFacade_h__

// Interface includes:
#include "IPMUnknown.h"
#include "URI.h"
#include "URIList.h"
#include "ILink.h"

class ITextModel;
class RangeData;
class ExtLinkOptionsData;
class IDocument;

// Project includes:
#include "ExtLinkID.h"

/** Abstract facade interface that encapsulates XMLExtLink features and functionality.

	@ingroup extendedlink
*/
class IExtLinkFacade : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IExtLinkFACADE };
	

	/** Determine if data can be inserted.
		@param iTextModel IN the text model of the target story.
		@param textIndex IN the start of the range of text to examine.
		@param length the IN length of the range of text to examine.
		Return kSuccess if tag can be inserted, other ErrorCode otherwise.
	*/	
	virtual bool16 CanInsertData(ITextModel* iTextModel, TextIndex textIndex, int32 length) = 0;

	/** 
		Create all data for each value in the given records of database.
		@pre IExtLinkFacade::CanInsertData == kTrue
		@param iTextModel IN the text model of the target story.
		@param textIndex IN the position in the text to insert the data.
		@return kSuccess on success, other ErrorCode otherwise.
	*/
	virtual ErrorCode InsertSelectedRecords(ITextModel* iTextModel, TextIndex& textIndex, const URIList& uriList) = 0;

	/**	
		 Converts an URI that represents our database asset to an IDFile. Only image type asset can be converted.
		 @param uri	 [IN] URI to convert.
		 @param file [OUT] Resulting IDFile.
		 @return True if the conversion succeeds, else false. 
	 */
	virtual bool URIToIDFile(const URI& uri, IDFile& file) const = 0;

	/**	Given resource read stream, return the content of the stream

		@param iPMStream input stream to process
		@param WideString [OUT]the field value of the corresponding asset from the linked resource
		@return True if the operation succeeds, else false. 
	 */
	virtual bool GetAssetValueFromResourceStream(IPMStream* iPMStream, WideString& value) const = 0;


	/**	Given an URI, return the field value of the corresponding asset.
		@param uri [IN] the link resource uri
		@param WideString [OUT]the field value of the corresponding asset from the linked resource
		@return True if the operation succeeds, else false. 
	 */
	virtual bool GetAssetValueFromURI(const URI& uri, WideString& value) const = 0;

	/**	Given an URI, return the version value of the corresponding asset.
		@param uri [IN] the link resource uri
		@param WideString [OUT]the version value of the corresponding asset from the linked resource
		@return True if the operation succeeds, else false. 
	 */
	virtual bool GetAssetVersionFromURI(const URI& uri, WideString& version) const = 0;

	/**	Exec cmd to change options for this plug-in
		@param options [IN] list of options to drive this command with
		@return  kSuccess if we could process it, kFailure otherwise
	 */
	virtual ErrorCode ProcessSetOptionsCommand(const K2Vector<ExtLinkOptionsData>& options, bool16 autoRefresh) = 0;
	
	/**	Exec cmd to set show in links UI flag of ILink
		@param options [IN] yes to show in Application's links panel
		@param linksUIDList [IN] uid list of ILinks to which the flag is set.
		@return  kSuccess if we could process it, kFailure otherwise
	 */
	virtual ErrorCode ProcessShowInLinksUICommand(const bool16 showInUI, UIDList& linksUIDList) = 0;
	
	/**	Exec cmd to set show in links UI flag of ILink
		@param options [IN] yes to show in Application's links panel
		@param document [IN] link objects in it the flag is set.
		@return  kSuccess if we could process it, kFailure otherwise
	 */
	virtual ErrorCode ProcessShowInLinksUICommand(const bool16 showInUI, IDocument* document) = 0;

	/**	Exec cmd to change the link object modification state
		@param options [IN] kObjectModified or kObjectUnmodified
		@param linksUIDList [IN] uid list of ILinks to which the flag is set.
		@return  kSuccess if we could process it, kFailure otherwise
	 */
	virtual ErrorCode ProcessSetLinkObjectModStateCommand(const ILink::ObjectModificationState state, UIDList& linksUIDList) = 0;
	
	/**	Exec cmd to change the link resource modification state
		@param state [IN] kResourceModified or kResourceUnmodified
		@param linksUIDList [IN] uid list of ILinks to which the flag is set.
		@return  kSuccess if we could process it, kFailure otherwise
	 */
	virtual ErrorCode ProcessSetLinkResourceModStateCommand(const ILink::ResourceModificationState state, UIDList& linksUIDList) = 0;
	
	/** open kExtLinkPointerStreamWriteBoss and direct buffer pointer to the stream for write
		@param buffer [IN] the buffer to transfer
		@param len [IN] number of bytes to transfer.
	*/
	virtual IPMStream* CreateExtLinkPointerStreamWrite(char *buffer, int32 len) const = 0;

	/**	Rebuild ODBC cache data
	 */
	virtual void RebuildODBCCacheData() = 0;
	
	/**	Exec cmd to set show in links UI flag in preference, call 
		ProcessShowInLinksUICommand() if there is an active document.
		@param options [IN] yes to show in Application's links panel
		@return  kSuccess if we could process it, kFailure otherwise
	 */
	virtual ErrorCode SetExtLinksShowInLinksPanel(bool16 showInUI) = 0;
	
};

#endif
