//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIDataNode.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes

// Other API includes
#include "K2Vector.tpp"

// Project includes
#include "ExtLinkUIDataNode.h"



/**
	@ingroup extendedlinkui
*/

/* Constructor
*/
ExtLinkUIDataNode::ExtLinkUIDataNode(URI uri, UID uid) : 
		fURI(uri),
		fUID(uid),
		fParent(nil)
{
}


/* Destructor
*/
ExtLinkUIDataNode::~ExtLinkUIDataNode()
{
}


/* Copy constructor
*/
ExtLinkUIDataNode::ExtLinkUIDataNode(const ExtLinkUIDataNode& rhs) : fParent(nil)
{
	deepcopy(rhs);
}

/* Overloaded =
*/
ExtLinkUIDataNode& ExtLinkUIDataNode::operator=(const ExtLinkUIDataNode& rhs)
{
	deepcopy(rhs);
	return *this;
}


/* Overloaded ==
*/
bool ExtLinkUIDataNode::operator==(const ExtLinkUIDataNode& rhs) const
{
	return		this->fParent == rhs.fParent && 
				this->fURI == rhs.fURI &&
				this->fUID == rhs.fUID;
				this->fChildren == rhs.fChildren;
}


/* deepcopy
*/
void ExtLinkUIDataNode::deepcopy(const ExtLinkUIDataNode& rhs)
{
	this->fChildren.clear();
	this->fChildren.assign( rhs.fChildren.begin(), rhs.fChildren.end());
	this->fParent = rhs.fParent;
	this->fURI = rhs.fURI;
	this->fUID = rhs.fUID;
}


/* GetNthChild
*/
const ExtLinkUIDataNode* ExtLinkUIDataNode::GetNthChild(int32 indexInParent) const
{
	return this->fChildren.at(indexInParent);
}


/* AddChild
*/
void ExtLinkUIDataNode::AddChild(const ExtLinkUIDataNode* o)
{
	this->fChildren.push_back(const_cast<ExtLinkUIDataNode* >(o));
}



/* RemoveChild
*/
void ExtLinkUIDataNode::RemoveChild(const ExtLinkUIDataNode* o)
{
	K2Vector<ExtLinkUIDataNode*>::iterator result = std::find(fChildren.begin(), fChildren.end(), o);
	if(result != fChildren.end())
	{
		fChildren.erase(result);
	}
}


/* GetParent
*/
ExtLinkUIDataNode* ExtLinkUIDataNode::GetParent() const
{
	return this->fParent;
}


/* SetParent
*/
void ExtLinkUIDataNode::SetParent(const ExtLinkUIDataNode* p)
{
	this->fParent = const_cast<ExtLinkUIDataNode* >(p);
}


/* ChildCount
*/
int32 ExtLinkUIDataNode::ChildCount() const
{
	return static_cast<int32>(this->fChildren.size());
}


/* SetNodeURI
*/
void ExtLinkUIDataNode::SetNodeURI(const URI theURI)
{
	this->fURI = theURI;
}

/* GetNodeURI
*/
URI ExtLinkUIDataNode::GetNodeURI() const
{
	return this->fURI;
}


/* SetLinkUID
*/
void ExtLinkUIDataNode::SetLinkUID(const UID theUID)
{
	this->fUID = theUID;
}


/* GetLinkUID
*/
UID ExtLinkUIDataNode::GetLinkUID() const
{
	return this->fUID;
}

//	end, File:	ExtLinkUIDataNode.cpp
