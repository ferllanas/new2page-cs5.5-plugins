//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkChangeOptionsCmd.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISubject.h"
#include "IWorkspace.h"
#include "IBoolData.h"

// General includes:
#include "CmdUtils.h"
#include "ErrorUtils.h"
#include "Command.h"

// Project includes:
#include "ExtLinkID.h"
#include "IExtLinkOptions.h"
#include "IExtLinkChangeOptionsCmdData.h"


/** Implementation of command to change the preferences IExtLinkOptions interface, which
	is passed into command in the item list.  The only IExtLinkOptions option is aggregated 
	on the session workspace. 

	
	@ingroup extendedlink
*/

class ExtLinkChangeOptionsCmd : public Command
{
public:
	/** Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.*/
	ExtLinkChangeOptionsCmd(IPMUnknown* boss);

protected:
	/** Performs notification. */
	virtual void DoNotify();

	/** Do the model chaneg to IExtLinkOptions, which is passed into the command through the item list */
	virtual void Do();

	/** Sets command name. */
	virtual PMString* CreateName();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(ExtLinkChangeOptionsCmd, kExtLinkChangeOptionsCmdImpl)


/* Constructor
*/
ExtLinkChangeOptionsCmd::ExtLinkChangeOptionsCmd(IPMUnknown* boss) :
	Command(boss)
{
}


/* Do
*/
void ExtLinkChangeOptionsCmd::Do()
{
	ErrorCode status = kFailure;
	do						
	{
		InterfacePtr<IExtLinkChangeOptionsCmdData> 
			cmdData (this, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}
		
		InterfacePtr<IBoolData> iAutoRefresh (this, IID_IAUTOREFRESHSTATUS);
		ASSERT(iAutoRefresh);		
		
		// Get the ItemList
		const UIDList* itemList = this->GetItemList();
		if (itemList == nil || itemList->IsEmpty())
		{
			break;
		}
		UIDRef uidRef = itemList->GetRef(0);
		InterfacePtr<IExtLinkOptions> 
			iExtLinkOptions(uidRef, UseDefaultIID());
		ASSERT(iExtLinkOptions);
		if (iExtLinkOptions == nil)
		{
			break;
		}

		iExtLinkOptions->AutoRefreshStatus(iAutoRefresh->Get());
		
		ASSERT(cmdData->GetOptionCount()>0);

		for(int32 i=0; i < cmdData->GetOptionCount(); i++)
		{
			iExtLinkOptions->AddOption(ExtLinkOptionsData(cmdData->GetNthOption(i)), i);
		}
		status = kSuccess;

	} while(kFalse);		

	// Handle any errors
	if (status != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(status);
	}
}

/* DoNotify
*/
void ExtLinkChangeOptionsCmd::DoNotify()
{
	InterfacePtr<IWorkspace> theWorkSpace(GetExecutionContextSession()->QueryWorkspace());
	InterfacePtr<ISubject> subject(theWorkSpace, IID_ISUBJECT);
	if (subject)
	{
		subject->ModelChange(kExtLinkChangeOptionsCmdBoss, IID_IEXTLINKOPTIONS, this);
	}
}


/* CreateName
*/
PMString* ExtLinkChangeOptionsCmd::CreateName()
{
	return new PMString(kExtLinkChangeOptionsCmdKey);
}


