//========================================================================================
//  
//  $File: //depot/indesign_4.0/highprofile/source/sdksamples/basicdragdrop/N2PSQLDNDDropTarget.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2005/03/08 13:08:25 $
//  
//  $Revision: #1 $
//  
//  $Change: 323503 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPageItemScrapData.h"
#include "IPathGeometry.h"
#include "ITextControlData.h"
#include "IPanelControlData.h"
#include "IPageItemTypeUtils.h"
#include "IGraphicFrameData.h"
#include "IWidgetParent.h"
#include "IMultiColumnTextFrame.h"
#include "IHierarchy.h"
#include "ILayoutUIUtils.h"
#include "IDocument.h"
#include "IComposeScanner.h"
#include "ITextModel.h"
#include "IXMLReferenceData.h"
#include "XMLReference.h"
#include "IIDXMLElement.h"
#include "IXMLUtils.h"

#include "ILinkManager.h"
#include "ILinkResource.h"
#include "ILinkUtils.h"
#include "IDataLink.h"
#include "ILink.h"

#include "IWidgetUtils.h"
#include "ITreeViewMgr.h"
#include "IStringListData.h"


// General includes:
#include "CDragDropTarget.h"
#include "CAlert.h"
#include "PMFlavorTypes.h"
#include "DataObjectIterator.h"
#include "UIDList.h"
#include "IPathUtils.h"
#include "PMString.h"
#include "SDKLayoutHelper.h"
#include "FileUtils.h"
#include "LinksID.h"

// Project includes:
#include "N2PsqlID.h"
#include "N2PSQLUtilities.h"
#include "UpdateStorysAndDBUtilis.h"
#include "InterlasaUtilities.h"

#ifdef WINDOWS


	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PsqlLogInLogOutUtilities.h"

#endif

#ifdef MACINTOSH


	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PsqlLogInLogOutUtilities.h"
	

#endif


/** the flavour our target will handle */
#define dropTgtFlavor PMFlavor(kPageItemFlavor)

/** N2PSQLDNDDropTarget
	Provides the drop behaviour. Our drag and drop target accepts page items and reasons about their shape. 
	We override the DoDragWithin method to replace the custom cursor behaviour (open hand rather than copy).
	This drag and drop target is bound to a panel...
	
	@ingroup basicdragdrop
	
*/
class N2PSQLDNDDropTarget : public CDragDropTarget
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSQLDNDDropTarget(IPMUnknown* boss);
		/**
			when we enter the panel, we change the cursor to be an open hand. If we had some dynamic cursor behaviour
			(the cursor changing dependent on where the mouse is within the panel), we would provide this behaviour in
			the DoDragWithin method.
		*/
		void DoDragEnter();
		/**
			this method defines the target response to the drag. It is called when the mouse enters the panel. We
			inidcate we can accept drags with a kPageItem flavor, that we do not want the default cursor and we do
			want the default panel highlight behaviour.

			@param dataIter IN iterator providing access to the data objects within the drag.
			@param fromSource IN the source of the drag.
			@param controller IN the drag drop controller mediating the drag.
			@return a target response (either won't accept or drop will copy).

			@see DragDrop::TargetResponse
		*/
		DragDrop::TargetResponse CouldAcceptTypes(DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController* controller) const;
		/**
			When the drop is performed, this method is called. We get the data item from the scrap and test its shape.
			We then change the static text widget associated with the panel to reflect the shape of this widget.
			@param controller IN the drag drop controller mediating the drag.
			@param type IN drag and drop command type
			@see DragDrop::eCommandType
		*/
		ErrorCode	ProcessDragDropCommand(IDragDropController* controller, DragDrop::eCommandType type);
		
	private:
		

		
		/**
			@return kTrue if the given item is an empty spline, kFalse otherwise.
		*/
		bool16 IsSpline(const UIDRef& pageItemUIDRef) const;


};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PSQLDNDDropTarget, kN2PSQLDNDDropTargetImpl)

/* N2PSQLDNDDropTarget Constructor
*/
N2PSQLDNDDropTarget::N2PSQLDNDDropTarget(IPMUnknown* boss)
: CDragDropTarget(boss)
{
}

/* DoDragEnter. The drag enters the panel.
*/
void N2PSQLDNDDropTarget::DoDragEnter()
{
	InterfacePtr<IDragDropController> ddController(GetExecutionContextSession(), IID_IDRAGDROPCONTROLLER);
	ddController->SetTrackingCursorFeedback(CursorSpec(kCrsrOpenHand));		
}

DragDrop::TargetResponse 
N2PSQLDNDDropTarget::CouldAcceptTypes(DataObjectIterator* dataIter, const IDragDropSource* fromSource, const IDragDropController*) const
{
	DataExchangeResponse response;
	
	
	response = dataIter->FlavorExistsWithPriorityInAllObjects(dropTgtFlavor);
			if(response.CanDo())
			{
 				return DragDrop::TargetResponse( response, DragDrop::kDropWillCopy,
					 DragDrop::kUseDefaultTrackingFeedback,
					 DragDrop::kTargetWillProvideCursorFeedback);

			}
	return DragDrop::kWontAcceptTargetResponse;
}

/*
	the drop has occured, we need to handle it.
*/
ErrorCode	
N2PSQLDNDDropTarget::ProcessDragDropCommand(IDragDropController* controller,DragDrop::eCommandType type)
{
	bool16 returnCode = kFailure;
	
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			
			break;
		}
		
		/////
		if(!N2PSQLUtilities::IsShowWidgetEnN2PSQLPaleta(kN2PsqlBotonSubirNewNoteWidgetID))
		{
			break;
		}
		
		//Si existen usuarios Logeados
		if(!N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			break;
		}
		
		PMString N2PSQLFolioPag = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
		
		//Si es una pagina de N2PSQL
		if(N2PSQLFolioPag.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert(kN2PSQLYouCouldSaveThisPageString);
			break;
		}
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil){
			CAlert::InformationAlert("UpdateStorys==nil");
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		//
		InterfacePtr<IControlView>  icontrolView(this, UseDefaultIID());
		ASSERT(icontrolView);
		if(!icontrolView) 
		{
			CAlert::InformationAlert("icontrolView for text widget is nil?");
			break;
		}
		
		
		WidgetID thisID = icontrolView->GetWidgetID();
		
	
		// we should ensure the drag has been internalized, if we are coming from a 
		// custom source, the handler may not be initialised
		if (controller->InternalizeDrag(dropTgtFlavor, dropTgtFlavor) != kSuccess)	
		{
			CAlert::InformationAlert("Can't internalize drag?");
			ASSERT_FAIL("Can't internalize drag?");
			break;
		}

		// get the data exchnage handler for this object...
		InterfacePtr<IDataExchangeHandler> handler(controller->QueryTargetHandler());
		if (handler == nil)
		{
			CAlert::InformationAlert("Data exchange handler is nil?");
			ASSERT_FAIL("Data exchange handler is nil?");
			break;
		}
		
		// the item that is being dragged is on the scrap
		InterfacePtr<IPageItemScrapData> data(handler,UseDefaultIID());
		if (data == nil)
		{
			CAlert::InformationAlert("Page item scrap data is nil?");
			ASSERT_FAIL("Page item scrap data is nil?");
			break;
		}
		
		// we capture the shape as a string
		PMString shape;
		
		// get the list of objects captured by the drag
		UIDList*	theUIDs = data->CreateUIDList();
		
		if(theUIDs->Length() > 1)
		{
			int32 numOfPageItemSinParagraphStyle=-1;
			for(int32 h=0;h<theUIDs->Length();h++){
				
				// we are only interested in the first object dragged.
				UIDRef primaryObject = theUIDs->GetRef(h);
				
				InterlasaUtilities::GetPageItemType(primaryObject,shape);
				
				//si no es textFrame sale de la funcion
				if(shape!="TextFrame")
				{
					CAlert::InformationAlert("shape!=TextFrame!"+shape);
					continue;
				}
				
				SDKLayoutHelper layoutHelper;
				InterfacePtr<IGraphicFrameData> graphicFrameData(primaryObject, UseDefaultIID());
				ASSERT(graphicFrameData);
				if (!graphicFrameData) 
				{
					CAlert::InformationAlert("textFrame control data interface for widget?");
					break;
				}
				UIDRef storyUIDRef = layoutHelper.GetTextModelRef(graphicFrameData);
				
				UID textModelUID = storyUIDRef.GetUID(); //UIDTextModel
				int32 UIDint32Textmodel=textModelUID.Get();
				
				InterfacePtr<ITextModel> textModel(storyUIDRef, UseDefaultIID());
				if (textModel == nil) 
				{
					CAlert::InformationAlert("textModel == nil");
					break;
				}
				
				PMString UIDTextModelString="";
				UIDTextModelString.AppendNumber(UIDint32Textmodel);
				
				InterfacePtr<IComposeScanner> compScanner(textModel, UseDefaultIID());
				if (compScanner == nil) 
				{
					CAlert::InformationAlert("compScanner == nil");
					break;
				}
				
				RangeData myrange= 	textModel->GetStoryThreadRange (0); 
				InDesign::TextRange range(textModel, myrange.Start(nil), myrange.End() - myrange.Start(nil));
				
				TextIndex indexPara=0;
				int32 lengtPara=0;
				bool16 SEencontro=kFalse;
				if(PrefConections.nameParaStyleTitulo.NumUTF16TextChars()>0)
				{	
					int32 countItems=InterlasaUtilities::CountItems(PrefConections.nameParaStyleTitulo,",");
					if(countItems>0)
					{
						for(int32 numItem=1;numItem<=countItems && SEencontro==kFalse;numItem++){
							PMString SXD="";
							SXD.AppendNumber(numItem);
							if(PrefConections.nameParaStyleTitulo.GetItem(",", numItem)!=nil){
								
								PMString Replaced;
								Replaced.AppendW(PrefConections.nameParaStyleTitulo.GetItem(",", numItem)->GrabWString());

								if(N2PSQLUtilities::FindParaStyleOnStory(range,Replaced,indexPara,lengtPara,kFalse )==kSuccess)
								{
									N2PSQLUtilities::SaveXMPVar("UIDTextModelTitulo", UIDTextModelString, document);
									thisID = kN2PsqlTextTituloWidgetID;
									SEencontro=kTrue;
								}
							}
						}
					}
					else {
						if(N2PSQLUtilities::FindParaStyleOnStory(range,PrefConections.nameParaStyleTitulo,indexPara,lengtPara,kFalse )==kSuccess)
						{
							N2PSQLUtilities::SaveXMPVar("UIDTextModelTitulo", UIDTextModelString, document);
							thisID = kN2PsqlTextTituloWidgetID;
							SEencontro=kTrue;
						}
					}

				}
				
				if(PrefConections.nameParaStyleBalazo.NumUTF16TextChars()>0)
				{	
					int32 countItems=InterlasaUtilities::CountItems(PrefConections.nameParaStyleBalazo,",");
					if(countItems>0)
					{
						for(int32 numItem=1;numItem<=countItems && SEencontro==kFalse;numItem++){
							if(PrefConections.nameParaStyleBalazo.GetItem(",", numItem)!=nil){
								
								PMString Replaced;
								Replaced.AppendW(PrefConections.nameParaStyleBalazo.GetItem(",", numItem)->GrabWString());
								
								
								if(N2PSQLUtilities::FindParaStyleOnStory(range,Replaced,indexPara,lengtPara,kFalse )==kSuccess)
								{
									N2PSQLUtilities::SaveXMPVar("UIDTextModelBalazo", UIDTextModelString, document);
									thisID = kN2PsqlTextBalazoWidgetID;
									SEencontro=kTrue;
								}
							}
						}
					}
					else {
						if(N2PSQLUtilities::FindParaStyleOnStory(range,PrefConections.nameParaStyleBalazo,indexPara,lengtPara,kFalse )==kSuccess)
						{
							N2PSQLUtilities::SaveXMPVar("UIDTextModelBalazo", UIDTextModelString, document);
							thisID = kN2PsqlTextBalazoWidgetID;
							SEencontro=kTrue;
						}
					}
					/*if(N2PSQLUtilities::FindParaStyleOnStory(range,PrefConections.nameParaStyleBalazo,indexPara,lengtPara,kFalse )==kSuccess)
					{
						N2PSQLUtilities::SaveXMPVar("UIDTextModelBalazo", UIDTextModelString, document);
						thisID = kN2PsqlTextBalazoWidgetID;
						SEencontro=kTrue;
					}*/
				}
				
				if(PrefConections.nameParaStyleContenido.NumUTF16TextChars()>0)
				{	
					int32 countItems=InterlasaUtilities::CountItems(PrefConections.nameParaStyleContenido,",");
					if(countItems>0)
					{
						for(int32 numItem=1;numItem<=countItems && SEencontro==kFalse;numItem++){
							if(PrefConections.nameParaStyleContenido.GetItem(",", numItem)!=nil){
								
								PMString Replaced;
								Replaced.AppendW(PrefConections.nameParaStyleContenido.GetItem(",", numItem)->GrabWString());
								
								
								if(N2PSQLUtilities::FindParaStyleOnStory(range,Replaced,indexPara,lengtPara,kFalse )==kSuccess)
								{
									N2PSQLUtilities::SaveXMPVar("UIDTextModelConteNota", UIDTextModelString, document);
									thisID = kN2PsqlTextNotaWidgetID;
									SEencontro=kTrue;
								}
							}
						}
					}else {
						if(N2PSQLUtilities::FindParaStyleOnStory(range,PrefConections.nameParaStyleContenido,indexPara,lengtPara,kFalse )==kSuccess)
						{
							N2PSQLUtilities::SaveXMPVar("UIDTextModelConteNota", UIDTextModelString, document);
							thisID = kN2PsqlTextNotaWidgetID;
							SEencontro=kTrue;
						}
					}
					/*if(N2PSQLUtilities::FindParaStyleOnStory(range,PrefConections.nameParaStyleContenido,indexPara,lengtPara,kFalse )==kSuccess)
					{
						N2PSQLUtilities::SaveXMPVar("UIDTextModelConteNota", UIDTextModelString, document);
						thisID = kN2PsqlTextNotaWidgetID;
						SEencontro=kTrue;
					}*/
				}
				
				
				if(SEencontro)
				{
					shape = N2PSQLUtilities::GetPMStringOfTextFrame(UIDint32Textmodel);
				
					shape.SetTranslatable(kFalse);
				
					InterlasaUtilities::setTextOfWidgetFromPalette(kN2PsqlPanelWidgetID,	thisID, shape);		
				}
				else {
					numOfPageItemSinParagraphStyle=h;
				}

				
				//delete theUIDs;
				returnCode = kSuccess;
				
			}
			
			//Alguna caja no tiene estilo de parrafo asignado
			if(numOfPageItemSinParagraphStyle >= 0)
			{
				// we are only interested in the first object dragged.
				UIDRef primaryObject = theUIDs->GetRef(numOfPageItemSinParagraphStyle);
				
				InterlasaUtilities::GetPageItemType(primaryObject,shape);
				
				//si no es textFrame sale de la funcion
				if(shape!="TextFrame")
				{
					CAlert::InformationAlert("shape!=TextFrame!"+shape);
					continue;
				}
				
				SDKLayoutHelper layoutHelper;
				InterfacePtr<IGraphicFrameData> graphicFrameData(primaryObject, UseDefaultIID());
				ASSERT(graphicFrameData);
				if (!graphicFrameData) 
				{
					CAlert::InformationAlert("textFrame control data interface for widget?");
					break;
				}
				UIDRef storyUIDRef = layoutHelper.GetTextModelRef(graphicFrameData);
				
				UID textModelUID = storyUIDRef.GetUID(); //UIDTextModel
				int32 UIDint32Textmodel=textModelUID.Get();
				
				InterfacePtr<ITextModel> textModel(storyUIDRef, UseDefaultIID());
				if (textModel == nil) 
				{
					CAlert::InformationAlert("textModel == nil");
					break;
				}
				
				PMString UIDTextModelString="";
				UIDTextModelString.AppendNumber(UIDint32Textmodel);
				
				InterfacePtr<IComposeScanner> compScanner(textModel, UseDefaultIID());
				if (compScanner == nil) 
				{
					CAlert::InformationAlert("compScanner == nil");
					break;
				}
				
				RangeData myrange= 	textModel->GetStoryThreadRange (0); 
				InDesign::TextRange range(textModel, myrange.Start(nil), myrange.End() - myrange.Start(nil));
				
				TextIndex indexPara=0;
				int32 lengtPara=0;
				bool16 SEencontro=kFalse;
				
				N2PSQLUtilities::SaveXMPVar("UIDTextModelConteNota", UIDTextModelString, document);
				thisID = kN2PsqlTextNotaWidgetID;
				SEencontro=kTrue;	
				
				
				shape = N2PSQLUtilities::GetPMStringOfTextFrame(UIDint32Textmodel);
				
				shape.SetTranslatable(kFalse);
				
				InterlasaUtilities::setTextOfWidgetFromPalette(kN2PsqlPanelWidgetID,	thisID, shape);		
			}			
			
		}
		else {
			// we are only interested in the first object dragged.
			UIDRef primaryObject = theUIDs->GetRef(0);

			InterlasaUtilities::GetPageItemType(primaryObject,shape);
			//si no es textFrame sale de la funcion
			if(shape!="TextFrame")
			{
				break;
			}
		
			//Para obtener el UIDModel del textFrame
			SDKLayoutHelper layoutHelper;
			InterfacePtr<IGraphicFrameData> graphicFrameData(primaryObject, UseDefaultIID());
			ASSERT(graphicFrameData);
			if (!graphicFrameData) 
			{
				CAlert::InformationAlert("textFrame control data interface for widget?");
				break;
			}
			UIDRef storyUIDRef = layoutHelper.GetTextModelRef(graphicFrameData);


			UID textModelUID = storyUIDRef.GetUID(); //UIDTextModel
			int32 UIDint32Textmodel=textModelUID.Get();//Pasa a entero el UID del TextModel
        
			bool16 esnota=kFalse;
			PMString IDNota="";
			if(N2PSQLUtilities::GetIDNota_ConIDTextFrame_SobreXMPNotasFluidas(UIDint32Textmodel, IDNota,esnota))
			{
				//este es un elemento de una nota de N2PSQL
				CAlert::InformationAlert(kN2PSQLThisIsElemDNotaString);
				break;
			}
		
		
		 
			PMString UIDTextModelString="";
			UIDTextModelString.AppendNumber(UIDint32Textmodel);
		
		
		
			switch(thisID.Get())
			{
					//Obtiene el WidgetID donde se va a colocar el texto
					// Y Guarda en XML el UID del TextModel del Correspondiente al elemento de cada nota
			
			
				case kN2PsqlStTextTituloTextWidgetID:
				{
				
					N2PSQLUtilities::SaveXMPVar("UIDTextModelTitulo", UIDTextModelString, document);
				
					thisID = kN2PsqlTextTituloWidgetID;
			
					break;
				}
			
				case kN2PsqlStTextBalazoTextWidgetID:
				{
					//kN2PsqlTextBalazoWidgetID
					N2PSQLUtilities::SaveXMPVar("UIDTextModelBalazo", UIDTextModelString,document);
					thisID = kN2PsqlTextBalazoWidgetID;
					break;
				}
			
			
			
				case kN2PsqlStTextNotaTextWidgetID:
				{
					//kN2PsqlTextNotaWidgetID
					N2PSQLUtilities::SaveXMPVar("UIDTextModelConteNota", UIDTextModelString, document);
					thisID = kN2PsqlTextNotaWidgetID;
					break;
				}
			
				case kN2PsqlStTextPieTextWidgetID:
				{
					//kN2PsqlTextPieWidgetID
					N2PSQLUtilities::SaveXMPVar("UIDTextModelPieFoto", UIDTextModelString, document);
					thisID = kN2PsqlTextPieWidgetID;
					break;
				}
			
			}
			
			shape = N2PSQLUtilities::GetPMStringOfTextFrame(UIDint32Textmodel);
			
			// we are not going to localise the possible values.
			shape.SetTranslatable(kFalse);
			
			
			InterlasaUtilities::setTextOfWidgetFromPalette(kN2PsqlPanelWidgetID,	thisID, shape);		
			
			delete theUIDs;
			returnCode = kSuccess;
			
		}

		
	}while(false);
	return returnCode;
}

// End, N2PSQLDNDDropTarget.cpp.






