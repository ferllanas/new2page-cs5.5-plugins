//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkID.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __ExtLinkID_h__
#define __ExtLinkID_h__

#include "SDKDef.h"

// Company:
#define kExtLinkCompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kExtLinkCompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kExtLinkPluginName	"ExtendedLink"			// Name of this plug-in.
#define kExtLinkPrefixNumber	0x68200 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kExtLinkVersion		kSDKDefPluginVersionString						// Version of this plug-in (for the About Box).
#define kExtLinkAuthor		"Adobe Developer Technologies"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kExtLinkPrefixNumber above to modify the prefix.)
#define kExtLinkPrefix		RezLong(kExtLinkPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kExtLinkStringPrefix	SDK_DEF_STRINGIZE(kExtLinkPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kExtLinkMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kExtLinkMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kExtLinkPluginID, kExtLinkPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kExtLinkErrorStringServiceBoss,		kExtLinkPrefix + 1)
DECLARE_PMID(kClassIDSpace, kExtLinkExportHandlerBoss,		kExtLinkPrefix + 2)
DECLARE_PMID(kClassIDSpace, kExtLinkDOMSerializerHandlerBoss,		kExtLinkPrefix + 3)
DECLARE_PMID(kClassIDSpace, kExtLinkODBCLinkResourceProviderBoss,		kExtLinkPrefix + 4)
DECLARE_PMID(kClassIDSpace, kExtLinkODBCLinkResourceHandlerBoss,		kExtLinkPrefix + 5)
DECLARE_PMID(kClassIDSpace, kExtLinkODBCUpdateLinkServiceProviderBoss,		kExtLinkPrefix + 6)
DECLARE_PMID(kClassIDSpace, kExtLinkODBCXMLLinkObjectReferenceBoss,		kExtLinkPrefix + 7)
DECLARE_PMID(kClassIDSpace, kExtLinkPointerStreamWriteBoss,		kExtLinkPrefix + 8)
DECLARE_PMID(kClassIDSpace, kExtLinkChangeOptionsCmdBoss, kExtLinkPrefix + 9)
DECLARE_PMID(kClassIDSpace, kExtLinkSetShowInLinksUICmdBoss, kExtLinkPrefix + 10)
DECLARE_PMID(kClassIDSpace, kExtLinkDocResponderBoss, kExtLinkPrefix + 11)
DECLARE_PMID(kClassIDSpace, kExtLinkSetObjectModificationStateCmdBoss, kExtLinkPrefix + 12)
DECLARE_PMID(kClassIDSpace, kExtLinkSetResourceModificationStateCmdBoss, kExtLinkPrefix + 13)
DECLARE_PMID(kClassIDSpace, kExtLinkStartupShutdownBoss, kExtLinkPrefix + 14)
DECLARE_PMID(kClassIDSpace, kExtLinkSetShowInUIPrefCmdBoss, kExtLinkPrefix + 15)
DECLARE_PMID(kClassIDSpace, kExtLinkConversionProviderBoss, kExtLinkPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kExtLinkBoss, kExtLinkPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IExtLinkSUITE, kExtLinkPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IExtLinkFACADE, kExtLinkPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_IExtLinkPARSINGCONTEXT, kExtLinkPrefix + 3)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKODBCCACHE, kExtLinkPrefix + 4)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKOPTIONS, kExtLinkPrefix + 5)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKCHANGEOPTIONSCMDDATA, kExtLinkPrefix + 6)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKOBSERVER, kExtLinkPrefix + 7)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKSTATE, kExtLinkPrefix + 8)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKSTATEUPDATEPROCESSOR, kExtLinkPrefix + 9)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKIDLETASK, kExtLinkPrefix + 10)
DECLARE_PMID(kInterfaceIDSpace, IID_ISHOWINLINKSUI, kExtLinkPrefix + 11)
DECLARE_PMID(kInterfaceIDSpace, IID_IAUTOREFRESHSTATUS, kExtLinkPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKINTERFACE, kExtLinkPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kExtLinkSuiteASBImpl,					kExtLinkPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kExtLinkSuiteTextCSBImpl,				kExtLinkPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kExtLinkFacadeImpl,					kExtLinkPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kExtLinkErrorStringServiceImpl,		kExtLinkPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kExtLinkExportHandlerImpl,		kExtLinkPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kExtLinkDOMSerializerHandlerImpl,		kExtLinkPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kExtLinkParsingContextImpl,		kExtLinkPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kExtLinkODBCLinkResourceHandlerImpl,		kExtLinkPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kExtLinkODBCLinkResourceFactoryImpl,		kExtLinkPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kExtLinkODBCLinkResourceStateProcessorFactoryImpl,		kExtLinkPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kExtLinkODBCLinkResourceStateUpdaterImpl,		kExtLinkPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kExtLinkODBCUpdateLinkServiceImpl,		kExtLinkPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kExtLinkODBCXMLElementLinkObjectImpl,		kExtLinkPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kExtLinkPointerStreamWriteImpl,		kExtLinkPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kExtLinkODBCCacheImpl, kExtLinkPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kExtLinkOptionsImpl, kExtLinkPrefix + 16)
DECLARE_PMID(kImplementationIDSpace, kExtLinkChangeOptionsCmdImpl, kExtLinkPrefix + 17)
DECLARE_PMID(kImplementationIDSpace, kExtLinkChangeOptionsCmdDataImpl, kExtLinkPrefix + 18)
DECLARE_PMID(kImplementationIDSpace, kExtLinkSetShowInLinksUICmdImpl, kExtLinkPrefix + 19)
DECLARE_PMID(kImplementationIDSpace, kExtLinkDocServiceProviderImpl, kExtLinkPrefix + 20)
DECLARE_PMID(kImplementationIDSpace, kExtLinkDocResponderImpl, kExtLinkPrefix + 21)
DECLARE_PMID(kImplementationIDSpace, kExtLinkDocObserverImpl, kExtLinkPrefix + 22)
DECLARE_PMID(kImplementationIDSpace, kExtLinkSetObjectModificationStateCmdImpl, kExtLinkPrefix + 23)
DECLARE_PMID(kImplementationIDSpace, kExtLinkSetResourceModificationStateCmdImpl, kExtLinkPrefix + 24)
DECLARE_PMID(kImplementationIDSpace, kExtLinkODBCLinkResourceStateUpdaterIdleTaskImpl, kExtLinkPrefix + 25)
DECLARE_PMID(kImplementationIDSpace, kExtLinkStateUpdateProcessorImpl, kExtLinkPrefix + 26)
DECLARE_PMID(kImplementationIDSpace, kExtLinkStartupShutdownImpl, kExtLinkPrefix + 27)
DECLARE_PMID(kImplementationIDSpace, kExtLinkShowInLinksUIImpl, kExtLinkPrefix + 28)
DECLARE_PMID(kImplementationIDSpace, kExtLinkSetShowInUIPrefCmdImpl, kExtLinkPrefix + 29)
//DECLARE_PMID(kImplementationIDSpace, kExtLinkImpl, kExtLinkPrefix + 30)
//DECLARE_PMID(kImplementationIDSpace, kExtLinkImpl, kExtLinkPrefix + 31)

// Link Client IDs:
DECLARE_PMID(kLinkClientIDSpace, kExtLinkClientID, kExtLinkPrefix + 1)

// ErrorIDs:
DECLARE_PMID(kErrorIDSpace, kExtLinkFailureErrorCode,						kExtLinkPrefix + 0)
DECLARE_PMID(kErrorIDSpace, kExtLinkDataAccessErrorCode,					kExtLinkPrefix + 1)
DECLARE_PMID(kErrorIDSpace, kExtLinkFileFormatErrorCode,					kExtLinkPrefix + 3)
DECLARE_PMID(kErrorIDSpace, kExtLinkTargetStoryLockedErrorCode,				kExtLinkPrefix + 4)
DECLARE_PMID(kErrorIDSpace, kExtLinkTargetTextAlreadyTaggedErrorCode,		kExtLinkPrefix + 5)
DECLARE_PMID(kErrorIDSpace, kExtLinkBadTargetTextLengthErrorCode,			kExtLinkPrefix + 6)
DECLARE_PMID(kErrorIDSpace, kExtLinkNoImageFolderSpecifiedErrorCode,		kExtLinkPrefix + 7)
DECLARE_PMID(kErrorIDSpace, kExtLinkNoAuthoritySpecifiedErrorCode,			kExtLinkPrefix + 8)
DECLARE_PMID(kErrorIDSpace, kExtLinkNoTableSpecifiedErrorCode,				kExtLinkPrefix + 9)


// Other StringKeys:
#define kExtLinkInsertSequenceStringKey 				kExtLinkStringPrefix "kExtLinkInsertSequenceStringKey"
#define kExtLinkMergeSequenceStringKey 					kExtLinkStringPrefix "kExtLinkMergeSequenceStringKey"
#define kExtLinkInsertAllSequenceStringKey 				kExtLinkStringPrefix "kExtLinkInsertAllSequenceStringKey"
#define kExtLinkChangeOptionsCmdKey 					kExtLinkStringPrefix "kExtLinkChangeOptionsCmdKey"
#define kExtLinkSetShowInLinksUICmdKey 					kExtLinkStringPrefix "kExtLinkSetShowInLinksUICmdKey"
#define kExtLinkSetObjectModStateCmdKey 				kExtLinkStringPrefix "kExtLinkSetObjectModStateCmdKey"
#define kExtLinkSetShowInUIPrefCmdKey 					kExtLinkStringPrefix "kExtLinkSetShowInUIPrefCmdKey"
	
// Error StringKeys:
#define kExtLinkFailureErrorCodeStringKey					kExtLinkStringPrefix "kExtLinkFailureErrorCodeStringKey"
#define kExtLinkDataAccessErrorCodeStringKey				kExtLinkStringPrefix "kExtLinkDataAccessErrorCodeStringKey"
#define kExtLinkOpenFileErrorCodeStringKey					kExtLinkStringPrefix "kExtLinkOpenFileErrorCodeStringKey"
#define kExtLinkTargetStoryLockedErrorCodeStringKey			kExtLinkStringPrefix "kExtLinkTargetStoryLockedErrorCode"
#define kExtLinkTargetTextAlreadyTaggedErrorCodeStringKey	kExtLinkStringPrefix "kExtLinkTargetTextAlreadyTaggedErrorCode"
#define kExtLinkBadTargetTextLengthErrorCodeStringKey		kExtLinkStringPrefix "kExtLinkBadTargetTextLengthErrorCode"


// Initial data format version numbers
#define kExtLinkFirstMajorFormatNumber  kSDKDef_50_PersistMajorVersionNumber
#define kExtLinkFirstMinorFormatNumber  kSDKDef_50_PersistMinorVersionNumber

// Data format version numbers for the PluginVersion resource 
#define kExtLinkCurrentMajorFormatNumber kExtLinkFirstMajorFormatNumber
#define kExtLinkCurrentMinorFormatNumber RezLong(2)

#define kExtLinkScheme	"odbc"				// scheme of the uri for our link.
//#define kExtLinkAuthority	"MySQL"			// authority for the mySQL database connection.

#endif // __ExtLinkID_h__

//  Code generated by DollyXs code generator
