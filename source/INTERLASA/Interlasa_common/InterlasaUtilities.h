//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/FileTreeUtils.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __InterlasaUtilities_H_DEFINED__
#define __InterlasaUtilities_H_DEFINED__

#include "IParcel.h"
#include "ITextParcelList.h"
#include <IHierarchy.h>//aÒadio maribel





//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")

class IDocument;
class IControlView;
class IWindow;


/** A collection of miscellaneous utility methods used by this plug-in.
@ingroup paneltreeview
*/
class InterlasaUtilities
{

public:
	
	enum IUAjustes { 
		kIUAjusteContenidoAFrame, 		/** ask the user how to handle missing plug-ins */
		kIUPrefAjusteCentrar, 	/** ignore any missing plug-ins */
		kIUPrefAjusteProporcionalmente
	};
	
	static PMString GetXMPVar(PMString Variable, IDocument* doc);
	static bool16 SaveXMPVar(PMString Variable,PMString value,  IDocument* doc);
	static bool16 RemoveXMPVar(PMString Variable, IDocument* doc);
	
	
	//XMP
	static int32 CountXMPArrayItem(IDocument* doc,const PMString& structName);
	static bool16 DeleteXMPArrayItem(IDocument* doc,const PMString& structName, const int32& numElemento);
	static bool16 AppendXMPArrayItem( IDocument* doc,const PMString& structName,const PMString& Elemento);
	static bool16 GetXMPArrayItem(IDocument* doc,const PMString& structName,PMString& Elemento, const int32& numElemento);
	static bool16 SetXMPArrayItem( IDocument* doc,const PMString& structName,const PMString& Elemento, const int32& numElemento);
	static int32 FindItem_ToNameAndValue_OnXMPArrayItem(IDocument* doc,const PMString& structName,const PMString Name,const PMString& Value);
	static int32 FindItem_ToValue_OnXMPArrayItem(IDocument* doc,const PMString& structName,const PMString& Value,PMString& NameElement);
	
	static PMString CrearFolderPreferencias(const PMString& NamePlugIn);
	
	static PMString TruncatePath(const PMString& fromthis);
	
	static bool16 validPath(const PMString& p);
	
	static PMString TruncateExtencion(const PMString& fromthis);
		
	static bool16 CreateTags(const UIDRef& documentUIDRef, const K2Vector<PMString>& tagNames);
	
	static bool16 TagGraphic(const PMString& TagName, const UIDRef& GraphicFrameUIDRef);
	
	static bool16 GetTextOfDialog(IControlView *DlgPrefView,	const WidgetID& Combowidget, PMString& itemToShow);
	
	static bool16 CreateAndProcessOpenDocCmd(const IDFile& theFile);
	
	static void CreateAndProcessCloseWinCmd(IWindow *win);
	
	static ErrorCode CenterContentInFrame(const UIDRef& contentUIDRef);
	
	//Funcion que arma una fecha a partir de AAAA/MM/DD
	static PMString ArmaFecha(PMString fecha,
								const PMString& Idioma, 
								PMString& ano,
								PMString& mes,
								PMString& Dia);
								
	static void nameInStr(char daysInWord[], int days,const PMString& Idioma);
	static int dayInYear(int dd, int mm);
	static int calcDay_Dec31(int yyyy);
	static int validateDate(int dd, int mm, int yyyy);
	static PMString DayWek(int dd,int mm,int yyyy,const PMString& Idioma);
	static void printError();
	
	static bool16 ShowThePageByNumberPage(int32 which);
	
	
	static bool16 ObtenerNombreDAplicacion(PMString &NombreAplicacion);
	
	
	static PMString ChangeInDesignDateStringToMySQLDateString(PMString &InDesignDate);
	
	static PMString ChangeInDesignDateStringToDateStringYMD(PMString &InDesignDate);
	
	static PMString MacToUnix(PMString path);	
	

	static PMString UnixToMac(PMString path);	
	
	static PMString FormatFechaYHoraManana(PMString Format);
	
	static PMString FormatFechaYHoraActual(PMString Format);
	
	static PMString RemoveCharactersReturn(const PMString& Ordinary);
	
	static ErrorCode SaveCopyOfDocumentAs(const UIDRef & 	documentUIDRef,	const IDFile & 	sysFile,UIFlags uiFlags);//,UIFlags uiFlags = kFullUI
	
	static PMRect GetPageItemBoundingBox(const UIDRef &elem, PMReal *rotationAngle, PMReal *skewAngle  );
	
	
	/*	Funcion para obtener llenar la lista de Link a actualizar	*/
	static bool16 GetListaDLinksParaActualizar( const InterfacePtr< IHierarchy > & FrameHierarchy,UIDList& uidsToUpdate);


	/*
	Funcion Recursiva para navegas sobre la gerarquia PageItem para llenar la Lista de Links a Actualizar
	*/
	static bool16 RecursiveHierarchyGetUpdateListLinks( const InterfacePtr< IHierarchy > & hierarchyItem, UIDList& uidsToUpdate);


	/*
	Adiciona a la lista de Link a actualizar si es que lo necesita.
	*/
	static bool16 AddLinkToUpdateList(const InterfacePtr< IHierarchy > & imageHierarchy, UIDList& uidsToUpdate);
	
	static bool16 getTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID, PMString& value);
	static bool16 setTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID, const PMString& value);
	static bool16 HideOrShowWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID,  bool16 mostrar);
	static bool16 EnabledOrDisabledWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID,  bool16 mostrar);
	static bool16 IsShowWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID);
	
	/**
	 Determine the type of page item.
	 @param pageItemUIDRef IN reference to page item to be typed.
	 @param pageItemType OUT a string that describes the type of item e.g. "Group", GraphicFrame".
	 */
	static void GetPageItemType(const UIDRef& pageItemUIDRef, PMString& pageItemType);
	
	static bool16 IsEmptyGraphicFrame(const UIDRef& itemUIDRef);
	
	/**
	 Determine the shape of a given path page item.
	 @param pageItemUIDRef IN reference to page item looked at.
	 @param shape OUT a string that describes the shape e.g. "Line", "Circle", etc.
	 @return kTrue if the item is a spline, kFalse otherwise.
	 */
	static bool16 IsSplineShape(const UIDRef& itemUIDRef, PMString& shape);
	
	/**
	 @return kTrue if the given item is a group, kFalse otherwise.
	 */
	static bool16 IsGroup(const UIDRef& itemUIDRef);
	
	/*
	 */
	static bool16 SetRrscEnWidgetFromPalette(const WidgetID& PaleteWidgetID,	const WidgetID& widgetID,  const RsrcID &id);
	
	static PMString Converter(char * web_path);
	
	
	static void AplicarAjusteRecImagen(IUAjustes &ajustes,UIDRef GFrameUIDRef);
	
	static bool16 GetTextOfComboOnDialog(IControlView *DlgPrefView,	const WidgetID& Combowidget, PMString& itemToShow, int32& indexSelected);

	static PMString GetFolderLibraryOfShared();
	
	static void replaceUTF32Text(PMString& Replaced);
	static void ReplaceAllUTF32Ocurrencias(PMString& origen,UTF32TextChar &Target, PMString& replace);
	
	static int32 CountItems(PMString origen, PMString delimited);
	
	static void InstallUnmanagedFrameAdornment(const UIDRef &lockableStoryRef);
};

#endif // __PnlTrvUtils_H_DEFINED__

