//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/common/SDKODBCWrapper.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"


#include "PMString.h"
#include "WideString.h"
#include "URI.h"
#include "K2Vector.h"
#include "IDataStringUtils.h"
#include "Utils.h"
#include "StringUtils.h"
#include "CAlert.h"

#include <boost/tokenizer.hpp>
#include <sstream>
#include "SDKODBCWrapper.h"
/*	Constructor
	Initialize cache and variables, allocate ODBC handles
*/

#define MAX_BUF_LEN  30000//30000
#define DATA_BUFFER_LEN  8192

struct replaceBackSlash
{
	void operator()(char& c) { if(c == '"') c = '\"'; }
};


SDKODBCWrapper::SDKODBCWrapper() :  kODBCScheme("odbc"), fConnectionStatus(false), fNumColumns(0)
{

	SQLRETURN rc = SQL_SUCCESS;
	
	// Allocate environment handle
	rc = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &fEnvHandle);

	// Set to version 3
	if (rc == SQL_SUCCESS) {
  		rc = SQLSetEnvAttr(fEnvHandle, SQL_ATTR_ODBC_VERSION,(SQLPOINTER) SQL_OV_ODBC3, 0);
	}
	// Allocate connection handle
	if (rc == SQL_SUCCESS) {
		rc = SQLAllocHandle(SQL_HANDLE_DBC, fEnvHandle, &fConnectHandle);
	}
	//Initialize statement handle to NULL, we will reuse this statement handle until the object is released.
	// It is allocated when performing queries.
	// We cannot allocate it here because it requires an established connection
	fStmtHandle = NULL;

}

// Define The Class Destructor
SDKODBCWrapper::~SDKODBCWrapper()
{
	SQLRETURN rc = SQL_SUCCESS;

	// Free statement handle
	if (fStmtHandle != NULL) {
		SQLFreeHandle(SQL_HANDLE_STMT, fStmtHandle);
	}
	// Free connection handle
	if (fConnectHandle != NULL) {
		rc = SQLDisconnect(fConnectHandle);  // Disconnect from database
		SQLFreeHandle(SQL_HANDLE_DBC, fConnectHandle);
	}

	// Free environment handle
	if (fEnvHandle != NULL) {
		SQLFreeHandle(SQL_HANDLE_ENV, fEnvHandle);
	}

	ClearRecordsBuffer();
}

/** Disposes the temporary storage for the current query 
*/
void SDKODBCWrapper::ClearRecordsBuffer()
{
	std::vector< std::vector< FieldInfo* > >::iterator iter;
	for (iter = fRows.begin(); iter != fRows.end(); iter++) {
		std::vector<FieldInfo*> fields = *iter;
		std::vector<FieldInfo*>::iterator fieldIter = fields.begin();
		for (fieldIter = fields.begin(); fieldIter != fields.end(); fieldIter++) {
			delete *fieldIter;
			*fieldIter = NULL;
		}
		fields.clear();	// clear the columns for this row
	}
	fRows.clear();
}


/** Connect to a data source name
  This implementation assumes a data source name has been created outside of InDesign, thus
  we connect to DSN directly.
*/
ISDKODBCWrapper::ODBCState SDKODBCWrapper::Connect(const char *dataSource)
{
	SQLRETURN rc = SQL_SUCCESS;

	if (fConnectHandle != NULL)
	{
		/* You must have a DSN (Data Source Name) setup for the connection to work.
			To setup a DSN, use ODBC Administrator (in /Applications/Utilities) on the Mac, or 
			Data Sources (ODBC) control panel (Start > Control Panel > Administrative Tools) on Windows.
			You can use either a regular DSN or a FILEDSN, as long as they are the same name.  If the
			connection cannot be made with the regular DSN, we try using the FILEDSN.  This way,
			if the regular DSN plug-in does not allow entry of username and password, you can edit 
			a file dsn to manually enter those values.
			
			Warning:  If you have a FILEDSN setup with a password, then use Microsoft ODBC Administrator 
			to edit the FILEDSN, your password will be lost.  You'll have to manually edit the dsn file 
			again to get it back.
			
			MySQL settings:
					Server				IP address of server machine
					User				Admin (or valid user name that's been setup in MySQL server)
					Password			myPass (leave empty if no password, or enter your valid password)
					Database			idsdk (or name of your database)
					
			SequeLink (FileMaker) Macintosh settings (added keyword/value pairs):
					Host				localhost (or IP address of server machine)
					ServerDataSource	inventory (or name of your database)
					Port				2399
					LogonID				Admin (or a valid user that's been setup in FileMaker)
					Password			myPass (leave empty if no password, or enter your valid password)

			SequeLink (FileMaker) Windows FILEDSN contents:  (from C:\Program Files\Common Files\ODBC\Data Sources\myFMPro.dsn)
				[ODBC]
				DRIVER=DataDirect 32-BIT SequeLink 5.5
				UID=Admin
				PRT=2399
				HST=localhost
				SDSN=inventory
				PWD=myPass
		*/

		char connectStr[256] = "";//dataSource;//"DSN=idsdk;UID=root;PWD=pwd";//"DSN=";
		strcat(connectStr, dataSource);
		
		//"DSN=idsdk;UID=root;PWD=pwd"
		PMString DSX=dataSource;
		DSX.Append(",");
		DSX.Append(connectStr);
		//CAlert::InformationAlert(DSX);
				
		char fileConnectStr[256] = "FILEDSN=";
		strcat(fileConnectStr, dataSource);
		strcat(fileConnectStr, ".dsn");
				
		SQLSMALLINT outConnectBufferLen;
		SQLCHAR outdsn[1024] = {'\0'};		
	 	rc = SQLDriverConnectA( fConnectHandle, NULL, (SQLCHAR *) connectStr, SQL_NTS, (SQLCHAR *) outdsn, sizeof(outdsn), &outConnectBufferLen, SQL_DRIVER_NOPROMPT);		
		if (rc != SQL_SUCCESS && rc != SQL_SUCCESS_WITH_INFO)
		{
			rc = SQLDriverConnectA( fConnectHandle, NULL, (SQLCHAR *) fileConnectStr, SQL_NTS, (SQLCHAR *) outdsn, sizeof(outdsn), &outConnectBufferLen, SQL_DRIVER_NOPROMPT);
		}

		if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO)
		{   //Cache the connection
			fConnectionStatus = true;
			fNameConnection = dataSource;
		}
		else
		{
			//we may also come here if the connection already established. We can not assume
			//the connection is valid even if cached fConnectionStatus is true because connection may have already timed out
			//So always try to connect, then check the error code.
			//Here we assume connection is valid if re-connection is failed. More "offical" way is to call 
			//SQLGetDiagRec to check error code and sqlState.

			if (fNameConnection.compare(dataSource) == 0 && fConnectionStatus) {
				rc = SQL_SUCCESS;  
			} else {
				
				SQLCHAR sqlState[6];
				SQLINTEGER nativeError;
				SQLCHAR errorMsg[MAX_BUF_LEN];
				SQLSMALLINT  bufLen;
				SQLGetDiagRec(SQL_HANDLE_DBC, fConnectHandle, 1, sqlState, &nativeError, errorMsg, MAX_BUF_LEN, &bufLen); 
				
				PMString Erroi="A numero de error:";
				Erroi.AppendNumber(nativeError);
				Erroi.Append(" ");
				for(int i=0;i<bufLen;i++)
					Erroi.Append((SQLCHAR)errorMsg[i]);
				
				CAlert::InformationAlert( Erroi);
				fConnectionStatus = false;
			}
		}

	}

	if (rc == SQL_SUCCESS || rc == SQL_SUCCESS_WITH_INFO) {
		return ODBCNormal;
	} else if (rc < 0) {
		return ODBCError;
	} else {
		return ODBCExecutedWithError;
	}
}



ISDKODBCWrapper::ODBCState SDKODBCWrapper::PerformQuery(const char *statement)
{
	SQLRETURN rc = SQL_SUCCESS;

	// Allocate a statement handle
	if (fStmtHandle == NULL) {
		rc = SQLAllocHandle(SQL_HANDLE_STMT, fConnectHandle, &fStmtHandle);
	}

	if (rc == SQL_SUCCESS)
	{
		TRACEFLOW("SDKODBCWrapper", "MySQL query statement: %s\n", statement);
		rc = SQLExecDirectA(fStmtHandle, (SQLCHAR *)statement, SQL_NTS);
	}

	if (rc == SQL_SUCCESS) {
		return ODBCNormal;
	} else if (rc < 0) {
		return ODBCError;
	} else {
		return ODBCExecutedWithError;
	}
}


/**
 Reset URI means delete caches we have, and use new URI to re-initialized the wrapper
 */
ISDKODBCWrapper::ODBCState  SDKODBCWrapper::ResetURI(const URI uri)
{
	//If the new URI is the same one?
	if (uri == fURI) {
		return ODBCNormal;
	}
	
	//Reset status
	fConnectionStatus = false;
	fNumColumns = 0;
	fColumnNameList.clear();  // clear out cached column name list

	// Free the statement handle
	if (fStmtHandle != NULL) {
		SQLCloseCursor(fStmtHandle);  //close cursor
		SQLFreeHandle(SQL_HANDLE_STMT,fStmtHandle);
	}

	// Disconnect from database
	// We might be able to keep the connect if the database of new URI pointing to the same one, 
	// however, that would have to be done after we parse the new URI. 
	if (fConnectHandle != NULL) {
		SQLDisconnect(fConnectHandle);
	}
	
	//Forward the task to Initialize
	return this->Query(uri);

}

inline std::string& replacesubstr(std::string &originalStr, 
								const std::string &searchStr,
								const std::string &replaceStr)
{
	size_t pos = 0;
	do
	{
		pos = originalStr.find(searchStr, pos);
		// bail if we can find the searchStr anymore
		if (pos == originalStr.npos) 
			break;
		originalStr.replace(pos, searchStr.size(), replaceStr);
		// advance passed the one we just replace
		pos += replaceStr.size();
	} while (true);
	return originalStr;
}


/**
	This method will set the URI, connect to database, perform query and store the result at internal struct.
	Client will use Get methods to get field information.
*/
ISDKODBCWrapper::ODBCState  SDKODBCWrapper::Query(const URI uri, const WideString& newValue)
{
	// If unexpected URI, return error
    if ( ! uri.VerifyURI() || uri.GetComponent(URI::kScheme) != kODBCScheme ) {
		CAlert::InformationAlert("! uri.VerifyURI() || uri.GetComponent(URI::kScheme) != kODBCScheme");
		return ODBCError;
	}
	
	bool16 bDoUpdate = false;
	if (!newValue.IsNull()) {
		CAlert::InformationAlert("!newValue.IsNull()");
		
		bDoUpdate = true;
	}
	// empty local cache
	ClearRecordsBuffer();

	// ODBC query are formed similar to odbc://MySQL/?table=inventory&sku='AB-219'#sku,price,image,description
	fURI = uri;

	// Connect to database
    WideString uriDatabase( fURI.GetComponent(URI::kAuthority) );  // kAuthoity holds the name of the DSN (e.g. "MySQL")
	
	PMString CDS(uriDatabase);
	this->URIStringToStdString(uriDatabase, fDataSourceName);
	if (this->Connect(fDataSourceName.c_str()) != ODBCNormal) {
		CAlert::InformationAlert("this->Connect");
		return ODBCError;
	}
	// Parse query. Query part specifies table and query condition
    WideString uriQuery( fURI.GetComponent(URI::kQuery) );
	std::string queryComponent;
	this->URIStringToStdString(uriQuery, queryComponent);
	//CAlert::InformationAlert(queryComponent.c_str());

	typedef boost::tokenizer<boost::char_separator<char> > Tokenizer;
    boost::char_separator<char> separator("&");
    Tokenizer tok(queryComponent, separator);

	// heriberto.picazo@gmail.com
    // Parse the tokens. Each token specifies either a table like table=inventory or a where condition  sku = <value>
    Tokenizer::iterator itr = tok.begin();

	//Get tables to select, it is always the first, like table=inventory, variety
	std::string tableClause(*itr++);
	std::string tableKey("table="); 
	int pos = static_cast<int>(tableClause.find(tableKey));  
	if ( pos == std::string::npos )
	{ 
		CAlert::InformationAlert("pos == std::string::npos");
		//No table clause find, invalid query
		return ODBCError;
	}

	// Now assume this is a list of tables separated with ','. You might want to check check its syntax
	std::string tableNames = tableClause.substr(tableKey.length());

	//The next are a set of where conditions. 
	//Ideally all conditions are composed in one token, but we also allow conditions separated using & to be combined as &&
    std::vector<std::string> conditions;
    while (itr != tok.end())
    {
		std::string onePair(*itr++);
		//CAlert::InformationAlert(onePair.c_str());
		conditions.push_back(onePair);
	}

 	//Parse fragment, which is corresponding to columns in a query
	std::string fieldsStr; 
	ItemType	colType = kIntegerType;
    if (fURI.HasComponent(URI::kFragment))
	{
		WideString uriFragment( fURI.GetComponent(URI::kFragment) );
		this->URIStringToStdString(uriFragment, fieldsStr);
	}
	else
	{	// if no field are supplied, assuming select *
		fieldsStr = "*";
	}

	//Now lets form the select statement
	std::string SQLStatementSelect("SELECT ");
	SQLStatementSelect += fieldsStr + " FROM " + tableNames;
	std::string SQLStatementUpdate("UPDATE ");
	if (bDoUpdate) {
		// when do update, we should probably check if all fields are of the same types, or there will be problem.  
		// But right now, we actually only update one field from one record at a time
		SQLStatementUpdate += tableNames + " SET " + fieldsStr + "='";  // put a single quote at the start of the value - this is currently done even for numeric values
		std::string updateStr;
		StringUtils::ConvertWideStringToUTF8(newValue, updateStr);

		// change all single quotes to two single quotes
		replacesubstr(updateStr, std::string("'"), std::string("''"));
		SQLStatementUpdate += updateStr;
		SQLStatementUpdate += "'";			// put a single quote at the end of the value
	}
	
	std::vector<std::string>::iterator conItr  = conditions.begin();

	if (conItr != conditions.end()) {
		SQLStatementSelect += " WHERE ";
		if (bDoUpdate)
			SQLStatementUpdate += " WHERE ";
	}
	while ( conItr != conditions.end())
    {
		std::string oneSKU = *conItr++;
		SQLStatementSelect += oneSKU;
		if (bDoUpdate)
			SQLStatementUpdate += oneSKU;
		if ( conItr != conditions.end()) {
			SQLStatementSelect += " AND ";
		if (bDoUpdate)
			SQLStatementUpdate += " AND ";
		}
	}
	
	//CAlert::InformationAlert(SQLStatementUpdate.c_str());
	//Perform update for the statement
	if (bDoUpdate) {
		if (this->PerformQuery(SQLStatementUpdate.c_str()) != ODBCNormal) {
			CAlert::InformationAlert("this->PerformQuery");
			return ODBCError;
		}
	}
	
	//CAlert::InformationAlert(SQLStatementSelect.c_str());
	//Perform query for the statement
 	if (this->PerformQuery(SQLStatementSelect.c_str()) != ODBCNormal) {
		CAlert::InformationAlert("this->PerformQuery(SQLStatementSelect.c_str()) != ODBCNormal");
		return ODBCError;
	}
	

	// bind to the columns in the result, and update the local cache to the entire result set
	if (this->CacheRecords() != SQL_SUCCESS) {
		CAlert::InformationAlert("this->CacheRecords() != SQL_SUCCESS");
		return ODBCError;
	}

	return ODBCNormal;
}



/**
	Dynamically bind the columns
*/
SQLRETURN SDKODBCWrapper::BindSQLColumns()
{
	SQLRETURN rc = SQL_SUCCESS;

	// The following code checks the type of the field and binds columns dynamically
	SQLSMALLINT	numCols, i;
	SQLINTEGER	SQLType;

	// Determine the number of result set columns. Allocate arrays to hold 
	// the C type, byte length, and buffer offset to the data.
	rc = SQLNumResultCols(fStmtHandle, &numCols);
	fNumColumns = numCols;
	fColumnNameList.clear();  //clear out cached column name list

	std::vector<FieldInfo*> fields;		// stores a FieldInfo record for each column

	char columnName[80];
	SQLSMALLINT columnNameLength;

	for (i = 0; i < numCols; i++) 
	{
		// allocate a fieldInfo record to store the data
		FieldInfo* info = new FieldInfo();

		//Get field information
		SQLColAttribute(fStmtHandle, ((SQLUSMALLINT) i)+1, SQL_DESC_TYPE, NULL, 0, NULL, (SQLLEN*) &SQLType);
		SQLColAttribute(fStmtHandle, ((SQLUSMALLINT) i)+1, SQL_DESC_OCTET_LENGTH, NULL, 0, NULL, (SQLLEN*)&(info->bufferSize));
		SQLColAttributeA(fStmtHandle, ((SQLUSMALLINT) i)+1, SQL_DESC_BASE_COLUMN_NAME, (SQLPOINTER)columnName, sizeof(columnName), &columnNameLength, NULL);
		fColumnNameList.push_back(columnName);

	   	//Calculate SQL_C_TYPE and buffer
		switch( SQLType ) 
		{
			case SQL_CHAR :
			case SQL_VARCHAR :
				info->pBuffer = new char[info->bufferSize];
				info->cType = SQL_C_CHAR;
				break;
			case SQL_INTEGER :
				info->pBuffer = new long;
				info->cType = SQL_C_SLONG;
				break;
			case SQL_SMALLINT :
				info->pBuffer = new short;
				info->cType = SQL_C_SSHORT;
				break;
			case SQL_DOUBLE  :
				info->pBuffer = new double;
				info->cType = SQL_C_DOUBLE;
				break;
			case SQL_DATETIME: // (9)
				info->pBuffer = new SQL_TIMESTAMP_STRUCT;
				info->cType = SQL_C_TYPE_TIMESTAMP ;
				break;
			case SQL_TIMESTAMP: //11
				info->pBuffer = new SQL_TIMESTAMP_STRUCT;
				info->cType = SQL_C_TYPE_TIMESTAMP ;
				break;
			default:
				info->pBuffer = NULL;
				ASSERT_MSG(kFalse, "The field type is not supported.");
				break;
		}

		if (info->pBuffer != NULL) {
			rc = SQLBindCol(fStmtHandle, ((SQLUSMALLINT) i) + 1,	info->cType,(SQLPOINTER)(info->pBuffer), info->bufferSize, NULL);
		}

		// append the column info to the fields list
		fields.push_back(info);
	}


	// append the list of columns to the local cache
	fRows.push_back(fields);

	return rc;
}


/**
	Returns the field name for the specified column
*/
ISDKODBCWrapper::ItemType SDKODBCWrapper::GetMthColumnInfo(const int32 columnIndex, std::string &fieldName) const
{
	if (columnIndex < 0 || columnIndex >= fNumColumns || fRows.size() <= 0) {
		ASSERT_MSG(kFalse, "The column index is invalid");
		return ISDKODBCWrapper::kIntegerType;
	}

	fieldName = fColumnNameList.at(columnIndex);

	std::vector<FieldInfo*> fields = fRows.at(0);
	FieldInfo* info = fields.at(columnIndex);

	ISDKODBCWrapper::ItemType retType = ISDKODBCWrapper::kIntegerType;
	switch( info->cType ) 
	{
		case SQL_C_CHAR:
			retType = ISDKODBCWrapper::kStringType;
			break;
		case SQL_C_SLONG :
		case SQL_C_SSHORT :
			retType = ISDKODBCWrapper::kIntegerType;
			break;
		case SQL_C_DOUBLE  :
			retType = ISDKODBCWrapper::kDoubleType;
			break;
		case SQL_C_TYPE_TIMESTAMP :
			retType = ISDKODBCWrapper::kTimeStampType;
			break;
		default:
			ASSERT_MSG(kFalse, "The field type is not supported. The value is not returned");
			break;
	}

	return retType;
}


/**
	This method traverses the result set using a forward-only cursor.  For every record in the set,
	the data therein is stored in the local cache, fRows.
	
	This method requires more memory, but provides very fast access while also allowing random access to the 
	result set.
*/
SQLRETURN SDKODBCWrapper::CacheRecords()
{
	SQLRETURN rc = SQL_SUCCESS;

	while (rc == SQL_SUCCESS) 
	{
		// Create new buffers for storing the result of the next fetch
		rc = BindSQLColumns();	
		if (rc == SQL_SUCCESS) 
		{
			// perform the fetch.  This will put data into the fRows cache buffers (via SQL binding)
			rc = SQLFetch(fStmtHandle);	
			if (rc != SQL_SUCCESS && rc != SQL_SUCCESS_WITH_INFO) 
			{
				rc = SQL_SUCCESS;	// done getting records

				// since nothing was found during the last fetch, clean up the last buffer stored in fRows - it's empty
				std::vector<FieldInfo*> fields = fRows.at(fRows.size() - 1);
				std::vector<FieldInfo*>::iterator fIter;
				for (fIter = fields.begin(); fIter != fields.end(); fIter++) {
					delete *fIter;
					*fIter = NULL;
				}
				fields.clear();	// clear the columns for this row
				fRows.erase(fRows.end()-1);

				break;
			}
		}
	}

	return rc;
}


 
// Get a record (a vector of field values)
K2Vector<WideString> SDKODBCWrapper::GetNthRecord(const int32 recordIndex, ODBCState *error) const
{
	K2Vector<WideString> retVec;

	if ( recordIndex < 0 || recordIndex >= fRows.size())
	{
		if (error) {
			*error = ISDKODBCWrapper::ODBCError;
		}
		return retVec;
	}
	
	// Walk through the result row fields, appending the data as we go
	std::vector<FieldInfo*> fields = fRows.at(recordIndex);
	std::vector<FieldInfo*>::iterator iter;
	for (iter = fields.begin(); iter != fields.end(); iter++)
	{
		FieldInfo* info = *iter;
		WideString theBuffer;
		info->GetBuffer(theBuffer);
		retVec.push_back(theBuffer);
	}
	
	if (error) {
		*error = ISDKODBCWrapper::ODBCNormal;
	}
	return retVec;
}



// Get one value
WideString SDKODBCWrapper::GetNthRecordMthColumn(const int32 recordIndex, const int32 columnIndex, ODBCState *error) const
{
	SQLRETURN rc = SQL_SUCCESS;
	WideString retStr;

	if (columnIndex < 0 || columnIndex >= fNumColumns || recordIndex < 0 || recordIndex >= fRows.size())
	{
		if (error) {
			*error = ISDKODBCWrapper::ODBCError;
		}
		return retStr;
	}

	// pull the requested data from the local cache
	std::vector<FieldInfo*> fields = fRows.at(recordIndex);
	FieldInfo* info = fields.at(columnIndex);
	info->GetBuffer(retStr);
			
	if (error) {
		*error = ISDKODBCWrapper::ODBCNormal;
	}

	PMString dbgStr(retStr);
	TRACEFLOW("SDKODBCWrapper", "MySQL queryresult: %s\n", dbgStr.GetPlatformString().c_str());

	return retStr; 
}


WideString SDKODBCWrapper::GetColumnOfNthRecordWithName(const int32 recordIndex, const std::string columnName, ODBCState  *error) const
{
	//Find the index
	std::vector<std::string>::const_iterator colItr  = std::find(fColumnNameList.begin(), fColumnNameList.end(), columnName);

	int32 columnIndex = colItr - fColumnNameList.begin();

	//get the value
	return this->GetNthRecordMthColumn(recordIndex,columnIndex,error);
}


/**********************************************************************************************
** Private functions
**
**	 WARNING: DO NOT USE THIS METHOD FOR GENERAL WIDESTRING PROCESSSING
**
***********************************************************************************************/
void SDKODBCWrapper::URIStringToStdString(const WideString& wideStr, std::string& stdStr) const
{
    // this method assumes the given WideString is in URI encoding
    // if not, an empty std::string is returned
    int32 length;
    ConstWString utf16Buffer = wideStr.GrabUTF16Buffer(&length);

    stdStr.clear();
    stdStr.reserve(length);

    uint32 utf16;

    for (uint32 i = 0; i < length; ++i) {
        utf16 = utf16Buffer[i];

        if (utf16 > 127) {
            ASSERT_FAIL("URIImpl::URIStringToStdString() - WideString is not in URI encoding!");
            stdStr.clear();
            return;
        }

        stdStr += static_cast<char>(utf16);
    }
}

