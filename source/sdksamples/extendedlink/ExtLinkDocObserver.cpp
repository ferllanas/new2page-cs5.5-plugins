//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkDocObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

//===========================
//  Plug-in includes
//===========================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IDocument.h"
#include "IHierarchy.h"
#include "ISubject.h"
#include "IXMLReferenceListData.h"
#include "IXMLElementCommands.h"
#include "XMLReference.h"
#include "IXMLUtils.h"

// API includes:
#include "CAlert.h"
#include "CmdUtils.h"
#include "CObserver.h"
#include "PageItemScrapID.h" // for kDeletePageItemCmdBoss
#include "StandOffID.h" // for kDeleteStandOffItemCmdBoss and kDeleteInsetPageItemCmdBoss
#include "UIDList.h"

// Project includes:
#include "ExtLinkID.h"


/** This observer observes the front document's IID_IIDXMLELEMENT protocol, specifically, changed by kXMLUnplaceElementCmdBoss.  
	Here is a little background about why this observer is needed:
	One of the XML workflows is that you have an untagged layout template in the document. Then you import your data from an XML 
	file. Once it's imported, all the data are stored in the backing store. Let's say then you drag an element to a text frame 
	in the layout. This is called placing an element. When you delete the tagged text frame, the idea is that you don't want the 
	data to be shown in the layout but you still want to keep it. Maybe later you want to place it to a different text frame. So 
	the contents of the story is not deleted. Instead it's moved to the backing store.  Since an XML link is associated to an XML 
	element not the story, unplace doesn't delete the link. The only way to delete the link is to delete the element.
	
	For the purpose of ExtLink, the xml element created is nothing but a way to help keeping track of links on a text range, this 
	way we can create a link on a range of text with very minimum effort, and being able to create link on text range instead of 
	text frame is the main point this sample trying to illustrate.  So when a frame that contains ExtLink link is deleted, we want 
	to delete the link plus the element associated with it.  In order to do that, we observe the  the front document's 
	IID_IIDXMLELEMENT protocol, specifically, changed by kXMLUnplaceElementCmdBoss here in this observer.

	This observer gets attached with the ISubject of kDocBoss when a document is opened or created, through the use of a open/new 
	document responder, and this observer is detached when a document is closed, through the use of a close document responder.
  
	@ingroup extendedlink
	@see ExtLinkDocResponder
*/
class ExtLinkDocObserver : public CObserver
{
public:
	/**	Constructor.
		@param boss IN interface ptr from boss object on which this interface is aggregated.
	*/
	ExtLinkDocObserver(IPMUnknown* boss);

	/**	Destructor.
	*/
	virtual ~ExtLinkDocObserver();

	/** Called by the application to allow the observer to attach to the 
	 * 	subjects to be observed (kDocBoss)
	*/
	void AutoAttach();

	/** Called by the application to allow the observer to detach from the 
	 * 	subjects being observed.
	*/
	void AutoDetach();

	/** Update is called for all registered observers, and is the method 
	 * 	through which changes are broadcast. 
	 * 	In this case, the only things we are interested in are if the drop 
	 * 	down list selection has changed and if a page item is deleted from 
	 * 	the document. 
	 * 
	 * 	@param theChange IN specifies the class ID of the change to the subject. 
	 * 		Frequently this is a command ID.
	 * 	@param theSubject IN points to the ISubject interface for the subject 
	 * 		that has changed.
	 * 	@param protocol IN specifies the ID of the changed interface on the 
	 * 		subject boss.
	 * 	@param changedBy IN points to additional data about the change. 
	 * 		Often this pointer indicates the class ID of the command 
	 * 		that has caused the change.
	*/
	virtual void Update(const ClassID& theChange, 
						ISubject* theSubject, 
						const PMIID& protocol, 
						void* changedBy);

protected:
	/**	Attaches this observer to a document.
	 * 	@param iDocument IN The document to which we want to attach.
	*/
	void AttachDocument(IDocument* iDocument);

	/**	Detaches this observer from a document.
	 * 	@param iDocument IN The document from which we want to detach.
	*/
	void DetachDocument(IDocument* iDocument);

	/**	The XML element is unplaced, we want to delete the element and its associated link
	 * 	@param changedBy IN points to additional data about the change. 
	 *	@see ExtLinkDocObserver::Update
	*/
	void HandleXMLElementUnplaced(void* changedBy);

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(ExtLinkDocObserver, kExtLinkDocObserverImpl)

/* ExtLinkDocObserver Constructor
*/
ExtLinkDocObserver::ExtLinkDocObserver(IPMUnknown* boss) 
	: CObserver(boss, IID_IEXTLINKOBSERVER)
{
}

/* ExtLinkDocObserver Destructor
*/
ExtLinkDocObserver::~ExtLinkDocObserver()	
{
}


/*	ExtLinkDocObserver::AutoAttach
*/
void ExtLinkDocObserver::AutoAttach()
{
	CObserver::AutoAttach();

	InterfacePtr<IDocument> iDocument(this, UseDefaultIID());
	if (iDocument != nil)
		this->AttachDocument(iDocument);
}


/*	ExtLinkDocObserver::AutoDetach
*/
void ExtLinkDocObserver::AutoDetach()
{
	CObserver::AutoDetach();

	InterfacePtr<IDocument> iDocument(this, UseDefaultIID());
	if (iDocument != nil)
		this->DetachDocument(iDocument);
}

/*	ExtLinkDocObserver::Update
*/
void ExtLinkDocObserver::Update(const ClassID& theChange, 
							   ISubject* theSubject, 
							   const PMIID& protocol, 
							   void* changedBy)
{
	do
	{
		ICommand* iCommand = (ICommand*)changedBy;

		if (protocol == IID_IIDXMLELEMENT)
		{
			if (theChange == kXMLUnplaceElementCmdBoss)			
			{
				HandleXMLElementUnplaced(changedBy);
			}
		}

	} while (kFalse);
}

/*	ExtLinkDocObserver::HandleXMLElementUnplaced
*/
void ExtLinkDocObserver::HandleXMLElementUnplaced(void* changedBy)
{
	do
	{
		ICommand* command = (ICommand*)changedBy;
		bool16 isCommandDone = command->GetCommandState() == ICommand::kDone;
		if (isCommandDone)
		{
			InterfacePtr<IXMLReferenceListData> unplacedElements(command, UseDefaultIID());
			int32 size = unplacedElements->Size();
			for (int32 index = 0; index < size; index++)
			{
				XMLReference xmlRef = unplacedElements->Get(index);
				Utils<IXMLElementCommands>()->DeleteElementAndContent(xmlRef, kTrue);
			}
		}
	} while (false);
}

/*	ExtLinkDocObserver::AttachDocument
*/
void ExtLinkDocObserver::AttachDocument(IDocument* iDocument)
{
	do
	{
		if (iDocument == nil)
		{
			ASSERT_FAIL("no document to attach to");
			break;
		}

		InterfacePtr<ISubject> backingStoreSubject(Utils<IXMLUtils>()->GetBackingStore(iDocument), UseDefaultIID());
		if ( backingStoreSubject && !backingStoreSubject->IsAttached(this,IID_IIDXMLELEMENT,IID_IEXTLINKOBSERVER) )
			backingStoreSubject->AttachObserver(ISubject::kRegularAttachment, this, IID_IIDXMLELEMENT, IID_IEXTLINKOBSERVER  );
	} while (kFalse);
}


/*	ExtLinkDocObserver::DetachDocument
*/
void ExtLinkDocObserver::DetachDocument(IDocument* iDocument)
{
	do
	{
		if (iDocument == nil)
		{
			ASSERT_FAIL("no document to attach to");
			break;
		}

		InterfacePtr<ISubject> backingStoreSubject(Utils<IXMLUtils>()->GetBackingStore(iDocument), UseDefaultIID());
		if ( backingStoreSubject && backingStoreSubject->IsAttached(this,IID_IIDXMLELEMENT,IID_IEXTLINKOBSERVER) )
			backingStoreSubject->DetachObserver(ISubject::kRegularAttachment, this, IID_IIDXMLELEMENT, IID_IEXTLINKOBSERVER  );
	} while (kFalse);
}


// End, ExtLinkDocObserver.cpp.

