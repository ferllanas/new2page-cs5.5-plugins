//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"

# include"IDataBase.h"				
# include"IDocument.h"
# include"ISelectionUtils.h"
# include"ISelectionManager.h"
# include"IConcreteSelection.h"
# include"ITextTarget.h"
# include"ITextFocus.h"
# include"ITextModel.h"
# include"IFrameList.h"	
# include"ITextFrameColumn.h"				
# include"IMultiColumnTextFrame.h"
# include"ILayoutUIUtils.h"
# include"TextEditorID.h"

//#include "FrmLblDataFacade.h"
#include "SDKLayoutHelper.h"

// Project includes:
#include "N2PAdIssueID.h"
#include "IN2PAdIssueUtils.h"
# include"IFrmLblData.h"

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup n2padornmentissue

*/
class N2PAdIssueActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		N2PAdIssueActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);

	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		
		/** Opens this plug-in's dialog. */
		void DoDialog();
		
		
		void TestDAdornos();

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PAdIssueActionComponent, kN2PAdIssueActionComponentImpl)

/* N2PAdIssueActionComponent Constructor
*/
N2PAdIssueActionComponent::N2PAdIssueActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void N2PAdIssueActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kN2PAdIssuePopupAboutThisActionID:
		case kN2PAdIssueAboutActionID:
		{
			this->DoAbout();
			break;
		}
					

		case kN2PAdIssueDialogActionID:
		{
			this->TestDAdornos();
			//this->DoDialog();
			break;
		}


		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void N2PAdIssueActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kN2PAdIssueAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}



/* DoDialog
*/
void N2PAdIssueActionComponent::DoDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PAdIssuePluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}


void N2PAdIssueActionComponent::TestDAdornos()
{
do{
		
		UID UIDTextModel = kInvalidUID;
		
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}
		
		
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionUtils pointer nil");
			break;
		}
		
		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionManager pointer nil");
			break;
		}
		
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel!=nil)
		{
			InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
			if(pTextTarget==nil)
			{
				CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect pTextTarget pointer nil");
				break;
			}
			
			InterfacePtr<ITextFocus> pFocus(pTextTarget->QueryTextFocus());
			if(pFocus!=nil)
			{
				InterfacePtr<ITextModel> iTextModel(pFocus->QueryModel());
				if(iTextModel==nil)
				{
					CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect iTextModel pointer nil");
					break;
				}
				
				InterfacePtr<IFrameList> frameList(iTextModel->QueryFrameList());
				if(!frameList)
				{
					CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect frameList pointer nil");
					break;
				}
				
				UIDTextModel = frameList->GetTextModelUID();
				InterfacePtr<ITextFrameColumn> TextFrameColumn(frameList->QueryNthFrame(0));
				if(!TextFrameColumn)
				{
					CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect frameList pointer nil");
					break;
				}
				
				SDKLayoutHelper help;
				UIDRef RefFram4e=help.GetGraphicFrameRef(TextFrameColumn);

				InterfacePtr<IMultiColumnTextFrame> MultiColumnText(TextFrameColumn->QueryMultiColumnTextFrame());
				if(!MultiColumnText)
				{
					CAlert::InformationAlert(" N2PSQLUtilities::GetUIDOfTextFrameSelect frameList pointer nil");
					break;
				}
				
				UIDList myList=UIDList(RefFram4e);
				
				int32 item = myList.Length();
				while (item-- > 0)
				{
					InterfacePtr<IFrmLblData> tempData(myList.GetRef(item), UseDefaultIID());
					if (tempData == nil)
					{
						myList.Remove(item);
					}
				}
				
				// If there are no items left in the list, then report an error and leave.
				if (myList.Length() == 0)
				{
					
						CAlert::WarningAlert("valio madres");
					break;
				}
				else
				{
				InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
				(
					kN2PAdIssueUtilsBoss,	// Object boss/class
					IN2PAdIssueUtils::kDefaultIID
				)));
		
				if(N2PAdIssueUtil==nil)
				{
					break;
				}
			
				//UIDList ListFrame=UIDList(sourceFrameRef);
		
				ErrorCode error = N2PAdIssueUtil->UpdateUIDListLabelfor( myList,"SAS", kTrue);
					/*for (int32 i = 0 ; i < myList.Length() ; i++)
					{
					
						CAlert::InformationAlert("Si jalo we veamos si se mantiene con las adicones");
						UIDRef itemRef = myList.GetRef(i);
						
						
						ErrorCode error = FrmLblDataFacade::UpdateFrameLabelProperties(itemRef, 
																			 "MAMASita",
																			 20, 
																			 kTrue);
						if (error != kSuccess)
						{
							ASSERT_FAIL(FORMAT_ARGS("FrmLblDataSuiteCSB::UpdateFrameLabelProperties - FrmLblDataFacade::UpdateFrameLabelProperties returned %d on item %d (0x%X)", 
													error, i, itemRef.GetUID().Get()));
							break; // for loop
						}
					}*/
				}
				
			}
			
			
			
			
		}
		
		//UIDTexModelOfMyFrame=UIDTextModel.Get();
		
	
	}while(false);
}

