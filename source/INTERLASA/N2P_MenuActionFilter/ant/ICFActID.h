//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/incopyfileactions/ICFActID.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #9 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//  
//  Defines IDs used by the ICFAct plug-in.
//  
//========================================================================================

#ifndef __ICFActID_h__
#define __ICFActID_h__

#define kInCopyFileActionsPrefix	0x65200 // Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).

// Company:
#define kICFActCompanyKey	"SDK"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kICFActCompanyValue	"SDK"	// Company name displayed externally.

// Plug-in:
#define kICFActPluginName	"InCopyFileActions"				// Name of this plug-in.
#define kICFActVersion		"1064"							// Version of this plug-in (for the About Box).
#define kICFActAuthor		"Rodney Cook"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kICFActPrefixNumber above to modify the prefix.)
#define kICFActPrefix		RezLong(kInCopyFileActionsPrefix)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kICFActStringPrefix	"0x65200"	// The string equivalent of the unique prefix number for  this plug-in.

// Don't add to this file.  Add to InCopyFileActionsID.h instead.

#endif // __ICFActID_h__
// End, ICFActID.h.




