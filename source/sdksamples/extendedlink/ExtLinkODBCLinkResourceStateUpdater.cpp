//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkODBCLinkResourceStateUpdater.cpp $
//  
//  Owner: Michael Easter
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "CPMUnknown.h"
#include "ILinkResourceStateUpdater.h"

#include "IIdleTask.h"
#include "ILinkResourceStateUpdateCmdData.h"

//#include "SDKODBCCache.h"
#include "Utils.h"
#include "IExtLinkFacade.h"
#include "IExtLinkStateUpdateProcessor.h"
#include "ExtLinkODBCLinkResourceStatePacket.h"
#include "ExtLinkID.h"
#include <vector>

//========================================================================================
// Class ExtLinkODBCLinkResourceStateUpdater
//========================================================================================
/**
	Provide state update specifically to link resource whose URI scheme matches the one specified in 
	ExtLinkODBCLinkResourceFactory, i.e., 'odbc'.  Link manager will call ILinkResourceStateUpdater
	when necessary, and it may request a synchronous or asynchronous update.  The main difference is
	you have to make sure synchronous update is done upon return to the caller (link manager).
	
	ILinkResourceStateUpdater will be given a link resource's UID when it is called to update its state.
	Typically, you "package" whatever information (probably most likely from the link resource) is pertinent 
	to your state update in a AsyncWorkPacket, and then you enque the packet with IExtLinkStateUpdateProcessor.  
	Then an idle task ExtLinkODBCLinkResourceStateUpdaterIdleTask will process the packet (i.e. provide the 
	latest state information) when your packet's turn is up in the queue.
	
	@ingroup extendedlink
	@see ExtLinkODBCLinkResourceFactory
	@see ExtLinkODBCLinkResourceStatePacket
*/
class ExtLinkODBCLinkResourceStateUpdater : public CPMUnknown<ILinkResourceStateUpdater>
{
public:
	typedef CPMUnknown<ILinkResourceStateUpdater> inherited;
	typedef object_type data_type;

	ExtLinkODBCLinkResourceStateUpdater(IPMUnknown* boss);
	virtual ~ExtLinkODBCLinkResourceStateUpdater();

	/**	
		Delegate to UpdateResourceState
		@see ILinkResourceHandler::UpdateResourceStateSync
	 */
	virtual ErrorCode UpdateResourceStateSync(const UIDRef& resourceRef, bool bNotify);
	/**	
		Delegate to UpdateResourceState
		@see ILinkResourceHandler::UpdateResourceStateAsync
	 */
	virtual ErrorCode UpdateResourceStateAsync(const UIDRef& resourceRef);

	/**	
		When you enque an AsyncWorkPacket, you supply an AsyncWorkPacketJobSpec that can identify
		your packet.  When link manager needs to cancel the resource update state request for whatever reason, 
		we search all outstanding AsyncWorkPackets for matches with the AsyncWorkPacketJobSpec when we enqued it.
		Then we cancel the job.
		
		@see ILinkResourceHandler::CancelUpdateResourceState
	 */
	virtual void CancelUpdateResourceState(const UIDRef& resourceRef);

	/**	@see ILinkResourceHandler::ResolveResource
	 */
	virtual ErrorCode ResolveResource(const UIDRef& resourceRef, const URI& relativeURI, bool bIgnoreStamp);

private:
	// Prevent copy construction and assignment.
	ExtLinkODBCLinkResourceStateUpdater(const ExtLinkODBCLinkResourceStateUpdater&);
	ExtLinkODBCLinkResourceStateUpdater& operator=(const ExtLinkODBCLinkResourceStateUpdater&);

	/**
		During a synchronous update, we check if there is an outstanding packet for the same resource already in the queue.  
		If we found one, we cancel the packet's job request.
		During an asynchronous update, if we found one packet already queued, we simply return to the caller to allow the 
		pending packet to complete processing asynchronously.
		Then we enqueue an ExtLinkODBCLinkResourceStatePacket with the information from link resource (resourceRef) that's
		passed into this method.
		@param resourceUID	 [IN] UIDRef of the link resource to resolve.
		@param bNotify		 [IN] Denotes whether to notify when the resource's state is updated.
		@param bSynchronous	 [IN] Denotes whether the update job should be done synchronously or not.
		@return kSuccess if the link resource is successfully resolved, else an error code.
	*/
	ErrorCode UpdateResourceState(const UIDRef& resourceRef, bool bNotify, bool bSynchronous);
};

CREATE_PMINTERFACE(ExtLinkODBCLinkResourceStateUpdater, kExtLinkODBCLinkResourceStateUpdaterImpl)


//========================================================================================
//
// ExtLinkODBCLinkResourceStateUpdater Public Implementation
//
//========================================================================================

//========================================================================================
// Constructor
//========================================================================================
ExtLinkODBCLinkResourceStateUpdater::ExtLinkODBCLinkResourceStateUpdater(IPMUnknown* boss)
: inherited(boss)
{
}

//========================================================================================
// Destructor
//========================================================================================
ExtLinkODBCLinkResourceStateUpdater::~ExtLinkODBCLinkResourceStateUpdater()
{
}

//========================================================================================
// ExtLinkODBCLinkResourceStateUpdater::UpdateResourceStateSync
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceStateUpdater::UpdateResourceStateSync(const UIDRef& resourceRef, bool bNotify)
{
	return UpdateResourceState(resourceRef, bNotify, true);
}

//========================================================================================
// ExtLinkODBCLinkResourceStateUpdater::UpdateResourceStateAsync
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceStateUpdater::UpdateResourceStateAsync(const UIDRef& resourceRef)
{
	return UpdateResourceState(resourceRef, true, false);
}

//========================================================================================
// ExtLinkODBCLinkResourceStateUpdater::CancelUpdateResourceState
//========================================================================================
void ExtLinkODBCLinkResourceStateUpdater::CancelUpdateResourceState(const UIDRef& resourceRef)
{
	IDataBase* db = resourceRef.GetDataBase();
	InterfacePtr<ILinkManager> iLinkMgr(db, db->GetRootUID(), UseDefaultIID());
	if (!iLinkMgr) {
		ASSERT_FAIL("ExtLinkODBCLinkResourceStateUpdater::CancelUpdateResourceState() - ILinkManager is nil!");
		return;
	}
	
	UID resourceUID = resourceRef.GetUID();
	
	// check to make sure we have a valid resource to queue
	InterfacePtr<ILinkResource> iResource(iLinkMgr->QueryResourceByUID(resourceUID));
	if (!iResource) {
		ASSERT_FAIL("ExtLinkODBCLinkResourceStateUpdater::CancelUpdateResourceState() - ILinkResource is nil!");
		return;
	}
	
	ClassID providerId = iLinkMgr->GetResourceProviderByScheme(iResource->GetURI().GetComponent(URI::kScheme));
	if (!providerId.IsValid()) {
		ASSERT_FAIL("ExtLinkODBCLinkResourceStateUpdater::CancelUpdateResourceState() - resource provider id is invalid!");
		return;
	}
	
	ExtLinkODBCLinkResourceStatePacket job(ILinkManager::kAsynchronous,
										   false,
										   db,
										   resourceUID,
										   iResource->GetURI(),
										   iResource->GetState(),
										   iResource->GetStamp());
	
	// first check to see if a job is already pending
	InterfacePtr<IExtLinkStateUpdateProcessor> stateUpdateProcessor(GetExecutionContextSession(), UseDefaultIID());
	if (stateUpdateProcessor) {
		if (stateUpdateProcessor->IsJobPending(job))
			stateUpdateProcessor->RemoveJob(job);
	}
}

//========================================================================================
// ExtLinkODBCLinkResourceStateUpdater::ResolveResource
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceStateUpdater::ResolveResource(const UIDRef& resourceRef, const URI& relativeURI, bool bIgnoreStamp)
{
	#pragma unused(resourceRef, relativeUIR, bIgnoreStamp)
	return kFailure;
}

//========================================================================================
//
// ExtLinkODBCLinkResourceStateUpdater Private Implementation
//
//========================================================================================

//========================================================================================
// ExtLinkODBCLinkResourceStateUpdater::UpdateResourceState
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceStateUpdater::UpdateResourceState(const UIDRef& resourceRef, bool bNotify, bool bSynchronous)
{
	IDataBase* db = resourceRef.GetDataBase();
	InterfacePtr<ILinkManager> iLinkMgr(db, db->GetRootUID(), UseDefaultIID());
	if (!iLinkMgr) {
		ASSERT_FAIL("ExtLinkODBCLinkResourceStateUpdater::UpdateResourceState() - ILinkManager is nil!");
		return kFailure;
	}

	UID resourceUID = resourceRef.GetUID();
	
	// check to make sure we have a valid resource to queue
	InterfacePtr<ILinkResource> iResource(iLinkMgr->QueryResourceByUID(resourceUID));
	if (!iResource) {
		ASSERT_FAIL("ExtLinkODBCLinkResourceStateUpdater::UpdateResourceState() - ILinkResource is nil!");
		return kFailure;
	}
	
	ClassID providerId = iLinkMgr->GetResourceProviderByScheme(iResource->GetURI().GetComponent(URI::kScheme));
	if (!providerId.IsValid()) {
		ASSERT_FAIL("ExtLinkODBCLinkResourceStateUpdater::UpdateResourceState() - resource provider id is invalid!");
		return kFailure;
	}
	InterfacePtr<IExtLinkStateUpdateProcessor> stateUpdateProcessor(GetExecutionContextSession(), UseDefaultIID());
	if (!stateUpdateProcessor) {
		ASSERT_FAIL("ExtLinkODBCLinkResourceStateUpdater::UpdateResourceState() - stateUpdateProcessor is nil!");
		return kFailure;
	}
	
	ExtLinkODBCLinkResourceStatePacket job(bSynchronous ? ILinkManager::kSynchronous : ILinkManager::kAsynchronous,
											  bNotify,
											  db,
											  resourceUID,
											  iResource->GetURI(),
											  iResource->GetState(),
											  iResource->GetStamp());
	
	// first check to see if a job is already pending
	if (stateUpdateProcessor->IsJobPending(job)) {
		if (bSynchronous)
			// cancel the pending packet, so we can process it synchronously
			stateUpdateProcessor->RemoveJob(job);
		else
			// allow the pending packet to complete processing asynchronously
			return kSuccess;
	}
	
	stateUpdateProcessor->AddJob(job);

	if (bSynchronous) {
		if (stateUpdateProcessor) {
			stateUpdateProcessor->UpdateStateForJob(job);
		}
	}

	return kSuccess;
}