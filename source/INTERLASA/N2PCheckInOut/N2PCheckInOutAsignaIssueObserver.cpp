/*
//	File:	CheckInOutAsignaIssueObserver.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "IApplication.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IListBoxController.h"


// General includes:
#include "CAlert.h"
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "ILayoutUIUtils.h"

#include "N2PCheckInOutListBoxHelper.h"
#include "Utils.h"

// Project includes:
#include "N2PCheckInOutID.h"
#ifdef MACINTOSH
	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif

/** CheckInOutAsignaIssueObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author Juan Fernando Llanas Rdz
*/
class CheckInOutAsignaIssueObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CheckInOutAsignaIssueObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~CheckInOutAsignaIssueObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

	private:
		
		PMString BuscarEnUsuarios(PMString Busqueda, PMString Campo);
		
		void LLenar_Combo_Seccion();
		
		K2Vector<PMString> VectorIdSeccion;
		
		int32 GetIdexOfStringOnComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);
	
		bool16 agregarIssuesAndSection();
	
		bool16 GetTextOfComboOnDialog(	const WidgetID& Combowidget, PMString& itemToShow, int32& indexSelected);
	
		bool16 LlenarListaDeIssueDePagina();
	
		bool16 ObtenerFolioDePaginaSeleccionada(PMString& Id_PaginaSel, PMString& Seccion);	
	
		void removeIssueDePagina();



};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(CheckInOutAsignaIssueObserver, kN2PCheckInOutAsignaIssueObserverImpl)

/* AutoAttach
*/
void CheckInOutAsignaIssueObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		AttachToWidget(kAddButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kComboBoxIssueWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		AttachToWidget(kN2PCkRemoveButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);

		// Now attach to N2PCheckInOut's info button widget.
		//AttachToWidget(kCheckInOutIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//AttachToWidget(kBuscarButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void CheckInOutAsignaIssueObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::AutoDetach() panelControlData invalid");
			break;
		}
			
		DetachFromWidget(kComboBoxIssueWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		DetachFromWidget(kAddButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kN2PCkRemoveButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);

		// Now we detach from N2PCheckInOut's info button widget.
		//DetachFromWidget(kCheckInOutIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		
		//DetachFromWidget(kBuscarButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void CheckInOutAsignaIssueObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		
		if (theSelectedWidget == kComboBoxIssueWidgetID && theChange == kPopupChangeStateMessage)
		{
			this->LLenar_Combo_Seccion();
		}
		
		
		if (theSelectedWidget == kAddButtonWidgetID && theChange == kTrueStateMessage)
		{
			agregarIssuesAndSection();
		}
		
		if (theSelectedWidget == kN2PCkRemoveButtonWidgetID && theChange == kTrueStateMessage)
		{
			removeIssueDePagina();
		}
		

	} while (false);
}


PMString CheckInOutAsignaIssueObserver::BuscarEnUsuarios(PMString Busqueda, PMString Campo)
{

	PMString cadena;

	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn(Campo,QueryVector[i]);
			cadena.SetTranslatable(kFalse);
		}
	}while(false);
	
	
	return(cadena);
}

//  Generated by Dolly build 17: template "Dialog".
// End, CheckInOutAsignaIssueObserver.cpp.

void CheckInOutAsignaIssueObserver::LLenar_Combo_Seccion()
{
	do
	{
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::LLenar_Combo_SeccionNo pudo Obtener panelControlData");
			break;
		}
		
		IControlView * ComboCViewPubli=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if (ComboCViewPubli == nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::LLenar_Combo_SeccionComboCViewPubli*");
			break;
		}
		
		InterfacePtr<ITextControlData> TextControlPubli(ComboCViewPubli,ITextControlData::kDefaultIID);
		if (TextControlPubli == nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::LLenar_Combo_SeccionTextControlPubli*");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::LLenar_Combo_Seccion ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::LLenar_Combo_SecciondropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		PMString Busqueda="SELECT   Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion = (SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + TextControlPubli->GetString() + "' ) ORDER BY Nombre_de_Seccion ASC";
		///borrado de la lista al inicializar el combo
		
	
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("CheckInOutAsignaIssueObserver::LLenar_Combo_Seccion IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		//PMString DSNName= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		VectorIdSeccion = QueryVector;
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}

		
		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

	}while(false);
}

int32 CheckInOutAsignaIssueObserver::GetIdexOfStringOnComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
		
	}while(false);
	return(retvalIndex);
}


bool16 CheckInOutAsignaIssueObserver::agregarIssuesAndSection()
{
	do{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		PMString IDUsuario_Actual="";
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in CheckInOutAsignaIssueObserver::Update()");
			break;
		}
		IDUsuario_Actual= wrkSpcPrefs->GetIDUserLogedString();
		
		
		PMString Issue="";
		PMString Seccion="";
		int32 indexSelected=0;
		
		GetTextOfComboOnDialog(	kComboBoxIssueWidgetID, Issue, indexSelected);
		if(Issue.NumUTF16TextChars()<=0)
			break;
		GetTextOfComboOnDialog(	kComboBoxSeccionWidgetID, Seccion, indexSelected);
		
		
		////////////////////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		//PMString DSNName= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		
		PMString SPString("CALL SP_addIssueAndSecctionToPage('"+Issue+"','"+Seccion+"',"+IDPaginaENXMP+",'"+IDUsuario_Actual+"');");
		K2Vector<PMString> QueryVector;
		
		PMString numerror="";
		PMString stringOUT="";
		PMString idsalida="";
		
		if(SQLInterface->SQLQueryDataBase(StringConection,SPString,QueryVector))
		{
				
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				idsalida=SQLInterface->ReturnItemContentbyNameColumn("idsalida",QueryVector[i]);
				stringOUT=SQLInterface->ReturnItemContentbyNameColumn("stringOUT",QueryVector[i]);
				idsalida=SQLInterface->ReturnItemContentbyNameColumn("idsalida",QueryVector[i]);
			}
		}
		
		if(numerror.GetAsNumber()>0)
		{
			CAlert::InformationAlert(stringOUT);
		}
		else
		{
			LlenarListaDeIssueDePagina();
		}

	}while (false);
	return kFalse;
}

bool16 CheckInOutAsignaIssueObserver::LlenarListaDeIssueDePagina()
{
	do{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if (panel == nil)
		{
			
			break;
		}		
		
		
		IControlView *iControViewList = panel->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		if(iControViewList==nil)
		{
			CAlert::InformationAlert("ControlView");
			break;
		}
		
		

		
		SDKListBoxHelper listHelper(iControViewList, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		listHelper.EmptyCurrentListBox();	
		
		
		PMString Busqueda="SELECT a.id as id, p.Nombre_Publicacion as publicacion, s.Nombre_de_Seccion as seccion ";
		Busqueda.Append(	  "	FROM edicionesdpagina a LEFT JOIN publicacion p ON p.Id_Publicacion=publicacion_idpublicacion");
		Busqueda.Append(	  " LEFT JOIN seccion s ON s.Id_Seccion=a.seccion_id_seccion WHERE pagina_id="+IDPaginaENXMP+ " AND a.estatus<90 ");
		
		//CAlert::InformationAlert(Busqueda);
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
		K2Vector<PMString> QueryVector;
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
		if(SQLInterface==nil)
		{
			ASSERT_FAIL("SQLInterface = nil");
			break;
		}
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		if(QueryVector.Length()<=0)
		{
			PMString NoNotas=PMString(kN2PCheckInOutNoSeEncontraronPaginasPlugInMenuKey);
			NoNotas.Translate();
			NoNotas.SetTranslatable(kTrue);
			CAlert::InformationAlert(NoNotas);
			break;
		}
		
		// cuando regresa solo un registro
		if(QueryVector.Length()==1)
		{
			
			//CAlert::InformationAlert(QueryVector[0]);
			PMString Privilegio = SQLInterface->ReturnItemContentbyNameColumn("Privilegio",QueryVector[0]);
			if(Privilegio.NumUTF16TextChars()>0)//Pregunta el rtegitro trae el campo con el nombre 'Privilegio'
			{
				//entonces no tiene privilegios para ver las paginas de esta seccion
				CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
			}
			else
			{// si no entonces es una regirtro con datos de una pagina
				for(int32 i=0;i<QueryVector.Length();i++)
				{
					PMString Nombre_Archivo = SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryVector[i]);
					PMString Secc = SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryVector[i]);
					PMString Estado="";// SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
					PMString Folio_Pagina = SQLInterface->ReturnItemContentbyNameColumn("id",QueryVector[i]);
					PMString Id_SeccionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
					PMString Id_PublicacionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
					listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
				}
			}
			
			//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogObserver::Update call(despues del llenado de lista de paginas)");
			
		}
		else
		{//si no entonces trae muchas registeros de paginas.
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				PMString Nombre_Archivo=SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryVector[i]);
				PMString Secc=SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryVector[i]);
				PMString Estado="";//SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
				PMString Folio_Pagina=SQLInterface->ReturnItemContentbyNameColumn("id",QueryVector[i]);
				PMString Id_SeccionText="";//SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
				PMString Id_PublicacionText="";//SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
				listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
			}
			
			//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogObserver::Update call(CreateAndProcessOpenDocCmd)");
			
		}
		
	}while (false);
return kTrue;
}

bool16 CheckInOutAsignaIssueObserver::GetTextOfComboOnDialog(	const WidgetID& Combowidget, PMString& itemToShow, int32& indexSelected)
{
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if (panel == nil)
		{
			
			break;
		}		
		
		
		IControlView *ControlView = panel->FindWidget(Combowidget);
		if(ControlView==nil)
		{
			CAlert::InformationAlert("ControlView");
			break;
		}
		
		//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
		InterfacePtr<IStringListControlData> iStringListControlData(ControlView,IID_ISTRINGLISTCONTROLDATA);
		if (iStringListControlData == nil)
		{
			CAlert::InformationAlert("dropListData");
			
			break;
		}
		
		InterfacePtr<IDropDownListController>	iDropDownListController( ControlView, IID_IDROPDOWNLISTCONTROLLER);
		if(iDropDownListController==nil)
		{
			CAlert::InformationAlert("CheckInOutAsignaIssueObserver::LLenar_Combo_Seccion IDDLDrComboBoxSelecPrefer");
			break;
		}
		
		indexSelected=iDropDownListController->GetSelected();
		
		itemToShow= iStringListControlData->GetString(indexSelected);
		
		
		
		retval=kTrue;
		
	}while(false);
	return(retval);
}

void CheckInOutAsignaIssueObserver::removeIssueDePagina()
{
	do{
		PMString Seccion="";
		PMString Id_PaginaSel="";
		
		if(this->ObtenerFolioDePaginaSeleccionada( Id_PaginaSel, Seccion)==kFalse)
		{
			//CAlert::InformationAlert("Salio por aqui");
			break;
		}
		
		if(Id_PaginaSel.NumUTF16TextChars()<=0)
		{
			break;
		}
		//CAlert::InformationAlert(Id_PaginaSel);
		
		PMString IDUsuario_Actual="";
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			CAlert::InformationAlert("Invalid workspace prefs in CheckInOutAsignaIssueObserver::Update()");
			break;
		}
		IDUsuario_Actual= wrkSpcPrefs->GetIDUserLogedString();
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			CAlert::InformationAlert("Error no puede hacer conexion");
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			CAlert::InformationAlert("Error no encontro preferencias de conexion");
			break;
		}
		
		//Arma Cadena de coneccion DSN
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		
		
		//Arma Cadena Query para Basee de datos
		PMString Consulta="UPDATE edicionesdpagina SET estatus=90, fmod=NOW(), umod='"+IDUsuario_Actual+"' WHERE ID=";
		Consulta.Append(Id_PaginaSel);
		
		
		K2Vector<PMString> QueryVectorUsario;
		
		
		//Realiza Query
		if(SQLInterface->SQLSetInDataBase(StringConection,Consulta))		
			LlenarListaDeIssueDePagina();
		
		
	}while(false);
}


bool16 CheckInOutAsignaIssueObserver::ObtenerFolioDePaginaSeleccionada(PMString& Id_PaginaSel, PMString& Seccion)
{
	bool16 retval=kFalse;
	
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
		
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			break;
		}
		
		InterfacePtr<IControlView>		ListCView( panel->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID), UseDefaultIID() );
		if(ListCView==nil)
		{
			ASSERT_FAIL("No se encontro el editbox");
			break;
		}
		
		//se declara crea una lista 
		SDKListBoxHelper listBox(ListCView, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		//obtencion del controladsor de la lista
		InterfacePtr<IListBoxController> listCntl(ListCView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
		ASSERT_MSG(listCntl != nil, "listCntl nil");
		if(listCntl == nil) 
		{
			break;
		}
		//  Get the first item in the selection (should be only one if the list box
		// has been set up correctly in the framework resource file)
		int32 indexSelected;
		K2Vector<int32> multipleSelection ;
		listCntl->GetSelected( multipleSelection ) ;//obtencion de la selecciones sobre la lista
		const int kSelectionLength =  multipleSelection.Length() ;//tamaÒo de la seleccion
		if (kSelectionLength> 0 )//si la cantidad de campos seleccionados es nmayor a 0
		{
			PMString dbgInfoString("Selected item(s): ");
			for(int i=0; i < multipleSelection.Length() ; i++) 
			{
				indexSelected = multipleSelection[i];
				dbgInfoString.AppendNumber(indexSelected);
				if(i < kSelectionLength - 1)
				{
					dbgInfoString += ", ";
				}
			}
			
			listBox.ObtenerTextoDeSeleccionActual(kFolio_PaginaLabelWidgetID,Id_PaginaSel,kLabelId_SeccionLabelWidgetID,Seccion,indexSelected);
			retval=kTrue;
		}
	}while(false);
	
	return(retval);
}

