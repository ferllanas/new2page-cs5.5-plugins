//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUINodeID.cpp $
//  
//  Owner: Matt Joss
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "ExtLinkUINodeID.h"
#include "IPMStream.h"
#include "widgetid.h"
#include "LinksUIID.h"



ExtLinkUINodeID::ExtLinkUINodeID(const URI& uri, const UID linkUID, const NodeID& parentNode) : 
	fURI(uri),
	fLinkUID(linkUID),
	fParentNode(parentNode) 
{
}

ExtLinkUINodeID::~ExtLinkUINodeID()
{
}
	
//----------------------------------------------------------------------------------------
// ExtLinkUINodeID::Compare
//----------------------------------------------------------------------------------------
int32 ExtLinkUINodeID::Compare(const NodeIDClass* NodeID) const
{
	const ExtLinkUINodeID* compareNode = static_cast<const ExtLinkUINodeID*>(NodeID);
	
	if (this->GetNodeURI() == compareNode->GetNodeURI()) {
		if (this->GetLinkUID() == compareNode->GetLinkUID() ) {
			return 0;
		} else if (this->GetLinkUID() < compareNode->GetLinkUID()) {
			return -1;
		} else {
			return 1;
		}
	} else if (this->GetNodeURI() < compareNode->GetNodeURI()) {
		return -1;
	} else {
		return 1;
	}
}

//----------------------------------------------------------------------------------------
// ExtLinkUINodeID::Copy
//----------------------------------------------------------------------------------------
NodeIDClass* ExtLinkUINodeID::Clone() const
{
	return new ExtLinkUINodeID(this->GetNodeURI(), this->GetLinkUID(), this->GetParentNode());
}

//----------------------------------------------------------------------------------------
// ExtLinkUINodeID::Read
//----------------------------------------------------------------------------------------
void ExtLinkUINodeID::Read(IPMStream*	stream)
{
	ExtLinkUINodeID::Write(stream);
}

//----------------------------------------------------------------------------------------
// ExtLinkUINodeID::Write
//----------------------------------------------------------------------------------------
void ExtLinkUINodeID::Write(IPMStream*	stream) const
{
	// also called for read.
	
	int32 tempLinkUID = fLinkUID.Get();
	stream->XferInt32(tempLinkUID);
	const_cast<ExtLinkUINodeID*>(this)->fLinkUID = tempLinkUID;

	URI tempNodeURI = fURI.GetURI();	
	tempNodeURI.ReadWrite(stream);	
	const_cast<ExtLinkUINodeID*>(this)->fURI = tempNodeURI;
}
