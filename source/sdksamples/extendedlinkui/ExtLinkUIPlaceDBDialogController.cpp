//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIPlaceDBDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
#include "IDropDownListController.h"
#include "IExtLinkSuite.h"
#include "SDKODBCWrapper.h"
#include "IPanelControlData.h"
#include "IStringListControlData.h"
#include "ITextControlData.h"
#include "ISelectionManager.h"

// General includes:
#include "CAlert.h"
#include "CDialogController.h"
#include "K2Vector.h"
#include "URI.h"
#include "URIList.h"
#include <vector>

// Project includes:
#include "SDKODBCCache.h"
#include "ExtLinkUtils.h"
#include "ExtLinkUIID.h"

#include "K2Vector.tpp"

/** ExtLinkUIPlaceDBDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	
	@ingroup extendedlinkui
*/
class ExtLinkUIPlaceDBDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		ExtLinkUIPlaceDBDialogController(IPMUnknown* boss) ;

		/** Destructor.
		*/
		virtual ~ExtLinkUIPlaceDBDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	    virtual void InitializeDialogFields(IActiveContext* dlgContext);


		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	    virtual WidgetID ValidateDialogFields(IActiveContext* myContext);

		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);

	private:

		K2Vector<WideString> fKeyValues;
		K2Vector<PMString> fFieldNames;
		PMString fKeyFieldName;
		PMString fDisplayFieldName;
		PMString fTableQueryPrefix;


		/** Retrieve values for records from the database, and init dialog fields.
		*/
		virtual void InitializeDBData(IActiveContext* dlgContext);

};

CREATE_PMINTERFACE(ExtLinkUIPlaceDBDialogController, kExtLinkUIPlaceDBDialogControllerImpl)


/** Constructor
*/
ExtLinkUIPlaceDBDialogController::ExtLinkUIPlaceDBDialogController(IPMUnknown* boss) : CDialogController(boss)
{
	// these are the names of the fields we want to be able to place...
/*	fFieldNames.push_back("sku");
	fFieldNames.push_back("name");
	fFieldNames.push_back("price");
	fFieldNames.push_back("image");
	fFieldNames.push_back("version");
	fFieldNames.push_back("quantity");
	fFieldNames.push_back("description");
*/
	fKeyFieldName = PMString("sku");	// need to get this from user
	fDisplayFieldName = PMString("name");
}



/* ApplyFields
*/
void ExtLinkUIPlaceDBDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	do
	{
		CDialogController::InitializeDialogFields(dlgContext);	

		if (dlgContext == nil)
		{
			ASSERT_FAIL("Active context is invalid");
			break;
		}

		this->InitializeDBData(dlgContext);

	} while (false);
}


/* ApplyFields
*/
void ExtLinkUIPlaceDBDialogController::InitializeDBData(IActiveContext* dlgContext)
{
	do
	{
	    InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
		if (panelData == nil)
		{
			ASSERT_FAIL("IPanelControlData is invalid");
			break;
		}
		IControlView* panelControlView = panelData->FindWidget(kExtLinkUIPlaceDBDropDownListWidgetID);
		if (panelControlView == nil)
		{
			ASSERT_FAIL("IControlView is invalid");
			break;
		}
		InterfacePtr<IDropDownListController> ddListController(panelControlView, UseDefaultIID());
		if (ddListController == nil)
		{
			ASSERT_FAIL("IDropDownListController is invalid");
			break;
		}

		InterfacePtr<IStringListControlData> listData(panelControlView, UseDefaultIID());
		if (listData)
		{
			listData->Clear();

			PMString scheme(kExtLinkScheme);
			//CAlert::InformationAlert(scheme);
			PMString dbAuthorityName = ExtLinkUtils::GetSelectedDataSource();
			//CAlert::InformationAlert(dbAuthorityName);
			PMString dbTableName = ExtLinkUtils::GetSelectedTableName();
			//CAlert::InformationAlert(dbTableName);
			fTableQueryPrefix = scheme + "://" + dbAuthorityName + "/" + "?table=" + dbTableName;

			//CAlert::InformationAlert(fTableQueryPrefix);
			// rebuild the cache here to speed up place when closing the dialog
			// this rebuilds the entire database since we have a very small database
			// a typical solution would only want to cache the portion of the database
			// that will be visible in the UI.
			URI myURI(fTableQueryPrefix.GetPlatformString());
			fKeyValues.clear();
			// LJH: figure out fFieldNames from the database in stead of hardcoding.
			fFieldNames.clear();
			
			//CAlert::InformationAlert("La instancia");
			SDKODBCCache::Instance().Rebuild(myURI, fKeyFieldName, fKeyValues, fFieldNames);
			//CAlert::InformationAlert("Despues de la instancia");
			
			PMString SWE="";
			SWE.Append(fKeyFieldName);
			SWE.Append(",");
			//CAlert::InformationAlert(SWE);
			// traverse the key set and create a drop-down list representing the data
			for (int32 key = 0; key < fKeyValues.Length(); key++) 
			{
				PMString nameQuery = fTableQueryPrefix + "&" + fKeyFieldName + "='" + fKeyValues[key] + "'#" + fDisplayFieldName;
				CAlert::InformationAlert(nameQuery);
				
				URI nameQueryURI(nameQuery.GetPlatformString());
				//CAlert::InformationAlert(nameQueryURI);
				
				K2Vector<ODBCCacheData> names = SDKODBCCache::Instance().GetCacheData(nameQueryURI, false);	// get all name fields for specified sku, do not update cache

				for (int32 fNum = 0; fNum < names.Length(); fNum++) 
				{
					PMString listEntryString(names[fNum].fFieldData);
					listEntryString.SetTranslatable(kFalse);
					
					CAlert::InformationAlert(listEntryString);
					listData->AddString(listEntryString);
				}
			}

			if (listData->Length() > 0) {
				ddListController->Select(0);
			}
		}

	} while (false);
}

/* ValidateFields
*/
WidgetID ExtLinkUIPlaceDBDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID badWidgetId = kDefaultWidgetId;

	do 
	{		
	} while(false);
	
	return badWidgetId;
}


/* ApplyFields
*/
void ExtLinkUIPlaceDBDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	do 
	{
		if (fKeyValues.size() <= 0) {
			ASSERT_FAIL("No Database records available");
			break;
		}
		InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
		if (panelData == nil)
		{
			ASSERT_FAIL("IPanelControlData is invalid");
			break;
		}
		IControlView* ctrlView = panelData->FindWidget(kExtLinkUIPlaceDBDropDownListWidgetID);
		if (ctrlView == nil)
		{
			ASSERT_FAIL("IControlView is invalid");
			break;
		}		
		InterfacePtr<IDropDownListController> ddListController(ctrlView, UseDefaultIID());
		if (ddListController == nil)
		{
			ASSERT_FAIL("IDropDownListController is invalid");
			break;
		}

		// get the current selection from the drop down list - this index corresponds to the record index in the database.
		int32 recordIndex = ddListController->GetSelected();
		if (recordIndex < 0 || recordIndex >= fKeyValues.size()) {
			ASSERT_FAIL("Invalid record selection - out of range");
			break;
		}

		URIList uriList;
		for (int32 i = 0; i < fFieldNames.size(); i++) {
			PMString query = fTableQueryPrefix + "&" + fKeyFieldName + "='" + fKeyValues[recordIndex] + "'#" + fFieldNames[i];
			URI theURI(query.GetPlatformString());
			uriList.AddURI(theURI);
		}

		// Insert the selected database entries as Links in the document
		InterfacePtr<IExtLinkSuite> duSuite(myContext->GetContextSelection(), UseDefaultIID());
		ASSERT(duSuite);
		if (duSuite) {
			ErrorCode status = duSuite->InsertSelectedRecords(uriList);
		}


	} while(false);
	
}

//  Code generated by DollyXs code generator
