
//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/paneltreeview/N2PSQLFotoPageTask.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: ipaterso $
//  
//  $DateTime: 2005/02/25 04:54:14 $
//  
//  $Revision: #6 $
//  
//  $Change: 320645 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// interface includes
#include "IApplication.h"
#include "IMenuManager.h"
#include "IActionManager.h"
#include "IIdleTaskMgr.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IBoolData.h"
#include "CAlert.h"


#include "ISelectionManager.h" // required by selection templates.
#include "ICommandSequence.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialogCreator.h"
#include "IDialog.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "ISelectableDialogSwitcher.h"
#include "IDocument.h"
#include "IDocumentUtils.h"
#include "IHierarchy.h"
#include "IPageList.h"

#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "SnapshotUtils.h"
#include "SDKLayoutHelper.h"
#include "StreamUtil.h"

// General includes:
#include "CPMUnknown.h"
#include "CmdUtils.h"	// so that the thing will compile (selectionasbtemplates.tpp)
#include "SelectionASBTemplates.tpp"
#include "ILayoutUIUtils.h"
#include "CAlert.h"
#include "FileUtils.h"
#include "SDKFileHelper.h"
#include "PlatformFileSystemIterator.h"
#include "SDKUtilities.h"

// General includes:

#include "CIdleTask.h"

// Project includes
#include "N2PsqlID.h"
#include "N2PSQLUtilities.h"
#include "UpdateStorysAndDBUtilis.h"



#ifdef WINDOWS

	#include "..\N2PCheckInOut\ICheckInOutSuite.h"
	#include "..\N2PCheckInOut\N2PCheckInOutID.h"
	#include "..\N2PCheckInOut\IN2PXMPUtilities.h"
	
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	
	#include "..\N2PFrameOverset\IN2PCTUtilities.h"	
	
#endif

#ifdef MACINTOSH


	#include "ICheckInOutSuite.h"
	#include "N2PCheckInOutID.h"
	#include "IN2PXMPUtilities.h"
	
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "IN2PSQLUtils.h"

	#include "IN2PCTUtilities.h"	
	
#endif



const static int kN2PSQLFotoPageExecInterval = 10*500;	// Refresh every 5 minutes in case we add new images
// milliseconds between checks

/** Implements IIdleTask, to refresh the view onto the file system.
	Just lets us keep up to date when people add new assets.
	This implementation isn't too respectful; it just clears the tree and calls ChangeRoot.

	
	@ingroup paneltreeview
*/

class N2PSQLFotoPageTask : public CIdleTask
{
public:

	/**	Constructor
		@param boss boss object on which this interface is aggregated
	*/
	N2PSQLFotoPageTask(IPMUnknown* boss);

	/**	Destructor
	*/
	virtual ~N2PSQLFotoPageTask() {}


	/**	Called by the idle task manager in the app core when the task is running
		@param appFlags [IN] specifies set of flags which task can check to see if it should do something
			this time round
		@param timeCheck [IN] specifies how many milliseconds before the next active task becomes overdue.
		@return uint32 giving the interval (msec) that should elapse before calling this back
	 */
	virtual uint32 RunTask(uint32 appFlags, IdleTimer* timeCheck);

	/**	Get name of task
		@return const char* name of task
	 */
	virtual const char* TaskName();
protected:

	/**	Update the treeview.
	 */
	void refresh();
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2PSQLFotoPageTask, kN2PSQLFotoPageTaskImpl)

/* Constructor
*/
N2PSQLFotoPageTask::N2PSQLFotoPageTask(IPMUnknown *boss)
	:CIdleTask(boss)
{

}


/* RunTask
*/
uint32 N2PSQLFotoPageTask::RunTask(
	uint32 appFlags, IdleTimer* timeCheck)
{
	if( appFlags & 
		( IIdleTaskMgr::kMouseTracking 
		| IIdleTaskMgr::kUserActive 
		| IIdleTaskMgr::kInBackground 
		| IIdleTaskMgr::kMenuUp))
	{
		return kOnFlagChange;
	}
	
	this->refresh();

	// Has FS changed?
	return kN2PSQLFotoPageExecInterval;
}


/* TaskName
*/
const char* N2PSQLFotoPageTask::TaskName()
{
	return kN2PSQLFotoPageTaskKey;
}


/* refresh
*/
void N2PSQLFotoPageTask::refresh()
{
	do
	{
	
		PreferencesConnection PrefConections;
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString NameDocumentActual="";
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
			
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			break;
		}		
		
		//Para Habiliotar o desxzabilitar la tarea
		InterfacePtr<IIdleTask> taskFoto(GetExecutionContextSession(), IID_IN2PSQLFotoPageTask);
		ASSERT(taskFoto);
		if(!taskFoto)
		{
			break;
		}
			
		PMString ID_Usuario="";	
		PMString Applicacion="";	
		ID_Usuario.AppendNumber(wrkSpcPrefs->GetID_Usuario());
		Applicacion.Append(wrkSpcPrefs->GetNameApp());
		
		if(Applicacion.NumUTF16TextChars()<1)
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			//CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
			break;
		}
		//Verifica que no se un documento nuevo
		
		document->GetName(NameDocumentActual);
		
		if(NameDocumentActual.Contains("Untitled") || NameDocumentActual.Contains("Sin tÌtulo-") || NameDocumentActual.Contains(".indt"))
		{
			//checkIn->SaveRevi();
			break;
		}
		
		InterfacePtr<IDocumentUtils> docUtils(GetExecutionContextSession(), IID_IDOCUMENTUTILS);
		if (docUtils == nil)
		{
			ASSERT_FAIL("FilActActionComponent::DoSave: docUtils invalid");
			break;
		}

		// We need a reference to the document:		
		UIDRef docUIDRef = ::GetUIDRef(document);
		
		IDataBase *db=docUIDRef.GetDataBase();
		
		
		///Obtiene las Ultimas Preferencias
		PrefParamDialogInOutPage PreferencesPara;
		
		//Obtitne los para,etros cpon los que fue guardada por primera vez
		PreferencesPara.Issue_To_SavePage= N2PSQLUtilities::GetXMPVar("N2PPublicacion", document);
		PreferencesPara.Date_To_SavePage = N2PSQLUtilities::GetXMPVar("N2PDate", document);
		PreferencesPara.Seccion_To_SavePage =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
		PreferencesPara.Pagina_To_SavePage = N2PSQLUtilities::GetXMPVar("N2PPagina", document);
		
		PMString fecha_Guion=PreferencesPara.Date_To_SavePage;
		SDKUtilities::Replace(fecha_Guion,"/","-");
		SDKUtilities::Replace(fecha_Guion,"/","-");
		 
		
		//Hay que validar que ya se ha realizadon un chech in o save revition
		// y que la pagina se llame igual a nombre de pagina actual
		if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars() == 0)
		{
			//si no se ha realizado un previo save as o  save se habre el diago save as 
		
			//checkIn->SaveRevi();
			break;
		}
		
		PMString NameDocUltimoSaveAs=PreferencesPara.Pagina_To_SavePage;
		NameDocUltimoSaveAs.Append(".indd");
		
		///Para ver si el documento de enfrente se llama igual que el ultimo salvado
		if(NameDocumentActual!=NameDocUltimoSaveAs)
		{
			//checkIn->SaveRevi();
			break;
		}
		
		PMString TmpStr="";
		PMString RutaJpg="";
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if(app==nil)
		{
			break;
		}
		//nombre de la aplicacion en que se esta trabajando
		PMString nameApp=app->GetApplicationName();
		nameApp.SetTranslatable(kFalse);
		
		TmpStr = PrefConections.PathOfServerFile + ":apaginas:N2PSQL:";
		TmpStr.Append(N2PSQLUtilities::GetXMPVar("N2PFechaCreacionPath", document) + ":" + N2PSQLUtilities::GetXMPVar("N2PNPubliCreacionPath", document) + ":" +  N2PSQLUtilities::GetXMPVar("N2PNSecCreacionPath", document) + ":" +  N2PSQLUtilities::GetXMPVar("N2PUsuarioCredor", document) + ":");
		RutaJpg="N2PSQL:";
		RutaJpg.Append(N2PSQLUtilities::GetXMPVar("N2PFechaCreacionPath", document) + ":" + N2PSQLUtilities::GetXMPVar("N2PNPubliCreacionPath", document) + ":" +  N2PSQLUtilities::GetXMPVar("N2PNSecCreacionPath", document) + ":" +  N2PSQLUtilities::GetXMPVar("N2PUsuarioCredor", document) + ":");
		
		
		
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
			(
				kN2PCTUtilitiesBoss,	// Object boss/class
				IN2PCTUtilities::kDefaultIID
			)));
			
			if(N2PCTUtils==nil)
			{
				break;
			}
				
			N2PCTUtils->OcultaNotasConFrameOverset();					
			
			
		checkIn->ActualizaFotosDeDocumento(TmpStr,RutaJpg,PreferencesPara.Pagina_To_SavePage);		
		
		
		
		if( N2PSQLUtilities::GetEstateCkBoxOfWidget(kN2PSQLCkBoxShowTxtOversetEditWidgetID))
				UpdateStorys->DoMuestraNotasConFrameOverset();
		
	} while(kFalse);
}

//	end, File:	N2PSQLFotoPageTask.cpp
