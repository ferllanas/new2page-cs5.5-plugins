//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkPointerXferBytes.cpp $
//  
//  Owner: jargast
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "ExtLinkPointerXferBytes.h"


ExtLinkPointerXferBytes::ExtLinkPointerXferBytes() 
{
	fMemory   = nil;
	fLocation = 0;
	fSize     = 0;
}


ExtLinkPointerXferBytes::~ExtLinkPointerXferBytes()
{
	// Do nothing
}


uint32 ExtLinkPointerXferBytes::Read(void *buffer, uint32 num) 
{
	uint32 numLeft = fSize - fLocation;
	uint32 numToXfer;
	
	if ( num > numLeft )
	{		
		ASSERT (num > numLeft);
		numToXfer    = numLeft;
		fStreamState = kStreamStateEOF;
	}
	else
		numToXfer = num;
		
	if ( numToXfer > 0 )
		memcpy (buffer, fMemory + fLocation, numToXfer);
	
	fLocation += numToXfer;
	
	return numToXfer;
}



uint32 ExtLinkPointerXferBytes::Write(void *buffer, uint32 num) 
{
	uint32 numLeft = fSize - fLocation;
	uint32 numToXfer;
	
	if ( num > numLeft )
	{
		numToXfer    = numLeft;
		fStreamState = kStreamStateEOF;
	}
	else
		numToXfer = num;
		
	if ( numToXfer > 0 )
		memcpy (fMemory + fLocation, buffer, numToXfer);
	
	fLocation += numToXfer;
	
	return numToXfer;
}



uint32 ExtLinkPointerXferBytes::Seek(int32 numberOfBytes, SeekFromWhere fromHere) 
{
	int32 sp;
	
	if ( fStreamState == kStreamStateEOF )
		fStreamState = kStreamStateGood;
			
	if ( fromHere == kSeekFromStart )
	{
		sp = numberOfBytes;
	}
  	else if ( fromHere == kSeekFromEnd )
	{
		sp = fSize + numberOfBytes;
	}
	else
	{
		sp = fLocation + numberOfBytes;
	}

	if ( sp < 0 )
		sp = 0;
	
	if ( sp > (int32)fSize )
		sp = fSize;
		
	fLocation = (uint32) sp;
	
	return sp;
}



void ExtLinkPointerXferBytes::Flush() 
{
	// Do nothing
}



StreamState ExtLinkPointerXferBytes::GetStreamState() 
{
	return fStreamState;
}



bool16 ExtLinkPointerXferBytes::Close()
{
	fMemory   = nil;
	fSize     = 0;
	fLocation = 0;
	
	return kTrue;
}



bool16 ExtLinkPointerXferBytes::Open(void* memory, uint32 memsize)
{
	fStreamState = kStreamStateGood;
	fMemory   = (char*) memory;
	fSize     = memsize;
	fLocation = 0;
	
	return kTrue;
}


void ExtLinkPointerXferBytes::SetEndOfStream()
{
}

