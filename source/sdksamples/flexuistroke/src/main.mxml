<?xml version="1.0" encoding="utf-8"?>
<!--========================================================================================
//
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/flexuistroke/src/main.mxml $
//
//  Owner: Adobe Developer Technologies
//
//  $Author: alokumar $
//
//  $DateTime: 2011/04/01 11:22:15 $
//
//  $Revision: #1 $
//
//  $Change: 781912 $
//
//  Copyright 2009 Adobe Systems Incorporated. All rights reserved.
//
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or
//  distribution of it requires the prior written permission of Adobe.
//
//	This mxml file implements a stroke weight panel.  It depends on flexuistroke.jsx
//	for event notification.  Most notebly, it shows how to interact with different selection
//	types (PageItem, InsertionPoint, Text, Table, Cell, and defaults).
//
//========================================================================================-->
<mx:Application xmlns:mx="http://www.adobe.com/2006/mxml" 
				layout="absolute" 
				width="250"
				height="150"
				applicationComplete="init()"
				removedFromStage="removeEventListeners()"
				 backgroundColor="#ECE9D8" borderColor="#ECE9D8" backgroundGradientAlphas="[1.0, 1.0]" backgroundGradientColors="[#ECE9D8, #ECE9D8]">
	<mx:Script>
		<![CDATA[	
			import com.adobe.csawlib.indesign.InDesign;
			import com.adobe.csxs.core.CSXSInterface;
			import com.adobe.indesign.Cell;
			import com.adobe.indesign.InsertionPoint;
			import com.adobe.indesign.PageItem;
			import com.adobe.indesign.Table;
			import com.adobe.indesign.Text;
			import com.adobe.indesign.TextStyleRanges;
			
			import mx.collections.ArrayCollection;

			private var _extendScriptInterface:HostObject;
			
			/**
			 * Registers eventlistners and this extension with ExtendScript.
			 */ 
			private function init():void	
			{
				// Register eventlisteners in ExtendScript
				addEventListeners();
				
				_extendScriptInterface = HostObject.getRoot("com.adobe.indesign.FLEXUIST");
				
				if(_extendScriptInterface != null)
					_extendScriptInterface.registerFlashExtension(this);
			}

			private function addEventListeners():void
			{
				CSXSInterface.getInstance().evalScript("addEventListeners()");
			}
			
			private function removeEventListeners():void
			{
				CSXSInterface.getInstance().evalScript("removeEventListeners()");
			}
			
			/**
			 * Sets the stroke weight on the InDesign application to the value in the 
			 * extension dropdown list.
			 */ 
			private function setInDesignStrokeWeight():void
			{
				// Remove event listeners so they are not called when we set the value.
				removeEventListeners();
				
				// Check for at least one open document.
				if ((InDesign.app.documents.length > 0))
				{
					// Check to see if anything is selected
					if (InDesign.app.selection.length > 0)
					{
						// Loop through every item in the selection
						for(var myCounter:int = 0; myCounter < InDesign.app.selection.length; myCounter++)
						{
							var selection:Object = InDesign.app.selection[myCounter];
							if(  selection is InsertionPoint )
							{
								// Text cursor (IBeam) insertion point
								var insertionPoint:InsertionPoint = selection as InsertionPoint;
								insertionPoint.strokeWeight = getPanelStrokeWeight();
							}
							else if( selection is Text )
							{
								// Range of selected text
								var text:Text = selection as Text;
								text.strokeWeight = getPanelStrokeWeight();
							}
							else if(selection is PageItem)
							{
								// A page item (shape, path, etc.)
								var pageItem:PageItem = selection as PageItem;
								pageItem.strokeWeight = getPanelStrokeWeight();
							}
							
							// Fort now skip Table and Cell cases.  This will need to be
							// implemented if we add table support.
						}
					}
					else
					{
						// Document defaults
						InDesign.app.activeDocument.pageItemDefaults.strokeWeight = getPanelStrokeWeight();
					}
				}
				else
				{
					// application defaults
					InDesign.app.pageItemDefaults.strokeWeight = getPanelStrokeWeight();
				}
					
				// Restore the event listeners.
				addEventListeners();
			}

			
			/**
			 * Returns the selected stroke as an integer.
			 */
			private function getPanelStrokeWeight():String
			{
				var weight:String = cbStrokeWeight.text;
				//weight = weight.substr(0, weight.indexOf(" pt"));
				//return parseInt(weight);
				return weight;
			}
			

			/**
			 * Sets Extension Stroke Weight Combo Box selected value
			 * to the value of the selected state in InDesign.
			 * If a PageItem is not selected, the default values of the
			 * active document or application are used.  
			 * Triggered by ExtendScript.
			 */
			public function setPanelStrokeWeight():void
			{	
				var strokeWeight:String = "";
				var enable:Boolean = true;
				
				if (InDesign.app.documents.length > 0)
				{
					// An open document means we need to check for a selection.
					var selections:Object;
					try
					{
						selections = InDesign.app.selection;
					}
					catch(e:Error)
					{
						// Timing issue... We can't get the selection at this point.
						// Just return.  The event handler will actually be called again
						// and AFAIK there are no end user problems.
						return;
					}
					
					//selections exist
					if(selections.length == 1 && selections[0] is InsertionPoint)
					{
						var insertionPoint:InsertionPoint = selections[0] as InsertionPoint;
						
						strokeWeight = insertionPoint.strokeWeight.toString();
					}
					else if(selections.length == 1 && selections[0] is Text)
					{
						// This is a range of text so we have to check style ranges.
						var text:Text = selections[0] as Text;
						var tsr:TextStyleRanges = text.textStyleRanges;
						
						// Look to see if we have a consistent strokeWeight,
						// or if we are in mixed mode (multiple strokeWeights selected)
						
						// Get the strokeWeight of the first style range.
						strokeWeight = tsr.item(0).strokeWeight.toString();
						
						// Check the strokeWeight of each additional
						for(var i:int = 1; i< tsr.count(); i++)
						{
							var nextStrokeWeight:String = tsr.item(i).strokeWeight.toString();
							if( nextStrokeWeight != strokeWeight )
							{
								strokeWeight = "";
								break;
							}
						}
					}
					else if(selections.length == 1 && selections[0] is Table)
					{
						// TODO we need to implement more UI to handle
						// table and cell strokes.  For now we just punt.
						var table:Table = selections[0] as Table;
						strokeWeight = "";
						enable = false;
					}
					else if(selections.length == 1 && selections[0] is Cell)
					{
						// TODO we need to implement more UI to handle
						// table and cell strokes.  For now we just punt.
						var cell:Cell = selections[0] as Cell;
						strokeWeight = "";
						enable = false;
					}
					else if (selections.length > 0 && selections[0] is PageItem)
					{
						// There are multiple page items in the selection.
						// Get strokeWeight for the first page item
						var haveSameStrokeWeights:Boolean = true;
						var pageItem:PageItem = selections[0] as PageItem;
						strokeWeight = pageItem.strokeWeight.toString();
						
						// Look for page items with a different strokeWeight
						for (var myCounter:int = 1; myCounter < selections.length; myCounter++)
						{
							//selected objects do not have the same stroke weight.
							pageItem = selections[myCounter] as PageItem;
							if(strokeWeight != pageItem.strokeWeight)
							{
								haveSameStrokeWeights = false;
								break;
							}							
						}
						
						// If at least one srokeWeight is different 
						// Enter mixed mode (where no value is displayed)
						if (haveSameStrokeWeights == false)
						{
							// Mixed mode
							strokeWeight = "";
						}
					}
					else if(selections.length == 0)
					{
						// Get strokeWeight from the document defaults
						strokeWeight = InDesign.app.activeDocument.pageItemDefaults.strokeWeight.toString();
					}
				}
				else
				{
					// Get strokeWeight from the application defaults
					strokeWeight = InDesign.app.pageItemDefaults.strokeWeight.toString();
				}
				
				// Check to see if we found a single stroke weight
				//
				if(strokeWeight != "" )
				{
					// Set the stroke weight text
					var str:String = strokeWeight + " pt";
					cbStrokeWeight.text = str;
					
					// Set the selected item in the list (or no selection if there is no matching item in the list)
					var dataProvider:ArrayCollection = cbStrokeWeight.dataProvider as ArrayCollection;
					cbStrokeWeight.selectedIndex = dataProvider.getItemIndex(str);
				}
				else
				{
					// There was no uniform stroke weight in the selection.  This is called mixed mode.
					// Clear the text and set the selected index to -1
					cbStrokeWeight.text = "";
					cbStrokeWeight.selectedIndex = -1;
				}
				
				// Enable/disable the stroke weight widget
				if( cbStrokeWeight.enabled != enable )
				{
					cbStrokeWeight.enabled = enable;
				}
					
			}
		]]>
	</mx:Script>	
	
	<mx:HBox
		paddingTop="20"
		paddingBottom="20"
		paddingLeft="20"
		paddingRight="20" borderColor="#ECE9D8" backgroundColor="#ECE9D8">
		<mx:Label text="Stroke Weight:" width="100" />
		<mx:ComboBox 
			id="cbStrokeWeight" 
			change="setInDesignStrokeWeight()"
			editable="false" 
			restrict="[0-9.]"
			width="100" 
			fontWeight="normal" 
			textAlign="right" >
			<mx:dataProvider>
				<mx:ArrayCollection>
					<mx:String>0 pt</mx:String>
					<mx:String>0.25 pt</mx:String>
					<mx:String>0.5 pt</mx:String>
					<mx:String>0.75 pt</mx:String>
					<mx:String>1 pt</mx:String>
					<mx:String>2 pt</mx:String>
					<mx:String>3 pt</mx:String>
					<mx:String>4 pt</mx:String>
					<mx:String>5 pt</mx:String>
					<mx:String>6 pt</mx:String>
					<mx:String>7 pt</mx:String>
					<mx:String>8 pt</mx:String>
					<mx:String>9 pt</mx:String>
					<mx:String>10 pt</mx:String>
					<mx:String>20 pt</mx:String>
					<mx:String>30 pt</mx:String>
					<mx:String>40 pt</mx:String>
					<mx:String>50 pt</mx:String>
					<mx:String>60 pt</mx:String>
					<mx:String>70 pt</mx:String>
					<mx:String>80 pt</mx:String>
					<mx:String>90 pt</mx:String>
					<mx:String>100 pt</mx:String>
				</mx:ArrayCollection>
			</mx:dataProvider>
		</mx:ComboBox>	
	</mx:HBox>	
</mx:Application>