
#ifndef __IN2PAdIssueUtils_h__
#define __IN2PAdIssueUtils_h__


// Interface includes:
#include "IPMUnknown.h"

// Project includes:
#include "N2PAdIssueID.h"

class IN2PAdIssueUtils : public IPMUnknown
{
	public:
	
		enum	{kDefaultIID = IID_IN2PADISSUEUTILS};
		/** Constructor
		@param boss
		*/
		//IN2PAdIssueUtils (IPMUnknown* boss);
		  

		

		/**	See IFrmLblDataSuite::GetFrameLabelAndVisibility
			@param label 
			@param visible 
		 */
		virtual void GetFrameLabelAndVisibility(PMString& label, bool16& visible)=0;


		/**	See IFrmLblDataSuite::UpdateFrameLabel
			@param label 
			@param visible 
			@return ErrorCode 
		 */
		virtual ErrorCode UpdateFrameLabel(const PMString& label, const bool16& visible)=0;
		
		virtual ErrorCode UpdateUIDListLabelfor(UIDList& ListUId,const PMString& label, const bool16& visible)=0;
		
		virtual void GetFrameLabelAndVisibilityUIDListLabelfor(UIDList& selectUIDList,PMString& theLabel, bool16& visibility)=0;
		
		virtual void GetVisibilityOfPenUnLockofUIDListLabelfor(UIDList& selectUIDList, bool16& visibility)=0;
		
		virtual ErrorCode UpdateUIDListLabelforPenUnLock(UIDList& selectUIDList, const int32& LockUnLock, const bool16& visibility)=0;
};

#endif // __IN2PAdIssueUtils_h__