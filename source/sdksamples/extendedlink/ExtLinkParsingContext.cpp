//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkParsingContext.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface Includes
#include "IExtLinkParsingContext.h"


// Implementation Includes

#include "CPMUnknown.h"
// Project includes:

/** Stores state we read or need when parsing.
	The intent was that we could can will know where we are when processing
	characters we get from the stream. Depending the state, we can parse the
	characters different.

	@ingroup extendedlink
*/

class ExtLinkParsingContext : public CPMUnknown<IExtLinkParsingContext>
{
public:

	/** Constructor
		@param boss interface ptr from boss object on which this interface is aggregated.
	*/
	ExtLinkParsingContext(IPMUnknown* boss);

	/** Destructor
	*/
	virtual ~ExtLinkParsingContext() {}

	/** Set up the current parsing state
		@param state specifying the ParsingState
	*/
	virtual void SetParsingState(const ParsingState& state) {
		this->fParsingState = state;
	}

	/** Get parsing state
		@return ParsingState 
	*/
	virtual ParsingState GetParsingState() const {
		return this->fParsingState;
	}

	/** Set up the current parsing element
		@param state specifying the parsing element
	*/
	virtual void SetParsingElement(const XMLReference& element) {
		this->fParsingElement = element;
	}

	/** Get parsing element
		@return Current parsing element 
	*/
	virtual XMLReference GetParsingElement() const {
		return this->fParsingElement;
	}

private:
	ParsingState fParsingState;
	XMLReference fParsingElement;
};


/*	
*/
CREATE_PMINTERFACE(ExtLinkParsingContext, kExtLinkParsingContextImpl)

/*	Constructor
*/
ExtLinkParsingContext::ExtLinkParsingContext(IPMUnknown* boss) :
CPMUnknown<IExtLinkParsingContext>(boss),fParsingState(IExtLinkParsingContext::kOutside) 
{
}


//	end, File: ExtLinkParsingContext.cpp
