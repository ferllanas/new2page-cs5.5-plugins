#include "VCPlugInHeaders.h"
#include "IImportProvider.h"
#include "IDataLink.h"
#include "IDataBase.h"

#ifndef __ImportImagenClass_h__
#define __ImportImagenClass_h__
class IPMUnknown;
class IControlView;

class ImportImagenClass
{
public:
	/** Reference to an import provider and the quality of its
		ability to handle a format.
	 */
	class ImportProvider {
							public:
								typedef object_type data_type;
								ImportProvider(IImportProvider::ImportAbility completeness, const InterfacePtr<IImportProvider>& importProvider) : fCompleteness(completeness), fImportProvider(importProvider) {}
								IImportProvider::ImportAbility fCompleteness;
								InterfacePtr<IImportProvider> fImportProvider;
							};

	/** Collection of import providers that can handle a format.
	 */
	typedef K2Vector<ImportProvider> ImportProviders;

	/**	Constructor.
		@param format of the data to be imported.
		@param layoutHierarchyUIDRef refers to parent for the frame that will display the imported content, 
				normally a spread layer.
		@param layoutGeometryUIDRef refers to the page or spread the frame shoud be positioned relative to.
	*/
	ImportImagenClass();

	/** Destructor.
	*/
	virtual ~ImportImagenClass();

	/**	Opens a stream containing data of the format specified on construction,
		imports this stream into a frame in the document.
		@return kSuccess on success, kFailure otherwise.
	*/
	UIDRef ImportIntoPageItemInFrame(IDFile sysFile,UIDRef frameUIDRef);
	
	
	UIDRef ImportImageAndLoadPlaceGun(const UIDRef& docUIDRef, const PMString& fromPath);

	void CreateAndProcessPlaceItemInGraphicFrameCmd(const UIDRef& docUIDRef,const UIDRef& itemToPlace, const UIDRef& graphicFrame);
protected:
	/** Open a stream in fContentStream of the format given by fFormat.
		Sources data from a number of hard coded buffers or from an 
		external file.
		@return kSuccess on success, kFailure otherwise.
	*/
	ErrorCode	OpenContentStream();

	/** Close stream and release dynamic memory if necessary.
	*/
	void		CloseContentStream();

	/** Scan the registry for services that can import fContentStream.
		@param importProviders list of services (IImportProvider) that can import the data fully.
		@return kSuccess on success, kFailure otherwise.
	*/
	ErrorCode	GetImportProviders(ImportProviders& importProviders);
	
	/**	Create an object that can the format of data in fContentStream. 
		@param importProvider to be used.
		@param uiFlags 
		@param contentUIDRef reference to the object that will handle the imported data.
		For graphics this will refer to a page item such as kImageItem for raster graphics,
		kEPSItem for EPS and so on. For text this will refer to a story, kTextStoryBoss.
		@return kSuccess on success, kFailure otherwise.
	*/
	ErrorCode	ImportContentStream(InterfacePtr<IImportProvider>& importProvider, UIFlags uiFlags, UIDRef& contentUIDRef);

	/**	Create a new frame and put the content in it.
		@param contentUIDRef the content to be framed
		@param frameUIDRef the new frame

	*/
	ErrorCode	CreateFrame(const UIDRef& contentUIDRef, UIDRef& frameUIDRef);	

	/**	Create a frame for text content. The frame is fixed in size at 100*100 points.
		@param contentUIDRef refers to the story to be displayed in the frame.
		@param frameUIDRef the new frame.
		@return kSuccess on success, kFailure otherwise.
	*/
	ErrorCode	CreateTextFrame(const UIDRef& contentUIDRef, UIDRef& frameUIDRef);

	/**	Create a frame for graphic content. The frame is sized to fit the geometry of the content.
		No data link gets created. Instead the content gets embedded in a blob in the document.
		@param contentUIDRef refers to the page item to be put in the frame.
		@param frameUIDRef the new frame.
		@return kSuccess on success, kFailure otherwise.
	*/
	ErrorCode	CreateGraphicFrame(const UIDRef& contentUIDRef, UIDRef& frameUIDRef);


	
	/** Process kAddToHierarchyCmdBoss.				const UIDRef& itemToPlace, const UIDRef& graphicFrame
		@param parent 
		@param child to be added to parent's hierarchy.
		@return kSuccess on success, kFailure otherwise.
	*/
	ErrorCode	AddToHierarchy(const UIDRef& parent, const UIDRef& child);

	/**	Process kCenterContentInFrameCmdBoss.
		@param contentUIDRef to be centred in frame.
		@return kSuccess on success, kFailure otherwise. 
	*/
	ErrorCode	CentreContentInFrame(const UIDRef& contentUIDRef);

	/**	Use IStoreInternal to store fContentStream in a blob 
		in the document.

		IStoreInternal is a persistent interface on a page item and the methods called here
		mutate it. So this code should really be in the scope of a custom command. API commands
		exist that allow you to embed content that has a datalink. However we are creating
		content from a memory based stream and there's no API command to help. Surprisingly
		this code runs cleanly under debug without ASSERTS probably because we are working with
		new page items rather than modifying existing ones, I'm not really sure. But if you run 
		into problems create a custom command and put the code in this method there.

		@param contentUIDRef page item the embedded data blob is to be associated with.
		@return kSuccess on success, kFailure otherwise. 
	*/
	ErrorCode	StoreContentStreamInternally(const UIDRef& contentUIDRef);

	/**	Get the name boss class on which the given IPMUnknown is aggregated.
		@param unknown 
		@return name or value of ClassID in hex.
	*/
	PMString	GetBossClassName(IPMUnknown* unknown);
	
	
	void CreateAndProcessAddLinkCmd(IDataBase* destDB, IDataLink* sourceDL,UID pageItemUID);

private:

		/** Desired data format to import.
	*/
	const PMString fFormat;

	/** Parent for the frame that will display the imported content, normally a spread layer.
	*/
	const UIDRef fLayoutHierarchyUIDRef;

	/** The page or spread the frame shoud be positioned relative to.
	*/
	const UIDRef fLayoutGeometryUIDRef;

	/** Heap allocated memory used to buffer file stream data in memory.
	*/
	uchar* fPointerStreamBuffer;

	/** Data to be imported.
	*/
	InterfacePtr<IPMStream> fContentStream;

};

#endif // __ImportImagenClass_h__