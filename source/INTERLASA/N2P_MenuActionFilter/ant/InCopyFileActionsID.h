//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/incopyfileactions/InCopyFileActionsID.h $
//  
//  Owner: Chris Parrish
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #10 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __InCopyFileActionsID__
#define __InCopyFileActionsID__

#include "CrossPlatformTypes.h"
#include "IDFactory.h"

#include "ICFActID.h"


//----------------------------------------------------------------------------------------
// Plug-in identifier
//----------------------------------------------------------------------------------------

#define kInCopyFileActionsPluginName 	"InCopyFileActions"
DECLARE_PMID(kPlugInIDSpace, kInCopyFileActionsPluginID, kInCopyFileActionsPrefix + 1)

//----------------------------------------------------------------------------------------
// Class identifiers
//----------------------------------------------------------------------------------------

//gap
DECLARE_PMID(kClassIDSpace,		kFileActionsOpenDocResponderBoss,		kInCopyFileActionsPrefix + 1)
DECLARE_PMID(kClassIDSpace,		kInCopyFileActionsComponentBoss,		kInCopyFileActionsPrefix + 2)
DECLARE_PMID(kClassIDSpace,		kInCopyDocDetectContentHandlerBoss,		kInCopyFileActionsPrefix + 3)
//gap
DECLARE_PMID(kClassIDSpace,		kInCopyDocFileHandlerBoss,				kInCopyFileActionsPrefix + 12)
DECLARE_PMID(kClassIDSpace,		kInCopyMRUListBoss,						kInCopyFileActionsPrefix + 13)
//DECLARE_PMID(kClassIDSpace,		kInCopySaveStoryCmdBoss,				kInCopyFileActionsPrefix + 14)
//DECLARE_PMID(kClassIDSpace,		kInCopyRevertStoryCmdBoss,				kInCopyFileActionsPrefix + 15)

//----------------------------------------------------------------------------------------
// Interface identifiers
//----------------------------------------------------------------------------------------

DECLARE_PMID(kInterfaceIDSpace,		IID_IDOCDETECTFILETYPEFOUND,	kInCopyFileActionsPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace,		IID_IDOCDETECTISINCOPYFILE,		kInCopyFileActionsPrefix + 2)

//----------------------------------------------------------------------------------------
// Implementation identifiers
//----------------------------------------------------------------------------------------

DECLARE_PMID(kImplementationIDSpace,		kInCopyFileActionsComponentImpl,	kInCopyFileActionsPrefix + 1)
DECLARE_PMID(kImplementationIDSpace,		kFileActionsOpenDocResponderImpl,	kInCopyFileActionsPrefix + 2)
DECLARE_PMID(kImplementationIDSpace,		kInCopyDocUtilsImpl,				kInCopyFileActionsPrefix + 3)
DECLARE_PMID(kImplementationIDSpace,		kInCopyDocDetectContentHandlerImpl,	kInCopyFileActionsPrefix + 4)
DECLARE_PMID(kImplementationIDSpace,		kDocDetectFileTypeFoundImpl,		kInCopyFileActionsPrefix + 5)
DECLARE_PMID(kImplementationIDSpace,		kDocDetectIsInCopyFileImpl,			kInCopyFileActionsPrefix + 6)
DECLARE_PMID(kImplementationIDSpace,		kInCopyDocFileHandlerImpl,			kInCopyFileActionsPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace,		kInCopySaveStoryCmdImpl,			kInCopyFileActionsPrefix + 11)
DECLARE_PMID(kImplementationIDSpace,		kInCopyRecentDocsDynamicMenuImpl,	kInCopyFileActionsPrefix + 12)
DECLARE_PMID(kImplementationIDSpace,		kInCopyRecentDocsComponentImpl,		kInCopyFileActionsPrefix + 13)
//GAP
//GAP
//GAP



//----------------------------------------------------------------------------------------
// Action identifiers
//----------------------------------------------------------------------------------------

DECLARE_PMID(kActionIDSpace,		kUpdateDesignActionID,		kInCopyFileActionsPrefix + 1)
DECLARE_PMID(kActionIDSpace,		kSaveOldActionID,			kInCopyFileActionsPrefix + 2)

#endif // __InCopyFileActionsID__
