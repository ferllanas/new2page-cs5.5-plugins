//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PFrOvtScriptingDefs_h__
#define __N2PFrOvtScriptingDefs_h__

#if defined WINDOWS
#include "ScriptingWindowDefs.h"
#endif

#define kCPrefs_CLSID { 0x8d448fe0, 0x8194, 0x11d3, { 0xa6, 0x53, 0x0, 0xe0, 0x98, 0x71, 0xa, 0x6f } }
DECLARE_GUID(CPrefs_CLSID, kCPrefs_CLSID);

// Note: 4-char IDs below must be unique.
// See ScriptingDefs.h for further information.

// Event IDs
enum N2PFrOvtScriptEvents
{
	e_Speak		= 'eSpk'
};

// Property IDs
enum N2PFrOvtScriptProperties
{
	p_Said		= 'pSed',
	p_Response		= 'pRsp'
};


#endif // __N2PFrOvtScriptingDefs_h__

//  Code generated by DollyXs code generator
