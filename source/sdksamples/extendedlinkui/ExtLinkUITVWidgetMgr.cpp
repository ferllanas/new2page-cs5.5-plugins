//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUITVWidgetMgr.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IImportProvider.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "ILink.h"
#include "ILinkInfoProvider.h"
#include "ILinkObject.h"
#include "ILinkState.h"
#include "ILinkResource.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "ITreeViewHierarchyAdapter.h"
#include "ExtLinkUINodeID.h"
#include "IExtLinkUIDataModel.h"
#include "IWidgetParent.h"
#include "IMenuUtils.h"
#include "IObserver.h"

// General includes:
#include "Utils.h"
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "IconRsrcDefs.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "OpenPlaceID.h"
#include "SDKFileHelper.h"
#include "ExtLinkUIDataNode.h"
#include "IExtLinkUITreeNodeList.h"

// Project includes:
#include "ExtLinkUIID.h"
#include "ExtLinkID.h"



/**
	Implements ITreeViewWidgetMgr, providing methods to create and describe the widgets for 
	nodes in the tree. This interface is similar to a widget factory in that this is where 
	you will create widgets for the nodes in the tree.
	
	For further reading on ITreeViewWidgetMgr interface, see the Widgets chapter
	of the User Interface technote.

	
	@ingroup extendedlinkui

*/

class ExtLinkUITVWidgetMgr: public CTreeViewWidgetMgr
{
public:
	/**
		Constructor
		@param boss object on which this interface is being aggregated
	*/	
	ExtLinkUITVWidgetMgr(IPMUnknown* boss);

	/**
		Destructor
	*/	
	virtual ~ExtLinkUITVWidgetMgr() {}

	/** Create instance of the widget that represents given node in the tree.
		@param node [IN] specifies node of interest
		@return return reference to a newly instantiated object that is suitable for this node
	*/	
	virtual	IControlView* CreateWidgetForNode(const NodeID& node) const;
	/**
		Retrieve ID of a widget that has the right appearance for the node
		that you're trying to display. Here we use the same widget types for all the nodes,
		but vary the appearance by showing or hiding the expander widget depending on the number
		of children associated with a node.
		@param node [IN] specifies node of interest
		@return the ID of a widget that has the correct appearance for the given node type
	*/	
	virtual	WidgetID GetWidgetTypeForNode(const NodeID& node) const;

	/** Determine how to render the given node to the specified control.
		Figure out how you're going to render it based on properties of the node
		such as whether is has children or not (don't show expander if no children, for instance).
		@param node [IN] refers to the node that we're trying to render
		@param widget [IN] the control into which this node's appearance is going to be rendered
		@param message [IN] (not used)
		@return kTrue if the node has been rendered successfully, kFalse otherwise
	*/	
	virtual	bool16 ApplyNodeIDToWidget( const NodeID& node, IControlView* widget, int32 message = 0) const;

	/** Given a particular node, how far should it be indented? This method
		returns a value in pixels that is used to indent the displayed node 
		from the left edge of the tree-view control.
		@param node [IN] specifies node of interest
		@return an indent for this particular node from the left edge of tree-view
	*/
	virtual PMReal GetIndentForNode(const NodeID& node) const;

private:
	void indent(const NodeID& node, IControlView* widget, IControlView* staticTextWidget) const;

	std::map<PMString, PMString> fExtensionMap;
};	

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(ExtLinkUITVWidgetMgr, kExtLinkUITVWidgetMgrImpl)

/* Constructor
*/
ExtLinkUITVWidgetMgr::ExtLinkUITVWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{
}


/* CreateWidgetForNode
*/
IControlView* ExtLinkUITVWidgetMgr::CreateWidgetForNode(const NodeID& node) const
{
	IControlView* retval= (IControlView*) ::CreateObject(
		::GetDataBase(this),
		RsrcSpec(LocaleSetting::GetLocale(), 
		kExtLinkUIPluginID, 
		kViewRsrcType, 
		kExtLinkUINodeWidgetRsrcID),
		IID_ICONTROLVIEW);

	ASSERT(retval);
	return retval;
}



/* GetWidgetTypeForNode
*/
WidgetID ExtLinkUITVWidgetMgr::GetWidgetTypeForNode(const NodeID& node) const
{
	return kExtLinkUINodeWidgetId;
}


/* ApplyNodeIDToWidget
*/
bool16 ExtLinkUITVWidgetMgr::ApplyNodeIDToWidget(const NodeID& node, IControlView* widget, int32 message) const
{
	CTreeViewWidgetMgr::ApplyNodeIDToWidget(node, widget);
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(widget, UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil) {
			break;
		}
		IControlView* nodeNameView = panelControlData->FindWidget( kExtLinkUINodeNameWidgetID );
		ASSERT(nodeNameView);
		if(nodeNameView == nil) {
			break;
		}
		InterfacePtr<ITextControlData> textControlData( nodeNameView, UseDefaultIID() );
		ASSERT(textControlData);
		if(textControlData== nil) {
			break;
		}
		
		InterfacePtr<const ITreeViewHierarchyAdapter> adapter(this, UseDefaultIID());
		ASSERT(adapter);
		if(adapter == nil) {
			break;
		}
		InterfacePtr<IExtLinkUIDataModel> model(this, UseDefaultIID());
		ASSERT(model);
		if(!model) {
			break;
		}
		TreeNodePtr<ExtLinkUINodeID> myNode(node);
		ASSERT(myNode);
		if(myNode == nil) {
			break;
		}	

		// Set expander widget to hidden state if this node has no kids
		IControlView* expanderWidget = panelControlData->FindWidget(kTreeNodeExpanderWidgetID);
		ASSERT(expanderWidget);
		if (expanderWidget) {
			if (adapter->GetNumChildren(node) <= 0) {
				expanderWidget->Hide();
			} else {
				expanderWidget->Show();
			}
		}


		PMString nodeText("parent node");
		RsrcID iconID(0);
		PMString statusString(kExtLinkUIUpToDateStringKey);
						
		
		if (myNode->GetLinkUID() == kInvalidUID || myNode->GetLinkUID() == kRootDocumentExtLinkNodeUID )
		{
			// not and actual link, but a grouping node - display the URI contained in the node
			if (myNode->GetNodeURI().HasComponent(URI::kFragment) && myNode->GetNodeURI().GetComponent(URI::kFragment) != WideString("")) {
				nodeText.SetString( myNode->GetNodeURI().GetComponent(URI::kFragment));
				
			} else if (myNode->GetNodeURI().HasComponent(URI::kQuery) && myNode->GetNodeURI().GetComponent(URI::kQuery) != WideString("")) {
				nodeText.SetString( myNode->GetNodeURI().GetComponent(URI::kQuery));
				
			} else if (myNode->GetNodeURI().HasComponent(URI::kPath) && myNode->GetNodeURI().GetComponent(URI::kPath) != WideString("")
																	 && myNode->GetNodeURI().GetComponent(URI::kPath) != WideString("/")) {
				nodeText.SetString( myNode->GetNodeURI().GetComponent(URI::kPath));

			} else if (myNode->GetNodeURI().HasComponent(URI::kAuthority) && myNode->GetNodeURI().GetComponent(URI::kAuthority) != WideString("")) {
				nodeText.SetString( myNode->GetNodeURI().GetComponent(URI::kAuthority));
				
			} else if (myNode->GetNodeURI().HasComponent(URI::kScheme) && myNode->GetNodeURI().GetComponent(URI::kScheme) != WideString("")) {
				nodeText.SetString( myNode->GetNodeURI().GetComponent(URI::kScheme));
			}
		}
		else 
		{
			// Real link - get info about the link from the link resource

			// get an info provider for link names - this one uses the name provider
			InterfacePtr<IK2ServiceRegistry> serviceR(GetExecutionContextSession(), UseDefaultIID()); 
			InterfacePtr<const IK2ServiceProvider> nameProvider(serviceR->QueryServiceProviderByClassID(kLinkInfoService, kLinkInfoNameProviderBoss)); 
			InterfacePtr<ILinkInfoProvider> nameInfoService(nameProvider, UseDefaultIID()); 

			// get the link
			
			InterfacePtr<const ILink> theLink(model->GetCurrentDocDB(), myNode->GetLinkUID(), UseDefaultIID());
			ASSERT(theLink);
			if (theLink)
			{
				InterfacePtr<const ILinkResource> theLinkResource(model->GetCurrentDocDB(), theLink->GetResource(), UseDefaultIID());
				ASSERT(theLinkResource);
				if (theLinkResource)
				{
					// LINK NAME
					nodeText = nameInfoService->GetUpdatedInfoForLink(theLink, theLinkResource,true); 

					// STATUS
					ILink::LinkType type = theLink->GetLinkType();
					URI linkURI = theLinkResource->GetURI();
					static const WideString kMyScheme(kExtLinkScheme);
					WideString theScheme = linkURI.GetComponent(URI::kScheme);
					if (theScheme == kMyScheme)
					{
						ILink::ResourceModificationState assetState = theLink->GetResourceModificationState();
						ILink::ObjectModificationState objectState = theLink->GetObjectModificationState();
						ILinkResource::ResourceState state = theLinkResource->GetState();

						if ((objectState == ILink::kObjectUnmodified) && (state == ILinkResource::kAvailable) && (assetState == ILink::kResourceModified)) 
						{
							// LINK OBJECT: NOT modified, LINK ASSET: modified
							iconID = kExtLinkUIImportLinkObjectUnmodifiedAssetOutOfDateIconRsrcID;
							statusString = kExtLinkUIResourceModifiedStringKey;

						}
						else if ((objectState == ILink::kObjectModified) && (state == ILinkResource::kAvailable) && (assetState == ILink::kResourceUnmodified)) 
						{
							// LINK OBJECT: modified, LINK ASSET: NOT modified
							iconID = kExtLinkUIImportLinkObjectModifiedAssetUpToDateIconRsrcID;
							statusString = kExtLinkUILinkModifiedStringKey;
						}
						else if ((objectState == ILink::kObjectModified) && (state == ILinkResource::kAvailable) && (assetState == ILink::kResourceModified)) 
						{
							// LINK OBJECT: modified, LINK ASSET: modified
							iconID = kExtLinkUIImportLinkObjectModifiedAssetOutOfDateIconRsrcID;
							statusString = kExtLinkUILinkAndResourceModifiedStringKey;
						}
						else if (state == ILinkResource::kMissing) 
						{
							// LINK ASSET: missing
							iconID = kExtLinkUIImportLinkAssetMissingIconRsrcID;
							statusString = kExtLinkUIAssetMissingStringKey;
						}

						// Add the node to the node list for easy look up
						InterfacePtr<IExtLinkUITreeNodeList> nodeList(this, UseDefaultIID());
						if (nodeList)
						{
							nodeList->AddTreeNodeForLink(myNode->GetLinkUID(), node);
						}
					}
				}
			}
		}


		// STATUS ICON
		IControlView* statusIconView = panelControlData->FindWidget( kExtLinkUIIconWidgetId );
		ASSERT(statusIconView);
		if(statusIconView) 
		{
			statusIconView->SetRsrcPluginID(kExtLinkUIPluginID);
			statusIconView->SetRsrcID(iconID);
		}

		// put the link information for the node in the display
		nodeText.SetTranslatable( kFalse );
		Utils<IMenuUtils>()->InsertAmpersandForDisplay(&nodeText);		// enable showing of '&' in URI
		textControlData->SetString(nodeText);
		this->indent( node, widget, nodeNameView );
		
	} while(kFalse);

	return kTrue;
}


/* GetIndentForNode
*/
PMReal ExtLinkUITVWidgetMgr::GetIndentForNode(const NodeID& node) const
{
	return 15.0;
}


/* indent
*/
void ExtLinkUITVWidgetMgr::indent( const NodeID& node, IControlView* widget, IControlView* staticTextWidget) const
{	
	do
	{
		ASSERT(widget);
		ASSERT(staticTextWidget);
		if(widget == nil || staticTextWidget == nil) {
			break;
		}
		const PMReal myIndent = this->GetIndent(node);	
		
		// adjust the size to fit the text 
		PMRect widgetFrame = widget->GetFrame();
		widgetFrame.Left() = myIndent;
		widget->SetFrame( widgetFrame );
		
		// Call window changed to force FittedStaticText to resize
		staticTextWidget->WindowChanged();
		PMRect staticTextFrame = staticTextWidget->GetFrame();
		staticTextFrame.Right( widgetFrame.Right() );
		staticTextWidget->SetFrame( staticTextFrame );

	} while(kFalse);
}



//	end, File: ExtLinkUITVWidgetMgr.cpp
