
/***************************************************************************
* UPROMEPS.H
*
* (C) Copyright 2004 SafeNet, Inc. All rights reserved.
*
* Description - UltraPro Multiple Entry Points Header file.
*
* Purpose     - This module provides a method for performing UltraPro API
*               commands so you do not have to deal with command packets.
*               It provides a function for each API command.             
*
****************************************************************************/
#ifndef _UPROMEPS_H
#define _UPROMEPS_H
/***************************************************************************
*                               Error Codes
****************************************************************************/
  
/* API Error Codes */

#define SP_ERR_SUCCESS                      0
#define SP_ERR_INVALID_FUNCTION_CODE        1
#define SP_ERR_INVALID_PACKET               2
#define SP_ERR_UNIT_NOT_FOUND               3
#define SP_ERR_ACCESS_DENIED                4
#define SP_ERR_INVALID_MEMORY_ADDRESS       5
#define SP_ERR_INVALID_ACCESS_CODE          6
#define SP_ERR_PORT_IS_BUSY                 7
#define SP_ERR_WRITE_NOT_READY              8
#define SP_ERR_NO_PORT_FOUND                9
#define SP_ERR_ALREADY_ZERO                 10
#define SP_ERR_DRIVER_OPEN_ERROR            11
#define SP_ERR_DRIVER_NOT_INSTALLED         12
#define SP_ERR_IO_COMMUNICATIONS_ERROR      13
#define SP_ERR_PACKET_TOO_SMALL             15
#define SP_ERR_INVALID_PARAMETER            16
#define SP_ERR_MEM_ACCESS_ERROR             17
#define SP_ERR_VERSION_NOT_SUPPORTED        18
#define SP_ERR_OS_NOT_SUPPORTED             19
#define SP_ERR_QUERY_TOO_LONG               20
#define SP_ERR_INVALID_COMMAND              21
#define SP_ERR_MEM_ALIGNMENT_ERROR          29
#define SP_ERR_DRIVER_IS_BUSY               30
#define SP_ERR_PORT_ALLOCATION_FAILURE      31
#define SP_ERR_PORT_RELEASE_FAILURE         32
#define SP_ERR_ACQUIRE_PORT_TIMEOUT         39
#define SP_ERR_SIGNAL_NOT_SUPPORTED         42
#define SP_ERR_UNKNOWN_MACHINE              44
#define SP_ERR_SYS_API_ERROR                45
#define SP_ERR_UNIT_IS_BUSY                 46
#define SP_ERR_INVALID_PORT_TYPE            47
#define SP_ERR_INVALID_MACH_TYPE            48
#define SP_ERR_INVALID_IRQ_MASK             49
#define SP_ERR_INVALID_CONT_METHOD          50
#define SP_ERR_INVALID_PORT_FLAGS           51
#define SP_ERR_INVALID_LOG_PORT_CFG         52
#define SP_ERR_INVALID_OS_TYPE              53
#define SP_ERR_INVALID_LOG_PORT_NUM         54
#define SP_ERR_INVALID_ROUTER_FLGS          56
#define SP_ERR_INIT_NOT_CALLED              57
#define SP_ERR_DRVR_TYPE_NOT_SUPPORTED      58
#define SP_ERR_FAIL_ON_DRIVER_COMM          59

/* Networking Error Codes */

#define SP_ERR_SERVER_PROBABLY_NOT_UP       60
#define SP_ERR_UNKNOWN_HOST                 61
#define SP_ERR_SENDTO_FAILED                62
#define SP_ERR_SOCKET_CREATION_FAILED       63
#define SP_ERR_NORESOURCES                  64
#define SP_ERR_BROADCAST_NOT_SUPPORTED      65
#define SP_ERR_BAD_SERVER_MESSAGE           66
#define SP_ERR_NO_SERVER_RUNNING            67
#define SP_ERR_NO_NETWORK                   68
#define SP_ERR_NO_SERVER_RESPONSE           69
#define SP_ERR_NO_LICENSE_AVAILABLE         70
#define SP_ERR_INVALID_LICENSE              71
#define SP_ERR_INVALID_OPERATION            72
#define SP_ERR_BUFFER_TOO_SMALL             73
#define SP_ERR_INTERNAL_ERROR               74
#define SP_ERR_PACKET_ALREADY_INITIALIZED   75
#define SP_ERR_PROTOCOL_NOT_INSTALLED       76
#define SP_ERR_NO_LEASE_FEATURE             101
#define SP_ERR_LEASE_EXPIRED                102
#define SP_ERR_COUNTER_LIMIT_REACHED        103 
#define SP_ERR_NO_DIGITAL_SIGNATURE         104
#define SP_ERR_SYS_FILE_CORRUPTED           105
#define SP_ERR_STRING_BUFFER_TOO_LONG       106 
 
/* Shell Error Codes */

#define SH_BAD_ALGO                     128
#define SH_LONG_MSG                     129
#define SH_READ_ERROR                   130
#define SH_NOT_ENOUGH_MEMORY            131
#define SH_CANNOT_OPEN                  132
#define SH_WRITE_ERROR                  133
#define SH_CANNOT_OVERWRITE             134
#define SH_TOO_MANY_RELOCATION          135
#define SH_BAD_RELOCATION               136
#define SH_INVALID_HEADER               137
#define SH_TMP_CREATE_ERROR             138
#define SH_PRG_TOO_SMALL                139
#define SH_PATH_NOT_THERE               140
#define SH_OUT_OF_RANGE                 141
#define SH_NUMBER_EXPECTED              142
#define SH_NOT_WINFILE                  143
#define SH_WIN3_OR_HIGHER               144
#define SH_WIN_VERSION_WARNING          145
#define SH_SELF_LOAD_FILE               146
#define SH_WITHOUT_ENTRY                147
#define SH_BAD_FILESPEC                 148
#define SH_SYNTAX                       149
#define SH_BAD_OPTION                   150
#define SH_TOO_MANY_FSPECS              151
#define SH_TOO_MANY_SEEDS               152
#define SH_NO_ENC_SEED                  153
#define SH_NO_HOOK_WARNING              154
#define SH_UNSUPPORTED_EXE              155
#define SH_TOOMANYFILES                 156
#define SH_BAD_FILE_INFO                157
  
/* Win32 Specific Error Codes */

#define SH_NOT_WIN32FILE                158
#define SH_INVALID_MACHINE              159
#define SH_INVALID_SECTION              160
#define SH_INVALID_RELOC                161
#define SH_NO_PESHELL                   162

/***************************************************************************
*                      Compiler Specific Definitions
****************************************************************************/
  
/* Dword Alignment Roll-up */

#define SP_APIPACKET_ALIGNMENT_VALUE (sizeof(unsigned long))
#define SP_APIPACKET_SIZE            (1024+SP_APIPACKET_ALIGNMENT_VALUE)
#define SP_MAX_QUERY_SIZE            56              /* in bytes         */

/* Create SP types */

#ifdef __cplusplus
#define SP_EXPORT extern "C"
#else
#define SP_EXPORT extern
#endif
#define SP_LOCAL static


/* Define OS */

#if ((defined(_NW_) || defined(CLIB_V311)) && !defined(_OS2_))
#ifndef _NW_
#define _NW_ 1
#endif
#endif

#if ((defined(_WIN32_) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)) && !defined(_NW_) && !defined(_QNX_))
#ifndef _WIN32_
#define _WIN32_ 1
#endif
#endif

#if ((defined(_WIN3_) || defined(__WINDOWS_H) || defined(_WINDOWS_) || \
      defined(_WIN_)  || defined(__WINDOWS__) || defined(_WINDOWS)  || \
      defined(_WINDLL)) && !defined(_OS2_) && !defined(_WIN32_))
#ifndef _WIN_
#define _WIN_ 1
#endif
#endif

#if (!defined(_OS2_) && !defined(_WIN32_) && !defined(_WIN_) && !defined(_NW_) && !defined(_QNX_))
#ifndef _DOS_
#define _DOS_ 1
#endif
#endif


/* GNU C Definitions */

#if (defined(__GNUC__) || defined(_GNUC_) || defined(__EMX__))
#if defined(_OS2_)
#define _SP_SYSCALL  _System
#define _SP_STDCALL
#define _SP_FASTCALL
#define _SP_PASCAL   _Pascal
#define _SP_CDECL    _Cdecl
#define _SP_FAR
#define _SP_NEAR
#define _SP_HUGE
#define _SP_API      EXPENTRY
#define _SP_PTR      *
#endif
#endif

/* Borland C Definitions */

#if defined(__BORLANDC__)
#if defined(_WIN32_)
#define _SP_STDCALL  __stdcall
#define _SP_FASTCALL __fastcall
#define _SP_PASCAL
#define _SP_CDECL    __cdecl
#define _SP_FAR
#define _SP_NEAR
#define _SP_HUGE
#define _SP_API      __stdcall
#define _SP_PTR      *
#elif (defined(_WIN_) || defined(_WIN3_))
#define _SP_LOADDS _loadds
#define _SP_STDCALL
#define _SP_FASTCALL
#define _SP_PASCAL  pascal
#define _SP_CDECL   _cdecl
#define _SP_FAR     far
#define _SP_NEAR    near
#define _SP_HUGE    huge
#define _SP_PTR     _SP_FAR *
#define _SP_API     __stdcall _RB_LOADDS
#elif defined(_DOS_)
#define _SP_STDCALL
#define _SP_FASTCALL
#define _SP_PASCAL  pascal
#define _SP_CDECL   _cdecl
#define _SP_FAR     far
#define _SP_NEAR    near
#define _SP_HUGE    huge
#if ( defined(_DOS4GW_) && defined(_WATC_) )
#define _SP_PTR     *
#endif
#if defined(_BPASF_)
#define _SP_PTR     _SP_FAR *
#define _SP_API     _SP_FAR _SP_PASCAL
#elif defined(_BPASN_)
#define _SP_PTR     _SP_FAR *
#define _SP_API     _SP_NEAR _SP_PASCAL
#else
#if (defined(__SMALL__) || defined(__MEDIUM__) || defined(__LARGE__) || defined(__COMPACT__))
#define _SP_PTR     _SP_FAR *
#elif defined(__HUGE__)
#define _SP_PTR     _SP_HUGE *
#elif defined(__TINY__)
#define _SP_PTR     *
#endif
#define _SP_API     _SP_FAR _SP_CDECL
#endif
#endif
#endif

/* Microsoft C Definitions */

#if  defined(_MSC_VER)
#if  defined(_WIN32_)
#define _SP_STDCALL  __stdcall
#define _SP_FASTCALL __fastcall
#define _SP_PASCAL
#define _SP_CDECL    __cdecl
#define _SP_FAR
#define _SP_NEAR
#define _SP_HUGE
#define _SP_API  __stdcall
#define _SP_PTR      *
#elif (defined(_WIN_) || defined(_WIN3_))
#define _SP_LOADDS   _loadds
#define _SP_FASTCALL _fastcall
#define _SP_PASCAL   _pascal
#define _SP_CDECL    _cdecl
#define _SP_FAR      _far
#define _SP_NEAR     _near
#define _SP_HUGE     _huge
#define _SP_API      __stdcall _SP_LOADDS
#define _SP_PTR     _SP_FAR *
#elif (_MSC_VER <= 7)
#define _SP_LOADDS   _loadds
#define _SP_FASTCALL _fastcall
#define _SP_PASCAL   _pascal
#define _SP_CDECL    _cdecl
#define _SP_FAR      _far
#define _SP_NEAR     _near
#define _SP_HUGE     _huge
#define _SP_API      _SP_FAR _SP_PASCAL
#define _SP_PTR      _SP_FAR *
#else
#define _SP_FASTCALL __fastcall
#define _SP_PASCAL
#define _SP_CDECL    __cdecl
#define _SP_FAR
#define _SP_NEAR
#define _SP_HUGE
#define _SP_API      _SP_CDECL
#define _SP_PTR      *
#endif
#endif

/* Watcom C Definitions */

#if defined(__WATCOMC__)
#if defined(_WIN32_)
#define _SP_STDCALL  __stdcall
#define _SP_FASTCALL __fastcall
#define _SP_PASCAL
#define _SP_CDECL    __cdecl
#define _SP_FAR
#define _SP_NEAR
#define _SP_HUGE
#define _SP_API      __stdcall
#define _SP_PTR      *
#elif defined(_NW_)
#define _SP_STDCALL
#define _SP_FASTCALL __fastcall
#define _SP_PASCAL   __pascal
#define _SP_CDECL    __cdecl
#define _SP_FAR
#define _SP_NEAR
#define _SP_HUGE
#define _SP_API      _SP_CDECL
#define _SP_PTR      *
#if defined(__FLAT__)
#define _SP_FAR
#define _SP_NEAR
#define _SP_HUGE
#define _SP_PTR      *
#define _SP_API      _SP_CDECL
#else
#define _SP_FAR      __far
#define _SP_NEAR     __near
#define _SP_HUGE     __huge
#define _SP_PTR      _SP_FAR *
#define _SP_API      _SP_FAR _SP_CDECL
#endif
#else
#define _SP_STDCALL
#define _SP_FASTCALL __fastcall
#define _SP_PASCAL   __pascal
#define _SP_CDECL    __cdecl
#define _SP_FAR
#define _SP_NEAR
#define _SP_HUGE
#define _SP_API      _SP_CDECL
#define _SP_PTR      *
#endif
#endif

#define SP_LOADDS   _SP_LOADDS
#define SP_SYSCALL  _SP_SYSCALL
#define SP_STDCALL  _SP_API
#define SP_FASTCALL _SP_FASTCALL
#define SP_PASCAL   _SP_PASCAL
#define SP_CDECL    _SP_CDECL
#define SP_FAR      _SP_FAR
#define SP_NEAR     _SP_NEAR
#define SP_HUGE     _SP_HUGE
#if ( defined(_DOS4GW_) && defined(__BORLANDC__) && defined(_WATC_) )
#define SP_API      __cdecl
#else
#define SP_API      _SP_API
#endif

#define SP_PTR      _SP_PTR
#define SP_IN
#define SP_OUT
#define SP_IO

/***************************************************************************
*              SafeNet Specific Type Definitions and Constants
****************************************************************************/
  
typedef                void  SP_VOID;
typedef unsigned       char  SP_BOOLEAN;
typedef unsigned       char  SP_BYTE;
typedef unsigned short int   SP_WORD;
typedef unsigned long  int   SP_DWORD;
typedef long int             SP_LONG;
typedef                char  SP_CHAR;

typedef SP_VOID    SP_PTR SPP_VOID;
typedef SP_BYTE    SP_PTR SPP_BYTE;
typedef SP_BOOLEAN SP_PTR SPP_BOOLEAN;
typedef SP_WORD    SP_PTR SPP_WORD;
typedef SP_DWORD   SP_PTR SPP_DWORD;
typedef SP_CHAR    SP_PTR SPP_CHAR;
 
/* Packet Definition */

typedef SP_DWORD SP_UPRO_APIPACKET[SP_APIPACKET_SIZE/sizeof(SP_DWORD)];
typedef SP_WORD  SP_STATUS;
typedef SPP_VOID SPP_UPRO_APIPACKET;

/* OS Types */

#define SP_MIN_OS_TYPE            0
#define SP_AUTODETECT_OS_TYPE     0          /* Autodetect OS type         */
#define SP_OS_DOS                 1          /* DOS operating system       */
#define SP_OS_RSRV1               2          /* reserved                   */
#define SP_OS_RSRV2               3          /* reserved                   */
#define SP_OS_WIN3x               4          /* Windows 3.x operating env  */
#define SP_OS_WINNT               5          /* Windows NT operating system*/
#define SP_OS_OS2                 6          /* OS/2 operating system      */
#define SP_OS_WIN95               7          /* Windows 95 operating system*/
#define SP_OS_WIN32s              8          /* Windows WIN32s env         */
#define SP_OS_NW                  9          /* Netware operating system   */
#define SP_OS_QNX                 10
#define SP_OS_LINUX               12         /* Linux operating system     */
#define SP_MAX_OS_TYPE            12

/* Driver types */

#define SP_DOSRM_LOCAL_DRVR       1          /* DOS Real Mode local driver */
#define SP_WIN3x_LOCAL_DRVR       2          /* Windows 3.x local driver   */
#define SP_WIN32s_LOCAL_DRVR      3          /* Win32s local driver        */
#define SP_WIN3x_SYS_DRVR         4          /* Windows 3.x system driver  */
#define SP_WINNT_SYS_DRVR         5          /* Windows NT system driver   */
#define SP_OS2_SYS_DRVR           6          /* OS/2 system driver         */
#define SP_WIN95_SYS_DRVR         7          /* Windows 95 system driver   */
#define SP_NW_LOCAL_DRVR          8          /* Netware local driver       */
#define SP_QNX_LOCAL_DRVR         9          /* QNX local driver           */
#define SP_UNIX_SYS_DRVR          10         /* UNIX local driver          */ 
#define SP_SOLARIS_SYS_DRVR       11         /* SOLARIS local driver       */ 
#define SP_LINUX_SYS_DRVR         12         /* Linux system driver        */
#define SP_LINUX_LOCAL_DRVR       13         /* Linux local driver         */
#define SP_AIX_SYS_DRVR           14         /* AIX system driver          */
#define SP_UNIXWARE_SYS_DRVR      15         /* UNIX system  driver        */

/* Heartbeat Constants */

#define SP_LIC_UPDATE_INT      120           /* Default heartbeat - 2*60 = 2 min.  */
#define SP_MAX_HEARTBEAT       2592000       /* Max heartbeat - 30*24*60*60 seconds*/ 
#define SP_MIN_HEARTBEAT       60            /* Min heartbeat - 60 seconds         */
#define SP_INFINITE_HEARTBEAT  0xFFFFFFFF    /* Infinite heartbeat                 */ 


/* Set Protocol Flags */

typedef SP_WORD SP_PROTOCOL_FLAG;
#define SP_TCP_PROTOCOL          1
#define SP_IPX_PROTOCOL          2
#define SP_NETBEUI_PROTOCOL      4
#define SP_SAP_PROTOCOL          8

/* Communication Modes */

#define SP_STANDALONE_MODE           "SP_STANDALONE_MODE"
#define SP_DRIVER_MODE               "SP_DRIVER_MODE"
#define SP_LOCAL_MODE                "SP_LOCAL_MODE"
#define SP_BROADCAST_MODE            "SP_BROADCAST_MODE"
#define SP_ALL_MODE                  "SP_ALL_MODE"
#define SP_SERVER_MODE               "SP_SERVER_MODE"


/* Defines for toolkit cell address */
#define SP_INT8_TYPE       1
#define SP_INT32_TYPE      2
#define SP_STRING_TYPE     3
#define SP_BOOL_TYPE       4
#define SP_DATE_TYPE       5
#define SP_LEASE_TYPE      6
#define SP_ALGO_TYPE       7
#define SP_COUNTER_TYPE    8
#define SP_DATA_WORD_TYPE  9
#define SP_RESERVED_TYPE   15

/* Defines for SFNTsntlReadValue */

#define SP_SERIAL_NUMBER_INTEGER  ((SP_INT32_TYPE<<16)     |  0)
#define SP_DEVELOPER_ID_WORD      ((SP_DATA_WORD_TYPE<<16) |  1)
#define SP_DESIGN_ID_WORD         ((SP_RESERVED_TYPE<<16)  |  15)

/* Defines for SFNTsntlReadString */

#define SP_PART_NUMBER_STRING     ((SP_RESERVED_TYPE << 16)  | 0x05)

/* Sharing Flags */

#define SP_SHARE_USERNAME           1
#define SP_SHARE_MAC_ADDRESS        2
#define SP_SHARE_DEFAULT            3

/* Key Type Constants */

#define SP_KEY_FORM_FACTOR_PARALLEL 0
#define SP_KEY_FORM_FACTOR_USB      1

#define SP_SUPERPRO_FAMILY_KEY      0
#define SP_ULTRAPRO_FAMILY_KEY      1
#define SP_UNKNOWN_FAMILY_KEY      16
/* Maximum values */

#define SP_MAX_NUM_DEV   10     /* Maximum number of devices  */
#define SP_MAX_NAME_LEN  64     /* Maximum host name length   */
#define SP_MAX_ADDR_LEN  32     /* Maximum host address length*/

/* Date Structure Used by SFNTsntlReadLease */

typedef struct tag_uproDate
{
  SP_WORD year;
  SP_WORD month;
  SP_WORD days;  
}SP_UPRO_DATE, *SPP_UPRO_DATE;

/***************************************************************************
*                       Function Prototypes
****************************************************************************/
 
SP_EXPORT SP_STATUS SP_API
SFNTsntlActivateLicense( SP_IN SPP_UPRO_APIPACKET thePacket,
                         SP_IN SP_DWORD           toolkitCellAddress,
                         SP_IN SP_WORD            writePassword,
                         SP_IN SP_WORD            activatePassword1,
                         SP_IN SP_WORD            activatePassword2);

SP_EXPORT SP_VOID SP_API 
SFNTsntlCleanup();

SP_EXPORT SP_STATUS SP_API
SFNTsntlDecrementCounter( SP_IN SPP_UPRO_APIPACKET thePacket,
                          SP_IN SP_DWORD           toolkitCellAddress,
                          SP_IN SP_WORD            writePassword,
                          SP_IN SP_WORD            decrementValue );

SP_EXPORT SP_STATUS SP_API
SFNTsntlGetContactServer ( SP_IN  SPP_UPRO_APIPACKET thePacket,
                           SP_OUT SPP_CHAR           serverNameBuf,
                           SP_IN  SP_WORD            serverNameBufSz );

SP_EXPORT SP_STATUS SP_API
SFNTsntlGetFullStatus( SP_IN SPP_UPRO_APIPACKET thePacket );


SP_EXPORT SP_STATUS SP_API
SFNTsntlGetHardLimit( SP_IN  SPP_UPRO_APIPACKET thePacket,
                      SP_OUT SPP_WORD           hardLimit ); 

SP_EXPORT SP_STATUS SP_API
SFNTsntlGetKeyType( SP_IN  SPP_UPRO_APIPACKET  thePacket,
                    SP_OUT SPP_WORD            keyFamily,
                    SP_OUT SPP_WORD            keyFormFactor,
                    SP_OUT SPP_WORD            keyMemorySize );

SP_EXPORT SP_STATUS SP_API
SFNTsntlGetLicense( SP_IN SPP_UPRO_APIPACKET thePacket,
                    SP_IN SP_WORD            devID,
                    SP_IN SP_WORD            designID,
                    SP_IN SP_DWORD           toolkitCellAddress );

SP_EXPORT SP_STATUS SP_API
SFNTsntlGetVersion( SP_IN  SPP_UPRO_APIPACKET thePacket,
                    SP_OUT SPP_BYTE           majVer,
                    SP_OUT SPP_BYTE           minVer,
                    SP_OUT SPP_BYTE           rev,
                    SP_OUT SPP_BYTE           osDrvrType );


SP_EXPORT SP_STATUS SP_API
SFNTsntlInitialize( SP_OUT SPP_UPRO_APIPACKET thePacket );

SP_EXPORT SP_STATUS SP_API
SFNTsntlLockData( SP_IN SPP_UPRO_APIPACKET  thePacket,
                  SP_IN SP_DWORD            toolkitCellAddress,
                  SP_IN SP_WORD             writePassword );

SP_EXPORT SP_STATUS SP_API
SFNTsntlQueryLicense( SP_IN  SPP_UPRO_APIPACKET  thePacket,
                      SP_IN  SP_DWORD            toolkitCellAddress,
                      SP_IN  SP_WORD             writePassword,
                      SP_IN  SPP_VOID            query,
                      SP_OUT SPP_VOID            response,                                  
                      SP_OUT SPP_DWORD           response32,
                      SP_IN  SP_WORD             length );


SP_EXPORT SP_STATUS SP_API
SFNTsntlQueryLicenseDecrement( SP_IN  SPP_UPRO_APIPACKET   thePacket,
                               SP_IN  SP_DWORD             toolkitCellAddress,
                               SP_IN  SP_WORD              writePassword,
                               SP_IN  SP_WORD              decAmount,
                               SP_IN  SPP_VOID             query,
                               SP_OUT SPP_VOID             response,
                               SP_OUT SPP_DWORD            response32,
                               SP_IN  SP_WORD              length );

SP_EXPORT SP_STATUS SP_API
SFNTsntlQueryLicenseLease( SP_IN  SPP_UPRO_APIPACKET   thePacket,     
                           SP_IN  SP_DWORD             toolkitCellAddress,
                           SP_IN  SP_WORD              writePassword,
                           SP_IN  SPP_VOID             query,
                           SP_OUT SPP_VOID             response,
                           SP_OUT SPP_DWORD            response32,
                           SP_IN  SP_WORD              length );                                        

SP_EXPORT SP_STATUS SP_API
SFNTsntlQueryLicenseSimple( SP_IN  SPP_UPRO_APIPACKET   thePacket,
                            SP_IN  SP_DWORD             toolkitCellAddress,
                            SP_IN  SPP_VOID             query,
                            SP_OUT SPP_VOID             response,
                            SP_OUT SPP_DWORD            response32,
                            SP_IN  SP_WORD              length );



SP_EXPORT SP_STATUS SP_API
SFNTsntlReadLease( SP_IN  SPP_UPRO_APIPACKET  thePacket,
                   SP_IN  SP_DWORD            toolkitCellAddress,
                   SP_OUT SPP_UPRO_DATE       dateValue,
                   SP_OUT SPP_WORD            daysLeft );

SP_EXPORT SP_STATUS SP_API
SFNTsntlReadString( SP_IN  SPP_UPRO_APIPACKET  thePacket,
                    SP_IN  SP_DWORD            toolkitCellAddress,
                    SP_OUT SPP_CHAR            string,
                    SP_IN  SP_WORD             stringLength );


SP_EXPORT SP_STATUS SP_API
SFNTsntlReadValue( SP_IN  SPP_UPRO_APIPACKET thePacket,
                   SP_IN  SP_DWORD           toolkitCellAddress,
                   SP_OUT SPP_DWORD          value );


SP_EXPORT SP_STATUS SP_API
SFNTsntlReleaseLicense( SP_IN  SPP_UPRO_APIPACKET thePacket,
                        SP_IN  SP_DWORD           toolkitCellAddress,    
                        SP_IO  SPP_WORD           numUserLic );

SP_EXPORT SP_STATUS SP_API
SFNTsntlSetContactServer ( SP_IN SPP_UPRO_APIPACKET thePacket,       
                           SP_IN SPP_CHAR           serverName );


SP_EXPORT SP_STATUS SP_API
SFNTsntlSetHeartBeat( SP_IN SPP_UPRO_APIPACKET thePacket,
                      SP_IN SP_DWORD           heartBeatValue );

SP_EXPORT SP_STATUS SP_API
SFNTsntlSetProtocol ( SP_IN SPP_UPRO_APIPACKET thePacket,
                      SP_IN SP_PROTOCOL_FLAG      protocol );


SP_EXPORT SP_STATUS SP_API
SFNTsntlSetSharedLicense( SP_IN SPP_UPRO_APIPACKET thePacket,
                          SP_IN SP_WORD            shareMode,
                          SP_IN SPP_VOID           reserved );



SP_EXPORT SP_STATUS SP_API
SFNTsntlWriteString( SP_IN SPP_UPRO_APIPACKET  thePacket,
                     SP_IN SP_DWORD            toolkitCellAddress,
                     SP_IN SPP_CHAR            string,
                     SP_IN SP_BYTE             flag,
                     SP_IN SP_WORD             writePassword );

SP_EXPORT SP_STATUS SP_API
SFNTsntlWriteValue( SP_IN SPP_UPRO_APIPACKET  thePacket,
                    SP_IN SP_DWORD            toolkitCellAddress,
                    SP_IN SP_DWORD            value,
                    SP_IN SP_BYTE             flag, 
                    SP_IN SP_WORD             writePassword );


SP_EXPORT SP_STATUS SP_API
SFNTsntlUnlockData( SP_IN SPP_UPRO_APIPACKET  thePacket,
                    SP_IN SP_DWORD            toolkitCellAddress,
                    SP_IN SP_WORD             writePassword,
                    SP_IN SP_WORD             OverwritePassword1,
                    SP_IN SP_WORD             OverwritePassword2 );

                             
#endif /* _UPROMEPS_H */
/* end of file */

