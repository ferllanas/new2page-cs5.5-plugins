//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/cellpanel/CelPnlEditBoxEventHandler.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ITextControlData.h"
#include "IPanelControlData.h"
#include "IWidgetParent.h"
#include "ITableCellData.h"

// General includes:
#include "PlatformEditBoxEH.h"
#include "PlatformEvent.h"
#include "keyboarddefs.h"	

// Project includes:
#include "CelPnlID.h"

/** CelPnlEditBoxEventHandler
	implements the event handler.

	CelPnlEditBoxEventHandler implements IEventHandler based on
	the partial implementation PlatformEditBoxEventHandler.

	@ingroup cellpanel
	
*/
class CelPnlEditBoxEventHandler : public PlatformEditBoxEventHandler
{
public:
	/**
		Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.
	*/
				CelPnlEditBoxEventHandler(IPMUnknown *boss);
	/**
		Destructor.
	*/
	virtual		~CelPnlEditBoxEventHandler();
					
	 /**
		On key-down, swallow all keys which have no meaning for positive numeric entry.
		Note: we can't notify observers, since the alteration of the associated text
		control data doesn't occur until the Key-up event is processed.
		
		@param pEvent IEvent object.
	*/
	virtual bool16	KeyDown(IEvent* pEvent) ; //Override
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(CelPnlEditBoxEventHandler, kCelPnlEditBoxEventHandlerImpl)

/* CelPnlEditBoxEventHandler
*/
CelPnlEditBoxEventHandler::CelPnlEditBoxEventHandler(IPMUnknown *boss) :
	PlatformEditBoxEventHandler(boss)
{

}

/* ~CelPnlEditBoxEventHandler
*/
CelPnlEditBoxEventHandler::~CelPnlEditBoxEventHandler()
{
}


/* KeyDown
	On key-down, swallow all keys which have no meaning for positive numeric entry
	Note: we can't notify observers, since the alteration of the associated text
	control data doesn't occur until the Key-up event is processed
*/
bool16 CelPnlEditBoxEventHandler::KeyDown(IEvent* pEvent)
{
	if(pEvent == nil) return kFalse;

	if(pEvent->CmdKeyDown())
		return PlatformEditBoxEventHandler::KeyDown(pEvent);

	const SysChar	nCharCode = pEvent->GetChar();
	switch(nCharCode)
	{
	case kReturnKey:
	case kEnterKey:
		{
			do{
			InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());
			if(myParent == nil) break;
			
			InterfacePtr<ITableCellData>	pTableCellData((ITableCellData*)myParent->QueryParentFor(IID_ITABLECELLDATA));
			if(pTableCellData == nil) break;

			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA));
			if(panel == nil) break;
			
			InterfacePtr<IControlView>		editBoxView(panel->FindWidget(kTableCellEditBoxWidgetID), UseDefaultIID());
			if(panel == nil) break;

			InterfacePtr<ITextControlData>	selectedChar(editBoxView, UseDefaultIID());
			if(selectedChar == nil) break;
			
			PMString  s = selectedChar->GetString();
			s.SetTranslatable(kFalse);
			
			pTableCellData->SetCellString(WideString(s)); // Set string to selected cell.
			
			InterfacePtr<IControlView> tableCell(panel->FindWidget(kTableCellWidgetID), UseDefaultIID());
			if(tableCell == nil) break;
			
			tableCell->Invalidate();

			}while(false);
		}
		return PlatformEditBoxEventHandler::KeyDown(pEvent);
		break;
	case kDeleteKey:
	case kTabKey:
	case kBackspaceKey:
	case kEscapeKey:
	case kRightArrowKey:
	case kLeftArrowKey:
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	default:
		return PlatformEditBoxEventHandler::KeyDown(pEvent);
		break;
	}
}

// End, CelPnlEditBoxEventHandler.cpp