//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkSetObjectModificationStateCmd.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISubject.h"
#include "IWorkspace.h"
#include "IIntData.h"
#include "ILink.h"

// General includes:
#include "CmdUtils.h"
#include "ErrorUtils.h"
#include "Command.h"

// Project includes:
#include "ExtLinkID.h"


/** Implementation of command to change the a link object's modification state.
	
	@see ILink
	@ingroup extendedlink
*/

class ExtLinkSetObjectModificationStateCmd : public Command
{
public:
	/** Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.*/
	ExtLinkSetObjectModificationStateCmd(IPMUnknown* boss);

protected:
	/** Performs notification. */
	virtual void DoNotify();

	/** Implements command. */
	virtual void Do();

	/** Sets command name. */
	virtual PMString* CreateName();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(ExtLinkSetObjectModificationStateCmd, kExtLinkSetObjectModificationStateCmdImpl)


/* Constructor
*/
ExtLinkSetObjectModificationStateCmd::ExtLinkSetObjectModificationStateCmd(IPMUnknown* boss) :
	Command(boss)
{
}


/* Do
*/
void ExtLinkSetObjectModificationStateCmd::Do()
{
	ErrorCode status = kFailure;
	do						
	{
		InterfacePtr<IIntData> 
			cmdData (this, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}
		
		// Get the ItemList
		const UIDList* itemList = this->GetItemList();
		if (itemList == nil || itemList->IsEmpty())
		{
			break;
		}
		
		for (int32 i=0; i<itemList->Length(); i++)
		{
			UIDRef uidRef = itemList->GetRef(i);
			InterfacePtr<ILink> 
				theLink(uidRef, UseDefaultIID());
			ASSERT(theLink);
			if (theLink != nil)
			{
				theLink->SetObjectModificationState((ILink::ObjectModificationState)cmdData->Get());
			}
			
		}

		status = kSuccess;

	} while(kFalse);		

	// Handle any errors
	if (status != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(status);
	}
}

/* DoNotify
*/
void ExtLinkSetObjectModificationStateCmd::DoNotify()
{
	InterfacePtr<IWorkspace> theWorkSpace(GetExecutionContextSession()->QueryWorkspace());
	InterfacePtr<ISubject> subject(theWorkSpace, IID_ISUBJECT);
	if (subject)
	{
		subject->ModelChange(kExtLinkSetObjectModificationStateCmdBoss, IID_IEXTLINKOPTIONS, this);
	}
}


/* CreateName
*/
PMString* ExtLinkSetObjectModificationStateCmd::CreateName()
{
	return new PMString(kExtLinkSetObjectModStateCmdKey);
}


