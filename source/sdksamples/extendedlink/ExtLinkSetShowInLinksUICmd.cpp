//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkSetShowInLinksUICmd.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISubject.h"
#include "IWorkspace.h"
#include "IBoolData.h"
#include "ILink.h"
#include "ILinkManager.h"

// General includes:
#include "CmdUtils.h"
#include "ErrorUtils.h"
#include "Command.h"
#include "ListLazyNotificationData.h"

// Project includes:
#include "ExtLinkID.h"


/** Implementation of command to change the flag in ILink that indicate if the link
	should show up in InDesign's links UI.

	@see ILink
	@ingroup extendedlink
*/

class ExtLinkSetShowInLinksUICmd : public Command
{
public:
	/** Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.*/
	ExtLinkSetShowInLinksUICmd(IPMUnknown* boss);

protected:
	/** Performs notification. */
	virtual void DoNotify();

	/** Implements command. */
	virtual void Do();

	/** Sets command name. */
	virtual PMString* CreateName();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(ExtLinkSetShowInLinksUICmd, kExtLinkSetShowInLinksUICmdImpl)


/* Constructor
*/
ExtLinkSetShowInLinksUICmd::ExtLinkSetShowInLinksUICmd(IPMUnknown* boss) :
	Command(boss)
{
}


/* Do
*/
void ExtLinkSetShowInLinksUICmd::Do()
{
	ErrorCode status = kFailure;
	do						
	{
		InterfacePtr<IBoolData> 
			cmdData (this, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}
		
		// Get the ItemList
		const UIDList* itemList = this->GetItemList();
		if (itemList == nil || itemList->IsEmpty())
		{
			break;
		}
		
		for (int32 i=0; i<itemList->Length(); i++)
		{
			UIDRef uidRef = itemList->GetRef(i);
			InterfacePtr<ILink> 
				theLink(uidRef, UseDefaultIID());
			ASSERT(theLink);
			if (theLink != nil)
			{
				theLink->SetShowInUI(cmdData->GetBool());
			}
			
		}

		status = kSuccess;

	} while(kFalse);		

	// Handle any errors
	if (status != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(status);
	}
}

/* DoNotify
*/
void ExtLinkSetShowInLinksUICmd::DoNotify()
{
	const UIDList* listOfLinks = GetItemList();
	IDataBase* dataBase = listOfLinks->GetDataBase();
	ASSERT(dataBase);
	InterfacePtr<ISubject> docSubject(dataBase, dataBase->GetRootUID(), UseDefaultIID());
	if(docSubject)
	{
		ListLazyNotificationData<ILinkManager::ChangeData> *linksData = new ListLazyNotificationData<ILinkManager::ChangeData>;
		for (int32 i = 0; i < listOfLinks->Length(); ++i)
		{
			linksData->ItemAdded(ILinkManager::ChangeData(ILinkManager::ChangeData::kLink, listOfLinks->At(i)));
		}
		docSubject->ModelChange(kExtLinkSetShowInLinksUICmdBoss, IID_ILINKDATA_CHANGED, this, linksData);
	}
}


/* CreateName
*/
PMString* ExtLinkSetShowInLinksUICmd::CreateName()
{
	return new PMString(kExtLinkSetShowInLinksUICmdKey);
}


