//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkFacade.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interfaces:
#include "IDocument.h"
#include "IStoryList.h"
#include "ITextModel.h"
#include "XMLReference.h"
#include "IXMLUtils.h"
#include "IXMLReferenceData.h"
#include "IItemLockData.h"
#include "ITextStoryThread.h"
#include "IIDXMLElement.h"
#include "IPointerStreamData.h"
#include "IBoolData.h"
#include "IIntData.h"
#include "ILinkManager.h"
#include "ILink.h"
#include "IActiveContext.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IWorkspace.h"
#include "IImportManagerOptions.h"
#include "IImportManager.h"
#include "ITextSelectionSuite.h"
#include "IImportSuite.h"
#include "ISelectionUtils.h"
#include "IExtLinkChangeOptionsCmdData.h"
#include "CAlert.h"

// General:
#include "URI.h"
#include "IURIUtils.h"
#include "CmdUtils.h"
#include "CPMUnknown.h"
#include "K2Vector.h"
#include "Utils.h"
#include "K2Vector.tpp"
#include "LinkQuery.h"
#include "LinkResourceQuery.h"

// Project:
#include "ExtLinkUtils.h"
#include "IExtLinkOptions.h"
#include "IExtLinkFacade.h"
#include "ExtLinkConst.h"
#include "ErrorUtils.h"
#include "SDKFileHelper.h"
#include "ExtLinkID.h"
#include "ISDKODBCWrapper.h"
#include "SDKODBCCache.h"

/** From SDK sample; IExtLinkFacade implementation.

	@ingroup ExtLink
*/
class ExtLinkFacade : public CPMUnknown<IExtLinkFacade>
{
public:

	/** Constructor
		@param boss on which this interface is aggregated.
	*/
	ExtLinkFacade(IPMUnknown* boss);

	/** Destructor.
	*/
	virtual ~ExtLinkFacade();

	/** See IExtLinkFacade::CanInsertData.
	*/
	bool16 CanInsertData(ITextModel* iTextModel, TextIndex textIndex, int32 length);

	/** See IExtLinkFacade::InsertSelectedRecords.
	*/
	ErrorCode InsertSelectedRecords(ITextModel* iTextModel, TextIndex& textIndex, const URIList& uriList);

	/** See IExtLinkFacade::URIToIDFile */
	bool URIToIDFile(const URI& uri, IDFile& file) const;

	/**	See IExtLinkFacade::GetAssetValueFromResourceStream.
	 */
	bool GetAssetValueFromResourceStream(IPMStream* iPMStream, WideString& value) const;

	/**	See IExtLinkFacade::GetAssetValueFromURI.
	 */
	bool GetAssetValueFromURI(const URI& uri, WideString& value) const;

	/**	See IExtLinkFacade::GetAssetValueFromURI.
	 */
	bool GetAssetVersionFromURI(const URI& uri, WideString& version) const;

	/**	See IExtLinkFacade::GetAssetValueFromURI.
	 */
	ErrorCode ProcessSetOptionsCommand(const K2Vector<ExtLinkOptionsData>& options, bool16 autoRefresh);

	/**	See IExtLinkFacade::ProcessShowInLinksUICommand.
	 */
	ErrorCode ProcessShowInLinksUICommand(const bool16 showInUI, UIDList& linksUIDList);

	/**	See IExtLinkFacade::ProcessShowInLinksUICommand.
	 */
	ErrorCode ProcessShowInLinksUICommand(const bool16 showInUI, IDocument* document);

	/**	See IExtLinkFacade::ProcessSetLinkObjectModStateCommand.
	 */
	ErrorCode ProcessSetLinkObjectModStateCommand(const ILink::ObjectModificationState state, UIDList& linksUIDList);

	/**	See IExtLinkFacade::ProcessSetLinkResourceModStateCommand.
	 */
	ErrorCode ProcessSetLinkResourceModStateCommand(const ILink::ResourceModificationState state, UIDList& linksUIDList);

	/**	See IExtLinkFacade::CreateExtLinkPointerStreamWrite.
	 */
	IPMStream* CreateExtLinkPointerStreamWrite(char *buffer, int32 len) const;

	/**	See IExtLinkFacade::RebuildODBCCacheData.
	 */
	void RebuildODBCCacheData();

	/**	See IExtLinkFacade::SetExtLinksShowInLinksPanel.
	 */
	ErrorCode SetExtLinksShowInLinksPanel(bool16 showInUI);
	
private:
	/** Is the element one of our special ExtLink elements?.
	*/
	bool16 IsExtLinkElement(XMLReference elementRef);
	
	void CollectExtLinkItems(IDocument* document, UIDList& extLinkList);

	void CollectExtLinkURIs(IDocument* document, URIList& extLinkURIList);
	
};

/* Make the implementation available to the application.
*/
CREATE_PMINTERFACE(ExtLinkFacade, kExtLinkFacadeImpl)

/*
*/
ExtLinkFacade::ExtLinkFacade(IPMUnknown* boss) : CPMUnknown<IExtLinkFacade>(boss)
{

}

/*
*/
ExtLinkFacade::~ExtLinkFacade()
{
}

bool16 ExtLinkFacade::IsExtLinkElement(XMLReference elementRef)
{
	if (elementRef != kInvalidXMLReference)
	{
		WideString ExtLinkStr(kExtLinkTagNamePrefix);
		InterfacePtr<IIDXMLElement> element(elementRef.Instantiate());
		WideString eleName = element->GetTagString();		
		if(eleName.Contains(ExtLinkStr))
			return kTrue;
	}

	return kFalse;
}
/*
*/
bool16 ExtLinkFacade::CanInsertData(ITextModel* textModel, TextIndex textIndex, int32 length)
{
	ErrorCode result = kExtLinkTargetTextAlreadyTaggedErrorCode;
	do {
		// First test if we have an image folder path set up in session's workspace, 
		// this needs to be set up before we insert any image data or we can't find the files
		PMString imageFolderPath = ExtLinkUtils::GetSelectedFolderPath();
		SDKFileHelper fileHelper(imageFolderPath);
		IDFile imgDirIDFile = fileHelper.GetIDFile();
		if(!imgDirIDFile.Exists()){
			result = kExtLinkNoImageFolderSpecifiedErrorCode;
			break;
		}
		PMString authority = ExtLinkUtils::GetSelectedDataSource();
		if (authority == "") {
			result = kExtLinkNoAuthoritySpecifiedErrorCode;
			break;
		}
		PMString table = ExtLinkUtils::GetSelectedTableName();
		if (authority == "") {
			result = kExtLinkNoTableSpecifiedErrorCode;
			break;
		}

		InterfacePtr<IItemLockData> itemLockData(textModel, UseDefaultIID());
		if (itemLockData != nil) {
			if (itemLockData->GetInsertLock() ) 
			{	// Can't insert  data if the story is  locked.
				result = kExtLinkTargetStoryLockedErrorCode;
				break;
			}
		}

		RangeData targetedRange = RangeData(textIndex, textIndex + length, RangeData::kLeanForward);
		TextIndex storyThreadStart = 0;
		int32 storyThreadSpan = 0;
		textModel->GetStoryThreadSpan(textIndex, &storyThreadStart, &storyThreadSpan);

		// see if the story thread has XML tag
		XMLReference parentXMLRef(Utils<IXMLUtils>()->GetStoryThreadXMLReference(textModel, textIndex));
		if (parentXMLRef == kInvalidXMLReference)
		{	// If the story do not have XML tags at all, we can insert
			result = kSuccess;
			break;	
		}

		// story thread is tagged, see if the selection falls under a single parent element
		bool16 isIP(length == 0);
		TextIndex endIndex = textIndex + length;
		XMLReference endParentXMLRef;
		int32 startIndexInParent(0), endIndexInParent(0);
		K2Vector<XMLReference> childRefList;
		ErrorCode calParent = Utils<IXMLUtils>()->CalcXMLParent(parentXMLRef, startIndexInParent, childRefList, endParentXMLRef, endIndexInParent, textIndex, endIndex, textModel);
		if (length == 0 || calParent == IXMLUtils::kSuccessCalc)
		{
			// Find a parent, see if it is any of our special ExtLink elements
			if (this->IsExtLinkElement(parentXMLRef))
				break; // If it is a ExtLink special element, don't not insert. (While technically we could allow nested XML tags)
			else
				result = kSuccess;
		}
		else 
		{
			break; // we have unmatching tags, selected across the boundary, bail out
		}

		// If we get here, insert is OK.
		result = kSuccess;

	} while(false);
	return result == kSuccess;
}

/*
*/
ErrorCode ExtLinkFacade::InsertSelectedRecords(ITextModel* iTextModel, TextIndex& textIndex, const URIList& uriList)
{
	
	ErrorCode status = kFailure; // Will indicate sequence init/fail/success.
	do
	{	
		// Begin the sequence:
		CmdUtils::SequencePtr seq;
		// Name the command sequence:
		seq->SetName(kExtLinkInsertAllSequenceStringKey);
#if 1
		int32 uriCount = uriList.GetURICount();
		if (uriCount > 0)
		{
			UIDRef storyRef = ::GetUIDRef(iTextModel);

			// Create XML tags is not yet created
			IDataBase* db = ::GetDataBase(iTextModel);
					
			InterfacePtr<IXMLTagList> tagList(Utils<IXMLUtils>()->QueryXMLTagList(db));
			ASSERT(tagList);
			if(!tagList) {
				return status;
			}
			
			WideString tableTag(kExtLinkTableTag);
			WideString keyTag(kExtLinkKeyTag); 
			WideString fieldTag(kExtLinkFieldTag); 
			UIDRef tableRef = ExtLinkUtils::AcquireOneTag(tagList, tableTag);
			UIDRef keyRef = ExtLinkUtils::AcquireOneTag(tagList, keyTag);
			UIDRef fieldRef = ExtLinkUtils::AcquireOneTag(tagList, fieldTag);
			if ((tableRef ==kInvalidUIDRef)||(keyRef ==kInvalidUIDRef)||(fieldRef ==kInvalidUIDRef))
				return status;

			// Insert each record passed by this method's parameters.
			WideString key;
			WideString fieldName;
			WideString fieldValue;
			
			int32 recordIndex = 0;
			TextIndex tableStartIndex = textIndex;
			TextIndex recordStartIndex = tableStartIndex;
			TextIndex currentPos = recordStartIndex;
			URI recordURI;
			for (int32 i=0; i<uriCount; i++)
			{
				URI linkURI = uriList.GetNthURI(i);
				K2Vector<ODBCCacheData> result =  SDKODBCCache::Instance().GetCacheData(linkURI);
				if (result.size() > 0)
				{
					fieldValue = result[0].fFieldData;
					CAlert::InformationAlert(fieldValue);
					if (!fieldValue.IsNull())
					{
						TextIndex fieldStartIndex = recordStartIndex;
						/*** TODO hack alert ***/
						WideString fragment = linkURI.GetComponent(URI::kFragment);
						CAlert::InformationAlert(fragment);
						if (fragment == kExtLinkDBColumnImage)	//image
						{
							InterfacePtr<IK2ServiceRegistry>    registry(GetExecutionContextSession(), UseDefaultIID());
							InterfacePtr<IK2ServiceProvider> importMgrService(registry->QueryDefaultServiceProvider(kImportManagerService));
							InterfacePtr<IImportManager> importMgr(importMgrService, UseDefaultIID());
							InterfacePtr<IImportManagerOptions> importOptions(importMgr, IID_IIMPORTMANAGEROPTIONS);
							bool16 retainFormat = importOptions->GetRetainFormat();
							bool16 convertQuotes = importOptions->GetConvertQuotes();
							bool16 replaceSelectedItem = importOptions->GetReplaceSelectedItem();
							bool16 applyCJKGrid = importOptions->GetApplyCJKGrid();

							InterfacePtr<IImportSuite> importSuite(static_cast<IImportSuite* >
																 ( Utils<ISelectionUtils>()->QuerySuite(IImportSuite::kDefaultIID)));
							ASSERT(importSuite != nil);
							if (importSuite && importSuite->CanImport())
							{
								bool16 importSuccess = importSuite->DoImport(linkURI, db, replaceSelectedItem, kSuppressUI, retainFormat, convertQuotes, applyCJKGrid);
								currentPos +=  1;
								if (importSuccess)
								{
									// IImportSuite::DoImport placed cursor in front of the inline, which is not what we want, move the cursor after the inline so we can continue the next fields.
									Utils<ISelectionUtils>selectionUtils;
									InterfacePtr<ISelectionManager> selectionManager(selectionUtils->QueryActiveSelection ());
									InterfacePtr<ITextSelectionSuite> selection(selectionUtils->QueryTextSelectionSuite(selectionManager));
									if (selection)
									{
										RangeData ab(currentPos, currentPos+1);
										selection->SetTextSelection(storyRef, ab, Selection::kDontScrollSelection, &ab);
									}
									status = kSuccess;
								}
							}
						}
						else
						{
							CAlert::InformationAlert("A4");
							WideString prefix;
							if (fragment == kExtLinkDBColumnSKU)
								prefix = kExtLinkCatalogSKUTitle;
							else if (fragment == kExtLinkDBColumnName)
								prefix = kExtLinkCatalogNameTitle;
							else if (fragment == kExtLinkDBColumnPrice)
								prefix = kExtLinkCatalogPriceTitle;
							else if (fragment == kExtLinkDBColumnQuantity)
								prefix = kExtLinkCatalogQuantityTitle;
							else if (fragment == kExtLinkDBColumnDescription)
								prefix = kExtLinkCatalogDescriptionTitle;
							int32 prefixLen = prefix.Length();
							// Create a buffer for the string to be inserted
							boost::shared_ptr<WideString> wideVal(new WideString(prefix+fieldValue));
							status = ExtLinkUtils::ReplaceText(iTextModel, fieldStartIndex, 0, wideVal);
							currentPos =  fieldStartIndex + wideVal->Length();
							
							// apply field tag
							status = ExtLinkUtils::TagTextWithAttributeValue(fieldRef.GetUID(), storyRef,
								fieldStartIndex+prefixLen, currentPos, kExtLinkFieldNameAttribute, fragment, linkURI);
							currentPos += numZWCharsAdded; 
						}
						if (status == kSuccess)
						{
							CAlert::InformationAlert("A5");
							if (status == kSuccess)
							{
								// Insert delimiter after each field, outside of the tag
								// Create a buffer for the string to be inserted
								boost::shared_ptr<WideString> delimiter(new WideString(kWideString_ExtLinkCarriageReturn));
								status = ExtLinkUtils::ReplaceText(iTextModel, currentPos,0, delimiter);
								currentPos +=  delimiter->Length();
							}
						}

						fieldStartIndex = currentPos; //update for next field

						// Now tag all the field values for each record on one line
	/*					if (status == kSuccess)
						{
							linkURI.RemoveComponent(URI::kFragment);
							status = ExtLinkUtils::TagTextWithAttributeValue(keyRef.GetUID(), storyRef,
							recordStartIndex, currentPos, kExtLinkRecordKeyAttribute, key, linkURI);
							if (status != kSuccess)
								break;
							currentPos += numZWCharsAdded; 
							
							// Insert a line breaker for each record
							boost::shared_ptr<WideString> recBreaker(new WideString(kWideString_ExtLinkCarriageReturn));
							status = ExtLinkUtils::ReplaceText(iTextModel, currentPos, 0, recBreaker);
							currentPos +=  recBreaker->Length();

						}
	 */
						CAlert::InformationAlert("A6");
						//prepare for next record
						recordStartIndex = currentPos;
						TRACEFLOW("ExtLink", "ExtLinkFacade::InsertSelectedRecords: Process one row done\n");
						recordURI = linkURI;
					}
				}
			}
			
			CAlert::InformationAlert("A7");
			if (status != kSuccess)
			{
				ErrorUtils::PMSetGlobalErrorCode(status);
			}
			else
			{
				InterfacePtr<IDocument> document(db, db->GetRootUID(), UseDefaultIID());
				status = ProcessShowInLinksUICommand(ExtLinkUtils::IsDisplayExtLinksInLinksPanel(), document);				
			}
			CAlert::InformationAlert("A8");
		}
#endif		

	} while (false); // Only one time.
	
	
	return status;
}


/* 
*/
bool ExtLinkFacade::URIToIDFile(const URI& uri, IDFile& file) const
{
	bool retCode = false;
	static const WideString kMyScheme(kExtLinkScheme);
//sdd	PMString dbAuthorityName = ExtLinkUtils::GetSelectedDataSource();
	WideString kMyAuthority(ExtLinkUtils::GetSelectedDataSource());
//	static const WideString kMyAuthority(kExtLinkAuthority);	// *** TODO "image" field
	WideString scheme = uri.GetComponent(URI::kScheme);
	WideString authority = uri.GetComponent(URI::kAuthority);
	WideString field = uri.GetComponent(URI::kFragment);
	WideString myKey = uri.GetComponent(URI::kQuery);
	if (scheme != kMyScheme || authority != kMyAuthority || field!=kExtLinkDBColumnImage) {
		return false;
	}
	// our uri with image fragment has the following format:
	// odbc://MySQL?table=inventory&sku='AB-219'#image
	// first get the database path from the uri, create a file helper from the path
	// *** TODO, these are all temp cause I can't do an uri based query yet.
	// Create An Instance Of The ODBCWrapper 
	K2Vector<ODBCCacheData> result =  SDKODBCCache::Instance().GetCacheData(uri);
	if (result.size() > 0)
	{
		WideString imagePath = result[0].fFieldData;
		if (!imagePath.IsNull())
		{
			PMString imageFileName(imagePath);
			PMString imageFolderPath = ExtLinkUtils::GetSelectedFolderPath();
			PMString imageFullPath = imageFolderPath + kPlatformPathDelimiter + imageFileName;
			SDKFileHelper fileHelper(imageFullPath);
			if (fileHelper.IsExisting())
			{
				file = fileHelper.GetIDFile();
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	else
		return false;
}


bool ExtLinkFacade::GetAssetValueFromResourceStream(IPMStream* iPMStream, WideString& value) const
{
	// Reset the stream to the start of the file
	iPMStream->Seek(0, kSeekFromStart);

	// Process the stream
	uchar oneChar;
	PMString workString;
	workString.SetTranslatable(kFalse);
	// Scan the characters in the first line:
	iPMStream->XferByte(oneChar);
	while (iPMStream->GetStreamState()==kStreamStateGood)
	{
/*
		switch (oneChar)
		{

#if defined(WINDOWS)
				case kuchar_CR:
#elif defined(MACINTOSH)
				case kuchar_LF:
#endif
				case kuchar_Separator: // end of field
					{
						workString.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
						workString.SetTranslatable(kFalse);
						WideString temp(workString);
						value = temp;
						workString.Clear();
						break;
					}


				default: //data
					{
						workString.Append((char)oneChar);
						break;
					}
		} // end switch
		// End of line?

#if defined(WINDOWS)
		if (oneChar == kuchar_CR)
#elif defined(MACINTOSH)
		if (oneChar == kuchar_LF)
#endif
		{
			break;
		}
*/
		workString.Append((char)oneChar);
		// Read the next character
		iPMStream->XferByte(oneChar);

	} // end while streamstategood
	
	WideString temp(workString);
	value = temp;
	return true;
}


bool ExtLinkFacade::GetAssetVersionFromURI(const URI& uri, WideString& version) const
{
	bool status = false;
	
	do
	{	
		URI	versionURI = uri;
		versionURI.RemoveComponent(URI::kFragment);
		versionURI.SetComponent(URI::kFragment, kExtLinkDBColumnVersion);
		K2Vector<ODBCCacheData> result =  SDKODBCCache::Instance().GetCacheData(versionURI);
		if (result.size() > 0) {
			version = result[0].fFieldData;	// caller should always pass in an URI that represents single field, so we only care about the first result
			if (!version.IsNull()) {
				status = true;
			}
		}
	} while (false); // Only one time.


	return status;
}


bool ExtLinkFacade::GetAssetValueFromURI(const URI& uri, WideString& value) const
{
	bool status = false;
	
	do
	{	
#if 1
		K2Vector<ODBCCacheData> result =  SDKODBCCache::Instance().GetCacheData(uri);
		if (result.size() > 0) {
			value = result[0].fFieldData;	// caller should always pass in an URI that represents single field, so we only care about the first result
			if (!value.IsNull()) {
				status = true;
			}
		}
#endif
	} while (false); // Only one time.


	return status;
}

/* ProcessSetOptionsCommand
*/
ErrorCode ExtLinkFacade::ProcessSetOptionsCommand(const K2Vector<ExtLinkOptionsData>& options, bool16 autoRefresh)
{
	ErrorCode retval = kFailure;
	do
	{
		// Begin the sequence:
		CmdUtils::SequencePtr seq;
		// Name the command sequence:
		seq->SetName(kExtLinkChangeOptionsCmdKey);
		
		InterfacePtr<IWorkspace>
			iSessionWorkspace(GetExecutionContextSession()->QueryWorkspace());
		ASSERT(iSessionWorkspace);
		if(iSessionWorkspace == nil)
		{
			break;
		}

		InterfacePtr<IExtLinkOptions>
			iOptions(iSessionWorkspace, UseDefaultIID());
		ASSERT(iOptions);
		if(!iOptions)
		{
			break;
		}
		InterfacePtr<ICommand>
			modOptionsCmd (CmdUtils::CreateCommand(kExtLinkChangeOptionsCmdBoss));
		ASSERT(modOptionsCmd);
		if(!modOptionsCmd)
		{
			break;
		}	
		modOptionsCmd->SetItemList(UIDList(iSessionWorkspace));

		InterfacePtr<IBoolData> iAutoRefresh(modOptionsCmd, IID_IAUTOREFRESHSTATUS);
		if(iAutoRefresh == nil)
		{
			break;
		}
		iAutoRefresh->Set(autoRefresh);
		
		InterfacePtr<IExtLinkChangeOptionsCmdData>
			cmdData (modOptionsCmd, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}

		K2Vector<ExtLinkOptionsData>::const_iterator iter;
		for(iter = options.begin(); iter != options.end(); ++iter)
		{
			cmdData->AddOption(ExtLinkOptionsData(*iter));	// template path
		}
		retval = CmdUtils::ProcessCommand(modOptionsCmd);
		ASSERT(retval == kSuccess);	

	} while(kFalse);

	return retval;
}

ErrorCode ExtLinkFacade::SetExtLinksShowInLinksPanel(bool16 showInUI)
{
	IDocument* document = nil;
	IActiveContext* context = GetExecutionContextSession()->GetActiveContext();
	if (context)
	{			
		document = context->GetContextDocument();
	}
	
	UIDRef workspaceRef = kInvalidUIDRef;
	if(document)
	{
		workspaceRef = document->GetDocWorkSpace();
	}
	else
	{
		InterfacePtr<IWorkspace> iSessionWorkspace(GetExecutionContextSession()->QueryWorkspace());
		workspaceRef = ::GetUIDRef(iSessionWorkspace);
	}

	CmdUtils::SequencePtr seq;
	// Name the command sequence:
	seq->SetName(kExtLinkSetShowInLinksUICmdKey);

	InterfacePtr<ICommand>
	setShowInUIPrefCmd (CmdUtils::CreateCommand(kExtLinkSetShowInUIPrefCmdBoss));
	ASSERT(setShowInUIPrefCmd);
	if(!setShowInUIPrefCmd)
	{
		return kFailure;
	}	
	setShowInUIPrefCmd->SetItemList(UIDList(workspaceRef));
	
	InterfacePtr<IBoolData> iBoolData(setShowInUIPrefCmd, UseDefaultIID());
	if(iBoolData == nil)
	{
		return kFailure;
	}	
	iBoolData->Set(showInUI);
	
	if(CmdUtils::ProcessCommand(setShowInUIPrefCmd) == kSuccess)
	{
		if(document)
		{
			return ProcessShowInLinksUICommand(showInUI, document);
		}
		else 
		{
			return kSuccess;
		}
	}
	return kFailure;
}


ErrorCode ExtLinkFacade::ProcessShowInLinksUICommand(const bool16 showInUI, IDocument* document)
{
	UIDList extLinkList(::GetDataBase(document));
	CollectExtLinkItems(document, extLinkList);
	return extLinkList.size() > 0 ? ProcessShowInLinksUICommand(showInUI, extLinkList) : kSuccess;
}

/* ProcessSetOptionsCommand
*/
ErrorCode ExtLinkFacade::ProcessShowInLinksUICommand(const bool16 showInUI, UIDList& linksUIDList)
{
	ErrorCode retval = kFailure;
	do
	{
		InterfacePtr<ICommand>
			setShowInUICmd (CmdUtils::CreateCommand(kExtLinkSetShowInLinksUICmdBoss));
		ASSERT(setShowInUICmd);
		if(!setShowInUICmd)
		{
			break;
		}	
		setShowInUICmd->SetItemList(linksUIDList);

		InterfacePtr<IBoolData>
			cmdData (setShowInUICmd, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}

		cmdData->Set(showInUI);
		retval = CmdUtils::ProcessCommand(setShowInUICmd);
		ASSERT(retval == kSuccess);	
	
	} while(kFalse);

	return retval;
}

void ExtLinkFacade::RebuildODBCCacheData()
{
	PMString dbAuthorityName = ExtLinkUtils::GetSelectedDataSource();
	PMString dbTableName = ExtLinkUtils::GetSelectedTableName();
	PMString tableQueryPrefix = PMString(kExtLinkScheme) + "://" + dbAuthorityName + "/" + "?table=" + dbTableName;

	URI myURI(tableQueryPrefix.GetPlatformString());
	K2Vector<WideString> keyValues;
	K2Vector<PMString> fieldNames;
	SDKODBCCache::Instance().Rebuild(myURI, PMString("sku"), keyValues, fieldNames);	
}

/* ProcessSetLinkObjectModStateCommand
*/
ErrorCode ExtLinkFacade::ProcessSetLinkObjectModStateCommand(const ILink::ObjectModificationState state, UIDList& linksUIDList)
{
	ErrorCode retval = kFailure;
	do
	{
		InterfacePtr<ICommand>
			setObjModStateCmd (CmdUtils::CreateCommand(kExtLinkSetObjectModificationStateCmdBoss));
		ASSERT(setObjModStateCmd);
		if(!setObjModStateCmd)
		{
			break;
		}	
		setObjModStateCmd->SetItemList(linksUIDList);

		InterfacePtr<IIntData>
			cmdData (setObjModStateCmd, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}

		cmdData->Set((int32)state);
		retval = CmdUtils::ProcessCommand(setObjModStateCmd);
		ASSERT(retval == kSuccess);	
	
	} while(kFalse);

	return retval;
}


/* ProcessSetLinkObjectModStateCommand
*/
ErrorCode ExtLinkFacade::ProcessSetLinkResourceModStateCommand(const ILink::ResourceModificationState state, UIDList& linksUIDList)
{
	ErrorCode retval = kFailure;
	do
	{
		InterfacePtr<ICommand>
			setRsrcModStateCmd (CmdUtils::CreateCommand(kExtLinkSetResourceModificationStateCmdBoss));
		ASSERT(setRsrcModStateCmd);
		if(!setRsrcModStateCmd)
		{
			break;
		}	
		setRsrcModStateCmd->SetItemList(linksUIDList);

		InterfacePtr<IIntData>
			cmdData (setRsrcModStateCmd, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}

		cmdData->Set((int32)state);
		retval = CmdUtils::ProcessCommand(setRsrcModStateCmd);
		ASSERT(retval == kSuccess);	
	
	} while(kFalse);

	return retval;
}

IPMStream* ExtLinkFacade::CreateExtLinkPointerStreamWrite(char *buffer, int32 len) const
{
    IPMStream *stream = (IPMStream *)::CreateObject(kExtLinkPointerStreamWriteBoss, IID_IPMSTREAM);
    InterfacePtr<IPointerStreamData> data(stream, IID_IPOINTERSTREAMDATA);
    data->Set(buffer, len);
    if (!stream->Open())    {
        stream->Release();
        return nil;
	}
    stream->SetSwapping(kFalse);
    return stream;
}

void ExtLinkFacade::CollectExtLinkItems(IDocument* document, UIDList& extLinkList)
{
	IDataBase* dataBase = ::GetDataBase(document);
	UIDList resultUIDs(dataBase);
	InterfacePtr<ILinkManager> linkMgr(document, UseDefaultIID());
	LinkQuery query; 
	linkMgr->QueryLinks(query, kIDLinkClientID, resultUIDs);

	// -->  Add all links with URIs using my scheme to the map
	for(UIDList::iterator iter = resultUIDs.begin(); iter != resultUIDs.end(); ++iter)
	{
		UID linkUID = *iter;
		InterfacePtr<ILink> theLink(dataBase, linkUID, UseDefaultIID());
		if(theLink) 
		{
			InterfacePtr<ILinkResource> theLinkResource(dataBase, theLink->GetResource(), UseDefaultIID()); 
			if (theLinkResource) 
			{
				WideString theScheme = theLinkResource->GetURI().GetComponent(URI::kScheme);
				if(theScheme == WideString(kExtLinkScheme))
				{
					extLinkList.push_back(*iter);
				}
			}
		}
	}
}

void ExtLinkFacade::CollectExtLinkURIs(IDocument* document, URIList& extLinkURIList)
{
	IDataBase* dataBase = ::GetDataBase(document);
	UIDList resultUIDs(dataBase);
	InterfacePtr<ILinkManager> linkMgr(document, UseDefaultIID());
	LinkQuery query; 
	linkMgr->QueryLinks(query, kIDLinkClientID, resultUIDs);
	
	// -->  Add all links with URIs using my scheme to the map
	for(UIDList::iterator iter = resultUIDs.begin(); iter != resultUIDs.end(); ++iter)
	{
		UID linkUID = *iter;
		InterfacePtr<ILink> theLink(dataBase, linkUID, UseDefaultIID());
		if(theLink) 
		{
			InterfacePtr<ILinkResource> theLinkResource(dataBase, theLink->GetResource(), UseDefaultIID()); 
			if (theLinkResource) 
			{
				WideString theScheme = theLinkResource->GetURI().GetComponent(URI::kScheme);
				if(theScheme == WideString(kExtLinkScheme))
				{
					extLinkURIList.AddURI(theLinkResource->GetURI());
				}
			}
		}
	}
	
}

// End ExtLinkFacade.cpp
