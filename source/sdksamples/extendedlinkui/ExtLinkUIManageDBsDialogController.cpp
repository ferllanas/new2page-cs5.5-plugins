//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIManageDBsDialogController.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActiveContext.h"
#include "IDropDownListController.h"
#include "IExtLinkSuite.h"
#include "ISDKODBCWrapper.h"
#include "IPanelControlData.h"
#include "IStringListControlData.h"
#include "ITextControlData.h"
#include "ISelectionManager.h"

// General includes:
#include "CAlert.h"
#include "CDialogController.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "Utils.h"
#include "FileUtils.h"
#include "SDKFileHelper.h"
#include "ExtLinkUtils.h"
#include "ErrorUtils.h"
#include "URI.h"
#include "URIList.h"
#include <vector>

// Project includes:
#include "IExtLinkOptions.h"
#include "IExtLinkFacade.h"
#include "ExtLinkUIID.h"


/** ExtLinkUIManageDBsDialogController
	Methods allow for the initialization, validation, and application of dialog widget
	values.
	Implements IDialogController based on the partial implementation CDialogController.

	@ingroup extendedlinkui
*/
class ExtLinkUIManageDBsDialogController : public CDialogController
{
	public:
		/** Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		ExtLinkUIManageDBsDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** Destructor.
		*/
		virtual ~ExtLinkUIManageDBsDialogController() {}

		/** Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
	       virtual void InitializeDialogFields(IActiveContext* dlgContext);

		/** Validate the values in the widgets.
			By default, the widget with ID kOKButtonWidgetID causes
			ValidateFields to be called. When all widgets are valid,
			ApplyFields will be called.
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
	       virtual WidgetID ValidateDialogFields(IActiveContext* myContext);


		/** Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId);

	private:


		/** Retrieve values for records from the database, and init dialog fields.
		*/
		virtual void InitializeDBData(IActiveContext* dlgContext);

};

CREATE_PMINTERFACE(ExtLinkUIManageDBsDialogController, kExtLinkUIManageDBsDialogControllerImpl)

/* ApplyFields
*/
void ExtLinkUIManageDBsDialogController::InitializeDialogFields(IActiveContext* dlgContext)
{
	do
	{
		CDialogController::InitializeDialogFields(dlgContext);	

		if (dlgContext == nil)
		{
			ASSERT_FAIL("Active context is invalid");
			break;
		}

		this->InitializeDBData(dlgContext);

	} while (false);
}


/* ApplyFields
*/
void ExtLinkUIManageDBsDialogController::InitializeDBData(IActiveContext* dlgContext)
{
	do
	{
		PMString theText(ExtLinkUtils::GetSelectedDataSource());
		if (theText == "") {
			theText = PMString(kExtLinkUIDefaultAuthorityStringKey);
		}
		this->SetTextControlData(kExtLinkUIAuthorityTextWidgetID, theText);

		// initialize shared folder string from saved data, if any
		theText = PMString(kExtLinkUIDefaultSharedFolderStringKey);
		PMString imageFolderPath = ExtLinkUtils::GetSelectedFolderPath();
		SDKFileHelper fileHelper(imageFolderPath);
		if (FileUtils::IsDirectory(fileHelper.GetIDFile())){
			theText = imageFolderPath;
		}
		this->SetTextControlData(kExtLinkUISharedFolderTextWidgetID, theText);	
		this->SetTriStateControlData(kExtLinkUIAutoRefreshCheckBoxWidgetID, ExtLinkUtils::IsAutoRefreshStatus());

/*		sdd - removed to force the use of the default table name, "inventory"

		theText = ExtLinkUtils::GetSelectedTableName();
		if (theText == "") {
			theText = PMString(kExtLinkUIDefaultTableStringKey);
		}
		this->SetTextControlData(kExtLinkUITableNameTextWidgetID, theText);
*/

	} while (false);
}


/* ValidateFields
*/
WidgetID ExtLinkUIManageDBsDialogController::ValidateDialogFields(IActiveContext* myContext)
{
	WidgetID badWidgetId = kDefaultWidgetId;

	do 
	{		
	} while(false);
	
	return badWidgetId;
}

/* ApplyFields
*/
void ExtLinkUIManageDBsDialogController::ApplyDialogFields(IActiveContext* myContext, const WidgetID& widgetId)
{
	do 
	{
		InterfacePtr<IPanelControlData> panelData(this, IID_IPANELCONTROLDATA);
		if (panelData == nil)
		{
			ASSERT_FAIL("IPanelControlData is invalid");
			break;
		}

		PMString authorityName = this->GetTextControlData(kExtLinkUIAuthorityTextWidgetID);
		PMString sharedFolder = this->GetTextControlData(kExtLinkUISharedFolderTextWidgetID);
		// now forcing use of default name, "inventory"
		PMString tableName = PMString(kExtLinkUIDefaultTableStringKey, PMString::kTranslateDuringCall );
		
		// save the options to session workspace
		ExtLinkOptionsData newOption;
		newOption.SetImageFolderPath(sharedFolder);
		newOption.SetDataSourceName(authorityName);
		newOption.SetDBTableName(tableName);
		Utils<IExtLinkFacade> facade;
		ASSERT(facade);
		if(!facade) {
			break;
		}
		K2Vector<ExtLinkOptionsData> optionsVec(1);
		optionsVec.push_back(newOption);
		ErrorCode status;
		if(sharedFolder.empty() == kFalse)
			status = facade->ProcessSetOptionsCommand(
								optionsVec, 
								this->GetTriStateControlData(kExtLinkUIAutoRefreshCheckBoxWidgetID) == ITriStateControlData::kSelected													  
								);
		if (status != kSuccess)
		{
			ErrorUtils::PMSetGlobalErrorCode(status);
			break;
		}
	} while(false);
	
}

//  Code generated by DollyXs code generator
