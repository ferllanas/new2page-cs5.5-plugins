//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/persistentlist/N2PSQLDocResponder.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #5 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IClassIDData.h"
#include "ISignalMgr.h"
#include "IDocumentSignalData.h"
#include "IUIDData.h"
#include "IObserver.h"
#include "CAlert.h"
//#include "IMetaDataAccess.h"
#include "SplineID.h"
#include "LayoutUIID.h"
#include "ITracker.h"

#include "IDocument.h"
// Implementation includes:
#include "CreateObject.h"
#include "CResponder.h"
#include "N2PsqlID.h"
#include "UpdateStorysAndDBUtilis.h"
#include "N2PSQLUtilities.h"

//#include "SuppUISuppressedUI.h"
#include "ITrackerFactory.h"
/** 
	Handles signals related to document file actions.  The file
	action signals it receives are dictated by the A2PProDocServiceProvider
	class.

	N2PSQLDocResponder implements IResponder based on
	the partial implementation CResponder.

	We are interested in knowing when a documnet is opened or closed.  When these events happen,
	we have to attach/detach a document observer which will get notified when a document's hierarchy 
	is changed.  This allows us to purge the custom data associated with the page item.  In
	A2PProDocServiceProvider, we registered our interests in kAfterNewDocSignalResponderService,
	kDuringOpenDocSignalResponderService, and kBeforeCloseDocSignalResponderService, that will allow
	us being notified when these event occurs.  In that case we will attach/detach the document
	observer accordingly.
	
	@ingroup persistentlist
	
*/
class N2PSQLDocResponder : public CResponder
{
	public:
	
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSQLDocResponder(IPMUnknown* boss);

		/**
			Respond() handles the file action signals when they
			are dispatched by the signal manager.  This implementation
			simply creates alerts to display each signal.

			@param signalMgr Pointer back to the signal manager to get
			additional information about the signal.
		*/
		virtual void Respond(ISignalMgr* signalMgr);

};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PSQLDocResponder, kN2PSQLDocResponderImpl)

/* N2PSQLDocResponder Constructor
*/
N2PSQLDocResponder::N2PSQLDocResponder(IPMUnknown* boss) :
	CResponder(boss)
{
}

/* N2PSQLDocResponder::Respond
*/
void N2PSQLDocResponder::Respond(ISignalMgr* signalMgr)
{
	// Get the service ID from the signal manager
	ServiceID serviceTrigger = signalMgr->GetServiceID();

	// Get a UIDRef for the document.  It will be an invalid UIDRef
	// for BeforeNewDoc, BeforeOpenDoc, AfterSaveACopy, and AfterCloseDoc because the
	// document doesn't exist at that point.
	InterfacePtr<IDocumentSignalData> iDocData(signalMgr, UseDefaultIID());
	if (iDocData == nil)
	{
		//ASSERT_FAIL("Invalid IDocumentSignalData* - N2PSQLDocResponder::Respond");
		return;
	}
	UIDRef docRef = iDocData->GetDocument();

	// Take action based on the service ID
	switch (serviceTrigger.Get())
	{
		case kAfterNewDocSignalResponderService:
		case kDuringOpenDocSignalResponderService:
		{
			InterfacePtr<IObserver> iDocObserver(docRef, IID_IN2PSQL_DOCUMENT_OBSERVER);
			if (iDocObserver != nil)
			{
				iDocObserver->AutoAttach();
			}

			break;
		}
		case kBeforeCloseDocSignalResponderService:
		{
		
			//FluirFunciones::LimpiarPanel();
			InterfacePtr<IObserver> iDocObserver(docRef, IID_IN2PSQL_DOCUMENT_OBSERVER);
			if (iDocObserver != nil)
			{
				iDocObserver->AutoDetach();
			}


			break;
		}

		case kAfterOpenDocSignalResponderService:
		{
  			InterfacePtr<IDocumentSignalData> docData(signalMgr, UseDefaultIID());
   			if (docData)
   	 		{
        		UIDRef doc = docData->GetDocument();
        		InterfacePtr<IDocument> iDocument( doc, UseDefaultIID() );
        		if ( iDocument )
        		{ 
					InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																											  (
																											   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																											   IUpdateStorysAndDBUtils::kDefaultIID
																											   )));
					
					if(UpdateStorys==nil)
					{
						//T>CAlert::InformationAlert("UpdateStorys");
						break;
					}
					
					PreferencesConnection PrefConections;
					if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
					{
						//T>CAlert::InformationAlert("GetDefaultPreferencesConnection");
						break;
					}
					
					if(PrefConections.N2PUtilizarGuias==1)
					{
						//CAlert::InformationAlert("N2PSQLDocResponder::Respond kAfterOpenDocSignalResponderService");
						N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kTrue);
						N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kFalse);
						PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", iDocument);
						if(Id_Seccion.NumUTF16TextChars()>0 )
							N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
						
					}
					else
					{
						//CAlert::InformationAlert("BBB");
						N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kFalse );
						N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kTrue );
						
					}
					
					/*	PMString CadenaCompleta = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", iDocument);
					 if(CadenaCompleta.NumUTF16TextChars()>0)
					 {
					 
					 
					 UIDRef ur(GetUIDRef(iDocument));
					 
					 IDataBase* db = ::GetDataBase(iDocument);
					 if (db == nil)
					 {
					 //T>CAlert::InformationAlert("db");
					 break;
					 }
					 
					 
					 int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
					 ClassRegEnXMP *ListUpdate = new ClassRegEnXMP[contUpdate];
					 N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
					 
					 for(int32 numElem=0;numElem<contUpdate;numElem++)
					 {
					 
					 PMString IDFrameEleNota="";
					 IDFrameEleNota.AppendNumber(ListUpdate[numElem].IDFrame);
					 
					 
					 
					 
					 UID UIDStory=UID(ListUpdate[numElem].IDFrame);
					 UIDRef UIDRefStory=UIDRef(db,UIDStory);
					 
					 InterfacePtr<IInCopyStoryList> icStoryList(iDocument,UseDefaultIID());
					 if(icStoryList==nil)
					 {
					 //T>CAlert::InformationAlert("icStoryList");
					 break;
					 }
					 
					 
					 icStoryList->ProcessAddStory(UIDRefStory.GetUID());
					 
					 //ErrorCode error=UpdateStorys->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kTrue);
					 //if (error != kSuccess)
					 //{
					 //	//T>CAlert::InformationAlert("CambiaModoDeEscrituraDElementoNota");
					 //	break;
					 //}
					 }
					 }
					 */
					//if(UpdateStorys->SeRequiereActualizacionDeNotasDeDocument(iDocument))
					//{
					//Actualizacion de contenido de elementos de una nota desde la base de datos
					//	UpdateStorys->ActualizaNotasDesdeBD(iDocument);
					//}
					
					//XMPManager::ConvertXMP(iDocument);
					//InterfacePtr<IMetaDataAccess> metaDataAccess(iDocument, UseDefaultIID());
					//if (metaDataAccess == nil)
					//{
					//ASSERT_FAIL("metaDataAccess is nil!");
					//	break;
					//}
					
					//if (metaDataAccess->PropertyExists("http://ns.adobe.com/xap/1.0/","DummyNator_Paginas")==kTrue)
					//{
					//	SuppUISuppressedUI::gSuppressStyleChages = kTrue;
					//	SuppUISuppressedUI::gSuppressFontAndSizeMenus = kTrue;
					//	SuppUISuppressedUI::gSuppressOpenFileDialogControls =  kTrue;												
					//}
					//else
					//{
					//	SuppUISuppressedUI::gSuppressStyleChages = kFalse;
					//	SuppUISuppressedUI::gSuppressFontAndSizeMenus = kFalse;
					//	SuppUISuppressedUI::gSuppressOpenFileDialogControls =  kFalse;												
					//}
					
					//if (metaDataAccess->PropertyExists("http://ns.adobe.com/xap/1.0/","DummyNator_Frames")==kTrue)
					//{
					//	InterfacePtr<ITrackerFactory> factory1(gSession, UseDefaultIID()); 
					//	if (factory1) 
					//	{ 
					
					//		InterfacePtr<ITracker> registeredTracker(factory1->QueryTracker(kSplineItemBoss, kResizeToolBoss)); 		
	 				//		if (registeredTracker) 
 					//		{	 
 					//			factory1->RemoveTracker(kSplineItemBoss, kResizeToolBoss); 
 					//			factory1->InstallTracker(kSplineItemBoss, kResizeToolBoss, kMyResizeTrackerBoss); 
					//		}
					
					
					//	}						
					//}
					//else
					//{
					//	InterfacePtr<ITrackerFactory> factory1(gSession, UseDefaultIID()); 
					//	if (factory1) 
					//	{ 
					//		InterfacePtr<ITracker> registeredTracker(factory1->QueryTracker(kSplineItemBoss, kResizeToolBoss)); 		
	 				//		if (registeredTracker) 
 					//		{	 
 					//			factory1->RemoveTracker(kSplineItemBoss, kResizeToolBoss); 
 					//			factory1->InstallTracker(kSplineItemBoss, kResizeToolBoss, kBBoxResizeTrackerBoss); 	
					//	 	}
					//	}
					
					
					//}
					
					//if (metaDataAccess->PropertyExists("http://ns.adobe.com/xap/1.0/","DummyNator_File")==kTrue)
					//{
					//	FluirFunciones::ActualizarArbol();
					//}
         		}
   	 		}
   	 		
		
			break;
			
		}
			
		default:
			break;
	}

}

// End, N2PSQLDocResponder.cpp.



