//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/common/SDKODBCCache.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __SDKODBCCache_H_DEFINED__
#define __SDKODBCCache_H_DEFINED__

//#include "K2Vector.tpp"
//#include "K2Vector.h"
#include "WideString.h"
#include "ISDKODBCWrapper.h"
#include "URI.h"
#include <map>

struct ODBCCacheData
{
public:
	typedef object_type data_type;
	std::string	fFieldName;
	ISDKODBCWrapper::ItemType	fFieldType;
	WideString	fFieldData;
};



/**  
	Class representing a cache of database data.  When ExtLink is initially implemented, whenever there is need
	to get a link resource's data, a query to database was made through ODBC.  It resulted in pretty bad performance.
	The cache is an attempt to resolve the situation.  The cache is nothing more then a key value pair map.  When an
	URI is used to query the data for the first time, we store the URI as the key, and the data as its value in a map
	of the cache. We can update the cache value as needed (for example, during a link state update, if we found the
	link resource is out of date, we will update the cache), otherwise, when a link resource needs it's data, we always 
	return the data from cache, which is faster.
	
	Currently, this class is implemented as a singlton object with boost::mutex for handling multi-threading environment.
	But This may not be 100% thread-safe.  We might be switching to a simple global varible.
	
	@ingroup extendedlink
*/
class PLUGIN_DECL SDKODBCCache
{
public:

    static SDKODBCCache& Instance();
    ~SDKODBCCache();
	
	/**	Given an URI, return the cache for it
	 */
//	virtual K2Vector<ODBCCacheData2> GetData(const URI& uri, bool updateCache = 0);
	
	/**	Given an URI, return the cache for it
	 */
	virtual K2Vector<ODBCCacheData> GetCacheData(const URI& uri, bool updateCache = 0);
	
	/**	Call when you want to force a recalculation
	 */
	virtual void Rebuild(const URI& tableURI, PMString primaryKeyFieldName, K2Vector<WideString> &keyValues, K2Vector<PMString> &columnNames);

	/**	Check if a key value pair keyed by uri is already exisited in the stored map
	 */
	virtual bool IsCacheExisted(const URI& uri) const;
protected:

	SDKODBCCache() {}
	/**	Destroy the tree represented in this 
	 */
	void deleteTree() ;

private:
	/** Only called from the static Instance method using boost::call_once().
	*/
    static void InitSDKODBCCacheOnce();

    static std::auto_ptr<SDKODBCCache> ts_SDKCacheSingleInstance;
	std::map<URI, WideString > fODBCCache;
	std::map<URI, ODBCCacheData > fODBCCache2;
};

#endif // __SDKODBCCache_H_DEFINED__