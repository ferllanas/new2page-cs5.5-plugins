//========================================================================================
//  
//  $File: $
//  
//  Owner: Fernando Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PCheckInOutID_h__
#define __N2PCheckInOutID_h__

#include "SDKDef.h"

// Company:
#define kN2PCheckInOutCompanyKey	"INTERLASA"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2PCheckInOutCompanyValue	"INTERLASA"	// Company name displayed externally.

// Plug-in:
#define kN2PCheckInOutPluginName	"N2PCheckInOut"			// Name of this plug-in.
#define kN2PCheckInOutPrefixNumber	0x96D00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2PCheckInOutVersion		"v1.0.8.7"						// Version of this plug-in (for the About Box).
#define kN2PCheckInOutAuthor		"Interlasa.com S.A. de C.V."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kN2PCheckInOutPrefixNumber above to modify the prefix.)
#define kN2PCheckInOutPrefix		RezLong(kN2PCheckInOutPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2PCheckInOutStringPrefix	SDK_DEF_STRINGIZE(kN2PCheckInOutPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2PCheckInOutMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2PCheckInOutMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2PCheckInOutPluginID, kN2PCheckInOutPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kCheckInOutActionComponentBoss, kN2PCheckInOutPrefix + 0)
DECLARE_PMID(kClassIDSpace, kCheckInOutStringRegisterBoss,	kN2PCheckInOutPrefix + 1)
DECLARE_PMID(kClassIDSpace, kCheckInOutMenuRegisterBoss,	kN2PCheckInOutPrefix + 2)
DECLARE_PMID(kClassIDSpace, kCheckInOutActionRegisterBoss,	kN2PCheckInOutPrefix + 3)
DECLARE_PMID(kClassIDSpace, kCheckInDialogBoss,				kN2PCheckInOutPrefix + 4)
DECLARE_PMID(kClassIDSpace, kCheckInPanelBoss,				kN2PCheckInOutPrefix + 5)
DECLARE_PMID(kClassIDSpace, kCheckOutPanelBoss,				kN2PCheckInOutPrefix + 6)
DECLARE_PMID(kClassIDSpace, kCheckInOutListWidgetBoss,		kN2PCheckInOutPrefix + 7)
DECLARE_PMID(kClassIDSpace, kGuardaRevicionDialogBoss,		kN2PCheckInOutPrefix + 8)
DECLARE_PMID(kClassIDSpace, kCheckOutDialogBoss,			kN2PCheckInOutPrefix + 9)
DECLARE_PMID(kClassIDSpace, kCheckInOutSuiteBoss,			kN2PCheckInOutPrefix + 10)
DECLARE_PMID(kClassIDSpace, kN2PXMPUtilitiesBoss,			kN2PCheckInOutPrefix + 11)
DECLARE_PMID(kClassIDSpace, kN2PImpRollOverIconCalButtonObserverBoss,	kN2PCheckInOutPrefix + 12)
DECLARE_PMID(kClassIDSpace, kCheckOutDuplicarPaginaDialogoBoss,	kN2PCheckInOutPrefix + 13)
DECLARE_PMID(kClassIDSpace, kCheckOutN2PQuestionDialogoBoss,	kN2PCheckInOutPrefix + 14)
DECLARE_PMID(kClassIDSpace, kCheckInOutAsignaIssueDialogBoss,	kN2PCheckInOutPrefix + 15)

// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_ICheckInOutSUITE,		kN2PCheckInOutPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PXMPUtilities,		kN2PCheckInOutPrefix + 1)

// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kN2PkCInOutActionComponentImpl,	kN2PCheckInOutPrefix + 0)
DECLARE_PMID(kImplementationIDSpace, kCheckInDialogControllerImpl,		kN2PCheckInOutPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kGuardaRevicionDialogObserverImpl, kN2PCheckInOutPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kGuardaRevDialogControllerImpl,	kN2PCheckInOutPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kGuardaRevDialogObserverImpl,		kN2PCheckInOutPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kCheckInDialogObserverImpl,		kN2PCheckInOutPrefix + 5)


DECLARE_PMID(kImplementationIDSpace, kCheckInDialogCreatorImpl,kN2PCheckInOutPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kCheckInPanelCreatorImpl,kN2PCheckInOutPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kCheckOutPanelCreatorImpl,kN2PCheckInOutPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kCheckOutPanelObserverImpl,kN2PCheckInOutPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kCheckInOutListBoxObserverImpl,kN2PCheckInOutPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kCheckInOutSuiteImpl,kN2PCheckInOutPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kCheckOutDialogControllerImpl, kN2PCheckInOutPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kCheckOutDialogObserverImpl, kN2PCheckInOutPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kN2PXMPUtilitiesImpl,	kN2PCheckInOutPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kN2PImpRollOverCalButtonObserverImpl,	kN2PCheckInOutPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kCheckOutDuplicarPaginaDialogObserverImpl,	kN2PCheckInOutPrefix + 16)
DECLARE_PMID(kImplementationIDSpace, kCheckOutDuplicarPaginaDialogControlerImpl,	kN2PCheckInOutPrefix + 17)
DECLARE_PMID(kImplementationIDSpace, kCheckOutN2PQuestionDialogoObserverImpl,	kN2PCheckInOutPrefix + 18)
DECLARE_PMID(kImplementationIDSpace, kCheckOutN2PQuestionDialogoControlerImpl,	kN2PCheckInOutPrefix + 19)

DECLARE_PMID(kImplementationIDSpace, kN2PCheckInOutAsignaIssueControllerImpl,	kN2PCheckInOutPrefix + 20)
DECLARE_PMID(kImplementationIDSpace, kN2PCheckInOutAsignaIssueObserverImpl,	kN2PCheckInOutPrefix + 21)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kCheckInOutAboutActionID, kN2PCheckInOutPrefix + 0)
DECLARE_PMID(kActionIDSpace, kCheckInOutDialogActionID, kN2PCheckInOutPrefix + 1)
DECLARE_PMID(kActionIDSpace, kGuardaRevDialogActionID, kN2PCheckInOutPrefix + 2)
DECLARE_PMID(kActionIDSpace, kCheckOutDialogActionID, kN2PCheckInOutPrefix + 3)

// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kCheckInOutDialogWidgetID, kN2PCheckInOutPrefix + 0)
DECLARE_PMID(kWidgetIDSpace, kCheckInOutIconSuiteWidgetID, kN2PCheckInOutPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kCheckInPanelWidgetID, kN2PCheckInOutPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kCheckOutPanelWidgetID, kN2PCheckInOutPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kComboBoxStatusWidgetID, kN2PCheckInOutPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kComboBoxDirigidoAWidgetID, kN2PCheckInOutPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kComboBoxSeccionWidgetID, kN2PCheckInOutPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kComboBoxUsuarioWidgetID, kN2PCheckInOutPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kPaginaWidgetID, kN2PCheckInOutPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kFechaWidgetID, kN2PCheckInOutPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kHraCreacionWidgetID, kN2PCheckInOutPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kHoraWidgetID, kN2PCheckInOutPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kPaginasListBoxWidgetID, kN2PCheckInOutPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kPaginaParentWidgetID, kN2PCheckInOutPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kPaginaLabelWidgetID, kN2PCheckInOutPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kSeccionLabelWidgetID, kN2PCheckInOutPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kEstadoLabelWidgetID, kN2PCheckInOutPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kGuardaRevDialogWidgetID, kN2PCheckInOutPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kBuscarButtonWidgetID, kN2PCheckInOutPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kLogoInterlasaWidgetID, kN2PCheckInOutPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kDatePubliWidgetID, kN2PCheckInOutPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kComboBoxIssueWidgetID, kN2PCheckInOutPrefix + 21)
DECLARE_PMID(kWidgetIDSpace, kN2PImpCalRollOverIconButtonWidgetID, kN2PCheckInOutPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kToCheckInOnDBWidgetID, kN2PCheckInOutPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, kFolio_PaginaLabelWidgetID, kN2PCheckInOutPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, kLabelId_PublicacionLabelWidgetID, kN2PCheckInOutPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kLabelId_SeccionLabelWidgetID, kN2PCheckInOutPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kLabelId_EstatusNotaLabelWidgetID, kN2PCheckInOutPrefix + 27)
DECLARE_PMID(kWidgetIDSpace, kCheckInUltimoUsuarioQModificoWidgetID,			kN2PCheckInOutPrefix + 28)
DECLARE_PMID(kWidgetIDSpace, kCheckInDuplicarPaginaDialogWidgetID,			kN2PCheckInOutPrefix + 29)
DECLARE_PMID(kWidgetIDSpace, kCheckOutN2PQuestionDialogoWidgetID,			kN2PCheckInOutPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kCheckInOutQuestionStaticTextWidgetID,			kN2PCheckInOutPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kCheckInOutAceptQuestionWidgetID,			kN2PCheckInOutPrefix + 32)

DECLARE_PMID(kWidgetIDSpace, kCheckInOutAsignaIssueDialogWidgetID,			kN2PCheckInOutPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kN2PCheckInOutIssueDPaginasListBoxWidgetID,			kN2PCheckInOutPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kAddButtonWidgetID,			kN2PCheckInOutPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kN2PCkRemoveButtonWidgetID,			kN2PCheckInOutPrefix + 36)

// Service IDs
//IDs de Servicios
DECLARE_PMID(kServiceIDSpace, kSelDlgService, kN2PCheckInOutPrefix + 0)


// "About Plug-ins" sub-menu:
#define kCheckInOutAboutMenuKey			kN2PCheckInOutStringPrefix "kCheckInOutAboutMenuKey"
#define kCheckInOutAboutMenuPath			kN2PCheckInOutStringPrefix kN2PCheckInOutCompanyKey

// "Plug-ins" sub-menu:
#define kCheckInOutPluginsMenuKey 		kN2PCheckInOutStringPrefix "kCheckInOutPluginsMenuKey"
#define kCheckInOutPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2PCheckInOutCompanyKey

// "Plug-ins" sub-menu item keys:
#define kCheckInOutDialogMenuItemKey		kN2PCheckInOutStringPrefix "kCheckInOutDialogMenuItemKey"
#define kGuardaRevDialogMenuItemKey		kN2PCheckInOutStringPrefix "kGuardaRevDialogMenuItemKey"
#define kCheckOutPluginsMenuPath		kN2PCheckInOutStringPrefix "kCheckOutPluginsMenuPath"

// "Plug-ins" sub-menu item positions:
#define kCheckInOutDialogMenuItemPosition	5.0
#define kCheckOutDialogMenuItemPosition		4.0
#define kGuardaRevDialogMenuItemPosition	3.0

// Other StringKeys:
#define kCheckInOutDialogTitleKey		kN2PCheckInOutStringPrefix "kCheckInOutDialogTitleKey"
#define kN2PkCInOutAboutBoxStringKey		kN2PCheckInOutStringPrefix "kN2PkCInOutAboutBoxStringKey"
#define kPestanaCheckInStringKey		kN2PCheckInOutStringPrefix "kPestanaChechInStringKey"
#define kPestanaCheckOuStringKey		kN2PCheckInOutStringPrefix "kPestanaCheckOuStringKey"
#define kGuardaRevDialogTitleKey		kN2PCheckInOutStringPrefix "kGuardaRevDialogTitleKey"
#define kCheckOutDialogMenuItemKey		kN2PCheckInOutStringPrefix "kCheckOutDialogMenuItemKey"
#define kLabelStatusPageStringKey		kN2PCheckInOutStringPrefix "kLabelStatusPageStringKey"
#define kLabelCheckOutbyPageStringKey		kN2PCheckInOutStringPrefix "kLabelCheckOutbyPageStringKey"
#define kLabelRoutedtoPageStringKey		kN2PCheckInOutStringPrefix "kLabelRoutedtoPageStringKey"
#define kLabelSectionStringKey		kN2PCheckInOutStringPrefix "kLabelSectionStringKey"
#define kLabelPageIDStringKey		kN2PCheckInOutStringPrefix "kLabelPageIDStringKey"
#define kLabelFechaCreacionStringKey		kN2PCheckInOutStringPrefix "kLabelFechaCreacionStringKey"
#define kLabelHoraCreacionStringKey		kN2PCheckInOutStringPrefix "kLabelHoraCreacionStringKey"
#define kBotonLabelBuscarStringKey		kN2PCheckInOutStringPrefix "kBotonLabelBuscarStringKey"
#define kLabelStatusStringKey		kN2PCheckInOutStringPrefix "kLabelStatusStringKey"
#define kBotonLabelCheckOutStringKey		kN2PCheckInOutStringPrefix "kBotonLabelCheckOutStringKey"
#define kBotonLabelCheckInStringKey		kN2PCheckInOutStringPrefix "kBotonLabelCheckInStringKey"
#define kLabelStatus2StringKey		kN2PCheckInOutStringPrefix "kLabelStatus2StringKey"
#define kLabelSection2StringKey		kN2PCheckInOutStringPrefix "kLabelSection2StringKey"
#define kLabelPageID2StringKey		kN2PCheckInOutStringPrefix "kLabelPageID2StringKey"
#define kBotonLabelSaveStringKey		kN2PCheckInOutStringPrefix "kBotonLabelSaveStringKey"
#define kLabelDatePubliStringKey		kN2PCheckInOutStringPrefix "kLabelDatePubliStringKey"
#define kLabelIssueStringKey		kN2PCheckInOutStringPrefix "kLabelIssueStringKey"
#define kMsgAlertHanerSeleccionStringKey		kN2PCheckInOutStringPrefix "kMsgAlertHanerSeleccionStringKey"

#define kN2PCheckInOutNewItemOnDBStringKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutNewItemOnDBStringKey"
#define kN2PCheckInOutPDFExportadoStringKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutPDFExportadoStringKey"
#define kN2PCheckInOutNoExisteDocumentoStringKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutNoExisteDocumentoStringKey"
#define kN2PChekInCantCheckInStringKey		kN2PCheckInOutStringPrefix "kN2PChekInCantCheckInStringKey"
#define kN2PCheckInOutAllowString		kN2PCheckInOutStringPrefix "kN2PCheckInOutAllowString"
#define kN2PImplIdiomaStringKey		kN2PCheckInOutStringPrefix "kN2PImplIdiomaStringKey"
#define kN2PCheckInOutNoPrivilegiosPGrupoDUsuarioStringkey		kN2PCheckInOutStringPrefix "kN2PCheckInOutNoPrivilegiosPGrupoDUsuarioStringkey"
#define kN2PCheckInOutStaNotaNoPuedeRetirarlaStringKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutStaNotaNoPuedeRetirarlaStringKey"
#define kN2PCkInOutAlertDontServerFileConectionStringKey		kN2PCheckInOutStringPrefix "kN2PCkInOutAlertDontServerFileConectionStringKey"
#define kN2PCheckInOutUserSinPriviPBuscarPageStringKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutUserSinPriviPBuscarPageStringKey"
#define kN2PsqlSinPermisosToCheckinPageStringKey		kN2PCheckInOutStringPrefix "kN2PsqlSinPermisosToCheckinPageStringKey"
#define kN2PsqlSinPermisosToEditPageStringKey		kN2PCheckInOutStringPrefix "kN2PsqlSinPermisosToEditPageStringKey"
#define kN2PCheckInCckInPageStringKey		kN2PCheckInOutStringPrefix "kN2PCheckInCckInPageStringKey"
#define kN2PsqlYaExistePaginaConEsteNombreStringKey		kN2PCheckInOutStringPrefix "kN2PsqlYaExistePaginaConEsteNombreStringKey"
#define kN2PsqlCambioEnVersionesStringKey		kN2PCheckInOutStringPrefix "kN2PsqlCambioEnVersionesStringKey"
#define kN2PSQLUltimoUsuarioQModificoStringKey		kN2PCheckInOutStringPrefix "kN2PSQLUltimoUsuarioQModificoStringKey"
#define kCheckInOutDuplicaPaginaDialogStringKey		kN2PCheckInOutStringPrefix "kCheckInOutDuplicaPaginaDialogStringKey"
#define kCheckOutN2PQuestionDialogoStringKey		kN2PCheckInOutStringPrefix "kCheckOutN2PQuestionDialogoStringKey"
#define kCheckInOutQuestionStringKey		kN2PCheckInOutStringPrefix "kCheckInOutQuestionStringKey"
#define kCheckInOutDuplicarNotasStringKey		kN2PCheckInOutStringPrefix "kCheckInOutDuplicarNotasStringKey"
#define kCheckInOutSiButtonApplicationKey		kN2PCheckInOutStringPrefix "kCheckInOutSiButtonApplicationKey"
#define kCheckInOutNoApplicationKey		kN2PCheckInOutStringPrefix "kCheckInOutNoApplicationKey"
#define kCheckInOutCancelarApplicationKey		kN2PCheckInOutStringPrefix "kCheckInOutCancelarApplicationKey"
#define kN2PCheckInOutNoSeEncontraronPaginasPlugInMenuKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutNoSeEncontraronPaginasPlugInMenuKey"
#define kN2PCheckInOutNotasEnEdicionWEBAlertInfoStringKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutNotasEnEdicionWEBAlertInfoStringKey"
#define kN2PCheckInOutPaginaSinXMPCoreStringKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutPaginaSinXMPCoreStringKey"



#define kN2PCheckInOutNoSeFechaValidaKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutNoSeFechaValidaKey"

#define kN2PCheckInOutNoSeFechaNoExisteStrKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutNoSeFechaNoExisteStrKey"
#define kCheckOutDialogAsignaIssueStringKey		kN2PCheckInOutStringPrefix "kCheckOutDialogAsignaIssueStringKey"
#define kN2PCheckInOutAsignarIssuesToPageStrKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutAsignarIssuesToPageStrKey"
#define kN2PCheckInOutListoStrKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutListoStrKey"

#define kN2PCheckInOutSelectMoreThanOneStrKey		kN2PCheckInOutStringPrefix "kN2PCheckInOutSelectMoreThanOneStrKey"




#define kSelDlgPanelOrderingResourceID			500	
#define kGuardarRevDialogResourceID				510

#define kCheckInDialogResourceID				515
#define kCheckInTabDialogResourceID				520


#define kCheckInPanelCreatorResourceID			525
#define kCheckInPanelResourceID					530
#define kCheckOutPanelCreatorResourceID			535
#define kCheckOutPanelResourceID				545
#define kPaginasListElementRsrcID				550
#define kCheckOutDialogResourceID				555
#define kCheckInDuplicarPaginaDialogResourceID	560
#define kCheckOutN2PQuestionDialogoResourceID	570
#define kCheckInOutAsignaIssueDialogResourceID	580


#define kN2PCheckInOutrsrc						128
#define kN2PImpCalPNGIconRsrcID					150



// Initial data format version numbers
#define kN2PCheckInOutFirstMajorFormatNumber  RezLong(1)
#define kN2PCheckInOutFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kN2PCheckInOutCurrentMajorFormatNumber kN2PCheckInOutFirstMajorFormatNumber
#define kN2PCheckInOutCurrentMinorFormatNumber kN2PCheckInOutFirstMinorFormatNumber

#endif // __N2PCheckInOutID_h__

//  Code generated by DollyXs code generator
