//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com S.A. de C.V.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __WPN2PID_h__
#define __WPN2PID_h__

#include "SDKDef.h"

// Company:
#define kWPN2PCompanyKey	"INTERLASA"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kWPN2PCompanyValue	"INTERLASA"	// Company name displayed externally.

// Plug-in:
#define kWPN2PPluginName	"WPN2P"			// Name of this plug-in.
#define kWPN2PPrefixNumber	0x1b8300 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kWPN2PVersion		"1.0"						// Version of this plug-in (for the About Box).
#define kWPN2PAuthor		"Interlasa.com S.A. de C.V."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kWPN2PPrefixNumber above to modify the prefix.)
#define kWPN2PPrefix		RezLong(kWPN2PPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kWPN2PStringPrefix	SDK_DEF_STRINGIZE(kWPN2PPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kWPN2PMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kWPN2PMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kWPN2PPluginID, kWPN2PPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kWPN2PwsdlcltUtilsBoss, kWPN2PPrefix + 3)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 4)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 5)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 6)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 7)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 8)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 9)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 10)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 11)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 12)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 13)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 14)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 16)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 17)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 18)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 19)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 22)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 23)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 24)
//DECLARE_PMID(kClassIDSpace, kWPN2PBoss, kWPN2PPrefix + 25)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PWSDLCLTUTILSINTERFACE, kWPN2PPrefix + 0)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 1)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 2)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 3)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 4)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 5)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 6)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 7)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 8)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 9)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 10)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 11)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 12)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 13)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 14)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 15)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 16)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 17)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 18)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 19)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 20)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 21)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 22)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 23)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 24)
//DECLARE_PMID(kInterfaceIDSpace, IID_IWPN2PINTERFACE, kWPN2PPrefix + 25)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kWPN2PwsdlcltUtilsImpl, kWPN2PPrefix + 0)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 1)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 2)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 3)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 4)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 5)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 6)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 8)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 9)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 10)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 11)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 12)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 13)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 14)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 15)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 16)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 18)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 19)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 20)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 21)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 22)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 24)
//DECLARE_PMID(kImplementationIDSpace, kWPN2PImpl, kWPN2PPrefix + 25)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kWPN2PAboutActionID, kWPN2PPrefix + 0)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 5)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 6)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 7)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 8)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 9)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 10)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 11)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 12)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 13)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 14)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 15)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 16)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 17)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 18)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 19)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 20)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 21)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 22)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 23)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 24)
//DECLARE_PMID(kActionIDSpace, kWPN2PActionID, kWPN2PPrefix + 25)


// WidgetIDs:
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 2)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 3)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 4)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 5)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 6)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 7)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 8)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 9)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 10)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 11)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 12)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 13)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 14)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 15)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 16)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 17)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 18)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 19)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 20)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 21)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 22)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 23)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 24)
//DECLARE_PMID(kWidgetIDSpace, kWPN2PWidgetID, kWPN2PPrefix + 25)


// "About Plug-ins" sub-menu:
#define kWPN2PAboutMenuKey			kWPN2PStringPrefix "kWPN2PAboutMenuKey"
#define kWPN2PAboutMenuPath		kSDKDefStandardAboutMenuPath kWPN2PCompanyKey

// "Plug-ins" sub-menu:
#define kWPN2PPluginsMenuKey 		kWPN2PStringPrefix "kWPN2PPluginsMenuKey"
#define kWPN2PPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kWPN2PCompanyKey kSDKDefDelimitMenuPath kWPN2PPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kWPN2PAboutBoxStringKey	kWPN2PStringPrefix "kWPN2PAboutBoxStringKey"
#define kWPN2PTargetMenuPath kWPN2PPluginsMenuPath

// Menu item positions:


// Initial data format version numbers
#define kWPN2PFirstMajorFormatNumber  RezLong(1)
#define kWPN2PFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kWPN2PCurrentMajorFormatNumber kWPN2PFirstMajorFormatNumber
#define kWPN2PCurrentMinorFormatNumber kWPN2PFirstMinorFormatNumber

#endif // __WPN2PID_h__

//  Code generated by DollyXs code generator
