//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/customprefs/N2RegisterUsersScriptProvider.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2003/12/18 11:20:39 $
//  
//  $Revision: #1 $
//  
//  $Change: 237988 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"


// General includes:
#include "CmdUtils.h"
#include "PrefsScriptProvider.h"
#include "Utils.h"

// Interface includes
#include "ICommand.h"
#include "IRegisterUsersUtils.h"
#include "N2PRegisterUsers.h"
#include "IDataBase.h"
#include "IDocument.h"
#ifdef DEBUG
#include "IObjectModel.h"
#endif
#include "IScript.h"
#include "IScriptRequestData.h"
#include "IWorkspace.h"

// Project includes:
#include "N2PInOutScriptingDefs.h"
#include "N2PInOutID.h"


/** N2RegisterUsersScriptProvider
	demonstrates extending the Application and Documentation script objects. 
  
	@ingroup customprefs
	@author Ken Sadahiro
*/
class N2RegisterUsersScriptProvider : public PrefsScriptProvider
{
public:
	/**
		Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.
	*/
	N2RegisterUsersScriptProvider(IPMUnknown* boss);

	/**
		Destructor. Does nothing.
	*/
	~N2RegisterUsersScriptProvider() {}

	/** This method is called if a provider can handle a property.
	 * 	@param propID identifies the ID of the property to handle.
	 * 	@param data identifies an interface pointer used to extract data.
	 * 	@param script identifies an interface pointer on the script object representing the parent of the application object.
	 */
	virtual ErrorCode  AccessProperty(ScriptID propID, IScriptRequestData* data, IScript* parent);

private:
	/** AccessProperty delegates to this method to handle the CustomPrefs property. 
	 * 	@param propID identifies the ID of the property to handle.
	 * 	@param data identifies an interface pointer used to extract data.
	 * 	@param script identifies an interface pointer on the script object representing the parent of the application object.
	 */
	virtual ErrorCode  AccessCustomPrefsProperty(ScriptID propID, IScriptRequestData* data, IScript* parent);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2RegisterUsersScriptProvider, kN2RegisterUsersScriptProviderImpl)


/* Constructor
*/
N2RegisterUsersScriptProvider::N2RegisterUsersScriptProvider(IPMUnknown* boss)
	:PrefsScriptProvider(boss)
{
	// Define preference 
	DefinePreference(c_N2PUserReg, c_N2PUserReg);
}

/* AccessProperty
*/
ErrorCode N2RegisterUsersScriptProvider::AccessProperty(ScriptID propID, IScriptRequestData* data, IScript* parent)
{
	ErrorCode status = kFailure;

    switch (propID.Get())
    {
	case p_N2PVal:
		// for custom prefs property, delegate
		status = AccessCustomPrefsProperty(propID, data, parent);
		break;

    default:
		status = PrefsScriptProvider::AccessProperty(propID, data, parent);
    }

    return status;
}

/* AccessCustomPrefsProperty
*/
ErrorCode N2RegisterUsersScriptProvider::AccessCustomPrefsProperty(ScriptID propID, IScriptRequestData* data, IScript* parent)
{
	ErrorCode status = kFailure;
    bool16 boolData = kFalse;
    ScriptData scriptData;
	IPMUnknown* thePrefsBoss = nil;

	do {
		// check parameters
		if (parent == nil) 
		{
			ASSERT_FAIL("Parent is nil! We won't be able to get the IN2PRegisterUsers interface.");
			break;
		}
		if (data == nil) 
		{
			ASSERT_FAIL("IScriptRequestData is nil!");
			break;
		}

		// get the database and UID for the corresponding parent boss
		IDataBase* db = parent->GetDataBase(data->GetRequestContext()); // CS2 CS3
		UID rootUID = db->GetRootUID();

		// query for a qualifier interface for IRegisterUsersUtils::QueryCustomPrefs()
		// first try IWorkspace
		thePrefsBoss = db->QueryInstance(rootUID, IWorkspace::kDefaultIID);
		if (thePrefsBoss == nil) 
		{
			// the parent is not on kWorkspaceBoss, so it must be kDocBoss
			// try IDocument
			thePrefsBoss = db->QueryInstance(rootUID, IDocument::kDefaultIID);
		}
		// something should have worked by now...
        ASSERT(thePrefsBoss);

#ifdef DEBUG
		// see which boss thePrefsBoss belongs to
		InterfacePtr<IObjectModel> om(GetExecutionContextSession(), UseDefaultIID());
		ASSERT(om);
		
		ClassID parentClassID = ::GetClass(parent);
		const char* parentClassname = om->GetIDName(parentClassID);
		
		// NOTE: prefsClassname should be either kWorkspaceBoss or kDocBoss
		// according to how we set up the ScriptElementInfo
		ClassID prefsClassID = ::GetClass(thePrefsBoss);
		const char* prefsClassname = om->GetIDName(prefsClassID);
		ASSERT(strcmp(prefsClassname, "kWorkspaceBoss") == 0 || strcmp(prefsClassname, "kDocBoss") == 0);
#endif
		// query the custom pref utils interface
		InterfacePtr<IN2PRegisterUsers> customPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(thePrefsBoss));
		if (customPrefs == nil) 
		{
			ASSERT_FAIL("Could not obtain the custom prefs interface");
			break;
		}
		
		// release thePrefsBoss - we don't need it anymore
		if (thePrefsBoss != nil) 
		{
			thePrefsBoss->Release();
			thePrefsBoss = nil;
		}
		// check the access mode
		
		if (data->IsPropertyGet())
		{
			// get the preference from the custom pref interface
			boolData = customPrefs->GetValue();

			// set the return data
            scriptData.SetBoolean(boolData);
			data->AppendReturnData( parent, propID, scriptData ) ; //data->SetReturnData(&scriptData); CS3 a CS4
			
			status = kSuccess;
			
		}
		else if (data->IsPropertyPut())
		{
			// get the data passed into the script event data
			status = data->ExtractRequestData(propID, scriptData);
			if (status != kSuccess) 
			{
				ASSERT_FAIL("Could not get the script data from IScriptRequestData!");
				break;
			}
			status = scriptData.GetBoolean(&boolData);
			if (status != kSuccess) 
			{
				ASSERT_FAIL("Could not get the boolean data from the script data!");
				break;
			}

			PMString Datos="News2Page Preferences\n";
		//	Datos.Append("Aplicacion:"+ Aplicacion +"\n");
		//	Datos.Append("FechaIn:" + fecha +"\n");
		//	Datos.Append("HoraIn:" + hora + "\n");
		//	Datos.Append("IDUserLogedString:" + hora + "\n");
			
			//Obtiene los datos en caso de que anteriormente se hayan realizado algun check in o checkOut
			Datos.Append("Estado_To_PageIn:"+	customPrefs->GetEstatusNota() +"\n");
			Datos.Append("SeccionNota:"+	customPrefs->GetSeccionNota() +"\n");
			Datos.Append("PaginaNota:" +	customPrefs->GetPaginaNota() + "\n");
			Datos.Append("IssueNota:"+		customPrefs->GetIssueNota() +"\n");
			Datos.Append("DateNota:" +		customPrefs->GetDateNota() +"\n");
			Datos.Append("DirigidoA:"+ 		customPrefs->GetDirigidoANota() +"\n");
				
			Datos.Append("StatusNotaPageOut:"+ customPrefs->GetEstatusNotaPageOut() + "\n");
			Datos.Append("SeccionNotaPageOut:"+ customPrefs->GetSeccionNotaPageOut() + "\n");
			Datos.Append("PaginaNotaPageOut:"+	customPrefs-> GetPaginaNotaPageOut() + "\n");
			Datos.Append("IssueNotaPageOut:"+	customPrefs->GetIssueNotaPageOut() + "\n");
			Datos.Append("DateNotaPageOut:"+	customPrefs->GetDateNotaPageOut() + "\n");
			Datos.Append("DirigidoAPageOut:"+	customPrefs->GetDirigidoANotaPageOut() + "\n");		
					
			// generate a command to modify the preferences
			InterfacePtr<ICommand> modPrefsCmd(Utils<IRegisterUsersUtils>()->CreateModPrefsCmd(customPrefs, boolData,0,Datos));
			if (modPrefsCmd == nil) 
			{
				ASSERT("Could not create ModCstPrfCmd!");
				break;
			}
			// process the command
			status = CmdUtils::ProcessCommand(modPrefsCmd);
		}
	} while (false);
	return status;
}



// End, N2RegisterUsersScriptProvider.cpp

