//========================================================================================
//  
//  $File: //depot/indesign_5.0/highprofile/source/sdksamples/gotolasttextedit/GTTxtEdtNewDeleteStoryObserver.cpp $
//  
//  Owner: Adrian O'Lenskie
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:27:55 $
//  
//  $Revision: #1 $
//  
//  $Change: 505962 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes
#include "IDocument.h"
#include "ISubject.h"
#include "ILayoutUIUtils.h"
#include "IObjectModel.h"
//#include "GTTxtEdtInvalHandler.h"
#include "DBUtils.h"

// General includes
#include "CObserver.h"
#include "GenericID.h"
#include "TextID.h"
#include "PreferenceUtils.h"
#include "CAlert.h"
#include "ILayoutUIUtils.h"
// Project includes:
#include "N2PsqlID.h"
#include "N2PSQLUtilities.h"
#include "UpdateStorysAndDBUtilis.h"

/** An observer watching for stories being created and deleted. This observer gets attached on document
creation/open by the GTTxtEdtResponder. While the responder also receives new/delete story events, this
observer allows us to manage the invalhandler appropriately (we require an inval handler 
per document). If this was managed within the responder, we would require to manintain the list of invalidation
handlers per document.

The invalidation hander is created on attach (which relates to a document create/open event), and destroyed on
detach (document close). If we receive a story create/delete signal, we indicate that the event if of interest
by calling the inval handler.

@see GTTxtEdtInvalHandler
@ingroup gotolasttextedit
*/
class GTTxtEdtNewDeleteStoryObserver : public CObserver
{
public:
	/**
	Constructor
	*/
	GTTxtEdtNewDeleteStoryObserver(IPMUnknown *boss);
	/**
	Destructor
	*/
	virtual ~GTTxtEdtNewDeleteStoryObserver();
	/**
	Attach this observer to the document.
	*/
	virtual void AutoAttach();
	/**
	Detach this observer from the document
	*/
	virtual void AutoDetach();
	/**
	Signal the observer that a change has occurred (used to determine whether we want to record this event through
	our invalidation handler).
	*/
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
private:
	/**
	Our invalidation handler. 
	*/
	//GTTxtEdtInvalHandler* fGTTxtEdtInvalHandler;

};

/*
*/
CREATE_PMINTERFACE(GTTxtEdtNewDeleteStoryObserver, kGTTxtEdtNewDeleteStoryObserverImpl)

/*
*/
GTTxtEdtNewDeleteStoryObserver::GTTxtEdtNewDeleteStoryObserver(IPMUnknown *boss) :
CObserver(boss)//,
//fGTTxtEdtInvalHandler(nil)
{
}

/*
*/
GTTxtEdtNewDeleteStoryObserver::~GTTxtEdtNewDeleteStoryObserver()
{

}


/*
*/
void GTTxtEdtNewDeleteStoryObserver::AutoAttach()
{
	do {
		InterfacePtr<IDocument> iDocument(this, UseDefaultIID());
		if (iDocument == nil){	
			ASSERT(iDocument);
			break;
		}
		// Attach the inval handler to the database.
		/*if (fGTTxtEdtInvalHandler == nil) {
			fGTTxtEdtInvalHandler = new GTTxtEdtInvalHandler();
			DBUtils::AttachInvalHandler(::GetDataBase(iDocument), fGTTxtEdtInvalHandler, kGTTxtEdtInvalHandlerID);
		}*/
		// Attach this observer to the story list.
		InterfacePtr<ISubject> iSubject(iDocument, UseDefaultIID());
		if (iSubject && !iSubject->IsAttached(ISubject::kRegularAttachment,this, IID_ISTORYLIST, IID_IGTTXTEDTSTORYCREATEDELETEOBSERVER)) {
			iSubject->AttachObserver(ISubject::kRegularAttachment,this, IID_ISTORYLIST, IID_IGTTXTEDTSTORYCREATEDELETEOBSERVER);
		}
	} while (kFalse);
}

/*
*/
void GTTxtEdtNewDeleteStoryObserver::AutoDetach()
{
	do {
		InterfacePtr<IDocument> iDocument(this, UseDefaultIID());
		if (iDocument == nil){	
			ASSERT(iDocument);
			break;
		}
		// Detach the inval handler from the database.
		/*if (fGTTxtEdtInvalHandler != nil){
			DBUtils::DetachInvalHandler(::GetDataBase(iDocument), fGTTxtEdtInvalHandler);
			fGTTxtEdtInvalHandler = nil;
		}*/
		//Detach this observer from the story list.
		InterfacePtr<ISubject> iSubject(iDocument, UseDefaultIID());
		if (iSubject && iSubject->IsAttached(ISubject::kRegularAttachment,this, IID_ISTORYLIST, IID_IGTTXTEDTSTORYCREATEDELETEOBSERVER)) {
			iSubject->DetachObserver(ISubject::kRegularAttachment,this, IID_ISTORYLIST, IID_IGTTXTEDTSTORYCREATEDELETEOBSERVER);
		}
	} while (kFalse);

}

/*
*/
void GTTxtEdtNewDeleteStoryObserver::Update(const ClassID& theChange, ISubject* theSubject,
											const PMIID &protocol, void* changedBy)
{
	do {      
		if ( theChange != kDeleteStoryCmdBoss)
		{
			break;
		}
/*#ifdef DEBUG
		InterfacePtr<IObjectModel> iObjectModel(gSession, UseDefaultIID());
		if (iObjectModel == nil){
			break;
		}
		TRACEFLOW(kGTTxtEdtPluginName,"GTTxtEdtNewDeleteStoryObserver::Update, pName %s cName %s subject %s\n", iObjectModel->GetIDName(theChange),iObjectModel->GetIDName(protocol),iObjectModel->GetIDName(::GetClass(theSubject)));
#endif*/

		/*ASSERT(fGTTxtEdtInvalHandler != nil);
		if (fGTTxtEdtInvalHandler == nil) {
			break;
		}*/

		ICommand* theCmd = static_cast<ICommand *>(changedBy);
		if (theCmd && theCmd->GetCommandState() == ICommand::kDone ) 
		{
			const UIDList* itemList = theCmd->GetItemList();
			
			//CAlert::InformationAlert("A chingado");
			if (itemList && itemList->Length()>0)
			{
				
				//CAlert::InformationAlert("1");
				IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
				if (document == nil)
					break;
				//	CAlert::InformationAlert("2");
				IDataBase* db = ::GetDataBase(document);
				if (db == nil)
					break;
				//CAlert::InformationAlert("3");
				// Story created or story deleted. Call the inval handler
				// so that the plug-in is called on undo or redo of this event.
				//OK mira cuando se borra una story por lo tanto aqui debo de ver se
				/////Elementos de Notas
				PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
				int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
				K2Vector<PMString> Params;
		
				ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
				N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
				UIDList myUIDList = *itemList;
				//CAlert::InformationAlert("4");
				int32 items = itemList->Length();
				for (int i=0;i<items;i++)
				{
					//CAlert::InformationAlert("5");
					UIDRef pageItemRef = myUIDList.GetRef(i);//selectUIDList->GetRef(0);		
					UID a = pageItemRef.GetUID();//OBTENGO EL UID del rectangulo
					int b=a.Get();			//obtengo el valor en exadecimal del rectangulo		
					PMString idFrameToDeleted;
					idFrameToDeleted.AppendNumber(b);
					PMString IDElemPadreFoto;
					PMString IDElementoFoto;
					
					UID UIDFrameFoto;
					
					//PMString XSA="";
							
					//XSA.AppendNumber(b);
					//CAlert::InformationAlert(XSA);
					
					for(int NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
					{
						//PMString XSA="";
						//XSA.AppendNumber( RegistrosEnXMP[NumReg].IDFrame);
						//XSA.Append("==");
						//XSA.AppendNumber(b);
						//CAlert::InformationAlert(XSA);
						//if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
						{	
							IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
							IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
							UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					
							UIDRef pageItemUIDRef(db,UIDFrameFoto);
							if(pageItemRef==pageItemUIDRef)
							{
								theCmd->SetName("Delete N2PElement");
								
								InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
								(
									kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
									IUpdateStorysAndDBUtils::kDefaultIID
								)));
			
								if(UpdateStorys==nil)
								{
									break;
								}
								
								
								PreferencesConnection PrefConections;
								
								UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
								
								UpdateStorys->CambiaEstadoYLapizDeElementoNOTA( RegistrosEnXMP[NumReg].IDFrame, kFalse,"",kTrue);
								
								UpdateStorys->UpdateCoordenadasDeUIDElemEnBaseD(IDElementoFoto,RegistrosEnXMP[NumReg].IDFrame,kFalse,kTrue, document, PrefConections);
		
								N2PSQLUtilities::BuscaYElimina_ElementoOnPaginaXMP("N2P_ListaUpdate",  RegistrosEnXMP[NumReg].IDFrame);
								
								CAlert::InformationAlert(kN2PSQLAcabasDBorrarUnElementoDLaNotaPendienteStringKey);
								//CAlert::InformationAlert("Esta tratando de borra un elemento de la nota");
								//DesligarElementoDEstrucPorNum
							}
						}
					}
					
				}
			
				//bool16 storyCreated = (theChange == kNewStoryCmdBoss) ? kTrue : kFalse;
				//CAlert::InformationAlert("Se borro este pedo");
				//fGTTxtEdtInvalHandler->AddStoryInvalInfo(itemList->GetRef(0), storyCreated);
			}
		}

	} while (kFalse);
}

