//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkSetShowInUIPrefCmd.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISubject.h"
#include "IWorkspace.h"
#include "IBoolData.h"
#include "IDocument.h"
#include "IActiveContext.h"

// General includes:
#include "CmdUtils.h"
#include "ErrorUtils.h"
#include "Command.h"

// Project includes:
#include "ExtLinkID.h"


/** 
	@ingroup extendedlink
*/

class ExtLinkSetShowInUIPrefCmd : public Command
{
public:
	/** Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.*/
	ExtLinkSetShowInUIPrefCmd(IPMUnknown* boss);

protected:
	/** Performs notification. */
	virtual void DoNotify();

	/** Do the model chaneg to IID_ISHOWINLINKSUI in workspace */
	virtual void Do();

	/** Sets command name. */
	virtual PMString* CreateName();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/											  
CREATE_PMINTERFACE(ExtLinkSetShowInUIPrefCmd, kExtLinkSetShowInUIPrefCmdImpl)


/* Constructor
*/
ExtLinkSetShowInUIPrefCmd::ExtLinkSetShowInUIPrefCmd(IPMUnknown* boss) :
	Command(boss)
{
}


/* Do
*/
void ExtLinkSetShowInUIPrefCmd::Do()
{
	ErrorCode status = kFailure;
	do						
	{
		InterfacePtr<IBoolData> iBoolData(this, UseDefaultIID());
		ASSERT(iBoolData);		
		
		// Get the ItemList
		const UIDList* itemList = this->GetItemList();
		if (itemList == nil || itemList->IsEmpty())
		{
			break;
		}
		UIDRef uidRef = itemList->GetRef(0);

		InterfacePtr<IBoolData> iShowInLinksUIPref(uidRef, IID_ISHOWINLINKSUI);
		ASSERT(iShowInLinksUIPref);
		iShowInLinksUIPref->Set(iBoolData->Get());
		
		status = kSuccess;

	} while(kFalse);		

	// Handle any errors
	if (status != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(status);
	}
}

/* DoNotify
*/
void ExtLinkSetShowInUIPrefCmd::DoNotify()
{
}


/* CreateName
*/
PMString* ExtLinkSetShowInUIPrefCmd::CreateName()
{
	return new PMString(kExtLinkSetShowInUIPrefCmdKey);
}


