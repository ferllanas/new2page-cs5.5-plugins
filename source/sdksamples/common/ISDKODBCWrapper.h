//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/common/ISDKODBCWrapper.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#pragma once
#ifndef __ODBCWrapper__H__
#define __ODBCWrapper__H__


// ----- Includes -----
//#include "ODBCID.h"
#include "URI.h"

class PMString;

/**
	@ingroup odbcwrapper

Wrapper ODBC queres to database to an interface that facilatate URI representation
data from a database.

The ideal usage of the interface would be as follows:
		URI uri("odbc://MySQL?table=inventory&sku='AB-219'#price");
		InterfacePtr<ISDKODBCWrapper> odbc((ISDKODBCWrapper*)::CreateObject(kODBCWrapperBoss, IID_ISDKODBCWrapper));
		if (odbc && ISDKODBCWrapper::ODBCNormal == odbc->Initialize(uri))
		{
			WideString ws(odbc->GetDataAsWideString());
			... Do processing the result....
		}

A URI can be used to specifiy a specific field, a record, all records of a field or even all records of the whole table.
For example:
A field: "odbc://MySQL?table=inventory&sku='AB-219'#price"
A record: "odbc://MySQL?table=inventory&sku='AB-219'"
All records of a field: "odbc://MySQL?table=inventory#price"
Whole table: "odbc://MySQL?table=inventory"
Custom results: "odbc://MySQL?table=inventory&sku='AB-219'||sku='AB-220'#sku,name,price,image,version,quantity"

For more information of URI, please refer to URI.h
For more information of how URI is used to represent a database destination, see ODBCWrapperDesign.txt.

*/
class ISDKODBCWrapper
{

public:

	/**
     Identifiers ODBC error state
     */
	enum ODBCState
	{
		ODBCNormal = 0,  // ODBC statment executed with a success
		ODBCError, //ODBC statment has with generic error, statment not executed
		ODBCExecutedWithError //ODBC statment has been executed, but with error
	};


	/** Initialize the odbc wrapper object. It will parse the URI, update the entry with the value if provided, perform the database query, the result
		is ready to be fetched. Initialize() cause information contained in the ODBC wrapper changed.
		
		Initialize() are supposed to be called only once

		@param uri IN The URI for a database resource data.
		@param newValue IN if you supply a valid newValue, the database field pointed to by the URI will be updated with this value.
		@return ODBC wrapper error state.
	*/
	virtual ODBCState Query(const URI uri, const WideString& newValue = WideString("")) = 0 ;

	/**	ResetURI reset URI after an object has been initialized.
		ResetURI cause information contained in the ODBC wrapper changed.

		@param uri IN The URI for a database resource data.
		@return ODBC wrapper error state.
	 */
	virtual ODBCState ResetURI(const URI uri) = 0;

	/**	Return the URI of odbcwrapper object
		@return URI 
	 */
	virtual URI GetURI() const = 0;

	/**	Return the ODBC data source name, which specifies a data base connection
		@return std::string 
	 */
	virtual std::string GetDataSourceName() const = 0;

	/** The data type of the database query result.
	*/
	enum ItemType
	{
		kIntegerType =0, //integer
		kDoubleType, //corresponding to double value in data base
		kStringType, //corresponding to CHAR, VARCHAR type in the database
		kTimeStampType //corresponding to TIMESTAMP, DATETIME in the database
	};


	/**	Return the number of columns in the query result.
		@return int32 
	 */
	virtual int32 GetNumberOfColumns() const = 0;

	/**	Return information of columns of a column.
		@param columnIndex IN Index of the column, start at 0.
		@param fieldName OUT the name of the column.
		@return column value type 
	 */
	virtual ItemType GetMthColumnInfo(const int32 columnIndex, std::string &fieldName) const  = 0;

	/**	Return the number of records in the query result. (Not number of record a table has)
		@return int32 
	 */
	virtual int32 GetNumberOfRecords() const = 0;

	/**	Return the value of a record as a vector of WideString.
		@param recordIndex IN Index of the record in result set, start at 0.
		@param error OUT ODBC status after fetch.
		@return K2Vector<WideString> 
	 */
	virtual K2Vector<WideString> GetNthRecord(const int32 recordIndex, ODBCState *error = 0) const = 0;

	/**	Return the value of a specific column in a result set.
		@param recordIndex IN Index of the record in result set, start at 0.
		@param columnIndex IN Index of the column, start at 0. 
			Note order of columns are the one specified in URI, not the order of database field
		@param error OUT ODBC status after fetch.
		@return WideString constructed from the value of the column 
	 */
	virtual WideString GetNthRecordMthColumn(const int32 recordIndex, const int32 columnIndex, ODBCState  *error = 0) const = 0;

	/**	Return the value of a specific column in a result set according to recordIndex and column name
		@param recordIndex IN Index of the record in result set, start at 0.
		@param columnName IN name of the column, from GetMthColumnInfo() or already known to caller. 
		@param error OUT ODBC status after fetch.
		@return  WideString constructed from the value of the column 
	 */
	virtual WideString GetColumnOfNthRecordWithName(const int32 recordIndex, const std::string columnName, ODBCState  *error = 0) const = 0;


};

#endif // __ODBCWrapper__H__
