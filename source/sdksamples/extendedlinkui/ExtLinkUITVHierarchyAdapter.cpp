//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUITVHierarchyAdapter.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IExtLinkUIDataModel.h"
#include "ITreeViewHierarchyAdapter.h"

// General includes:
#include "CPMUnknown.h"

// Project includes:
#include "ExtLinkUIID.h"
#include "ExtLinkUINodeID.h"


/** Implementation of ITreeViewHierarchyAdapter, adapts our tree model to the needs of the 
	tree-view widget. Implements an adapter for the tree hierarchy; this is where the hierarchy 
	is accessed by the application core - you'll find that this is the only implementation 
	you provide with methods like GetNumChildren or GetParent.

	For further reading on ITreeViewHiearchyAdapter interface, see the Widgets chapter
	of the User Interface technote.
	
	@ingroup extendedlinkui

*/

class ExtLinkUITVHierarchyAdapter : public CPMUnknown<ITreeViewHierarchyAdapter>
{
public:
	/** Constructor
	*/	
	ExtLinkUITVHierarchyAdapter(IPMUnknown* boss);

	/** Destructor
	*/	
	virtual ~ExtLinkUITVHierarchyAdapter();
	

	/** 
		Acquire reference to root node of our tree. 
		@return reference to root node
	*/	
	virtual NodeID_rv GetRootNode() const;

	/** Acquire a reference to the parent of the given node.
		GetParentNode should always return kInvalidNodeID for the root.
		@param node [IN] specifies node of interest
		@return reference to parent of given node
	*/	
	virtual NodeID_rv GetParentNode(const NodeID& node) const;

	/** 
		Determine the number of child nodes that the given node has.
		@param node [IN] specifies node of interest
		@return count of children for given node
	*/	
	virtual int32 GetNumChildren(const NodeID& node) const;

	/**
		Acquire a reference to the nth child. Note: the indexing is from 0 to (numChildren-1).
		@param parentNode [IN] identifies the parent node for which we're seeking the nth child
		@param nth [IN] get the child at this index in the list of children of the specified node
		@return reference to the nth child
	*/	
	virtual NodeID_rv GetNthChild(const NodeID& parentNode, const int32& nth) const;

	/** Determines the index of given child in the parent's list of children.
		@param parent [IN] reference to the parent node
		@param child [IN] reference to the child node, whose index we're seeking
		@return index of child in parent's kids, or (-1) if the child doesn't belong to this parent
	*/	
	virtual int32 GetChildIndex(const NodeID& parent, const NodeID& child) const;

	/** Acquire a reference to a general-purpose node.
		This should return an instance of a node that can be used anywhere in the hierarchy.
		@return reference to general purpose node
	*/	
	virtual NodeID_rv GetGenericNodeID() const;


};	

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(ExtLinkUITVHierarchyAdapter, kExtLinkUITVHierarchyAdapterImpl)


/* Constructor
*/
ExtLinkUITVHierarchyAdapter::ExtLinkUITVHierarchyAdapter(IPMUnknown* boss) : 
	CPMUnknown<ITreeViewHierarchyAdapter>(boss)
{
}


/* Destructor
*/
ExtLinkUITVHierarchyAdapter::~ExtLinkUITVHierarchyAdapter()
{
}


/* GetRootNode
*/
NodeID_rv ExtLinkUITVHierarchyAdapter::GetRootNode() const
{
	// When we call ITreeViewMgr::ChangeRoot, then this is the first method that gets called
	InterfacePtr<IExtLinkUIDataModel> model(this, UseDefaultIID());
	ASSERT(model);
	return ExtLinkUINodeID::Create( model->GetRootURI(), kRootDocumentExtLinkNodeUID, nil);		
}


/* GetParentNode
*/
NodeID_rv ExtLinkUITVHierarchyAdapter::GetParentNode(const NodeID& node) const
{
	TreeNodePtr<const ExtLinkUINodeID> extLinkUINodeID(node);
	if (extLinkUINodeID) {
		if ( kRootDocumentExtLinkNodeUID != extLinkUINodeID->GetLinkUID() ) {
			return extLinkUINodeID->GetParentNode();
		}
	}
	return kInvalidNodeID;
}


/*
*/
int32 ExtLinkUITVHierarchyAdapter::GetNumChildren(const NodeID& node) const
{
	TreeNodePtr<const ExtLinkUINodeID> extLinkUINodeID(node);
	if (extLinkUINodeID) {
		InterfacePtr<IExtLinkUIDataModel> model(this, UseDefaultIID());
		if (model) {
			return model->GetNumChildren(extLinkUINodeID->GetNodeURI(), extLinkUINodeID->GetLinkUID());
		}
	}
	return 0;
}



/* GetNthChild
*/
NodeID_rv ExtLinkUITVHierarchyAdapter::GetNthChild(const NodeID& parentNode, const int32& nth) const
{
	NodeID childNode = kInvalidNodeID;
	TreeNodePtr<const ExtLinkUINodeID> parentExtLinkNode(parentNode);
	if (parentExtLinkNode) 
	{
		InterfacePtr<IExtLinkUIDataModel> model(this, UseDefaultIID());
		if (model) 
		{
			URI childURI;
			UID childUID;
			if (model->GetNthChildData(parentExtLinkNode->GetNodeURI(), parentExtLinkNode->GetLinkUID(), nth, childURI, childUID)) 
			{
				return ExtLinkUINodeID::Create(childURI, childUID, parentNode);
			}
		}
	}
	return childNode;
}


/* GetChildIndex
*/
int32 ExtLinkUITVHierarchyAdapter::GetChildIndex( const NodeID& parent, const NodeID& child) const
{
	TreeNodePtr<const ExtLinkUINodeID> parentNodeID(parent);
	ASSERT(parentNodeID);
	if (parentNodeID) 
	{
		TreeNodePtr<const ExtLinkUINodeID> childNodeID(child);
		ASSERT(childNodeID);
		if (childNodeID) 
		{
			InterfacePtr<IExtLinkUIDataModel> model(this, UseDefaultIID());
			if (model) 
			{
				return model->GetChildIndexFor( parentNodeID->GetNodeURI(), parentNodeID->GetLinkUID(), childNodeID->GetNodeURI(), childNodeID->GetLinkUID());
			}
		}
	}
	return -1;
}


/* GetGenericNodeID
*/
NodeID_rv ExtLinkUITVHierarchyAdapter::GetGenericNodeID() const
{
	return ExtLinkUINodeID::Create( URI(), kInvalidUID, nil );	
}



//	end, File: ExtLinkUITVHierarchyAdapter.cpp
