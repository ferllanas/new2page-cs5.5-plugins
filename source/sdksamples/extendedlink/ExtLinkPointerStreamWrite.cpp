//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkPointerStreamWrite.cpp $
//  
//  Owner: jargast
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "CStreamWrite.h"
#include "ExtLinkPointerXferBytes.h"
#include "IPointerStreamData.h"

#include "ShuksanID.h"
//#include "bossrecycler.h"
#include "ExtLinkID.h"

//RECYCLE_BOSS(kExtLinkPointerStreamWriteBoss, 8, 0, 4, "ExtLinkPointerStreamWrite", nil)															\


class ExtLinkPointerStreamWrite : public CStreamWrite
{
public:
	ExtLinkPointerStreamWrite(IPMUnknown *boss);
	
	bool16 Open();
	void Close();
	
private:
	ExtLinkPointerXferBytes fExtLinkPointerXferBytes;
};

CREATE_PMINTERFACE(ExtLinkPointerStreamWrite, kExtLinkPointerStreamWriteImpl)


ExtLinkPointerStreamWrite::ExtLinkPointerStreamWrite(IPMUnknown *boss) : 
	CStreamWrite(boss)
{
	SetXferBytes(&fExtLinkPointerXferBytes);
}



void ExtLinkPointerStreamWrite::Close()
{
	fExtLinkPointerXferBytes.Close();
}



bool16 ExtLinkPointerStreamWrite::Open()
{
	InterfacePtr<IPointerStreamData> data(this, IID_IPOINTERSTREAMDATA);
	return fExtLinkPointerXferBytes.Open (data->GetBuffer(), static_cast<uint32>(data->GetLength()));
}


