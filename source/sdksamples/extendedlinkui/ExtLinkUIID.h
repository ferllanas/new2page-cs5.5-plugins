//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIID.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __ExtLinkUIID_h__
#define __ExtLinkUIID_h__

#include "SDKDef.h"

// Company:
#define kExtLinkUICompanyKey	kSDKDefPlugInCompanyKey		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kExtLinkUICompanyValue	kSDKDefPlugInCompanyValue	// Company name displayed externally.

// Plug-in:
#define kExtLinkUIPluginName	"ExtendedLinkUI"			// Name of this plug-in.
#define kExtLinkUIPrefixNumber	0x113D00					// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kExtLinkUIVersion		kSDKDefPluginVersionString					// Version of this plug-in (for the About Box).
#define kExtLinkUIAuthor		"Adobe Developer Technologies"				// Author of this plug-in (for the About Box).
#define kExtLinkUIPrefix		RezLong(kExtLinkUIPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kExtLinkUIStringPrefix	SDK_DEF_STRINGIZE(kExtLinkUIPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kExtLinkUIMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS	// URL displayed in Missing Plug-in dialog
#define kExtLinkUIMissingPluginAlertValue	kSDKDefMissingPluginAlertValue		// Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kExtLinkUIPluginID, kExtLinkUIPrefix + 0)


// ClassIDs:
DECLARE_PMID(kClassIDSpace, kExtLinkUIActionComponentBoss, kExtLinkUIPrefix + 0)
DECLARE_PMID(kClassIDSpace, kExtLinkUIPlaceDBDialogBoss, kExtLinkUIPrefix + 2)
DECLARE_PMID(kClassIDSpace, kExtLinkUIPanelWidgetBoss, kExtLinkUIPrefix + 3)
DECLARE_PMID(kClassIDSpace, kExtLinkUITreeViewWidgetBoss, kExtLinkUIPrefix + 4)
DECLARE_PMID(kClassIDSpace, kExtLinkUINodeWidgetBoss, kExtLinkUIPrefix + 5)
DECLARE_PMID(kClassIDSpace, kExtLinkUICViewPanelWidgetBoss, kExtLinkUIPrefix + 6)
DECLARE_PMID(kClassIDSpace, kExtLinkUITreeObserver, kExtLinkUIPrefix + 8)
DECLARE_PMID(kClassIDSpace, kExtLinkUIManageDBsDialogBoss, kExtLinkUIPrefix + 9)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKUIDATAMODEL, kExtLinkUIPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKUITREENODELIST, kExtLinkUIPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_IEXTLINKUISHADOWEVENTHANDLER, kExtLinkUIPrefix + 3)
DECLARE_PMID(kInterfaceIDSpace, IID_EXTLINKPANELDRAWINGDISABLED, kExtLinkUIPrefix + 4)
DECLARE_PMID(kInterfaceIDSpace, IID_EXTLINKPANELSHOULDINVALDATA, kExtLinkUIPrefix + 5)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kExtLinkUIActionComponentImpl, kExtLinkUIPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kExtLinkUIPlaceDBDialogControllerImpl, kExtLinkUIPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kExtLinkUITVWidgetMgrImpl, kExtLinkUIPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kExtLinkUITVHierarchyAdapterImpl, kExtLinkUIPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kExtLinkUIDataModelImpl, kExtLinkUIPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kExtLinkUINodeEHImpl, kExtLinkUIPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kExtLinkUITreeObserverImpl, kExtLinkUIPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kExtLinkUIManageDBsDialogControllerImpl, kExtLinkUIPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kExtLinkUIManageDBsDialogObserverImpl, kExtLinkUIPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kExtLinkUITreeNodeListImpl, kExtLinkUIPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kExtLinkUIPanelViewImpl, kExtLinkUIPrefix + 13)




// ActionIDs:
DECLARE_PMID(kActionIDSpace, kExtLinkUIAboutActionID, kExtLinkUIPrefix + 0)
DECLARE_PMID(kActionIDSpace, kExtLinkUIPlaceDBDialogActionID, kExtLinkUIPrefix + 1)
DECLARE_PMID(kActionIDSpace, kExtLinkUIPanelWidgetActionID, kExtLinkUIPrefix + 2)
DECLARE_PMID(kActionIDSpace, kExtLinkUIRefreshActionID, kExtLinkUIPrefix + 3)
DECLARE_PMID(kActionIDSpace, kExtLinkUIUpdateLinkActionID, kExtLinkUIPrefix + 4)
DECLARE_PMID(kActionIDSpace, kExtLinkUIGotoLinkActionID, kExtLinkUIPrefix + 5)
DECLARE_PMID(kActionIDSpace, kExtLinkUISeparator1ActionID, kExtLinkUIPrefix + 6)
DECLARE_PMID(kActionIDSpace, kExtLinkUIPopupAboutThisActionID, kExtLinkUIPrefix + 7)
DECLARE_PMID(kActionIDSpace, kExtLinkUIManageDBsActionID, kExtLinkUIPrefix + 8)
DECLARE_PMID(kActionIDSpace, kExtLinkUIShowInUIActionID, kExtLinkUIPrefix + 9)

// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPlaceDBDialogWidgetID, kExtLinkUIPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPlaceDBStaticTextWidgetID, kExtLinkUIPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPlaceDBDropDownListWidgetID, kExtLinkUIPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPlaceDBSortByStaticWidgetID, kExtLinkUIPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPlaceDBSortByDropDownWidgetID, kExtLinkUIPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPlaceDBRecordURIStaticWidgetID, kExtLinkUIPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPlaceDBRecordURIDropDownWidgetID, kExtLinkUIPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPlaceDBURIWidgetID, kExtLinkUIPrefix + 8)

DECLARE_PMID(kWidgetIDSpace, kExtLinkUIPanelWidgetID,				kExtLinkUIPrefix + 10) 
DECLARE_PMID(kWidgetIDSpace, kExtLinkUITreeViewWidgetID,			kExtLinkUIPrefix + 11) 
DECLARE_PMID(kWidgetIDSpace, kExtLinkUINodeWidgetId,				kExtLinkUIPrefix + 12) 
DECLARE_PMID(kWidgetIDSpace, kExtLinkUINodeNameWidgetID,			kExtLinkUIPrefix + 13) 
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIElementWidgetId,				kExtLinkUIPrefix + 14) 
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIIconWidgetId,				kExtLinkUIPrefix + 15) 
DECLARE_PMID(kWidgetIDSpace, kExtLinkUICustomPanelViewWidgetID,		kExtLinkUIPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUITextMessageWidgetID,			kExtLinkUIPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUINodeStatusWidgetID,			kExtLinkUIPrefix + 18)

DECLARE_PMID(kWidgetIDSpace, kExtLinkUIManageDBsDialogWidgetID,	kExtLinkUIPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIManageDBsDropDownListWidgetID,	kExtLinkUIPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIConnectionsDropDownListWidgetID,	kExtLinkUIPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIInfoGroupLabelWidgetID,	kExtLinkUIPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIAuthorityTextWidgetID,	kExtLinkUIPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUISharedFolderTextWidgetID,	kExtLinkUIPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIFindFolderButtonWidgetID,	kExtLinkUIPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUINewButtonWidgetID,	kExtLinkUIPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUIFieldNamesTextWidgetID,	kExtLinkUIPrefix + 38)
DECLARE_PMID(kWidgetIDSpace, kExtLinkUITableNameTextWidgetID,	kExtLinkUIPrefix + 39)

DECLARE_PMID(kWidgetIDSpace, kExtLinkUISplitterPanelWidgetID,	kExtLinkUIPrefix + 40)

DECLARE_PMID(kWidgetIDSpace, kExtLinkUIAutoRefreshCheckBoxWidgetID,	kExtLinkUIPrefix + 42)


//-------------------------------------------------------------------------------------------------------------------------------------
// Menu Defines
//-------------------------------------------------------------------------------------------------------------------------------------
//
// "About Plug-ins" sub-menu:
#define kExtLinkUIAboutMenuPath				kSDKDefStandardAboutMenuPath kExtLinkUICompanyKey
#define kExtLinkUIAboutMenuKey				kExtLinkUIStringPrefix "kExtLinkUIAboutMenuKey"

// "Extended Link" sub-menu:
#define kExtLinkUIPluginsMenuKey				kExtLinkUIStringPrefix "kExtLinkUIPluginsMenuKey"
#define kExtLinkUIPluginsMenuPath				kSDKDefPlugInsStandardMenuPath kExtLinkUICompanyKey kSDKDefDelimitMenuPath kExtLinkUIPluginsMenuKey
#define kExtLinkUIManageDBsMenuKey				kExtLinkUIStringPrefix "kExtLinkUIManageDBsMenuKey"
#define kExtLinkUIManageDBsMenuItemPosition		1.0

// "Place..." sub-menu:			// TODO  this should eventually be replaced as Place sub-menu items
#define kExtLinkUIPlaceMenuPath				"Main:&File"
#define kExtLinkUIPlaceDBMenuItemPosition	kPlaceMenuPosition + 1
#define kExtLinkUIPlaceDBMenuItemKey		kExtLinkUIStringPrefix "kExtLinkUIPlaceDBMenuKey"
#define kExtLinkUIShowInUIMenuItemKey		kExtLinkUIStringPrefix "kExtLinkUIShowInUIMenuItemKey"

// Panel popup menu:
#define kExtLinkUIInternalPopupMenuNameKey			kExtLinkUIStringPrefix	"kExtLinkUIInternalPopupMenuNameKey"
#define kExtLinkUITargetMenuPath					kExtLinkUIInternalPopupMenuNameKey
#define kExtLinkUIRefreshMenuItemKey				kExtLinkUIStringPrefix	"kExtLinkUIRefreshMenuItemKey"
#define kExtLinkUIUpdateLinkMenuItemKey				kExtLinkUIStringPrefix	"kExtLinkUIUpdateLinkMenuItemKey"
#define kExtLinkUIGotoLinkMenuItemKey				kExtLinkUIStringPrefix	"kExtLinkUIGotoLinkMenuItemKey"
#define kExtLinkUIRefreshMenuItemPosition			1.0
#define kExtLinkUIUpdateLinkMenuItemPosition		2.0
#define kExtLinkUIGotoLinkMenuItemPosition			3.0
#define kExtLinkUISeparator1MenuItemPosition		10.0
#define kExtLinkUIAboutThisMenuItemPosition			11.0
#define kExtLinkUIShowInUIMenuItemPosition			12.0

// Panel right mouse context menu
#define kExtLinkUIRtMenuPathNameKey					kExtLinkUIStringPrefix "kExtLinkUIRtMenuPathNameKey"
#define kExtLinkUIRtMenuPath						kExtLinkUIRtMenuPathNameKey



//-------------------------------------------------------------------------------------------------------------------------------------
// Widget defines:
//-------------------------------------------------------------------------------------------------------------------------------------
#define kExtLinkUINodeWidgetRsrcID				1200
#define kExtLinkUIManageDBsDialogResourceID		1300
const int32 kRootDocumentExtLinkNodeUID = -1; 

#define kExtLinkUIImportLinkObjectUnmodifiedAssetOutOfDateIconRsrcID		10250
#define kExtLinkUIImportLinkObjectModifiedAssetUpToDateIconRsrcID			10251
#define kExtLinkUIImportLinkObjectModifiedAssetOutOfDateIconRsrcID			10252
#define kExtLinkUIImportLinkAssetMissingIconRsrcID							10253                   
#define kExtLinkUIUpdateDBWarningIconRsrcID									10254                   

//-------------------------------------------------------------------------------------------------------------------------------------
// String Keys:
//-------------------------------------------------------------------------------------------------------------------------------------
//
// About box
#define kExtLinkUIAboutBoxStringKey		kExtLinkUIStringPrefix "kExtLinkUIAboutBoxStringKey"
//
// Place Database dialog
#define kExtLinkUIPlaceDBDlgTitleKey		kExtLinkUIStringPrefix "kExtLinkUIPlaceDBDlgTitleKey"
#define kExtLinkUIDBLabelStringKey			kExtLinkUIStringPrefix "kExtLinkUIDBLabelStringKey"
#define kExtLinkUISortByLabelStringKey		kExtLinkUIStringPrefix "kExtLinkUISortByLabelStringKey"
#define kExtLinkUIRecordURILabelStringKey	kExtLinkUIStringPrefix "kExtLinkUIRecordURILabelStringKey"
//
// Alert Strings
#define kAlertInvalidURIKey		kExtLinkUIStringPrefix "kAlertInvalidURIKey"
//
// Panel Strings
#define kExtLinkUIPanelTitleKey						kExtLinkUIStringPrefix	"kExtLinkUIPanelTitleKey"
#define kExtLinkUIStaticTextKey						kExtLinkUIStringPrefix	"kExtLinkUIStaticTextKey"
#define kExtLinkUIResourceModifiedStringKey			kExtLinkUIStringPrefix	"kExtLinkUIResourceModifiedStringKey"
#define kExtLinkUILinkModifiedStringKey				kExtLinkUIStringPrefix	"kExtLinkUILinkModifiedStringKey"
#define kExtLinkUILinkAndResourceModifiedStringKey	kExtLinkUIStringPrefix	"kExtLinkUILinkAndResourceModifiedStringKey"
#define kExtLinkUIAssetMissingStringKey				kExtLinkUIStringPrefix	"kExtLinkUIAssetMissingStringKey"
#define kExtLinkUIUpToDateStringKey					kExtLinkUIStringPrefix	"kExtLinkUIUpToDateStringKey"
//
// Database Connections dialog
#define kExtLinkUIAuthorityLabelItemKey				kExtLinkUIStringPrefix	"kExtLinkUIAuthorityLabelItemKey"
#define kExtLinkUISharedFolderLabelItemKey			kExtLinkUIStringPrefix	"kExtLinkUISharedFolderLabelItemKey"
#define kExtLinkUIFindFolderButtonKey				kExtLinkUIStringPrefix	"kExtLinkUIFindFolderButtonKey"
#define kExtLinkUIManageDBsTitleKey					kExtLinkUIStringPrefix  "kExtLinkUIManageDBsTitleKey"
#define kExtLinkUITableNameLabelItemKey				kExtLinkUIStringPrefix	"kExtLinkUITableNameLabelItemKey"
#define kExtLinkUIDefaultAuthorityStringKey			kExtLinkUIStringPrefix	"kExtLinkUIDefaultAuthorityStringKey"
#define kExtLinkUIDefaultTableStringKey				kExtLinkUIStringPrefix	"kExtLinkUIDefaultTableStringKey"
#define kExtLinkUIDefaultSharedFolderStringKey		kExtLinkUIStringPrefix	"kExtLinkUIDefaultSharedFolderStringKey"

#define kExtLinkUIAutoRefreshStringKey				kExtLinkUIStringPrefix	"kExtLinkUIAutoRefreshStringKey"	

//-------------------------------------------------------------------------------------------------------------------------------------
// Version info:
//-------------------------------------------------------------------------------------------------------------------------------------
//
// Initial data format version numbers
#define kExtLinkUIFirstMajorFormatNumber  RezLong(1)
#define kExtLinkUIFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kExtLinkUICurrentMajorFormatNumber kExtLinkUIFirstMajorFormatNumber
#define kExtLinkUICurrentMinorFormatNumber kExtLinkUIFirstMinorFormatNumber



#endif // __ExtLinkUIID_h__

//  Code generated by DollyXs code generator
