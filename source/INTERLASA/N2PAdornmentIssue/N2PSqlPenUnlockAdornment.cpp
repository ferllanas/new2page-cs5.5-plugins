//========================================================================================
//  
//  $File: //depot/indesign_5.0/highprofile/source/sdksamples/framelabel/N2PSqlPenUnlockAdornment.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:27:55 $
//  
//  $Revision: #1 $
//  
//  $Change: 505962 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IAdornmentShape.h"
#include "IGraphicsPort.h"
#include "IHierarchy.h"
#include "IDocument.h"
#include "ISwatchList.h"
#include "IPMFont.h"
#include "IFontInstance.h"
#include "IGeometry.h"

// General includes:
#include "AutoGSave.h"
#include "ILayoutUIUtils.h"
#include "CPMUnknown.h"
#include "CAlert.h"

// Project includes:
#include "IN2PSqlPenUnLockData.h"
#include "N2PAdIssueID.h"

/** Provides the adornment implementation to draw a text label
	on drawable page items; implements IAdornmentShape. 
	@ingroup framelabel
	
*/

class N2PSqlPenUnlockAdornment : public CPMUnknown<IAdornmentShape>
{

	public:

		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSqlPenUnlockAdornment(IPMUnknown* boss);

		/** 
			Destructor.
		*/
		~N2PSqlPenUnlockAdornment();


		/**
			Tells the draw manager when in the drawing sequence the adornment
			should be asked to draw.
			@return Enum describing when this adornment should be drawn as part of the draw sequence.  See IAdornmentShape.h.
		*/
		virtual AdornmentDrawOrder GetDrawOrderBits();


		/**
			Draws the adornment in the inner coordinates of the page item to which this adornment is attached.
			Note that for the draw to work correctly, it must be within the bounds reported by GetPaintedAdornmentBounds() below.
			It's confusing, because we draw in the page item's inner coordinates, but we report bounds in the view coordinates.
			@param	iShape		Ptr to page item requesting this adornment to draw.  Handy for getting information about the owning page item, such as its bounding box.
			@param	drawOrder	Tells the adornment when in the (AdornmentDrawOrder) sequence it's being asked to draw.  Used when an adornment is registered for more than one point in the draw order.
			@param	gd			Graphics data structure.  Used to acquire the drawing port, view, draw manager, etc.
			@param	flags		IShape flags describing mode of draw: dynamic, printing, patient user, etc.  Used for deciding when an adornment should not draw, or draw differently to a particular port. For example, draw to the screen but not to print.		
		*/
		virtual void DrawAdornment
			(
				IShape* 			iShape,
				AdornmentDrawOrder	drawOrder,
				GraphicsData*		gd,
				int32				flags
			);


		/** 
			Reports the dimensions of the adornment in the coordinates of the view in which this adornment will draw.
			Note the bounds reported by this method must accurately reflect the bounds used by Draw().
			It's confusing, because we draw in the page item's inner coordinates, but we report bounds in the view coordinates.
			@param	iShape		Ptr to page item requesting this adornment to draw.  Handy for getting information about the owning page item, such as its bounding box.
			@param	drawOrder	Tells the adornment when in the (AdornmentDrawOrder) sequence it's being asked to draw.  Used when an adornment is registered for more than one point in the draw order.
			@param	itemBounds	The painted bounds of the owing page item in the page item's inner coordinates.
			@param	innertoview	The transformation from the inner bounds coordinates of the page item to which this adornment is attached, to the coordinate system of the view in which the adornment will appear.
		*/
		virtual PMRect GetPaintedAdornmentBounds
			(
				IShape*				iShape,
				AdornmentDrawOrder	drawOrder,
				const PMRect&		itemBounds,
				const PMMatrix&		innertoview
			);

		/** 
			Here for completeness.  See IAdornmentShape for an explanation.
		*/
		virtual void Inval
			(
				IShape*				iShape,
				AdornmentDrawOrder	drawOrder,
				GraphicsData*		gd, 
				ClassID 			reasonForInval, 
				int32 				flags
			);

		/**
			Adornments affect when they are called by the priority value they return
			via their GetPriority() method. Higher priorities (smaller numbers) are
			called before lower priorities (larger numbers).
			@return Always returns the highest priority.
		*/
		virtual PMReal GetPriority() 
				{return 0;}


		/** 
			Adornments can modify their inking bounds if they exceed the content bounds.
			For example, if content adds a drop shadow under some conditions, then the inking
			bounds would be different than the painted bbox calculated in this class.  (Which
			is based on the adornment text and fonts, etc.)
			This class doesn't support any inking changes that would make the inking bounds
			different from the painted bbox, so we just have a do-nothing implementation.
			@param iShape IN Points to interface on page item boss to which this adornment is applied.
			@param inOutBounds IN Points to rectangle describing the current ink bounds. Modify this PMRect to reflect accurate ink bounds.
		*/
		virtual void AddToContentInkBounds(IShape* iShape, PMRect* inOutBounds) {}


		/**
			Adornments can declare they want to draw in TextOffscreen mode.  This will suspend drawing of
			offscreen drawing, so the default is to not draw during text offscreen mode.
			See the Draw() documentation for an explanation of parameters. 
		*/
		virtual bool16 WillDraw
		(
			IShape* 			iShape,		// owning page item
			AdornmentDrawOrder	drawOrder,	// for items that registered for more than one order
			GraphicsData*		gd,
			int32			flags
		)
		{ return kFalse; }


		/**	
		  	See IAdornmentShape::WillPrint
		 */
		virtual bool16 WillPrint(void)
		{ return kFalse; }
	
	/** See IAdornmentShape::HitTest
	 */
	virtual bool16 HitTest 
	( 
	 IShape*		          iShape,         // The owning page item 
	 AdornmentDrawOrder    adornmentDrawOrder,    // In case this adornment is used more than once 
	 IControlView *		layoutView,
	 const PMRect&         r 
	 ){ return kFalse; }
	
};


CREATE_PMINTERFACE(N2PSqlPenUnlockAdornment, kN2PSqlPenUnlockAdornmentImpl)


/*	Constructor.
*/
N2PSqlPenUnlockAdornment::N2PSqlPenUnlockAdornment(IPMUnknown* boss) 
	: CPMUnknown<IAdornmentShape>(boss)
{
	TRACE("Creating the adornment\n");
}

/* Destructor
*/
N2PSqlPenUnlockAdornment::~N2PSqlPenUnlockAdornment()
{
	TRACE("Releasing the Adronemnt\n");
}


/* Draw
*/
IAdornmentShape::AdornmentDrawOrder N2PSqlPenUnlockAdornment::GetDrawOrderBits()
{
	IAdornmentShape::AdornmentDrawOrder flags = (IAdornmentShape::AdornmentDrawOrder)(kAfterShape);
	return flags;
}


/* Draw
	Performs the following steps:
	1) Get the graphics port from the graphics data
	2) Set the graphics port for drawing the label
	3) Compute the starting coordinates for drawing the label
	4) Draw the label
	5) Restore the graphics port
*/
void N2PSqlPenUnlockAdornment::DrawAdornment
	(
		IShape* 			shape,
		AdornmentDrawOrder	drawOrder,
		GraphicsData*		gd,
		int32				flags 
	)
{

	// Only draw when the frame edge flag is set
	TRACE("Drawing the frame adornment!\n");
	if ( !(flags & IShape::kDrawFrameEdge) )
	{
		return;
	}
	
	// We choose not to draw for printing.  (Although we could.)
	if ( flags & IShape::kPrinting )
	{
		return;
	}

	TRACE("Got a call to N2PSqlPenUnlockAdornment::Draw\n");		
	do {
		
		if (gd == nil)
		{
			ASSERT_FAIL("Nil GraphicsData*");
			break;
		}
		/*PMMatrix currentMat = gd->GetTransform();
			
		// Get access to the data describing the label
		InterfacePtr<IN2PSqlPenUnLockData> labelData(shape, UseDefaultIID());
		if (labelData == nil)
		{
			TRACE("Label data == nil for this item");
			// We are often called in the general list to process, so return without assert.
			break; 
		}
		PMString labelString = labelData->GetString();
		if (labelString.empty())
		{
			TRACE("Label length is zero");
			break;
		}
		
		// The labels are created using the default font.
		InterfacePtr<IFontMgr> fontMgr(gSession, UseDefaultIID());
		if (fontMgr == nil)
		{
			ASSERT_FAIL("LabelDrawHandler::HandleEvent: fontMgr invalid");
			break;
		}

		// The default font, and the items geometry & hierarchy should be
		// available, but if they aren't a return is sufficient (an
		// assert here might come up a lot on a heavily populated page
		// and if this code is in error, the labels won't draw anyway.)
		InterfacePtr<IPMFont> theFont(fontMgr->QueryFont(fontMgr->GetDefaultFontName()));
		InterfacePtr<IGeometry> itemGeometry(labelData, UseDefaultIID());
		InterfacePtr<IHierarchy> itemHier(labelData, UseDefaultIID());
		if (theFont == nil || itemGeometry == nil || itemHier == nil)
		{
			// ASSERT_FAIL("LabelDrawHandler::HandleEvent: font, geometry, or hierachy unavailable");
			break;
		}

		InterfacePtr<IPMPersist> persist(itemHier, UseDefaultIID());
		if (persist == nil)
		{
			ASSERT_FAIL("Nil IPMPersist*");
			break;
		}
		IDataBase* theDB = persist->GetDataBase();
		InterfacePtr<IDocument> theDoc(theDB, theDB->GetRootUID(), UseDefaultIID());
		if (theDoc == nil)
		{
			ASSERT_FAIL("Nil IDocument*");
			break;
		}

		// The graphics port is the port that the page item is currently drawing to.
		// That could be the screen, a printer, a PDF, or some other port:
		IGraphicsPort* gPort = gd->GetGraphicsPort();
		if (gPort == nil)
		{
			ASSERT_FAIL("Nil IGraphicsPort*");
			break;
		}

		// Drawing methods are much like the PostScript commands. 
		// A good reference for the IGraphicsPort methods is the  PostScript
		// Language Reference Manual.

		// Save the current port settings.  This is the equivalent of pushing them on a stack
		AutoGSave autoGSave(gPort);

		// Set the drawing color for the port to a special value for the label
		gPort->setrgbcolor(203/256.0, 43/256.0, 138/256.0);

		// Set the font in the port
		gPort->selectfont(theFont, static_cast<PMReal>(labelData->GetSize()));

		// Compute the starting position for drawing the label in the graphics port
		// Both the width and the height have some room added so the label isn't right
		// on the bounding box:
		PMRect fontBB = theFont->GetFontBBox();
		PMReal fontHeight = fontBB.Height();
		PMReal fontWidth = fontBB.Width();
		PMReal x, y;
		TRACE("Font height is set to %f\n",::ToDouble(fontHeight));
		TRACE("Font width is set to %f\n",::ToDouble(fontWidth));
		PMReal fHeightOfText = fontHeight*static_cast<PMReal>(labelData->GetSize());
		PMRect bBox = itemGeometry->GetPathBoundingBox();

		// Place the label beneath and aligned with the left side of the frame
		x = bBox.Left(); 
		y = bBox.Bottom() + fHeightOfText; 
		TRACE("the label is going in at (%f,%f) and is %d characters long\n",::ToDouble(x),::ToDouble(y),labelString.CharCount());

		// Draw the string in the port
		gPort->show(x, y, labelString.NumUTF16TextChars(), labelString.GrabUTF16Buffer(nil));*/
		InterfacePtr<IN2PSqlPenUnLockData> labelData(shape, UseDefaultIID());
				if (labelData == nil)
				{
					TRACE("Label data == nil for this item");
					// We are often called in the general list to process, so return without assert.
					break; 
				}
		
                UIDRef boxRef = ::GetUIDRef( shape );
                if(boxRef == UIDRef::gNull )
                {
                	CAlert::InformationAlert("Nil boxRef*");
                	break;
                }
                InterfacePtr<IGeometry> itemGeometry(boxRef, UseDefaultIID() );
               if(itemGeometry == nil )
               {
               		CAlert::InformationAlert("Nil itemGeometry*");
               		break;
               }
                
                IGraphicsPort* gPort = gd->GetGraphicsPort();
                if(gPort == nil )
                {
                	CAlert::InformationAlert("Nil gPort*");
                	break;
                }
                
                PMRect bBox = itemGeometry->GetPathBoundingBox();
                
                gPort->gsave();
				
				//Lapiz Sombra
                gPort->newpath();
                gPort->moveto(bBox.Left() + 5, bBox.Top() + 5 );
                gPort->lineto(bBox.Left() + 8, bBox.Top() + 5 );
                gPort->lineto(bBox.Left() + 16, bBox.Top() -3 );
                gPort->lineto(bBox.Left() + 16, bBox.Top()  -5 );
                gPort->lineto(bBox.Left() + 14, bBox.Top() - 5 );
                gPort->lineto(bBox.Left() + 5, bBox.Top() + 4 );
                gPort->lineto(bBox.Left() + 5, bBox.Top() + 5 );
                gPort->setrgbcolor( PMReal(157/225), PMReal(165/225), PMReal(148/225 ) );
                gPort->fill();
				
				//Rosa borrador
                gPort->newpath();
                gPort->moveto(bBox.Left() + 14, bBox.Top() + -3 );
                gPort->lineto(bBox.Left() + 15.7, bBox.Top() -3);
                gPort->lineto(bBox.Left() + 15.7, bBox.Top()  -4.7 );
                gPort->lineto(bBox.Left() + 14, bBox.Top() + -4.7 );
                gPort->lineto(bBox.Left() + 14, bBox.Top() + -3 );
                gPort->setopacity(1, true );
                gPort->setrgbcolor(PMReal( 1 ), PMReal(191/225 ), PMReal(1 ) );
                gPort->fill();
                
                //Amarillo
                gPort->newpath();
                gPort->moveto(bBox.Left() + 13, bBox.Top() + -2 );
                gPort->lineto(bBox.Left() + 14.7, bBox.Top() -2);
                gPort->lineto(bBox.Left() + 7.7, bBox.Top() + 4.7 );
                gPort->lineto(bBox.Left() + 7.7, bBox.Top() + 3 );
                gPort->lineto(bBox.Left() + 6.3, bBox.Top() + 3 );
                gPort->lineto(bBox.Left() + 13.3, bBox.Top() - 3.7 );
                gPort->lineto(bBox.Left() + 13, bBox.Top() + -2 );
                 gPort->setopacity(1, true );
                gPort->setrgbcolor(PMReal(1), PMReal(1), PMReal(100/225) );
                gPort->fill();
                
                //Punta lapiz
                gPort->newpath();
                gPort->moveto(bBox.Left() + 5, bBox.Top() + 5 );
                gPort->lineto(bBox.Left() + 7.5, bBox.Top() + 5);
                gPort->lineto(bBox.Left() + 7.5, bBox.Top()  + 4.5 );
                gPort->lineto(bBox.Left() + 6.5, bBox.Top() + 4.5);
                gPort->lineto(bBox.Left() + 5, bBox.Top() + 4);
                 gPort->lineto(bBox.Left() + 5, bBox.Top() + 5);
               // gPort->setopacity(1, true );
                gPort->setrgbcolor(PMReal( 0 ), PMReal(0 ), PMReal( 0 ) );
                gPort->fill();
                
                
                
               //Linea negra
                gPort->setrgbcolor(PMReal(0), PMReal(0), PMReal(0));
				gPort->setlinewidth(PMReal(0.5));
				gPort->setmiterlimit(PMReal(0.5));  // SPAM: what units does this use?
				gPort->moveto(bBox.Left() + 7.7, bBox.Top() + 3);
				gPort->lineto(bBox.Left() + 13, bBox.Top() - 2);
				gPort->stroke();
				
				//Linea roja
				if(labelData->GetLockOrUnlock()<=0)
                {
                	gPort->setrgbcolor(PMReal(1), PMReal(0), PMReal(0));
					gPort->setlinewidth(PMReal(1));
					gPort->setmiterlimit(PMReal(1));  // SPAM: what units does this use?
					gPort->moveto(bBox.Left()+5, bBox.Top() -5);
					gPort->lineto(bBox.Left()+16, bBox.Top()+ 5);
					gPort->stroke();
               		//gPort->stroke();
                }
                
           
                // Restore the port setting to be the same as when we started
                gPort->grestore();
	} while (false); // Only do once.
}


/* GetPaintedAdornmentBounds
*/
PMRect N2PSqlPenUnlockAdornment::GetPaintedAdornmentBounds
	(
		IShape*				shape,
		AdornmentDrawOrder	drawOrder,
		const PMRect&		itemBounds,
		const PMMatrix&		innertoview	
	)
{

	PMRect aChildRect(itemBounds );
        innertoview.Transform(&aChildRect );
        return aChildRect;

}

/* Inval
*/
void N2PSqlPenUnlockAdornment::Inval
	(
		IShape*				iShape,
		AdornmentDrawOrder	drawOrder,
		GraphicsData*		gd, 
		ClassID 			reasonForInval, 
		int32 				flags
	)
{
}

