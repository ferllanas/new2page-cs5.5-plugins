//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkDocResponder.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IClassIDData.h"
#include "isignalmgr.h"
#include "IDocumentSignalData.h"
#include "IUIDData.h"
#include "IObserver.h"
#include "IDocument.h"
#include "ISubject.h"
#include "IBoolData.h"
#include "IWorkSpace.h"

#include "Utils.h"

// Implementation includes:
#include "CreateObject.h"
#include "CResponder.h"
#include "ExtLinkID.h"
#include "IExtLinkFacade.h"

/** 
	Handles signals related to document file actions.  The file
	action signals it receives are dictated by the ExtLinkDocServiceProvider
	class.

	ExtLinkDocResponder implements IResponder based on
	the partial implementation CResponder.

	We are interested in knowing when a documnet is opened or closed.  When these events happen,
	we have to attach/detach a document observer which will get notified when an XML element is
	unplaced.
	In ExtLinkDocServiceProvider, we registered our interests in kAfterNewDocSignalResponderService,
	kDuringOpenDocSignalResponderService, and kBeforeCloseDocSignalResponderService, that will allow
	us being notified when these event occurs.  In that case we will attach/detach the document
	observer accordingly.
	
	@ingroup extendedlink
	
*/
class ExtLinkDocResponder : public CResponder
{
	public:
	
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		ExtLinkDocResponder(IPMUnknown* boss);

		/**
			Respond() handles the file action signals when they
			are dispatched by the signal manager.  This implementation
			simply creates alerts to display each signal.

			@param signalMgr Pointer back to the signal manager to get
			additional information about the signal.
		*/
		virtual void Respond(ISignalMgr* signalMgr);

};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(ExtLinkDocResponder, kExtLinkDocResponderImpl)

/* ExtLinkDocResponder Constructor
*/
ExtLinkDocResponder::ExtLinkDocResponder(IPMUnknown* boss) :
	CResponder(boss)
{
}

/* ExtLinkDocResponder::Respond
*/
void ExtLinkDocResponder::Respond(ISignalMgr* signalMgr)
{
	// Get the service ID from the signal manager
	ServiceID serviceTrigger = signalMgr->GetServiceID();

	// Get a UIDRef for the document.  It will be an invalid UIDRef
	// for BeforeNewDoc, BeforeOpenDoc, AfterSaveACopy, and AfterCloseDoc because the
	// document doesn't exist at that point.
	InterfacePtr<IDocumentSignalData> iDocData(signalMgr, UseDefaultIID());
	if (iDocData == nil)
	{
		ASSERT_FAIL("Invalid IDocumentSignalData* - ExtLinkDocResponder::Respond");
		return;
	}
	
	UIDRef docRef = iDocData->GetDocument();
	InterfacePtr<IDocument> document(docRef, UseDefaultIID());

	// Take action based on the service ID
	switch (serviceTrigger.Get())
	{
		case kAfterNewDocSignalResponderService:
		{
			InterfacePtr<IObserver> iDocObserver(docRef, IID_IEXTLINKOBSERVER);
			if (iDocObserver != nil)
			{
				iDocObserver->AutoAttach();
			}
			
			if(!document)
				break;
			
			InterfacePtr<IBoolData> iDocShowInUI(document->GetDocWorkSpace(), IID_ISHOWINLINKSUI);
			ASSERT(iDocShowInUI);
			
			InterfacePtr<IWorkspace> iSessionWorkspace(GetExecutionContextSession()->QueryWorkspace());
			ASSERT(iSessionWorkspace);
			
			InterfacePtr<IBoolData> iShowInUI(iSessionWorkspace, IID_ISHOWINLINKSUI);
			ASSERT(iShowInUI);
			
			iDocShowInUI->Set(iShowInUI->Get());
			
			break;
		}
		
		case kDuringOpenDocSignalResponderService:
		{
			InterfacePtr<IObserver> iDocObserver(docRef, IID_IEXTLINKOBSERVER);
			if (iDocObserver != nil)
			{
				iDocObserver->AutoAttach();
			}
			
			//rebuild the odbc cache for the opended document
			if(document)
			{
				Utils<IExtLinkFacade>()->RebuildODBCCacheData();
			}
			break;
		}
		
		case kBeforeCloseDocSignalResponderService:
		{
			InterfacePtr<IObserver> iDocObserver(docRef, IID_IEXTLINKOBSERVER);
			if (iDocObserver != nil)
			{
				iDocObserver->AutoDetach();
			}
			break;
		}

		default:
			break;
	}

}

// End, ExtLinkDocResponder.cpp.



