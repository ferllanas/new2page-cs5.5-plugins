//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIDataModel.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes
#include "ILinkManager.h"
#include "IUIDListData.h"

// Impl includes
#include <map>
#include "LinkQuery.h"
#include "Utils.h"

// Project includes
#include "IExtLinkFacade.h"
#include "IExtLinkUIDataModel.h"
#include "ExtLinkUINodeID.h"
#include "ExtLinkUIDataNode.h"
#include "URI.h"
#include "ExtLinkID.h"


/**  Class representing a folder hierarchy. 
	@ingroup extendedlinkui
*/
class ExtLinkUIDataModel : CPMUnknown<IExtLinkUIDataModel>
{
public:
	/** Default constructor */
	ExtLinkUIDataModel(IPMUnknown* boss);

	/**	Destructor
	 */
	virtual ~ExtLinkUIDataModel();

	/**		See IExtLinkUIDataModel::GetCurrentDocDB
	 */
	virtual IDataBase* GetCurrentDocDB() const { return fCurrentDocDB; }

	/**		See IExtLinkUIDataModel::SetCurrentDocDB
	 */
	virtual void SetCurrentDocDB(IDataBase* newDocDB) { fCurrentDocDB = newDocDB; }

	/**		See IExtLinkUIDataModel::GetChildCount
	 */
	virtual int32 GetNumChildren(const URI& uri, const UID uid) const; 

	/**		See IExtLinkUIDataModel::GetNode
	 */
	virtual const ExtLinkUIDataNode* GetNode(const URI& uri, const UID uid) const;

	/**		See IExtLinkUIDataModel::GetNthChildData
	 */
	virtual const ExtLinkUIDataNode* GetNthChild(const ExtLinkUIDataNode* parentNode, int32 nth) const;

	/**		See IExtLinkUIDataModel::GetNthChildData
	 */
	virtual bool16 GetNthChildData(const URI& parentURI, const UID parentUID, int32 nth, URI& childURI, UID& childUID) const; 

	/**		See IExtLinkUIDataModel::GetChildIndexFor 
	 */
	virtual int32 GetChildIndexFor(const URI& parentNodeURI, const UID parentLinkUID, const URI& childNodeURI, const UID childLinkUID) const;

	/**	See IExtLinkUIDataModel::Rebuild
	 */
	virtual void Rebuild();
	
	/**	See IExtLinkUIDataModel::GetRootURI
	 */
	virtual URI GetRootURI() const { return URI("root"); }


protected:

	/**	Destroy the tree represented in this 
	 */
	void deleteTree();


private:
	struct NodeInfo 
	{
		NodeInfo( URI uri, UID uid) {fURI = uri; fUID = uid;}
		
		URI fURI;
		UID fUID;
		
		bool operator < (const NodeInfo& info) const 
		{
			if (fUID < info.fUID)
				return true;
			else if (fUID == info.fUID)
				return fURI < info.fURI;
			else 
				return false;
		}
	};
	
	
	std::map<NodeInfo, ExtLinkUIDataNode*> fNodeMap;
	ExtLinkUIDataNode* fRootNode;
	IDataBase* fCurrentDocDB;
};



/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(ExtLinkUIDataModel, kExtLinkUIDataModelImpl)



/* Constructor
*/
ExtLinkUIDataModel::ExtLinkUIDataModel(IPMUnknown* boss) : 
	CPMUnknown<IExtLinkUIDataModel>(boss),
	fRootNode(nil),
	fCurrentDocDB(nil)
{
}


	
/* Destructor
*/
ExtLinkUIDataModel::~ExtLinkUIDataModel()
{
	deleteTree();
}



/* deleteTree
*/
void ExtLinkUIDataModel::deleteTree() 
{
	// We've flattened the tree in our hashtable anyway so use this to delete the nodes.
	std::map<NodeInfo, ExtLinkUIDataNode* >::iterator iter;
	for(iter = fNodeMap.begin(); iter != fNodeMap.end(); iter++)
	{
		ExtLinkUIDataNode* node = iter->second;
		ASSERT(node);
		delete node;
	}
	fNodeMap.clear();
	fRootNode = nil;
}


/* Rebuild
*/
void ExtLinkUIDataModel::Rebuild()
{
	if(fRootNode != nil) {
		deleteTree();
	}
	
	if (fCurrentDocDB) 
	{
		fRootNode =  new ExtLinkUIDataNode( GetRootURI(), kRootDocumentExtLinkNodeUID);
		NodeInfo rootInfo(GetRootURI(), kRootDocumentExtLinkNodeUID);

		UIDList resultUIDs(fCurrentDocDB);
		InterfacePtr<ILinkManager> linkMgr(fCurrentDocDB, fCurrentDocDB->GetRootUID(), UseDefaultIID());
		LinkQuery query; 
		linkMgr->QueryLinks(query, kIDLinkClientID, resultUIDs);

		// -->  Add all links with URIs using my scheme to the map
		UIDList odbcLinks(fCurrentDocDB);
		for(UIDList::iterator iter = resultUIDs.begin(); iter != resultUIDs.end(); ++iter)
		{
			UID linkUID = *iter;
			InterfacePtr<ILink> theLink(fCurrentDocDB, linkUID, UseDefaultIID());
			if(theLink) 
			{
				InterfacePtr<ILinkResource> theLinkResource(fCurrentDocDB, theLink->GetResource(), UseDefaultIID()); 
				if (theLinkResource) 
				{
					URI linkURI = theLinkResource->GetURI();					
					static const WideString kMyScheme(kExtLinkScheme);
					WideString theScheme = linkURI.GetComponent(URI::kScheme);
					if (theScheme == kMyScheme)
					{
						// Add all links with my scheme to a flat map - grouping comes later
						odbcLinks.Append(linkUID);
						NodeInfo info(linkURI, *iter);					
						ExtLinkUIDataNode* newNode = new ExtLinkUIDataNode(info.fURI, info.fUID);
						fRootNode->AddChild(static_cast<const ExtLinkUIDataNode*>(newNode));
						newNode->SetParent(fRootNode);				
						fNodeMap.insert( std::pair<NodeInfo, ExtLinkUIDataNode*>(info, newNode));
					}
				}
			}
		}
#if 0
		// LJH: set links ui flag to false on our links so they don't show in the Links Panel
		Utils<IExtLinkFacade> facade;
		ASSERT(facade);
		if (!odbcLinks.IsEmpty()){
			facade->ProcessShowInLinksUICommand(kFalse, odbcLinks);	
		}
#endif
		//
		// group all links based on their URI
		//
		// keep track of the group nodes to add when all nodes are regrouped
		std::map<NodeInfo, ExtLinkUIDataNode*> groupMap;
		
		// perform grouping top-down based on the components of the URI
		URI::Component components[] = {URI::kScheme, URI::kAuthority, URI::kPath, URI::kQuery, URI::kFragment};
		for (int32 cIndex = 0; cIndex < 5; cIndex++)
		{
			// run through all the links for each component - process only the bottom-most children each pass
			std::map<NodeInfo, ExtLinkUIDataNode*>::iterator nodeIter;
			for(nodeIter = fNodeMap.begin(); nodeIter != fNodeMap.end(); ++nodeIter)
			{
				// Link must be our bottom-most level, so make sure link has no kids and has not already been regrouped
				ExtLinkUIDataNode* node = nodeIter->second;
				WideString componentValue(node->GetNodeURI().GetComponent(components[cIndex]));
				if (node->ChildCount() == 0 && 
					(componentValue != WideString("") && 
					!(components[cIndex] == URI::kPath && componentValue == WideString("/"))))
				{
					// build a group URI using the values of the first cIndex components of the node's URI
					URI groupURI;
					
					/* Force the kPath component of the URI to contain '/'.
						These group nodes are created by using a fake URI to store the necessary data to
						represent the group's title.  The reason we need a valid kPath component is to 
						maintain compatibility with URI's read/write from stream.  When a URI is output to
						a stream, it is written using the string returned from GetURI(), and that string will 
						always have '/' prepended for the path, whether the path component exists or not.  
						When the URI is streamed back in, the code sees the '/' for the path and sets the 
						URI's kPath component to "/".
						When the tree is read back in after being persisted, the tree tries to rebuild itself
						by comparing its persisted data to this model data.  If we don't have our kPath
						component initialized to the same value as the persisted data, group nodes are not found,
						and the tree ends up being truncated.
					*/
					groupURI.SetComponent(URI::kPath, WideString("/"));

					for (int32 c = 0; c <= cIndex; c++) {
						groupURI.SetComponent(components[c], node->GetNodeURI().GetComponent(components[c]));
					}
					
					// see if the group node already exists
					NodeInfo groupNodeInfo(groupURI, kInvalidUID);
					ExtLinkUIDataNode* groupNode = nil;
					std::map<NodeInfo, ExtLinkUIDataNode*>::const_iterator groupIter = groupMap.find(groupNodeInfo);
					if (groupIter == groupMap.end()) {
						// does not exist, create the group node
						groupNode = new ExtLinkUIDataNode(groupNodeInfo.fURI, groupNodeInfo.fUID);
						node->GetParent()->AddChild(static_cast<const ExtLinkUIDataNode*>(groupNode));
						groupNode->SetParent(node->GetParent());				
						groupMap.insert( std::pair<NodeInfo, ExtLinkUIDataNode*>(groupNodeInfo, groupNode));
					} else {
						groupNode = groupIter->second;
					}
					// make the current node a child of the group node
					node->GetParent()->RemoveChild(static_cast<const ExtLinkUIDataNode*>(node));
					groupNode->AddChild(static_cast<const ExtLinkUIDataNode*>(node));
					node->SetParent(groupNode);	
				}
			}
		}
		
		// last, add the group and root nodes to the map
		std::map<NodeInfo, ExtLinkUIDataNode*>::iterator nodeIter;
		for(nodeIter = groupMap.begin(); nodeIter != groupMap.end(); ++nodeIter){
			fNodeMap.insert( std::pair<NodeInfo, ExtLinkUIDataNode*>(nodeIter->first, nodeIter->second));
		}
		fNodeMap.insert( std::pair<NodeInfo, ExtLinkUIDataNode*>(rootInfo, fRootNode));
	}
}


/* GetChildCount
*/
//int32 ExtLinkUIDataModel::GetChildCount(const PMString& data) // not const any more
int32 ExtLinkUIDataModel::GetNumChildren(const URI& uri, const UID uid) const
{
	NodeInfo nodeInfo(uri, uid);

	std::map<NodeInfo, ExtLinkUIDataNode*>::const_iterator iter = fNodeMap.find(nodeInfo);
	if (iter != fNodeMap.end()) {
		return iter->second->ChildCount();
	}
	return 0;
}


/* GetNthChild
*/
const ExtLinkUIDataNode* ExtLinkUIDataModel::GetNthChild(const ExtLinkUIDataNode* parentNode, int32 nth) const
{
	ExtLinkUIDataNode* childNode = nil;
	if (parentNode) {
		if(parentNode->ChildCount() > nth && nth >= 0) {
			childNode = const_cast<ExtLinkUIDataNode*>(parentNode->GetNthChild(nth));
		}
	}
	return childNode;
}


/* GetNthChildData
*/
const ExtLinkUIDataNode* ExtLinkUIDataModel::GetNode(const URI& uri, const UID uid) const
{
	ExtLinkUIDataNode* node = nil;
	
	NodeInfo nodeInfo(uri, uid);
	std::map<NodeInfo, ExtLinkUIDataNode* >::const_iterator result = fNodeMap.find(nodeInfo);
	if (result != fNodeMap.end()) {
		node = result->second;
	}
	return node;
}


/* GetNthChildData
*/
bool16 ExtLinkUIDataModel::GetNthChildData(const URI& parentURI, const UID parentUID, int32 nth, URI& childURI, UID& childUID) const
{
	NodeInfo parentInfo(parentURI, parentUID);
	std::map<NodeInfo, ExtLinkUIDataNode* >::const_iterator result = fNodeMap.find(parentInfo);
	if (result != fNodeMap.end()) {
		ExtLinkUIDataNode* parentNode = result->second;
		if (parentNode) {
			if(parentNode->ChildCount() > nth) {
				const ExtLinkUIDataNode* childNode = parentNode->GetNthChild(nth);
				childURI = childNode->GetNodeURI();
				childUID = childNode->GetLinkUID();
				return true;
			}
		}
	}
	return false;
}


/* GetChildIndexFor
*/
int32  ExtLinkUIDataModel::GetChildIndexFor(const URI& parentNodeURI, const UID parentLinkUID, const URI& childNodeURI, const UID childLinkUID) const
{
	int32 retval = -1;
	NodeInfo parentInfo(parentNodeURI, parentLinkUID);
	std::map<NodeInfo, ExtLinkUIDataNode*>::const_iterator result = fNodeMap.find(parentInfo);
	if (result != fNodeMap.end()) 
	{
		ExtLinkUIDataNode* parentNode = result->second;
		for (int32 i = 0; i < parentNode->ChildCount(); i++)
		{
			const ExtLinkUIDataNode* childNode = parentNode->GetNthChild(i);
			if (childNode->GetNodeURI() == childNodeURI && childNode->GetLinkUID() == childLinkUID) {
				retval = i;
				break;
			}
		}
	}
	return retval;
}



//	end, File: ExtLinkUIDataModel.cpp
