//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/paneltreeview/N2PInOutStartupShutdown.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: ipaterso $
//  
//  $DateTime: 2005/02/25 04:54:14 $
//  
//  $Revision: #6 $
//  
//  $Change: 320645 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IIdleTask.h"
#include "IStartupShutdownService.h"
#include "IControlView.h"
#include "ICommandInterceptor.h"
#include "ICommandProcessor.h"

// General includes:
#include "CPMUnknown.h"
#include "CAlert.h"

// Project includes:
#include "N2PInOutID.h"
#include "N2PsqlLogInLogOutUtilities.h"

/** Implements IStartupShutdownService; purpose is to install the idle task.

	
	@ingroup paneltreeview
*/
class N2PInOutStartupShutdown : 
	public CPMUnknown<IStartupShutdownService>
{
public:
	/**	Constructor
		@param boss interface ptr from boss object on which this interface is aggregated.
	 */
    N2PInOutStartupShutdown(IPMUnknown* boss );

	/**	Destructor
	 */
	virtual ~N2PInOutStartupShutdown() {}

	/**	Called by the core when app is starting up
	 */
    virtual void Startup();

	/**	Called by the core when app is shutting down
	 */
    virtual void Shutdown();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2PInOutStartupShutdown, kN2PInOutStartupShutdownImpl)

/* Constructor
*/
N2PInOutStartupShutdown::N2PInOutStartupShutdown(IPMUnknown* boss) :
    CPMUnknown<IStartupShutdownService>(boss)
{
}


/* Startup
*/
void N2PInOutStartupShutdown::Startup()
{
    do 
	{
		//PlgPro10Utils::CargarPfrDefaultToInDesign();
		InterfacePtr<IIdleTask> task(GetExecutionContextSession(), IID_IN2PLogInTask);
		ASSERT(task);
		if(!task)
		{
			break;
		}
		task->InstallTask(0);
		
		InterfacePtr<ICommandInterceptor> cmdInterceptor ( this, UseDefaultIID() );
		if( cmdInterceptor )
		{
			//CAlert::InformationAlert("Debio ser instalado pero veamos que rollo");
			InterfacePtr<ICommandProcessor> proc ( GetExecutionContextSession()->QueryCommandProcessor() );
			proc->InstallInterceptor( cmdInterceptor );
		} 


		//N2PsqlLogInLogOutUtilities::DoLogInUser();
	} while(kFalse);
}


/* Shutdown
*/
void N2PInOutStartupShutdown::Shutdown()
{
	do 
	{
		InterfacePtr<ICommandInterceptor> cmdInterceptor ( this, UseDefaultIID() );
		if( cmdInterceptor )
		{
			InterfacePtr<ICommandProcessor> proc ( GetExecutionContextSession()->QueryCommandProcessor() );
			proc->DeinstallInterceptor( cmdInterceptor );
		} 
		
		N2PsqlLogInLogOutUtilities::DoLogOutUserWhenShutDownApp();
		
	} while(kFalse);
}

 