//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/IExtLinkChangeOptionsCmdData.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IExtLinkCHANGEOPTIONSCMDDATA__
#define __IExtLinkCHANGEOPTIONSCMDDATA__

#include "ExtLinkID.h"
class ExtLinkOptionsData;

/** data interface for ExtLinkChangeOptionsCmd, responsible for changing options for this plug-in.

	@see ExtLinkChangeOptionsCmd
	@see ExtLinkOptionsData
	@ingroup extendedlink
*/

class IExtLinkChangeOptionsCmdData : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IEXTLINKCHANGEOPTIONSCMDDATA };

	/** Method to append option to list of options
		@param newVal [IN] new  value to append to option-list
	*/
	virtual void AddOption(const ExtLinkOptionsData& newVal)=0; 

	/** Accessor for stored option by index
		@param index [IN] specifies position of interest (zero-based)
		@return the option at this position
	*/
	virtual ExtLinkOptionsData GetNthOption(const int32 index)=0; 


	/**	Return count of options on this data interface
		@return int32 returned giving the number of options currently cached
	 */
	virtual int32 GetOptionCount()=0;

};


#endif // __IExtLinkCHANGEOPTIONSCMDDATA__
