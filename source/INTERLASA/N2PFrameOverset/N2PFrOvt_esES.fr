//========================================================================================
//  
//  $File: $
//  
//  Owner: Fernando Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifdef __ODFRC__

// English string table is defined here

resource StringTable (kSDKDefStringsResourceID + index_esES)
{
        k_esES,									// Locale Id
        kEuropeanWinToMacEncodingConverter,		// Character encoding converter (irp) I made this WinToMac as we have a bias to generate on Win...
        {
        	  // ----- Menu strings
                kN2PFrOvtCompanyKey,					kN2PFrOvtCompanyValue,
                kN2PFrOvtAboutMenuKey,					kN2PFrOvtPluginName "[ES]...",
                kN2PFrOvtPluginsMenuKey,				kN2PFrOvtPluginName "[ES]",
	
                kSDKDefAboutThisPlugInMenuKey,			kSDKDefAboutThisPlugInMenuValue_enUS,

                // ----- Command strings

                // ----- Window strings

                // ----- Panel/dialog strings


		// ----- Misc strings
                kN2PFrOvtAboutBoxStringKey,			kN2PFrOvtPluginName " [ES], version " kN2PFrOvtVersion " by " kN2PFrOvtAuthor "\n\n" kSDKDefCopyrightStandardValue "\n\n" kSDKDefPartnersStandardValue_enUS,

		
        }

};

#endif // __ODFRC__

//  Code generated by DollyXs code generator
