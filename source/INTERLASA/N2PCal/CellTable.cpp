//========================================================================================
// DATE: 10/09/08 
//========================================================================================

#include "VCPluginHeaders.h"

#include "CellTable.h"

/* CellData
*/
CellData::CellData()
{
}

/* ~CellData
*/
CellData::~CellData()
{
}

/* SetString
*/
bool CellData::SetString(const PMString& str)
{
	string = str;
	return true;
}

/* GetString
*/
const PMString& CellData::GetString(void)
{
	return string;
}

/* CellTable
*/
CellTable::CellTable(int32 numColumn, int32 numRow)
{
	table = new CellData[numColumn*numRow];
	maxColumn = numColumn;
	maxRow = numRow;
}

/* ~CellTable
*/
CellTable::~CellTable()
{
	delete[] table;
}

/* SetString
*/
bool CellTable::SetString(const PMString& str, int32 column, int32 row)
{
	if((0 <= column) || (0 <= row)){
		if((column <= maxColumn) || (row <= maxRow)){
			table[row * maxColumn + column ].SetString(str);
			return true;
		}
	}
	
	return false;
}
 
 /* GetString
 */
const PMString& CellTable::GetString(int32 column, int32 row)
{
	if((0 <= column) || (0 <= row)){
		if((column <= maxColumn) || (row <= maxRow)){
			return table[row * maxColumn + column ].GetString();
		}
	}
	
	return kNullString;
}	

// End, CellTable.cpp.
