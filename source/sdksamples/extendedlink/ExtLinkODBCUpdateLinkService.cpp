//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkODBCUpdateLinkService.cpp $
//  
//  Owner: Lin Xia
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "ICommand.h"
#include "IDataBase.h"
#include "IDocument.h"
#include "IIDXMLElement.h"
#include "IImportXMLData.h"
#include "ILink.h"
#include "ILinkObject.h"
#include "ILinkObjectReference.h"
#include "ILinkResource.h"
#include "IPMUnknownData.h"
#include "IUpdateLinkService.h"
#include "IXMLImportOptions.h"
#include "IXMLImportOptionsPool.h"
#include "IXMLReferenceData.h"
#include "IXMLUtils.h"
#include "ISelectUtils.h"
#include "IActiveContext.h"
#include "IWidgetUtils.h"
#include "IControlView.h"
#include "IExtLinkFacade.h"
#include "ExtLinkUtils.h"
#include "textiterator.h"
#include "SDKODBCCache.h"
#include "ILinkFacade.h"
#include "ILinkResource.h"
#include "ITextStrand.h"
#include "DataWrapper.h"

#include "CPMUnknown.h"

#include "LayoutUIID.h"
#include "OpenPlaceID.h"
#include "ExtLinkID.h"

#include "IURIUtils.h"
#include "SDKODBCWrapper.h"

#include  "K2Vector.tpp"

/**
	An interface used by ILink::Update() to update a link.
	
	A link object implementation can specify the way to import from resource to link object/export 
	from link object to link resource either thru a direct Import/Export call in ILinkObject or 
	through a service provider, whose provider class are specified also in ILinkObject implementation.
	
	ExtLink's ILinkObject implementation, ExtLinkODBCXMLElementLinkObject, chose to implement the 
	import/export through the service provider kExtLinkODBCUpdateLinkServiceProviderBoss. And 
	ExtLinkODBCUpdateLinkService provides the actual import/export service.
	
	@ingroup extendedlink
	@see ExtLinkODBCXMLElementLinkObject
	@see ILink
*/
class ExtLinkODBCUpdateLinkService : public CPMUnknown<IUpdateLinkService>
{
public:
	ExtLinkODBCUpdateLinkService(IPMUnknown *boss);

	/**
		@see IUpdateLinkService::CanUpdateLink
	*/
	virtual bool16		CanUpdateLink(const ILink* iLink);

	/**
		@see IUpdateLinkService::PreUpdateLink
	*/
	virtual ErrorCode	PreUpdateLink(ILink* iLink);

	/**
		@see IUpdateLinkService::PostUpdateLink
	*/
	virtual ErrorCode	PostUpdateLink(ILink* iLink);

	/**
		Need to know it the update is for import or export because our link is bi-directional.
		From ExtLinkUI, when a user modifies the text ranges that's associated with our link and
		when the user chooses to Update the database from the menu, we mark the link's object
		modification state to ILink::kObjectModified, we use this flag to decide if we are doing
		an import or export here.
		@see IUpdateLinkService::DoUpdateLink
	*/
	virtual ErrorCode	DoUpdateLink(ILink* iLink, UID& objectUID, UIFlags uiFlags);
	
private:
	WideString GetTextSpan(ITextModel *textModel,TextIndex start, int32 length);
	
};


CREATE_PMINTERFACE(ExtLinkODBCUpdateLinkService, kExtLinkODBCUpdateLinkServiceImpl)

//---------------------------------------------------------------
// ExtLinkODBCUpdateLinkService::ExtLinkODBCUpdateLinkService
//---------------------------------------------------------------
ExtLinkODBCUpdateLinkService::ExtLinkODBCUpdateLinkService(IPMUnknown *boss) :
	CPMUnknown<IUpdateLinkService>(boss)
{
}

//---------------------------------------------------------------
// ExtLinkODBCUpdateLinkService::CanUpdateLink
//---------------------------------------------------------------
bool16 ExtLinkODBCUpdateLinkService::CanUpdateLink(const ILink* iLink)
{
	return (::GetClass(iLink) == kExtLinkODBCXMLLinkObjectReferenceBoss);
}

//---------------------------------------------------------------
// ExtLinkODBCUpdateLinkService::PreUpdateLink
//---------------------------------------------------------------
ErrorCode ExtLinkODBCUpdateLinkService::PreUpdateLink(ILink* iLink)
{
	// no op
	return kSuccess;
}

//---------------------------------------------------------------
// ExtLinkODBCUpdateLinkService::PostUpdateLink
//---------------------------------------------------------------
ErrorCode ExtLinkODBCUpdateLinkService::PostUpdateLink(ILink* iLink)
{
	// no op
	return kSuccess;
}

//---------------------------------------------------------------
// ExtLinkODBCUpdateLinkService::DoUpdateLink
//---------------------------------------------------------------
ErrorCode ExtLinkODBCUpdateLinkService::DoUpdateLink(ILink* iLink, UID& objectUID, UIFlags uiFlags)
{
	ErrorCode err = kFailure;
	if (!iLink || objectUID == kInvalidUID)
		return err;

	IDataBase *db = ::GetDataBase(iLink);
	// get the element associated with this link
	InterfacePtr<ILinkObject> iLinkObject(db, objectUID, UseDefaultIID());
	InterfacePtr<ILinkResource> iLinkResource(db, iLink->GetResource(), UseDefaultIID());
	InterfacePtr<IIDXMLElement> element((IIDXMLElement*)iLinkObject->QueryLinkedObject(IID_IIDXMLELEMENT));

	if (element)
	{
		InterfacePtr<ITextModel> elementModel(Utils<IXMLUtils>()->QueryTextModel(element));
		// get the element content reference and content type
		const XMLContentReference& contentReference = element->GetContentReference();
		XMLContentReference::ContentType contentType(contentReference.GetContentType());
		// We only handle text content type and the element is supposed to be text content in this sample 
		if(contentType == XMLContentReference::kContentType_Text)
		{
			TextIndex startPos(kInvalidTextIndex);
			TextIndex endPos(kInvalidTextIndex);	
			if (!Utils<IXMLUtils>()->GetElementIndices(element, &startPos, &endPos))
			{
				// If this is not tagged text, get the first item in in the text.
				InterfacePtr<ITextModel> storyModel(contentReference.GetUIDRef(), UseDefaultIID());
				if (storyModel)
				{
					startPos = -1;
					endPos = storyModel->GetPrimaryStoryThreadSpan() - 1;
				}
			}
			
			if (iLink->GetObjectModificationState() == ILink::kObjectModified)
			{
				TextIterator begin(elementModel, startPos+1);
				TextIterator end(elementModel, endPos);
				PMString newValue("");
				for (TextIterator iter = begin; iter != end; iter++) 
				{
					// borrowed this code from SnpInspectTextModel to get char out from UTF32TextChar
					const UTF32TextChar characterCode = *iter;
					PMString character;
					character.AppendW(characterCode);
					TextIndex position = iter.Position();
					newValue += character;
				}	
				URI uri = iLinkResource->GetURI();
				SDKODBCWrapper	odbc;
				if (ISDKODBCWrapper::ODBCNormal == odbc.Query(uri, WideString(newValue)))
				{
					K2Vector<ODBCCacheData> result =  SDKODBCCache::Instance().GetCacheData(uri, true);
					if (result.size() <= 0) 
					{
						ASSERT_FAIL("The URI does not exist in the cache.");
						return kFailure;
					}
					else if (result[0].fFieldData != WideString(newValue))	// the cache value after the update should be the same
					{
						if (result[0].fFieldType == ISDKODBCWrapper::kIntegerType || result[0].fFieldType == ISDKODBCWrapper::kDoubleType)
						{
							PMString resultStr(result[0].fFieldData);
							double cacheNumber = resultStr.GetAsDouble();
							double pubNumber = newValue.GetAsDouble();
							if (cacheNumber == pubNumber)
							{
								//Replace underlying text with new value
								boost::shared_ptr<WideString> wideVal(new WideString(result[0].fFieldData));
								// the text to be replaced needs to exclude the hidden text we insert for tag
								err = ExtLinkUtils::ReplaceText(elementModel, startPos+1, endPos - startPos-1, wideVal);
							}
						}
						else
						{
							ASSERT_FAIL("Cache out of sync! you may get wrong status update");
							return kFailure;
						}
					}
					else
					{
						Utils<Facade::ILinkFacade> iLinkFacade;
						Utils<IExtLinkFacade> iExtLinkFacade;
						if (iExtLinkFacade && iLinkFacade)
						{
							// ater we update the cache successfully, we should set the resource stamp to reflect the
							// new value to avoid state out of sync
							WideString scheme = uri.GetComponent(URI::kScheme);
							WideString newStamp(scheme);
							newStamp += WideString(newValue);
							UIDList rsrcList(db, ::GetUID(iLinkResource));
							iLinkFacade->UpdateResourceStates(rsrcList, ILinkManager::kSynchronous);	// need to wait for the state update completed before we move on.
						//	iLinkResource->SetStamp(newStamp);
							UIDList uidList(db, ::GetUID(iLink));
							iExtLinkFacade->ProcessSetLinkResourceModStateCommand(ILink::kResourceUnmodified, uidList);
							TRACEFLOW("ExtLink", "Set resource state to unmodified\n");
						}
						return kSuccess;
					}
				}
				else
				{
					ASSERT_FAIL("Failed to update database thru SDKODBCWrapper");
					return kFailure;
				}					
			}
			else
			{
				URI uri = iLinkResource->GetURI();
				WideString newValue; 
				{
					if (!iLinkResource->CanQueryStream(ILinkResource::kRead))
						return kFailure;
					InterfacePtr<IPMStream> importStream(iLinkResource->QueryStream(ILinkResource::kRead));
					if (!importStream)
						return kFailure;

					StreamState streamState = importStream->GetStreamState();
					if (streamState == kStreamStateFailure || streamState == kStreamStateClosed)
					{
						// this can happen if we fail to open a file.
						// an open Word file on a network is a classic example.
						return kFailure;
					}
					if (Utils<IExtLinkFacade>()->GetAssetValueFromResourceStream(importStream, newValue))
					{
						//Replace underlying text with new value
						boost::shared_ptr<WideString> wideVal(new WideString(newValue));
						// the text to be replaced needs to exclude the hidden text we insert for tag
						err = ExtLinkUtils::ReplaceText(elementModel, startPos+1, endPos - startPos-1, wideVal);
					}
				}
			}			
		}
	}
	return err;
}
WideString ExtLinkODBCUpdateLinkService::GetTextSpan(ITextModel *textModel,TextIndex start, int32 length)
{
	int32 totalLen = textModel->TotalLength();
	if ( start + length > totalLen ) length = totalLen - start;
	
	InterfacePtr<ITextStrand> iStrand((ITextStrand*)textModel->QueryStrand( kTextDataStrandBoss, IID_ITEXTSTRAND) );
	if( iStrand && start>=0 && length > 0 )
	{
		int32 chunkSize = 0;
		WideString wstr;
		
		while (length > 0)
		{
			DataWrapper<textchar> chunk = iStrand->FindChunk(start, &chunkSize);
			
			chunkSize = ::minimum(length, chunkSize);
			if( chunkSize > 0 )
			{
				wstr.Append(chunk, chunkSize);	
				
				start += chunkSize;
				length -= chunkSize;
			}
		}
		
		return wstr;
	}
	
	return WideString();
}
