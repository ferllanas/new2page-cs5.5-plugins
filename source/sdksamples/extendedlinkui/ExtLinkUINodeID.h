//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUINodeID.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//  
//  Each C++ implementation class in the plug-in and its corresponding ImplementationID
//  should be registered in this file.
//  
//========================================================================================

#ifndef __ExtLinkUINodeID__
#define __ExtLinkUINodeID__

#include "IPMStream.h"

#include "NodeID.h"
#include "URI.h"

#include "ExtLinkUIID.h"


/** Represents a node in the extended link heirarchy.  Parent nodes are grouping nodes and
	do not represent an actual link in the links architecture.
	Overriding this class lets us populate the tree with our own data type that
	represents whatever we want on each node.

	
	@ingroup extendedlinkui
*/
class ExtLinkUINodeID : public NodeIDClass
{
	public:
		enum { kNodeType = kExtLinkUITreeViewWidgetBoss }; 


		/**	Destructor
		 */
		virtual ~ExtLinkUINodeID();
		

		/**	@return type of this node
		 */
		virtual	NodeType GetNodeType() const { return kNodeType; } 

		/**	Comparator function
			@param NodeID [IN] specifies the node that we should compare against
			@return  Comparison results
		 */
		virtual int32 Compare(const NodeIDClass* NodeID) const;

		/**	Create a copy of this
			@return  
		 */
		virtual NodeIDClass* Clone() const;

		/**	Read this from the given stream
			@param stream [IN] specified stream
			@return  
		 */
		virtual void Read(IPMStream* stream);

		/**	Write this to the given stream
			@param [OUT] stream 
			@return  
		 */
		virtual void Write(IPMStream* stream) const;

		/** Accessor for the link's UID, if it does not exist, kInvalidUID is returned
			@return UID
		*/
		UID GetLinkUID() const { return fLinkUID; }

		/** Accessor for the link's URI
			@return const URI&
		*/
		const URI& GetNodeURI() const { return fURI; }


		/** Accessor for the nodes Parent node
			@return NodeID_rv
		*/
		const NodeID_rv GetParentNode() const { return fParentNode; }


		/** Factory method
			@return NodeID_rv
		*/
		static NodeID_rv Create( const URI& uri, const UID linkUID, const NodeID& parentNode) 
		{ 
			return new ExtLinkUINodeID(uri, linkUID, parentNode);
		}

	private:
		// Note we're keeping the constructor private to force use of the factory methods
		ExtLinkUINodeID( const URI& uri, const UID linkUID, const NodeID& parentNode);
		ExtLinkUINodeID();
		ExtLinkUINodeID(const ExtLinkUINodeID& copy);
		
		URI		fURI;
		UID		fLinkUID;
		NodeID	fParentNode;
};

#endif // __ExtLinkUINodeID__


