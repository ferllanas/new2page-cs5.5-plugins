//========================================================================================
//
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/flexuistroke/flexuistroke.jsx $
//
//  Owner: Adobe Developer Technologies
//
//  $Author: alokumar $
//
//  $DateTime: 2011/04/01 11:22:15 $
//
//  $Revision: #1 $
//
//  $Change: 781912 $
//
//  Copyright 2009 Adobe Systems Incorporated. All rights reserved.
//
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or
//  distribution of it requires the prior written permission of Adobe.
//
//
//	This jsx file exists because in the release CS5 release it is not possible
//	to register an ActionScript event handler.  This code works around this by
//	registering ExtendScript event handlers and calling an ActionScript update 
//	method from the ExtendScript event handler.
//
//========================================================================================

/**
 * Tells the Flash extension to update the stroke weight now.
 */
 function updateNow()
{
	_flashExtensionInterface.setPanelStrokeWeight();
}

/**
 * Add event listeners.
 * 
 * Note: This is the whole point of this ExtendScript Code.
 */
function addEventListeners()
{
	app.addEventListener(Event.AFTER_SELECTION_CHANGED, updateNow, false);
	app.addEventListener(Event.AFTER_SELECTION_ATTRIBUTE_CHANGED, updateNow, false);
	app.addEventListener(Event.AFTER_CONTEXT_CHANGED, updateNow, false);
	app.addEventListener(Event.AFTER_NEW, updateNow, false);
	app.addEventListener(Event.AFTER_OPEN, updateNow, false);
	app.addEventListener(Event.AFTER_CLOSE, updateNow, false);
}

/**
 * Remove event listeners.
 */
function removeEventListeners()
{
	app.removeEventListener(Event.AFTER_SELECTION_CHANGED, updateNow, false);
	app.removeEventListener(Event.AFTER_SELECTION_ATTRIBUTE_CHANGED, updateNow, false);
	app.removeEventListener(Event.AFTER_CONTEXT_CHANGED, updateNow, false);
	app.removeEventListener(Event.AFTER_NEW, updateNow, false);
	app.removeEventListener(Event.AFTER_OPEN, updateNow, false);
	app.removeEventListener(Event.AFTER_CLOSE, updateNow, false);
}

/**
 * Registers flash extension with InDesign.
 *
 * Note: The point of this is to store a reference to the Flash extension.
 */
function registerFlashExtension(flashExtension) 
{
	_flashExtensionInterface = flashExtension;
	updateNow();
}

var _flashExtensionInterface;

