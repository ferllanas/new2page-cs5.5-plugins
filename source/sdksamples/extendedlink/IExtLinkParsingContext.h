//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/IExtLinkParsingContext.h $
//  
//  Owner: Ian Paterson
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#pragma once
#ifndef __IExtLinkParsingContext_h__
#define __IExtLinkParsingContext_h__

#include "IPMUnknown.h"
#include "XMLReference.h"
#include "ExtLinkID.h"

/** Stores state that we need during parsing.
	@see kXMLParserLocatorBoss.

	@ingroup xmlExtLink
*/
class IExtLinkParsingContext : public IPMUnknown
{
	public:
		enum { kDefaultIID = IID_IExtLinkPARSINGCONTEXT }; 

		typedef enum
		{
			kOutside, 
			kWithinTableHasChildren,
			kWithinTableNoChild,
			kWithinRecordHasChildren,
			kWithinRecordNoChild
		} ParsingState;

		/** Set up the current parsing state
			@param state specifying the ParsingState
		*/
		virtual void SetParsingState(const ParsingState& state)=0;

		/** Get parsing state
			@return ParsingState 
		*/
		virtual ParsingState GetParsingState()  const =0;

		/** Set up the current parsing element
			@param state specifying the parsing element
		*/
		virtual void SetParsingElement(const XMLReference& element) = 0;

		/** Get parsing element
			@return Current parsing element 
		*/
		virtual XMLReference GetParsingElement() const = 0;
	
};

#endif // __IExtLinkParsingContext_h__


