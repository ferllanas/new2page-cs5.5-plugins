//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/FileTreeUtils.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __N2PSQLLOGINLOGOUTUTILITIES_H_DEFINED__
#define __N2PSQLLOGINLOGOUTUTILITIES_H_DEFINED__

#ifdef WINDOWS
	#include "..\N2PSQL\N2PsqlID.h"
#endif

#ifdef MACINTOSH
	#include "../../../source/INTERLASA/N2PSQL/N2PsqlID.h"
#endif

//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")

class IDocument;
class IControlView;


/** A collection of miscellaneous utility methods used by this plug-in.
@ingroup paneltreeview
*/
class N2PsqlLogInLogOutUtilities
{

public:
	
	/*
		Funcion que verifica si se puede hacer log in para un usuario.
	*/
	static bool16 CanDoLogInUser();
	
	/*
		
	*/
	static bool16 DoLogInUser();
	
	/*
		Funcion que habre el dialogo para realizar el login.
	*/
	static bool16 DoOpenDialogLogInUser();
	
	/*
		Funcion que pregunta si existe un usuario logeado.
	*/
	static bool16 ExistsLoggedUsers();
	
	/*
		Fucnion que preguna si puede hacer logout del sisitema.
	*/
	static bool16 CanDoLogOutUser();
	
	/*
		Funcion que realiza el logout.
	*/
	static bool16 DoLogOutUser();
	
	static bool16 DoLogOutUserWhenShutDownApp();
	
	/*
		Funcion que muestra o oculta una paleta por medio del Id del Widget
	*/
	static bool16 ShowHidePaleteByWidgetID(WidgetID widget, bool16 show);
	
	static void SetUserNameOnApp(PMString pmUserName);
	
	private:
	/*
	*/
	static PMString FechaYHora();
	
	
	static bool16 InsertEnMovimientos(K2Vector<PMString>& QueryVector);
	
	static IControlView * GetAssociatedWidget(WidgetID widgetID);
	
	
	
	
};

#endif // __PnlTrvUtils_H_DEFINED__

