//========================================================================================
//  
//  $File: $
//  
//  Owner: Fernando Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

#include "ILayoutUIUtils.h"
#include "ICheckInOutSuite.h"
#include "IDocument.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
#include "ICheckInOutSuite.h"

// Project includes:
#include "N2PCheckInOutID.h"

#ifdef WINDOWS
	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
#endif

#ifdef MACINTOSH
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
#endif




/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

*/
class N2PkCInOutActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		N2PkCInOutActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);

	private:
		/** Pops this plug-in's about box. */
		void DoAbout();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PkCInOutActionComponent, kN2PkCInOutActionComponentImpl)

/* N2PkCInOutActionComponent Constructor
*/
N2PkCInOutActionComponent::N2PkCInOutActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void N2PkCInOutActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kCheckInOutAboutActionID:
		{
			this->DoAbout();
			break;
		}	

		case kCheckInOutDialogActionID:
		{
			
			ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
			checkIn->CheckIn();
			break;
		}

		case kCheckOutDialogActionID:
		{
			ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
			checkIn->CheckOut();
			break;

		}

		case kGuardaRevDialogActionID:
		{
			do
			{
				
				PMString NameDocumentActual="";
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
				
			
				if(UpdateStorys==nil)
				{
					break;
				}
		
				IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
				if(document==nil)
				{
					//ASSERT_FAIL(kN2PInOutNoDocumentoAbiertoStringKey);
					break;
				}
		
				//Verifica que no sea un documento nuevo
					document->GetName(NameDocumentActual);
			
		
				//Para Gurdar Revision
					ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
					(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
					if(checkIn==nil)
						break;
			
		
		
					if(NameDocumentActual.Contains("Untitled") || NameDocumentActual.Contains("Sin t�tulo-") || NameDocumentActual.Contains(".indt"))
					{
						
						checkIn->SaveRevi(kTrue);
						break;
					}
			
			
				///Obtiene las Ultimas Preferencias
					PrefParamDialogInOutPage PreferencesPara;
			
					checkIn->GetPreferencesParametrosOfDB(&PreferencesPara);
					//checkIn->GetPreferencesParametrosOfFile(&PreferencesPara);
		
				//Hay que validar que ya se ha realizadon un chech in o save revition
				// y que la pagina se llame igual a nombre de pagina actual
					
					if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars() == 0)
					{
						//si no se ha realizado un previo save as o  save se habre el diago save as 
						CAlert::InformationAlert("si no se ha realizado un previo save as o  save se habre el diago save as "	);
						checkIn->SaveRevi(kTrue);
						break;
					}
			
					PMString NameDocUltimoSaveAs=PreferencesPara.Pagina_To_SavePage;
					NameDocUltimoSaveAs.Append(".indd");
		
				///Para ver si el documento de enfrente se llama igual que el ultimo salvado
				/*	if(NameDocumentActual!=NameDocUltimoSaveAs)
					{
						checkIn->SaveRevi(kTrue);
						break;
					}
				*/
					
					checkIn->SaveRevi(kFalse);
			}while(false);
			//ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			//(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			//if(checkIn==nil)
			//	break;
			
			//	checkIn->SaveRevi(kFalse);
			
			break;
		}
			
		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void N2PkCInOutActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kN2PkCInOutAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}

//  Code generated by DollyXS code generator
