/*
//	File:	GuardarRevDialogObserver.cpp
//
//	Date:	28-Jan-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2004 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ITextControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ILayoutUIUtils.h"
#include "IListBoxController.h"

// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "CAlert.h"
#include "N2PCheckInOutListBoxHelper.h"
#include "IWidgetParent.h"

// Project includes:
#include "N2PCheckInOutID.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif


/** GuardarRevDialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author Juan Fernando Llanas Rdz
*/
class GuardarRevDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		GuardarRevDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~GuardarRevDialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		
	private :
	void LLenar_Combo_Seccion();
	bool16 agregarIssuesAndSection();
	bool16 GetTextOfComboOnDialog(	const WidgetID& Combowidget, PMString& itemToShow, int32& indexSelected);
	bool16 LlenarListaDeIssueDePagina();
	
	void removeIssueDePagina();
	bool16 ObtenerFolioDePaginaSeleccionada(PMString& Id_PaginaSel, PMString& Seccion);


};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(GuardarRevDialogObserver, kGuardaRevDialogObserverImpl)

/* AutoAttach
*/
void GuardarRevDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("GuardarRevDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to MiPrimerDialogo's info button widget.
		AttachToWidget(kComboBoxIssueWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		AttachToWidget(kAddButtonWidgetID    , IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kN2PCkRemoveButtonWidgetID    , IID_IBOOLEANCONTROLDATA, panelControlData);
		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void GuardarRevDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("GuardarRevDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from MiPrimerDialogo's info button widget.
		DetachFromWidget(kComboBoxIssueWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		DetachFromWidget(kAddButtonWidgetID    , IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kN2PCkRemoveButtonWidgetID    , IID_IBOOLEANCONTROLDATA, panelControlData);
		
		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void GuardarRevDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("GuardarRevDialogObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if (theSelectedWidget == kComboBoxIssueWidgetID && theChange == kPopupChangeStateMessage)
		{
			this->LLenar_Combo_Seccion();
		}
		
		if (theSelectedWidget == kAddButtonWidgetID && theChange == kTrueStateMessage)
		{
			agregarIssuesAndSection();
		}
		
		if (theSelectedWidget == kN2PCkRemoveButtonWidgetID && theChange == kTrueStateMessage)
		{
			removeIssueDePagina();
		}


	} while (false);
}
//  Generated by Dolly build 17: template "Dialog".
// End, GuardarRevDialogObserver.cpp.


void GuardarRevDialogObserver::LLenar_Combo_Seccion()
{
	do
	{
			
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			ASSERT_FAIL(" GuardarRevDialogObserver::LLenar_Combo_Seccion No pudo Obtener panelControlData*");
			break;
		}
		
		IControlView * ComboCViewPubli=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if (ComboCViewPubli == nil)
		{
			ASSERT_FAIL("GuardarRevDialogObserver::LLenar_Combo_Seccion No pudo Obtener ComboCViewPubli*");
			break;
		}
		
		InterfacePtr<ITextControlData> TextControlPubli(ComboCViewPubli,ITextControlData::kDefaultIID);
		if (TextControlPubli == nil)
		{
			ASSERT_FAIL("GuardarRevDialogObserver::LLenar_Combo_SeccionNo pudo Obtener TextControlPubli*");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("GuardarRevDialogObserver::LLenar_Combo_SeccionNo pudo Obtener ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("GuardarRevDialogObserver::LLenar_Combo_SeccionNo pudo Obtener dropListData");
			break;
		}
		dropListData->Clear(kTrue);

		PMString Busqueda="SELECT Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion=(SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + TextControlPubli->GetString() + "') ORDER BY Nombre_de_Seccion ASC";
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("GuardarRevDialogObserver::LLenar_Combo_SeccionNo pudo Obtener IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}		
		
		
		PreferencesConnection PrefConections;
		//PMString DSNName= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}

		
		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

	}while(false);
}


bool16 GuardarRevDialogObserver::agregarIssuesAndSection()
{
	do{
		
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		
		
		PMString IDUsuario_Actual="";
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in CheckInOutAsignaIssueObserver::Update()");
			break;
		}
		IDUsuario_Actual= wrkSpcPrefs->GetIDUserLogedString();
		
		
		PMString Issue="";
		PMString Seccion="";
		int32 indexSelected=0;
		
		GetTextOfComboOnDialog(	kComboBoxIssueWidgetID, Issue, indexSelected);
		if(Issue.NumUTF16TextChars()<=0)
			break;
		GetTextOfComboOnDialog(	kComboBoxSeccionWidgetID, Seccion, indexSelected);
		
		
		////////////////////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		//PMString DSNName= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		
		PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		if( IDPaginaENXMP.NumUTF16TextChars() > 0 )
		{
			PMString SPString("CALL SP_addIssueAndSecctionToPage('"+Issue+"','"+Seccion+"',"+IDPaginaENXMP+",'"+IDUsuario_Actual+"');");
			K2Vector<PMString> QueryVector;
		
			PMString numerror="";
			PMString stringOUT="";
			PMString idsalida="";
		
			if(SQLInterface->SQLQueryDataBase(StringConection,SPString,QueryVector))
			{
			
				for(int32 i=0;i<QueryVector.Length();i++)
				{
					idsalida=SQLInterface->ReturnItemContentbyNameColumn("idsalida",QueryVector[i]);
					stringOUT=SQLInterface->ReturnItemContentbyNameColumn("stringOUT",QueryVector[i]);
					idsalida=SQLInterface->ReturnItemContentbyNameColumn("idsalida",QueryVector[i]);
				}
			}
		
			if(numerror.GetAsNumber()>0)
			{
				CAlert::InformationAlert(stringOUT);
			}
			else
			{
				LlenarListaDeIssueDePagina();
			}
		}
		else 
		{
			//Agregar solo a la lista
			InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
			if (panel == nil)
			{
				
				break;
			}	
			IControlView *iControViewList = panel->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID);
			if(iControViewList==nil)
			{
				CAlert::InformationAlert("ControlView");
				break;
			}
			
			SDKListBoxHelper listHelper(iControViewList, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
			
			PMString Estado="";
			PMString Folio_Pagina="0";
			PMString Id_SeccionText="";
			PMString Id_PublicacionText="";
			listHelper.AddElementListPag(Issue,Seccion,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
		}
	}while (false);
	return kFalse;
}


bool16 GuardarRevDialogObserver::GetTextOfComboOnDialog(	const WidgetID& Combowidget, PMString& itemToShow, int32& indexSelected)
{
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if (panel == nil)
		{
			
			break;
		}		
		
		
		IControlView *ControlView = panel->FindWidget(Combowidget);
		if(ControlView==nil)
		{
			CAlert::InformationAlert("ControlView");
			break;
		}
		
		//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
		InterfacePtr<IStringListControlData> iStringListControlData(ControlView,IID_ISTRINGLISTCONTROLDATA);
		if (iStringListControlData == nil)
		{
			CAlert::InformationAlert("dropListData");
			
			break;
		}
		
		InterfacePtr<IDropDownListController>	iDropDownListController( ControlView, IID_IDROPDOWNLISTCONTROLLER);
		if(iDropDownListController==nil)
		{
			CAlert::InformationAlert("CheckInOutAsignaIssueObserver::LLenar_Combo_Seccion IDDLDrComboBoxSelecPrefer");
			break;
		}
		
		indexSelected=iDropDownListController->GetSelected();
		
		itemToShow= iStringListControlData->GetString(indexSelected);		
		
		retval=kTrue;
		
	}while(false);
	return(retval);
}


bool16 GuardarRevDialogObserver::LlenarListaDeIssueDePagina()
{
	do{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if (panel == nil)
		{
			
			break;
		}		
		
		
		IControlView *iControViewList = panel->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		if(iControViewList==nil)
		{
			CAlert::InformationAlert("ControlView");
			break;
		}
		
		
		
		
		SDKListBoxHelper listHelper(iControViewList, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		listHelper.EmptyCurrentListBox();	
		
		
		PMString Busqueda="SELECT a.id as id, p.Nombre_Publicacion as publicacion, s.Nombre_de_Seccion as seccion ";
		Busqueda.Append(	  "	FROM edicionesdpagina a LEFT JOIN publicacion p ON p.Id_Publicacion=publicacion_idpublicacion");
		Busqueda.Append(	  " LEFT JOIN seccion s ON s.Id_Seccion=a.seccion_id_seccion WHERE pagina_id="+IDPaginaENXMP+ " AND a.estatus<90 ");
		
		//CAlert::InformationAlert(Busqueda);
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
		K2Vector<PMString> QueryVector;
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
		if(SQLInterface==nil)
		{
			ASSERT_FAIL("SQLInterface = nil");
			break;
		}
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		if(QueryVector.Length()<=0)
		{
			PMString NoNotas=PMString(kN2PCheckInOutNoSeEncontraronPaginasPlugInMenuKey);
			NoNotas.Translate();
			NoNotas.SetTranslatable(kTrue);
			CAlert::InformationAlert(NoNotas);
			break;
		}
		
		// cuando regresa solo un registro
		if(QueryVector.Length()==1)
		{
			
			//CAlert::InformationAlert(QueryVector[0]);
			PMString Privilegio = SQLInterface->ReturnItemContentbyNameColumn("Privilegio",QueryVector[0]);
			if(Privilegio.NumUTF16TextChars()>0)//Pregunta el rtegitro trae el campo con el nombre 'Privilegio'
			{
				//entonces no tiene privilegios para ver las paginas de esta seccion
				CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
			}
			else
			{// si no entonces es una regirtro con datos de una pagina
				for(int32 i=0;i<QueryVector.Length();i++)
				{
					PMString Nombre_Archivo = SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryVector[i]);
					PMString Secc = SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryVector[i]);
					PMString Estado="";// SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
					PMString Folio_Pagina = SQLInterface->ReturnItemContentbyNameColumn("id",QueryVector[i]);
					PMString Id_SeccionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
					PMString Id_PublicacionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
					listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
				}
			}
		}
		else
		{//si no entonces trae muchas registeros de paginas.
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				PMString Nombre_Archivo=SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryVector[i]);
				PMString Secc=SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryVector[i]);
				PMString Estado="";//SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
				PMString Folio_Pagina=SQLInterface->ReturnItemContentbyNameColumn("id",QueryVector[i]);
				PMString Id_SeccionText="";//SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
				PMString Id_PublicacionText="";//SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
				listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
			}
		}
	}while (false);
return kTrue;
}


void GuardarRevDialogObserver::removeIssueDePagina()
{
	do{
		PMString Seccion="";
		PMString Id_PaginaSel="";
		
		if(this->ObtenerFolioDePaginaSeleccionada( Id_PaginaSel, Seccion)==kFalse)
		{
			break;
		}
		
		if(Id_PaginaSel.NumUTF16TextChars()<=0)
		{
			break;
		}
		
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
			
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			break;
		}
			
		InterfacePtr<IControlView>		ListCView( panel->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID), UseDefaultIID() );
		if(ListCView==nil)
		{
			ASSERT_FAIL("No se encontro el editbox");
			break;
		}
			
		//se declara crea una lista 
		SDKListBoxHelper listBox(ListCView, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		
		if(listBox.GetElementCount()>1)
		{
			if(Id_PaginaSel=="0")
			{
			
				//obtencion del controladsor de la lista
				InterfacePtr<IListBoxController> listCntl(ListCView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
				ASSERT_MSG(listCntl != nil, "listCntl nil");
				if(listCntl == nil) 
				{
					break;
				}
				//  Get the first item in the selection (should be only one if the list box
				// has been set up correctly in the framework resource file)
				int32 indexSelected;
				K2Vector<int32> multipleSelection ;
				listCntl->GetSelected( multipleSelection ) ;//obtencion de la selecciones sobre la lista
				const int kSelectionLength =  multipleSelection.Length() ;//tamaÒo de la seleccion
				if(listBox.GetElementCount()>=kSelectionLength){
					CAlert::InformationAlert(kN2PCheckInOutSelectMoreThanOneStrKey);
				}
				else {
					if (kSelectionLength> 0 )//si la cantidad de campos seleccionados es nmayor a 0
					{
						PMString dbgInfoString("Selected item(s): ");
						for(int i=0; i < multipleSelection.Length() ; i++) 
						{
							indexSelected = multipleSelection[i];
							dbgInfoString.AppendNumber(indexSelected);
							if(i < kSelectionLength - 1)
							{
								dbgInfoString += ", ";
							}
						}
						
						listBox.RemoveElementAt(indexSelected);
					}
				}				
			}
			else 
			{
				PMString IDUsuario_Actual="";
		
				InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
				if (wrkSpcPrefs == nil)
				{	
					CAlert::InformationAlert("Invalid workspace prefs in CheckInOutAsignaIssueObserver::Update()");
					break;
				}
				IDUsuario_Actual= wrkSpcPrefs->GetIDUserLogedString();
		
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
				if(UpdateStorys==nil)
				{
					CAlert::InformationAlert("Error no puede hacer conexion");
					break;
				}
		
				PreferencesConnection PrefConections;
				if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
				{
					CAlert::InformationAlert("Error no encontro preferencias de conexion");
					break;
				}
		
				//Arma Cadena de coneccion DSN
				PMString StringConection="";
		
				StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		
		
				//Arma Cadena Query para Basee de datos
				PMString Consulta="UPDATE edicionesdpagina SET estatus=90, fmod=NOW(), umod='"+IDUsuario_Actual+"' WHERE ID=";
				Consulta.Append(Id_PaginaSel);
		
		
				K2Vector<PMString> QueryVectorUsario;
		
		
				//Realiza Query
				if(SQLInterface->SQLSetInDataBase(StringConection,Consulta))		
					LlenarListaDeIssueDePagina();
			}
		}
		else {
			CAlert::InformationAlert(kN2PCheckInOutSelectMoreThanOneStrKey);
		}

	}while(false);
}

bool16 GuardarRevDialogObserver::ObtenerFolioDePaginaSeleccionada(PMString& Id_PaginaSel, PMString& Seccion)
{
	bool16 retval=kFalse;
	
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
		
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			break;
		}
		
		InterfacePtr<IControlView>		ListCView( panel->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID), UseDefaultIID() );
		if(ListCView==nil)
		{
			ASSERT_FAIL("No se encontro el editbox");
			break;
		}
		
		//se declara crea una lista 
		SDKListBoxHelper listBox(ListCView, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		//obtencion del controladsor de la lista
		InterfacePtr<IListBoxController> listCntl(ListCView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
		ASSERT_MSG(listCntl != nil, "listCntl nil");
		if(listCntl == nil) 
		{
			break;
		}
		//  Get the first item in the selection (should be only one if the list box
		// has been set up correctly in the framework resource file)
		int32 indexSelected;
		K2Vector<int32> multipleSelection ;
		listCntl->GetSelected( multipleSelection ) ;//obtencion de la selecciones sobre la lista
		const int kSelectionLength =  multipleSelection.Length() ;//tamaÒo de la seleccion
		
		if(kSelectionLength >= listBox.GetElementCount()){
			CAlert::InformationAlert(kN2PCheckInOutSelectMoreThanOneStrKey);
		}
		else {
			if (kSelectionLength> 0 )//si la cantidad de campos seleccionados es nmayor a 0
			{
				PMString dbgInfoString("Selected item(s): ");
				for(int i=0; i < multipleSelection.Length() ; i++) 
				{
					indexSelected = multipleSelection[i];
					dbgInfoString.AppendNumber(indexSelected);
					if(i < kSelectionLength - 1)
					{
						dbgInfoString += ", ";
					}
				}
				
				listBox.ObtenerTextoDeSeleccionActual(kFolio_PaginaLabelWidgetID,Id_PaginaSel,kLabelId_SeccionLabelWidgetID,Seccion,indexSelected);
				retval=kTrue;
			}
		}

		
	}while(false);
	
	return(retval);
}


