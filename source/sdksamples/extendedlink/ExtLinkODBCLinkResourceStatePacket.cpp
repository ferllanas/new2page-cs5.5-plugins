//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkODBCLinkResourceStatePacket.cpp $
//  
//  Owner: Michael Easter
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "ExtLinkODBCLinkResourceStatePacket.h"


//========================================================================================
//
// ExtLinkODBCLinkResourceStatePacket Implementation
//
//========================================================================================

//========================================================================================
// Constructor
//========================================================================================
ExtLinkODBCLinkResourceStatePacket::ExtLinkODBCLinkResourceStatePacket(ILinkManager::OperationType opType,
												 bool bNotify,
												 IDataBase* db,
												 const UID& uid,
												 const URI& uri,
												 ILinkResource::ResourceState state,
												 const WideString& stamp)
: fOpType(opType),
  fbNotify(bNotify),
  fDB(db),
  fUID(uid),
  fURI(uri),
  fState(state),
  fStamp(stamp),
  fURIChanged(false),
  fStateChanged(false),
  fStampChanged(false)
{
}

//========================================================================================
// Destructor
//========================================================================================
ExtLinkODBCLinkResourceStatePacket::~ExtLinkODBCLinkResourceStatePacket()
{
}

//========================================================================================
// ExtLinkODBCLinkResourceStatePacket::operator==
//========================================================================================
bool ExtLinkODBCLinkResourceStatePacket::operator==(const ExtLinkODBCLinkResourceStatePacket& rhs) const
{
	if (&rhs == this) {
		return true;
	}

	// only check the database and UID of the link resource,
	// since all the work packets are keyed by the database
	// and UID only
	return (fDB == rhs.fDB && fUID == rhs.fUID);
}

//========================================================================================
// ExtLinkODBCLinkResourceStatePacket::SetURI
//========================================================================================
void ExtLinkODBCLinkResourceStatePacket::SetURI(const URI& uri)
{
	if (fURI != uri) {
		fURI = uri;
		fURIChanged = true;
	}
}

//========================================================================================
// ExtLinkODBCLinkResourceStatePacket::SetState
//========================================================================================
void ExtLinkODBCLinkResourceStatePacket::SetState(ILinkResource::ResourceState state)
{
	if (fState != state) {
		fState = state;
		fStateChanged = true;
	}
}

//========================================================================================
// ExtLinkODBCLinkResourceStatePacket::SetStamp
//========================================================================================
void ExtLinkODBCLinkResourceStatePacket::SetStamp(const WideString& stamp)
{
	if (fStamp != stamp) {
		PMString oldS(fStamp);
		PMString newS(stamp);
		TRACEFLOW("ExtLink", "old stamp=%s, new stamp=%s", oldS.GetPlatformString().c_str(), newS.GetPlatformString().c_str());
		fStamp = stamp;
		fStampChanged = true;
	}
}
