/*
//	File:	N2PsqlDocWchResponder.cpp
//
//	Date:	6-Mar-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IClassIDData.h"
#include "ISignalMgr.h"
#include "IDocumentSignalData.h"
#include "IUIDData.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IObserver.h"

// Implementation includes:
#include "CreateObject.h"
#include "CResponder.h"
#include "DocWchUtils.h"
#include "IPalettePanelUtils.h"
#include "CAlert.h"

#include "N2PsqlID.h"
#include "UpdateStorysAndDBUtilis.h"
#include "N2PSQLUtilities.h"
#include "N2PSQLListBoxHelper.h"


/** N2PsqlDocWchResponder
	Handles signals related to document file actions.  The file
	action signals it receives are dictated by the N2PsqlDocWchServiceProvider
	class.

	N2PsqlDocWchResponder implements IResponder based on
	the partial implementation CResponder.


	@author John Hake
*/
class N2PsqlDocWchResponder : public CResponder
{
	public:
	
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PsqlDocWchResponder(IPMUnknown *boss);

		/**
			Respond() handles the file action signals when they
			are dispatched by the signal manager.  This implementation
			simply creates alerts to display each signal.

			@param signalMgr Pointer back to the signal manager to get
			additional information about the signal.
		*/
		virtual void Respond(ISignalMgr* signalMgr);
private:
	IControlView * GetAssociatedWidget(WidgetID widgetID);

};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PsqlDocWchResponder, kN2PsqlDocWchResponderImpl)

/* DocWchActionComponent Constructor
*/
N2PsqlDocWchResponder::N2PsqlDocWchResponder(IPMUnknown* boss) :
	CResponder(boss)
{
}

/* Respond
*/
void N2PsqlDocWchResponder::Respond(ISignalMgr* signalMgr)
{

	// Exit if the responder should do nothing
	if (DocWchUtils::QueryDocResponderMode() != kTrue)
	{
		return;
	}

	// Get the service ID from the signal manager
	ServiceID serviceTrigger = signalMgr->GetServiceID();

	// Get a UIDRef for the document.  It will be an invalid UIDRef
	// for BeforeNewDoc, BeforeOpenDoc, AfterSaveACopy, and AfterCloseDoc because the
	// document doesn't exist at that point.
	InterfacePtr<IDocumentSignalData> docData(signalMgr, UseDefaultIID());
	if (docData == nil)
	{
		ASSERT_FAIL("Invalid IDocumentSignalData* - N2PsqlDocWchResponder::Respond");
		return;
	}
	UIDRef doc = docData->GetDocument();

	//IControlView* iControViewList=GetAssociatedWidget(kN2PsqlListUpdateWidgetID);
	//N2PSQLListBoxHelper listUpdateHelper(iControViewList, kN2PSQLPluginID,kN2PsqlListUpdateWidgetID);

	// Take action based on the service ID
	switch (serviceTrigger.Get())
	{
		case kBeforeNewDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kBeforeNewDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			break;
		}
		case kDuringNewDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kDuringNewDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kAfterNewDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kAfterNewDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			InterfacePtr<IDocumentSignalData> data(signalMgr, UseDefaultIID());
				if (data == nil)
				{
					break;
				}

				InterfacePtr<IDocument> iDocument(data->GetDocument(), UseDefaultIID());
				if (iDocument == nil)
				{
					ASSERT_FAIL("GTTxtEdtResponder::Respond - document nil in responder?");
					break;
				}

				// attach the new/delete story observer
				InterfacePtr<IObserver> iNewDeleteStoryObserver(iDocument,IID_IGTTXTEDTSTORYCREATEDELETEOBSERVER);
				if (iNewDeleteStoryObserver == nil){
					ASSERT_FAIL("GTTxtEdtResponder::Respond - document has no story create/delete observer in responder?");
					break;
				}
				iNewDeleteStoryObserver->AutoAttach();

			break;
		}
		case kBeforeOpenDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kBeforeOpenDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kDuringOpenDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kDuringOpenDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kAfterOpenDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kAfterOpenDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			InterfacePtr<IDocumentSignalData> data(signalMgr, UseDefaultIID());
				if (data == nil)
				{
					break;
				}

				InterfacePtr<IDocument> iDocument(data->GetDocument(), UseDefaultIID());
				if (iDocument == nil)
				{
					ASSERT_FAIL("GTTxtEdtResponder::Respond - document nil in responder?");
					break;
				}

				// attach the new/delete story observer
				InterfacePtr<IObserver> iNewDeleteStoryObserver(iDocument,IID_IGTTXTEDTSTORYCREATEDELETEOBSERVER);
				if (iNewDeleteStoryObserver == nil){
					ASSERT_FAIL("GTTxtEdtResponder::Respond - document has no story create/delete observer in responder?");
					break;
				}
				iNewDeleteStoryObserver->AutoAttach();
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			
			PreferencesConnection PrefConections;
			if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
			
			
			//if(PrefConections.N2PUtilizarGuias==1)
			//{
			//	PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", iDocument);
			//	if(Id_Seccion.NumUTF16TextChars()>0 )
			//		N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
			//}
			
			if(PrefConections.N2PUtilizarGuias==1)
			{
				//CAlert::InformationAlert("N2PsqlDocWchResponder::Respond kAfterOpenDocSignalResponderService");
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kTrue);
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kFalse);
				PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", iDocument);
				if(Id_Seccion.NumUTF16TextChars()>0 )
					N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
				
			}
			else
			{
				//CAlert::InformationAlert("BBB");
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kFalse );
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kTrue );
				
			}
			break;
		}
		case kBeforeSaveDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kBeforeSaveDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kAfterSaveDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kAfterSaveDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kBeforeSaveAsDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kBeforeSaveAsDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kAfterSaveAsDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kAfterSaveAsDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kBeforeSaveACopyDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kBeforeSaveACopyDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kDuringSaveACopyDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kDuringSaveACopyDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kAfterSaveACopyDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kAfterSaveACopyDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kBeforeRevertDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kBeforeRevertDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			break;
		}
		case kAfterRevertDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kAfterRevertDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}
		case kBeforeCloseDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kBeforeCloseDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			break;
		}
		case kAfterCloseDocSignalResponderService:
		{
			//DocWchUtils::DwAlert(doc, "kAfterCloseDocSignalStringKey");
			//listUpdateHelper.EmptyCurrentListBox();
			
			break;
		}

		default:
			break;
	}

}

/*

*/
IControlView * N2PsqlDocWchResponder::GetAssociatedWidget(WidgetID widgetID)
{
	IControlView * listControlView=nil;
	do{
		InterfacePtr<IPanelControlData> panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID));	
		ASSERT_MSG( panelData !=nil, "panelData nil");
		if(panelData==nil)
		{
			
			break;
		}
		listControlView = panelData->FindWidget(widgetID);
		ASSERT_MSG( listControlView != nil, "listControlView nil");
		if(listControlView == nil) {
			
			break;
		}
	} while(0);
	
	return listControlView;
}

// End, N2PsqlDocWchResponder.cpp.


