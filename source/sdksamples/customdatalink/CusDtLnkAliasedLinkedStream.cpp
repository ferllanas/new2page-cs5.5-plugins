//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/customdatalink/CusDtLnkAliasedLinkedStream.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ICusDtLnkFacade.h"
#include "IDatalink.h"
#include "ILinkedStream.h"
#include "IWorkspace.h"
#include "IUIDData.h"

// Impl includes:
#include "CPMUnknown.h"
#include "NAMEINFO.H"
#include "SDKFileHelper.h"
#include "StreamUtil.h"
#include "Utils.h"


// Proj includes:
#include "CusDtLnkID.h"


/** Implements ILinkedStream
	@ingroup customdatalink

*/
class CusDtLnkAliasedLinkedStream : public CPMUnknown<ILinkedStream>
{
	public:
		/** Constructor 
			@param boss object on which this interface aggregated
		*/
		CusDtLnkAliasedLinkedStream(IPMUnknown* boss);
		/** Destructor */
		virtual ~CusDtLnkAliasedLinkedStream();


		/**	Create stream from data stored in this datalink
			@return IPMStream which is nil if we couldn't create
				one based on our current state
		 */
		virtual IPMStream *CreateReadStream();

private:


};

CREATE_PMINTERFACE(CusDtLnkAliasedLinkedStream, kCusDtLnkAliasedLinkedStreamImpl)


/* 
*/
CusDtLnkAliasedLinkedStream::CusDtLnkAliasedLinkedStream(IPMUnknown *boss) :
	CPMUnknown<ILinkedStream>(boss)
{
}

/*
*/
CusDtLnkAliasedLinkedStream::~CusDtLnkAliasedLinkedStream()
{
}

/*
*/
IPMStream* CusDtLnkAliasedLinkedStream::CreateReadStream()
{
	
	do {
		InterfacePtr<IDataLink> dataLink(this, UseDefaultIID());
		ASSERT(dataLink);
		if(!dataLink) {
			break;
		}
		
		Utils<ICusDtLnkFacade> facade;
		ASSERT(facade);
		if(!facade) {
			break;
		}
		PMString uniqueKey = facade->RetrieveUniqueKey(dataLink);

		UIDRef  wsRef;
		IDataBase* db = ::GetDataBase(this);
		//The database may be NIL if the datalink is temporary created)
		if (db)
		{
			InterfacePtr<IDocument> doc(db, db->GetRootUID(), UseDefaultIID());
			wsRef = doc->GetDocWorkSpace();
		}
		else
		{
			InterfacePtr<IUIDData> iWorkspaceRef(this, IID_IUIDDATA);
			wsRef = iWorkspaceRef->GetRef();
		}
		InterfacePtr<IWorkspace> ws(wsRef, UseDefaultIID());

		IDFile idFile;
		idFile = facade->GetLocalFileFromAssetIdentifier(ws, uniqueKey);
		SDKFileHelper fileHelper(idFile);
		// Hmmm, we didn't resolve through to a local file
		if(!fileHelper.IsExisting()) {

			// OK, perhaps it is a real filename in the custom datalink?
			// This can happen when we're doing a RestoreLink initiated
			// through the Links Panel UI
			NameInfo nameInfo;
			uint32 filetype;
			PMString formatName;
			int32 errCode = dataLink->GetNameInfo(&nameInfo, &formatName, &filetype);
			ASSERT(errCode == 0);
			if(errCode != 0) {
				break;
			}
			ASSERT(nameInfo.GetFilename());
			if(!(nameInfo.GetFilename())) {
				break;
			}
			PMString realFilePath = *(nameInfo.GetFilename());
			SDKFileHelper realFileHelper(realFilePath);
			idFile = realFileHelper.GetIDFile();
		}

		// The existing linked stream implementation sets up information on the stream
		// (which has an IDataLink)
		// for the place providers. I'm just ignoring this as it wouldn't
		// necessarily make sense (it's a kDataLinkImpl).
		// See for instance kFileStreamReadBoss etc
		InterfacePtr<IPMStream> stream(StreamUtil::CreateFileStreamReadLazy(idFile));
		return stream.forget();
	
	} while(kFalse);
	return nil;
}


//  Code generated by DollyXS code generator
