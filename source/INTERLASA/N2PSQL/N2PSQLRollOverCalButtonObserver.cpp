//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/pictureicon/PicIcoSuiteButtonObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #6 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"
#include "IDialogController.h"
#include "ITextControlData.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IWidgetParent.h"

#include "IDropDownListController.h"
#include "IStringListControlData.h"

// Interface includes
#include "ISubject.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

// Implem includes

#include "CAlert.h"
#include "CObserver.h"
#include "N2PsqlID.h"


#ifdef MACINTOSH 
	#include "N2PCalID.h"
	#include "IN2PCalTableCellData.h"#endif
#endif

#ifdef WINDOWS
	#include "..\N2PCalendaro\N2PCalID.h"
	#include "..\N2PCalendaro\IN2PCalTableCellData.h"
#endif


/**
	Adding an IObserver interface to a widget boss class creates a button
	whose presses can be registered as Update messages to the client code. 

	@ingroup pictureicon
	
*/
class N2PSQLRollOverCalButtonObserver : public CObserver
{
public:
	/**
		Constructor 
	*/		
	N2PSQLRollOverCalButtonObserver(IPMUnknown *boss);
	/**
		Destructor
	*/
	~N2PSQLRollOverCalButtonObserver();
	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called when the 
		widget boss object is shown.
	*/		
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when the widget is hidden.
	*/		
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

	
};

CREATE_PMINTERFACE(N2PSQLRollOverCalButtonObserver, kN2PSQLRollOverCalButtonObserverImpl)


N2PSQLRollOverCalButtonObserver::N2PSQLRollOverCalButtonObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


N2PSQLRollOverCalButtonObserver::~N2PSQLRollOverCalButtonObserver()
{
	
}


void N2PSQLRollOverCalButtonObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ITRISTATECONTROLDATA);
	}
}


void N2PSQLRollOverCalButtonObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ITRISTATECONTROLDATA);
	}
}



void N2PSQLRollOverCalButtonObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if(theChange == kTrueStateMessage) 
	{
		// No behaviour in this sample: but indicate at least that the message has been received.
		// Dialog-specific resource includes:
		do
		{
		
			// Get the application interface and the DialogMgr.	
			InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
			ASSERT(application);
			if (application == nil) {	
				break;
			}
			InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
			ASSERT(dialogMgr);
			if (dialogMgr == nil) {
				break;
			}

			// Load the plug-in's resource.
			PMLocaleId nLocale = LocaleSetting::GetLocale();
			RsrcSpec dialogSpec
			(
				nLocale,					// Locale index from PMLocaleIDs.h. 
				kN2PCalPluginID,			// Our Plug-in ID  
				kViewRsrcType,				// This is the kViewRsrcType.
				kSDKDefDialogResourceID,	// Resource ID for our dialog.
				kTrue						// Initially visible.
			);

			// CreateNewDialog takes the dialogSpec created above, and also
			// the type of dialog being created (kMovableModal).
			IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
			ASSERT(dialog);
			if (dialog == nil) {
				break;
			}

			// Open the dialog.
			dialog->Open(); 
			
			IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
				
			InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
			if (dialogController == nil)
			{	
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
				break;
			}
		
			dialog->WaitForDialog();
			
			
			//////
			InterfacePtr<IWidgetParent>		myParent(CVDialogNuevaPref, UseDefaultIID());				
			if(myParent==nil)
			{
				ASSERT_FAIL("C2PFilePreferencesUtils::ApplyPreferences myParent");
				break;
			}
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
				ASSERT_FAIL("C2PFilePreferencesUtils::ApplyPreferences panel");
				break;
			}
			
			InterfacePtr<ITableCellData>	pTableCellData((ITableCellData*)myParent->QueryInterface(IID_IN2PCALTABLECELLDATA));
			if(pTableCellData == nil) 
			{
				ASSERT_FAIL("pTableCellData");
			}
		
			int32 row =pTableCellData->GetSelectRow();
			int32 column =pTableCellData->GetSelectColumn();
		
			if(row>=0 && column>=0)
			{
				InterfacePtr<IControlView>		editBoxView(panel->FindWidget(kN2PCalTableCellEditBoxWidgetID), UseDefaultIID());
				if(editBoxView == nil) break;
		
				InterfacePtr<ITextControlData>	selectedChar(editBoxView, UseDefaultIID());
				if(selectedChar == nil) break;
		
				PMString assetPathTitle(kN2PCalIdiomaStringkey);
				assetPathTitle.Translate();
				assetPathTitle.SetTranslatable(kTrue);
		
		
				PMString NumDiadeMes = pTableCellData->GetCellString(column, row);
				if(NumDiadeMes.NumUTF16TextChars()<2)
				{
					NumDiadeMes="0" + NumDiadeMes;
				}
				NumDiadeMes.SetTranslatable(kFalse);
			
				///Get AÒo
				IControlView* ComboCViewComboBoxAno = panel->FindWidget(kN2PCalComboBoxAnoWidgetID);
				ASSERT(ComboCViewComboBoxAno);
				if(ComboCViewComboBoxAno == nil)
				{
					ASSERT_FAIL("LlenarComboPreferenciasOnPanel ComboCView");
					break;
				}
		

				InterfacePtr<IStringListControlData> dropListDataComboBoxAno(ComboCViewComboBoxAno,IID_ISTRINGLISTCONTROLDATA);
				if (dropListDataComboBoxAno == nil)
				{
					ASSERT_FAIL("No pudo Obtener dropListDataComboBoxAno*");
					break;
				}
		
				InterfacePtr<IDropDownListController>	IDDLDrComboBoxAnoPrefer( ComboCViewComboBoxAno, IID_IDROPDOWNLISTCONTROLLER);
				if(IDDLDrComboBoxAnoPrefer==nil)
				{
					ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
					break;
				}
			
				PMString stringAno="";
				if(IDDLDrComboBoxAnoPrefer->GetSelected()>=0)
				{
					stringAno = dropListDataComboBoxAno->GetString(IDDLDrComboBoxAnoPrefer->GetSelected());
				}
				else
				{
					break;
				}
		
				//Get Mes
		
			
				IControlView* ComboCView = panel->FindWidget(kN2PCalComboBoxMesStatusWidgetID);
				ASSERT(ComboCView);
				if(ComboCView == nil)
				{
					ASSERT_FAIL("LlenarComboPreferenciasOnPanel ComboCView");
					break;
				}
		

				InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
				if (dropListData == nil)
				{
					ASSERT_FAIL("No pudo Obtener dropListData*");
					break;
				}
		
				InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
				if(IDDLDrComboBoxSelecPrefer==nil)
				{
					ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
					break;
				}
			
				PMString MesString ="";
				PMString nmnk="";
				if(IDDLDrComboBoxSelecPrefer->GetSelected()>=0)
					nmnk.AppendNumber(IDDLDrComboBoxSelecPrefer->GetSelected()+1);
				
				if(nmnk.NumUTF16TextChars()<2)
				{
					nmnk="0" + nmnk;
				}
				
				if(IDDLDrComboBoxSelecPrefer->GetSelected()>=0)
				{
					MesString = dropListData->GetString(IDDLDrComboBoxSelecPrefer->GetSelected());
					MesString.Translate();
					MesString.SetTranslatable(kTrue);
					
				}
				else
				{
					break;
				}
			
				PMString FechaWhitFormat="";
				FechaWhitFormat.Append(nmnk);
				FechaWhitFormat.Append("/");
				FechaWhitFormat.Append(NumDiadeMes);
				FechaWhitFormat.Append("/");
				FechaWhitFormat.Append(stringAno);
				///
			
				PMString StringFecha=dialogController->GetTextControlData(kN2PCalTableCellEditBoxWidgetID);
			
				//Si fue cancelada el Dialogo fecha
				if(StringFecha.NumUTF16TextChars()<=0)
				{
					break;
				}
			
			
				InterfacePtr<IWidgetParent> widgetParent( this, UseDefaultIID());
				if(widgetParent==nil)
				{
					break;
				}
			
				InterfacePtr<IPanelControlData>	iPanelControlData( (IPanelControlData*)widgetParent->QueryParentFor(IID_IPANELCONTROLDATA) );
				if(iPanelControlData==nil)
				{
					break;
				}
				
				IControlView* FechaControlView = iPanelControlData->FindWidget(kN2PsqlTextFechaWidgetID);
				ASSERT(FechaControlView);
				if(!FechaControlView)
				{
					break;
				}
				
				InterfacePtr<ITextControlData> TextFechaEditFechaControlView(FechaControlView, UseDefaultIID());
				if(TextFechaEditFechaControlView==nil)
				{
					break;
				}
			
				//Coloca la fecha seleccionada
				TextFechaEditFechaControlView->SetString(FechaWhitFormat);
			}
			
			
		} while (false);	
	}	
}