//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkPointerXferBytes.h $
//  
//  Owner: jargast
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#pragma once
#ifndef __ExtLinkPointerXferBytes__
#define __ExtLinkPointerXferBytes__

#include "IXferBytes.h"

/**
	ExtLink implements a pointer based stream class, kExtLinkPointerStreamWriteBoss, which allows
	ExtLink to move pointer base buffer.  ExtLinkPointerXferBytes is the underlying streaming
	protocol.
	
	@ingroup extendedlink
	@see	kExtLinkPointerStreamWriteBoss
	@see	ExtLinkPointerStreamWrite
*/
class ExtLinkPointerXferBytes : public IXferBytes
{
public:
	ExtLinkPointerXferBytes();

	virtual ~ExtLinkPointerXferBytes();
	
	/**
		Set up the stream buffer for transfer
		@param memory IN pointer to tbe memory buffer to be transferred.
		@param memsize IN the size of the memory buffer.
		@return kTrue.
	*/
	virtual bool16 Open(void* memory, uint32 memsize);

	/**
		reset the data member of this class
		@return kTrue.
	*/
	virtual bool16 Close();
	
	/**
		Write number of bytes of data from the class's stream buffer to the caller's return buffer
		@param memory OUT pointer to tbe caller's buffer to store the requested number of bytes of data from the class's memory buffer.
		@param memsize IN the size of the bytes to transfer.
		@return uint32 number of bytes actually read.
		@see IXferBytes::Read
	*/
	virtual uint32 Read(void *buffer, uint32 num) ; 

	
	/**
		Transfer number of bytes of data from the caller's buffer to the stream at the current location
		@param memory OUT pointer to tbe caller's buffer that contains the data to write
		@param memsize IN the size of the bytes to transfer.
		@return uint32 number of bytes actually write.
		@see IXferBytes::Write
	*/
	virtual uint32 Write(void *buffer, uint32 num) ; 

	/**
		@see IXferBytes::Seek
	*/
	virtual uint32 Seek(int32 numberOfBytes, SeekFromWhere fromHere) ; 
	/**
		@see IXferBytes::SetEndOfStream
	*/
	virtual void SetEndOfStream();

	/**
		@see IXferBytes::Flush
	*/
	virtual void Flush() ; 

	/**
		@see IXferBytes::GetStreamState
	*/
	virtual StreamState GetStreamState() ; 

private:
	char*			fMemory;
	uint32			fLocation;
	uint32			fSize;
	StreamState		fStreamState;
};


#endif
