//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkODBCXMLElementLinkObject.cpp $
//  
//  Owner: Rich Gartland
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "CLinkObject.h"

#include "IIDXMLElement.h"
#include "ILinkManager.h"
#include "ILinkResource.h"
#include "IXMLReferenceData.h"
#include "IXMLUtils.h"
#include "Utils.h"
#include "URI.h"
#include "IExtLinkFacade.h"
#include "ExtLinkUtils.h"

#include "ExtLinkID.h"

//========================================================================================
// Class ExtLinkODBCXMLElementLinkObject
//========================================================================================
/**
	An indirect link object that points to an XML element.  XML element is not persistent, so
	we can't directly use it as a link object.  Instead, we create a reference object
	kExtLinkODBCXMLLinkObjectReferenceBoss, which is what link manager sees.
	
	The link object specifis which service provider should be used for import/export purpose,
	it also provide resolve policy, which establishes when there is a conflict on the link's 
	object and resource modification state, what to do to resolve the conflict.

	@ingroup extendedlink
	@see ExtLinkODBCUpdateLinkService
*/
class ExtLinkODBCXMLElementLinkObject : public CLinkObject
{
public:
	typedef CLinkObject inherited;
	typedef object_type data_type;

	ExtLinkODBCXMLElementLinkObject(IPMUnknown* boss);
	virtual ~ExtLinkODBCXMLElementLinkObject();
	
	/**
		@see ILinkObject::IsDirectLink
	*/
	virtual bool IsDirectLink() const { return false; }
	
	/**
		@see ILinkObject::QueryLinkedObject
	*/
	virtual IPMUnknown* QueryLinkedObject(const PMIID& interfaceId) const;

	
	/**
		@see ILinkObject::CanGotoLinkedObject
	*/
	virtual bool CanGotoLinkedObject() const { return false; }
	
	/**
		@see ILinkObject::GotoLinkedObject
	*/
	virtual ErrorCode GotoLinkedObject(PMString* errorString) const { return kSuccess; }

	
	/**
		@see ILinkObject::GetImportProvider
	*/
	virtual ClassID GetImportProvider(const ILinkResource* iResource, UID linkUID) const { return kExtLinkODBCUpdateLinkServiceProviderBoss; }
	
	/**
		@see ILinkObject::Import
	*/
	virtual ErrorCode Import(const ILinkResource* iResource, UID& linkUID, UIFlags uiFlags);

	
	/**
		@see ILinkObject::GetExportProvider
	*/
	virtual ClassID GetExportProvider(const ILinkResource* iResource, UID linkUID) const { return kExtLinkODBCUpdateLinkServiceProviderBoss; }
	
	/**
		@see ILinkObject::Export
	*/
	virtual ErrorCode Export(const ILinkResource* iResource, UID& linkUID, UIFlags uiFlags) const;

	
	/**
		@see ILinkObject::GetResolveProvider
	*/
	virtual ClassID GetResolveProvider(const ILinkResource* iResource, UID linkUID) const { return kInvalidClass; }
	
	/**
		ExtLink's Resolve policy is simple for demo purpose, we always let import win, i.e., when both link resource and link object are
		modified, and the link needs an update, we always do an import from link resource to link object.
		@see ILinkObject::Resolve
	*/
	virtual ErrorCode Resolve(const ILinkResource* iResource, UID& linkUID, UIFlags uiFlags);

private:
	// Prevent copy construction and assignment.
	ExtLinkODBCXMLElementLinkObject(const ExtLinkODBCXMLElementLinkObject&);
	ExtLinkODBCXMLElementLinkObject& operator=(const ExtLinkODBCXMLElementLinkObject&);
};

// read/write supplied by CLinkObject, used to determine links for copy/paste
CREATE_PERSIST_PMINTERFACE(ExtLinkODBCXMLElementLinkObject, kExtLinkODBCXMLElementLinkObjectImpl)


//========================================================================================
//
// ExtLinkODBCXMLElementLinkObject Public Implementation
//
//========================================================================================

//========================================================================================
// Constructor
//========================================================================================
ExtLinkODBCXMLElementLinkObject::ExtLinkODBCXMLElementLinkObject(IPMUnknown* boss)
: inherited(boss)
{
	fClientId = kIDLinkClientID;
}

//========================================================================================
// Destructor
//========================================================================================
ExtLinkODBCXMLElementLinkObject::~ExtLinkODBCXMLElementLinkObject()
{
}

//========================================================================================
// ExtLinkODBCXMLElementLinkObject::Import
//========================================================================================
ErrorCode ExtLinkODBCXMLElementLinkObject::Import(const ILinkResource* iResource, UID& linkUID, UIFlags uiFlags)
{
	ASSERT_UNIMPLEMENTED(); 
	return kFailure;
}

//========================================================================================
// ExtLinkODBCXMLElementLinkObject::Export
//========================================================================================
ErrorCode ExtLinkODBCXMLElementLinkObject::Export(const ILinkResource* iResource, UID& linkUID, UIFlags uiFlags) const
{
	ASSERT_UNIMPLEMENTED(); 
	return kFailure;
}

//========================================================================================
// ExtLinkODBCXMLElementLinkObject::Resolve
//========================================================================================
ErrorCode ExtLinkODBCXMLElementLinkObject::Resolve(const ILinkResource* iResource, UID& linkUID, UIFlags uiFlags)
{
	// Import always win!

	ErrorCode err = kFailure;

	IDataBase *db = ::GetDataBase(iResource);
	// get the element associated with this link
	InterfacePtr<ILink> iLink(db, linkUID, UseDefaultIID());
	InterfacePtr<ILinkObject> iLinkObject(db, iLink->GetObject(), UseDefaultIID());
	if (!iLink || !iLinkObject)
		return err;
	InterfacePtr<IIDXMLElement> element((IIDXMLElement*)iLinkObject->QueryLinkedObject(IID_IIDXMLELEMENT));

	if (element)
	{
		InterfacePtr<ITextModel> elementModel(Utils<IXMLUtils>()->QueryTextModel(element));
		// get the element content reference and content type
		const XMLContentReference& contentReference = element->GetContentReference();
		XMLContentReference::ContentType contentType(contentReference.GetContentType());
		// We only handle text content type and the element is supposed to be text content in this sample 
		if(contentType == XMLContentReference::kContentType_Text)
		{
			TextIndex startPos(kInvalidTextIndex);
			TextIndex endPos(kInvalidTextIndex);	
			if (!Utils<IXMLUtils>()->GetElementIndices(element, &startPos, &endPos))
			{
				// If this is not tagged text, get the first item in in the text.
				InterfacePtr<ITextModel> storyModel(contentReference.GetUIDRef(), UseDefaultIID());
				if (storyModel)
				{
					startPos = -1;
					endPos = storyModel->GetPrimaryStoryThreadSpan() - 1;
				}
			}
			
			URI uri = iResource->GetURI();
			WideString newValue; 
			{
				if (!iResource->CanQueryStream(ILinkResource::kRead))
					return kFalse;
				InterfacePtr<IPMStream> importStream(iResource->QueryStream(ILinkResource::kRead));
				if (!importStream)
					return kFalse;

				StreamState streamState = importStream->GetStreamState();
				if (streamState == kStreamStateFailure || streamState == kStreamStateClosed)
				{
					// this can happen if we fail to open a file.
					// an open Word file on a network is a classic example.
					return kFalse;
				}
				if (Utils<IExtLinkFacade>()->GetAssetValueFromResourceStream(importStream, newValue))
				{
					//Replace underlying text with new value
					boost::shared_ptr<WideString> wideVal(new WideString(newValue));
					// the text to be replaced needs to exclude the hidden text we insert for tag
					err = ExtLinkUtils::ReplaceText(elementModel, startPos+1, endPos - startPos-1, wideVal);
				}
			}
		}
	}
	return err;
}

//========================================================================================
// ExtLinkODBCXMLElementLinkObject::QueryLinkedObject
//========================================================================================
IPMUnknown* ExtLinkODBCXMLElementLinkObject::QueryLinkedObject(const PMIID& interfaceId) const
{
	//UID myUID = (uid == kInvalidUID) ? fUID : uid;
	IDataBase* 	db = ::GetDataBase(this);
	if (db)
	{
		InterfacePtr<IXMLReferenceData> referenceData(this, UseDefaultIID());
		if (referenceData)
		{
			XMLReference xmlRef = referenceData->GetReference();
			InterfacePtr<IIDXMLElement> xmlElement( xmlRef.Instantiate() );
			if (xmlElement)
				return (IPMUnknown*)xmlElement->QueryInterface(interfaceId);
		}
	}
	else
	{
		ASSERT_FAIL("Attempt to call QueryLinkedObject() before data link is instantiated?");
	}

	return nil;
}
