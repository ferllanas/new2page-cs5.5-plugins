//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/persistentlist/N2PSQL_Document_Observer.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sadahiro $
//  
//  $DateTime: 2005/02/09 17:46:58 $
//  
//  $Revision: #8 $
//  
//  $Change: 316959 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

//===========================
//  Plug-in includes
//===========================

#include "VCPlugInHeaders.h"




// Project includes:



// Interface includes:
#include "IDocument.h"
#include "IHierarchy.h"
#include "ISubject.h"

// API includes:
#include "CAlert.h"
#include "CmdUtils.h"
#include "CObserver.h"
#include "PageItemScrapID.h" // for kDeletePageItemCmdBoss
#include "StandoffID.h" // for kDeleteStandOffItemCmdBoss and kDeleteInsetPageItemCmdBoss
#include "UIDList.h"
//#include "N2PExpPnlTrvUtils.h"
#include "KeyStates.h"

// Project includes:
//#include "IA2PProUIDList.h"
#include "N2PsqlID.h"
//#include "A2PProSelectionObserver.cpp"
#include "IDocumentSignalData.h"
#include "IHierarchy.h"
#include "IPageList.h"
#include "ILayoutUIUtils.h"

#include "N2PSQLUtilities.h"


/** This observer observes the front document's IID_IHIERARCHY_DOCUMENT protocol, so when a page 
	item is deleted from the document, this observer will be notified.  This is needed because we want 
	to be notified _Before_ the page item is about to be deleted so this implementation can have a chance
	to delete the instances of persistent list items associated with the said page item, 
	otherwise if we wait until SelectionAttributeChanged of my CSB gets called, it's too late; 
  	The page item will have been deleted from the hierarchy, and all of instances of persistent list items
	will be left dangling somewhere in memory (= boss leaks)!  This observer gets attached
	with the ISubject of kDocBoss when a document is opened or created, through the use 
  	of a open/new document responder, and this observer is detached when a document is closed, through the
	use of a close document responder.
  
	@ingroup persistentlist
	@see A2PProDocResponder
*/
class N2PSQL_Document_Observer : public CObserver
{
public:
	/**	Constructor.
		@param boss IN interface ptr from boss object on which this interface is aggregated.
	*/
	N2PSQL_Document_Observer(IPMUnknown* boss);

	/**	Destructor.
	*/
	virtual ~N2PSQL_Document_Observer();

	/** Called by the application to allow the observer to attach to the 
	 * 	subjects to be observed (kDocBoss)
	*/
	void AutoAttach();

	/** Called by the application to allow the observer to detach from the 
	 * 	subjects being observed.
	*/
	void AutoDetach();

	/** Update is called for all registered observers, and is the method 
	 * 	through which changes are broadcast. 
	 * 	In this case, the only things we are interested in are if the drop 
	 * 	down list selection has changed and if a page item is deleted from 
	 * 	the document. 
	 * 
	 * 	@param theChange IN specifies the class ID of the change to the subject. 
	 * 		Frequently this is a command ID.
	 * 	@param theSubject IN points to the ISubject interface for the subject 
	 * 		that has changed.
	 * 	@param protocol IN specifies the ID of the changed interface on the 
	 * 		subject boss.
	 * 	@param changedBy IN points to additional data about the change. 
	 * 		Often this pointer indicates the class ID of the command 
	 * 		that has caused the change.
	*/
	virtual void Update(const ClassID& theChange, 
						ISubject* theSubject, 
						const PMIID& protocol, 
						void* changedBy);

protected:
	/**	Attaches this observer to a document.
	 * 	@param iDocument IN The document to which we want to attach.
	*/
	void AttachDocument(IDocument* iDocument);

	/**	Detaches this observer from a document.
	 * 	@param iDocument IN The document from which we want to detach.
	*/
	void DetachDocument(IDocument* iDocument);

	/**	The selected page items are about to be deleted, call IA2PProSuite's 
	 * 	function to delete its associated IA2PProData objects.  
	 * 	It is called when we get the pre-notification of deletion of 
	 * 	page items.
	 * 	@param changedBy IN points to additional data about the change. 
	 *	@see N2PSQL_Document_Observer::Update
	*/
	void HandlePageItemDeleted(void* changedBy);

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PSQL_Document_Observer, kN2PSQL_Document_ObserverImpl)

/* N2PSQL_Document_Observer Constructor
*/
N2PSQL_Document_Observer::N2PSQL_Document_Observer(IPMUnknown* boss) 
	: CObserver(boss, IID_IN2PSQL_DOCUMENT_OBSERVER)
{
}

/* N2PSQL_Document_Observer Destructor
*/
N2PSQL_Document_Observer::~N2PSQL_Document_Observer()	
{
}


/*	N2PSQL_Document_Observer::AutoAttach
*/
void N2PSQL_Document_Observer::AutoAttach()
{
	CObserver::AutoAttach();

	InterfacePtr<IDocument> iDocument(this, UseDefaultIID());
	if (iDocument != nil)
		this->AttachDocument(iDocument);
}


/*	N2PSQL_Document_Observer::AutoDetach
*/
void N2PSQL_Document_Observer::AutoDetach()
{
	CObserver::AutoDetach();

	InterfacePtr<IDocument> iDocument(this, UseDefaultIID());
	if (iDocument != nil)
		this->DetachDocument(iDocument);
}

/*	N2PSQL_Document_Observer::Update
*/
void N2PSQL_Document_Observer::Update(const ClassID& theChange, 
							   ISubject* theSubject, 
							   const PMIID& protocol, 
							   void* changedBy)
{
	do
	{
		ICommand* iCommand = (ICommand*)changedBy;
		const UIDList itemList = iCommand->GetItemListReference();
		ICommand::CommandState cmdState = iCommand->GetCommandState();


		
		if ((protocol == IID_IHIERARCHY_DOCUMENT) || 
			(protocol == IID_IHIERARCHY))
		{
			if ((protocol == IID_IHIERARCHY) &&
				(theChange == kAddToHierarchyCmdBoss || theChange == kRemoveFromHierarchyCmdBoss))
			{
				//	These commands double broadcast on both IID_IHIERARCHY & IID_IHIERARCHY_DOCUMENT
				//	Only react to the document broadcasts
				return;
			}

			if ((theChange == kDeletePageItemCmdBoss) ||
				(theChange == kDeleteStandOffItemCmdBoss) ||
				(theChange == kDeleteInsetPageItemCmdBoss))//kDeleteStoryCmdBoss==theChange/*
			{
				if (itemList.Length() && 
					(cmdState == ICommand::kNotDone))
				{

			
					// we only do this when we got here thru Command::Do(), this fixes a bug before when it also
					// come here thru ReDo (cmdState == ICommand::kNotRedone, it resulted in trying to push the 
					// command during a do/undo operation because we got here thru a pre-notification from redo, 
					// and HandlePageItemDeleted() will eventually process a command again.  Since in the redo
					// method, I willc all Do() again, so we don't need to do anything here.
					//PMReal zoom = FluirFunciones::GetZoomFactor();
					//FluirFunciones::ZoomPage();
					//CAlert::InformationAlert("Fue borrada un Story");
					HandlePageItemDeleted(changedBy);
					//FluirFunciones::Zoom(zoom);
					
				}
				/*else
				{
					if(cmdState == ICommand::kDone)
					{
						IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
						if (document == nil)
							break;
						IDataBase* db = ::GetDataBase(document);
						if (db == nil)
							break;
						CAlert::InformationAlert("Pasa por aca XXX");
						iCommand->DoImmediate();
						CAlert::InformationAlert("Pasa por aca");
					}
				}*/
			}
			
		}

	} while (kFalse);
}

/*	N2PSQL_Document_Observer::HandlePageItemDeleted
*/
void N2PSQL_Document_Observer::HandlePageItemDeleted(void* changedBy)
{
	do
	{
	
		bool presionado = IsCommandKeyPressed();
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
			break;
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
			break;
	
		ICommand* cmd = (ICommand*)changedBy;
		const UIDList* selectUIDList = cmd->GetItemList();

		if (selectUIDList->Length() < 1)
		{
			//ASSERT_FAIL("invalid page item");
			break;
		}

		UIDList myUIDList = *selectUIDList;
		
		int32 items = selectUIDList->Length();
		
		/////Elementos de Notas
		PMString DatosEnXMP=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		
		K2Vector<PMString> Params;
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		

		
		for (int i=0;i<items;i++)
		{

			UIDRef pageItemRef = myUIDList.GetRef(i);//selectUIDList->GetRef(0);		
			UID a = pageItemRef.GetUID();//OBTENGO EL UID del rectangulo
			int b=a.Get();			//obtengo el valor en exadecimal del rectangulo		
			PMString idFrameToDeleted;
			idFrameToDeleted.AppendNumber(b);
			
			/////////////
			//Verifica si no se esta tratando de borrar alguna foto		
			PMString IDElemPadreFoto;
			PMString IDElementoFoto;
			UID UIDFrameFoto;
			for(int NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
			{
				if(RegistrosEnXMP[NumReg].TipoDato=="Foto")
				{	
					IDElemPadreFoto = RegistrosEnXMP[NumReg].IDElemPadre;
					IDElementoFoto = RegistrosEnXMP[NumReg].IDElemento;
					UIDFrameFoto = RegistrosEnXMP[NumReg].IDFrame;
					
					UIDRef pageItemUIDRef(db,UIDFrameFoto);
					if(pageItemRef==pageItemUIDRef)
					{
						//CAlert::InformationAlert("Esta tratando de borra una foto");
						//DesligarElementoDEstrucPorNum
					}
				}
			}
			
			
			
		/*	/////////////	
			//Verifica si no se esta tratando de borrar alguna foto	
			int32 contNotasInVector=30;	//Cantidad de notas que se han fluido sobre el documento
		
			StructIdFramesElementosXIdNota* VectorNota=new StructIdFramesElementosXIdNota[30];
		
			this->LlenarVectorElemDNotasFluidas(VectorNota, contNotasInVector, document);
		
		
			if(contNotasInVector>0)
			{
				
				
				//Actualiza des de la base de datos los elementos de3 cada una de las notas fluidos sobre el documento abierto
				for(int32 numNota=0 ; numNota<contNotasInVector ; numNota++)
				{	
				
					DesligarElementoDEstrucPorNum(VectorNota,numNota);
				
				}
				
			
			}*/
			
			
		}
		
		
	} while (false);
	//CAlert::InformationAlert("fin");
}

/*	N2PSQL_Document_Observer::AttachDocument
*/
void N2PSQL_Document_Observer::AttachDocument(IDocument* iDocument)
{
	do
	{
		if (iDocument == nil)
		{
			//ASSERT_FAIL("no document to attach to");
			break;
		}

		InterfacePtr<ISubject> iDocSubject(iDocument, UseDefaultIID());
		if (iDocSubject == nil)
		{
			//ASSERT_FAIL("no document subject to attach the observer");
			break;
		}

		//	Protocols used for page item model changes
		if (!iDocSubject->IsAttached(this, IID_IHIERARCHY_DOCUMENT, IID_IN2PSQL_DOCUMENT_OBSERVER))
		{
			iDocSubject->AttachObserver(this, IID_IHIERARCHY_DOCUMENT, IID_IN2PSQL_DOCUMENT_OBSERVER);
		}

		if (!iDocSubject->IsAttached(this, IID_IHIERARCHY, IID_IN2PSQL_DOCUMENT_OBSERVER))
		{
			iDocSubject->AttachObserver(this, IID_IHIERARCHY, IID_IN2PSQL_DOCUMENT_OBSERVER);
		}
	} while (kFalse);
}


/*	N2PSQL_Document_Observer::DetachDocument
*/
void N2PSQL_Document_Observer::DetachDocument(IDocument* iDocument)
{
	do
	{
		if (iDocument == nil)
		{
			//ASSERT_FAIL("no document to attach to");
			break;
		}

		InterfacePtr<ISubject>      iDocSubject(iDocument, UseDefaultIID());
		if (iDocSubject == nil)
		{
			//ASSERT_FAIL("no document subject to attach the observer");
			break;
		}

		if (iDocSubject->IsAttached(this, IID_IHIERARCHY_DOCUMENT, IID_IN2PSQL_DOCUMENT_OBSERVER))
		{
			iDocSubject->DetachObserver(this, IID_IHIERARCHY_DOCUMENT, IID_IN2PSQL_DOCUMENT_OBSERVER);
		}

		if (iDocSubject->IsAttached(this, IID_IHIERARCHY, IID_IN2PSQL_DOCUMENT_OBSERVER))
		{
			iDocSubject->DetachObserver(this, IID_IHIERARCHY, IID_IN2PSQL_DOCUMENT_OBSERVER);
		}
	} while (kFalse);
}



// End, N2PSQL_Document_Observer.cpp.

