//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkOptions.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface Includes
#include "ISession.h"
#include "IWorkspace.h"
#include "IPMStream.h"

// Implementation Includes
#include "CPMUnknown.h"
#include "K2Vector.tpp"

// Project includes:
#include "IExtLinkOptions.h"

/** Class to persist a list of options to the session workspace.
	ExtLinkOptions is a persistent implementation of interface IExtLinkOptions. 

	
	@ingroup extendedlink
*/

class ExtLinkOptions : public CPMUnknown<IExtLinkOptions>
{
public:

	/** Constructor
		@param boss interface ptr from boss object on which this interface is aggregated.
	*/
	ExtLinkOptions(IPMUnknown* boss);

	/** Destructor
	*/
	virtual ~ExtLinkOptions() {}

	/** Method to add in an options to the list of persistent options, if the option in the index exists, update the data
		@param newVal [IN] new  value.
		@pararm indexWhere [IN] position you want the option
	*/
	virtual void AddOption(
		const ExtLinkOptionsData& newVal, const int32 indexWhere); 

	/** Retrieves list option specified by index.
		@param index [IN] specifies index of desired option.
		@return Returns option as string.
	*/
	virtual ExtLinkOptionsData GetNthOption(const int32 index); 

	/** Retrieves current list option (for Undo). We only have one option hence the list is overkill,
		but if you store more options, then you'd only need to modify this
		method.
		
		@return Returns option as string.
	*/
	virtual void GetOptionListCopy(K2Vector<ExtLinkOptionsData>& outList) { 
		for(int32 i=0; i < fPersistentOptionList.size(); i++) {
			outList.push_back(fPersistentOptionList[i]);
		}
	}
	
	/**	Return the flag whether refresh resource status automatically
	 */
	virtual bool16 IsAutoRefreshStatus() const;
	
	/**	Set flag whether refresh resource status automatically
		@param val [IN] true auto refresh, false otherwise
	 */
	virtual void AutoRefreshStatus(bool16 val);
	
	/** Persistence related method; reads from or writes to given stream
		@param s [IN] the persistent in and out stream.
		@param prop [IN] the implementation ID.
	*/
	virtual void ReadWrite(IPMStream* s, ImplementationID prop);
	
private:
	bool16 validate(const int32 index);

	K2Vector<ExtLinkOptionsData> fPersistentOptionList;
	
	bool16 fAutoRefreshStatus;
};

/*	CREATE_PERSIST_PMINTERFACE
	This macro creates a persistent class factory for the given class name
	and registers the ID with the host.
*/
CREATE_PERSIST_PMINTERFACE(ExtLinkOptions, kExtLinkOptionsImpl)

/*	Constructor
	The preference state is set to kFalse for the application 
	workspace. The preference state for a new document is set
	to the application preference state.
*/
ExtLinkOptions::ExtLinkOptions(IPMUnknown* boss) :
		CPMUnknown<IExtLinkOptions>(boss),
		fAutoRefreshStatus(kTrue)
{
}


/* AddOption
*/
void ExtLinkOptions::AddOption(
	const ExtLinkOptionsData& newVal, const int32 indexWhere) 
{ 
	PreDirty ();
	if(indexWhere < 0 || indexWhere >= fPersistentOptionList.size())
	{
		fPersistentOptionList.push_back(newVal); 
	}
	else
	{
		fPersistentOptionList[indexWhere] =  newVal;
	}
	
}


/* GetNthOption
*/
ExtLinkOptionsData ExtLinkOptions::GetNthOption(const int32 index)
{ 	
	if(this->validate(index))
	{
		return fPersistentOptionList[index]; 
	}
	return ExtLinkOptionsData();
}

bool16 ExtLinkOptions::IsAutoRefreshStatus() const
{
	return fAutoRefreshStatus;
}

void ExtLinkOptions::AutoRefreshStatus(bool16 val)
{
	PreDirty();
	fAutoRefreshStatus = val;
}

/* ReadWrite
*/
void ExtLinkOptions::ReadWrite(IPMStream* s, ImplementationID prop)
{
	/* 	
	//	REMINDER: 
	//	If you change the arrangement of persistent data in this method, 
	//	remember to update the format number in the PluginVersion resource. 
	//	(See the ExtLinkID.h file for the plug-in specific format numbers.) 
	*/
	
	int32 numOptions;
	if(s->IsReading())
	{
		s->XferInt32(numOptions);
		fPersistentOptionList.clear();
		fPersistentOptionList.reserve(numOptions);
		for(int32 i = 0; i < numOptions; i++)
		{
			ExtLinkOptionsData tempOption;
			PMString	tempStr;
			// Read in from the stream
			tempStr.ReadWrite(s);
			tempOption.SetConnectionName(tempStr);
			tempStr.ReadWrite(s);
			tempOption.SetImageFolderPath(tempStr);
			tempStr.ReadWrite(s);
			tempOption.SetDataSourceName(tempStr);
			tempStr.ReadWrite(s);
			tempOption.SetDBTableName(tempStr);
			fPersistentOptionList.push_back(tempOption);
		}				
	}
	else
	{
		numOptions = fPersistentOptionList.size();
		s->XferInt32(numOptions);
		for(int32 i=0; i < numOptions; i++)
		{
			// Write out the option to the stream
			PMString	tempStr;
			tempStr = fPersistentOptionList[i].GetConnectionName();
			tempStr.ReadWrite(s);
			tempStr = fPersistentOptionList[i].GetImageFolderPath();
			tempStr.ReadWrite(s);
			tempStr = fPersistentOptionList[i].GetDataSourceName();
			tempStr.ReadWrite(s);
			tempStr = fPersistentOptionList[i].GetDBTableName();
			tempStr.ReadWrite(s);
		}
	}	

	s->XferBool(fAutoRefreshStatus);
}


/* validate
*/
bool16 ExtLinkOptions::validate(const int32 index)  
{
	return index >= 0 && index < fPersistentOptionList.size();
}

//	end, File: ExtLinkOptions.cpp
