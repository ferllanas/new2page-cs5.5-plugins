//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/FileTreeUtils.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __PnlTrvUtils_H_DEFINED__
#define __PnlTrvUtils_H_DEFINED__

#include "IParcel.h"
#include "TextRange.h"
#include "ITextParcelList.h"
#include "N2PNotesStorage.h"
#include "N2PSQLListBoxHelper.h"
#include "UpdateStorysAndDBUtilis.h"

#include "N2PsqlID.h"
//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")
class IPMFont;
class IDocument;
class IControlView;
//class K2Vector;
class IMultiColumnTextFrame;
struct ClassRegEnXMP
{
	PMString IDElemPadre;
	PMString IDElemento;
	int32 IDFrame;
	PMString TipoDato;
	PMString NameDSN;
	PMString LastCheckInNote;
};


struct ClassDatosDePagEnXMP
{
	int32 NumPagina;
	PMString Seccion;
	PMString Pubicacion;
	PMString FechaPublicacion;
};



/** A collection of miscellaneous utility methods used by this plug-in.
@ingroup paneltreeview
*/
class N2PSQLUtilities
{

public:
	/** Return a version of the supplied path which consists only of the 
		file 'name', removing everything up to and including the last path separator.
		@param fromthis [IN] path of interest
		@return string containing the path-part minus the last segment
	*/

	

	///////////////////////////////////////////////////////
/*	static PMString QueryKey(HKEY hKey, LPTSTR lPath, LPCTSTR lSubKey);

	static int CreateKey(LPCTSTR lpSubKey, LPTSTR lpClass, LPTSTR lpData);

	static int DeleteKey(HKEY hKey, LPTSTR sPsth, LPCTSTR lpSubKey);

	static bool16 ExistKey(HKEY hKey, LPTSTR lPath, LPCTSTR lSubKey );*/

	static PMString ProporcionaFechaArchivo(CString Archivo);

	static PMString DescryptSerial(CString CadenaEncriptada);

	static bool16 SaveXMPVar(PMString Variable,PMString valor, IDocument* doc);
	
	static PMString GetXMPVar(PMString Variable, IDocument* doc);
	
	static bool16 RemoveXMPVar(PMString Variable, IDocument* doc);

	static bool16 ExisteArchivoParaLectura(PMString *filePath);

	static PMString TruncateExtencion(const PMString& fromthis);

	static PMReal ConversionPuntosAUnidades(PMReal Cantidad, PMString &NameIOM);

	//Busca y reemplaza los datos de un registro en caso de que ya exista.
	static bool16 BuscaYReemplaza_ElementoOnPaginaXMP(PMString variableXMP,PMString Id_Elemento_padre, PMString Id_Elemento,
										   PMString FrameID, PMString TipoDato, 
										   PMString NombrePreferDSN,PMString StatusNota,
										   bool16 UnLockStory,
										   PMString FYHUCheckinNota, bool16 EsElementoFoto);
										   
	//static bool16 LlaveDongleCorrecta();
	////////////////////////////////////////////////////////
	static int32 GetCantDatosEnXMP(PMString DatosEnXMP);
	
	
	static bool16 LlenaStructRegistrosDeXMP(PMString DatosEnXMP,ClassRegEnXMP *RegistrosEnXM);

	static void LLenarListaNotasEnPaletaN2PSQL(PMString Buscar,bool16 isOnPasesNotas, PreferencesConnection PrefConections);
	

	static PMString ObtenerTextoDeWidgetEnN2PSQLPaleta(WidgetID widget);

	static void RealizarBusqueda_Y_llenado(PMString Busqueda, bool16 IsOnlyPases, PreferencesConnection PrefConections);
	
	static PMString CrearFolderPreferencias();
	
	
	static PMString TruncatePath(const PMString& fromthis);
	
	static bool16 validPath(const PMString& p);	
	
	static bool16 ColocarTextoDeWidgetEnN2PSQLPaleta(WidgetID PanelWidgetID,WidgetID widget,PMString cadena);
	
	static bool16 ReemplazaLastCheckInNotaEnXMP(PMString IDNote,PMString UltimoCheckIn,  IDocument* document);
	
	static int32 GetUIDOfTextFrameSelect(int32& NumPage);
	
	static bool16 GetIDNota_ConIDTextFrame_SobreXMPNotasFluidas(int32 IDFrame,PMString& IDNota,bool16& PuedeModificarGuia);
	
	static bool16 MuestraWidgetEnN2PSQLPaleta(WidgetID widget,bool16 Mostrar);
	
	////Funciones que manipulan el los Datos que servisar para el N2PCS
	static bool16 ColocaDatosDePaginaOnXMP(int32 numeroPaginaActual,PMString Seccion,PMString Publicacion,PMString FechaPubli);

	static bool16 LlenaStructRegistrosDDatosDePagXMP(PMString DatosEnXMP,ClassDatosDePagEnXMP* RegistrosEnXM);

	static bool16 CambiaEstadoEnCheckBoxWidgetEnN2PSQLPaleta(WidgetID widget,bool16 selected);

	static PMString GetPMStringOfTextFrame(int32 UIDModelString);
	
	static bool16  Busca_y_Reemplaza_En_XMP_IDlementosNotas( PMString ID_ElementoPadreAnterior , PMString ID_ElementoPadreNuevo,
												 PMString Id_Titulo,PMString Id_PieFoto, 
												 PMString Id_Balazo,PMString Id_Content,
												 PMString FYHUCheckinNota, PMString DSNNuevo,
												 PMString FechaNueva);
												 
												 
	static bool16 GetEstateCkBoxOfWidget(WidgetID Widget);
	
	static void RealizarBusqueda_Y_llenadoDeListaPhoto(PMString Busqueda, PreferencesConnection PrefConections);
	
	static void MuestraIconodeCamaraSiNotaTieneFotos(PMString Busqueda, PreferencesConnection PrefConections);
	
	static UIDRef ImportImageAndLoadPlaceGun(	const UIDRef& docUIDRef, const IDFile& idFile);
	
	
	static bool16 GETStoryParams_OfUIDTextModel( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumLinesFaltantes,
 												int32& NumLinesRestantes,
 												int32& NumCarFaltantes,
 												int32& NumCarRestantes,
 												int32& NumCarOfTextModel);
 												
 												
 	static IParcel* QueryParcelContaining(ITextModel* textModel, const TextIndex at);
 	
 	
 	static UIDRef GetGraphicFrameRef(const InterfacePtr<IMultiColumnTextFrame>& textFrame, const bool16 isTOPFrameAllowed);
 	
 	static ErrorCode CountWordsAndLines(UID UIDTextModelofElementoNota, const InterfacePtr<ITextParcelList>& textParcelList, ParcelKey fromParcelKey, 
											int32 numberOfParcels, PMReal& estimatedDepth,
											int32& NumeroDeLineaAntesDelOverset,int32& UltimoCaracterMostrado, int32& CaracFaltAprox);
											
	static PMRect Get_Coordinates_Reales(const UIDRef& pageItemRef, int32& numpag);
	
	static bool16  BuscaYReemplaza_IDFrameFotoOnXMP(PMString variableXMP, PMString ID_Elemento, PMString FrameID);
	
	static ErrorCode InsertTextMiee(ITextModel* mytextmodel, const TextIndex position,const boost::shared_ptr<WideString>& mytext);
	
	static ErrorCode DeleteText(ITextModel* textModelz,const TextIndex position,const int32 length);
	
	
	
	static bool16 AAAAXCSXCSED( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumLinesFaltantes,
 												int32& NumLinesRestantes,
 												int32& NumCarFaltantes,
 												int32& NumCarRestantes,
 												int32& NumCarOfTextModel, int32& UltimoCaracterMostrado);
 												
 	static bool16  Busca_y_Elimina_En_XMP_IDlementosNotas( PMString ID_ElementoPadre);
 
 	static bool16 LlenarComboUsuarioSobrePaletaN2P();
 	
 	static bool16 IsShowWidgetEnN2PSQLPaleta(WidgetID widget);
 	
 	static PMString ReturnContenidoOfId_Elemento(PMString StringConection, PMString Id_ElementoDContenido, const bool16& ReturnWhitPases);
 	
 	static bool16 LlenarElementosDePasesEnVentanaN2PSQL(PreferencesConnection PrefConections);
 	
 	
	static void LLena_Subnaotas_D_Nota(PMString Id_Nota_Padre, 
									   N2PSQLListBoxHelper listHelper, 
									   bool16 isOnPasesNotas, 
									   int32 Herencia,
									   PreferencesConnection PrefConections);
	
	static UIDRef ImportTaggedTextOfPMString(PMString MyTexTagged, K2Vector<N2PNotesStorage*>& fBNotesStore);
	
	static PMString GetTextPlainOfUIDRefImportTextTagged(UIDRef refTextTaggedImported);
	
	static bool16 ProcessCopyStoryCmd(const UIDRef& itemToPlace, const UIDRef& graphicFrame, IDocument* document);
	
	static PMString ExportTaggedTextOfPMString(UIDRef textModelRef, const PMString& ElementoSS);
	
	static bool16 convertTaggetTextToTaggedTextToXML(PMString& thissestring, const K2Vector<N2PNotesStorage*>& fN2PNotesStore);
	
	static bool16 ExractOfTextXML_TaggetText(PMString& thissestring);
	
	static ErrorCode ObtenListaDeAtributosDRangoDTexto(ITextModel* textModel, const TextIndex start, const TextIndex length);
	
	static bool16 ExtarctOfTextXML_Notes(const PMString& thissestring, K2Vector<N2PNotesStorage*>& fBNotesStore);
	
	static PMString CrearFolderRespaldoNotas(PMString Servidor,PMString Fecha,PMString Publicacion,PMString Seccion, PMString Usuario);

	static int32 GetUIDOfGraphicFrameSelect();
	
	static bool16 BuscaYElimina_IDFrameFotoOnXMP(PMString variableXMP, PMString ID_Elemento, PMString FrameID);
	
	static bool16 LlenarComboEstatusSobrePaletaN2P();
	
	static bool16 GETCoordenadasDePrimerFrameOfTextModel( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumCarOfTextModel,
												K2Vector<PMString>& multiplesFrames,
														 int32& NumLinesFaltantes,
														 int32& NumLinesRestantes,
														 int32& NumCarFaltantes,
														 int32& NumCarRestantes);
 												
 	static void CleanPalette();
 	
 	static bool16 DELETE_Pinches_HyperlinksYBookMarks();
 	
 	static bool16 RemoveTags(PMString& CadenaOriginal, PMString& TagABorrar);
 	
 	
 	static void SetRootStyleADefaultCharStyle();
 	
 	static bool16 EnableWidgetEnN2PSQLPaleta(WidgetID widget,bool16 Enable);
 	
 	static void BusquedaPorDialogo(PreferencesConnection PrefConections);
 	
 	static PMString AbrirDialogoBusqueda(bool16& FindOnlyPases);
 	
 	//static void ExportarNotasATablaWEB(IDocument* document, const bool16& RefreshListaNotas);
 	
 	//static PMString ExportOnlyTextOfUIDTextModelRef(UIDRef textModelRef);
 	
 	static K2Vector<PMString> GetPDFExportStyles(void);
 	
 	
	static bool16 ExportPaginaPDF(UIDRef pageUIDRef,PMString PathFile);
	
	
	static bool16 N2PExportPDFPages(PMString& RutaToSaveFile,PMString& RutaTOJpg,PMString Pagina_To_SavePage);

 	static bool16 CreateFolder(PMString path,PMString foldername);
 	
 	static int Contador_Archivos_Carpeta(PMString Dir);
 	
 	static PMRect Get_Coordinates_On_Spread(const UIDRef& pageItemRef);
 	 	
 	static UID GetUIDTextModelFromUIDRefTextFrame(UIDRef RefOfTextFrame);
 	
 	static bool16  Existe_IDFrameFotoOnXMP(PMString variableXMP, PMString FrameID); 	
 
 	static void ReplaceAllOcurrencias(PMString& origen,PMString &Target, PMString& replace);
 	
 	static void BorrarTagDTextoTageado(PMString& origen,PMString &Target);
 	
 	static void ReemplazarSlashesPorCommayPunto(PMString& origen,PMString &Target);
 	
 	static bool16 VerificarSiExisteDocumentoSobrePathInicial(IDocument * document);

	static bool16 SetInComboSobrePaletaN2P(WidgetID idCombo,PMString ItemToSelect);
	
	static void ReplaceRetornBy(PMString& origen, PMString& replace);
	
	static void  ExportPDFAsPresetName( PMString Path,PMString& NombrePref);
	
	
	static bool16  BuscaYElimina_ElementoOnPaginaXMP(PMString variableXMP, int32 FrameID);
	
	static void InicializaDebugeo();
	static void FinalizaDebugeo();
	static void ImprimeMensaje(const PMString& msgDebug);
	
	/*
	 Busca si se encuentra una foto
	 IN FrameID, id frame de la foto
	 OUT ID_Elemento, Id_Elemento de foto
	 IN variableXMP, variable a buscar en XMP
	 
	 regresa verdadero si el id frame ya es un elemento de N2P.
	 */
	static bool16  Busca_IDFrameFotoOnXMP(PMString variableXMP, PMString& ID_Elemento, PMString FrameID);
	
	/*
		Funcion que Obtiene el id Frame de Un Elemento de Nota y la etiqueta de este elemento(Estatus y/o dirigido a)
			IN variableXMP		Variable de XMP en donde se busca el id del elemento de la foto.
			IN ID_ElementoFoto	 id elemento de foto.
			IN/OUT	int32FrameElementnota	uid en forma de entero del frame del elemento de la Nota.
			IN/OUT	Etiqueta			etiqueta que contiene el elemento de la Nota. (Estatus Dirigido A).
	 
		Return false si no es encontrado el elemento de la Nota sobre la variable del XMP.
	 */
	static bool16  GetIdFrameDElementoNotaANDEtiquetaDNota(PMString variableXMP,int32& int32FrameElementnota ,PMString& Etiqueta, PMString ID_ElementoFoto);
	
	/*
		Funcion que prapara e importa una imagen a partir de la ruta, antes de ser colocada.
			OUT	docUIDRef  UIDRef del elemento importado.
			IN	fromPath	Ruta del elemento o archivo  a importar
		return false si no fue importado con exito el archivo.
	 */
	static UIDRef PreparaImportImageAndLoadPlaceGun(const UIDRef& docUIDRef, const PMString& fromPath);
	
	/*
		Funcion que Coloca el archivo que fue importado por la funcion PreparaImportImageAndLoadPlaceGun sobre un frame.
			IN	docUIDRef UIDRef dek documento donde va a ser colocado el archivo importado.
			IN	itemToPlace	UIDRef del Archivo que fue importado.
			IN graphicFrame	UIDRef del frame donde sera colocado el archivo importado.
	 */
	static void CreateAndProcessPlaceItemInGraphicFrameCmd(const UIDRef& docUIDRef,const UIDRef& itemToPlace, const UIDRef& graphicFrame);
	
	static IPMFont* QueryFont(const InDesign::TextRange& textRange, 
										PMString& fontFamilyName,
										PMString& fontStyleName);
	static ErrorCode FindParaStyleOnStory(const InDesign::TextRange& fTextRange, const PMString &nameStyToFind,
										  TextIndex &position,int32 &length, const bool16& equalNameParaStyle=kTrue);
	
	static bool16 LlenarComboGuiasNotasSobrePaletaN2P(PMString Seccion);
	
	static UIDList GetUIDListOfGraphicFramesSelected(UIDRef &RefOfTextFrame);

	static UIDList GetUIDListOfTextFramesSelected(UIDRef &RefOfTextFrame);
	
	static bool16 IniciaDetieneObserver(WidgetID PanelWidgetID, WidgetID widget,bool16 startObserber);

};

#endif // __PnlTrvUtils_H_DEFINED__

