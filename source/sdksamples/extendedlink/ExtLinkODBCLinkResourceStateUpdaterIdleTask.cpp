//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkODBCLinkResourceStateUpdaterIdleTask.cpp $
//  
//  Owner: Michael Easter
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  ADOBE CONFIDENTIAL
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "CIdleTask.h"

// ----- Interfaces -----
#include "IDataBase.h"
#include "IIdleTaskMgr.h"
#include "ILinkManager.h"
#include "IExtLinkStateUpdateProcessor.h"

// ----- ID.h files -----
#include "ExtLinkID.h"

//========================================================================================
// Class ExtLinkODBCLinkResourceStateUpdaterIdleTask
//========================================================================================
/**
	This idle task is to process the pending job for providing status update for a link resource.  
	The jobs are maintained by the IExtLinkStateUpdateProcessor, this idle task simply calls 
	IExtLinkStateUpdateProcessor::UpdateStateForTopJob to process the job on the queue.
	
	@ingroup extendedlink
	@see ExtLinkODBCLinkResourceStateUpdater
	@see IExtLinkStateUpdateProcessor
	@see ExtLinkStateUpdateProcessor
 */
class ExtLinkODBCLinkResourceStateUpdaterIdleTask : public CIdleTask
	{
	public:
		ExtLinkODBCLinkResourceStateUpdaterIdleTask(IPMUnknown* boss);
		
		virtual uint32 RunTask(uint32 flags, IdleTimer* timeCheck);
		virtual const char* TaskName();
};

CREATE_PMINTERFACE(ExtLinkODBCLinkResourceStateUpdaterIdleTask, kExtLinkODBCLinkResourceStateUpdaterIdleTaskImpl)


//========================================================================================
//
// ExtLinkODBCLinkResourceStateUpdaterIdleTask Public Implementation
//
//========================================================================================

//========================================================================================
// Constructor
//========================================================================================
ExtLinkODBCLinkResourceStateUpdaterIdleTask::ExtLinkODBCLinkResourceStateUpdaterIdleTask(IPMUnknown* boss) 
: CIdleTask(boss)
{
}

//========================================================================================
// ExtLinkODBCLinkResourceStateUpdaterIdleTask::RunTask
//========================================================================================
uint32 ExtLinkODBCLinkResourceStateUpdaterIdleTask::RunTask(uint32 flags, IdleTimer* idleTimer)
{
	if (flags & (IIdleTaskMgr::kInBackground | IIdleTaskMgr::kMenuUp | IIdleTaskMgr::kMouseTracking)) 
		return kOnFlagChange;
	
	InterfacePtr<IExtLinkStateUpdateProcessor> stateUpdateProcessor(this, UseDefaultIID());
	if (stateUpdateProcessor) {
		bool outOfTime(false);
		do {
			stateUpdateProcessor->UpdateStateForTopJob();
			if (idleTimer && (*idleTimer)() == 0)
				outOfTime = true;
		} while (!outOfTime);
	}
	return 250;
}

//========================================================================================
// ExtLinkODBCLinkResourceStateUpdaterIdleTask::TaskName
//========================================================================================
const char* ExtLinkODBCLinkResourceStateUpdaterIdleTask::TaskName()
{
	return "ExtLinkODBCLinkResourceStateUpdaterIdleTask";
}

