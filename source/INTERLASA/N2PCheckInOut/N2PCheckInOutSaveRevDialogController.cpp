/*
//	File:	GuardarRevDialogController.cpp
//
//	Date:	28-Jan-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2004 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/





#include "VCPlugInHeaders.h"


#include "IFileUtility.h"
// Interface includes:
// Interface includes:
#include "IApplication.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"
#include "IDocumentCommands.h"

#include "IDocument.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "ICoreFilename.h"
#include "IDocumentUtils.h"
#include "IDataBase.h"


#include "FileUtils.h"
#include "ErrorUtils.h"
#include "ILayoutUIUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"
#include "OpenPlaceID.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "CAlert.h"
#include "SDKUtilities.h"
#include "SDKFileHelper.h"
#include "SDKLayoutHelper.h"

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "PlatformFileSystemIterator.h"
#include "ImportExportUIID.h"

#ifdef WINDOWS
	#include <sys/types.h> 
	#include <sys/stat.h> 
#endif
#ifdef MACINTOSH
	#include <types.h> 
	#include <sys/stat.h>  
#endif

#include <time.h>
#include <locale.h> 
#include <Math.h> 

// Project includes:
#include "N2PCheckInOutID.h"
#include "N2PCheckInOutListBoxHelper.h"
#include "ICheckInOutSuite.h"
#include "IN2PXMPUtilities.h"
#ifdef MACINTOSH
	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif

//#include <winreg.h>    //If you are using MS Visual C++ you do not need this
//#include <direct.h>
//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")

//#using <mscorlib.dll>

//using namespace System;
//using namespace System::IO;

/** GuardarRevDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class GuardarRevDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		GuardarRevDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~GuardarRevDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);
		
		virtual void UserCancelled();
		
	private:

		
		bool16 LLenar_Combo_Publicacion(PMString Id_PublicacionText);
		
		bool16 LLenar_Combo_Seccion(PMString Id_SeccionText);	
		
		int32 GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);
		
		bool16 LLenar_Combo_EstatusPagina(PMString& Id_EstatusActual);
		
		PMString  Aplicar_Usuario_Actual();

		PMString BuscarEnUsuarios(PMString &Busqueda);

		void LLenar_Combo_DirigidoA();
		
		void SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget);
		
		bool16 EnableDisableWidget(WidgetID widget,bool16 Enabled);
		
		K2Vector<PMString> VectorIdSeccion;
		K2Vector<PMString> VectorIdPublicacion;
	
		bool16 agregarIssuesAndSection(PMString Issue, PMString Seccion);
	
	bool16 LlenarListaDeIssueDePagina();
		
		ErrorCode CheckInPagina();
		//
		
		PMString Aplicacion;			//Nombre de la aplicacion sobre la que nos encontramos trabajando
		PMString IDUsuario;			//ID del Usuario que se encuentra trabajando en este momento
		PMString HoraIn;				//Hora que entro a trabajar el usuario
		PMString FechaIn;			//Fecha que entro el usuario a trabajar
			
		PMString horaCrea;			//Datos de la ultima pagina chekineada
		PMString fechaCrea;
		PMString Seccion;
		PMString DirigidoA;
		PMString Estatus;
		PMString Pagina;
		PMString Date;
		PMString Issue;
		PMString NomAplicacionActual;
		PMString Busqueda;
		PMString File;
		PMString ID_Pagina;
		PMString ID_Evento;
		PMString N2PFolio_Pagina;
		PMString CadenaSinDosRutas;
		PMString CopiaToLast;
		PMString StringConection;
		PMString MySQLDate;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(GuardarRevDialogController, kGuardaRevDialogControllerImpl)

/* ApplyDialogFields
*/
void GuardarRevDialogController::InitializeDialogFields(IActiveContext* context) 
{
	
	PrefParamDialogInOutPage PreferencesPara;	//Preferencias
	
	PMString CopyPage="";	//Copia del nombre de la pagina
	PMString FrontDocName="";// documento actualPMString DirigidoA="";// documento actual

	
	//Limpia TextEdits
	SetTextControlData(kPaginaWidgetID,"");
	SetTextControlData(kDatePubliWidgetID,"");
	SetTextControlData(kComboBoxIssueWidgetID,"");

	////Llena Te4xtEdit del Usuario en el sistema
	PMString Usuario=this->Aplicar_Usuario_Actual();


	

	do
	{
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
		if(NomAplicacionActual.Contains("InCopy"))
		{
			//this->EnableDisableWidget(kComboBoxStatusWidgetID,kFalse);
			//this->EnableDisableWidget(kComboBoxDirigidoAWidgetID,kFalse);
			this->EnableDisableWidget(kPaginaWidgetID,kFalse);
		}
		
		//
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(document==nil)
			{
				CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
				break;
			}
		
		//Obtiene el nombre del documento actual
		document->GetName(FrontDocName);
		
		
		
		//
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
		
			//obtiene Fecha y hora actuales del sistema
		
		
		PMString fecha = checkIn->FechaYHoraActual();
		PMString* Hora = fecha.Substring(0,fecha.IndexOfWChar(94));
		fecha.Remove(0,fecha.IndexOfWChar(94)+2);
		PMString hora = Hora->GrabCString();

		//Coloca fecha y hora actules del sistema
		fecha.SetTranslatable(kFalse);
		SetTextControlData(kFechaWidgetID,fecha);
		
		hora.SetTranslatable(kFalse);
		SetTextControlData(kHraCreacionWidgetID,hora);
	
	
		//Obtiene las Preferencias con que se modifico la ultima vez el documento
		checkIn->GetPreferencesParametrosOfDB(&PreferencesPara);
		
		
		CopyPage = PreferencesPara.Pagina_To_SavePage;
		CopyPage.Append(".indd");	
		
		
		
	
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			break;
		}

		
		this->LLenar_Combo_DirigidoA();
		
		document->GetName(FrontDocName);
		
		
		//si el nombre del documento actual es diferente al nombre de la ultima pagina modificada
		// los datos se quedan en blanco
		if(CopyPage==FrontDocName) 
		{	
			this->SeleccionarCadenaEnComboBox(PreferencesPara.DirididoA_To_SavePage, kComboBoxDirigidoAWidgetID);
		}
		else
		{	
			this->SeleccionarCadenaEnComboBox(Usuario, kComboBoxDirigidoAWidgetID);
		}
		
			
		//Selecciona el Estado Anterior		
		LLenar_Combo_EstatusPagina(PreferencesPara.Estado_To_SavePage);
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Estado_To_SavePage, kComboBoxStatusWidgetID);
		
		//Selecciona la seccion Anterior
		this->LLenar_Combo_Publicacion(PreferencesPara.Issue_To_SavePage);		
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Issue_To_SavePage, kComboBoxIssueWidgetID);

		
		//Selecciona la seccion Anterior
		this->LLenar_Combo_Seccion(PreferencesPara.Seccion_To_SavePage);	
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Seccion_To_SavePage, kComboBoxSeccionWidgetID);
		
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
				(
					kN2PXMPUtilitiesBoss,	// Object boss/class
					IN2PXMPUtilities::kDefaultIID
				)));
				
		PMString Date = XMPUtils->GetXMPVar("A2PFechaEdicionDPagina");
		if(Date.NumUTF16TextChars()>0)
		{
			Date.Append("/");
			PMString *Anno=Date.GetItem("/",1);
			PMString *MM=Date.GetItem("/",2);
			PMString *DD=Date.GetItem("/",3);
		
			Date="";
			if(MM->NumUTF16TextChars()>0)
			{
				Date.Append(MM->GrabCString());
				Date.Append("/");
			}
			if(DD->NumUTF16TextChars()>0)
			{
				Date.Append(DD->GrabCString());
				Date.Append("/");
			}
			if(DD->NumUTF16TextChars()>0)
			{
				Date.Append(Anno->GrabCString());
			
			}
		}
		else
		{
			Date=InterlasaUtilities::FormatFechaYHoraManana("%m/%d/%Y");
		}
		
		PMString Pagina = XMPUtils->GetXMPVar("A2PFolioDPagina");
		
		if(PreferencesPara.Date_To_SavePage.NumUTF16TextChars()<=0)
		 	SetTextControlData(kDatePubliWidgetID,Date);
		else
		{
			PreferencesPara.Date_To_SavePage.SetTranslatable(kFalse);
			SetTextControlData(kDatePubliWidgetID,PreferencesPara.Date_To_SavePage);
		}
		
		if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars()<=0)
		 	SetTextControlData(kPaginaWidgetID,Pagina);
		else
		{
			PreferencesPara.Pagina_To_SavePage.SetTranslatable(kFalse);
			SetTextControlData(kPaginaWidgetID,PreferencesPara.Pagina_To_SavePage);
		}
		
		/**************************/
		PMString IDPaginaXMP = XMPUtils->GetXMPVar("N2PSQLIDPagina");
			
		IControlView * ComboSeccionView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
			if(ComboSeccionView==nil)
			{
				
				break;
			}
			
		IControlView * ComboPublicacionView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
			if(ComboPublicacionView==nil)
			{
				
				break;
			}
		if(IDPaginaXMP.NumUTF16TextChars()>0)
		{
			if(NomAplicacionActual.Contains("InCopy"))
			{
				ComboSeccionView->Enable(kFalse);
				ComboPublicacionView->Enable(kFalse);
			}
			
			//CAlert::InformationAlert("tum tururtui");
		}
		else
		{
			
			ComboSeccionView->Enable(kTrue);	
			ComboPublicacionView->Enable(kTrue);
		}
		
		//
		LlenarListaDeIssueDePagina();
		
	}while(false);

}

/* ValidateDialogFields
*/
WidgetID GuardarRevDialogController::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID result=kNoInvalidWidgets;
	
	do{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
		
		
		
		PMString NombrePag =  this->GetTextControlData(kPaginaWidgetID);
		if(NombrePag.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert("NombrePag");
			result = kPaginaWidgetID;
			break;
		}
		
		PMString Publicacion =  this->GetTextControlData(kComboBoxIssueWidgetID);
		if(Publicacion.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert("Publicacion");
			result = kN2PsqlComboPubliWidgetID;
			break;
		}
		
		PMString Seccion = this->GetTextControlData(kComboBoxSeccionWidgetID);
		if(Seccion.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert("Seccion");
			result = kN2PsqlComboPubliWidgetID;
			break;
		}
		
		PMString Estatus =this->GetTextControlData(kComboBoxStatusWidgetID);
		if(Estatus.NumUTF16TextChars()<=0 && !NomAplicacionActual.Contains("InCopy"))
		{
			CAlert::InformationAlert("Estatus");
			result = kN2PsqlTextStatusWidgetID;
			break;
		}
	
		PMString DirigidoA =this->GetTextControlData(kComboBoxDirigidoAWidgetID);
		/*if(DirigidoA.NumUTF16TextChars()<=0)
		{
			result = kN2PsqlTextStatusWidgetID;
			break;
		}*/
		
		PMString CreacionDEDocumento = N2PSQLUtilities::GetXMPVar("CreateDate", document);
		if(CreacionDEDocumento.NumUTF16TextChars()<=0)
		{
			CAlert::InformationAlert(kN2PCheckInOutPaginaSinXMPCoreStringKey);
			result=kPaginaWidgetID;
			break;
		}
		
		if(this->CheckInPagina()==kSuccess)
		{
			result=kNoInvalidWidgets;
		}
		else
		{
			result=kPaginaWidgetID;
		}
	}while(false);
	SystemBeep();

	return result;
}

/* ApplyDialogFields
*/
void GuardarRevDialogController::ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId) 
{

}



/**
	Obtiene ID-Usuario del ultimo usuario que a InDesign desde el regEdit 
	para buscar en la base de datos el Nombre del usuario correspondiente al ID que se obtuvo
	y poner el nombre subre su TextEdit
*/
PMString GuardarRevDialogController::Aplicar_Usuario_Actual()
{
	PMString retval="";

	InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs != nil)
		{	
			retval = wrkSpcPrefs->GetIDUserLogedString();
			retval.SetTranslatable(kFalse);
			SetTextControlData(kComboBoxUsuarioWidgetID,retval);
		}
	return(retval);
}



void GuardarRevDialogController::LLenar_Combo_DirigidoA()
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxDirigidoAWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Usuario From Usuario ORDER By Id_Usuario ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		PMString DirigidoA="";
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs != nil)
		{	
			DirigidoA.Append(wrkSpcPrefs->GetDirigidoANota());
		}
		
		int32 Index=dropListData->GetIndex(DirigidoA);
		
		if(Index>0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
		
		
		////////////////////////////
		
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
		(
			kN2PXMPUtilitiesBoss,	// Object boss/class
			IN2PXMPUtilities::kDefaultIID
		)));
		
		PMString IDPaginaXMP = XMPUtils->GetXMPVar("N2PSQLIDPagina");
		
		if(IDPaginaXMP.NumUTF16TextChars()>0)
		{
			QueryVector.clear();
			PMString Busqueda="SELECT Id_Usuario From Bitacora_Pagina WHERE Pagina_ID ="+IDPaginaXMP+" AND Id_Evento IN(5,6,13,31,32) ORDER BY ID DESC LIMIT 1";
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);

			PMString cadena="";
			
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				
				cadena = SQLInterface->ReturnItemContentbyNameColumn("Id_Usuario",QueryVector[i]);
			
			
			
			}
			
			if(cadena.NumUTF16TextChars()>0)
				this->SetTextControlData(kCheckInUltimoUsuarioQModificoWidgetID,cadena);
			
		}
	}while(false);
}



bool16 GuardarRevDialogController::LLenar_Combo_Seccion(PMString Id_SeccionText)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion=(SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + GetTextControlData(kComboBoxIssueWidgetID) + "' ) ORDER BY Nombre_de_Seccion ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_SeccionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//Selecciona el item deseado o la seccion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
		
			
	}while(false);
	return(kTrue);
}

		
bool16 GuardarRevDialogController::LLenar_Combo_Publicacion(PMString Id_PublicacionText)
{
	//kComboBoxIssueWidgetID
	
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Publicacion, Nombre_Publicacion From Publicacion";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia la lista de la consulta
		VectorIdPublicacion = QueryVector;
		
		//Variable para indicar que Publicacion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_PublicacionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[i]);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		//Selecciona el item deseado o la Publicacion deseada
		if(IndexToSelect>=0)
			IDDLDrComboBoxSelecPrefer->Select(IndexToSelect);
			
	}while(false);
	return(kTrue);
}

		
		
PMString GuardarRevDialogController::BuscarEnUsuarios(PMString &Busqueda)
{

	PMString cadena="";
	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Usuario",QueryVector[i]);
		}
		
	}while(false);
		
	
	return(cadena);
}





void GuardarRevDialogController::SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			break;
		}

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			break;
		}

		
		int32 Index=dropListData->GetIndex(CadenaASeleccionar);
		
		if(Index < 0)
		{
			Index=dropListData->Length();
		}
		
		IDDLDrComboBoxSelecPrefer->Select(Index);
		
	}while(false);
}




bool16 GuardarRevDialogController::LLenar_Combo_EstatusPagina(PMString& Id_EstatusActual)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxStatusWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Estatus,Nombre_Estatus From Estatus_Elemento WHERE Id_tipo_ele='2' ORDER BY Nombre_Estatus ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
		
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
			PMString IdEstatusQuery=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[i]);
			if(Id_EstatusActual==IdEstatusQuery)
			{
				Id_EstatusActual=cadena;
			}
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

		
		
		int32 Index=dropListData->GetIndex(Id_EstatusActual);
		
		if(Index>0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
			
	}while(false);
	return(kTrue);
}


int32 GuardarRevDialogController::GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
	
		/*InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
			
		retvalIndex = IDDLDrComboBoxSelecPrefer->GetSelected() 
		*/
	}while(false);
	return(retvalIndex);
}


void GuardarRevDialogController::UserCancelled()
{
	SetTextControlData(kToCheckInOnDBWidgetID,"Cancel");
}

bool16 GuardarRevDialogController::EnableDisableWidget(WidgetID widget,bool16 Enabled)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		if(Enabled)
		{
			ComboCView->Enable(kTrue,kTrue);
		}
		else
		{
			ComboCView->Disable(kTrue);
		}
		
	}while(false);
	return(retval);
}




ErrorCode GuardarRevDialogController::CheckInPagina()
{
	
	//N2PSQLUtilities::ImprimeMensaje("GuardarRevDialogController::CheckInPagina ini");
	ErrorCode errorValue=kFailure;
	IAbortableCmdSeq* seq = nil;
	ErrorCode result = kFailure;
	do
	{	
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			CAlert::InformationAlert("FilActActionComponent::DoSave: document invalid");
			break;
		}
		
		seq = CmdUtils::BeginAbortableCmdSeq();
			
		/*SDKLayoutHelper fersdklayout;
		UIDRef docUIDRefFER = ::GetUIDRef(document);
		if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 40");*/
		
		Aplicacion="";			//Nombre de la aplicacion sobre la que nos encontramos trabajando
		IDUsuario="";			//ID del Usuario que se encuentra trabajando en este momento
		HoraIn="";				//Hora que entro a trabajar el usuario
		FechaIn="";			//Fecha que entro el usuario a trabajar
			
		horaCrea="";			//Datos de la ultima pagina chekineada
		fechaCrea="";
		Seccion="";
		DirigidoA="";
		Estatus="";
		Pagina="";
		Date="";
		Issue="";
		NomAplicacionActual="";
		Busqueda="";
		File="";
		ID_Pagina="0";
		ID_Evento="";
		N2PFolio_Pagina="";
		CadenaSinDosRutas="";
		CopiaToLast="";
		StringConection="";
		IDFile sysFile;
		SDKLayoutHelper helperLayout;
		PMString FileToRemove="";
		PMString RutaPaInCopy="";
			
		//////////Estructura de Preferencias
		PrefParamDialogInOutPage PreferencesPara;
		PreferencesConnection PrefConections;
			
		//////////Interface de Aplicacion
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		NomAplicacionActual = app->GetApplicationName(); //Obtiene el nombre de la aplicacion InDesign/InCopy
		NomAplicacionActual.SetTranslatable(kFalse);
			
		/////////Para Obtener UIDRef del Documento
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 41");
		*/
		UIDRef docUIDRef = ::GetUIDRef(document); //UIDRef del Documento
		IDataBase *db=docUIDRef.GetDataBase();	//DataBase del Documento
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 42");*/
		///
		//db->BeginTransaction();
		
		////////Interface de Utiloidades del Documento
		InterfacePtr<IDocumentUtils> docUtils(GetExecutionContextSession(), IID_IDOCUMENTUTILS);
		if (docUtils == nil)
		{
			CAlert::InformationAlert("FilActActionComponent::DoSave: docUtils invalid");
			break;
		}
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 43");	
		*/		
		///////Obtiene la Interface ICheckInOutSuite para mandar hablar a sus metodos
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
		if(checkIn==nil)
		{
			CAlert::InformationAlert("ICheckInOutSuite");
			break;
		}
			
			
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 44");
		*/
		///////HaceUpdate a la base de datos de las notas que se estaban editando
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 45");
		*/	
		///////////
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
		
		if(N2PCTUtils==nil)
		{
			CAlert::InformationAlert("N2PCTUtils");
			break;
		}	
			
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 46");			
		*/
		////////
		InterfacePtr<IN2PXMPUtilities> XMPUtils(static_cast<IN2PXMPUtilities*> (CreateObject
		(
			kN2PXMPUtilitiesBoss,	// Object boss/class
			IN2PXMPUtilities::kDefaultIID
		)));
				
				
		//Obtiene los datos de registro de usuarios si solo si se encuentra un usuario logeado en la aplicacion
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			CAlert::InformationAlert("Invalid workspace prefs in CheckInOutDialogController::ValidateDialogFields()");
			break;
		}
		
					
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 47");
		*/
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			CAlert::InformationAlert("UpdateStorys");
			break;
		}
		
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 48");			
		*/
		/////
		UpdateStorys->GuardarContenidoNotasEnEdicion(PrefConections);
		//UpdateStorys->CheckInNotasEnEdicion(kFalse);	//hace check in de las notas que se encuentren en edicion sobre el documento actual
			
		
			
		IDUsuario = wrkSpcPrefs->GetIDUserLogedString();	//Obtiene el Id_Usuario que se encuentra logeado actuamente
		Aplicacion.Append(wrkSpcPrefs->GetNameApp());			//Obtiene el nombre de la aplicacion en que se encuentra logeado el usuario
		HoraIn.Append(wrkSpcPrefs->GetHoraIn());				//Obtiene la Hora en se logeo el Usuario
		FechaIn.Append(wrkSpcPrefs->GetFechaIn());				//Obtiene la fecha en que se logeo el usuario
		
		
		
		//Datos de la ultima pagina chekineada que se muestran en el dialogo
		horaCrea = GetTextControlData(kHraCreacionWidgetID);			
		fechaCrea = GetTextControlData(kFechaWidgetID);
			
		DirigidoA = GetTextControlData(kComboBoxDirigidoAWidgetID);
		Estatus = GetTextControlData(kComboBoxStatusWidgetID);
		Pagina = GetTextControlData(kPaginaWidgetID);
		
		Date = GetTextControlData(kDatePubliWidgetID);
		
		MySQLDate = InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Date);
		
			
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 49");
		*/
		Seccion=GetTextControlData(kComboBoxSeccionWidgetID);
		Issue=GetTextControlData(kComboBoxIssueWidgetID);
		
		PMString NombreSeccion=	Seccion;
		PMString NombrePublicacion=Issue;
		
		//Obtiene el index del nombre de la publicacio y de la seccion para despues buscalos en los vectors correcpondientes
		int32 indexOfIssue = this->GetIdexSelectedOfComboBoxWidgetID(kComboBoxIssueWidgetID, Issue);
		if(indexOfIssue>=0 && VectorIdPublicacion.Length()>0 && indexOfIssue< VectorIdPublicacion.Length())
		{
			Issue = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",VectorIdPublicacion[indexOfIssue]);
		}
		
		
		
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 50");
		*/
		int32 indexOfSeccion = this->GetIdexSelectedOfComboBoxWidgetID(kComboBoxSeccionWidgetID, Seccion);
		
		this->LLenar_Combo_Seccion(Seccion);
		this->SeleccionarCadenaEnComboBox(Seccion , kComboBoxSeccionWidgetID);
		
		if(VectorIdSeccion.Length()>0)
		{
			if(indexOfSeccion>=0 && indexOfSeccion<VectorIdSeccion.Length())
			{
				Seccion = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",VectorIdSeccion[indexOfSeccion]);
			}
		}
		
		/////////////////////////////////////////
		////Para revisar que existe la pagina sobre la base de datos y  ademas de eso traernos la ultima ruta con que fue generada
		/////////////////////////////////////////
		K2Vector<PMString> QueryVector;	
		PMString Busqueda="SELECT P.Ruta_Elemento FROM  Pagina P";
				Busqueda.Append("  WHERE  P.ID = ");
				Busqueda.Append(XMPUtils->GetXMPVar("N2PSQLIDPagina"));
				Busqueda.Append("");
		
		/////////////////////////////////////////
			
		PreferencesPara.Estado_To_SavePage = Estatus;
		PreferencesPara.Seccion_To_SavePage = Seccion;
		PreferencesPara.Pagina_To_SavePage = Pagina;
		PreferencesPara.Issue_To_SavePage = Issue;
		PreferencesPara.Date_To_SavePage = Date;
		PreferencesPara.DirididoA_To_SavePage = DirigidoA;
		
		
		PMString N2PFolio_Pagina = N2PSQLUtilities::GetXMPVar("N2PSQLFolioPagina", document);
		PMString N2P_Pagina = N2PSQLUtilities::GetXMPVar("N2PPagina", document);
		
				
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 51");
		*/
		N2PCTUtils->OcultaNotasConFrameOverset();	
		PMString FotoVersionPagina="";
		PMString FotoLastPagina="";
		PMString fecha_Guion="";
					
					
					
		PMString FechaParaRuta = "";//InterlasaUtilities::FormatFechaYHoraManana("%Y%m%d");
		PMString *mm=fechaCrea.GetItem("/",1);
		PMString *dd=fechaCrea.GetItem("/",2);
		PMString *aa=fechaCrea.GetItem("/",3);
		FechaParaRuta=aa->GrabCString();
		FechaParaRuta.Append(mm->GrabCString());
		FechaParaRuta.Append(dd->GrabCString());
					
		/*PMString nnn="Macintosh HD:FEROTROP.indd";
		IDFile MMsysFile=FileUtils::PMStringToSysFile(nnn);
		if(Utils<IDocumentCommands>()->SaveAs(docUIDRef,MMsysFile)!=kSuccess)
		{
			CAlert::InformationAlert("Chin salio");
			break;
		}		*/
		
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 52");
		*/
		if(NomAplicacionActual.Contains("InDesign"))
		{
			if(N2P_Pagina.NumUTF16TextChars()>0)//
			{
				if(N2P_Pagina!=Pagina)//e va a cambiar el nombre del documento
				{
							
					FileToRemove= PrefConections.PathOfServerFile ;
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append( XMPUtils->GetXMPVar("N2PNPubliCreacionPath"));
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append( XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append( XMPUtils->GetXMPVar("N2PUsuarioCredor"));
					SDKUtilities::AppendPathSeparator(FileToRemove);
					FileToRemove.Append( N2P_Pagina );
					FileToRemove.Append(".indd");

							
					ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
					fecha_Guion= PreferencesPara.Date_To_SavePage;
					SDKUtilities::Replace(fecha_Guion,"/","-");
					SDKUtilities::Replace(fecha_Guion,"/","-");
				
					//Arma la Ruta para el Documento InDesign
					File= PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(File);
					File.Append("apaginas");
					SDKUtilities::AppendPathSeparator(File);
					File.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(File);
					File.Append(FechaParaRuta);
					SDKUtilities::AppendPathSeparator(File);
					File.Append( NombrePublicacion);
					SDKUtilities::AppendPathSeparator(File);
					File.Append( NombreSeccion );
					SDKUtilities::AppendPathSeparator(File);
					File.Append( IDUsuario);
					SDKUtilities::AppendPathSeparator(File);
					File.Append( Pagina );
					File.Append(".indd");
				
					if(NomAplicacionActual.Contains("InDesign"))//si nos encontramos en InDesign
					{
						sysFile=FileUtils::PMStringToSysFile(File);
						if(FileUtils::DoesFileExist(sysFile))
						{
							CAlert::InformationAlert(kN2PInOutYaExistePaginaConEsteFolioStringAlert);
							break;
						}
									
					}	
							
					//Arma la ruta para os ducumento de Versiones y fotos o previews
					FotoVersionPagina=PrefConections.PathOfServerFile ;
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(FechaParaRuta);
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(NombrePublicacion); 
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(NombreSeccion);
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(IDUsuario);
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							
					FotoLastPagina=PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(FechaParaRuta );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(NombrePublicacion );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(NombreSeccion );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(IDUsuario);
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
						

					FotoVersionPagina.Append(PreferencesPara.Pagina_To_SavePage);
					FotoLastPagina.Append(PreferencesPara.Pagina_To_SavePage);
					checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
				
				
					//Para ruta de copia de pagina
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
				/*	PAthToCopyDocumentBackUp.Append("News2Page");
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(NombrePublicacion);
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(FechaParaRuta);
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(NombreSeccion);
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(IDUsuario);
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(PreferencesPara.Pagina_To_SavePage +".indd");
				*/
					//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage);
				
					//Obtiene Folio del XMP si es que existe
			
					CadenaSinDosRutas=FotoVersionPagina;
					//Remueve todo hasta a paginas
					if(CadenaSinDosRutas.NumUTF16TextChars()>0)
					{
						int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
						if(indexapaginas>0)
							CadenaSinDosRutas.Remove(0, indexapaginas+9);
					}
							
					CopiaToLast=FotoLastPagina;
					CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
					CopiaToLast.Append("_last.jpg");
					fechaCrea.Append(" " + horaCrea);
							
					//Quiere Decir que es el Primer CheckIn
			
					//Si es en InDesign significa que si se cambio el nombre desde inDesign
					//Si es inCopy significa que cambiaron el nombre desde InDesign por lo tanto solo se actualiza datos iod evento 15
					if(NomAplicacionActual.Contains("InDesign"))
						ID_Evento="31";
											
					//Si Es el Primer CheckIn
				
					//Arma Folio
					N2PFolio_Pagina = PreferencesPara.Pagina_To_SavePage;
					N2PFolio_Pagina.Append("_" + PreferencesPara.Seccion_To_SavePage + "_" + fecha_Guion);
				}
				else
				{//Hace un save normal
				
					fecha_Guion = XMPUtils->GetXMPVar("N2PDate");
					ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
				
					SDKUtilities::Replace(fecha_Guion,"/","-");
					SDKUtilities::Replace(fecha_Guion,"/","-");
									
					//Arma la Ruta para el Documento InDesign
					//Arma la Ruta para el Documento InDesign
					File= PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(File);
					File.Append("apaginas");
					SDKUtilities::AppendPathSeparator(File);
					File.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(File);
					File.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( XMPUtils->GetXMPVar("N2PNPubliCreacionPath"));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
					SDKUtilities::AppendPathSeparator(File);
					File.Append( XMPUtils->GetXMPVar("N2PUsuarioCredor"));
					SDKUtilities::AppendPathSeparator(File);
					File.Append( PreferencesPara.Pagina_To_SavePage  );
					File.Append(".indd");
				
					//Arma la ruta para os ducumento de Versiones y fotos o previews
							
					FotoVersionPagina=PrefConections.PathOfServerFile ;
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath")); 
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath"));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
					SDKUtilities::AppendPathSeparator(FotoVersionPagina);
					FotoLastPagina=PrefConections.PathOfServerFile;
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("apaginas");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append("N2PSQL");
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
					SDKUtilities::AppendPathSeparator(FotoLastPagina);
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
					SDKUtilities::AppendPathSeparator(FotoLastPagina);

					FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
					FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
					checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));	//Toma Foto de paginas para obtener el Preview
				
				
					//Para ruta de copia de pagina
					/*SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append("News2Page");
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				
					SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
					PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PPagina")+".indd");*/
				
					//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));
							
					//Obtiene Folio del XMP si es que existe
			
					CadenaSinDosRutas=FotoVersionPagina;
					//Remueve todo hasta a paginas
					if(CadenaSinDosRutas.NumUTF16TextChars()>0)
					{
						int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
						if(indexapaginas>0)
							CadenaSinDosRutas.Remove(0, indexapaginas+9);
					}
							
					CopiaToLast=FotoLastPagina;
					CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
					CopiaToLast.Append("_last.jpg");
					fechaCrea.Append(" " + horaCrea);
							
					if(NomAplicacionActual.Contains("InDesign"))
						ID_Evento="7";
					
					
				}
			
			
				/*///////////////////////
				///Se Pregunta aqui pues se supone que es una pagina ya adiconada al sistema
				SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
				
				//CAlert::InformationAlert(Busqueda);
			
				if(QueryVector.Length()>0)
				{
					//CAlert::InformationAlert(QueryVector[QueryVector.Length()-1]);
					RutaPaInCopy =  SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.Length()-1]);
				}
				else
				{
					CAlert::InformationAlert("Error 101 no se encontraron datos para el id de pagina especificado");
					break;
				}*/
			}
			else
			{
				//CAlert::InformationAlert("No existe un nombre para esta pagina en XMP por lo tanto se supone que es una pagina nueva");
				fecha_Guion= PreferencesPara.Date_To_SavePage;
				SDKUtilities::Replace(fecha_Guion,"/","-");
				SDKUtilities::Replace(fecha_Guion,"/","-");
			
				//Arma la Ruta para el Documento InDesign
				File= PrefConections.PathOfServerFile;
				SDKUtilities::AppendPathSeparator(File);
				File.Append("apaginas");
				SDKUtilities::AppendPathSeparator(File);
				File.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(File);
				File.Append(FechaParaRuta);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( NombrePublicacion);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( NombreSeccion );
				SDKUtilities::AppendPathSeparator(File);
				File.Append( IDUsuario);
				SDKUtilities::AppendPathSeparator(File);
				File.Append( PreferencesPara.Pagina_To_SavePage );
				File.Append(".indd");
				
				//Verifica que no exista ningun archivo fisico con este mismo nombre sobre la ruta establecida
				sysFile=FileUtils::PMStringToSysFile(File);
				if(FileUtils::DoesFileExist(sysFile))
				{
					CAlert::InformationAlert(kN2PsqlYaExistePaginaConEsteNombreStringKey);
					break; 
				}

				//Arma la ruta para os ducumento de Versiones y fotos o previews
				
				FotoVersionPagina=PrefConections.PathOfServerFile ;
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(FechaParaRuta);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(NombrePublicacion); 
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(NombreSeccion);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				FotoVersionPagina.Append(IDUsuario);
				SDKUtilities::AppendPathSeparator(FotoVersionPagina);

				FotoLastPagina=PrefConections.PathOfServerFile;
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append("apaginas");
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append("N2PSQL");
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(FechaParaRuta );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(NombrePublicacion );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(NombreSeccion );
				SDKUtilities::AppendPathSeparator(FotoLastPagina);
				FotoLastPagina.Append(IDUsuario);
				SDKUtilities::AppendPathSeparator(FotoLastPagina);

				FotoVersionPagina.Append(PreferencesPara.Pagina_To_SavePage);
				FotoLastPagina.Append(PreferencesPara.Pagina_To_SavePage);
				checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
			
			
				//Para ruta de copia de pagina
				/*SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append("News2Page");
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(NombrePublicacion );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(FechaParaRuta );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(NombreSeccion );
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(IDUsuario);
				
				SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
				PAthToCopyDocumentBackUp.Append(PreferencesPara.Pagina_To_SavePage+".indd");
			*/
			
				//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,PreferencesPara.Pagina_To_SavePage); //Toma Foto de paginas para obtener el Preview
				
				//Obtiene Folio del XMP si es que existe
			
				CadenaSinDosRutas=FotoVersionPagina;
					
				//Remueve todo hasta a paginas
				if(CadenaSinDosRutas.NumUTF16TextChars()>0)
				{
					int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
					if(indexapaginas>0)
						CadenaSinDosRutas.Remove(0, indexapaginas+9);
				}
						
				CopiaToLast=FotoLastPagina;
				CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
				CopiaToLast.Append("_last.jpg");
				fechaCrea.Append(" " + horaCrea);
						
				//Quiere Decir que es el Primer CheckIn
						
				ID_Evento="8";
						
				//Si Es el Primer CheckIn
				
				//Arma Folio
				N2PFolio_Pagina = PreferencesPara.Pagina_To_SavePage;
				N2PFolio_Pagina.Append("_" + PreferencesPara.Seccion_To_SavePage + "_" + fecha_Guion);
			}
			
			/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
				CAlert::InformationAlert("FerCanSaveDocumentAs 52");
			 */
		}
		else
		{	//Hace un save normal en InCopy
			//Pues se supone que unicamente deposita
			fecha_Guion = XMPUtils->GetXMPVar("N2PDate");
			ID_Pagina = XMPUtils->GetXMPVar("N2PSQLIDPagina");
				
			SDKUtilities::Replace(fecha_Guion,"/","-");
			SDKUtilities::Replace(fecha_Guion,"/","-");
									
			//Arma la Ruta para el Documento InDesign
			//Arma la Ruta para el Documento InDesign
			File= PrefConections.PathOfServerFile;
			SDKUtilities::AppendPathSeparator(File);
			File.Append("apaginas");
			SDKUtilities::AppendPathSeparator(File);
			File.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(File);
			File.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
			SDKUtilities::AppendPathSeparator(File);
			File.Append( XMPUtils->GetXMPVar("N2PNPubliCreacionPath"));
			SDKUtilities::AppendPathSeparator(File);
			File.Append( XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
			SDKUtilities::AppendPathSeparator(File);
			File.Append( XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				SDKUtilities::AppendPathSeparator(File);
			File.Append( PreferencesPara.Pagina_To_SavePage  );
			File.Append(".indd");
			
			//Arma la ruta para os ducumento de Versiones y fotos o previews
							
			FotoVersionPagina=PrefConections.PathOfServerFile ;
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append("apaginas");
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath"));
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath")); 
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath"));
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
			SDKUtilities::AppendPathSeparator(FotoVersionPagina);
			FotoLastPagina=PrefConections.PathOfServerFile;
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append("apaginas");
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append("N2PSQL");
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
			SDKUtilities::AppendPathSeparator(FotoLastPagina);
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
			SDKUtilities::AppendPathSeparator(FotoLastPagina);

			FotoVersionPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
			FotoLastPagina.Append(XMPUtils->GetXMPVar("N2PPagina"));
			checkIn->TomarFotosDeDocumento(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));	//Toma Foto de paginas para obtener el Preview
				
				
			//Para ruta de copia de pagina
			/*SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append("News2Page");
			
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNPubliCreacionPath") );
				
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PFechaCreacionPath") );
			
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PNSecCreacionPath") );
				
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PUsuarioCredor"));
				
			SDKUtilities::AppendPathSeparator(PAthToCopyDocumentBackUp);
			PAthToCopyDocumentBackUp.Append(XMPUtils->GetXMPVar("N2PPagina")+".indd");
			*/	
			//checkIn->TomarFotosDeSpread(FotoVersionPagina,FotoLastPagina,XMPUtils->GetXMPVar("N2PPagina"));
							
			//Obtiene Folio del XMP si es que existe
			
			CadenaSinDosRutas=FotoVersionPagina;
			//Remueve todo hasta a paginas
			if(CadenaSinDosRutas.NumUTF16TextChars()>0)
			{
				int32 indexapaginas=CadenaSinDosRutas.IndexOfString("apaginas");
				if(indexapaginas>0)
					CadenaSinDosRutas.Remove(0, indexapaginas+9);
			}
							
			CopiaToLast=FotoLastPagina;
			CopiaToLast.Remove( CopiaToLast.LastIndexOfWChar(95) , CopiaToLast.NumUTF16TextChars()-CopiaToLast.LastIndexOfWChar(95) );
			CopiaToLast.Append("_last.jpg");
			fechaCrea.Append(" " + horaCrea);
						
			ID_Evento="15";
			
			///////////////////////
			///Se Pregunta aqui pues se supone que es una pagina ya adiconada al sistema
			SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
				
			//CAlert::InformationAlert(Busqueda);
			
			if(QueryVector.Length()>0)
			{
				//CAlert::InformationAlert(QueryVector[QueryVector.Length()-1]);
				RutaPaInCopy =  SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.Length()-1]);
			}
			else
			{
				CAlert::InformationAlert("Error 101 no se encontraron datos para el id de pagina especificado");
				break;
			}
					
		}
		
/////////////////////
			
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 54");
		*/
		 PMString SPCallPaginnaChickIn="CALL PaginaCheckInStoreProc(";
		SPCallPaginnaChickIn.Append(ID_Pagina);
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append(PreferencesPara.Issue_To_SavePage);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(MySQLDate);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Seccion_To_SavePage);
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(N2PFolio_Pagina);
		SPCallPaginnaChickIn.Append("',");
		SPCallPaginnaChickIn.Append("1");	//PLIEGO
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("0");	//PAR/IMPAR
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("1");	//ID-Color
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append(PreferencesPara.DirididoA_To_SavePage);	
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(IDUsuario);	//Proveniente_de
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("SQLServer");	//Servidor
		SPCallPaginnaChickIn.Append("','");
		if(NomAplicacionActual.Contains("InCopy"))
		{
			PMString CadenaToWindows=RutaPaInCopy;
			#ifdef WINDOWS
				PMString Diagonal="";
				Diagonal.AppendW(92);
				PMString DDiagonal="";
				DDiagonal.Append("\\\\");
				N2PSQLUtilities::ReplaceAllOcurrencias(CadenaToWindows,Diagonal,DDiagonal);
			#endif
			SPCallPaginnaChickIn.Append(CadenaToWindows);
		}
		else
		{
			/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
				CAlert::InformationAlert("FerCanSaveDocumentAs 55");*/
			PMString CadenaToWindows=File;
			#ifdef WINDOWS
				PMString Diagonal="";
				Diagonal.AppendW(92);
				PMString DDiagonal="";
				DDiagonal.Append("\\\\");
				N2PSQLUtilities::ReplaceAllOcurrencias(CadenaToWindows,Diagonal,DDiagonal);
			#endif
			SPCallPaginnaChickIn.Append(CadenaToWindows);	//Ruta_Elemento 
		}
		
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Pagina_To_SavePage);	//Nombre_Archivo 
		SPCallPaginnaChickIn.Append("',");
		SPCallPaginnaChickIn.Append("@A");	//Fecha_Creacion
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("@X");	//Fecha_Ult_Mod
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("0");	//Webable
		SPCallPaginnaChickIn.Append(",");
		SPCallPaginnaChickIn.Append("10");	//Calificacion
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append("Pagina");	//Id_TipoElemento
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(PreferencesPara.Estado_To_SavePage);	//Id_Estatus
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("No se que es Cameo");	//Cameo
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append(IDUsuario);	//Id_Empleado
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("0");	//Fech_Evento
		SPCallPaginnaChickIn.Append("',");
				
		SPCallPaginnaChickIn.Append(ID_Evento);	//Id_Evento
				
		SPCallPaginnaChickIn.Append(",'");
		SPCallPaginnaChickIn.Append(CadenaSinDosRutas);	//Ruta_Previo
		SPCallPaginnaChickIn.Append("','");
		SPCallPaginnaChickIn.Append("Check In");	//Descripcion_Evento
		SPCallPaginnaChickIn.Append("','");
				
		if(NomAplicacionActual.Contains("InCopy"))
			SPCallPaginnaChickIn.Append(RutaPaInCopy);
		else
			SPCallPaginnaChickIn.Append(CopiaToLast);	//Descripcion_Evento
		SPCallPaginnaChickIn.Append("');");
				
				
		//CAlert::InformationAlert(StringConection);	
		//CAlert::InformationAlert(SPCallPaginnaChickIn);	
			
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 56");
		*/
		UpdateStorys->GuardarQuery(SPCallPaginnaChickIn);
					
		QueryVector.clear();	
					
		SQLInterface->SQLQueryDataBase(StringConection,SPCallPaginnaChickIn,QueryVector);
					
		if(QueryVector.Length()<0)
		{
			CAlert::InformationAlert("Hubo un error y se va a salir del ciclo");
			break;
		}
		else
		{
			PMString cadena = SQLInterface->ReturnItemContentbyNameColumn("Fecha_CreacionIN",QueryVector[0]);
			PMString Resultado = SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[0]);
			ID_Pagina = SQLInterface->ReturnItemContentbyNameColumn("Pagina_IDX",QueryVector[0]);
			//CAlert::InformationAlert(ID_Pagina);
			if(Resultado=="9")
			{
				CAlert::InformationAlert(kN2PsqlYaExistePaginaConEsteNombreStringKey);	
				break;
			}
			else
			{	if(cadena=="SinPrivilegios" || cadena.NumUTF16TextChars()<=0)
				{
					CAlert::InformationAlert(kN2PsqlSinPermisosToCheckinPageStringKey);		
					break;
				}
							
			}
						
		}
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 57");		
		*/
		//CAlert::InformationAlert("8");	
			
		if(PreferencesPara.Date_To_SavePage!=XMPUtils->GetXMPVar("N2PDate"))
		{
			XMPUtils->SaveXMPVar("N2PDate",PreferencesPara.Date_To_SavePage);
		}
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 58");
*/
		if( ID_Evento=="8"|| ID_Evento=="31")
		{
			//XMPUtils->CreatePropertiesN2P(document);
			XMPUtils->SaveXMPVar("N2PPublicacion",PreferencesPara.Issue_To_SavePage);
			XMPUtils->SaveXMPVar("N2PAplicacion",NomAplicacionActual);
			XMPUtils->SaveXMPVar("N2PEstatus",PreferencesPara.Estado_To_SavePage);
			XMPUtils->SaveXMPVar("N2PDirigidoA",PreferencesPara.DirididoA_To_SavePage);
			XMPUtils->SaveXMPVar("N2PSeccion",PreferencesPara.Seccion_To_SavePage);
			XMPUtils->SaveXMPVar("N2PPagina",PreferencesPara.Pagina_To_SavePage);
			XMPUtils->SaveXMPVar("N2PIssue",PreferencesPara.Issue_To_SavePage);
			XMPUtils->SaveXMPVar("N2PFechaCreacionPath",FechaParaRuta);
			XMPUtils->SaveXMPVar("N2PUsuarioCredor",IDUsuario);
			XMPUtils->SaveXMPVar("N2PNPubliCreacionPath",NombrePublicacion);
			XMPUtils->SaveXMPVar("N2PNSecCreacionPath",NombreSeccion);
			//Poner la fecha actual
			if(PreferencesPara.Date_To_SavePage.NumUTF16TextChars()==0)
				XMPUtils->SaveXMPVar("N2PDate","00/00/0000");
			else
				XMPUtils->SaveXMPVar("N2PDate",PreferencesPara.Date_To_SavePage);
			XMPUtils->SaveXMPVar("N2PSQLFolioPagina",N2PFolio_Pagina);
			XMPUtils->SaveXMPVar("N2PSQLIDPagina",ID_Pagina);
			XMPUtils->SaveXMPVar("N2PSQLFolioPagina",N2PFolio_Pagina);
		}
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 59");
	*/
		if(NomAplicacionActual.Contains("InDesign"))//si nos encontramos en InDesign
		{
			//CAlert::InformationAlert(File);
			sysFile=FileUtils::PMStringToSysFile(File);
			if(FileUtils::DoesFileExist(sysFile))
			{
				Utils<IDocumentUtils>()->DoSave(document); 
			}
			else
			{
				//Utils<IDocumentCommands>()->SaveAs(docUIDRef,sysFile);
				if(helperLayout.SaveDocumentAs(docUIDRef,sysFile,kFullUI)!=kSuccess)
				{
					CAlert::InformationAlert("helperLayout");
					break;
				}
			}
							
		}
		
		/*if(fersdklayout.FerCanSaveDocumentAs(docUIDRefFER)==kFailure)
			CAlert::InformationAlert("FerCanSaveDocumentAs 60");
					
		*/

		if(ID_Evento=="31")
		{	
			
			//CAlert::InformationAlert("Remueve anterior Pagina"+FileToRemove);
			if ( remove(InterlasaUtilities::MacToUnix(FileToRemove).GrabCString()) != 0 )
			{
				//CAlert::InformationAlert("No pudo Borrar la anterior Pagina "+ InterlasaUtilities::MacToUnix(FileToRemove));
				//ASSERT_FAIL("Error al intentar borrar file "+FileToRemove);			
			}
			
		}
				
			
		//Obtiene preferencias de Check in en documento de preferencias"ParametrosDeDialogosN2PSQL.pfd"
		checkIn->GetPreferencesParametrosOfFile(&PreferencesPara);
			
		PreferencesPara.Estado_To_SavePage=Estatus;
		PreferencesPara.Seccion_To_SavePage=Seccion;
		PreferencesPara.Pagina_To_SavePage=Pagina;
		PreferencesPara.Issue_To_SavePage=Issue;
		PreferencesPara.Date_To_SavePage=Date;
		PreferencesPara.DirididoA_To_SavePage=DirigidoA;
		//Guarda preferencias de Check in en documento de preferencias "ParametrosDeDialogosN2PSQL.pfd"
		checkIn->SetPreferencesParametros(&PreferencesPara);
		
		
		//Agregar Todos los issues a la pagina
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			break;
		}
		
		IControlView *iControViewList = panelControlData->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		if(iControViewList==nil)
		{
			CAlert::InformationAlert("ControlView");
			break;
		}
		
		SDKListBoxHelper listHelper(iControViewList, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		
		for(int32 n=0;n<listHelper.GetElementCount();n++){
			
			PMString Publicacion="";
			PMString SeccionS="";
			listHelper.ObtenerTextoDeSeleccionActual(kPaginaLabelWidgetID,Publicacion,kSeccionLabelWidgetID,SeccionS,n);
			
			this->agregarIssuesAndSection( Publicacion,  SeccionS);
			//CAlert::InformationAlert(Id_PaginaSel+","+Seccion);
		}
		errorValue=kSuccess;
		
	}while(false);
	
	if(errorValue==kFailure)
	{
		//Proboca que se queden fisicamente abiertas las notas
		//CmdUtils::RollBackCommandSequence(seq);
		//CmdUtils::AbortCommandSequence(seq);
		CmdUtils::EndCommandSequence(seq);
	}
	else
	{
		CmdUtils::EndCommandSequence(seq);
	}
	seq=nil;
	
	//N2PSQLUtilities::ImprimeMensaje("GuardarRevDialogController::CheckInPagina fin");
	return errorValue;
}


bool16 GuardarRevDialogController::agregarIssuesAndSection(PMString Issue, PMString Seccion)
{
	do{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		PMString IDUsuario_Actual="";
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in CheckInOutAsignaIssueObserver::Update()");
			break;
		}
		IDUsuario_Actual= wrkSpcPrefs->GetIDUserLogedString();
		
		
		
		////////////////////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		
		PMString SPString("CALL SP_addIssueAndSecctionToPage('"+Issue+"','"+Seccion+"',"+IDPaginaENXMP+",'"+IDUsuario_Actual+"');");
		K2Vector<PMString> QueryVector;
		
		PMString numerror="";
		PMString stringOUT="";
		PMString idsalida="";
		
		if(SQLInterface->SQLQueryDataBase(StringConection,SPString,QueryVector))
		{
			
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				idsalida=SQLInterface->ReturnItemContentbyNameColumn("idsalida",QueryVector[i]);
				stringOUT=SQLInterface->ReturnItemContentbyNameColumn("stringOUT",QueryVector[i]);
				idsalida=SQLInterface->ReturnItemContentbyNameColumn("idsalida",QueryVector[i]);
			}
		}
		
		/*if(numerror.GetAsNumber()>0)
		{
			CAlert::InformationAlert(stringOUT);
		}
		else
		{
			LlenarListaDeIssueDePagina();
		}
		 */
		
	}while (false);
	return kFalse;
}

bool16 GuardarRevDialogController::LlenarListaDeIssueDePagina()
{
	do{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if (panel == nil)
		{
			
			break;
		}		
		
		IControlView *iControViewList = panel->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		if(iControViewList==nil)
		{
			CAlert::InformationAlert("ControlView");
			break;
		}		
		
		
		SDKListBoxHelper listHelper(iControViewList, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		listHelper.EmptyCurrentListBox();	
		
		if(IDPaginaENXMP.NumUTF16TextChars()<=0)
			break;
		
		PMString Busqueda="SELECT a.id as id, p.Nombre_Publicacion as publicacion, s.Nombre_de_Seccion as seccion ";
		Busqueda.Append(	  "	FROM edicionesdpagina a LEFT JOIN publicacion p ON p.Id_Publicacion=publicacion_idpublicacion");
		Busqueda.Append(	  " LEFT JOIN seccion s ON s.Id_Seccion=a.seccion_id_seccion WHERE pagina_id="+IDPaginaENXMP+ " AND a.estatus<90 ");
		
		//CAlert::InformationAlert(Busqueda);
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
		K2Vector<PMString> QueryVector;
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
		if(SQLInterface==nil)
		{
			ASSERT_FAIL("SQLInterface = nil");
			break;
		}
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		if(QueryVector.Length()<=0)
		{
			PMString NoNotas=PMString(kN2PCheckInOutNoSeEncontraronPaginasPlugInMenuKey);
			NoNotas.Translate();
			NoNotas.SetTranslatable(kTrue);
			CAlert::InformationAlert(NoNotas);
			break;
		}
		
		// cuando regresa solo un registro
		if(QueryVector.Length()==1)
		{
			
			//CAlert::InformationAlert(QueryVector[0]);
			PMString Privilegio = SQLInterface->ReturnItemContentbyNameColumn("Privilegio",QueryVector[0]);
			if(Privilegio.NumUTF16TextChars()>0)//Pregunta el rtegitro trae el campo con el nombre 'Privilegio'
			{
				//entonces no tiene privilegios para ver las paginas de esta seccion
				CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
			}
			else
			{// si no entonces es una regirtro con datos de una pagina
				for(int32 i=0;i<QueryVector.Length();i++)
				{
					PMString Nombre_Archivo = SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryVector[i]);
					PMString Secc = SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryVector[i]);
					PMString Estado="";// SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
					PMString Folio_Pagina = SQLInterface->ReturnItemContentbyNameColumn("id",QueryVector[i]);
					PMString Id_SeccionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
					PMString Id_PublicacionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
					listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
				}
			}
		}
		else
		{//si no entonces trae muchas registeros de paginas.
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				PMString Nombre_Archivo=SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryVector[i]);
				PMString Secc=SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryVector[i]);
				PMString Estado="";//SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
				PMString Folio_Pagina=SQLInterface->ReturnItemContentbyNameColumn("id",QueryVector[i]);
				PMString Id_SeccionText="";//SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
				PMString Id_PublicacionText="";//SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
				listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
			}
		}
	}while (false);
	return kTrue;
}