
//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/paneltreeview/N2PLogInTask.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: ipaterso $
//  
//  $DateTime: 2005/02/25 04:54:14 $
//  
//  $Revision: #6 $
//  
//  $Change: 320645 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// interface includes
#include "IApplication.h"
#include "IMenuManager.h"
#include "IActionManager.h"
#include "IIdleTaskMgr.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IBoolData.h"
#include "CAlert.h"

#include "Utils.h"
#include "IInCopyDocUtils.h"
#include "ILayoutUIUtils.h"
#include "LayoutUIUtils.h"
// General includes:

#include "CIdleTask.h"

//
#include "N2PInOutID.h"
#include "N2PsqlLogInLogOutUtilities.h"


//#include "..\..\..\source\Interlasa\N2PMessenger\IN2PMSGDynPnPanelManager.h"

const static int kN2PLogInInterval = 10*500;	// Refresh every 5 minutes in case we add new images
// milliseconds between checks

/** Implements IIdleTask, to refresh the view onto the file system.
	Just lets us keep up to date when people add new assets.
	This implementation isn't too respectful; it just clears the tree and calls ChangeRoot.

	
	@ingroup paneltreeview
*/

class N2PLogInTask : public CIdleTask
{
public:

	/**	Constructor
		@param boss boss object on which this interface is aggregated
	*/
	N2PLogInTask(IPMUnknown* boss);

	/**	Destructor
	*/
	virtual ~N2PLogInTask() {}


	/**	Called by the idle task manager in the app core when the task is running
		@param appFlags [IN] specifies set of flags which task can check to see if it should do something
			this time round
		@param timeCheck [IN] specifies how many milliseconds before the next active task becomes overdue.
		@return uint32 giving the interval (msec) that should elapse before calling this back
	 */
	virtual uint32 RunTask(uint32 appFlags, IdleTimer* timeCheck);

	/**	Get name of task
		@return const char* name of task
	 */
	virtual const char* TaskName();
protected:

	/**	Update the treeview.
	 */
	void refresh();
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2PLogInTask, kN2PLogInTaskImpl)

/* Constructor
*/
N2PLogInTask::N2PLogInTask(IPMUnknown *boss)
	:CIdleTask(boss)
{
}


/* RunTask
*/
uint32 N2PLogInTask::RunTask(
	uint32 appFlags, IdleTimer* timeCheck)
{
	if( appFlags & 
		( IIdleTaskMgr::kMouseTracking 
		| IIdleTaskMgr::kUserActive 
		| IIdleTaskMgr::kInBackground 
		| IIdleTaskMgr::kMenuUp))
	{
		return kOnFlagChange;
	}
	
	this->refresh();

	// Has FS changed?
	return kN2PLogInInterval;
}


/* TaskName
*/
const char* N2PLogInTask::TaskName()
{
	return kN2PLogInTaskKey;
}


/* refresh
*/
void N2PLogInTask::refresh()
{
	do
	{
		/*PARA EL MENSAJE DE NEWS2PAGE
			InterfacePtr< IN2PMSGDynPnPanelManager> dynPanelManager( IN2PMSGDynPnPanelManager::QueryDynamicPanelManager());
			ASSERT(dynPanelManager);
			if(!dynPanelManager) 
			{
				CAlert::InformationAlert("dynPanelManager");
				break;
			}
			int32 dynPanelCount = dynPanelManager->DestroyALL();
		*/
		N2PsqlLogInLogOutUtilities::DoLogInUser();
		this->UninstallTask();
		
		
	} while(kFalse);
}

//	end, File:	N2PLogInTask.cpp
