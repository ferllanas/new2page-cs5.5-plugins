/*
//	$File: //depot/devtech/sdkanna/utilities/N2PSQLListPasesBoxHelper.h $
// 
//	Owner:	Ian Paterson
//
//	$Author: ipaterso $
//
//	$DateTime: 2002/10/16 08:12:04 $
//
//	$Revision: #5 $
//
//	$Change: 163058 $
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2002 Adobe Systems Incorporated. All rights reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
//	Purpose:
//
*/
#ifndef __N2PSQLListBoxHelper_h__
#define __N2PSQLListBoxHelper_h__

class IPMUnknown;
class IControlView;

/**
	Encapsulates detail of working with the list-box API. 
	@author Ian Paterson
*/
class N2PSQLListPasesBoxHelper
{
public:
	/**
		Constructor.
		@param fOwner reference to boss object using this helper.
		@param pluginId Identifier of plug-in that resources can be found in.
	*/
	N2PSQLListPasesBoxHelper(IPMUnknown * fOwner, int32 pluginId,WidgetID widget);
	virtual ~N2PSQLListPasesBoxHelper();

	/**
		Add an element with the specified name at a given location in the listbox.
		@param displayName string value
		@param updateWidgetId the text widget ID within the cell
		@param atIndex specify the location, by default, the end of the list-box.
	*/
	virtual void AddElement( const PMString & displayName,   WidgetID updateWidgetId, int atIndex = -2 /* kEnd */);


	/**
	
	virtual void AddElementListPag( const PMString & PagName,const PMString & SeccionName,const PMString & EstadoName, WidgetID updateWidgetId, int atIndex=-2);
*/
	/**
		Method to remove a list-box element by position.
		@param indexRemove position of element to remove.
	*/
	virtual void RemoveElementAt(int indexRemove);
	
	/**
		Method to remove the last element in the list-box.
	*/
	virtual void RemoveLastElement();

	/**
		Query for the list-box on the panel that is currently visible, assumes one visible at a time.
		@return reference to the current listbox, not add-ref'd.
	*/
	virtual IControlView * FindCurrentListBox();

	/**
		Method to delete all the elements in the list-box.
	*/
	virtual void EmptyCurrentListBox();

	/**
		Accessor for the number of elements in encapsulated list-box.
	*/
	virtual int GetElementCount();

	virtual void ObtenerTextoDeSeleccionActual( WidgetID widget1,PMString &Text1,WidgetID widget2,PMString &Text2,int32 indexSelected);

	virtual void Mostrar_IconFluido(int32 indexSelected);

	void SetTextoEnSeleccionActual( WidgetID widget1, PMString &Text1, int32 indexSelected);

	void GetTextoEnSeleccionActual( WidgetID widget1, PMString &Text1, int32 indexSelected);
	
protected:
	// impl methods
	bool16 verifyState() { return (fOwner!=nil) ? kTrue : kFalse; }
	/**
		helper method to a new list element widget.
	*/
	void addListElementWidget(InterfacePtr<IControlView> & elView, const PMString & displayName, WidgetID updateWidgetId, int atIndex);

	/**
	*/
	void addListPagElementWidget(InterfacePtr<IControlView> & elView, const PMString & PagName,const PMString & SeccionName,const PMString & EstadoName, WidgetID updateWidgetId, int atIndex);

	/**
		Helper method to remove the
	*/
	void removeCellWidget(IControlView * listBox, int removeIndex);
	
	

	IPMUnknown * fOwner;
	int32 fOwnerPluginID;
	WidgetID fwidget;
};


#endif // __N2PSQLListBoxHelper_h__



