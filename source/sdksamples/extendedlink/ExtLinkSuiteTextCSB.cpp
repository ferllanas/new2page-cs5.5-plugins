//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkSuiteTextCSB.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ITextTarget.h"
#include "ITextModel.h"
#include "IActiveContext.h"
#include "ITextSelectionSuite.h"

// General includes:
#include "CPMUnknown.h"
#include "Utils.h"
#include "K2Vector.tpp"
#include "SelectionExtTemplates.tpp"

// Project includes:
#include "ExtLinkID.h"
#include "IExtLinkSuite.h"
#include "IExtLinkFacade.h"

/** Text CSB for IExtLinkSuite, we only support Text CSB, to insert the databse data from ExtLink, a text selection is required.

	@ingroup extendedlink
	@see IExtLinkSuite
*/
class ExtLinkSuiteTextCSB : public CPMUnknown<IExtLinkSuite>
{
public:
	/**
		Constructor.
		@param boss interface ptr from boss object on 
		which this interface is aggregated.
	*/
	ExtLinkSuiteTextCSB(IPMUnknown* boss);

	/** See IExtLinkSuite.
	*/
	virtual bool16 CanInsertData() const;

	/**	See IExtLinkSuite.
	*/
	virtual ErrorCode InsertSelectedRecords(const URIList& uriList);

};

/* Makes the implementation available to the application.
*/
CREATE_PMINTERFACE(ExtLinkSuiteTextCSB, kExtLinkSuiteTextCSBImpl)

/* Constructor
*/
ExtLinkSuiteTextCSB::ExtLinkSuiteTextCSB(IPMUnknown* boss) :
	CPMUnknown<IExtLinkSuite>(boss)
{
}



/*
*/
bool16 ExtLinkSuiteTextCSB::CanInsertData() const
{
	bool16 result = kFalse;

	do
	{
		// Get the story and range of text to target.
		InterfacePtr<ITextTarget> 	textTarget(this, UseDefaultIID());
		if (textTarget == nil) {
			break;
		}
		InterfacePtr<ITextModel> textModel(textTarget->QueryTextModel());
		if (textModel == nil) {
			break;
		}
		RangeData rangeData = textTarget->GetRange();
		TextIndex textIndex = rangeData.Start(nil);
		int32 length = rangeData.Length();
		result = Utils<IExtLinkFacade>()->CanInsertData(textModel, textIndex, length);
	} while(false);

	return result;
}


/*
*/
ErrorCode ExtLinkSuiteTextCSB::InsertSelectedRecords(const URIList& uriList)
{
	ErrorCode status = kFailure;
	
	do
	{	
		// Get the story and TextIndex to target.
		InterfacePtr<ITextTarget> 	textTarget(this, UseDefaultIID());
		ASSERT(textTarget);
		if (textTarget == nil) {
			break;
		}
		InterfacePtr<ITextModel> textModel(textTarget->QueryTextModel());
		ASSERT(textModel);
		if (textModel == nil) {
			break;
		}
		RangeData rangeData = textTarget->GetRange();
		TextIndex textIndex = rangeData.Start(nil);

		// Use the facade to do the work.
		status = Utils<IExtLinkFacade>()->InsertSelectedRecords(textModel, textIndex, uriList);
	
	} while (false);

	return status;
}

// end:	ExtLinkSuiteTextCSB.cpp
