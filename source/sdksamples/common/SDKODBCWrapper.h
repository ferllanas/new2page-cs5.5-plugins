//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/common/SDKODBCWrapper.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#pragma once
#ifndef __SDKODBCWrapper__
#define __SDKODBCWrapper__

#include "ISDKODBCWrapper.h"
#include "IDataStringUtils.h"
#include "Utils.h"
#include <boost/tokenizer.hpp>
#include <sstream>

// ----- Includes -----
#include "URI.h"
#include "StringUtils.h"

// System include
#include <sql.h>
#include <sqlext.h>

class PMString;

/**
	@ingroup SDKODBCWrapper

\li Provide an interface to access a database through ODBC.

<h2> Introduction </h2>

<p>This is a base class to support Adobe InDesign links to database fields.</p>
  
<p>The InDesign links feature provides the ability to track external assets, via the Links Panel. Common link types include images and text that are stored as local files, files on the network, or with asset-management tools like Adobe Version Cue. The links architecture also provides interfaces that can extend its capability to support custom links; for example, to a database field.</p>
  
<p>ExtendedLink and ExtendedLinkUI provide reference implementations of such links. This plug-in serves as a gateway to a database. When you exercise extended links via the user interface, you are excercising the code in this plug-in.</p>
  
<p>Although the interface is implemented as an InDesign boss class and the plug-in is a regular InDesign plug-in, you may re-implement it as a standalone DLL or simple C++ class.</p>

<h2> Architecture </h2>

<p>This plug-in really only deals with one boss object, <code>kSDKODBCWrapperBoss</code>. The <code>SDKODBCWrapper</code> class implements the <code>ISDKODBCWrapper</code> interface, which provide two types: (1) initialize <code>SDKODBCWrapper</code> with a URI and (2) get information about the wrapper.</p>

<h3> Why URI? </h3>

<p>URIs are used as a standard way to specify InDesign links. A custom link that points to a database also can be specified as a URI. We can use the URI to specify a database table, a record of a table, a column of a table, and even a specific field of a table. For examples, see <code>ISDKODBCWrapper.h</code>.</p>

<p>A URI has five components: scheme, authority, path, query, and fragment. They are used to specify data from a database as follows:</p>
<UL>
	<LI> scheme --  <code>odbc</code></LI>
	<LI> authority --  data source name</LI>
	<LI> path -- empty</LI>
	<LI> query -- table name and select conditions; for example, ?table=tableName&sku=AB-219&..</LI>
	<LI> fragment --  column names; for example, #sku,name,price,quantity,image,description,version</LI>
	<LI> ScriptingGetObject</LI>
</UL>

<p>For example, the following URI refers to the price of an item whose SKU is AB-219:</p>
<pre>&quot;odbc://MySQL?table=inventory&sku=AB-219#price&quot;</pre>

<p>The following URI refers to the entire inventory table:</p>
<pre>&quot;odbc://MySQL?table=inventory&quot;</pre>

<p>The following URI refers to the name, price, and image columns of all records in a table:</p>
<pre>&quot;odbc://MySQL?table=inventory#name,price,image&quot;</pre>

<h3> Database set-up </h3>

<p>To use the SDKODBCWrapper, you need to set up database as described in  <a class="el" href="group__mysqlsetupsetup.html">MySQL Setup Guide</a>.</p>

<h3>The ODBCWrapper implementation</h3>

<p>Conceptually, the ODBCWrapper is a wrapper of ODBC function calls that gives the client code an InDesign-style interface. Its purpose is to enable InDesign links to access databases easily. To place a database field into an InDesign document, you need to create an ODBCWrapper object, initialize it with the URI of the field, get the value of the field, and create a memory stream to let InDesign links code do the rest of work.</p>
  
<p>The internal implementation of the ODBCWrapper is as follows: </p>

<ul>
 	<li> Parse the URI of the object, find the database, and format a <code>SELECT</code> statement according to the components of the URI.</LI>
	<li> Connect to the database specified by the DSN.</LI>
	<li> Perform the query.</LI>
	<li> Dynamically bind variables with the query result columns.</LI>
	<li> Access the specific value using the ODBC cursor mechanism.</LI>
</ul>

 @ingroup sdk_common
*/

class PLUGIN_DECL SDKODBCWrapper: public ISDKODBCWrapper
{
public:


	// constructor/destructor
	SDKODBCWrapper();           
	virtual ~SDKODBCWrapper();

	//Initialize and reset URI cause information contained in the ODBC wrapper changed
	virtual ODBCState Query(const URI uri, const WideString& newValue = WideString(""));
	virtual ODBCState ResetURI(const URI uri);

	//Get current information
	virtual URI GetURI() const {return fURI; }
	virtual std::string GetDataSourceName() const { return fDataSourceName; }

	/**
		@see ISDKODBCWrapper
	*/
	virtual int32 GetNumberOfColumns() const {return fNumColumns; }

	/**
		@see ISDKODBCWrapper
	*/
	virtual ItemType GetMthColumnInfo(const int32 columnIndex, std::string &fieldName) const ;

	/**
		@see ISDKODBCWrapper
	*/
	virtual int32 GetNumberOfRecords() const { return (int32)(fRows.size()); }

	/**
		@see ISDKODBCWrapper
	*/
	virtual K2Vector<WideString> GetNthRecord(const int32 recordIndex, ODBCState *error) const;

	/**
		@see ISDKODBCWrapper
	*/
	virtual WideString GetNthRecordMthColumn(const int32 recordIndex, const int32 columnIndex, ODBCState  *error = 0) const;

	/**
		@see ISDKODBCWrapper
	*/
	virtual WideString GetColumnOfNthRecordWithName(const int32 recordIndex, const std::string columnName, ODBCState  *error) const;


private:
    const WideString kODBCScheme;  //"odbc"

	/** Cleans out the most recent query result from internal buffer */
	void ClearRecordsBuffer();

	/** Connect to a data source specified by dataSource
	*/
	ODBCState Connect(const char *dataSource);

	/** Perform a select statement 
	*/
	ODBCState PerformQuery(const char *statement);

	/** Converts URI components specified as WideString to std::string. 
	  ALERT: This implementation is not the correct way to convert a WideString to std::string. 
			 It only works when source string is URI encoding. Beware. 
	*/
	void URIStringToStdString(const WideString& wideStr, std::string& stdStr) const;

	/** Bind C type to database (ODBC) types after a query
	*/
	SQLRETURN BindSQLColumns();

	/**
		Builds a local cache to store the result of the most recent query
	*/
	SQLRETURN CacheRecords();


	/** The struct store field/column information.
	    The struct has 3 fields to hold the (database internal C) data type, buffer to store the value, and the value size.
		SDKODBCWrapper holds a vector of these structs used to bind to the columns of Database query result data set. 
		
		This implementation reuses the default constructor.  Memory is allocated dynamically when the type of the data is available. 
		It implements a destructor to release internal memory when it is destructed.  The copy constructor and assignment operator 
		are private to prevent unwanted assignment and pass by value operations. 
	*/
	class FieldInfo
	{
	public:
	    SQLINTEGER cType;		//data type
		void *pBuffer;			//data buffer
		SQLINTEGER bufferSize;	//buffer size

		// Defalut constructor
		FieldInfo() : pBuffer (NULL), cType(0), bufferSize(0) 
		{
		}

		~FieldInfo()	//destrutor
		{
		   //Release buffer according to the type of data.
			switch( cType ) 
			{
				case SQL_C_CHAR :
					delete[] (char *)pBuffer;// string type was newed as an array, so delete with []
					break;
					
				case SQL_C_SLONG :
					delete (long *)pBuffer;
					break;
					
				case SQL_C_SSHORT :
					delete (short *)pBuffer;
					break;
					
				case SQL_C_DOUBLE  :
					delete (double *)pBuffer ;
					break;
					
				case SQL_C_TYPE_TIMESTAMP:
					delete (SQL_TIMESTAMP_STRUCT *)pBuffer ;  //Time stamp is a struct
					break;
					
				default:
					ASSERT_MSG(kFalse, "The field type is not supported.");
					break;
			}
			pBuffer = NULL;			
		}

		void GetBuffer(WideString& retStr) const
		{
			switch( cType ) 
			{
				case SQL_C_CHAR:
					{
						StringUtils::ConvertUTF8ToWideString((char*)pBuffer, retStr);
						break;
					}
				case SQL_C_SLONG :
					{
						Utils<IDataStringUtils> dataStringUtils;
						dataStringUtils->Int32ToString(*(long *)pBuffer, &retStr);		
						break;
					}
				case SQL_C_SSHORT :
					{
						Utils<IDataStringUtils> dataStringUtils;
						dataStringUtils->Int32ToString(*(short *)pBuffer, &retStr);		
						break;
					}
				case SQL_C_DOUBLE  :
					{
						std::wostringstream oStream;
						oStream <<  *(double *)pBuffer;
						retStr.SetX16String(oStream.str().c_str());
						break;
					}
				case SQL_C_TYPE_TIMESTAMP :
					{
						SQL_TIMESTAMP_STRUCT *timestamp = (SQL_TIMESTAMP_STRUCT *)pBuffer;
						PMString timeStr;
						timeStr.AppendNumber(timestamp->year); timeStr.Append(":");
						timeStr.AppendNumber(timestamp->month); timeStr.Append(":");
						timeStr.AppendNumber(timestamp->day); timeStr.Append(":");
						timeStr.AppendNumber(timestamp->hour); timeStr.Append(":");
						timeStr.AppendNumber(timestamp->minute); timeStr.Append(":");
						timeStr.AppendNumber(timestamp->second); timeStr.Append(":");
						timeStr.AppendNumber(timestamp->fraction); 

						retStr = WideString(timeStr) ;
						break;
					}
				default:
					ASSERT_MSG(kFalse, "The field type is not supported. The value cannot be returned.");
					break;
			} 
		}

	private:
		// make copy constructor and assignment operator private to prevent misuse.
		FieldInfo& operator=(const FieldInfo& rhs ){return *this;}
		FieldInfo(const FieldInfo& aField){}

	};
	
	//environment, connection, and statement handles that used for data base specific operation
	SQLHANDLE fEnvHandle;
	SQLHANDLE fConnectHandle;
	SQLHANDLE fStmtHandle;

	//The URI of the database. This could be a table, a record or a field
	URI fURI; 
	std::string fDataSourceName; //The data source name of an ODBC. keep sync with the fURI;
    std::vector<std::string> fColumnNameList; //Column name list get from the query result. 
	                                          //If the fragment part of URI is not empty, it should be same as these fields.

	int32 fNumColumns;  //cached number of columns of the query result
	std::vector< std::vector< FieldInfo* > > fRows;		// the local cache holding the result of the query
	std::string fNameConnection;  // DSN
	bool fConnectionStatus;

};

#endif // __SDKODBCWrapper__
