//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkStateUpdateProcessor.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ILinkResourceStateUpdateCmdData.h"

// Implementation includes:
#include "CPMUnknown.h"

// Project includes:
#include "SDKODBCCache.h"
#include "Utils.h"
#include "IExtLinkFacade.h"
#include "ExtLinkODBCLinkResourceStatePacket.h"
#include "IExtLinkStateUpdateProcessor.h"
#include "ExtLinkID.h"
#include "ExtLinkUtils.h"

#include "K2Vector.tpp"

/** IExtLinkStateUpdateProcessor implementation which maintains a queue of ExtLinkODBCLinkResourceStatePacket.  ExtLinkODBCLinkResourceStatePacket is
	created by ExtLinkODBCLinkResourceStateUpdater, when the updater is asked by link manager to give a state update for a link resource, it creates
	a packet which contains a snapshot of the resource state at the time when link manager asks for status update.  The packet is then queued in the
	fJobQueue of ExtLinkStateUpdateProcessor.  The ExtLink sample has an idle task, which will call ExtLinkStateUpdateProcessor::UpdateStateForTopJob
	each time the task is run, which essentially takes one packet off the queue and update the packet with the latest resource information.
 
 @ingroup extendedlink
 @see IExtLinkStateUpdateProcessor
 */
class ExtLinkStateUpdateProcessor : public CPMUnknown<IExtLinkStateUpdateProcessor>
{
public:
	/**
	 Constructor.
	 @param boss interface ptr from boss object on 
	 which this interface is aggregated.
	 */
	ExtLinkStateUpdateProcessor(IPMUnknown* boss);
	
	/** Destructor
	 */
	virtual ~ExtLinkStateUpdateProcessor() {}
	
	
	/** Add a job (ExtLinkODBCLinkResourceStatePacket) to the processor
	 
	 @return kTrue if job is added successfully.
	 */
	virtual bool16 AddJob(const ExtLinkODBCLinkResourceStatePacket& job);
	
	/** Remove the job (ExtLinkODBCLinkResourceStatePacket) from the processor
	 
	 @return kTrue if job is removed successfully.
	 */
	virtual bool16 RemoveJob(const ExtLinkODBCLinkResourceStatePacket& job);
	
	/** Remove the job from the processor
	 
	 @return kTrue if job is removed successfully.
	 */
	virtual bool16 IsJobPending(const ExtLinkODBCLinkResourceStatePacket& job) const;
		
	/**
	 The state updater mainains a job queue. A job is to provide state update for a particular linkr resource.
	 This routine process the state update function given a particular job packet, and remove it from the queue
	 */
	virtual bool16 UpdateStateForJob(const ExtLinkODBCLinkResourceStatePacket& job);
	
	/**
	 The state updater mainains a job queue. A job is to provide state update for a particular linkr resource.
	 This routine removes a job from the job queue and process the state update function
	 */
	virtual bool16 UpdateStateForTopJob();
private:
	
	std::vector<ExtLinkODBCLinkResourceStatePacket> fJobQueue;
};

/*	Makes the implementation available to the application.
 */
CREATE_PMINTERFACE(ExtLinkStateUpdateProcessor, kExtLinkStateUpdateProcessorImpl)


/* Constructor
 */
ExtLinkStateUpdateProcessor::ExtLinkStateUpdateProcessor(IPMUnknown* boss) :
CPMUnknown<IExtLinkStateUpdateProcessor>(boss)
{
}


/*
 */
bool16 ExtLinkStateUpdateProcessor::AddJob(const ExtLinkODBCLinkResourceStatePacket& job)
{
	std::vector<ExtLinkODBCLinkResourceStatePacket>::iterator iter = std::find(fJobQueue.begin(), fJobQueue.end(), job);
	if (iter != fJobQueue.end()) {
		fJobQueue.erase(iter);
	}
	fJobQueue.push_back(job);
	return true;
}


/*
 */
bool16 ExtLinkStateUpdateProcessor::RemoveJob(const ExtLinkODBCLinkResourceStatePacket& job)
{
	std::vector<ExtLinkODBCLinkResourceStatePacket>::iterator iter = std::find(fJobQueue.begin(), fJobQueue.end(), job);
	if (iter != fJobQueue.end()) {
		fJobQueue.erase(iter);
		return true;
	}
	else
		return false;
}


/*
 */
bool16 ExtLinkStateUpdateProcessor::IsJobPending(const ExtLinkODBCLinkResourceStatePacket& job) const
{
	std::vector<ExtLinkODBCLinkResourceStatePacket>::const_iterator iter = std::find(fJobQueue.begin(), fJobQueue.end(), job);
	if (iter != fJobQueue.end())
		return true;
	else
		return false;
}

/*
 */
bool16 ExtLinkStateUpdateProcessor::UpdateStateForJob(const ExtLinkODBCLinkResourceStatePacket& job)
{
	std::vector<ExtLinkODBCLinkResourceStatePacket>::iterator iter = std::find(fJobQueue.begin(), fJobQueue.end(), job);
	if (iter == fJobQueue.end()) {
		return kFalse;
	}
	
	ExtLinkODBCLinkResourceStatePacket& packet = *iter;

	bool16 bAutoRefreshCache = ExtLinkUtils::IsAutoRefreshStatus();
	
	URI uri = packet.GetURI();
	WideString scheme = uri.GetComponent(URI::kScheme);
	if(scheme == WideString(kExtLinkScheme))
	{
		WideString uriAuthority = uri.GetComponent(URI::kAuthority);
		
		if (uriAuthority == WideString(ExtLinkUtils::GetSelectedDataSource()))
		{
			TRACEFLOW("ExtLink", "Enter ODBC Resource State Processor\n");
			// 1 if we have no notion where the database we refer to is, we are plain "missing"
			// 2 if there is not a record in the database for the key, it is missing too
			// 3 if the field value for this uri has changed, update the resource's stamp
			// if a connection to database existed, the state will be ILinkResource::kAvailable, otherwise ILinkResource::kMissing					
			K2Vector<ODBCCacheData> result = SDKODBCCache::Instance().GetCacheData(uri, bAutoRefreshCache);
			if (result.size() > 0 && !result[0].fFieldData.IsNull()) 
			{
				packet.SetState(ILinkResource::kAvailable);
				
				WideString currentValue; 
				if (Utils<IExtLinkFacade>()->GetAssetValueFromURI(uri, currentValue))
				{
					PMString stmpVal(currentValue);
					// update the resource's stamp, we are going to be using the stamp that corresponding to the asset field value to determine 
					// if the link is out of date.  
					WideString stamp;
					stamp.Append(scheme);
					stamp.Append(currentValue);
					
					packet.SetStamp(stamp);
				}
			}
			else
			{
				packet.SetState(ILinkResource::kMissing);
			}
		}	
	}
	
	if (packet.LinkResourceChanged()) {
		InterfacePtr<ICommand> updateCmd(CmdUtils::CreateCommand(kLinkResourceStateUpdateCmdBoss));
		InterfacePtr<ILinkResourceStateUpdateCmdData> updateCmdData(updateCmd, UseDefaultIID());
		
		updateCmdData->SetResource(packet.GetResource());
		updateCmdData->SetDatabase(packet.GetDatabase());
		updateCmdData->SetNotify(packet.GetNotify());
		
		if (packet.URIChanged()) {
			updateCmdData->SetUpdateAction(ILinkResourceStateUpdateCmdData::kUpdateURI);
			updateCmdData->SetURI(packet.GetURI());
		}
		else if (packet.StateChanged()) {
			updateCmdData->SetUpdateAction(ILinkResourceStateUpdateCmdData::kUpdateState);
			updateCmdData->SetState(packet.GetState());
			updateCmdData->SetStamp(packet.GetStamp());
		}
		else if (packet.StampChanged()) {
			updateCmdData->SetUpdateAction(ILinkResourceStateUpdateCmdData::kUpdateStamp);
			updateCmdData->SetStamp(packet.GetStamp());
		}
		
		if (packet.GetOperationType() == ILinkManager::kAsynchronous) {
			CmdUtils::ScheduleCommand(updateCmd, ICommand::kMediumPriority);
		}
		else {
			CmdUtils::ProcessCommand(updateCmd);
		}
	}
	fJobQueue.erase(iter);	// remove job from queue
	return kTrue;
}

/*
 */
bool16 ExtLinkStateUpdateProcessor::UpdateStateForTopJob()
{
	if (fJobQueue.empty())
		return true;
		
	ExtLinkODBCLinkResourceStatePacket& packet = fJobQueue.front();
	return UpdateStateForJob(packet);
}


// end:	PrnSelSuiteASB.cpp

