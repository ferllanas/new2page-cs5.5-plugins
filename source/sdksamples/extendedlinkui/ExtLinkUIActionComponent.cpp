//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIActionComponent.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
//#include "ISession.h"
#include "IActiveContext.h"
#include "IApplication.h"
#include "IActionStateList.h"
#include "IExtLinkSuite.h"
#include "SDKODBCCache.h"
#include "IDataLinkAction.h"
#include "IDialogMgr.h"
#include "IDatabase.h"
#include "IDocument.h"
#include "ILayoutUtils.h"
#include "ILayoutUIUtils.h"
#include "ILink.h"
#include "ILinksUIUtils.h"
#include "ILinkFacade.h"
#include "ILinkResource.h"
#include "IPalettePanelUtils.h"
#include "IPanelControlData.h"
#include "IPanelMgr.h"
#include "ISelectionManager.h"
#include "ITreeViewMgr.h"
#include "ITreeViewController.h"
#include "ITreeViewHierarchyAdapter.h"
#include "LinkResourceQuery.h"
#include "IImageFormatAccess.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "K2Vector.tpp"			// required for NodeIDList to link
#include "RsrcSpec.h"

// General includes:
#include "CActionComponent.h"
#include "CAlert.h"
#include "UIDList.h"

// Project includes:
#include "ExtLinkUIID.h"
#include "IExtLinkFacade.h"
#include "IExtLinkUIDataModel.h"
#include "ExtLinkUINodeID.h"
#include "ExtLinkUIDataNode.h"
#include "ExtLinkUtils.h"

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup extendedlinkui

*/
class ExtLinkUIActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		ExtLinkUIActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);

		/** Called by the framework to allow actions to be enabled or disabled
			dependent on the capability of the selection.		
			@see CActionComponent::UpdateActionStates
			@param ac context in which the action is to be performed.
			@param listToUpdate list containing items to be enabled disabled.
			@param mousePoint ontains the global mouse location at time of event causing action (e.g. context menus), kInvalidMousePoint otherwise.
			@param widget the widget that invoked the action or nil otherwise
		*/
		virtual void UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint = kInvalidMousePoint, IPMUnknown* widget = nil);

	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		

		/** Determines if the current state will allow a Place DB to occur. */
		bool16 CanPlaceDB(IActiveContext* ac);

		/** Opens this plug-in's Place Database dialog. */
		void DoPlaceDBDialog();

		
		/** Refreshes the treeview - good for refreshing the status of the links */
		void DoRefresh();

		/** Updates the selected links - if link is out of date, updates the link and copies latest data to document. */
		void DoUpdateLinks();
		
		/** Sets the selection to whatever the link points to in the document. */
		void DoGotoLink();
		
		/** Opens the Manage Databases dialog. */
		void DoManageDatabasesDialog();
		
		/** Set show in UI. */
		void DoShowInUI();

		// Utility methods
		IDataBase* GetFrontDocDB() const;
		void AddKidsToSelectedLinkUIDs(const IExtLinkUIDataModel* model, const ExtLinkUIDataNode* node, UIDList& selectedUIDs) const;
		void GetPanelSelectedLinkUIDs( UIDList& selectedUIDs) const;
		void GetPanelSelectedLinkURIs( URIList& selectedURIs ) const;
		int32 CountSelectedLinks() const;
		
	private:
		bool16 fShowInUI;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(ExtLinkUIActionComponent, kExtLinkUIActionComponentImpl)



/* ExtLinkUIActionComponent Constructor
*/
ExtLinkUIActionComponent::ExtLinkUIActionComponent(IPMUnknown* boss) : 
	CActionComponent(boss),
	fShowInUI(kTrue)		
{
}

/* DoAction
*/
void ExtLinkUIActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{

		case kExtLinkUIPopupAboutThisActionID:
		case kExtLinkUIAboutActionID:
		{
			this->DoAbout();
			break;
		}
		case kExtLinkUIPlaceDBDialogActionID:
		{
			this->DoPlaceDBDialog();
			break;
		}
		case kExtLinkUIRefreshActionID:
		{
			this->DoRefresh();
			break;
		}
		case kExtLinkUIUpdateLinkActionID:
		{
			this->DoUpdateLinks();
			break;
		}
		case kExtLinkUIGotoLinkActionID:
		{
			this->DoGotoLink();
			break;
		}
		case kExtLinkUIManageDBsActionID:
		{
			this->DoManageDatabasesDialog();
			break;
		}
		case kExtLinkUIShowInUIActionID:
		{
			this->DoShowInUI();
			break;
		}
		default:
		{
			break;
		}
	}
}


/* UpdateActionStates
*/
void ExtLinkUIActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	do
	{
		if (ac == nil)
		{
			ASSERT_FAIL("IActiveContext is invalid");
			break;
		}

		int32 numSelectedLinks = this->CountSelectedLinks();

		int32 numActions = listToUpdate->Length();
		for (int32 i = 0 ; i < numActions ; i++)
		{
			ActionID actionID = listToUpdate->GetNthAction(i);
			int16 oldState = listToUpdate->GetNthActionState(i);
			int16 newState = oldState;
			
			switch (actionID.Get()) 
			{
				case kExtLinkUIPlaceDBDialogActionID:
				{
					newState = this->CanPlaceDB(ac) ? kEnabledAction : kDisabled_Unselected;
					break;
				}
				case kExtLinkUIUpdateLinkActionID:
				{
					newState = (numSelectedLinks >= 1) ? kEnabledAction : kDisabled_Unselected;
					break;
				}
				case kExtLinkUIGotoLinkActionID:
				{
					newState = (numSelectedLinks >= 1) ? kEnabledAction : kDisabled_Unselected;
					break;
				}
				case kExtLinkUIShowInUIActionID:
				{
					fShowInUI = ExtLinkUtils::IsDisplayExtLinksInLinksPanel();
					newState = fShowInUI ? kEnabledAction | kSelectedAction : kEnabledAction;
					break;
				}
				default:
				{
					break;
				}
			}
			
			// Update state only if needed
			if (oldState != newState)
			{	
				listToUpdate->SetNthActionState(i, newState);
			}
		}
	} while (false);	
}



/* DoAbout
*/
void ExtLinkUIActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kExtLinkUIAboutBoxStringKey,		// Alert string
		kOKString,							// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,									// Set OK button to default
		CAlert::eInformationIcon			// Information icon.
	);
}

/* CanPlaceDB
 */
bool16 ExtLinkUIActionComponent::CanPlaceDB(IActiveContext* ac)
{
	bool16 canPlace = kFalse;
	
	if (ac)
	{
		InterfacePtr<IExtLinkSuite> duSuite(ac->GetContextSelection(), UseDefaultIID());
		if (duSuite) 
		{
			canPlace = duSuite->CanInsertData();
		}
	}
	
	return canPlace;
}


/* DoPlaceDBDialog
*/
void ExtLinkUIActionComponent::DoPlaceDBDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kExtLinkUIPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}



/* DoRefresh
*/
void ExtLinkUIActionComponent::DoRefresh()
{
	do
	{
		InterfacePtr<IPanelControlData> panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kExtLinkUIPanelWidgetID));	
		// Don't assert, fail silently, the tree view panel may be closed.
		if(panelData == nil)
		{
			break;
		}
		IControlView* treeWidget = panelData->FindWidget(kExtLinkUITreeViewWidgetID);
		ASSERT(treeWidget);
		if(treeWidget == nil)
		{
			break;
		}

		InterfacePtr<ITreeViewMgr> iTreeViewMgr(treeWidget, UseDefaultIID());
		ASSERT(iTreeViewMgr);
		if(!iTreeViewMgr){
			break;
		}
		
		// find selected links
		URIList selectedURIs;
		this->GetPanelSelectedLinkURIs(selectedURIs);

		// update extendedlink cache for each link (other link statuses are updated when we rebuild the tree view)
		for (int32 i = 0; i < selectedURIs.GetURICount(); i++)
		{
			SDKODBCCache::Instance().GetCacheData(selectedURIs.GetNthURI(i), kTrue);
			// ignore return value - all we really want to do is force the cache to update before updating the tree view
		}

		// give links a chance to update its status with the new cache, there better be a front doc here!
		IDataBase* db = GetFrontDocDB();
		if(!db)
		{
			break;
		}
		
		InterfacePtr<ILinkManager> iLinkMgr(db, db->GetRootUID(), UseDefaultIID());
		if (!iLinkMgr) {
			return;
		}
		
		LinkResourceQuery query;
		ILinkManager::QueryResult linkQueryResult;
		
		if (iLinkMgr->QueryResources(query, kInvalidLinkClientID, linkQueryResult) != 0) {
			for (ILinkManager::QueryResult::const_iterator iter(linkQueryResult.begin()), end(linkQueryResult.end()); iter != end; ++iter) {
				iLinkMgr->UpdateResourceState(*iter, ILinkManager::kAsynchronous);
			}
		}
		
		iTreeViewMgr->ChangeRoot(kTrue);

		
	} while(kFalse);
}



/* DoUpdateLinks
 */
void ExtLinkUIActionComponent::DoUpdateLinks()
{
	InterfacePtr<IPanelControlData> panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kExtLinkUIPanelWidgetID));	
	// Don't assert, fail silently, the tree view panel may be closed.
	if(panelData == nil)
	{
		return;
	}
	IControlView* treeWidget = panelData->FindWidget(kExtLinkUITreeViewWidgetID);
	ASSERT(treeWidget);
	if(treeWidget == nil)
	{
		return;
	}
	
	IDataBase* db = GetFrontDocDB();
	if (db) {
		UIDList selectedUIDs(db);
		this->GetPanelSelectedLinkUIDs(selectedUIDs);
		
		UIDList uidsToUpdate(db);
		for(int32 linkIndex = selectedUIDs.Length() - 1; linkIndex >= 0; --linkIndex)
		{
			InterfacePtr<const ILink> oneLink(db, selectedUIDs[linkIndex], UseDefaultIID());
			if (oneLink->GetResourceModificationState() == ILink::kResourceModified )
			{
				uidsToUpdate.push_back(selectedUIDs[linkIndex]);
			}
		}
		if ( !uidsToUpdate.IsEmpty() )
		{
			Utils<Facade::ILinkFacade>()->UpdateLinks(uidsToUpdate, false, kMinimalUI, false);		// link uids, do not force, minimal ui, do not schedule cmd
		}
	}
}


/* DoGotoLink
 */
void ExtLinkUIActionComponent::DoGotoLink()
{
	InterfacePtr<IPanelControlData> panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kExtLinkUIPanelWidgetID));	
	// Don't assert, fail silently, the tree view panel may be closed.
	if(panelData == nil)
	{
		return;
	}
	IControlView* treeWidget = panelData->FindWidget(kExtLinkUITreeViewWidgetID);
	ASSERT(treeWidget);
	if(treeWidget == nil)
	{
		return;
	}
	
	InterfacePtr<ITreeViewMgr> iTreeViewMgr(treeWidget, UseDefaultIID());
	ASSERT(iTreeViewMgr);
	if(!iTreeViewMgr){
		return;
	}
	
	IDataBase* db = GetFrontDocDB();
	if (db) {
		UIDList selectedUIDs(db);
		this->GetPanelSelectedLinkUIDs(selectedUIDs);

		WideString strVersion("version");
		WideString strImage("image");
		UIDList uidsToUpdate(db);
		for(int32 linkIndex = selectedUIDs.Length() - 1; linkIndex >= 0; --linkIndex)
		{
			InterfacePtr<const ILink> oneLink(db, selectedUIDs[linkIndex], UseDefaultIID());
			if (oneLink)
			{
				InterfacePtr<ILinkResource> theLinkResource(db, oneLink->GetResource(), UseDefaultIID()); 
				if (theLinkResource) {
					URI linkURI = theLinkResource->GetURI();
					//don't update the version and image 
					WideString fragment = linkURI.GetComponent(URI::kFragment);
					if(fragment != strVersion && fragment != strImage)
					{
						uidsToUpdate.push_back(selectedUIDs[linkIndex]);					
					}
				}
			}
		}
		
		if(uidsToUpdate.size() > 0)
		{
			Utils<IExtLinkFacade>()->ProcessSetLinkObjectModStateCommand(ILink::kObjectModified, uidsToUpdate);
			Utils<Facade::ILinkFacade>()->UpdateLinks(uidsToUpdate, false, kMinimalUI, false);
		}
	}
}


/* DoGotoLink
 */
void ExtLinkUIActionComponent::DoManageDatabasesDialog()
{
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kExtLinkUIPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kExtLinkUIManageDBsDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
/*		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kExtLinkUIPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kExtLinkUIManageDBsDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
*/	
	} while (false);			
}


void ExtLinkUIActionComponent::DoShowInUI()
{
	fShowInUI = !fShowInUI;
	Utils<IExtLinkFacade>()->SetExtLinksShowInLinksPanel(fShowInUI);
}

/* AddKidsToSelectedLinkUIDs
*/
void ExtLinkUIActionComponent::AddKidsToSelectedLinkUIDs(const IExtLinkUIDataModel* model, const ExtLinkUIDataNode* node, UIDList& selectedUIDs) const
{
	// add the node to the list of selected uids
	UID linkUID = node->GetLinkUID(); 
	if(linkUID != kInvalidUID && linkUID != kRootDocumentExtLinkNodeUID) {
		selectedUIDs.push_back(linkUID);
	}

	// add this node's kids to the list of selected uids
	int32 numKids = model->GetNumChildren(node->GetNodeURI(), linkUID); 
	for (int32 kidNum = 0; kidNum < numKids; kidNum++)
	{
		const ExtLinkUIDataNode* childNode = model->GetNthChild(node, kidNum);

	//	NodeID_rv childNode = adapter->GetNthChild(node, kidNum);
		this->AddKidsToSelectedLinkUIDs(model, childNode, selectedUIDs);
	}
}


/* GetLinksSelectedInPanel
*/
void ExtLinkUIActionComponent::GetPanelSelectedLinkUIDs( UIDList& selectedUIDs) const
{
    InterfacePtr<const IApplication> theApp(GetExecutionContextSession()->QueryApplication()); 
    InterfacePtr<const IPanelMgr> panelMgr(theApp->QueryPanelManager()); 
    if (panelMgr) {
        IControlView* panel = panelMgr->GetVisiblePanel(kExtLinkUIPanelWidgetID);
		if (panel) {
			InterfacePtr<const IPanelControlData> panelData(panel, UseDefaultIID());
			if (panelData) {
				IControlView* treeView = panelData->FindWidget(kExtLinkUITreeViewWidgetID); 
				if (treeView) {
					InterfacePtr<const ITreeViewController> treeController(treeView, UseDefaultIID()); 
					if (treeController) 
					{
						NodeIDList selectedItems; 
						treeController->GetSelectedItems(selectedItems); 
						for(int32 i = selectedItems.size() - 1; i >= 0; --i) 
						{ 
							InterfacePtr<const IExtLinkUIDataModel> model(treeView, UseDefaultIID());
							if (model) {
								TreeNodePtr<const ExtLinkUINodeID> node(selectedItems[i]); 
								ExtLinkUIDataNode* dataNode = const_cast<ExtLinkUIDataNode*>(model->GetNode(node->GetNodeURI(), node->GetLinkUID()));
								if (dataNode) {
									this->AddKidsToSelectedLinkUIDs(model, dataNode, selectedUIDs);
								}
							}
						} 
					}
				}
			}
		}
	}
} 


/* GetFrontDocDB
*/
IDataBase* ExtLinkUIActionComponent::GetFrontDocDB() const
{
	IDataBase* db = nil;
	IDocument* iDoc = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
	if (iDoc) {
		db = ::GetDataBase(iDoc);
	}
	return db;
}


/* GetPanelSelectedLinkURIs
*/
void ExtLinkUIActionComponent::GetPanelSelectedLinkURIs( URIList& selectedURIs) const
{
	IDataBase* db = GetFrontDocDB();
	if (db) {
		UIDList selectedUIDs(db);
		this->GetPanelSelectedLinkUIDs(selectedUIDs);
		for (int32 i = 0; i < selectedUIDs.size(); i++ ) {
			InterfacePtr<ILink> theLink(db, selectedUIDs[i], UseDefaultIID());
			if(theLink) {
				InterfacePtr<ILinkResource> theLinkResource(db, theLink->GetResource(), UseDefaultIID()); 
				if (theLinkResource) {
					URI linkURI = theLinkResource->GetURI();
					selectedURIs.AddURI(linkURI);
				}
			}
		}
	}
}


/* CountSelectedLinks
 */
int32 ExtLinkUIActionComponent::CountSelectedLinks() const
{
	int32 numSelected = 0;
	
	IDataBase* db = GetFrontDocDB();
	if (db) {
		UIDList selectedUIDs(db);
		this->GetPanelSelectedLinkUIDs(selectedUIDs);
		numSelected = selectedUIDs.size();
	}
	
	return numSelected;
}





//  Code generated by DollyXs code generator
