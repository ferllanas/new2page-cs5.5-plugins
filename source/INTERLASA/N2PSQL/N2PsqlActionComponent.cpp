//========================================================================================
//  
//  $File: $
//  
//  Owner: Juan Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IApplication.h"
#include "IDialogMgr.h"
#include "IDialog.h"

// General includes:
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "ILayoutUIUtils.h"
#include "IPanelControlData.h"

#include "CActionComponent.h"
#include "CAlert.h"
#include "UpdateStorysAndDBUtilis.h"
#include "IGalleyUtils.h"
#include "IPalettePanelUtils.h"

#include "TextID.h"

// Project includes:
#include "N2PsqlID.h"
#include "N2PSQLUtilities.h"

#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"

#include "IDialogController.h"
#include "UpdateStorysAndDBUtilis.h"
#include "IPanelControlData.h"
#include "IWidgetParent.h"
#include "IDropDownListController.h"
#include "IPDFExptStyleListMgr.h"
#include "IWorkspace.h"

#include "IStringListControlData.h"
#include "SDKFileHelper.h"
#include "PlatformFileSystemIterator.h"


#include "N2PsqlLogInLogOutUtilities.h"
#include "N2PInOutID.h"
#include "ICheckInOutSuite.h"
#include"IN2PCTUtilities.h"




/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

*/
class N2PsqlActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		N2PsqlActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);

	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();

		/** Encapsulates functionality for the ItemOne menu item. */
		void DoItemOne();


		void DoCheckInPage();


		void DoCheckOutPage();


		void DoSaveRev();

		void OpenPreferencesDLG();

		void DoCheckOutNota(IActiveContext* ac);
		
		void DoCheckInNota();
		
		void DoImportNota();
		
		void DoDetachNota();
		
		void DoCrearNuevaNota();
		
		void DoCrearPaseDeNota();
		
		void DoLigarImagenANota();
		
		void Fotos_DoAceptarLigasDImagenANota();
		
		void DoDuplicarPagina();
	
	void OpenAsignarIssues();
	
	void LlenarPDFComboBox(IControlView* CVDialogNuevaPref,PMString PresetToPDFeditorial,PMString PresetToPDFeditorialWEB);
	
	void LlenarComboPreferenciasOnDlg(IControlView* CVDialogNuevaPref, PMString NomPref);
		
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PsqlActionComponent, kN2PsqlActionComponentImpl)

/* N2PsqlActionComponent Constructor
*/
N2PsqlActionComponent::N2PsqlActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}




/* DoAction
*/
void N2PsqlActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	switch (actionID.Get())
	{
	
		case kN2PsqlDetachPhotoActionID:
		{
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			PreferencesConnection PrefConections;
			
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
			UpdateStorys->Fotos_DesligarDePagina(PrefConections);
			break;
		}
		
		
		case kN2PsqlDetachPaginaActionID:
		{
			//LogIN
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(document==nil)
			{
				//CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
				break;
			}
			
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			if(app==nil)
			{
				break;
			}
		
		
			PMString Aplicacion = app->GetApplicationName();
			Aplicacion.SetTranslatable(kFalse);
		
			if(Aplicacion.Contains("InCopy"))
			{
				break;
			}
		
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			PreferencesConnection PrefConections;
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			UpdateStorys->DesligarPagina(document, PrefConections);
			break;
		}
		
		case kN2PsqlLogInActionID:
		{
			//LogIN
			N2PsqlLogInLogOutUtilities::DoLogInUser();;
			break;
		}

		case kN2PsqlLogOutActionID:
		{
			//Logout
			N2PsqlLogInLogOutUtilities::DoLogOutUser();
			break;
		}
		
		case kN2PsqlDBPopupAboutThisActionID:
		case kN2PsqlAboutActionID:
		{
			
			this->DoAbout();
			break;
		}

		case kN2PsqlDepositarPaginaActionID:
		{
			//check in
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			if(app==nil)
			{
				break;
			}
		
		
			PMString Aplicacion = app->GetApplicationName();
			Aplicacion.SetTranslatable(kFalse);
		
			if(Aplicacion.Contains("InCopy"))
			{
				if(!N2PSQLUtilities::VerificarSiExisteDocumentoSobrePathInicial(Utils<ILayoutUIUtils>()->GetFrontDocument()))
				{
					CAlert::InformationAlert(kN2PHacerUpdateAntesDContinuarStringKey);
					break;
				}
			}
			
			this->DoCheckInPage();
			break;
		}
		
		case kN2PSQLDuplicarPaginaActionID:
		{
			//check in
			this->DoDuplicarPagina();
			break;
		}
		

		case kN2PsqlRetirarPaginaActionID:
		{
			//check out
			this->DoCheckOutPage();
			break;
		}

		case kN2PsqlSalvarActionID:
		{
			//check in
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			if(app==nil)
			{
				break;
			}
		
		
			PMString Aplicacion = app->GetApplicationName();
			Aplicacion.SetTranslatable(kFalse);
		
			if(Aplicacion.Contains("InCopy"))
			{
				if(!N2PSQLUtilities::VerificarSiExisteDocumentoSobrePathInicial(Utils<ILayoutUIUtils>()->GetFrontDocument()))
				{
					CAlert::InformationAlert(kN2PHacerUpdateAntesDContinuarStringKey);
					break;
				}
			}
			this->DoSaveRev();
			break;
		}

		case kN2PsqlDBItemOneActionID:
		{
			this->DoItemOne();
			break;
		}

		case kN2PSQLPreferenciasActionID:
		{
			this->OpenPreferencesDLG();
			/*if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}*/
			
			break;
		}
		
		case kN2PSQLCheckOutNotaActionID:
		{
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				this->DoCheckOutNota(ac);
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			
			break;
		}
		
		case kN2PSQLCheckInNotaActionID:
		{
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				this->DoCheckInNota();
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			
			break;
		}
		
		
		case kN2PSQLImportNotaToActServerActionID:
		{
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				this->DoImportNota();
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			break;
		}
		
		case kN2PSQLDetachNotaActionID:
		{
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				this->DoDetachNota();
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			break;
		}
		
		
		case kN2PSQLCrearSpaceAndNotaActionID:
		{
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				this->DoCrearNuevaNota();
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			break;
		}
		
/*		case kN2PSQLLigarImagensANotaActionID:
		{
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				this->DoLigarImagenANota();
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			break;
		}
*/		
		
		case kN2PSQLAceptarImagenesDNotaActionID:
		{
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				this->Fotos_DoAceptarLigasDImagenANota();
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			break;
		}
		
		case kN2PSQLCrearPaseDeNotaActionID:
		{
			if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
			{
				this->DoCrearPaseDeNota();
			}
			else
			{
				CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
			}
			break;
		}
		
		
		case kN2PsqlBuscarNotasDPaginaActionID:
		{
			PreferencesConnection PrefConections;
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			
			
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
			N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse, PrefConections);
			break;
		}
		
		case kN2PsqlBuscarNotasActionID:
		{
			PreferencesConnection PrefConections;
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			
			
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
			N2PSQLUtilities::BusquedaPorDialogo(PrefConections);
			break;
		}
		
		
		case kN2PsqlActualizarTodoActionID:
		{
			PreferencesConnection PrefConections;
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			
			
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
			UpdateStorys->ActualizaTodasLNotasYDisenoPagina("N2PActualizacion", PrefConections);
			
			break;
		}
			
		case kN2PsqlCreatePieDFotoActionID:
		{
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			PreferencesConnection PrefConections;
			
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
			UpdateStorys->CreaPieDeFotoFunction(PrefConections);
			
			break;
			
		}
			
		case kN2PSQLOpenDialogIssuesActionID:
		{
			this->OpenAsignarIssues();
			break;
		}
		
		default:
		{
			break;
		}
	}
}

/* DoAbout
*/
void N2PsqlActionComponent::DoAbout()
{
	
	
	CAlert::InformationAlert(kN2PsqlAboutBoxStringKey);
	
		   
	
}

/* DoItemOne
*/
void N2PsqlActionComponent::DoItemOne()
{
	CAlert::InformationAlert(kN2PsqlItemOneStringKey);
}

/**
*/
void N2PsqlActionComponent::DoCheckInPage()
{
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		PreferencesConnection PrefConections;
		
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			//CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
			break;
		}
		
		if(!UpdateStorys->ValidaExistePaginaSobreBD(document, PrefConections))
		{
			CAlert::InformationAlert(kN2PsqlErrorAlertPaginaBorradaEnBDString);
			break;
		}
		
		// Our menu item has been chosen.  FIrst, create the
		// object that supports the IHelloWorld interface:
		InterfacePtr<ICheckInOutSuite> checkIn(static_cast<ICheckInOutSuite*> (CreateObject
			(
				kCheckInOutSuiteBoss,	// Object boss/class
				ICheckInOutSuite::kDefaultIID
			)));
			
		// If we couldn't get a valid pointer, report that:
		if (checkIn == nil)
		{
			ASSERT_FAIL("HWMMenuComponent::DoCheckInPage: DoCheckInPage invalid");
		}
		else
		{
			checkIn->CheckIn(); // Call method to display message.
		}
			
	}while(false);
	
}

/**
*/
void N2PsqlActionComponent::DoCheckOutPage()
{
	// Our menu item has been chosen.  FIrst, create the
	// object that supports the IHelloWorld interface:
	InterfacePtr<ICheckInOutSuite> checkOut(static_cast<ICheckInOutSuite*> (CreateObject
			(
				kCheckInOutSuiteBoss,	// Object boss/class
				ICheckInOutSuite::kDefaultIID
			)));
			
	// If we couldn't get a valid pointer, report that:
	if (checkOut == nil)
	{
		ASSERT_FAIL("HWMMenuComponent::DoCheckInPage: checkOut invalid");
	}
	else
	{
		checkOut->CheckOut(); // Call method to display message.
	}
}


/**
*/
void N2PsqlActionComponent::DoSaveRev()
{
	// Our menu item has been chosen.  FIrst, create the
	// object that supports the IHelloWorld interface:
	InterfacePtr<ICheckInOutSuite> saveRevi(static_cast<ICheckInOutSuite*> (CreateObject
			(
				kCheckInOutSuiteBoss,	// Object boss/class
				ICheckInOutSuite::kDefaultIID
			)));
			
	// If we couldn't get a valid pointer, report that:
	if (saveRevi == nil)
	{
		ASSERT_FAIL("HWMMenuComponent::DoCheckInPage: saveRevi invalid");
	}
	else
	{
		
		do
		{
			PMString NameDocumentActual="";
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			PreferencesConnection PrefConections;
			
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
		
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if(document==nil)
			{
				//CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
				break;
			}
		
			//Verifica que no sea un documento nuevo
			document->GetName(NameDocumentActual);
			
			
			if(!UpdateStorys->ValidaExistePaginaSobreBD(document, PrefConections))
			{
				CAlert::InformationAlert(kN2PsqlErrorAlertPaginaBorradaEnBDString);
				break;
			}
		
			//Para Gurdar Revision
			ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
			
		
		
			if(NameDocumentActual.Contains("Untitled") || NameDocumentActual.Contains("Sin tÌtulo-") || NameDocumentActual.Contains(".indt"))
			{
				checkIn->SaveRevi(kTrue);
				break;
			}
			
			
			///Obtiene las Ultimas Preferencias
			PrefParamDialogInOutPage PreferencesPara;
			
			checkIn->GetPreferencesParametrosOfDB(&PreferencesPara);
			//checkIn->GetPreferencesParametrosOfFile(&PreferencesPara);
		
			//Hay que validar que ya se ha realizadon un chech in o save revition
			// y que la pagina se llame igual a nombre de pagina actual
			if(PreferencesPara.Pagina_To_SavePage.NumUTF16TextChars() == 0)
			{
				//si no se ha realizado un previo save as o  save se habre el diago save as 
				checkIn->SaveRevi(kTrue);
				break;
			}
			
			PMString NameDocUltimoSaveAs=PreferencesPara.Pagina_To_SavePage;
			NameDocUltimoSaveAs.Append(".indd");
		
			///Para ver si el documento de enfrente se llama igual que el ultimo salvado
			/*if(NameDocumentActual!=NameDocUltimoSaveAs)
			{
				checkIn->SaveRevi(kTrue);
				break;
			}*/
			
			//
			checkIn->SaveRevi(kFalse);
			//PreferencesConnection PrefConections;
			//CAlert::InformationAlert("AAA");
			
			//UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
			if(PrefConections.N2PUtilizarGuias==1)
			{
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kTrue);
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kFalse);
				PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
				if(Id_Seccion.NumUTF16TextChars()>0 )
					N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
				
			}
			else
			{
				//CAlert::InformationAlert("BBB");
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlComboguiaWidgetID,kFalse );
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlTextEnGuiaWidgetID,kTrue );
				
			}
		}while(false);
	}
}

void N2PsqlActionComponent::OpenPreferencesDLG()
{
	//RsrcID dialogID = kBscSlDlgDialogResourceID;
	//WidgetID panelID = kYangPanelWidgetID;
	do
	{
		// Get the registered services, including our selectable dialog service:
		InterfacePtr<IK2ServiceRegistry> sRegistry(GetExecutionContextSession(), UseDefaultIID());//UseDefaultIID());
		if (sRegistry == nil)
		{
			CAlert::InformationAlert("BscSlDlgActionComponent::DoBasicSelectableDialog: sRegistry invalid");
			break;
		}
		
		// The service is provided based on the service ID and the boss passed in 
		ClassID classID;
		//if (isTabStyle)
		//{
		//	classID = kBscSlDlgTabDialogBoss;
		//}
		//else
		//{
		classID = kN2PSQLSlDlgDialogBoss;
		//}
		InterfacePtr<IK2ServiceProvider> 
		sdService(sRegistry->QueryServiceProviderByClassID(kSelectableDialogServiceImpl, classID));
		if (sdService == nil)
		{
			CAlert::InformationAlert("BscSlDlgActionComponent::DoBasicSelectableDialog: sdService invalid");
			break;
		}
		
		// This is an interface to our service, which will end up calling our 
		// plug-in's dialog creator implementation:
		InterfacePtr<IDialogCreator> dialogCreator(sdService, IID_IDIALOGCREATOR);
		if (dialogCreator == nil)
		{
			CAlert::InformationAlert("BscSlDlgActionComponent::DoBasicSelectableDialog: dialogCreator invalid");
			break;
		}
		
		// This is our CreateDialog, which loads the dialog from our resource:
		IDialog* dialog = dialogCreator->CreateDialog();
		if (dialog == nil)
		{
			CAlert::InformationAlert("BscSlDlgActionComponent::DoBasicSelectableDialog: could not create dialog");
			break;
		}
		
		// The panel data will be the selectable panel of the dialog:
		InterfacePtr<IPanelControlData> panelData(dialog, UseDefaultIID());
		if (panelData == nil)
		{
			CAlert::InformationAlert("BscSlDlgActionComponent::DoBasicSelectableDialog: panelData invalid");
			break;
		}
		
		// Find the panel:
		WidgetID    widgetID;
		//if (isTabStyle)
		//{
		//	widgetID = kBscSlDlgTabDialogWidgetID;
		//}
		//else
		//{
		widgetID = kBscSlDlgDialogWidgetID;
		//}
		IControlView* dialogView = panelData->FindWidget(widgetID);
		if (dialogView == nil)
		{
			CAlert::InformationAlert("BscSlDlgActionComponent::DoBasicSelectableDialog: dialogView invalid");
			break;
		}
		
		// The dialog switcher controls how the panels are switched between:
		InterfacePtr<ISelectableDialogSwitcher> 
		dialogSwitcher(dialogView, IID_ISELECTABLEDIALOGSWITCHER);
		if (dialogSwitcher == nil)
		{
			CAlert::InformationAlert("BscSlDlgActionComponent::DoBasicSelectableDialog: dialogSwitcher invalid");
			break;
		}
		
		dialogSwitcher->SetDialogServiceID(kBscSlDlgService);
		
		// Sets default panel.
		dialogSwitcher->SwitchDialogPanelByID(kYinPanelWidgetID);
		
		// Open the dialog.
		dialog->Open(); 
		
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
		
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			CAlert::ErrorAlert("N2PsqlWidgetObserver::AbrirDialogoBusqueda: dialogController invalid");
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		PreferencesConnection PrefConections;
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		
		
		if(UpdateStorys!=nil)
		{
			InterfacePtr<IWidgetParent>		myParent(CVDialogNuevaPref, UseDefaultIID());				
			if(myParent==nil)
			{
				CAlert::InformationAlert("myParent==nil");
				break;
			}
			
			
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panelData1((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panelData1==nil)
			{
				CAlert::InformationAlert("panelData1==nil");
				break;
			}	
			
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
			//PrefConections.IPServerConnection.SetTranslatable(kFalse);
			//dialogController->SetTextControlData(kN2PSQLIPServerOfDBEditWidgetID,PrefConections.IPServerConnection);
			
			PrefConections.NameConnection.SetTranslatable(kFalse);
			dialogController->SetTextControlData(kN2PSQLNameConectionDBWidgetID,PrefConections.NameConnection);
			
			PrefConections.DSNNameConnection.SetTranslatable(kFalse);
			dialogController->SetTextControlData(kN2PSQLDSNNameEditWidgetID,PrefConections.DSNNameConnection);
			
			PrefConections.NameUserDBConnection.SetTranslatable(kFalse);
			dialogController->SetTextControlData(kN2PSQLUsuarioOfDBEditWidgetID,PrefConections.NameUserDBConnection);
			//CAlert::InformationAlert(PrefConections.NameUserDBConnection);
			
			dialogController->SetTextControlData(kN2PSQLPassswordOfDBWidgetID, PrefConections.PwdUserDBConnection ,panelData1,kTrue, kTrue);
			//dialogController->SetTextControlData(kN2PSQLPassswordOfDBWidgetID,"");
			
			PrefConections.PathOfServerFile.SetTranslatable(kFalse);
			dialogController->SetTextControlData(kN2PSQLPathOfServerFileEditWidgetID,PrefConections.PathOfServerFile);
			dialogController->SetTextControlData(kN2PSQLPathOfServerFileImagenesEditWidgetID,PrefConections.PathOfServerFileImages);
			dialogController->SetTextControlData(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID,PrefConections.ImagCarpInServFImages);
			dialogController->SetTextControlData(kN2PSQLRutaDServidorRespNotasEditWidgetID,PrefConections.RutaDServidorRespNotas);
			dialogController->SetTextControlData(kN2PSQLURLWebServicesEditWidgetID,PrefConections.URLWebServices);
			
			dialogController->SetTextValue(KN2PSQLTimeToCheckUpdateElemsWidgetID,PrefConections.TimeToCheckUpdateElements);
			dialogController->SetTextValue(kN2PResizeHeightWidgetID,PrefConections.N2PResizeHeight);
			dialogController->SetTextValue(kN2PResizeWidthWidgetID,PrefConections.N2PResizeWidth);
			
			if(PrefConections.LlenarTablaWEB==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,ITriStateControlData::kUnselected);
			
			dialogController->SetTextControlData(KN2PSQLEstatusParaExportarATWEBWidgetID,PrefConections.EstadoParaExportarATWEB);
			
			this->LlenarComboPreferenciasOnDlg(CVDialogNuevaPref,PrefConections.NameConnection);
			
			
			if(PrefConections.EsCampoGuiaObligatorio==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,ITriStateControlData::kUnselected);
			
			if(PrefConections.N2PResizePreview==1)
			{
				dialogController->SetTriStateControlData(kN2PResizePreviewViewWidgetID,ITriStateControlData::kSelected);
			}
			else
			{
				dialogController->SetTriStateControlData(kN2PResizePreviewViewWidgetID,ITriStateControlData::kUnselected);
			}
			
			
			if(PrefConections.N2PUtilizarGuias==1)
			{
				dialogController->SetTriStateControlData(kN2PUsarGuiasWidgetID,ITriStateControlData::kSelected);
			}
			else
			{
				dialogController->SetTriStateControlData(kN2PUsarGuiasWidgetID,ITriStateControlData::kUnselected);
			}
			
			if(PrefConections.N2PHmteSendToHmca==1)
			{
				//CAlert::InformationAlert("E select");
				dialogController->SetTriStateControlData(kN2PHmteSendToHmcaWidgetID,ITriStateControlData::kSelected);
			}
			else
			{
				//CAlert::InformationAlert("E kUnselected");
				dialogController->SetTriStateControlData(kN2PHmteSendToHmcaWidgetID,ITriStateControlData::kUnselected);
			}
			
			if(PrefConections.ExportPDFPresetsEditWEB==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,ITriStateControlData::kUnselected);
			
			
			if(PrefConections.ExportPDFPresets==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,ITriStateControlData::kUnselected);
			
			if(PrefConections.EnviaAWEBEnAltasPieFoto==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,ITriStateControlData::kUnselected);
			
			
			if(PrefConections.EnviaAWEBSinRetornosTitulo==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,ITriStateControlData::kUnselected);
			
			
			if(PrefConections.EsEnviarNotasHijasAWEB==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,ITriStateControlData::kUnselected);
			
			
			if(PrefConections.CreateBackUpPagina==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,ITriStateControlData::kUnselected);
			
			if(PrefConections.ExportarPaginaComoPDF==1)
				dialogController->SetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,ITriStateControlData::kUnselected);
			
			dialogController->SetTextControlData(KN2PSQLNombreEstatusParaNotasCompletasWidgetID,PrefConections.NombreEstatusParaNotasCompletas);
			dialogController->SetTextControlData(KN2PSQLRutaDePaginaComoPDFWidgetID,PrefConections.RutaDePaginaComoPDF);
			dialogController->SetTextControlData(KN2PSQLRutaDePaginaComoPDFWEBWidgetID,PrefConections.RutaDePaginaComoPDFWEB);
			dialogController->SetTextControlData(kN2PSQLnameParaStyleCreditoWidgetID,PrefConections.nameParaStyleCredito);
			
			dialogController->SetTextControlData(kN2PSQLnameParaStyleContenidoWidgetID, PrefConections.nameParaStyleContenido);
			dialogController->SetTextControlData(kN2PSQLnameParaStyleBalazoWidgetID, PrefConections.nameParaStyleBalazo);
			dialogController->SetTextControlData(kN2PSQLnameParaStyleTituloWidgetID, PrefConections.nameParaStyleTitulo);
			dialogController->SetTextControlData(kN2PSQLnameParaStylePieFotoWidgetID, PrefConections.nameParaStylePieFoto);
			
			
			
			//CAlert::InformationAlert(PrefConections.N2PHmeDSNName);
			dialogController->SetTextControlData(kN2PHmeDSNNameWidgetID,PrefConections.N2PHmeDSNName);
			dialogController->SetTextControlData(kN2PHmeDSNUserLoginWidgetID,PrefConections.N2PHmeDSNUserLogin);
			dialogController->SetTextControlData(kN2PHmeDSNPwdLoginWidgetID, PrefConections.N2PHmeDSNPwdLogin ,panelData1,kTrue, kTrue);
		
			dialogController->SetTextControlData(kN2PHmeFTPServerWidgetID,PrefConections.N2PHmeFTPServer);
			dialogController->SetTextControlData(kN2PHmeFTPUserWidgetID,PrefConections.N2PHmeFTPUser);
			dialogController->SetTextControlData(kN2PHmeFTPPwdWidgetID, PrefConections.N2PHmeFTPPwd ,panelData1,kTrue, kTrue);
			dialogController->SetTextControlData(kN2PHmeFTPFolderWidgetID,PrefConections.N2PHmeFTPFolder);
			LlenarPDFComboBox( CVDialogNuevaPref, PrefConections.PresetTOPDFEditorial,PrefConections.PresetTOPDFEditorialWEB);	
			
			
			if(PrefConections.N2PUseFototeca==1)
				dialogController->SetTriStateControlData(kN2PUseFototecaWidgetID,ITriStateControlData::kSelected);
			else
				dialogController->SetTriStateControlData(kN2PUseFototecaWidgetID,ITriStateControlData::kUnselected);
			
			dialogController->SetTextControlData(kN2PPhotoServerFolderINWidgetID,PrefConections.N2PPhotoServerFolderIN);
			dialogController->SetTextControlData(kPhotoServerFolderOUTWidgetID,PrefConections.PhotoServerFolderOUT);
			
			if(PrefConections.N2PCkBoxSendWordPress==1)
				if(dialogController->GetTriStateControlData(kN2PCkBoxSendWordPressWidgetID) ==ITriStateControlData::kUnselected)
					dialogController->SetTriStateControlData(kN2PCkBoxSendWordPressWidgetID,ITriStateControlData::kSelected);
				else
					dialogController->SetTriStateControlData(kN2PCkBoxSendWordPressWidgetID,ITriStateControlData::kUnselected);
			
			dialogController->SetTextControlData(kN2PLinkWebServicesWordPressWidgetID, PrefConections.N2PLinkWebServicesWordPress);
			
			
			
			dialog->WaitForDialog();//bool16 bSupressScheduled = kFalse
			
			
			//PreferencesConnection PrefConections;
			//InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			//																						  (
			//																						   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			//																						   IUpdateStorysAndDBUtils::kDefaultIID
			//																						   )));
			
						
			if(UpdateStorys!=nil)
			{
				UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kTrue);
			}
			
		}
	} while (false);         

}


void N2PsqlActionComponent::DoCheckOutNota(IActiveContext* ac)
{

	
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
			if(UpdateStorys==nil)
			{
				break;
			}
		PreferencesConnection PrefConections;
			UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			UpdateStorys->CheckOutNota(PrefConections);
	}while(false);
	
}


void N2PsqlActionComponent::DoCheckInNota()
{	
	do
	{
			
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
						
		InterfacePtr<IN2PCTUtilities> N2PCTUtils(static_cast<IN2PCTUtilities*> (CreateObject
		(
			kN2PCTUtilitiesBoss,	// Object boss/class
			IN2PCTUtilities::kDefaultIID
		)));
			
		if(N2PCTUtils==nil)
		{
			break;
		}
				
		N2PCTUtils->OcultaNotasConFrameOverset();					
			
		PreferencesConnection PrefConections;
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
		UpdateStorys->NotaCmdDepositarCheckIn(PrefConections);
			
		if( N2PSQLUtilities::GetEstateCkBoxOfWidget(kN2PSQLCkBoxShowTxtOversetEditWidgetID))
			UpdateStorys->DoMuestraNotasConFrameOverset();
			
	}while(false);
}

void N2PsqlActionComponent::DoImportNota()
{
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
			break;
			
		UpdateStorys->ImportarNota(PrefConections);
	}while(false);
}

void N2PsqlActionComponent::DoDetachNota()
{
	
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		PreferencesConnection PrefConections;
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			
		UpdateStorys->DetachNota(PrefConections);
	}while(false);

}

void N2PsqlActionComponent::DoCrearNuevaNota()
{
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
			
		PreferencesConnection PrefConections;
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);

		UpdateStorys->DoCrearNuevaNota(PrefConections);
	}while(false);
}



/*void N2PsqlActionComponent::DoLigarImagenANota()
{
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
			
		UpdateStorys->DoLigarImagenANota();
	}while(false);
}*/

void N2PsqlActionComponent::DoCrearPaseDeNota()
{
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		PreferencesConnection PrefConections;
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
		UpdateStorys->DoCrearPaseNota(PrefConections);
	}while(false);
}

void N2PsqlActionComponent::Fotos_DoAceptarLigasDImagenANota()
{
	do
	{
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
		UpdateStorys->Fotos_DoAceptarLigasDImagenANota(PrefConections);
	}while(false);
}


/**
*/
void N2PsqlActionComponent::DoDuplicarPagina()
{
	do
	{
		// Our menu item has been chosen.  FIrst, create the
		// object that supports the IHelloWorld interface:
	
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
		
		
			
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			//CAlert::InformationAlert(kN2PInOutNoDocumentoAbiertoStringKey);
			break;
		}
		
		PMString N2PSQL_ID_Pag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
			
		if(N2PSQL_ID_Pag.NumUTF16TextChars()<=0)
		{
			this->DoSaveRev();//Guardar como primera revision
			break;
		}
		
		if(!UpdateStorys->ValidaExistePaginaSobreBD(document, PrefConections))
		{
			CAlert::InformationAlert(kN2PsqlErrorAlertPaginaBorradaEnBDString);
			break;
		}
		
		InterfacePtr<ICheckInOutSuite> checkOut(static_cast<ICheckInOutSuite*> (CreateObject
			(
				kCheckInOutSuiteBoss,	// Object boss/class
				ICheckInOutSuite::kDefaultIID
			)));
			
		// If we couldn't get a valid pointer, report that:
		if (checkOut == nil)
		{
			ASSERT_FAIL("HWMMenuComponent::DoCheckInPage: checkOut invalid");
		}
		else
		{
			checkOut->DuplicarPagina(); // Call method to display message.
		}
	}while(false);
	
}


void N2PsqlActionComponent::LlenarPDFComboBox(IControlView* CVDialogNuevaPref, PMString PresetToPDFeditorial,PMString PresetToPDFeditorialWEB)
{
		
	do
	{
			
			
			
		InterfacePtr<IWidgetParent>		myParent(CVDialogNuevaPref, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
			
			
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelData==nil)
		{
			break;
		}	
			
		IControlView* ComboCView = panelData->FindWidget(kN2PsqlExportPDFPresetsDropDownWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
			
			
		IControlView* ComboCViewEditWEB = panelData->FindWidget(kN2PsqlExportPDFPresetsEditWEBDropDownWidgetID);
		ASSERT(ComboCViewEditWEB);
		if(ComboCViewEditWEB == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCViewEditWEB");
			break;
		}
			
		InterfacePtr<IStringListControlData> dropListDataWEB(ComboCViewEditWEB,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataWEB == nil)
		{
			CAlert::InformationAlert("No pudo Obtener dropListDataWEB*");
			
			break;
		}
			
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPreferWEB( ComboCViewEditWEB, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPreferWEB==nil)
		{
			CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPreferWEB*");
			break;
		}
			
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			CAlert::InformationAlert("No pudo Obtener dropListData*");
			
			break;
		}
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
			break;
		}
			
		dropListDataWEB->Clear(kFalse, kFalse);
		dropListData->Clear(kFalse, kFalse);
			
		InterfacePtr<IWorkspace> piWorkspace(GetExecutionContextSession()->QueryWorkspace());
		if(piWorkspace==nil)
		{
			CAlert::InformationAlert("No pudo Obtener piWorkspace*");
			break;
		}
			
		InterfacePtr<IPDFExptStyleListMgr> piPDFExptStyleListMgr((IPDFExptStyleListMgr *) piWorkspace->QueryInterface(IID_IPDFEXPORTSTYLELISTMGR));
		if(piPDFExptStyleListMgr==nil)
		{
			CAlert::InformationAlert("No pudo Obtener piPDFExptStyleListMgr*");
			break;
		}
			
		int32 indexFromEditorial=0;
		int32 indexFromEditorialWEB=0;
		for(int32 i=0;i<piPDFExptStyleListMgr->GetNumStyles(); i++)
		{
			PMString pName="";
			ErrorCode er=piPDFExptStyleListMgr->GetNthStyleName (i, &pName);
			if(er==kSuccess)
			{
				dropListData->AddString(pName, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
				dropListDataWEB->AddString(pName, IStringListControlData::kEnd, kFalse, kFalse);
			}
				
			if(pName==PresetToPDFeditorial)
			{
				indexFromEditorial=i;
			}
			
			if(pName==PresetToPDFeditorialWEB)
			{
				indexFromEditorialWEB=i;
			}
		}
			
		if(dropListData->Length()>0)//si se lleno con almenos un path el combo
		{
			if(indexFromEditorial>=0)
				IDDLDrComboBoxSelecPrefer->Select(indexFromEditorial);
			else
				IDDLDrComboBoxSelecPrefer->Select(0);
		}
			
			
			
		if(dropListDataWEB->Length()>0)//si se lleno con almenos un path el combo
		{
			if(indexFromEditorial>=0)
				IDDLDrComboBoxSelecPreferWEB->Select(indexFromEditorialWEB);
			else
				IDDLDrComboBoxSelecPreferWEB->Select(0);
		}
	}while(false);
	
}

void N2PsqlActionComponent::LlenarComboPreferenciasOnDlg(IControlView* CVDialogNuevaPref,PMString NomPref)
{
	PMString Archivo;
		
	PMString Arc;
	int posicion;
	do
	{
			
		InterfacePtr<IWidgetParent>		myParent(CVDialogNuevaPref, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
			
			
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			break;
		}
			
			
			
		InterfacePtr<IControlView>		CVComboBoxSelecPrefer(panel->FindWidget(kN2PsqlDataBaseComboWidgetID), UseDefaultIID() );
		if(CVComboBoxSelecPrefer==nil)
		{
			break;
		}
			
			
		InterfacePtr<IStringListControlData> dropListData(CVComboBoxSelecPrefer, UseDefaultIID());
		if (dropListData == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		///borrado de la lista al inicializar el combo
			
		dropListData->Clear(kFalse, kFalse);//lIMPIA LA LISTA ACTUAL
			
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////COMENTARIO ES MUY EXTRAÑO QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	    PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
			
			
			
		//remoeve los ultimos dos puntos
	    PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
			
  			
		if(PathPMS.IsEmpty() == kTrue)
		{
			break;
		}
		SDKFileHelper rootFileHelper(PathPMS);
		IDFile rootSysFile = rootFileHelper.GetIDFile();
		PlatformFileSystemIterator iter;
		
		iter.SetStartingPath(rootSysFile);
			
		IDFile sysFile;
		bool16 hasNext= iter.FindFirstFile(sysFile,"//*.*");
		while(hasNext)
		{
			SDKFileHelper fileHelper(sysFile);
			PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
			
			if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
			{
				truncP=N2PSQLUtilities::TruncateExtencion(truncP);
				dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
			}
			hasNext= iter.FindNextFile(sysFile);
		}
			
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( CVComboBoxSelecPrefer, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
				
			break;
		}
			
		if(dropListData->Length()>0)
		{
			posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
			if(posicion!=-1)
				dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
			posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
			if(posicion!=-1)
				dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
				
				
				
			posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
				
			if(posicion<0)
			{
				IDDLDrComboBoxSelecPrefer->Select(0);
			}
			else
			{
				IDDLDrComboBoxSelecPrefer->Select(posicion);
			}
		}
		
	}while(false);
}

void N2PsqlActionComponent::OpenAsignarIssues()
{
	InterfacePtr<ICheckInOutSuite> checkOut(static_cast<ICheckInOutSuite*> (CreateObject
																			(
																			 kCheckInOutSuiteBoss,	// Object boss/class
																			 ICheckInOutSuite::kDefaultIID
																			 )));
	
	// If we couldn't get a valid pointer, report that:
	if (checkOut == nil)
	{
		ASSERT_FAIL("HWMMenuComponent::DoCheckInPage: checkOut invalid");
	}
	else
	{
		checkOut->OpenDialogAsignaIssue(); // Call method to display message.
	}
}
//  Code generated by DollyXS code generator
