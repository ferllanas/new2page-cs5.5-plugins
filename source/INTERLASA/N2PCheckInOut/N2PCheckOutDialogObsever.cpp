/*
//	File:	CheckOutDialogObserver.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "IWidgetParent.h"
#include "ITextControlData.h"
#include "IApplication.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"


// General includes:
#include "CAlert.h"
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "N2PCheckInOutListBoxHelper.h"
#include "Utils.h"

// Project includes:
#include "N2PCheckInOutID.h"
#ifdef MACINTOSH
	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif

/** CheckOutDialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author Juan Fernando Llanas Rdz
*/
class CheckOutDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CheckOutDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~CheckOutDialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

	private:
		
		PMString BuscarEnUsuarios(PMString Busqueda, PMString Campo);
		
		void LLenar_Combo_Seccion();
		
		K2Vector<PMString> VectorIdSeccion;
		
		int32 GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(CheckOutDialogObserver, kCheckOutDialogObserverImpl)

/* AutoAttach
*/
void CheckOutDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to N2PCheckInOut's info button widget.
		AttachToWidget(kCheckInOutIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);

		AttachToWidget(kBuscarButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kComboBoxIssueWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void CheckOutDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from N2PCheckInOut's info button widget.
		DetachFromWidget(kCheckInOutIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kBuscarButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kComboBoxIssueWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void CheckOutDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		
		if (theSelectedWidget == kComboBoxIssueWidgetID && theChange == kPopupChangeStateMessage)
		{
			this->LLenar_Combo_Seccion();
		}
		
		if (theSelectedWidget == kCheckInOutIconSuiteWidgetID && theChange == kTrueStateMessage)
		{
			// Bring up the About box.
			ASSERT_FAIL(kN2PkCInOutAboutBoxStringKey);
		}
		else
		{	if (theSelectedWidget == kBuscarButtonWidgetID && theChange == kTrueStateMessage)
			{
				PMString IDUsuario_Actual="";
				
				InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
				if (wrkSpcPrefs == nil)
				{	
					ASSERT_FAIL("Invalid workspace prefs in CheckOutDialogObserver::Update()");
					break;
				}
				IDUsuario_Actual= wrkSpcPrefs->GetIDUserLogedString();
				
				InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
				PMString nameApp = app->GetApplicationName();
				nameApp.SetTranslatable(kFalse);
				
				InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
				if(myParent==nil)
				{
					break;
				}

				InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
				if(panel==nil)
				{
					break;
				}

				InterfacePtr<IControlView>		CBoxUsuario( panel->FindWidget(kComboBoxUsuarioWidgetID), UseDefaultIID() );
				if(CBoxUsuario==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<IControlView>		CBoxStatus( panel->FindWidget(kComboBoxStatusWidgetID), UseDefaultIID() );
				if(CBoxStatus==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<IControlView>		CBoxSeccion( panel->FindWidget(kComboBoxSeccionWidgetID), UseDefaultIID() );
				if(CBoxSeccion==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<IControlView>		CBoxIssue( panel->FindWidget(kComboBoxIssueWidgetID), UseDefaultIID() );
				if(CBoxIssue==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}


				InterfacePtr<IControlView>		CBoxDate( panel->FindWidget(kDatePubliWidgetID), UseDefaultIID() );
				if(CBoxDate==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<IControlView>		CBoxPage( panel->FindWidget(kPaginaWidgetID), UseDefaultIID() );
				if(CBoxPage==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}
			
				InterfacePtr<ITextControlData>		TextDataPage( CBoxPage, UseDefaultIID() );
				if(TextDataPage==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<ITextControlData>		TextDataDate( CBoxDate, UseDefaultIID() );
				if(TextDataDate==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<ITextControlData>		TextDataIssue( CBoxIssue, UseDefaultIID() );
				if(TextDataIssue==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<ITextControlData>		TextDataUsuario( CBoxUsuario, UseDefaultIID() );
				if(TextDataUsuario==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<ITextControlData>		TextDataStatus( CBoxStatus, UseDefaultIID() );
				if(TextDataStatus==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}

				InterfacePtr<ITextControlData>		TextDataSeccion( CBoxSeccion, UseDefaultIID() );
				if(TextDataSeccion==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}
				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
				if(SQLInterface==nil)
				{
					ASSERT_FAIL("SQLInterface = nil");
					break;
				}
				
				PMString Usuario=TextDataUsuario->GetString();
				PMString Status=TextDataStatus->GetString();
				PMString Seccion=TextDataSeccion->GetString();
				PMString Issue=TextDataIssue->GetString();
				
				PMString Date=TextDataDate->GetString();
				PMString Page=TextDataPage->GetString();
				
				/*CAlert::InformationAlert("1");
				int32 indexOfIssue = this->GetIdexSelectedOfComboBoxWidgetID(kComboBoxIssueWidgetID, Issue);
				int32 indexOfSeccion = this->GetIdexSelectedOfComboBoxWidgetID(kComboBoxSeccionWidgetID, Seccion);
				CAlert::InformationAlert("2");
				if(VectorIdSeccion.)
				Seccion = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",VectorIdSeccion[indexOfSeccion]);
				CAlert::InformationAlert("3");*/
				
				
				InterfacePtr<IControlView>		iControViewList( panel->FindWidget(kPaginasListBoxWidgetID), UseDefaultIID() );
				if(iControViewList==nil)
				{
					ASSERT_FAIL("No se encontro el editbox");
					break;
				}
				
				//se declara crea una lista 
				SDKListBoxHelper listHelper(iControViewList, kN2PCheckInOutPluginID,kPaginasListBoxWidgetID);
				listHelper.EmptyCurrentListBox();	

				/*********************************************************/
				
				if(Date.NumUTF16TextChars()>0)
				{
					Date.Append("/");
					PMString *MM=Date.GetItem("/",1);
					if(MM==nil)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaValidaKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}
					
					/*if(MM->GetAsNumber()>0 && MM->GetAsNumber()<13)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaNoExisteStrKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}*/
					
					PMString *DD=Date.GetItem("/",2);
					
					if(DD==nil)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaValidaKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}
					
					/*if(DD->GetAsNumber()>0 && DD->GetAsNumber()<32)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaNoExisteStrKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}*/
					
					PMString *Anno=Date.GetItem("/",3);
					
					if(Anno==nil)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaValidaKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}
					
					/*if(Anno->GetAsNumber()>0)
					{
						PMString NoNotas=PMString(kN2PCheckInOutNoSeFechaNoExisteStrKey);
						NoNotas.Translate();
						NoNotas.SetTranslatable(kTrue);
						CAlert::InformationAlert(NoNotas);
						break;
					}*/
					
					Date="";
					if(Anno->NumUTF16TextChars()>0)
					{	
						Date.Append(Anno->GrabCString());
						
						Date.Append("-");
					}
					if(MM->NumUTF16TextChars()>0)
					{
						
						Date.Append(MM->GrabCString());
						Date.Append("-");
					}
					if(DD->NumUTF16TextChars()>0)
					{
						Date.Append(DD->GrabCString());
					}
					
				}
				
				
				
				if(nameApp.Contains("InDesign"))
				{
					nameApp="InDesign CS2";
				}
				else
				{
					nameApp="InCopy CS2";
				}
				
				//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogObserver::Update call(CreateAndProcessOpenDocCmd)");

				PMString Busqueda=" CALL BuscaPaginas2 ('" + IDUsuario_Actual + "' , '"  + Page + "' , '" + Date + "' , '" + Seccion + "' , '" + Issue + "' , '" + Usuario + "' , '" + Status + "', '" + nameApp + "',1)";

				//CAlert::InformationAlert(Busqueda);
				////// Obtiene Preferencias para conexion /////////////
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
			
				if(UpdateStorys==nil)
				{
					break;
				}
		
				PreferencesConnection PrefConections;
		
				if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
				{
					break;
				}
				

		
				PMString StringConection="";
		
				StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
				
			
				K2Vector<PMString> QueryVector;
				
				SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
				
				if(QueryVector.Length()<=0)
				{
					PMString NoNotas=PMString(kN2PCheckInOutNoSeEncontraronPaginasPlugInMenuKey);
					NoNotas.Translate();
  					NoNotas.SetTranslatable(kTrue);
					CAlert::InformationAlert(NoNotas);
					break;
				}
				
				// cuando regresa solo un registro
				if(QueryVector.Length()==1)
				{
					
					//CAlert::InformationAlert(QueryVector[0]);
					PMString Privilegio = SQLInterface->ReturnItemContentbyNameColumn("Privilegio",QueryVector[0]);
					if(Privilegio.NumUTF16TextChars()>0)//Pregunta el rtegitro trae el campo con el nombre 'Privilegio'
					{
						//entonces no tiene privilegios para ver las paginas de esta seccion
						CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
					}
					else
					{// si no entonces es una regirtro con datos de una pagina
						for(int32 i=0;i<QueryVector.Length();i++)
						{
							PMString Nombre_Archivo = SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",QueryVector[i]);
							PMString Secc = SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
							PMString Estado = SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
							PMString Folio_Pagina = SQLInterface->ReturnItemContentbyNameColumn("ID",QueryVector[i]);
							PMString Id_SeccionText = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
							PMString Id_PublicacionText = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
							listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
						}
					}
					
					//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogObserver::Update call(despues del llenado de lista de paginas)");

				}
				else
				{//si no entonces trae muchas registeros de paginas.
					for(int32 i=0;i<QueryVector.Length();i++)
					{
						PMString Nombre_Archivo=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",QueryVector[i]);
						PMString Secc=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
						PMString Estado=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
						PMString Folio_Pagina=SQLInterface->ReturnItemContentbyNameColumn("ID",QueryVector[i]);
						PMString Id_SeccionText =SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
						PMString Id_PublicacionText=SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
						listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
					}
					
					//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogObserver::Update call(CreateAndProcessOpenDocCmd)");

				}
				
			}
		}
	} while (false);
}


PMString CheckOutDialogObserver::BuscarEnUsuarios(PMString Busqueda, PMString Campo)
{

	PMString cadena;

	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn(Campo,QueryVector[i]);
			cadena.SetTranslatable(kFalse);
		}
	}while(false);
	
	
	return(cadena);
}

//  Generated by Dolly build 17: template "Dialog".
// End, CheckOutDialogObserver.cpp.

void CheckOutDialogObserver::LLenar_Combo_Seccion()
{
	do
	{
	
		
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::LLenar_Combo_SeccionNo pudo Obtener panelControlData");
			break;
		}
		
		IControlView * ComboCViewPubli=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if (ComboCViewPubli == nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::LLenar_Combo_SeccionComboCViewPubli*");
			break;
		}
		
		InterfacePtr<ITextControlData> TextControlPubli(ComboCViewPubli,ITextControlData::kDefaultIID);
		if (TextControlPubli == nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::LLenar_Combo_SeccionTextControlPubli*");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::LLenar_Combo_Seccion ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::LLenar_Combo_SecciondropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		PMString Busqueda="SELECT   Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion = (SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + TextControlPubli->GetString() + "' ) ORDER BY Nombre_de_Seccion ASC";
		///borrado de la lista al inicializar el combo
		
	
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("CheckOutDialogObserver::LLenar_Combo_Seccion IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		//PMString DSNName= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		VectorIdSeccion = QueryVector;
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}

		
		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

	}while(false);
}

int32 CheckOutDialogObserver::GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
		
		/*InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
			
		retvalIndex = IDDLDrComboBoxSelecPrefer->GetSelected() 
		*/
	}while(false);
	return(retvalIndex);
}