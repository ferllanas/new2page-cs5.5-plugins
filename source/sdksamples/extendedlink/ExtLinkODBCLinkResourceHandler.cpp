//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkODBCLinkResourceHandler.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "ExtLinkODBCLinkResourceHandler.h"

#include "IPMStream.h"
#include "IURIUtils.h"
#include "IExtLinkFacade.h"
#include "CoreFileUtils.h"
#include "AString.h"
#include "FileTypeInfo.h"
#include "FileUtils.h"
#include "IDFile.h"
#include "StreamUtil.h"
#include "URI.h"
#include "Utils.h"
#include "WideString.h"
#include "ExtLinkID.h"
#include "ExtLinkConst.h"
#include "ISDKODBCWrapper.h"
#include "SDKODBCCache.h"
#include "ILinkManager.h"

#ifdef MACINTOSH
#include "MacFileUtils.h"
#endif

CREATE_PMINTERFACE(ExtLinkODBCLinkResourceHandler, kExtLinkODBCLinkResourceHandlerImpl)


//========================================================================================
//
// ExtLinkODBCLinkResourceHandler Public Implementation
//
//========================================================================================

//========================================================================================
// Constructor
//========================================================================================
ExtLinkODBCLinkResourceHandler::ExtLinkODBCLinkResourceHandler(IPMUnknown* boss)
: inherited(boss),
	mBuffer(nil)
{
}

//========================================================================================
// Destructor
//========================================================================================
ExtLinkODBCLinkResourceHandler::~ExtLinkODBCLinkResourceHandler()
{
	delete [] mBuffer;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::Init
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::Init(const UIDRef& ref, const URI& uri)
{
	return true;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::IsResourceURIValid
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::IsResourceURIValid(const UIDRef& ref, const URI& uri) const
{
	const WideString myScheme(kExtLinkScheme);

	WideString uriScheme(uri.GetComponent(URI::kScheme));
	if (uriScheme != myScheme) {
		return false;
	}

	if (SDKODBCCache::Instance().IsCacheExisted(uri))
		return true;
	else
		return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::AreResourceIdsEquivalent
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::AreResourceIdsEquivalent(const ILinkResource::ResourceId& id1, const ILinkResource::ResourceId& id2) const
{
	return (id1 == id2);
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::GetResourceDataType
//========================================================================================
FileTypeInfo ExtLinkODBCLinkResourceHandler::GetResourceDataType(const UIDRef& ref, const URI& uri) const
{
	TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::GetResourceDataType In\n");
	IDFile file;
	if (!Utils<IExtLinkFacade>()->URIToIDFile(uri, file)) {
		TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::GetResourceDataType Out\n");
		// the URI does not represent a file, it is a text field in the database
		return FileTypeInfo();
	}

	AString extension(file.GetName().GetExtension());

#ifdef MACINTOSH
	AOSType creator;
	AOSType type;
	MacFileUtils::GetCreatorAndType(file, creator, type);
	TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::GetResourceDataType Out\n");
	return FileTypeInfo(type, creator, PMString(extension.GrabWString(), extension.UTF16Count()), "", 0);
#else
	TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::GetResourceDataType Out\n");
	return FileTypeInfo(0, 0, PMString(extension.GrabWString(), extension.UTF16Count()), "", 0);
#endif
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::GetShortResourceName
//========================================================================================
WideString ExtLinkODBCLinkResourceHandler::GetShortResourceName(const UIDRef& ref, const URI& uri, bool) const
{
	WideString shortName;
	if (uri.HasComponent(URI::kQuery))
	{
		WideString sku = uri.GetComponent(URI::kQuery);
		shortName.Append(sku);
	}
	if (uri.HasComponent(URI::kFragment))
	{
		WideString field = uri.GetComponent(URI::kFragment);
		shortName.Append(WideString(":"));
		shortName.Append(field);
	}
	return shortName;

//	AString name(file.GetNameStr());
//	return WideString(name.GrabWString(), name.UTF16Count(), name.CharCount());
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::GetLongResourceName
//========================================================================================
WideString ExtLinkODBCLinkResourceHandler::GetLongResourceName(const UIDRef& ref, const URI& uri, bool) const
{
	WideString dbPath = uri.GetComponent(URI::kPath);
	WideString longName;
	longName.Append(dbPath);
	if (uri.HasComponent(URI::kQuery))
	{
		WideString sku = uri.GetComponent(URI::kQuery);
		longName.Append(WideString("/"));
		longName.Append(WideString(sku));
		if (uri.HasComponent(URI::kFragment))
		{
			WideString field = uri.GetComponent(URI::kFragment);
			longName.Append(WideString(":"));
			longName.Append(field);
		}
	}
	return longName;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::AreStampsEquivalent
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::AreStampsEquivalent(const WideString& stamp1, const WideString& stamp2) const
{
	if (stamp1 == stamp2) {
		return true;
	}

	// TODO
	return (stamp1 == stamp2);
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanReadResource
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanReadResource(const UIDRef& ref, const URI& uri) const
{
	TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::CanReadResource In\n");
	return  SDKODBCCache::Instance().IsCacheExisted(uri);
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanWriteResource
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanWriteResource(const UIDRef& ref, const URI& uri) const
{
	return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanReadWriteResource
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanReadWriteResource(const UIDRef& ref, const URI& uri) const
{
	return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanCreateResourceStream
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanCreateResourceStream(const UIDRef& ref, const URI& uri, ILinkResource::AccessMode mode) const
{
	TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::CanCreateResourceStream In\n");
	bool odbcConnectNormal = false;
	odbcConnectNormal =  SDKODBCCache::Instance().IsCacheExisted(uri);
	switch (mode) {
		case ILinkResource::kRead:
			return odbcConnectNormal;
		case ILinkResource::kWrite:
			break;
		case ILinkResource::kReadWrite:
			break;
		default:
			ASSERT_FAIL("ExtLinkODBCLinkResourceHandler::CanCreateResourceStream() - Unrecognized access mode!");
			break;
	}

	TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::CanCreateResourceStream Out\n");
	return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CreateResourceReadStream
//========================================================================================
IPMStream* ExtLinkODBCLinkResourceHandler::CreateResourceReadStream(const UIDRef& ref, const URI& uri) const
{
	TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::CreateResourceReadStream In\n");
	WideString	newValue;
	IDFile	imageFile;
	if (Utils<IExtLinkFacade>()->URIToIDFile(uri, imageFile) == true)
	{
		if (!imageFile.GetAttribute(IDFile::kReadable)) {
			return nil;
		}

	#ifdef MACINTOSH
		AOSType creator;
		AOSType type;
		MacFileUtils::GetCreatorAndType(imageFile, creator, type);
		return InterfacePtr<IPMStream>(StreamUtil::CreateFileStreamReadLazy(imageFile, kOpenIn, type, creator)).forget();
	#else
		return InterfacePtr<IPMStream>(StreamUtil::CreateFileStreamReadLazy(imageFile, kOpenIn)).forget();
	#endif
	}
	else if (Utils<IExtLinkFacade>()->GetAssetValueFromURI(uri, newValue))
	{
		URI	data(newValue);
		PMString newDataToStream(newValue);
		int32 len(newDataToStream.WCharLength());

		if (mBuffer)
		{
			delete [] mBuffer;
			mBuffer = nil;
		}
		mBuffer = new char[len+1];


		InterfacePtr<IPMStream> outStream(Utils<IExtLinkFacade>()->CreateExtLinkPointerStreamWrite((char*)mBuffer, len+1));
		const uchar kInternalNewLine='\n';
		
		
	#ifdef MACINTOSH
			uchar kExternalNewLine=kuchar_LF;
	#else
			uchar kExternalNewLine=kuchar_CR;
	#endif

		for (int32 i = 0; i < newDataToStream.CharCount(); i++)
		{
			PlatformChar pc = newDataToStream[i];
			// if dual-byte char, output as is, highbyte first.
			if (pc.IsTwoByte()) 
			{
				uchar	hb = pc.HighByte(), 
						lb = pc.LowByte();
				outStream->XferByte(&hb, 1);
				outStream->XferByte(&lb, 1);
			}
			else // non-dualbyte char
			{
				uchar c = pc.GetAsOneByteChar();
					outStream->XferByte(&c, 1);
			}
		}
		len = outStream->Seek(0, kSeekFromCurrent);
		return InterfacePtr<IPMStream> (StreamUtil::CreatePointerStreamRead((char*)mBuffer, len)).forget();
	}
	TRACEFLOW("ExtLink", "ExtLinkODBCLinkResourceHandler::CreateResourceReadStream Out\n");
		
	return nil;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CreateResourceWriteStream
//========================================================================================
IPMStream* ExtLinkODBCLinkResourceHandler::CreateResourceWriteStream(const UIDRef& ref, const URI& uri) const
{
	return nil;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CreateResourceReadWriteStream
//========================================================================================
IPMStream* ExtLinkODBCLinkResourceHandler::CreateResourceReadWriteStream(const UIDRef& ref, const URI& uri) const
{
	return nil;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanCopyToFile
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanCopyToFile(const UIDRef& ref, const URI& uri) const
{
	return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CopyToFile
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceHandler::CopyToFile(const UIDRef& ref, const URI& uri, IDFile& file) const
{
// *** TODO

//	IDFile rsrcFile;
//	if (!Utils<IExtLinkFacade>()->URIToIDFile(uri, rsrcFile))
		return kFailure;
	
//	return CoreFileUtils::CopyFile(rsrcFile, file) ? kSuccess : kFailure;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanEditResource
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanEditResource(const UIDRef& ref, const URI& uri) const
{
//	ASSERT_UNIMPLEMENTED(); 
	// TODO
	return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::EditResource
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceHandler::EditResource(const UIDRef& ref, const URI& uri, const AppInfo& appInfo, PMString* errorString) const
{
//	ASSERT_UNIMPLEMENTED(); 
	// TODO
	return kFailure;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanRevealResource
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanRevealResource(const UIDRef& ref, const URI& uri) const
{
//	ASSERT_UNIMPLEMENTED(); 
	// TODO
	return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::RevealResource
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceHandler::RevealResource(const UIDRef& ref, const URI& uri) const
{
//	ASSERT_UNIMPLEMENTED(); 
	// TODO
	return kFailure;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanRevealResourceInBridge
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanRevealResourceInBridge(const UIDRef& ref, const URI& uri) const
{
//	ASSERT_UNIMPLEMENTED(); 
	// TODO
	return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::RevealResourceInBridge
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceHandler::RevealResourceInBridge(const UIDRef& ref, const URI& uri) const
{
//	ASSERT_UNIMPLEMENTED(); 
	// TODO
	return kFailure;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::CanRevealResourceInMiniBridge
//========================================================================================
bool ExtLinkODBCLinkResourceHandler::CanRevealResourceInMiniBridge(const UIDRef& ref, const URI& uri) const
{
	//	ASSERT_UNIMPLEMENTED(); 
	// TODO
	return false;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::RevealResourceInMiniBridge
//========================================================================================
ErrorCode ExtLinkODBCLinkResourceHandler::RevealResourceInMiniBridge(const UIDRef& ref, const URI& uri) const
{
	//	ASSERT_UNIMPLEMENTED(); 
	// TODO
	return kFailure;
}

//========================================================================================
// ExtLinkODBCLinkResourceHandler::AllocateBuffer
//========================================================================================
void ExtLinkODBCLinkResourceHandler::AllocateBuffer(const int32 length)
{
	if (mBuffer)
	{
		delete [] mBuffer;
		mBuffer = nil;
	}
	mBuffer = new char[length];
}
