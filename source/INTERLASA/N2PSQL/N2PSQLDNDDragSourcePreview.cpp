//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/basicdragdrop/N2PSQLDNDDragSourceOfPreview.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPageItemScrapData.h"
#include "IPathUtils.h"
#include "IPMDataObject.h"
#include "IDragDropController.h"
#include "IGraphicAttributeUtils.h"
#include "ISwatchUtils.h"
#include "ISwatchList.h"
#include "ILayoutUtils.h"
#include "ITreeNodeIDData.h"
#include "ISysFileListData.h"
#include "IDataExchangeHandler.h"
#include "ILayoutControlData.h"
#include "IControlView.h"
#include "IPanorama.h"
#include "IGeometry.h"
#include "ILayoutUtils.h"
#include "IPageItemLayerData.h"
#include "IPanelControlData.h"
#include "IListBoxController.h"

// Interface includes:
#include "ITreeNodeIDData.h"
#include "IDragDropController.h"
#include "IDataExchangeHandler.h"
#include "ISysFileListData.h"
#include "IPMDataObject.h"


// General includes:
#include "CDragDropSource.h"
#include "SDKFileHelper.h"
//#include "DocumentID.h"
#include "IPalettePanelUtils.h"

// General includes:
#include "CDragDropSource.h"
#include "UIDList.h"
#include "CmdUtils.h"
#include "UIDRef.h"
#include "CmdUtils.h"
#include "PMFlavorTypes.h"
#include "SDKFileHelper.h"
#include "CAlert.h"
#include "ILayoutUIUtils.h"
#include "ISysFileData.h"

// Project includes:
#include "N2PSQLID.h"
#include "N2PSQLPhotoListBoxHelper.h"

/** The flavor this source will create. If we want to use a custom flavor, we need to also define a data exchange handler. */





/** 
	Provides drag capability. Most of the functionality is implemented in the 
	parent (helper) implementation class.

	@ingroup basicdragdrop
	@author Jane Bond
*/
class N2PSQLDNDDragSourceOfPreview : public CDragDropSource
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSQLDNDDragSourceOfPreview(IPMUnknown* boss);
		/**
			we will only allow the drag if there is an existing document. We faux the creation of a page item,
			this simplifies the database requirements.
			@param e IN the event signifying the start of the drag.
			@return kTrue if we will drag from this location, otherwise kFalse.
		*/
		bool16 			WillDrag(IEvent* e) const;
		/**
			we override this method to add the content we want to drag from this widget.
			@param DNDController IN the controller in charge of this widget's DND capabilities.
			@return kTrue if valid content has been added to the drag
		*/
		bool16			DoAddDragContent(IDragDropController* DNDController);

		/** In our case we have a custom drag item, so the target does not know how to draw feedback for this item.
			Therefore we assume responsibility for this.
			@return the SysRgn representing the feedback we want to convey.
		*/
		//SysWireframe 			DoMakeDragOutlineRegion() const;

	private:
		

		/** Gets the SysFile associated with this widget.
		@param contentFile [OUT] specifies associated SysFile.
		@return kTrue if a draggable SysFile can be identified, kFalse otherwise.
	 */
	bool16 getContentFile(IDFile& contentFile) const;


	/** Adds kPMSysFileFlavor data to the drag describing the SysFile associated
		with this widget.
		@param controller [IN] in charge of this widget's DND capabilities.
		@return kTrue if valid content has been added to the drag, kFalse otherwise.
	 */
	bool16 doAddSysFileDragContent(IDragDropController* controller);
	
	//ErrorCode CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight);
	
	UIDRef fPageItem;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2PSQLDNDDragSourceOfPreview, kN2PSQLDNDDragSourcePreviewImpl)

/* N2PSQLDNDDragSourceOfPreview Constructor
*/
N2PSQLDNDDragSourceOfPreview::N2PSQLDNDDragSourceOfPreview(IPMUnknown* boss)
: CDragDropSource(boss)
{
}

/*
	We indicate we are only interested in dragging if there is a document open. This is making the assumption that we
	want to drag the item onto the layout widget, but also allows us to create the page item within the context of that document.
*/
bool16 N2PSQLDNDDragSourceOfPreview::WillDrag(IEvent* e) const
{
	
	IDFile contentFile;
	IDocument* theFrontDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
	if (theFrontDoc == nil)
	{
		return kFalse;
	}
	
	return this->getContentFile(contentFile);
}

/* getContentFile
*/
bool16 N2PSQLDNDDragSourceOfPreview::getContentFile(IDFile& contentFile) const
{
	bool16 result = kFalse;
	do 
	{
		
		InterfacePtr<IControlView>		iControView(this, UseDefaultIID() );
		if(iControView==nil)
		{
			CAlert::InformationAlert("iControView");
			break;
		}
		
		InterfacePtr<ISysFileData> 	iSysFileData(iControView, IID_ISYSFILEDATA);
		ASSERT(iSysFileData);
		if(!iSysFileData)
		{
			CAlert::InformationAlert("iSysFileData");
			break;
		}
		
		contentFile= iSysFileData->GetSysFile();

		SDKFileHelper fileHelp(contentFile);
		if(fileHelp.IsExisting())
		{
			//CAlert::InformationAlert("Existe este archivo pues chido");
			result=kTrue;
		}
		
		
		
		//
		
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPhotoPanelWidgetID)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("  N2PSQLDNDDragSourceOfPreview::getContentFile  No se encontro el editbox");
			break;
		}

		InterfacePtr<IControlView>		iControViewList( panel->FindWidget(kN2PsqlPhotoListBoxWidgetID), UseDefaultIID() );
		if(iControViewList==nil)
		{
			CAlert::InformationAlert("  N2PSQLDNDDragSourceOfPreview::getContentFile iControViewList");
			break;
		}
		
		N2PSQLPhotoListBoxHelper N2PSQLPhotoListHelper(iControViewList, kN2PSQLPluginID,kN2PsqlPhotoListBoxWidgetID, kN2PsqlPhotoPanelWidgetID);
		
		//Index del elemento en la lista de fotos que pertenece al widget de la foto que se desea dragear 
		int32 index = N2PSQLPhotoListHelper.SelectElemListOfPictureDraged(iControView); //funcion que selecciona(coloca el Focus) el elemento de a lista que se esta drageando
		//PMString SAQWE="";
		//SAQWE.AppendNumber(index);
		//CAlert::InformationAlert("ZASX"+ SAQWE);
	} while(false);
	return result;
}



/*
	We override the DoAddDragContent method to define the content for the drag, 
	in this case a simple spline item is added. Note, this is just a simple 
	implementation for demonstration purposes. 
*/
bool16 N2PSQLDNDDragSourceOfPreview::DoAddDragContent(IDragDropController* DNDController)
{
	return this->doAddSysFileDragContent(DNDController);
}


/* doAddSysFileDragContent
*/
bool16 N2PSQLDNDDragSourceOfPreview::doAddSysFileDragContent(
	IDragDropController* controller)
{
	bool16 result = kFalse;
	
	
	InterfacePtr<IDragDropController> ddController(Utils<ILayoutUIUtils>()->GetFrontDocument(), IID_IDRAGDROPCONTROLLER);
#ifdef MACINTOSH 
	controller->SetTrackingCursorFeedback(CursorSpec(kCrsrPlaceTextSnap));	
#endif
	do
	{
		// Stop if we can't determine the SysFile we are associated with.
		IDFile contentFile;
		if (this->getContentFile(contentFile) == kFalse)
		{
			CAlert::InformationAlert(" N2PSQLDNDDragSourceOfPreview::doAddSysFileDragContent getContentFile");
			break;
		}

		// Point the controller at the handler.
		InterfacePtr<IDataExchangeHandler> 		dataExchangeHandler(controller->QueryHandler(N2PSQLCustomFlavorFile));
		ASSERT(dataExchangeHandler);
		if (!dataExchangeHandler)
		{
			//CAlert::InformationAlert("dataExchangeHandler nil");
			CAlert::InformationAlert(" N2PSQLDNDDragSourceOfPreview::doAddSysFileDragContent dataExchangeHandler");
			break;
		}
		if (dataExchangeHandler->IsEmpty() == kFalse)
		{
			dataExchangeHandler->Clear();
		}
		controller->SetSourceHandler(dataExchangeHandler);

		// Add the SysFile to be dragged.
		InterfacePtr<ISysFileListData> 
			sysFileListData(dataExchangeHandler, IID_ISYSFILELISTDATA);
		ASSERT(sysFileListData);
		if (!sysFileListData)
		{
			CAlert::InformationAlert(" N2PSQLDNDDragSourceOfPreview::doAddSysFileDragContent sysFileListData");
			break;
		}
		sysFileListData->Append(contentFile);
			
		// Indicate the flavour in the drag object. 
		InterfacePtr<IPMDataObject>			pmDataObject(controller->AddDragItem(1));
		ASSERT(pmDataObject);
		if(pmDataObject==nil)
		{
			CAlert::InformationAlert(" N2PSQLDNDDragSourceOfPreview::doAddSysFileDragContent IPMDataObject");
			break;
		}
		PMFlavorFlags flavorFlags=kNormalFlavorFlag;
		pmDataObject->PromiseFlavor(N2PSQLCustomFlavorFile, flavorFlags);

		result = kTrue;

	} while(false);

	return result; 
}


/* As we have a custom flavour, the drag target is unlikely to know how to draw feedback, therfore we draw it in the source */
/*SysWireframe
N2PSQLDNDDragSourceOfPreview::DoMakeDragOutlineRegion() const
{
	// do while(false) loop for error control
	do 
	{
		InterfacePtr<ILayoutControlData> LayoutData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if(LayoutData==nil)
		{
			CAlert::InformationAlert(" N2PSQLDNDDragSourceOfPreview::DoMakeDragOutlineRegion LayoutData");
			break;
		}
			
		
		InterfacePtr<IControlView> LayoutControlView(LayoutData,UseDefaultIID());
		if(LayoutControlView==nil)
		{
			CAlert::InformationAlert(" N2PSQLDNDDragSourceOfPreview::DoMakeDragOutlineRegion LayoutControlView");
			break;
		}
			
		
		InterfacePtr<IPanorama> Panorama(LayoutControlView,UseDefaultIID());
		if(Panorama==nil)
		{
			CAlert::InformationAlert(" N2PSQLDNDDragSourceOfPreview::DoMakeDragOutlineRegion Panorama");
			break;
		}
			
		
		PMReal XScale=Panorama->GetXScaleFactor();
		
		
		
		// get the geometry of the page item
		//InterfacePtr<IGeometry> iGeometry(fPageItem,UseDefaultIID());
		//if (iGeometry == nil)
		//{
		//	//CAlert::InformationAlert("No geometry on scrap item!");
		//	CAlert::InformationAlert(" N2PSQLDNDDragSourceOfPreview::DoMakeDragOutlineRegion iGeometry");
		//	break;
		//}
		
		// we will draw a rectangle around the bounding box
		PMRect thePIGeo = iGeometry->GetStrokeBoundingBox();
	
		// we need to draw the bounding box at the current mouse location
		SysPoint currentMouse = GetMousePosition();
		PMPoint start(currentMouse);
		
		PMPoint lTop(thePIGeo.LeftTop());
		PMPoint rBottom(thePIGeo.RightBottom());

		PMReal xStart=start.X();
		PMReal yStart=start.Y();
		
		lTop=PMPoint(xStart+5,yStart);
		
		rBottom=PMPoint((xStart+5)+(thePIGeo.Width()*XScale),yStart+(thePIGeo.Height()*XScale));
		// create a new pmrect based on the page item, offset from the mouse location
		// On windows this feedback resorts to the default, on the mac it would be 
		PMRect offsetRect(lTop,rBottom);

		SysRect windowRect = ::ToSys(offsetRect);
		SysWireframe origRgn = ::CreateRectSysWireframe(windowRect);
		
		// Get the window region for the rectangle.
		::InsetSysRect(windowRect,0,0);
		
		SysWireframe windowRgn = ::CreateRectSysWireframe(windowRect);
		
		DiffSysWireframe(origRgn,windowRgn,origRgn);
		//::DeleteSysWireframe(windowRgn);
		return origRgn;
	}
	while (kFalse);
	return nil;
}
*/



/* this method takes in a points list, stroke weight and parent UIDRef and creates a spline with those points, the stroke weight
	and attaches it to the parent.

ErrorCode N2PSQLDNDDragSourceOfPreview::CreatePageItem(UIDRef& newPageItem,UIDRef parent,PMPointList points,PMReal strokeWeight)
{
	ErrorCode returnValue = kFailure;
	do 
	{
	
		PMRect bounds(0,0,200,150);
		// create the page item
		UIDRef newSplineItem = Utils<IPathUtils>()->CreateRectangleSpline(parent, bounds, INewPageItemCmdData::kGraphicFrameAttributes);
		
		// put the new item into a splinelist
		const UIDList splineItemList(newSplineItem);
		
		newPageItem = newSplineItem;
		newPageItem = newSplineItem;
		returnValue=kSuccess;
	} while(false);
	return returnValue;
}*/
// End, N2PSQLDNDDragSourceOfPreview.cpp.






