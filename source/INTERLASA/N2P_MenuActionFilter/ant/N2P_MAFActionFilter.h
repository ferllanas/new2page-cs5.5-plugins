//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/customactionfilter/N2P_MAFActionFilter.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #2 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __CstAFltActionFilter__
#define __CstAFltActionFilter__

#include "IActionFilter.h"

/** Filters actions in a configurable way, with the	help of ActionFilterHelper.
 * 
 * 	@ingroup customactionfilter
 * 	@author Ken Sadahiro
*/
class N2P_MAFActionFilter : public CPMUnknown<IActionFilter>
{
public:
	/** Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.
	*/
	N2P_MAFActionFilter(IPMUnknown* boss);

	/** Destructor.
	*/
	virtual ~N2P_MAFActionFilter(void)
	{
	}

	/** FilterAction
		@see IActionFilter
	*/
	virtual void FilterAction(ClassID* componentClass, 
							  ActionID* actionID,   
							  PMString* actionName,
							  PMString* actionArea, 
							  int16* actionType, 
							  uint32* enablingType, 
							  bool16* userEditable);

	/** Sets up filters for this particular implementation.
	 * 	Change the code to suit your needs.
	 */
	static void ConfigureFilters(void);

};
#endif // __CstAFltActionFilter__
