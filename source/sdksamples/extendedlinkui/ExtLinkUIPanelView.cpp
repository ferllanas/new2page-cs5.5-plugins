//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIPanelView.cpp $
//  
//  Owner: Susan Doan
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "PalettePanelView.h"
#include "ExtLinkUIID.h"
#include "IBoolData.h"
#include "IPanelControlData.h"
#include "IExtLinkUIDataModel.h"
#include "ILinkManager.h"


/** Implements IControlView; overrides the draw routine for the panel containing our
	links tree view.

	
	@ingroup extendedlinkui

*/
class ExtLinkUIPanelView : public PalettePanelView
{
public:
	/**
	 Constructor.
	 @param boss interface ptr from boss object on which this interface is aggregated.
	 */
	ExtLinkUIPanelView(IPMUnknown *boss);

	/**
	 Destructor.
	 */
	virtual ~ExtLinkUIPanelView();

	/**
	 The Draw method is overridden in order to allow drawing only when the Links model is not changing.
	 If the model is changing when Draw is called, IID_EXTLINKPANELSHOULDINVALDATA on the panel is set
	 to true, triggering an Inval call on the panel when the model change is complete.
	 */
	virtual void Draw(IViewPort* viewPort, SysRgn updateRgn);
};


CREATE_PERSIST_PMINTERFACE(ExtLinkUIPanelView, kExtLinkUIPanelViewImpl)


/* Constructor
*/
ExtLinkUIPanelView::ExtLinkUIPanelView(IPMUnknown *boss) : 
	PalettePanelView(boss)
{
}


/* Destructor
*/
ExtLinkUIPanelView::~ExtLinkUIPanelView()
{
}


/* Draw
*/
void ExtLinkUIPanelView::Draw(IViewPort* viewPort, SysRgn updateRgn)
{
	InterfacePtr<IBoolData> drawDisabledFlag(this,IID_EXTLINKPANELDRAWINGDISABLED);

	// if the links mgr is changing model data at this time, disable drawing
	InterfacePtr<IPanelControlData> panelData(this, UseDefaultIID());
	if(panelData) 
	{
		IControlView* treeView = panelData->FindWidget(kExtLinkUITreeViewWidgetID); 
		if (treeView) 
		{
			InterfacePtr<const IExtLinkUIDataModel> model(treeView, UseDefaultIID());
			if (model) 
			{
				IDataBase* docDB = model->GetCurrentDocDB();
				if (docDB) 
				{
					InterfacePtr<const ILinkManager> linkMgr(docDB, docDB->GetRootUID(), UseDefaultIID());
					if(linkMgr->IsModelChanging()) 
					{
						drawDisabledFlag->Set(kTrue);
					}
				}
			}
		}
	}
				
	if (!drawDisabledFlag->GetBool()) {
		PalettePanelView::Draw(viewPort, updateRgn);
	} 
	else
	{
		// drawing is disabled, and we were asked to draw. Make sure we inval when drawing is turned on again.
		InterfacePtr<IBoolData> invalWhenPossibleData(this, IID_EXTLINKPANELSHOULDINVALDATA);
		invalWhenPossibleData->Set(kTrue);
	}
}


