//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/customprefs/CstPrfScriptingDefs.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2003/12/18 11:20:39 $
//  
//  $Revision: #1 $
//  
//  $Change: 237988 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//  
//  Defines Scripting IDs used by the CstPrf plug-in.
//  
//========================================================================================

#ifndef __N2PInOutScriptingDefs_h__
#define __N2PInOutScriptingDefs_h__

#ifdef WINDOWS
#include "ScriptingWindowDefs.h"
#endif

#define kN2PUsr_CLSID { 0xbd4188d5, 0x984d, 0x4894, { 0x8a, 0xf0, 0x7d, 0x88, 0x42, 0xf4, 0x9b, 0x8a } }
DECLARE_GUID( N2PUsr_CLSID, kN2PUsr_CLSID );
// {BD4188D5-984D-4894-8AF0-7D8842F49B8A}
//static const GUID <<name>> = 
//
// { 0x8d448fe0, 0x8194, 0x11d3, { 0xa6, 0x53, 0x0, 0xe0, 0x98, 0x71, 0xa, 0xf6 } }

// Class ID. 
#define c_N2PUserReg 'N2Pu'

// Event ID. 
#define e_FlipN2PRegUsersState  'fN2P'  // state on/off event

// Property ID.
#define p_N2PUser  'sN2P'   // custom pref object
#define p_N2PVal  'pN2P'      // state on/off pref


#endif // #ifndef __CstPrfScriptingDefs_h__
