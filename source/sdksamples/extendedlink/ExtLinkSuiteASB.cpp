//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkSuiteASB.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IIntegratorTarget.h"

// Implementation includes:
#include "CPMUnknown.h"
#include "SelectionASBTemplates.tpp"

// Project includes:
#include "IExtLinkSuite.h"
#include "ExtLinkID.h"


/** IExtLinkSuite implementation for the abstract integrator suite. 
	Uses templates provided by the API to delegate calls to IExtLinkSuite
	implementations on underlying concrete selection boss
	classes.

	@ingroup extendedlink
	@see IExtLinkSuite
*/
class ExtLinkSuiteASB : public CPMUnknown<IExtLinkSuite>
{
public:
	/**
		Constructor.
		@param boss interface ptr from boss object on 
		which this interface is aggregated.
	*/
	ExtLinkSuiteASB(IPMUnknown* boss);

	/** See IExtLinkSuite
	*/
	virtual bool16 CanInsertData() const;

	/** See IExtLinkSuite
	*/
	virtual ErrorCode InsertSelectedRecords(const URIList& uriList);
};

/*	Makes the implementation available to the application.
*/
CREATE_PMINTERFACE(ExtLinkSuiteASB, kExtLinkSuiteASBImpl)


/* Constructor
*/
ExtLinkSuiteASB::ExtLinkSuiteASB(IPMUnknown* boss) :
	CPMUnknown<IExtLinkSuite>(boss)
{
}


/*
*/
bool16 ExtLinkSuiteASB::CanInsertData() const
{
    return (AnyCSBSupports(make_functor(&IExtLinkSuite::CanInsertData), this, IID_IExtLinkSUITE));
}


/*
*/
ErrorCode ExtLinkSuiteASB::InsertSelectedRecords(const URIList& uriList)
{
    return (Process (make_functor(&IExtLinkSuite::InsertSelectedRecords, uriList), this, IID_IExtLinkSUITE));
}


// end:	PrnSelSuiteASB.cpp

