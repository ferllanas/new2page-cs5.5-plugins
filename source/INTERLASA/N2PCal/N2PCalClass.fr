//========================================================================================
//DATE: 10/09/08
//========================================================================================

#include "VCPlugInHeaders.h"


#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "CommandID.h"
#include "WorkspaceID.h"			// needed for IID_ISTRINGREGISTER
#include "WidgetID.h"				// needed for kAutoStringRegisterImpl

#include "TextPanelID.h"
#include "BuildNumber.h"
#include "PlugInModel_UIAttributes.h"

#include "PanelList.fh"
#include "Widgets.fh"				// for PalettePanelWidget
#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"

#include "GenericID.h"
#include "ShuksanID.h"
#include "TextID.h"

// Project includes:
#include "N2PCalID.h"


#ifdef __ODFRC__

#include "Schema.fh"

/*---------------------Plugin version definition------------------------*/
resource PluginVersion (kSDKDefPluginVersionResourceID)
{
	kTargetVersion,
	kN2PCalPluginID,
	kSDKDefPlugInMajorVersionNumber, kSDKDefPlugInMinorVersionNumber,
	kSDKDefHostMajorVersionNumber,   kSDKDefHostMinorVersionNumber,
	kN2PCalCurrentMajorFormatNumber, kN2PCalCurrentMinorFormatNumber,
	{ kInDesignProduct, kInCopyProduct},
	{ kWildFS },
	kUIPlugIn,
	kN2PCalVersion
};



/*------------------------Boss class definition.-------------------------*/

resource ClassDescriptionTable(kSDKDefClassDescriptionTableResourceID)
{{{
	
	/* This boss class supports two interfaces: IActionComponent and IPMPersist.@ingroup n2pcalendario*/
	
	Class
	{
		kN2PCalActionComponentBoss,
		kInvalidClass,
		{
			/** Handle the actions from the menu. */
			IID_IACTIONCOMPONENT, kN2PCalActionComponentImpl,
			/** Persist the state of the menu across application instantiation.
			Implementation provided by the API.*/
			IID_IPMPERSIST, kPMPersistImpl
		}
	},

	/*	 This boss class inherits from an API panel boss class, and
		 adds an interface to control a pop-up menu on the panel.
		 The implementation for this interface is provided by the API.
		 @ingroup n2pcalendario */

	Class
	{
		kN2PCalPanelWidgetBoss,
		kPalettePanelWidgetBoss,
		{
			/** The plug-in's implementation of ITextControlData with an exotic IID of IID_IPANELMENUDATA.
			Implementation provided by the API.*/
			IID_IPANELMENUDATA, kCPanelMenuDataImpl,
		}
	},


			
	/*	 This boss class implements the dialog for this plug-in. All
		 dialogs must implement IDialogController. Specialisation of
		 IObserver is only necessary if you want to handle widget
		 changes dynamically rather than just gathering their values
		 and applying in the IDialogController implementation.
		 In this implementation IObserver is specialised so that the
		 plug-in's about box is popped when the info button widget
		 is clicked.  @ingroup n2pcalendario */
	
	Class
	{
		kN2PCalDialogBoss,
		kDialogBoss,
		{
			/** Provides management and control over the dialog.*/		
			IID_IDIALOGCONTROLLER, kN2PCalDialogControllerImpl,
			
			/** Allows dynamic processing of dialog changes.*/
			IID_IOBSERVER, kN2PCalDialogObserverImpl,
				
			/** TableCellData is an interface for an implementation that manages the data in TableCell.*/
			IID_IN2PCALTABLECELLDATA, kN2PCalTableCellDataImpl,		
			
			/** CelPnlPanelView restrict the size of the panel when a panel draws.*/
			IID_IINTEGERCONTROLDATA,	kCIntegerControlDataImpl,
		}
	},

	/** The Boss Class extending kTextEditBoxWidgetBoss.@ingroup cellpanel*/
	
	Class
	{
		kN2PCalTextEditBoxWidgetBoss,
		kTextEditBoxWidgetBoss,
		{
			/** The plug-in's implementation of an event handler,
				extending PlatformEditBoxEventHandler.*/
			IID_IEVENTHANDLER,	kN2PCalEditBoxEventHandlerImpl,	
			
			/** The plug-in's implementation.This observer is empty! Edit this method to observe the EditBox widget.*/
			IID_IOBSERVER, kN2PCalEditBoxObserverImpl,		
		}
	},
	
	/**	The Boss Class for the table cell widget.@ingroup cellpanel*/
	
	Class
	{
		kN2PCalTableCellWidgetBoss,
		kBaseWidgetBoss,
		{
			/** This implementation draws the table and the cell contents.*/
			IID_ICONTROLVIEW, 			kN2PCalTableCellViewImpl,					
			
			/** When the user clicks on the table,this implementation calculates the coordinate of the clicked cell in the table.*/
			IID_IEVENTHANDLER,			kN2PCalTableCellEventHImpl,				
			
			/** This implementation handles the updating of the scroll bars and updates
				the table cell widget (kN2PCalTableCellWidgetBoss).	*/
			IID_IOBSERVER,				kN2PCalTableCellObserverImpl,				
		}
	}
	
	/** Adds observer to our child of the kRollOverIconButtonWidgetBoss.
		@ingroup pictureicon */
	
	Class 
	{
		kICalRollOverIconButtonObserverBoss,
		kRollOverIconButtonBoss,
		{
			/*Handles button press (and release), see PicIcoRollOverButtonObserver.*/
			IID_IOBSERVER, kICalRollOverButtonObserverImpl,
		}
	},

}}};

#endif

//End, N2PCalClass.Fr