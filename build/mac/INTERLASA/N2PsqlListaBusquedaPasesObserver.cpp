/*
//	File:	N2PsqlListaBusquedaPasesObserver.cpp
//
//	Date:	5-Mar-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
*/
#include "VCPlugInHeaders.h"

// Implementation includes
#include "WidgetID.h"

// Interface includes
#include "ISubject.h"
#include "IControlView.h"
#include "IListControlData.h"
#include "IListBoxController.h"
#include "IApplication.h"
// Implem includes
#include "CAlert.h"
#include "CObserver.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"

#include "N2PsqlID.h"
#include "N2PSQLListBoxHelper.h"
#include "N2PSQLUtilities.h"


/**

	Implements IObserver. The intent of this class is to provide handling for the AutoAttach (sent when shown), AutoDetach (hidden)
	and Update (when listbox hit by end-user, for instance) message
	
	 The class is derived from CObserver, and overrides the
	AutoAttach(), AutoDetach(), and Update() methods.
	This class implements the IObserver interface using the CObserver helper class,
	and is listening along the IID_ILISTCONTROLDATA protocol for changes in the list-data model.

	@author Ian Paterson
	@version API Level 1.5
*/
class N2PsqlListaBusquedaPasesObserver : public CObserver
{
public:
	
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	N2PsqlListaBusquedaPasesObserver(IPMUnknown *boss);

	/**
		Destructor for N2PsqlListaBusquedaPasesObserver class - 
		performs cleanup 
	*/	
	~N2PsqlListaBusquedaPasesObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 

		This class is interested in changes along IID_ILISTCONTROLDATA protocol with classID of
		kListSelectionChangedByUserMessage. This message is sent when a user clicks on an element
		in the list-box.

	
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	/**
		Helper method to change state of list-box, dependent on whether
		it is being shown or hidden.
		@param isAttaching specifies whether attaching (shown) or detaching (hidden)
	*/
	void updateListBox(bool16 isAttaching);
	
	
	
	
};

CREATE_PMINTERFACE(N2PsqlListaBusquedaPasesObserver, kN2PsqlListaResultadoPasesBoxObserverImpl)


N2PsqlListaBusquedaPasesObserver::N2PsqlListaBusquedaPasesObserver(IPMUnknown* boss)
: CObserver(boss)
{
	
}


N2PsqlListaBusquedaPasesObserver::~N2PsqlListaBusquedaPasesObserver()
{
	
}


void N2PsqlListaBusquedaPasesObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());// IID_ISUBJECT);
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_ILISTCONTROLDATA);
	}
	//updateListBox(kTrue);
}


void N2PsqlListaBusquedaPasesObserver::AutoDetach()
{
	updateListBox(kFalse);
	InterfacePtr<ISubject> subject(this, UseDefaultIID());//IID_ISUBJECT);
	if (subject != nil)
	{
		subject->DetachObserver(this, IID_ILISTCONTROLDATA);
	}
}


void N2PsqlListaBusquedaPasesObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	if ((protocol == IID_ILISTCONTROLDATA) && (theChange == kListSelectionChangedByUserMessage) ) {
		do {
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			ASSERT(app);
			if (app == nil) {	
				break;
			}
		
			PMString nameApp = app->GetApplicationName();
			
			N2PSQLListBoxHelper listHelper(this, kN2PsqlPluginID,kN2PsqlPasesListBoxWidgetID);
			IControlView * listBox = listHelper.FindCurrentListBox();
			if(listBox == nil) {
				break;
			}
			InterfacePtr<IListBoxController> listCntl(listBox,IID_ILISTBOXCONTROLLER); // kDefaultIID not def'd
			ASSERT_MSG(listCntl != nil, "listCntl nil");
			if(listCntl == nil) {
				break;
			}
			//  Get the first item in the selection (should be only one if the list box
			// has been set up correctly in the framework resource file)
			PMString IdElemPadre="";
			PMString Seccion="";
			PMString Estado="";
			K2Vector<int32> multipleSelection ;
			listCntl->GetSelected( multipleSelection ) ;
			const int kSelectionLength =  multipleSelection.Length() ;
			if (kSelectionLength> 0 )
			{
				
				int indexSelected = multipleSelection[0];
				listHelper.ObtenerTextoDeSeleccionActual(kN2PsqlIDPaseLabelWidgetID,IdElemPadre,kN2PsqlContenidoPaselLabelWidgetID,Seccion,indexSelected);
				
				//CAlert::InformationAlert(Seccion);
				//
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextNotaWidgetID,Seccion);
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_ContentNota_WidgetID,IdElemPadre);
				
				PMString Busqueda= "EXEC QueryElementsOfPases '" + IdElemPadre +"'";
			
				N2PSQLUtilities::RealizarBusqueda_Y_llenado(Busqueda, kTrue);
				
				//Para ver si existen 
				Busqueda=" SELECT Id_Elemento  ";
 				Busqueda.Append(" FROM Elemento  Where Id_elemento_padre='");
 				Busqueda.Append(IdElemPadre);
 				Busqueda.Append("' AND Id_tipo_ele=4");
 				if(nameApp.Contains("InDesign"))
 				{
 					Busqueda.Append(" AND Mostrar_Indesign=1");
 				}
 				N2PSQLUtilities::MuestraIconodeCamaraSiNotaTieneFotos(Busqueda);
 			
 				Busqueda="CALL N2PV1055_QueryFotoFromElemPadre (" + IdElemPadre + ")" ;
 				N2PSQLUtilities::RealizarBusqueda_Y_llenadoDeListaPhoto(Busqueda);
			}
		} while(0);
	}
}


 
void N2PsqlListaBusquedaPasesObserver::updateListBox(bool16 isAttaching)
{
	N2PSQLListBoxHelper listHelper(this, kN2PsqlPluginID,kN2PsqlNotasListBoxWidgetID);
	listHelper.EmptyCurrentListBox();
	// See if we can find the  listbox
	// If we can then populate the listbox in brain-dead way
	if(isAttaching) {
		do {
				const int targetDisplayWidgetId = kN2PsqlNotaLabelWidgetID;
				const int kNumberItems = 12;
				for(int i=0; i < kNumberItems; i++) {
					PMString name("mio");
					name.AppendNumber(i+1);
					listHelper.AddElement(name, targetDisplayWidgetId);
				}
		   } while(0);
	}
}




