//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkExportHandler.cpp $
//  
//  Owner: robin briggs
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//  
//  Purpose:
//  Handler for exporting XML, called for every element of the structure tree.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// ----- Interfaces -----

#include "IIDXMLElement.h"
#include "IXMLAttributeCommands.h"
#include "IXMLExportHandler.h"
#include "IXMLGenerator.h"

#include "ExtLinkID.h"
#include "ExtLinkConst.h"

/** 
	allows plugins to handle certain elements differently during export. We implemented this so we can customize our export tags.
	@ingroup extendedlink
	
*/
class ExtLinkExportHandler : public CPMUnknown<IXMLExportHandler>
{
	public:
		ExtLinkExportHandler(IPMUnknown *boss);
		
		/**	@see IXMLExportHandler::CanHandleElement
		 */
		virtual bool16 CanHandleElement(IIDXMLElement *element);
		/**	@see IXMLExportHandler::CanHandleElement
		 */
		virtual bool16 HandleElementBefore(IXMLOutStream* stream, IIDXMLElement *element);
		/**	@see IXMLExportHandler::HandleElementBefore
		 */
		virtual bool16 HandleAfterNthChild(IXMLOutStream* stream, IIDXMLElement *element, int32 childIndex);
		/**	@see IXMLExportHandler::HandleAfterNthChild
		 */
		virtual bool16 HandleElementAfter(IXMLOutStream* stream, IIDXMLElement *element);
		
	private:
};


CREATE_PMINTERFACE(ExtLinkExportHandler, kExtLinkExportHandlerImpl)


ExtLinkExportHandler::ExtLinkExportHandler(IPMUnknown *boss) :
	CPMUnknown<IXMLExportHandler>(boss)
{
}

bool16 ExtLinkExportHandler::CanHandleElement(IIDXMLElement *element)
{
	WideString tagString(element->GetTagString());
	
	if (tagString == kExtLinkKeyTag || tagString == kExtLinkFieldTag)
		return kTrue;

	return kFalse;
}

bool16 ExtLinkExportHandler::HandleElementBefore(IXMLOutStream* stream, IIDXMLElement *element)
{
	ASSERT_MSG(stream != nil, "ExtLinkExportHandler::HandleElementBefore -- stream is nil!");

	WideString tagString(element->GetTagString());
	if (tagString != kExtLinkFieldTag)
	{ // we try to omit the field tags 
		return kFalse;
	}
	

	InterfacePtr<IXMLGenerator> generator(element, UseDefaultIID());
	if (generator)
		generator->GenerateContent(stream);


	return kTrue;
}

//
//	ExtLinkExportHandler::HandleAfterNthChild
//
//	Called after each child is iterated. This is the place to do things in
//		between child elements
//
//	Parameters:
//		element		-> main/root/parent element
//		childIndex	-> index of element's child that was just processed
//
bool16 ExtLinkExportHandler::HandleAfterNthChild(IXMLOutStream* stream, IIDXMLElement *element, int32 childIndex)
{
	// For every child of the record (field), we need a "; " to separate (except for the last child)
	WideString tagString(element->GetTagString());
	if (tagString == kExtLinkKeyTag)
	{ 
		if (element->GetChildCount() - 1 == childIndex)
			stream->Write(WideString(), kFalse);
		else
			stream->Write(WideString(", "), kFalse);

		return kTrue;
	}
	
	return kFalse;
}

bool16 ExtLinkExportHandler::HandleElementAfter(IXMLOutStream* stream, IIDXMLElement *element)
{
	// I actually don't want closing tag for field element for exporting too
	WideString tagString(element->GetTagString());
	if (tagString == kExtLinkFieldTag)
	{ // we try to omit the field tags 
		return kTrue;
	}

	return kFalse;
}

