//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/detailcontrollistsize/DCLSizPalettePanelView.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interfaces:
#include "IPanelControlData.h"

// Includes:
#include "ErasablePanelView.h"
#include "DCLSizID.h"
#include "IDCLSizPanelOptions.h"
// Utility files:
#include "IPalettePanelUtils.h"

/**
	Implements IControlView for the panel, based on ErasablePanelView. This is required to 
	support the correct behaviour on ConstrainDimensions being sent to the view.

	@ingroup detailcontrollistsize
	
*/
class  DCLSizPalettePanelView : public ErasablePanelView
{
public:
	/**
		Constructor.
		@param boss interface ptr from boss object on which this interface is being aggregated.
	*/
	DCLSizPalettePanelView(IPMUnknown* boss);

	/**
		Destructor.
	*/
	virtual ~DCLSizPalettePanelView();
	

	/**
		Override ErasablePanelView::ConstrainDimensions. This ensures that the list box
		does not become smaller when the list-element size is reduced.
		@param dimensions set up the dimension through this out-parameter.
	*/
	virtual PMPoint	ConstrainDimensions(const PMPoint& dimensions) const;

private:
	static const int kMinDCLSizPalWidth;
	static const int kMaxDCLSizPalWidth;
	static const int kMinDCLSizPalHeight;
	static const int kMaxDCLSizPalHeight;
};


const int DCLSizPalettePanelView::kMinDCLSizPalWidth	=	207;
const int DCLSizPalettePanelView::kMaxDCLSizPalWidth	=	10000;
const int DCLSizPalettePanelView::kMinDCLSizPalHeight	=	(19 * 2) + 17;
const int DCLSizPalettePanelView::kMaxDCLSizPalHeight	=	10000;


/* CREATE_PERSIST_PMINTERFACE
 Binds the C++ persistent implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PERSIST_PMINTERFACE(DCLSizPalettePanelView, kDCLSizPalettePanelViewImpl)

/* Constructor
*/
DCLSizPalettePanelView::DCLSizPalettePanelView(IPMUnknown* boss) :
	ErasablePanelView(boss) 
{
}

/* Destructor
*/
DCLSizPalettePanelView::~DCLSizPalettePanelView()
{
}


/* ConstrainDimensions
*/
PMPoint DCLSizPalettePanelView::ConstrainDimensions(const PMPoint& dimensions) const
{
	PMPoint constrainedDim = dimensions;

	// Width can vary if not above maximum or below minimum
	if(constrainedDim.X() > kMaxDCLSizPalWidth) 
	{
		constrainedDim.X(kMaxDCLSizPalWidth);
	}
	else
		if(constrainedDim.X() < kMinDCLSizPalWidth) 
		{
			constrainedDim.X(kMinDCLSizPalWidth);
		}

	// Height can vary if not above maximum or below minimum
	if(constrainedDim.Y() > kMaxDCLSizPalHeight) 
	{
		constrainedDim.Y(kMaxDCLSizPalHeight);
	}
	else 
	{
		if(constrainedDim.Y() < kMinDCLSizPalHeight) 
		{
			constrainedDim.Y(kMinDCLSizPalHeight);
		}
	}

	InterfacePtr<IPanelControlData> panelData(this, UseDefaultIID());
	
	IControlView* treeWidget = panelData->FindWidget(kDCLSizListBoxWidgetID);
	if(treeWidget != nil) 
	{
		// The list-box widget itself may further constrain the dimensions, so give it the chance.
		constrainedDim = treeWidget->ConstrainDimensions(constrainedDim);	
	}
	return constrainedDim;
}

// End, DCLSizPalettePanelView.cpp.

