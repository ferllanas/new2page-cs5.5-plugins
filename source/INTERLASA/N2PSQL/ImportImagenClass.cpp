#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"

// Interface includes:
#include "ISession.h"


#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IImportProvider.h"
#include "IPMStream.h"
#include "IDocument.h"
#include "ILayoutUIUtils.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IPathUtils.h"
#include "IGeometry.h"
#include "ICommand.h"
#include "IHierarchyCmdData.h"
#include "IStoreInternal.h"
#include "ITextModel.h"
#include "INewPageItemCmdData.h"
#include "ICreateMCFrameData.h"
#include "IPageItemTypeUtils.h"
#include "IObjectModel.h"
#include "IPlacePIData.h"
#include "IDocument.h"
#include "ILayoutUIUtils.h"
#include "IDataLinkHelper.h"
#include "IDataLink.h"
#include "IAddLinkCmdData.h"

// General includes:

#include "SDKFileHelper.h"
#include "IPlaceGun.h"
#include "IUIDData.h"
//#include "IImportFileCmdData.h"

#include "IImportResourceCmdData.h"
#include "IURIUtils.h"

#include "K2Vector.h"
#include "K2Vector.tpp"
#include "StreamUtil.h"
#include "TransformUtils.h"
#include "CmdUtils.h"
#include "OpenPlaceID.h"
#include "LinksID.h"
#include "TextID.h"
#include "SplineID.h"
#include "CAlert.h"
#include "SDKLayoutHelper.h"
#include "ImportImagenClass.h"
#include "FileUtils.h"

/*
	Called by SnippetRunner framework to parse parameters and 
	call ImportImagenClass. Helper methods are provided
	by the SnipRun class to tokenise the parameter string and
	convert parameters of various types.
	@param params string containing parameters for the code snippet
	@return kFailure if parameter parsing failed, ErrorCode returned by snippet otherwise.
*/
ImportImagenClass::ImportImagenClass()
{
}

ImportImagenClass::~ImportImagenClass()
{
}



/* ImportImageAndLoadPlaceGun
*/
UIDRef ImportImagenClass::ImportImageAndLoadPlaceGun(const UIDRef& docUIDRef, const PMString& fromPath)
{
	// Precondition: active document
	// Precondition: file exists
	// 
	UIDRef retval = UIDRef::gNull;
	do
	{
		IDFile idFile;
		FileUtils::PMStringToIDFile(fromPath,idFile);
		SDKFileHelper fileHelper(idFile);
		bool16 fileExists = fileHelper.IsExisting();
		// Fail quietly... ASSERT(fileExists);
		if(!fileExists)
		{
			break;
		}
		InterfacePtr<IDocument> 
		iDocument(docUIDRef, UseDefaultIID());
		ASSERT(iDocument);
		if(!iDocument)
		{
			break;
		}
		
		// Wrap the import in a command sequence.
		//PMString seqName("Import");
		//CmdUtils::SequenceContext seq(&seqName);
		//seq=kFailure;
		
		// If the place gun is already loaded abort it so we can re-load it. 
		InterfacePtr<IPlaceGun> placeGun(iDocument, UseDefaultIID());
		ASSERT(placeGun);
		if (!placeGun) 
		{
			break;
		}
		if (placeGun->IsLoaded())
		{
			InterfacePtr<ICommand> abortPlaceGunCmd( CmdUtils::CreateCommand(kAbortPlaceGunCmdBoss));
			ASSERT(abortPlaceGunCmd);
			if (!abortPlaceGunCmd)
			{
				break;
			}
			InterfacePtr<IUIDData> uidData(abortPlaceGunCmd, UseDefaultIID());
			ASSERT(uidData);
			if (!uidData) 
			{
				break;
			}
			uidData->Set(placeGun);
			if (CmdUtils::ProcessCommand(abortPlaceGunCmd) != kSuccess) 
			{
				ASSERT_FAIL("kAbortPlaceGunCmdBoss failed");
				break;
			}
		}
		
		IDataBase* db = docUIDRef.GetDataBase();
		ASSERT(db);
		if(!db)
		{
			break;
		}
		
		// Now import the selected file and load it's UID into the place gun.
		InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
		ASSERT(importCmd);
		if (!importCmd) {
			break;
		}
		
		InterfacePtr<IImportResourceCmdData> data(importCmd, IID_IIMPORTRESOURCECMDDATA);
		if (data == nil)
			break;
		
		//IDFile myfile(fromPath);
		URI fileURI;
		Utils<IURIUtils>()->IDFileToURI(idFile, fileURI);
		
		data->Set(db, fileURI, K2::kSuppressUI);  
		
		ErrorCode err = CmdUtils::ProcessCommand(importCmd);
		
		ASSERT(err == kSuccess);
		if(err != kSuccess)
		{
			break;
		}	
		// Get the contents of the place gun as our return value
		UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
		retval = placedItem; 
		//seq.SetState(kSuccess);
	} while(kFalse);
	
	return retval;
}


/*
CreateAndProcessPlaceItemInGraphicFrameCmd(const UIDRef& docUIDRef,const UIDRef& itemToPlace, const UIDRef& graphicFrame)
*/
void ImportImagenClass::CreateAndProcessPlaceItemInGraphicFrameCmd(const UIDRef& docUIDRef,const UIDRef& itemToPlace, const UIDRef& graphicFrame)
{
	do
	{
		// Create a PlaceItemInGraphicFrameCmd:
		InterfacePtr<ICommand>
		placeCmd(CmdUtils::CreateCommand(kPlaceItemInGraphicFrameCmdBoss));
		// Set the PlaceItemInGraphicFrameCmd's ItemList:
		placeCmd->SetItemList(UIDList(itemToPlace));
		// Get an IPlacePIData Interface for the PlaceItemInGraphicFrameCmd:
		InterfacePtr<IPlacePIData> placeData(placeCmd, IID_IPLACEPIDATA);
		// Set the IPlacePIData Interface's data (don't use PlaceGun in thisexample):
		placeData->Set(graphicFrame, nil, kFalse);
		// Process the PlaceItemInGraphicFrameCmd:
		if (CmdUtils::ProcessCommand(placeCmd) != kSuccess)
		{
			CAlert::ErrorAlert("Error al intentar importar la imagen");
		}
		else
		{
			InterfacePtr<ICommand> clearPlaceGun(CmdUtils::CreateCommand(kClearPlaceGunCmdBoss));
             ASSERT(clearPlaceGun);
             InterfacePtr<IUIDData> docUIDData(clearPlaceGun, UseDefaultIID());
            ASSERT(docUIDData);
             if(!docUIDData) {
                 break;
             }
             docUIDData->Set(docUIDRef);
             
             if(CmdUtils::ProcessCommand(clearPlaceGun)!=kSuccess)
             {
             	break;
             }
		}
	}while(false);
}




/*
void ImportImagenClass::PlaceImageOnFrame(UIDRef docUIDRef,UIDRef parent)
{
	do
	{
		InterfacePtr<IPlaceGun> placeGun(docUIDRef, UseDefaultIID());
		ASSERT(placeGun);
		if(!placeGun)
		{
			break;
		}
		
		 InterfacePtr<IPlaceBehavior> sourceBehavior(docUIDRef.GetDataBase(),    placeGun->GetItemUID(), 
                     UseDefaultIID());
         ASSERT(sourceBehavior);
         if(!sourceBehavior) 
         {
             break;
         }
         ICursorMgr::eCursorModifierState cursorModState = (ICursorMgr::eCursorModifierState) ICursorMgr::kNoModifiers;
 
         InterfacePtr<ILayoutControlViewHelper> layoutControlViewHelper(layoutView, UseDefaultIID());// IID_ILAYOUTCONTROLVIEWHELPER);
         ASSERT(layoutControlViewHelper);
         if(!layoutControlViewHelper)
         {
             break;
         }
             
         InterfacePtr<IPlaceBehavior> targetBehavior(static_cast<IPlaceBehavior*>
				(layoutControlViewHelper-> QueryHitTestPageItemNew(currentPoint, kSolidTOPMPHitTestHandlerBoss, IID_IPLACEBEHAVIOR)));
         if(targetBehavior==nil)
		{
			break;
		}
		
         UIDRef placedItem = sourceBehavior->ProcessPlace(targetBehavior, parent, points, cursorModState, cursorModState, targetSpread); 
         
         if (placedItem.GetUID() != kInvalidUID)
         {
             InterfacePtr<ICommand> clearPlaceGun(CmdUtils::CreateCommand(kClearPlaceGunCmdBoss));
             ASSERT(clearPlaceGun);
             InterfacePtr<IUIDData> docUIDData(clearPlaceGun, UseDefaultIID());
            ASSERT(docUIDData);
             if(!docUIDData) {
                 break;
             }
             docUIDData->Set(docUIDRef);
             
             if(CmdUtils::ProcessCommand(clearPlaceGun)!=kSuccess)
             {
             	break;
             }
             
             
            
         }
			
		
		
		
		UID UIdParentOfItemPlaced=placedItem.GetUID();
	}while(false);
}

*/

/*
*/
UIDRef ImportImagenClass::ImportIntoPageItemInFrame(IDFile sysFile,UIDRef frameUIDRef)	
{
	UIDRef contentUIDRef(nil, kInvalidUID);
	do {
	
		SDKLayoutHelper helperLayout;
		PMRect boundInFrameCoords(0,0,100,100);
		PMRect boundInParentCoords=helperLayout.PageToSpread(frameUIDRef,boundInFrameCoords);
		UIFlags uiFlags=K2::kMinimalUI;
		
		
		contentUIDRef = helperLayout.PlaceFileInFrame(sysFile,frameUIDRef,boundInParentCoords,uiFlags,kFalse,kFalse,kFalse);


	/*	//Miercoles 22 de Diciembre del 2004
		//Este codigo fue reemplazado por el de la parte superior para aumentar el tiempo de importado de los avisos
		//
		ErrorCode status = kFailure;
							
		CmdUtils::SequencePtr commandSequence(status);//creo una secuencia de comandos
							
								
		//CAlert::ErrorAlert("15");
		IDocument* iDocument = ::GetFrontDocument(); //obtengo la interfaz del documento
		ASSERT(iDocument);
		if(!iDocument) 
		{
			//CAlert::ErrorAlert("2");
			break;
		}
		
		//obtiene un interfaz IDataBase del documento de enfrente
		IDataBase* db = ::GetDataBase(iDocument);
					
		

		//Obtengo los UIFlags
		const UIFlags flags = kSuppressUI; 
		//crea una interfaz del tipo IPMStream a partir de la direccion del archivo imagen
		//A continuacion se da una serie de pasos para importar y asignar un UIDref a la imagen 
		// de la direccion determinada
		
		//CAlert::ErrorAlert("18");
		InterfacePtr<IPMStream> fileStream(SDKUtilities::CreateFileStreamRead(SDKUtilities::SysFileToPMString(&sysFile), kOpenIn));
							
		ASSERT(fileStream != nil);
		if (fileStream == nil) 
		{		
			break;
		}
		//CAlert::ErrorAlert("19");
									
		fileStream->Seek(0, kSeekFromStart);
									
		int32 count = 0;

									//
		while (fileStream->GetStreamState() == kStreamStateGood) 
		{
			uchar tmp;
			fileStream->XferByte(tmp);
			if (fileStream->GetStreamState() == kStreamStateEOF) 
			{
				break;
			}
			count++;
		}
		//CAlert::ErrorAlert("20");
		//	if (count > 500000) 
		//{
		//	CAlert::ErrorAlert("5");
		//	break;
		//}
			
		fPointerStreamBuffer = new uchar[count];
		fileStream->Seek(0, kSeekFromStart);
		int32 index = 0;
		while (fileStream->GetStreamState() == kStreamStateGood) 
		{
			uchar tmp;
			fileStream->XferByte(tmp);
			if (fileStream->GetStreamState() == kStreamStateEOF) 
			{
				break;
			}
			fPointerStreamBuffer [index++] = tmp;
		}
		
		//CAlert::ErrorAlert("21");
							
		InterfacePtr<IPMStream> pointerStream(StreamUtil::CreatePointerStreamRead((char*)fPointerStreamBuffer, count));
		fContentStream = pointerStream;
		ImportProviders importProviders;
		if (GetImportProviders(importProviders) != kSuccess) 
		{	
			//CAlert::ErrorAlert("No import provider for this stream, check it is a supported format");
			break;
		}
		//Aqui me quede
		//CAlert::ErrorAlert("22");
		InterfacePtr<IImportProvider> importProvider;
		for (ImportProviders::iterator iter = importProviders.begin(); iter < importProviders.end(); iter++) 
		{
			if (iter->fCompleteness == IImportProvider::kFullImport) 
			{		
				importProvider = iter->fImportProvider;
				break;
			}
		}
		//	CAlert::ErrorAlert("23");
				
		if (importProvider == nil) 
		{
			for (ImportProviders::iterator iter = importProviders.begin(); iter < importProviders.end(); iter++) 
			{
				if (iter->fCompleteness == IImportProvider::kPartialImport)
					{
						importProvider = iter->fImportProvider;
						break;
					}
			}
		}
		
		//CAlert::ErrorAlert("24");
					
		ASSERT(importProvider != nil);
		if (importProvider == nil) 
		{	
		//	CAlert::ErrorAlert("6");
			break;
		}
								
		//obtengo un UIDRef para la imagen a importar
		//se importa la imagen asignandole el UIDRef
		status = ImportContentStream(importProvider, kSuppressUI, contentUIDRef);
		ASSERT(status == kSuccess);
		if (status != kSuccess) 
		{
			//CAlert::ErrorAlert("7");
			break;
		}
		//crea y procesa el comando import y ¥place en frame
		//contentUIDRef, contenido o imagen a importar
		//frameUIDRef, frame a donde se a importar
		CreateAndProcessPlaceItemInGraphicFrameCmd(contentUIDRef,frameUIDRef);
		
		// create a datalink for the file
		InterfacePtr<IDataLinkHelper> helper(CreateObject2<IDataLinkHelper>(kDataLinkHelperBoss));
		if (helper == nil) 
		{
			ASSERT(helper); break;
		}
		InterfacePtr<IDataLink> dataLink(helper->CreateDataLink(sysFile));
		if (dataLink == nil) 
		{
			ASSERT(dataLink); break;
		}

		CreateAndProcessAddLinkCmd(db,dataLink,contentUIDRef.GetUID());

		//ajuste de la imagen segun preferencias
	
		commandSequence.SetState(status);*/
	} while (false);
	return(contentUIDRef);
} 

void ImportImagenClass::CreateAndProcessAddLinkCmd(IDataBase* destDB, IDataLink* sourceDL,UID pageItemUID)
{
	// Create an AddLinkCmd:
	InterfacePtr<ICommand> addLinkCmd(CmdUtils::CreateCommand(kAddLinkCmdBoss));
	// Get an IAddLinkCmdData Interface for the AddLinkCmd:
	InterfacePtr<IAddLinkCmdData> cmdData(addLinkCmd, IID_IADDLINKCMDDATA);
	// Set the IAddLinkCmdData Interface's data:
	cmdData->Set(destDB, sourceDL, pageItemUID);
	// Process the AddLinkCmd:
	if (CmdUtils::ProcessCommand(addLinkCmd) != kSuccess)
	{
		CAlert::InformationAlert("Canít process AddLinkCmd");
	}
}


ErrorCode ImportImagenClass::GetImportProviders(ImportProviders& importProviders)	
{
	ErrorCode status = kFailure;
	//CAlert::ErrorAlert("Scan registry for import providers that can import the stream");
	do {
		ASSERT(fContentStream != nil);
		if (fContentStream == nil) 
		{
			//CAlert::ErrorAlert("GetImportProviders 1");
			break;
		}
		// Scan the registry and add all services that can import this stream
		// to the collection of import providers capable of fully importing the data.
		InterfacePtr<IK2ServiceRegistry> serviceRegistry(GetExecutionContextSession(), UseDefaultIID());
		ASSERT(serviceRegistry != nil);
		if (serviceRegistry == nil) 
		{
			break;
		}
		const int32	numProviders = serviceRegistry->GetServiceProviderCount(kImportProviderService);
		for (int32 i = 0; i < numProviders; i++)
		{
			fContentStream->Seek(0, kSeekFromStart);
			ASSERT(fContentStream->GetStreamState() == kStreamStateGood);
			if (fContentStream->GetStreamState() != kStreamStateGood) 
			{
				break;
			}

			InterfacePtr<IK2ServiceProvider> serviceProvider(serviceRegistry->QueryNthServiceProvider(kImportProviderService, i));
			ASSERT(serviceProvider != nil);
			if (serviceProvider == nil) 
			{
				continue;
			}
			InterfacePtr<IImportProvider> importProvider(serviceProvider, IID_IIMPORTPROVIDER);
			ASSERT(importProvider != nil);
			if (importProvider == nil) 
			{
				continue;
			}
			IImportProvider::ImportAbility completeness = importProvider->CanImportThisStream(fContentStream);
			if (completeness == IImportProvider::kCannotImport) 
			{
				continue;
			}
			// Found a service that can import this format.
			importProviders.push_back(ImportProvider(completeness, importProvider));
			//SNIPLOG("importProvider boss ClassID: %s, completeness=%d", GetBossClassName(importProvider).GrabCString(), completeness);
		}
		if (importProviders.size() <= 0 ) 
		{
			//CAlert::ErrorAlert("GetImportProviders 4");
			// Didn't find any services that can import this format.
			break;
		}
		
		status = kSuccess;

	} while(false);

	return status;

}

PMString ImportImagenClass::GetBossClassName(IPMUnknown* unknown)
{
	PMString result("unknown");
	do 
	{
		if (unknown == nil) 
		{
			break;
		}
		ClassID id = ::GetClass(unknown);

		// Get the object model
		InterfacePtr <IObjectModel> objectModel(GetExecutionContextSession(), UseDefaultIID()); 
		ASSERT(objectModel); 
		if (objectModel == nil) 
		{	
			break;
		}
			
		// Get the name of the boss class
		const char* name = objectModel->GetIDName(kClassIDSpace,id.Get());
		if (name == nil) {
			// No name available so present id in hex.
			char buffer[16];
//			sprintf(buffer, "0x%x", id);
			result = buffer;
			break;
		}
		result = name;
	} while (false);
	return result;
}

ErrorCode ImportImagenClass::ImportContentStream(InterfacePtr<IImportProvider>& importProvider, UIFlags uiFlags, UIDRef& contentUIDRef)
{
	ErrorCode status = kFailure;
	//CAlert::ErrorAlert("Import the stream into an object that can handle this content");
	do {
		IDocument* iDocument = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (iDocument == nil)
		{	
			break;
		}
		// get the database
		IDataBase* db = ::GetDataBase(iDocument);
		// Place the frame in the active spread layer.
		InterfacePtr<ILayoutControlData> layoutControlData( Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutControlData == nil)
		{
			break;
		}
		InterfacePtr<IHierarchy> layerHierarchy(layoutControlData->QueryActiveLayer());
		if (layerHierarchy == nil)
		{
			break;
		}
		UIDRef fLayoutHierarchyUIDRef2(::GetUIDRef(layerHierarchy));

		ASSERT(fContentStream != nil);
		if (fContentStream == nil) 
		{	break;
		}
		ASSERT(fContentStream->IsReading() == kTrue);
		if (fContentStream->IsReading() == kFalse) 
		{
			break;
		}
		PMString format;
		contentUIDRef = UIDRef(nil, kInvalidUID);
		importProvider->ImportThis(fLayoutHierarchyUIDRef2.GetDataBase(), fContentStream, uiFlags, &contentUIDRef, nil/*no datalink*/, &format);
		ASSERT(contentUIDRef != UIDRef(nil, kInvalidUID));
		if (contentUIDRef == UIDRef(nil, kInvalidUID))
		{
			break;
		}
		InterfacePtr<IPMUnknown> unknown(contentUIDRef, IID_IUNKNOWN);
		//SNIPLOG("contentUIDRef boss class: %s, format: %s", GetBossClassName(unknown).GrabCString(), format.GrabCString());
		status = kSuccess;
	} while(false);
	return status;
}



/*
*/
ErrorCode ImportImagenClass::CreateFrame(const UIDRef& contentUIDRef, UIDRef& frameUIDRef)
{
	ErrorCode status = kFailure;
	////SNIPLOG("Create a frame for the content");
	do {
		InterfacePtr<IHierarchy> layoutHierarchy(fLayoutHierarchyUIDRef, UseDefaultIID());
		ASSERT(layoutHierarchy != nil);
		if (layoutHierarchy == nil) {
			break;
		}
		//SNIPLOG("frame's parent boss ClassID: %s", this->GetBossClassName(layoutHierarchy).GrabCString());

		// Figure out the type of content that was imported.		
		bool16 isTextContent = kFalse;
		InterfacePtr<ITextModel> textModel(contentUIDRef, UseDefaultIID());
		if (textModel != nil) {
			isTextContent = kTrue;
		}
		bool16 isGraphicContent = Utils<IPageItemTypeUtils>()->IsGraphic(contentUIDRef);

		if (isTextContent == kTrue) {
			this->CreateTextFrame(contentUIDRef, frameUIDRef);
		}
		else if (isGraphicContent == kTrue) {
			this->CreateGraphicFrame(contentUIDRef, frameUIDRef);
		}
		else {
			// Only know how to handle text and graphic content.
			//SNIPLOG("Don't know how to create a frame for this content");
			break;
		}

		ASSERT(frameUIDRef != UIDRef(nil, kInvalidUID));
		if (frameUIDRef == UIDRef(nil, kInvalidUID)) {
			break;
		}

		status = kSuccess;

	} while(false);
	return status;
}

/* 
*/
ErrorCode ImportImagenClass::CreateTextFrame(const UIDRef& contentUIDRef, UIDRef& frameUIDRef)
{
	ErrorCode status = kFailure;
	//SNIPLOG("Create text frame");
	do
	{
		// Check that the content is text.
		InterfacePtr<ITextModel> textModel(contentUIDRef, UseDefaultIID());
		ASSERT(textModel != nil);
		if (textModel == nil) {
			break;
		}

		// Just use a fixed size 100*100 point frame.
		PMRect frameBounds(0, 0, 100, 100);

		// Position the frame on the given layout (page or spread).
		InterfacePtr<IGeometry> layoutGeometry(fLayoutGeometryUIDRef, UseDefaultIID());
		ASSERT(layoutGeometry != nil);
		if (layoutGeometry == nil) {
			break;
		}
		//::InnerToPasteboard(layoutGeometry, &frameBounds);

		// Create frame.
		InterfacePtr<ICommand> createMultiColumnItemCmd(CmdUtils::CreateCommand(kCreateMultiColumnItemCmdBoss));
		ASSERT(createMultiColumnItemCmd != nil);
		if (createMultiColumnItemCmd == nil) {
			break;
		}

		InterfacePtr<INewPageItemCmdData> newPageItemCmdData(createMultiColumnItemCmd, UseDefaultIID());
		ASSERT(newPageItemCmdData != nil);
		if (newPageItemCmdData == nil) {
			break;
		}
		PMPointList points(2);
		points.push_back( frameBounds.LeftTop() );
		points.push_back( frameBounds.RightBottom() );
		const ClassID frameClassID = kSplineItemBoss; // The kMultiColumnItemBoss is a child of a boss object of this class.
		newPageItemCmdData->Set(fLayoutHierarchyUIDRef.GetDataBase(), frameClassID, fLayoutHierarchyUIDRef.GetUID(), points);

		// Note that the story already exists so we tell the command to
		// create a frame for this story here.
		InterfacePtr<ICreateMCFrameData> createFrameData(createMultiColumnItemCmd, UseDefaultIID());
		ASSERT(createFrameData);
		if (createFrameData == nil) {
			break;
		}
		createFrameData->SetStory(contentUIDRef);

		status = CmdUtils::ProcessCommand(createMultiColumnItemCmd);
		ASSERT_MSG(status == kSuccess, "kCreateMultiColumnItemCmdBoss failed");
		if (status != kSuccess) {
			break;
		}
	
		// Return the UIDRef of the new text frame to the caller.
		frameUIDRef = createMultiColumnItemCmd->GetItemListReference().GetRef(0);

	} while(kFalse);

	return status;
}

/*
*/
ErrorCode ImportImagenClass::CreateGraphicFrame(const UIDRef& contentUIDRef, UIDRef& frameUIDRef)
{
	ErrorCode status = kFailure;
	//SNIPLOG("Create graphic frame");
	do {
		// Size the frame to fit the content.
		InterfacePtr<IGeometry> contentGeometry(contentUIDRef, UseDefaultIID());
		ASSERT(contentGeometry != nil);
		if (contentGeometry == nil) {
			break;
		}
		PMRect frameBounds = contentGeometry->GetStrokeBoundingBox();

		// Position the frame on the given layout (page or spread).
		InterfacePtr<IGeometry> layoutGeometry(fLayoutGeometryUIDRef, UseDefaultIID());
		ASSERT(layoutGeometry != nil);
		if (layoutGeometry == nil) {
			break;
		}
		//::InnerToPasteboard(layoutGeometry, &frameBounds);
			
		// Create the frame.
		frameUIDRef = Utils<IPathUtils>()->CreateRectangleSpline(fLayoutHierarchyUIDRef,
			frameBounds, 
			INewPageItemCmdData::kGraphicFrameAttributes);
		ASSERT(frameUIDRef != UIDRef(nil, kInvalidUID));
		if (frameUIDRef == UIDRef(nil, kInvalidUID)) {
			break;
		}

		// Make the content a child of the frame's hierarchy.
		status = this->AddToHierarchy(frameUIDRef, contentUIDRef);
		ASSERT(status == kSuccess);
		if (status != kSuccess) {
			break;
		}
		
		status = this->CentreContentInFrame(contentUIDRef);
		ASSERT(status == kSuccess);
		if (status != kSuccess) {
			break;
		}

		// No data link is created, instead the data is stored in an object
		// inside the document. This is *NOT* the normal approach. Normally
		// a data link would be created to reference the content stored
		// externally from the document. However for some applications,
		// layout engines for personalised publishing for example, where 
		// performance is key you may wish to embed the content in the document
		// like this.
		status = this->StoreContentStreamInternally(contentUIDRef);
		ASSERT(status == kSuccess);
		if (status != kSuccess) {
			break;
		}

		status = kSuccess;

	} while(false);
	return status;
}

/* 
*/
ErrorCode ImportImagenClass::AddToHierarchy(const UIDRef& parent, const UIDRef& child)
{
	//SNIPLOG("Add the imported content object to the frame hierarchy");
	ErrorCode status = kFailure;
    do 
	{
		InterfacePtr<ICommand> addToHierarchyCmd(CmdUtils::CreateCommand(kAddToHierarchyCmdBoss));
		ASSERT(addToHierarchyCmd != nil);
		if (addToHierarchyCmd == nil) {
			break;
		}
		InterfacePtr<IHierarchyCmdData> hierarchyCmdData(addToHierarchyCmd, IID_IHIERARCHYCMDDATA);
		ASSERT(hierarchyCmdData != nil);
		if (hierarchyCmdData == nil) {
			break;
		}		
		hierarchyCmdData->SetParent(parent);
		K2Vector<int32> indexInParent;
		indexInParent.push_back(0); // the frame has a single child, the item that is added at index 0.
		hierarchyCmdData->SetIndexInParent(indexInParent); 
		addToHierarchyCmd->SetItemList(UIDList(child));
		status = CmdUtils::ProcessCommand(addToHierarchyCmd);
		ASSERT_MSG(status == kSuccess, "kAddToHierarchyCmdBoss failed");
	} while (false);
	return status; 
}

/*
*/
ErrorCode ImportImagenClass::CentreContentInFrame(const UIDRef& contentUIDRef)
{
	//SNIPLOG("Centre the content in the frame");
	ErrorCode status = kFailure;
	do 
	{
		InterfacePtr<ICommand> centerContentInFrameCmd(CmdUtils::CreateCommand(kCenterContentInFrameCmdBoss));
		ASSERT(centerContentInFrameCmd != nil);
		if (centerContentInFrameCmd == nil) {
			break;
		}
		centerContentInFrameCmd->SetItemList(UIDList(contentUIDRef));
		status = CmdUtils::ProcessCommand(centerContentInFrameCmd);
		ASSERT_MSG(status == kSuccess, "kCenterContentInFrameCmdBoss failed");
	} while (false);
	
	return status; 
}

/* 
*/
ErrorCode ImportImagenClass::StoreContentStreamInternally(const UIDRef& contentUIDRef)
{
	//SNIPLOG("Store the stream internally within the document(no datalink)");
	ErrorCode status = kFailure;
	/*	Stores the data in a blob embedded in the document.
		IStoreInternal is a persistent interface on a page item and the methods called here
		mutate it. So this code should really be in the scope of a custom command. API commands
		exist that allow you to embed content that has a datalink. However we are creating
		content from memory based streams and there's no API command to do this. Surprisingly
		this code runs cleanly under debug without ASSERTS probably because we are working with
		new page items rather than modifying existing ones, I'm not really sure. But if you run 
		into problems just create a custom command and put this code in there.
	*/
	do {
		ASSERT(fContentStream != nil);
		if (fContentStream == nil) {
			break;
		}
		ASSERT(fContentStream->IsReading() == kTrue);
		if (fContentStream->IsReading() == kFalse) {
			break;
		}
		fContentStream->Seek(0, kSeekFromStart);
		ASSERT(fContentStream->GetStreamState() == kStreamStateGood);
		if (fContentStream->GetStreamState() != kStreamStateGood) {
			break;
		}
		InterfacePtr<IStoreInternal> storeInternal(contentUIDRef, IID_ISTOREINTERNAL);
		ASSERT(storeInternal != nil);
		if (storeInternal == nil) {
			break;
		}
		ASSERT_MSG(storeInternal->GetStoredUID().IsValid() == kFalse, "data already embedded, how did that happen?");
		if (storeInternal->GetStoredUID().IsValid() == kTrue) {
			break;
		}
		storeInternal->StoreStream(fContentStream);
		ASSERT_MSG(storeInternal->GetStoredUID().IsValid() == kTrue, "data not successfully embedded");
		if (storeInternal->GetStoredUID().IsValid() == kFalse) {
			break;
		}
		status = kSuccess;
	} while(false);
	return status;
}


/*	Buffer containing EPS image data. The letters "EPS" are approximated using 
	lines and arcs (i.e. no texts/fonts used).
*/
const uchar kEPSBuffer[] = "%!PS-Adobe-3.0 EPSF-3.0" "\r\n"
"%%BoundingBox: 0 0 120 100" "\r\n"
"0 setgray" "\r\n"
"4 setlinewidth" "\r\n"
/*E*/"30 10  moveto 10 10 lineto 10 90 lineto 30 90 lineto 10 50 moveto 20 50 lineto stroke" "\r\n"
/*P*/"40 10 moveto 40 50 lineto stroke 40 70 20 270 450 arc closepath stroke"  "\r\n"
/*S*/"85 35 25 240 450 arc stroke 85 75 15 50 270 arc stroke"  "\r\n"
"%%EOF";

/*	Buffer containing JPEG image data. The 100*100 point 72 dpi image was created using Photoshop 6 
	SaveForWeb and will render the word "JPG" in black on a white background.
*/
const uchar kJPGBuffer[] = {0xff, 0xd8, 0xff, 0xe0, 0x0, 0x10, 0x4a, 0x46, 0x49, 0x46, 0x0, 0x1, 0x2, 0x0, 0x0, 0x64, 
0x0, 0x64, 0x0, 0x0, 0xff, 0xec, 0x0, 0x11, 0x44, 0x75, 0x63, 0x6b, 0x79, 0x0, 0x1, 0x0, 
0x4, 0x0, 0x0, 0x0, 0x1b, 0x0, 0x0, 0xff, 0xee, 0x0, 0xe, 0x41, 0x64, 0x6f, 0x62, 0x65, 
0x0, 0x64, 0xc0, 0x0, 0x0, 0x0, 0x1, 0xff, 0xdb, 0x0, 0x84, 0x0, 0x11, 0xc, 0xc, 0xc, 
0xd, 0xc, 0x11, 0xd, 0xd, 0x11, 0x19, 0x10, 0xe, 0x10, 0x19, 0x1d, 0x16, 0x11, 0x11, 0x16, 
0x1d, 0x22, 0x17, 0x17, 0x17, 0x17, 0x17, 0x22, 0x21, 0x1a, 0x1d, 0x1c, 0x1c, 0x1d, 0x1a, 0x21, 
0x21, 0x26, 0x28, 0x2b, 0x28, 0x26, 0x21, 0x34, 0x34, 0x38, 0x38, 0x34, 0x34, 0x41, 0x41, 0x41, 
0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x1, 0x12, 0x10, 0x10, 
0x13, 0x15, 0x13, 0x17, 0x14, 0x14, 0x17, 0x16, 0x12, 0x15, 0x12, 0x16, 0x1c, 0x16, 0x18, 0x18, 
0x16, 0x1c, 0x29, 0x1c, 0x1c, 0x1e, 0x1c, 0x1c, 0x29, 0x35, 0x26, 0x21, 0x21, 0x21, 0x21, 0x26, 
0x35, 0x2f, 0x32, 0x2b, 0x2b, 0x2b, 0x32, 0x2f, 0x39, 0x39, 0x35, 0x35, 0x39, 0x39, 0x41, 0x41, 
0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0x41, 0xff, 0xc0, 0x0, 
0x11, 0x8, 0x0, 0x64, 0x0, 0x64, 0x3, 0x1, 0x22, 0x0, 0x2, 0x11, 0x1, 0x3, 0x11, 0x1, 
0xff, 0xc4, 0x0, 0x75, 0x0, 0x1, 0x0, 0x3, 0x1, 0x1, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x3, 0x5, 0x6, 0x4, 0x2, 0x1, 0x1, 0x1, 0x0, 0x0, 
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x10, 0x0, 
0x1, 0x4, 0x1, 0x3, 0x1, 0x3, 0x6, 0xc, 0x7, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 
0x1, 0x2, 0x3, 0x4, 0x11, 0x12, 0x5, 0x6, 0x21, 0x31, 0x71, 0x13, 0x41, 0x51, 0xa1, 0x22, 
0xb3, 0x35, 0x61, 0x32, 0x52, 0x62, 0x33, 0x73, 0x14, 0x34, 0x74, 0x15, 0x36, 0x7, 0x91, 0x42, 
0x92, 0xb2, 0xd2, 0x93, 0x54, 0x11, 0x1, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xff, 0xda, 0x0, 0xc, 0x3, 0x1, 0x0, 0x2, 0x11, 
0x3, 0x11, 0x0, 0x3f, 0x0, 0xde, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x45, 0x25, 0xaa, 0xd1, 0x48, 0xc8, 0xa5, 0x99, 0x91, 
0xc9, 0x27, 0x48, 0xd8, 0xe7, 0x23, 0x5c, 0xe5, 0x55, 0xc7, 0xaa, 0x8a, 0xb9, 0x5e, 0xa0, 0x4a, 
0x8, 0xe7, 0xb1, 0x5e, 0xbb, 0x11, 0xf6, 0x25, 0x64, 0x2c, 0x55, 0xc2, 0x3a, 0x47, 0x23, 0x13, 
0x3d, 0xb8, 0xcb, 0x88, 0x3f, 0x36, 0xda, 0xff, 0x0, 0xed, 0x83, 0xfd, 0xac, 0xff, 0x0, 0x20, 
0x3a, 0xc1, 0x1c, 0x33, 0xc1, 0x3b, 0x75, 0xc1, 0x23, 0x25, 0x6f, 0xca, 0x63, 0x91, 0xc9, 0xfc, 
0x50, 0x90, 0x0, 0x39, 0xed, 0x6e, 0x14, 0x29, 0x26, 0x6d, 0xd9, 0x8a, 0xbe, 0x7b, 0x3c, 0x47, 
0xb5, 0x8a, 0xbd, 0xc8, 0xaa, 0x78, 0xab, 0xbb, 0x6d, 0x97, 0x5d, 0xa2, 0xa5, 0xb8, 0x67, 0x7f, 
0xc8, 0x63, 0xda, 0xe7, 0x7f, 0x4e, 0x72, 0x7, 0x58, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x8b, 
0xe5, 0xbf, 0xaa, 0x76, 0xf, 0xad, 0x8f, 0xdb, 0x30, 0xda, 0x18, 0xbe, 0x5b, 0xfa, 0xa7, 0x60, 
0xfa, 0xd8, 0xfd, 0xb3, 0x0, 0x9f, 0xf7, 0x1f, 0xdc, 0x70, 0x7e, 0x29, 0x9e, 0xce, 0x52, 0x6a, 
0xbc, 0x23, 0x8e, 0xc9, 0x5a, 0x19, 0x1f, 0x5d, 0xea, 0xe7, 0xb1, 0xae, 0x72, 0xf8, 0x8f, 0xed, 
0x54, 0x45, 0x5f, 0x29, 0xf, 0xee, 0x3f, 0xb8, 0xe0, 0xfc, 0x53, 0x3d, 0x9c, 0xa4, 0x75, 0xb7, 
0xee, 0x58, 0xda, 0xf1, 0x35, 0x9b, 0x26, 0xb6, 0x35, 0x8d, 0x46, 0xbb, 0x52, 0xf5, 0x44, 0x4e, 
0x8a, 0x5, 0x7f, 0x20, 0xda, 0x99, 0xc4, 0xec, 0xd3, 0xdd, 0xb6, 0x89, 0x1f, 0x1b, 0x1d, 0x26, 
0x89, 0x61, 0x73, 0x95, 0x51, 0xdf, 0xcd, 0xa7, 0x3e, 0x56, 0xaa, 0x22, 0xe7, 0x26, 0x93, 0x94, 
0xef, 0xcb, 0xb4, 0x6d, 0x1f, 0x68, 0x83, 0xef, 0x36, 0x15, 0x19, 0x5f, 0x3d, 0x74, 0xab, 0x93, 
0x2a, 0xf5, 0x4f, 0x9a, 0x9e, 0x9c, 0x14, 0xaf, 0xda, 0x79, 0x1f, 0x25, 0xbd, 0x5e, 0x4d, 0xea, 
0x16, 0x52, 0xdb, 0xab, 0xbb, 0x57, 0x82, 0xd5, 0x4d, 0x4f, 0xf3, 0xa6, 0x35, 0x39, 0x72, 0xb8, 
0xc6, 0x57, 0x18, 0xf2, 0x21, 0xeb, 0x9d, 0xfb, 0xcf, 0x63, 0x6c, 0x9f, 0x41, 0xe2, 0xbb, 0x3e, 
0x6f, 0x8f, 0x16, 0xaf, 0x40, 0x13, 0xed, 0x1c, 0x2a, 0xac, 0x91, 0x36, 0xee, 0xfb, 0xae, 0xe5, 
0xf9, 0xf0, 0xf9, 0x1a, 0xf7, 0xb9, 0x1a, 0xc5, 0x5e, 0xb8, 0x5d, 0x2a, 0x8a, 0xe5, 0xf3, 0xf5, 
0xc1, 0xef, 0x77, 0xe0, 0xdb, 0x64, 0xb5, 0xdf, 0x2e, 0xd4, 0xc7, 0x54, 0xbb, 0x1a, 0x6a, 0x8b, 
0x43, 0xdc, 0xad, 0x73, 0x9b, 0xd5, 0x11, 0x75, 0x2a, 0xe3, 0x3e, 0x74, 0x35, 0x66, 0x5a, 0xc7, 
0x2b, 0xde, 0x61, 0xb1, 0x2c, 0x4c, 0xe3, 0xd6, 0x65, 0x64, 0x6f, 0x73, 0x5b, 0x22, 0x2c, 0x98, 
0x7a, 0x35, 0x70, 0x8e, 0x4c, 0x40, 0xbd, 0xa0, 0x58, 0x71, 0x6b, 0x9b, 0x95, 0xbd, 0xa6, 0x37, 
0x6e, 0x71, 0x49, 0x15, 0x98, 0xd5, 0x58, 0xae, 0x95, 0xaa, 0xc7, 0x48, 0xd4, 0x44, 0x56, 0xbf, 
0xb, 0xde, 0x5c, 0x94, 0x7c, 0x6b, 0x91, 0xae, 0xfc, 0xcb, 0x2e, 0x5a, 0xdf, 0x65, 0x5a, 0xce, 
0x6b, 0x55, 0xaa, 0xff, 0x0, 0x13, 0x3a, 0xb3, 0xf3, 0x19, 0x8c, 0x60, 0xbc, 0x0, 0x0, 0x0, 
0x0, 0x3, 0x19, 0xf9, 0xfd, 0x9d, 0xa7, 0x96, 0xda, 0xad, 0xbb, 0xd8, 0x72, 0x6d, 0xf2, 0xb5, 
0x56, 0xbe, 0xaf, 0xa3, 0x66, 0xbd, 0x2e, 0x62, 0xf4, 0x4e, 0xce, 0x8a, 0xd3, 0x9e, 0xfd, 0xca, 
0xfc, 0x83, 0x97, 0xed, 0x6c, 0xdb, 0x55, 0x66, 0x8a, 0x92, 0xb6, 0x49, 0x66, 0x44, 0x54, 0x6f, 
0xa8, 0xff, 0x0, 0x11, 0xdd, 0xbe, 0x4e, 0x88, 0x99, 0xf3, 0xa9, 0xb2, 0xbb, 0xb7, 0x50, 0xbe, 
0xd4, 0x65, 0xda, 0xf1, 0xd8, 0x46, 0xfc, 0x5d, 0x6d, 0x45, 0x56, 0xf7, 0x2f, 0x6a, 0xa, 0x5b, 
0x6d, 0xa, 0xd, 0x56, 0xd2, 0xaf, 0x1d, 0x74, 0x77, 0xc6, 0xd0, 0xd4, 0x45, 0x77, 0x7a, 0xf6, 
0xa8, 0x19, 0xbf, 0xdc, 0x7f, 0x71, 0xc1, 0xf8, 0xa6, 0x7b, 0x39, 0x4d, 0x35, 0x1f, 0xb9, 0x57, 
0xfa, 0xa6, 0x7f, 0x6a, 0x1f, 0x6c, 0xd4, 0xab, 0x6d, 0x89, 0x1d, 0xb8, 0x23, 0xb1, 0x1a, 0x2e, 
0xa4, 0x64, 0xac, 0x6c, 0x8d, 0x47, 0x22, 0x63, 0x38, 0x72, 0x2f, 0x5e, 0xa4, 0xad, 0x6b, 0x5a, 
0xd4, 0x6b, 0x51, 0x1a, 0xd6, 0xa6, 0x11, 0x13, 0xa2, 0x22, 0x20, 0x1f, 0x4a, 0x2e, 0x5b, 0xb1, 
0xbf, 0x79, 0xdb, 0x34, 0xc1, 0xf7, 0xba, 0xee, 0xf1, 0x20, 0x4e, 0xcd, 0x4b, 0x8c, 0x39, 0x99, 
0xf8, 0x53, 0xd3, 0x82, 0xf4, 0x1, 0x94, 0xda, 0x39, 0xad, 0x3f, 0xd, 0xb4, 0xf7, 0xad, 0x54, 
0xaf, 0xc2, 0x88, 0xc9, 0x56, 0x46, 0xae, 0x97, 0xb9, 0x3c, 0xbd, 0x13, 0xd5, 0x55, 0xf2, 0xe7, 
0xa7, 0xc2, 0x75, 0x5f, 0xe7, 0x1b, 0x5, 0x48, 0xd5, 0x62, 0x9d, 0x6d, 0x4b, 0x8f, 0x56, 0x38, 
0x91, 0x57, 0x3d, 0xee, 0x54, 0x46, 0xa1, 0x6f, 0x73, 0x6c, 0xdb, 0xaf, 0x63, 0xed, 0x95, 0xa2, 
0x9d, 0x53, 0xa2, 0x2b, 0xd8, 0x8e, 0x72, 0x77, 0x2f, 0x6a, 0x10, 0xd5, 0xd8, 0xb6, 0x6a, 0x8f, 
0xf1, 0x2b, 0xd2, 0x86, 0x39, 0x13, 0xb1, 0xe8, 0xc4, 0x57, 0x27, 0x72, 0xae, 0x55, 0x0, 0xa1, 
0xe0, 0x14, 0xae, 0x57, 0xad, 0x72, 0xcd, 0x98, 0x5d, 0xb, 0x2d, 0x48, 0xd7, 0x44, 0x8f, 0xe8, 
0xaa, 0x89, 0xab, 0x2b, 0x85, 0xeb, 0x8f, 0x58, 0xd7, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 
0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x7, 0xff, 0xd9};

/*	Buffer containing GIF image data. The 100*100 point 72 dpi image was created using Photoshop 6 
	SaveForWeb and will render the word "JPG" in black on a white background.
*/
const uchar kGIFBuffer[] = {0x47, 0x49, 0x46, 0x38, 0x39, 0x61, 0x64, 0x0, 0x64, 0x0, 0xb3, 0x0, 0x0, 0x44, 0x44, 0x44, 
0xbb, 0xbb, 0xbb, 0x88, 0x88, 0x88, 0xee, 0xee, 0xee, 0x11, 0x11, 0x11, 0xdd, 0xdd, 0xdd, 0x66, 
0x66, 0x66, 0xcc, 0xcc, 0xcc, 0x77, 0x77, 0x77, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x22, 0x22, 
0x22, 0x99, 0x99, 0x99, 0xaa, 0xaa, 0xaa, 0x0, 0x0, 0x0, 0xff, 0xff, 0xff, 0x21, 0xf9, 0x4, 
0x0, 0x0, 0x0, 0x0, 0x0, 0x2c, 0x0, 0x0, 0x0, 0x0, 0x64, 0x0, 0x64, 0x0, 0x0, 0x4, 
0xff, 0xf0, 0xc9, 0x49, 0xab, 0xbd, 0x38, 0xeb, 0xcd, 0xbb, 0xff, 0x60, 0x28, 0x8e, 0x64, 0x69, 
0x9e, 0x68, 0xaa, 0xae, 0x6c, 0xeb, 0xbe, 0x70, 0x2c, 0xcf, 0x74, 0x6d, 0xdf, 0x78, 0xae, 0xef, 
0x7c, 0xef, 0xff, 0xc0, 0xa0, 0x70, 0x48, 0x2c, 0x1a, 0x8f, 0xc8, 0xa4, 0x72, 0xc9, 0x6c, 0x3a, 
0x9f, 0xd0, 0xa8, 0x74, 0x4a, 0xad, 0x5a, 0xaf, 0xd8, 0xac, 0x76, 0xcb, 0xed, 0xa6, 0x6, 0x81, 
0xf0, 0xc1, 0xdb, 0x19, 0x30, 0x14, 0x4, 0x87, 0x7a, 0xd, 0x10, 0xc, 0x34, 0x8, 0x0, 0x0, 
0x61, 0x91, 0xdb, 0xef, 0x78, 0xfb, 0xf8, 0x27, 0x48, 0xaf, 0xff, 0x6b, 0x4, 0x2, 0x19, 0x0, 
0x6a, 0x0, 0x16, 0x80, 0x89, 0x89, 0x1, 0x3e, 0x3, 0x85, 0x7f, 0x4, 0x76, 0x7e, 0x6b, 0x6, 
0x18, 0x8f, 0x87, 0x15, 0x8a, 0x9a, 0x6a, 0x8c, 0x3c, 0x3, 0x9, 0x81, 0x2, 0x5, 0x15, 0x7, 
0x6, 0x7f, 0x74, 0x75, 0x86, 0x88, 0x6a, 0x6, 0x61, 0xae, 0xaf, 0xaf, 0x6f, 0x3c, 0xa, 0x6c, 
0xb2, 0x17, 0x7, 0x93, 0x9d, 0x14, 0x97, 0xab, 0xe, 0x83, 0x49, 0x1, 0x6c, 0x1c, 0x7, 0x94, 
0xa9, 0xe, 0x98, 0x14, 0x6b, 0xbf, 0x48, 0xa0, 0xe, 0x4, 0xb6, 0x1a, 0xa6, 0x6a, 0xd0, 0xf, 
0xbc, 0x99, 0x6a, 0xcb, 0x46, 0xc4, 0x6a, 0xc, 0x1e, 0x5, 0x86, 0xa3, 0xbb, 0xaa, 0xd7, 0xbe, 
0x49, 0x8, 0x6b, 0xd4, 0x1a, 0xba, 0x15, 0xd6, 0xc9, 0xd8, 0x49, 0xcd, 0xa, 0x29, 0xed, 0x13, 
0xca, 0x49, 0xf6, 0x28, 0xf4, 0x12, 0xf8, 0xda, 0x6b, 0xd, 0xf3, 0xe3, 0xdc, 0x39, 0x58, 0x90, 
0x7, 0xcf, 0x1e, 0x1d, 0xc1, 0x38, 0x1, 0x3c, 0xd6, 0x6b, 0xd3, 0x9a, 0x75, 0x38, 0x12, 0x3a, 
0x80, 0x38, 0x41, 0xa2, 0x22, 0x64, 0x12, 0xf4, 0x3d, 0x70, 0x8, 0x88, 0xa2, 0xd, 0x89, 0x1e, 
0xbb, 0x1f, 0x58, 0x4c, 0x84, 0xb1, 0x5a, 0xc0, 0x7a, 0xac, 0x60, 0xa9, 0x4c, 0x67, 0x63, 0x0, 
0xbf, 0xa, 0x23, 0x1, 0x95, 0xd4, 0xf8, 0xb2, 0x88, 0x9f, 0x4a, 0x1f, 0xf4, 0xd1, 0x7c, 0x87, 
0x84, 0xd6, 0x40, 0x10, 0x3a, 0x4f, 0xee, 0xe3, 0x79, 0x84, 0xc1, 0xc3, 0x9c, 0x42, 0x77, 0x96, 
0x43, 0x32, 0xc0, 0x4f, 0x49, 0xd, 0x41, 0x19, 0x92, 0xcb, 0x66, 0x44, 0x40, 0x4d, 0x42, 0x49, 
0x85, 0x6e, 0x24, 0xca, 0x6c, 0xd, 0xaa, 0xc, 0x3, 0xa4, 0x49, 0x9d, 0xa0, 0x94, 0xaa, 0x91, 
0x2, 0x93, 0x16, 0x74, 0xb3, 0x50, 0x0, 0xc1, 0x24, 0x7, 0x5f, 0x33, 0x6a, 0xbd, 0xaa, 0xad, 
0x19, 0x1b, 0x1, 0x78, 0x15, 0x2c, 0x0, 0x24, 0xc8, 0xd8, 0x53, 0xba, 0x46, 0x6, 0x58, 0xe5, 
0xc8, 0x2a, 0x1c, 0xbb, 0xb9, 0x5c, 0x97, 0x14, 0x10, 0xb0, 0x57, 0x51, 0x2, 0x51, 0x58, 0xc7, 
0xa2, 0x5c, 0xa, 0xa5, 0x40, 0x0, 0xbc, 0x78, 0x3, 0x18, 0x26, 0xc3, 0xb9, 0xb3, 0xe7, 0xcf, 
0xa0, 0x43, 0x8b, 0x1e, 0x4d, 0xba, 0xb4, 0xe9, 0xd3, 0xa8, 0x53, 0xab, 0x5e, 0xcd, 0xba, 0xb5, 
0xeb, 0xd7, 0xb0, 0x63, 0xcb, 0x9e, 0x4d, 0xbb, 0xb6, 0xed, 0xdb, 0xb8, 0x73, 0xeb, 0xde, 0xcd, 
0xbb, 0xb7, 0xef, 0xdf, 0xc0, 0x83, 0xb, 0x1f, 0x4e, 0xfc, 0x73, 0x4, 0x0, 0x3b};

/* Buffer containing Adobe InDesign Tagged Text.
*/
const char kTaggedTextBuffer[] = "<ASCII-WIN>" "\r\n"
"<Version:2.000000><FeatureSet:InDesign-Roman><ColorTable:=<Black:COLOR:CMYK:Process:0.000000,0.000000,0.000000,1.000000>>" "\r\n"
"<ParaStyle:><cSize:24.000000>Tagged Text<cSize:>";

/*
*/
ErrorCode ImportImagenClass::OpenContentStream()	
{
	ErrorCode status = kFailure;
	//SNIPLOG("Open a stream to import %s data from", fFormat.GrabCString());
	do {
		if (fFormat == "EPS") {
			InterfacePtr<IPMStream> epsStream(StreamUtil::CreatePointerStreamRead((char*)kEPSBuffer, sizeof(kEPSBuffer)));
			fContentStream = epsStream;
			status = kSuccess;
		}
		else if (fFormat == "JPG") {
			InterfacePtr<IPMStream> jpgStream(StreamUtil::CreatePointerStreamRead((char*)kJPGBuffer, sizeof(kJPGBuffer)));
			fContentStream = jpgStream;
			status = kSuccess;
		}
		else if (fFormat == "GIF") {
			InterfacePtr<IPMStream> gifStream(StreamUtil::CreatePointerStreamRead((char*)kGIFBuffer, sizeof(kGIFBuffer)));
			fContentStream = gifStream;
			status = kSuccess;
		}
		else if (fFormat == "TXT") {
			InterfacePtr<IPMStream> taggedTextStream(StreamUtil::CreatePointerStreamRead((char*)kTaggedTextBuffer, sizeof(kTaggedTextBuffer)));
			fContentStream = taggedTextStream;
			status = kSuccess;
		}
		else if (fFormat == "Pointer") { 
			// Let the user choose a file and read the data from this file
			// into a dynamic buffer in memory then present this data inside
			// a PointerStream (see StreamUtil).

			// Browse for the file.
			IDFile file;
		//	if (::SnipOpenFileDialog(file) != kSuccess) {
			//	break;
			//}

			// Count characters in the file.
			InterfacePtr<IPMStream> fileStream(StreamUtil::CreateFileStreamRead(file));
			ASSERT(fileStream != nil);
			if (fileStream == nil) {
				break;
			}
			fileStream->Seek(0, kSeekFromStart);
			int32 count = 0;
			while (fileStream->GetStreamState() == kStreamStateGood) {
				uchar tmp;
				fileStream->XferByte(tmp);
				if (fileStream->GetStreamState() == kStreamStateEOF) {
					break;
				}
				count++;
			}
			if (count > 500000) {
				//SNIPLOG("Big file, if you really want to try this bump the limit");
				break;
			}

			// Create a buffer and copy the data from the file into memory.
			// Note we have to manage the lifetime of the buffer and delete it later.
			fPointerStreamBuffer = new uchar[count];
			fileStream->Seek(0, kSeekFromStart);
			int32 index = 0;
			while (fileStream->GetStreamState() == kStreamStateGood) {
				uchar tmp;
				fileStream->XferByte(tmp);
				if (fileStream->GetStreamState() == kStreamStateEOF) {
					break;
				}
				fPointerStreamBuffer [index++] = tmp;
			}

			// Put the data in a PointerStream.
			InterfacePtr<IPMStream> pointerStream(StreamUtil::CreatePointerStreamRead((char*)fPointerStreamBuffer, count));
			fContentStream = pointerStream;
			status = kSuccess;
		}
		else {
			// Let the user chose a file and stream data direct from the file
			// via a FileStream.
			IDFile file;
		/*	if (::SnipOpenFileDialog(file) != kSuccess) {
				break;
			}
			*/
			InterfacePtr<IPMStream> fileStream(StreamUtil::CreateFileStreamRead(file));
			fContentStream = fileStream;
			status = kSuccess;
		}
	} while(false);

	if (status == kSuccess) {
		//SNIPLOG("Importing format %s via stream boss ClassID: %s", fFormat.GrabCString(), this->GetBossClassName(fContentStream).GrabCString());
	}

	return status;
}

/*
*/
void ImportImagenClass::CloseContentStream()	
{
	if (fContentStream != nil) {
		fContentStream->Close();
	}
	if (fPointerStreamBuffer != nil) {
		delete [] fPointerStreamBuffer;
		fPointerStreamBuffer = nil;
	}
}


//End, ImportImagenClass.cpp
