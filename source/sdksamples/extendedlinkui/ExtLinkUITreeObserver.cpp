//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUITreeObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Implementation includes
#include "widgetid.h"

// Interface includes
#include "IActiveContext.h"
#include "IControlView.h"
#include "IBoolData.h"
#include "IDocument.h"
#include "IExtLinkUIDataModel.h"
#include "ILayoutUIUtils.h"
#include "ILinkManager.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ITreeViewController.h"
#include "ITreeViewMgr.h"
#include "IWorkspace.h"
#include "IWidgetParent.h"
#include "IExtLinkUITreeNodeList.h"

// Implementation includes
#include "CAlert.h"
#include "CObserver.h"
#include "ExtLinkUIID.h"
#include "LinksUIID.h"
#include "ListLazyNotificationData.h"
#include "Utils.h"

#include "K2Vector.tpp"




/** Updates a tree view user interface to display the ExtendedLink links
	in the current document.

	@ingroup extendedlinkui
*/
class ExtLinkUITreeObserver : public CObserver
{
public:

	/** Constructor.
		@param boss object on which this interface is aggregated.
	*/
	ExtLinkUITreeObserver(IPMUnknown *boss);

	/** Destructor.
	*/	
	~ExtLinkUITreeObserver();

	/** Attaches to the active context if a document is active.
	*/	
	virtual void AutoAttach();

	/** Detaches from the active context if it is attached.
	*/	
	virtual void AutoDetach();

	/** Reacts to change in the active context.

		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);
	
    /** Updates this plug-in's panel when the plug-in's model changes.  Widgets that react to changes 
	    in the plug-in's model are updated by lazy notifications handled by this method.
        @param theSubject [IN] provides a reference to the object which has changed.
        @param protocol [IN] the protocol along which the change occurred.
        @param data [IN] 
    */
	virtual void LazyUpdate(ISubject* theSubject, const PMIID &protocol, const LazyNotificationData* data);

private:


	/**	Called when the active context changes.
		@param theSubject 
		@param changedBy 
	 */
	void HandleTreeUpdate(ISubject* theSubject, void* changedBy);

	/**	Handles detaching / attaching as the active document changes. 
		@param newFrontDoc 
	 */
	void AttachToNewFrontDoc(const IDocument* newFrontDoc);

	/**	Handles detaching from the document. 
	 */
	void DetachFromDoc();

	/**	Handles detaching / attaching as the active document changes. 
	 */
	void ActiveDocChange();

	/**	Displays the ODBC links active in the document.
	 */
	void RebuildTree();
	
	/**	Handles updating the tree view when the links model has changed. 
		@param db 
		@param data 
	 */
	void HandleLinkModelChange(IDataBase* db, const LazyNotificationData* data);

	/**	Updates the drawing status of the tree view. 
	 */
	void EnableDrawingIfDisabled();


	bool fEnableRedrawAtNextLinkNotification;
};



/*
*/
CREATE_PMINTERFACE(ExtLinkUITreeObserver, kExtLinkUITreeObserverImpl)


/*
*/
ExtLinkUITreeObserver::ExtLinkUITreeObserver(IPMUnknown* boss)
: CObserver(boss), fEnableRedrawAtNextLinkNotification(false)
{

}

/*
*/
ExtLinkUITreeObserver::~ExtLinkUITreeObserver()
{

}

/*
*/
void ExtLinkUITreeObserver::AutoAttach()
{
	// Observe the active context.
	IActiveContext* context = GetExecutionContextSession()->GetActiveContext();
	if (context)
	{
		InterfacePtr<ISubject> contextSubject(context, UseDefaultIID());
		if (contextSubject) {
			contextSubject->AttachObserver(ISubject::kRegularAttachment,this, IID_IACTIVECONTEXT, IID_IOBSERVER);
		}
	}

	// Observe the current document in the active context (if any).
	this->ActiveDocChange();	
	this->RebuildTree();
}


/*
*/
void ExtLinkUITreeObserver::AutoDetach()
{
	CObserver::AutoDetach();

	IActiveContext* context = GetExecutionContextSession()->GetActiveContext();
	if (context)
	{
		InterfacePtr<ISubject> contextSubject(context, UseDefaultIID());
		if (contextSubject) {
			if (contextSubject->IsAttached(ISubject::kRegularAttachment, this, IID_IACTIVECONTEXT, IID_IOBSERVER)) {
				contextSubject->DetachObserver(ISubject::kRegularAttachment, this, IID_IACTIVECONTEXT, IID_IOBSERVER);
			}
		}
	}

	DetachFromDoc();
}

/*
*/
void ExtLinkUITreeObserver::Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy)
{
	if (protocol == IID_IACTIVECONTEXT)
	{	
		const PMIID& what = *((const PMIID*)changedBy);
		if (what == IID_IDOCUMENT) {
			this->ActiveDocChange();
			this->RebuildTree();
		}
	}
	else if (protocol == IID_ILINKMANAGER)
	{
		if(theChange == kLinkModelChangeEnd)
		{
			// once we get a model change end, we know we're out of sync with the model until we get an abort or a lazy notification.
			TRACEFLOW("Links:LinksUI","LinksUIPanelTreeObserver::Update got kLinkModelChangeEnd. Will enable redraw at next notification.\n");
			fEnableRedrawAtNextLinkNotification = true;
			InterfacePtr<const IWidgetParent> wp(this, UseDefaultIID());
			InterfacePtr<IBoolData> disableDrawingData((IBoolData*)wp->QueryParentFor(IID_EXTLINKPANELDRAWINGDISABLED));
			disableDrawingData->Set(kTrue);

		}
		else if(theChange == kLinkModelChangeAbort)
		{
			TRACEFLOW("Links:LinksUI","LinksUIPanelTreeObserver::Update got kLinkModelChangeAbort. Will enable redraw now.\n");
			fEnableRedrawAtNextLinkNotification = true;
			EnableDrawingIfDisabled();
		}
	} 
}


/*
*/
void ExtLinkUITreeObserver::AttachToNewFrontDoc(const IDocument* newFrontDoc)
{
	if( newFrontDoc )
	{
		InterfacePtr<IExtLinkUIDataModel> dataModel(this, UseDefaultIID());
		ASSERT(dataModel);
		if(dataModel) {
			dataModel->SetCurrentDocDB(::GetDataBase(newFrontDoc));
		}

		InterfacePtr<ISubject> docSubject(newFrontDoc, UseDefaultIID());
		if (docSubject) 
		{
			if( !docSubject->IsAttached(ISubject::kLazyAttachment, this, IID_ILINKDATA_CHANGED, IID_IOBSERVER)) {
				docSubject->AttachObserver(ISubject::kLazyAttachment, this, IID_ILINKDATA_CHANGED, IID_IOBSERVER);
			}
			if( !docSubject->IsAttached(ISubject::kRegularAttachment,this, IID_ILINKMANAGER, IID_IOBSERVER)) {
				docSubject->AttachObserver(ISubject::kRegularAttachment,this, IID_ILINKMANAGER, IID_IOBSERVER);
			}
		}
	}
}



/*
*/
void ExtLinkUITreeObserver::DetachFromDoc()
{
	InterfacePtr<IExtLinkUIDataModel> dataModel(this, UseDefaultIID());
	if (dataModel) 
	{
		IDataBase* currentExtLinkUIDocDB = dataModel->GetCurrentDocDB();
		if (currentExtLinkUIDocDB)
		{
			InterfacePtr<ISubject> docSubject( currentExtLinkUIDocDB, currentExtLinkUIDocDB->GetRootUID(), UseDefaultIID() );
			if( docSubject )
			{
				if( docSubject->IsAttached(ISubject::kLazyAttachment, this, IID_ILINKDATA_CHANGED, IID_IOBSERVER)) {
					docSubject->DetachObserver(ISubject::kLazyAttachment, this, IID_ILINKDATA_CHANGED, IID_IOBSERVER);
				}
					
				TRACEFLOW("Links:LinksUI","Detaching from IID_ILINKMANAGER on document\n");	
				if( docSubject->IsAttached(ISubject::kRegularAttachment, this, IID_ILINKMANAGER, IID_IOBSERVER))
					docSubject->DetachObserver(ISubject::kRegularAttachment, this, IID_ILINKMANAGER, IID_IOBSERVER);
			}
		}
		
		dataModel->SetCurrentDocDB(nil);
	}
}


/*
*/
void ExtLinkUITreeObserver::ActiveDocChange()
{
	const IDocument* newFrontDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();

	InterfacePtr<IExtLinkUIDataModel> dataModel(this, UseDefaultIID());
	if (dataModel) 
	{
		IDataBase* currentExtLinkUIDocDB = dataModel->GetCurrentDocDB();
		if (::GetDataBase(newFrontDoc) != currentExtLinkUIDocDB)
		{		
			this->DetachFromDoc();			
			this->AttachToNewFrontDoc(newFrontDoc);
		}
	}
    InterfacePtr<ITreeViewController> treeController(this, UseDefaultIID());
    treeController->DeselectAll();

	this->EnableDrawingIfDisabled();
}


/*
*/ 
void ExtLinkUITreeObserver::RebuildTree()
{
	InterfacePtr<IExtLinkUIDataModel> dataModel(this, UseDefaultIID());
	ASSERT(dataModel);
	if (dataModel) 
	{
		// rebuild the tree data
		dataModel->Rebuild();
		
		InterfacePtr<ITreeViewMgr> iTreeViewMgr(this, UseDefaultIID());
		ASSERT(iTreeViewMgr);
		if (iTreeViewMgr)
		{
			// redraw the tree
			iTreeViewMgr->ChangeRoot(kTrue);
		}
	}
}


/* ExtLinkUITreeObserver::LazyUpdate 
*/
void ExtLinkUITreeObserver::LazyUpdate(ISubject* theSubject, const PMIID &protocol, const LazyNotificationData* data)
{
	#pragma unused(data, theSubject)

	if (protocol == IID_ILINKDATA_CHANGED) {    
		HandleLinkModelChange(::GetDataBase(theSubject), data);
	}
}


void ExtLinkUITreeObserver::HandleLinkModelChange(IDataBase* db, const LazyNotificationData* data)
{
	K2Vector<ILinkManager::ChangeData> addedItems, updatedItems, deletedItems;
	const ListLazyNotificationData<ILinkManager::ChangeData> *listData = static_cast<const ListLazyNotificationData<ILinkManager::ChangeData> *>(data);
	if (listData) {
		listData->BreakoutChanges(&addedItems, &deletedItems, &updatedItems);
	}
	//TRACEFLOW("ExtLinkUI","Notification contains %d added items, %d deleted items, and %d updated items\n",addedItems.Length(),deletedItems.Length(),updatedItems.Length());

	if(!listData || !addedItems.empty() || !deletedItems.empty())
	{
		//TRACEFLOW("ExtLinkUI","ExtLinkUITreeObserver::HandleLinkModelChange forcing tree to rebuild because of adds/deletes/unknown changes\n");
		RebuildTree();
	}
	else
	{
		// find the updated items in the tree and invalidate them.
		if(listData && !updatedItems.empty())
		{
			InterfacePtr<ITreeViewMgr> treeViewMgr(this, UseDefaultIID());

			int32 numUpdated = updatedItems.Length();
			for(int i = 0; i < numUpdated; ++i)
			{
				if(updatedItems[i].GetType() == ILinkManager::ChangeData::kLink)
				{
					UIDRef linkUIDRef(db, updatedItems[i].GetUID());
					//TRACEFLOW("ExtLinkUI","ExtLinkUITreeObserver::HandleLinkModelChange updating single node for Link change. Link UID: %d\n",updatedItems[i].GetUID().Get());
					InterfacePtr<const ILink> theLink(linkUIDRef, UseDefaultIID());
					InterfacePtr<IExtLinkUITreeNodeList> nodeList(this, UseDefaultIID());
					NodeID changedNode = nodeList->GetTreeNodeForLink(linkUIDRef.GetUID());
					if(changedNode.IsValid())
					{
						treeViewMgr->NodeChanged(changedNode, kTrue);
					}
				}

				// this notification has the UID of the link resource. We need to get the UID of the associated link(s).
				else if(updatedItems[i].GetType() == ILinkManager::ChangeData::kResource)
				{
					InterfacePtr<const ILinkManager> linkMgr(db, db->GetRootUID(), UseDefaultIID());
					
					// ok, we need to find all the nodes in the tree that refer to this resource, and mark them changed.
					UIDRef changedResourceRef(db, updatedItems[i].GetUID());
					
					ILinkManager::QueryResult resultUIDs;
					linkMgr->QueryLinksByResourceUID(updatedItems[i].GetUID(), true, resultUIDs);
					for(int j = static_cast<int>(resultUIDs.size()) - 1; j >= 0; --j)
					{
						//TRACEFLOW("ExtLinkUI","ExtLinkUITreeObserver::HandleLinkModelChange updating single node for IID_ILINKRESOURCE change. Link UID: %d, Link Resource UID: %d\n",resultUIDs[j].Get(),updatedItems[i].GetUID().Get());
						UIDRef linkUIDRef(db,resultUIDs[j]);
						InterfacePtr<const ILink> theLink(linkUIDRef, UseDefaultIID());
						InterfacePtr<IExtLinkUITreeNodeList> nodeList(this, UseDefaultIID());
						NodeID changedNode = nodeList->GetTreeNodeForLink(linkUIDRef.GetUID());
						if(changedNode.IsValid())
						{
							treeViewMgr->NodeChanged(changedNode,kTrue);
						}
					}
				}
				else
				{
					ASSERT_MSG(updatedItems[i].GetType() == ILinkManager::ChangeData::kLinksResource,"Unknown type of change");
					//TRACEFLOW("ExtLinkUI","ExtLinkUITreeObserver::HandleLinkModelChange rebuilding tree because Link UID: %d, changed to use a different Link Resource UID.\n",updatedItems[i].GetUID().Get());
					RebuildTree();
					break;
				}
			}
		}

	}
	EnableDrawingIfDisabled();
}



void ExtLinkUITreeObserver::EnableDrawingIfDisabled()
{
	if(fEnableRedrawAtNextLinkNotification)
	{
		fEnableRedrawAtNextLinkNotification = false;
		InterfacePtr<const IWidgetParent> wp(this,UseDefaultIID());
		InterfacePtr<IBoolData> disableDrawingData((IBoolData*)wp->QueryParentFor(IID_EXTLINKPANELDRAWINGDISABLED));
		disableDrawingData->Set(kFalse);
		
		// IID_EXTLINKPANELSHOULDINVALDATA is set to true inside the View's Draw routine if the links mgr is busy during a draw.
		// Now that we know the model is done changing, invalidate the view to update it.
		InterfacePtr<IBoolData> invalWhenPossibleData(disableDrawingData, IID_EXTLINKPANELSHOULDINVALDATA);
		if(invalWhenPossibleData->Get())
		{
			invalWhenPossibleData->Set(kFalse);
			InterfacePtr<IControlView> panelView(disableDrawingData,UseDefaultIID());
			panelView->Invalidate();
		}
	}
}



