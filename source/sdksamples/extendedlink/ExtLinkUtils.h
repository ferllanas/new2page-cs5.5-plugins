//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkUtils.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __ExtLinkUtils__
#define __ExtLinkUtils__

// Interfaces:
#include "IXMLTagList.h"
#include "URI.h"

class ITextModel;

/** Utility methods for the ExtLink plug-in.

	@ingroup extendedlink
*/
class ExtLinkUtils
{
public:

//	Text manipulation
	/**
		Replace the given range of text with the WideString supplied.
		@param iTextModel is the text model contant to be manipulated.
		@param textIndex is where the starting text to be replaced.
		@param length is the range of text to be replaced.
		@param val is the text to replace the old text.
		@return	ErrorCode of the processed command result.
	*/
	static ErrorCode ReplaceText(ITextModel* iTextModel, const TextIndex& textIndex, int32 length, const boost::shared_ptr<WideString>& wideVal);

    /** Acquire XMLExtLink XML Tags. Create new tag if they have not been created before
		@param tagList XML tag list of a document.
		@param tagName is the name of XML tag.
    */
	static UIDRef AcquireOneTag(IXMLTagList* tagList,const WideString& tagName );

	/**
		Tag the specified range of text with XML tag, and create one attribtue with attribute value.

		@param tagUID XML tag UID used to tag the text.
		@param story is the story of the text to be manipulated.
		@param startIndex is the starting index at which the text is to be tagged.
		@param endIndex is the ending index at which the text is to be tagged.
		@param attrName is the attribute name of the XML element be created.
		@param attrValue is the value of the attribute of the XML element be created.
		@param uri is the link resource of the XML element to be created.
		@return	the error code of command processing result.
	*/
	static ErrorCode TagTextWithAttributeValue(UID tagUID, 
									UIDRef story, TextIndex startIndex, TextIndex endIndex, 
									const WideString attrName, const WideString attrValue, const URI &uri);

	/** Get path the user has chosen, if it exists
		@return PMString containing path selected by user, empty if none chosen
	*/
	static PMString GetSelectedFolderPath();


	/** Get data source the user has chosen, if it exists
		@return PMString containing data source selected by user, empty if none chosen
	*/
	static PMString GetSelectedDataSource();


	/** Get table name the user has chosen, if it exists
		@return PMString containing table name selected by user, empty if none chosen
	*/
	static PMString GetSelectedTableName();
	
	/** 
		@return bool16, true set show in UI flag is set, false otherwise
	*/
	static bool16 IsDisplayExtLinksInLinksPanel();

	/** 
		@return bool16, true auto refresh resource status flag is set, false otherwise
	*/
	static bool16 IsAutoRefreshStatus();

};

#endif 		__ExtLinkUtils__

// End, ExtLinkUtils.h


