//========================================================================================
//  
//  $File: //depot/indesign_5.0/highprofile/source/sdksamples/framelabel/IFrmLblData.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:27:55 $
//  
//  $Revision: #1 $
//  
//  $Change: 505962 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IFrmLblData_h__
#define __IFrmLblData_h__

// Interface includes:
#include "IPMUnknown.h"

// General includes:

// Project includes:
#include "N2P2ID.h"

class PMString;

/** 
	From SDK sample; interface to store persistent data to control the display of
	the frame label adornment.

	@ingroup framelabel
	
*/
class IFrmLblData : public IPMUnknown
{
public:

	enum	{kDefaultIID = IID_IFRMLBLDATA};

	/** Accessor 
		Sets the properties of this class
		@param	theLabel	String to be displayed by the frame label.
		@param	width		Width of label display area in points
		@param	size		Height of label text in points
		@param	visible		Flag to enable or disable the label display.
	*/
	virtual void Set
					(
						const PMString& theLabel,
						const int32 width,
						const int32 size,
						const bool16 visible
					) = 0;
	
	/** Accessor
		@return	Label string
	*/
	virtual const PMString&	GetString() = 0;
	/** Accessor
		@return	Label display area width in points
	*/
	virtual int32 GetWidth() = 0;
	/** Accessor
		@return	Height of label text in points
	*/
	virtual int32 GetSize() = 0;
	
	/** Accessor
		@return	Display flag value
	*/
	virtual bool16 GetVisibility() = 0;
	
};

#endif // __IFrmLblData_h__

// End, IFrmLblData.h.


