//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkODBCLinkResourceHandler.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//  
//  Description:
//  
//========================================================================================

#pragma once
#ifndef __FileLinkResourceHandler__
#define __FileLinkResourceHandler__

// ----- Includes -----
#include "CPMUnknown.h"
#include "ILinkResourceHandler.h"


/** The resource handler for link resource with an URI whose scheme is 'odbc'.  A resource handler is required when you create link
	with youe own URI scheme.  By default InDesign's file link has its URI begins with file://... the file in the URI is the scheme.
	Therefore, in the InDesign code base, there is a resource handler that knows how to handle link resource of URI scheme 'file'.
	
	ExtLink creats a new type of link with URI scheme 'odbc', its URI beings with odbc://... This resource handler ExtLinkODBCLinkResourceHandler
	provides important data for the link resource, such as the import stream.  When a odbc link needs to import data, this resource 
	handler will be the one called upon by links architecture to actually talk to odbc and retrieve data from the database.

	@ingroup extendedlink
*/
class ExtLinkODBCLinkResourceHandler : public CPMUnknown<ILinkResourceHandler>
{
public:
	typedef CPMUnknown<ILinkResourceHandler> inherited;
	typedef object_type data_type;

	ExtLinkODBCLinkResourceHandler(IPMUnknown* boss);
	virtual ~ExtLinkODBCLinkResourceHandler();


	/**	@see ILinkResourceHandler::Init
	 */
	virtual bool Init(const UIDRef& ref, const URI& uri);
	
	/**	
		We want to make sure the URI is the type we can handle, which has the scheme 'odbc'
		
		@see ILinkResourceHandler::IsResourceURIValid
	 */
	virtual bool IsResourceURIValid(const UIDRef& ref, const URI& uri) const;
	
	/**	@see ILinkResourceHandler::AreResourceIdsEquivalent
	 */
	virtual bool AreResourceIdsEquivalent(const ILinkResource::ResourceId& id1, const ILinkResource::ResourceId& id2) const;
	
	/**	
		There are 2 types of data we have for odbc, one is image type, the other type is text, based on the URI, we have to figure out
		which type the URI is actually referring to.
		
		@see ILinkResourceHandler::GetResourceDataType
	 */
	virtual FileTypeInfo GetResourceDataType(const UIDRef& ref, const URI& uri) const;

	/**	@see ILinkResourceHandler::GetShortResourceName
	 */
	virtual WideString GetShortResourceName(const UIDRef& ref, const URI& uri, bool bUIName) const;
	
	/**	@see ILinkResourceHandler::GetLongResourceName
	 */
	virtual WideString GetLongResourceName(const UIDRef& ref, const URI& uri, bool bUIName) const;

	/**	@see ILinkResourceHandler::GetResourceStateInfo
	 */
	virtual PMString GetResourceStateInfo(const UIDRef& ref, const URI& uri, ILinkResource::ResourceState state, ILinkResource::ResourceStoreState storeState) const { return PMString(); }

	/**	@see ILinkResourceHandler::AreStampsEquivalent
	 */
	virtual bool AreStampsEquivalent(const WideString& stamp1, const WideString& stamp2) const;

	/**	@see ILinkResourceHandler::CanReadResource
	 */
	virtual bool CanReadResource(const UIDRef& ref, const URI& uri) const;
	
	/**	@see ILinkResourceHandler::CanWriteResource
	 */
	virtual bool CanWriteResource(const UIDRef& ref, const URI& uri) const;
	
	/**	@see ILinkResourceHandler::CanReadWriteResource
	 */
	virtual bool CanReadWriteResource(const UIDRef& ref, const URI& uri) const;

	/**	@see ILinkResourceHandler::CanCreateResourceStream
	 */
	virtual bool CanCreateResourceStream(const UIDRef& ref, const URI& uri, ILinkResource::AccessMode mode) const;
	
	/**	
		Depends on the data type, open the resource read stream for the link resource.  For image link, it creates a file stream 
		from the file.  For text, it grabs the data from the database, stuff the data into the a pointer based stream and return
		the stream so import provider can use it to do the import.
	
		@see ILinkResourceHandler::CreateResourceReadStream
	 */
	virtual IPMStream* CreateResourceReadStream(const UIDRef& ref, const URI& uri) const;
	
	/**	@see ILinkResourceHandler::CreateResourceWriteStream
	 */
	virtual IPMStream* CreateResourceWriteStream(const UIDRef& ref, const URI& uri) const;
	
	/**	@see ILinkResourceHandler::CreateResourceReadWriteStream
	 */
	virtual IPMStream* CreateResourceReadWriteStream(const UIDRef& ref, const URI& uri) const;

	/**	@see ILinkResourceHandler::CanCacheResource
	 */
	virtual bool CanCacheResource(const UIDRef& ref, const URI& uri) const { return true; }
	
	/**	@see ILinkResourceHandler::CanEmbedResource
	 */
	virtual bool CanEmbedResource(const UIDRef& ref, const URI& uri) const { return true; }
	
	/**	@see ILinkResourceHandler::CanUnembedResource
	 */
	virtual bool CanUnembedResource(const UIDRef& ref, const URI& uri) const { return true; }

	/**	
		@see ILinkResourceHandler::CanCopyToFile
	 */
	virtual bool CanCopyToFile(const UIDRef& ref, const URI& uri) const;

	/**	
		Don't support this in this sample.
		
		@see ILinkResourceHandler::CopyToFile
	 */
	virtual ErrorCode CopyToFile(const UIDRef& ref, const URI& uri, IDFile& file) const;

	/**	
		Don't support this in this sample, return false.
		
		@see ILinkResourceHandler::CanEditResource
	 */
	virtual bool CanEditResource(const UIDRef& ref, const URI& uri) const;
	
	/**	
		Don't support this in this sample.
		
		@see ILinkResourceHandler::EditResource
	 */
	virtual ErrorCode EditResource(const UIDRef& ref, const URI& uri, const AppInfo& appInfo, PMString* errorString) const;

	/**	
		Don't support this in this sample, return false.
		
		@see ILinkResourceHandler::CanRevealResource
	 */
	virtual bool CanRevealResource(const UIDRef& ref, const URI& uri) const;
	
	/**	
		Don't support this in this sample.
		
		@see ILinkResourceHandler::RevealResource
	 */
	virtual ErrorCode RevealResource(const UIDRef& ref, const URI& uri) const;
	
	/**	
		Don't support this in this sample, return false.
		
		@see ILinkResourceHandler::CanRevealResourceInBridge
	 */
	virtual bool CanRevealResourceInBridge(const UIDRef& ref, const URI& uri) const;
	
	/**
		Don't support this in this sample.
		
		@see ILinkResourceHandler::RevealResourceInBridge
	 */
	virtual ErrorCode RevealResourceInBridge(const UIDRef& ref, const URI& uri) const;
	
	/**	
	 Don't support this in this sample, return false.
	 
	 @see ILinkResourceHandler::CanRevealResourceInMiniBridge
	 */
	virtual bool CanRevealResourceInMiniBridge(const UIDRef& ref, const URI& uri) const;
	
	/**
	 Don't support this in this sample.
	 
	 @see ILinkResourceHandler::RevealResourceInMiniBridge
	 */
	virtual ErrorCode RevealResourceInMiniBridge(const UIDRef& ref, const URI& uri) const;
protected:

private:
	// Prevent copy construction and assignment.
	ExtLinkODBCLinkResourceHandler(const ExtLinkODBCLinkResourceHandler&);
	ExtLinkODBCLinkResourceHandler& operator=(const ExtLinkODBCLinkResourceHandler&);
	void AllocateBuffer(const int32 length);
	mutable char* mBuffer;
};

#endif // __FileLinkResourceHandler__
