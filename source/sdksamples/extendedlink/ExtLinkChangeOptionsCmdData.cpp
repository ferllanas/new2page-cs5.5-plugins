//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkChangeOptionsCmdData.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface Includes

// Implementation Includes
#include "CPMUnknown.h"
#include "K2Vector.tpp"

// Project includes:
#include "IExtLinkOptions.h"
#include "IExtLinkChangeOptionsCmdData.h"

/** Persistent implementation of interface IExtLinkChangeOptionsCmdData. 
	It is used by the command to change the options for this plug-in.

	
	@ingroup extendedlink

*/
class ExtLinkChangeOptionsCmdData : 
	public CPMUnknown<IExtLinkChangeOptionsCmdData>
{
public:
	/**
		Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.
	*/
	ExtLinkChangeOptionsCmdData(IPMUnknown* boss);

	/** 
		Destructor
	*/
	virtual ~ExtLinkChangeOptionsCmdData() {}

	/** Add option to list of options stored on this.
		@param newVal [IN] new  value to append to list.
	*/
	virtual void AddOption(const ExtLinkOptionsData& newVal); 

	/** Accessor for option at zero-based index in list of options
		@param index [IN] position of interest
		@return option at given position, or empty string if invalid index
	*/
	virtual ExtLinkOptionsData GetNthOption(const int32 index); 

	/** Accessor for size of options list
		@return size of list of options
	*/
	virtual int32 GetOptionCount() { return fOptions.size(); }

private:
	bool16 validate(const int32 index);
	K2Vector<ExtLinkOptionsData> fOptions;
};	

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(ExtLinkChangeOptionsCmdData, kExtLinkChangeOptionsCmdDataImpl)

/*	Constructor
*/
ExtLinkChangeOptionsCmdData::ExtLinkChangeOptionsCmdData(IPMUnknown* boss) :
	CPMUnknown<IExtLinkChangeOptionsCmdData>(boss)
{
}


/* AddOption
*/
void ExtLinkChangeOptionsCmdData::AddOption(const ExtLinkOptionsData& newVal) 
{ 
	fOptions.push_back(newVal); 
}


/* GetNthOption
*/
ExtLinkOptionsData ExtLinkChangeOptionsCmdData::GetNthOption(const int32 index)
{ 
	ASSERT(validate(index));
	if(this->validate(index))
	{
		return fOptions[index]; 
	}
	return ExtLinkOptionsData();
}


/* validate
*/
bool16 ExtLinkChangeOptionsCmdData::validate(const int32 index)  
{
	return index >= 0 && index < fOptions.size();
}

//	end, File:	ExtLinkChangeOptionsCmdData

