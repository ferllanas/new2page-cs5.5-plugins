//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/IExtLinkSuite.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IExtLinkSuite_h__
#define __IExtLinkSuite_h__

// Interface includes:
#include "IPMUnknown.h"
#include "URI.h"
#include "URIList.h"

// Project includes:
#include "ExtLinkID.h"

/** Abstract suite interface for ExtLink to insert data into a text frame through odbc connection.
  
	@ingroup extendedlink
*/
class IExtLinkSuite : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IExtLinkSUITE };


	/** Check if data can be inserted at the position.

		@return kTrue if tag can be inserted, other ErrorCode otherwise.
	*/
	virtual bool16 CanInsertData() const = 0;

	/** 
		Insert every value in the given records of database.
		@pre IExtLinkSuite::CanInsertData returns kTrue
		@param file
		@return kSuccess on success, other ErrorCode otherwise.
	*/
	virtual ErrorCode InsertSelectedRecords(const URIList& uriList) = 0;

};

#endif
