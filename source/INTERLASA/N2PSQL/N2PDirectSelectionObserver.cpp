/*
 *  N2PDirectSelectionObserver.cpp
 *  N2PDirect
 *
 *  Created by Fernando  Llanas on 16/02/10.
 *  Copyright 2010 Interlasa. All rights reserved.
 *
 */
#include "VCPlugInHeaders.h"

#include "SelectionObserver.h"

// Interface includes:
#include "IApplication.h"
#include "ISession.h"
#include "IWorkspace.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IWidgetParent.h"
#include "IListControlData.h"
#include "CObserver.h"
#include "ISubject.h"
#include "IPalettePanelUtils.h"
#include "IOpenFileDialog.h"
#include "ITextControlData.h"
#include "ILayoutUIUtils.h" //GetFrontDocument
#include "IDocument.h"
#include "IHierarchy.h"
#include "IDropDownListController.h"
#include "ILinkManager.h"
#include "IDataLink.h"
#include "ILinkResource.h"
#include "ILinkFacade.h"
#include "ILink.h"
#include "ILinkUtils.h"
#include "IURIUtils.h"
#include "IImportResourceCmdData.h"
#include "ICommand.h"
#include "IComposeScanner.h"
#include "IGraphicFrameData.h"
#include "ITextColumnSizer.h"
#include "IMultiColumnTextFrame.h"
#include "IPlaceGun.h"
#include "IBoolData.h"
#include "IUIDData.h"
#include "IListBoxController.h"
#include "ITreeViewMgr.h"
#include "ITreeViewController.h"
#include "ITreeViewHierarchyAdapter.h"
#include "IStringListData.h"
#include "IPageList.h"
#include "IPMStream.h"
#include "IDFile.h"
#include "IDocumentList.h"
#include "IFileList.h"
#include "IPageSetupPrefs.h"
#include "IGeometry.h"
#include "ITextUtils.h"
#include "IParcel.h"
#include "IFrameList.h"
#include "IParcelList.h"
#include "ITextParcelList.h"
#include "ITextFrameColumn.h"
#include "PreferenceUtils.h"
#include "TransformUtils.h"
#include "ITextParcelListComposer.h"
#include "ITOPSplineData.h"
#include "IPlugInList.h"
#include "ISpread.h"
#include "ISpreadList.h"
#include "IPageItemTypeUtils.h"
#include "IPathGeometry.h"
#include "IPathUtils.h"
#include "ISelectionUtils.h"
#include "ISelectionManager.h"
#include "IConcreteSelection.h"
#include "ILayoutTarget.h"

#ifdef WINDOWS
#include <sys/types.h> 
#include <sys/stat.h> 
#endif
#ifdef MACINTOSH
#include <types.h> 
#include <sys/stat.h>
#endif

// General includes:
#include "SDKUtilities.h"
#include "UIDList.h"
#include "SysFileList.h"
#include "CAlert.h"
#include "LinksID.h"
#include "SDKLayoutHelper.h"
#include "OpenPlaceID.h"
#include "SnapshotUtils.h"
#include "StreamUtil.h"
#include "ProgressBar.h"
#include "K2Vector.tpp" // For NodeIDList to compile

// Project includes:
#include "N2PsqlID.h"
#include "N2PSQLUtilities.h"
//#include "XMPClass.h"
#include "InterlasaUtilities.h"
//#include "XMPClass.h"
#include "FileUtils.h"
//#include "IN2PDirectRegisterUsers.h"
//"Scroll"
//#include "WLBCmpNodeID.h"
//#include "DebugTimeLog.h"
//#include "N2PDirectUtilities.h"

/** SelDlgIconObserver
 Allows dynamic processing of icon button widget state changes, in this case
 the tab dialog's info button. 
 
 Implements IObserver based on the partial implementation CObserver. 
 
 @author Lee Huang
 */
class N2PDirectSelectionObserver : public ActiveSelectionObserver
{
public:	
	//StructAvisos Avisos;
	/**
	 Constructor.
	 
	 @param boss interface ptr from boss object on which interface is aggregated.
	 */
	N2PDirectSelectionObserver(IPMUnknown *boss); //: CObserver(boss) {}
	
	/** Destructor. */
	virtual ~N2PDirectSelectionObserver();
	
protected:
	
	/**
	 When an object is added or removed from the selection or if the kind
	 of selection changes (e.g. from a a layout selection to a text selection)
	 this method is called which results in N2PDirectSelectionObserver::UpdatePanel
	 being called.
	 @param message from selection describing change or nil if the active selection changed.
	 */
	virtual void HandleSelectionChanged(const ISelectionMessage* message);
	
	/**
	 If the selection's data changes this method gets called. We check
	 that data we are interested in is affected before taking any action
	 by calling ISelectionMessage::WasSuiteAffected and checking that
	 a message from IDummySuite is present. If so we update the data in the
	 panel's widgets to reflect that of the selection.
	 @param message from selection indicating the attribute(s) affected
	 */
	virtual void HandleSelectionAttributeChanged (const ISelectionMessage* message);
	
	/**
	 Update the panel to reflect selection's IDummySuite data.
	 */
	void		UpdatePanel();
	
	
	//void llenarListaArticulos(IDocument* theFrontDoc);
	
	
	
	UIDList getUIDRefOfSelectionObserver();
	
	/*	
	 Deselecciona todo en lista de componentes
	 */
	//void SalirDeSeleccionEnListaComponentes();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
 */
CREATE_PMINTERFACE(N2PDirectSelectionObserver, kN2PDirectSelectionObserverSuiteImpl)

N2PDirectSelectionObserver::N2PDirectSelectionObserver(IPMUnknown* boss)
: ActiveSelectionObserver(boss)
{
}

/* Destructor
 */
N2PDirectSelectionObserver::~N2PDirectSelectionObserver()
{
}
/*
 */
void N2PDirectSelectionObserver::HandleSelectionChanged(const ISelectionMessage* message)
{
	UIDList myList= getUIDRefOfSelectionObserver();
	if(myList.Length()<=1)
	{
		this->UpdatePanel();
	}
	/*else
	 if(myList.Length()>1)
	 {
	 //this->QuitSSelectionOnTree();
	 this->SalirDeSeleccionEnListaComponentes();
	 N2PDirectUtilities::LimpiarListaDComponentes(kN2PDirectPanelWidgetID,kN2DirectComponentsListBoxWidgetID,kN2PDirectDragHereIconWidgetID,kTrue);
	 }
	 */
	IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
	//si no exite un documento en frente sale de la funcion
	if (document != nil)
	{	
		if(message!=nil)
		{
			ISelectionMessage::SelectionMessageType mensaje= message->GetMessageType();
			switch (mensaje) {
				case ISelectionMessage::kSelectionChanged:
					//CAlert::InformationAlert("kClientMessage");
					break;
				case ISelectionMessage::kSelectionAttributeChanged:
					//CAlert::InformationAlert("kASB_SuiteMessage");
					break;
					
				case ISelectionMessage::kSelectionChanged_Frequent:
					//CAlert::InformationAlert("kCSB_NeedsNotify");
					break;
				default:
					//CAlert::InformationAlert("Otro che mensaje");
					break;
			}
			
		}
		else
		{
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			PreferencesConnection PrefConections;
			if(UpdateStorys!=nil)
			{
				UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
			}
			
			
			//this->UpdatePanel();
			//CAlert::InformationAlert("AÑ 8");
			N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse, PrefConections);
			PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
			if(Id_Seccion.NumUTF16TextChars()>0 && Id_Seccion!=" ")
				N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
			//N2PDirectUtilities::llenarListaArticulos(document);
			//N2PDirectUtilities::LimpiarListaDComponentes(kN2PDirectPanelWidgetID,kN2DirectComponentsListBoxWidgetID,kN2PDirectDragHereIconWidgetID,kTrue);
			//WidgetID PaletteID,WidgetID ListElementWidgetid, WidgetID pictureId,bool16 MuestraPicture)
		 
		}
	}
	
	
}

/*
 */
void N2PDirectSelectionObserver::HandleSelectionAttributeChanged( const ISelectionMessage* iSelectionMsg)
{
	// It is very important that you filter this call for a suite PMIID.
	// Your active selection observer is called when any attribute of the
	// selection changes, so you only want to update widgets when a change
	// that is of interest occurs.
	
	/*if (iSelectionMsg->WasSuiteAffected(IDummySuite::kDefaultIID)) {
	 // Always call ISelectionMessage::WasSuiteAffected before 
	 // taking any action. Otherwise your implementation is inefficient.
	 this->UpdatePanel();
	 FluirFunciones::ActivarCapa();
	 }*/
	//CAlert::InformationAlert("SAASSA");
}

/*
 */
void N2PDirectSelectionObserver::UpdatePanel()
{
	do 
	{
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		//si no exite un documento en frente sale de la funcion
		if (document == nil)
		{	
			//CAlert::InformationAlert("no document");
			break;
		}
		
		//N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL("simple", kFalse);
		//PMString Id_Seccion =  N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
		//CAlert::InformationAlert("A1");
		//if(Id_Seccion.NumUTF16TextChars()>0 )
		//	N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Id_Seccion);
	}while (false);
}






UIDList N2PDirectSelectionObserver::getUIDRefOfSelectionObserver()
{
	UIDList result;
	do
	{		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			break;
		}
		
		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			break;
		}
		
		//	Para textFrame 
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			ASSERT_FAIL("pTextSel");
			break;
		}
		
		
		UIDRef selectedFrameRef = UIDRef::gNull;
		UID selectedFrameItem = kInvalidUID;
		InterfacePtr<ILayoutTarget> pLayoutTarget(pTextSel, UseDefaultIID());    
		if (pLayoutTarget)
		{
			result = pLayoutTarget->GetUIDList(kStripStandoffs);
			
		}
		
	}	while(false);
	return(result);
}


