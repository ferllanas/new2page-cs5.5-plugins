//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUIDataNode.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __ExtLinkUIDataNode_H_DEFINED__
#define __ExtLinkUIDataNode_H_DEFINED__


#include "URI.h"
#include <map>






/** Class to represent a generic node within a tree, with data
	that is encapsulated in a PMString.
	
	@ingroup extendedlinkui
*/

class ExtLinkUIDataNode
{
public:
	/** Constructor */
	ExtLinkUIDataNode(URI uri, UID uid);

	/** Destructor */
	virtual ~ExtLinkUIDataNode();

	// +K2Vector support

	/**	Copy constructor
		@param rhs [IN] node reference
		@return  
	 */
	ExtLinkUIDataNode(const ExtLinkUIDataNode& rhs);

	/**	Operator assignment
		@param rhs [IN] node reference
		@return ExtLinkUIDataNode& 
	 */
	ExtLinkUIDataNode& operator=(const ExtLinkUIDataNode& rhs);

	/** For K2Vector copy semantics
	*/
	typedef object_type data_type;
	// -K2Vector support

	/**	Operator equality
		@param rhs [IN] node reference
		@return bool 
	 */
	bool operator==(const ExtLinkUIDataNode& rhs) const;


	/**	Accessor for child by zerobased index in parent's list of kids
		@param indexInParent [IN] which child by zero-based index
		@return child at given index 
	 */
	const ExtLinkUIDataNode* GetNthChild(int32 indexInParent) const;

	/**	Accessor for parent
		@return reference to parent node
	 */
	ExtLinkUIDataNode* GetParent() const;

	/** Mutator for the parent of this node
		@param p [IN] specifies new parent to set
	 */
	void SetParent(const ExtLinkUIDataNode* p);


	/**	Accessor for size of child list on this node
		@return int32 giving the number of children on this node 
	 */
	int32 ChildCount() const;

	/**	Get the URI associated with this node
		@return data represented here 
	 */
	URI GetNodeURI() const;

	/**	Mutator for URI stored on this
		@param data [IN] specifies the new data to store
	 */
	void SetNodeURI(const URI theURI);

	/**	Get the UID associated with this node
		@return data represented here 
	 */
	UID GetLinkUID() const;

	/**	Mutator for UID stored on this
		@param data [IN] specifies the new data to store
	 */
	void SetLinkUID(const UID theUID);

	/**	Add child to this node (at end of list)
		@param o [IN] child node to add
	 */
	void AddChild(const ExtLinkUIDataNode* o);

	/**	Remove specified child from our list
		@param o [IN] specifies child to remove
	 */
	void RemoveChild(const ExtLinkUIDataNode* o);



private:

	ExtLinkUIDataNode* fParent;
	K2Vector<ExtLinkUIDataNode*> fChildren;

	URI	fURI;
	UID fUID;

	void deepcopy(const ExtLinkUIDataNode& rhs);
};

#endif // __ExtLinkUIDataNode_H_DEFINED__

