-- MySQL dump 10.11
--
-- Host: localhost    Database: idsdk
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `idsdk`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `idsdk` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `idsdk`;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (
  `sku` char(6) NOT NULL,
  `name` varchar(50) default NULL,
  `price` double default NULL,
  `quantity` int(11) default NULL,
  `image` varchar(255) default NULL,
  `description` varchar(1023) default NULL,
  `version` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`sku`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
INSERT INTO `inventory` VALUES ('AB-223','Cutter & Buck Leather Zippered Padfolio',64.95,12,'CBLatherPadfolioAB-223.gif','Genuine top grain leather 13\"H x 10.25\"W x 1.62\"D Color: Mahogany Includes an interior organizer with gusseted file pocket, PDA/electronics Pockets, zippered closure, open front pocket, 8.5\"x11\" writing pad. Signature Cutter & Buck lining. Includes 2-piece Cutter & Buck gift box. The Adobe logo is tastefully debossed on the front cover.','2008-01-16 22:06:45'),('AB-219','17\" Laptop Sleeve',14.5,26,'LaptopSleeveAB-219.gif','The large, easy-access main compartment on this bag will accommodate up to a 17\" laptop. Features: dual top haul handles and durable scuba like fabric. Individually polybagged. The Adobe logo is imprinted in white.','2007-12-13 23:58:17'),('AB-121','Men\'s Easy Care Black Dress Shirt',23,19,'MansEasyCareShirtAB-122.gif','Great for travel and trade shows this easy care herringbone pattern button down shirt comes through for you. Features include: special stain-repellent finish, button down collar, left chest pocket, two button adjustable cuffs and buttoned sleeve placket. An extra button is sewn on center front hem. Machine washable. The Adobe logo is embroidered on the left chest.','2007-12-13 23:58:17'),('AB-123','Men\'s Pima Pique Long Sleeve Polo',26.75,100,'MensPimaLongSleeveAB-123.gif','When you want to be a little more dressed up but don\'t want a button down, this 100% Peruvian pima cotton long sleeve polo fits the ticket. Features additional yarn in collar and cuffs for a neater appearance and greater durability; three-button placket, Dura-pearl','2007-12-13 23:58:17'),('AB-307','Mini Auto Open/Close Umbrella',13.75,101,'MiniAutoUmbrellaAB-307.gif','Stylish 43\" Arc mini fold umbrella with auto open and close feature. Mahogany wood hook handle. Pongee fabric with black frame. Closes to a civilized 12\" in length. The Adobe logo is imprinted in two colors on one panel. ','2007-12-13 23:58:17'),('AB-221','Red Mini Optical Mouse',10.5,96,'RedMouseAB-221.gif','This sleek looking retractable mini optical mouse will brighten any desk. Glossy red with chrome accents. Size: 3','2007-12-13 23:58:17'),('AB-222','Round 4 Port Hub',11,97,'Round4PortHubAB-222.gif','This is an extremely affordable yet high quality 4 port USB hub. It','2007-12-13 23:58:17'),('AB-122','Women\'s Easy Care Black Dress Shirt',23,51,'WomansEasyCareShirtAB-122.gif','Great for travel and trade shows this easy care herringbone pattern button down shirt will not let you down. Features include: special stain-repellent finish, three-quarter sleeve with adjustable cuffs, straight point collar and curved hem. Machine washable. The Adobe logo is embroidered on the left chest.','2007-12-13 23:58:17'),('AB-220','World Travel Adapter',24,48,'WorldTravelAdapterAB-220.gif','Swiss design, lifetime warranty. This single adapter contains options for more than 150 countries in a single unit - no pieces to lose or fumble with. Adapter doe not convert voltage. The Adobe logo is imprinted in white.','2007-12-13 23:58:17');
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2008-01-18 23:07:54
