//========================================================================================
//  
//  $File: //depot/indesign_5.0/highprofile/source/sdksamples/framelabel/N2PSqlPenUnLockData.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2007/02/15 13:27:55 $
//  
//  $Revision: #1 $
//  
//  $Change: 505962 $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

// General includes:
#include "IPMStream.h"
#include "CPMUnknown.h"

// Project includes:
#include "IN2PSqlPenUnLockData.h"
#include "N2PAdIssueID.h"

// forward declarations
class IPMStream;


/** Stores persistent data to control the display of
	the frame label adornment; implements IFrmLblData.
  
	@ingroup framelabel
	
*/
class N2PSqlPenUnLockData : public CPMUnknown<IN2PSqlPenUnLockData>
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSqlPenUnLockData(IPMUnknown*  boss);
		/** 
			Destructor.
		*/
		virtual ~N2PSqlPenUnLockData();

		/**
			Stores new persistent values for the label adornment. These values
			correspond 1:1 to the class properties.
			@param theLabel String to be displayed as the label.
			@param width The bounding width (points) of the adornment box.  Labels larger than this limit are truncated.
			@param size The bounding height (points) of the adornment box. Labels taller than this limit are truncated.
			@param visibility Flag, kTrue indicating the adornment should be displayed.
		*/
		virtual void Set
						(
						int32 LockUnLock, 
						bool16 visibility
						);

		/** 
			Retrieves the current string to be displayed as the adornment.
			@return The adornment label string.
		
		virtual const PMString&	GetString() { return fLabel; }*/
		/**
			Retrieves the width limit of the adornment.
			@return The width in points.
		
		virtual int32 GetWidth() { return fWidth; }*/
		/**
			Retrives the height limit of the adornment.
			@return The height in points.
		
		virtual int32 GetSize() { return fSize; }*/

		/** 
			Retrieves the display flag.
		*/
		virtual bool16 GetVisibility() { return fVisibility; }

		/** Saves persistently, reads persistent data.
			Reads if the stream is a 'read-stream'
			and writes if the stream is a write stream:
		*/
		void ReadWrite(IPMStream*  stream, ImplementationID implementation);
		
		virtual int32 GetLockOrUnlock() { return lockOnUnLock; }

	private:
		/** String label to be displayed */		
	//	PMString	fLabel;				
		/** Width limit of display box */
	//	int32		fWidth;	
		/** Height limit of display text */
		int32		lockOnUnLock;
		/** Display flag, kTrue means display the adornment */
		bool16		fVisibility;};


/* CREATE_PERSIST_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PERSIST_PMINTERFACE(N2PSqlPenUnLockData, kN2PSqlPenUnLockDataImpl)

/* N2PSqlPenUnLockData Constructor
	The initial values in the member values indicate the default persistent values.
	Also see the SchemaList resource in FrmLbl.fr.
*/
N2PSqlPenUnLockData::N2PSqlPenUnLockData(IPMUnknown* boss) : 
	CPMUnknown<IN2PSqlPenUnLockData>(boss), 
	lockOnUnLock(0), 
	fVisibility(kFalse)
{
	//fLabel.SetTranslatable(kFalse);
}

/* N2PSqlPenUnLockData Destructor
*/
N2PSqlPenUnLockData::~N2PSqlPenUnLockData()
{
	// Add code to delete extra private data, if any.
}

/*	Set
*/
void N2PSqlPenUnLockData::Set
(
	//const PMString& newLabel, 
//	int32 newWidth, 
	int32 newLockOrUnLock, 
	bool16 visibility
)
{
	PreDirty ();
	//	fLabel = newLabel;
//	fWidth = newWidth;
	lockOnUnLock = newLockOrUnLock;
	fVisibility = visibility;
	
}

/* ReadWrite

	DATA FORMAT HISTORY: 
		See SchemaList in FrmLbl.fr
*/
void N2PSqlPenUnLockData::ReadWrite(IPMStream*  stream, ImplementationID implementation)
{
	//	fLabel.ReadWrite(stream);
//	stream->XferInt32(fWidth);
	stream->XferInt32(lockOnUnLock);
	stream->XferBool(fVisibility);
}

// End, N2PSqlPenUnLockData.cpp.






