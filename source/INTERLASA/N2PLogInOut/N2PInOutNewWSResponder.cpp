//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/customprefs/N2PInOutNewWSResponder.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2003/12/18 11:20:39 $
//  
//  $Revision: #1 $
//  
//  $Change: 237988 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ICommand.h"
#include "INewWorkspaceSignalData.h"
#include "ISignalMgr.h"
#include "IWorkSpace.h"
#include "IDialogMgr.h"
#include "IApplication.h"

// General includes:
#include "CmdUtils.h"
#include "CResponder.h"
#include "ErrorUtils.h"
#include "Utils.h"
#include "CAlert.h"
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"

// Project includes:
#include "N2PInOutID.h"
#include "N2PRegisterUsers.h"
#include "IRegisterUsersUtils.h"



/**	Responder for a new workspace. 
 * 	This responder gets called when a brand new global workspace is being created. 
 * 	We use this to set a default state for a preference item.
 * 	To have this responder get called, delete your InDesign Defaults file before
 * 	starting the InDesign application.
 * 
 *  @ingroup customprefs
 * 	@author Ken Sadahiro
 */
class N2PInOutNewWSResponder : public CResponder
{
public:
	/** Constructor.  Does nothing.
	 */
	N2PInOutNewWSResponder(IPMUnknown *boss) 
		: CResponder(boss) {}

	/** Respond to the new workspace signal.
	 * 	@param signalMgr IN Provides information regarding the signal. 
	 */
    virtual void Respond(ISignalMgr* signalMgr);
private:
	
	void OpenDlgLogIn();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/

CREATE_PMINTERFACE(N2PInOutNewWSResponder, kN2PInOutNewWSResponderImpl)

/* Respond
*/
void N2PInOutNewWSResponder::Respond(ISignalMgr* signalMgr)
{
	ErrorCode status = kFailure;
	do {
		// verify we can get to the signal manager.
		if (signalMgr == nil) 
		{
			ASSERT(signalMgr); break;
		}
		// get the new workspace signal data
		InterfacePtr<INewWorkspaceSignalData> newWorkspaceSignalData(signalMgr, UseDefaultIID());
		if (newWorkspaceSignalData == nil) 
		{
			ASSERT(newWorkspaceSignalData); break;
		}
		// get the new workspace
		InterfacePtr<IWorkspace> workspace(newWorkspaceSignalData->GetWorkspace(), UseDefaultIID());
		if (workspace == nil) 
		{
			ASSERT(workspace); break;
		}
		
		this->OpenDlgLogIn();
	/*	// get custom prefs from global workspace
		InterfacePtr<ICustomPrefs> customPrefs(Utils<ICstPrfUtils>()->QueryCustomPrefs(workspace));
		if (customPrefs == nil) 
		{
			ASSERT(customPrefs); break;
		}

		// generate the command to preset the custom prefs value in the new workspace
		// NOTE: Since this responder is called from NewWorkspaceCmd, we might as well just
		// modify the model directly, but we will call a command just to be good InDesign 
		// API citizens.
		InterfacePtr<ICommand> modCstPrfCmd(Utils<ICstPrfUtils>()->CreateModPrefsCmd(customPrefs, kTrue,200,"Juani"));
		if (modCstPrfCmd == nil) 
		{
			ASSERT(modCstPrfCmd); break;
		}
		
		// process the command
		status = CmdUtils::ProcessCommand(modCstPrfCmd);
		ASSERT(status == kSuccess);*/

	} while (false);

	if (status != kSuccess) 
	{
		PMString errString("kCstPrfNewWSResponderFailedStringKey");
		errString.Translate();
		ErrorUtils::PMSetGlobalErrorCode(status);//, errString, kFalse, &errString
	}
}

// End, N2PInOutNewWSResponder.cpp.


/* OpenDlgLogIn
*/
void N2PInOutNewWSResponder::OpenDlgLogIn()
{
	do
	{
	
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
			if (wrkSpcPrefs == nil)
			{	
				ASSERT_FAIL("Invalid workspace prefs in N2PInOutNewWSResponder::OpenDlgLogIn()");
				break;
			}
		
		PMString Aplicacion="";
		Aplicacion.Append(wrkSpcPrefs->GetNameApp());
		
		PMString IDUsuario="";
		IDUsuario.AppendNumber(wrkSpcPrefs->GetID_Usuario());
		
		PMString HoraIn="";
		HoraIn.Append(wrkSpcPrefs->GetHoraIn());
		
		PMString FechaIn="";
		FechaIn.Append(wrkSpcPrefs->GetFechaIn());
		
		if(Aplicacion.NumUTF16TextChars()>1)
		{
			CAlert::InformationAlert(kN2PInOutAUsersLogedStringKey);
			break;
		}
		
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PInOutPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kSDKDefDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			break;
		}

		// Open the dialog.
		dialog->Open(); 
	
	} while (false);			
}

