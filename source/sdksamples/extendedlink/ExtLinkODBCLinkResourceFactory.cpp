//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkODBCLinkResourceFactory.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "CPMUnknown.h"
#include "ILinkResourceFactory.h"

#include "IDataBase.h"
#include "ILinkResourceHandler.h"
#include "ILinkResourceStateUpdater.h"
#include "ILinkResourceFactory.h"

#include "URI.h"
#include "WideString.h"
#include "ExtLinkID.h"


/** Implementation of ILinkResourceFactory to instantiate couple link resource related classes, 
	ILinkResourceHandler and ILinkResourceStateUpdater, both are aggregated on 
	kExtLinkODBCLinkResourceHandlerBoss, and they are to handle link resource who has the URI scheme
	of 'odbc'
	
	@ingroup extendedlink
*/
class ExtLinkODBCLinkResourceFactory : public CPMUnknown<ILinkResourceFactory>
{
public:
    typedef CPMUnknown<ILinkResourceFactory> inherited;
	typedef object_type data_type;

	/**	Constructor.
		@param boss IN interface ptr from boss object on which this interface is aggregated.
	*/
	ExtLinkODBCLinkResourceFactory(IPMUnknown* boss);
	/**	Destructor.
	*/
	virtual ~ExtLinkODBCLinkResourceFactory();

	/**
	 Returns the list of URI schemes supported by the handler, which in our case, odbc.
	 
	 @see ILinkResourceFactory::GetSchemes
	 @param schemes [OUT] List of supported schemes.
	 */ 
	virtual void GetSchemes(K2Vector<WideString>& schemes) const;

	/**
	 Returns the link resource handler, kExtLinkODBCLinkResourceHandlerBoss, that handles our 'odbc' type resource.
	 
	 @see ILinkResourceFactory::GetSchemes
	 */ 
	virtual ILinkResourceHandler* QueryHandler(const URI& uri, IDataBase* db) const;

	/**
	 Returns the link resource state updater who provides status update for link resource whose URI scheme is 'odbc'	 
	 @see ILinkResourceFactory::QueryStateUpdater
	 */ 
	virtual ILinkResourceStateUpdater* QueryStateUpdater(const URI& uri, IDataBase* db) const;

private:
	// Prevent copy construction and assignment.
	ExtLinkODBCLinkResourceFactory(const ExtLinkODBCLinkResourceFactory&);
	ExtLinkODBCLinkResourceFactory& operator=(const ExtLinkODBCLinkResourceFactory&);
};

CREATE_PMINTERFACE(ExtLinkODBCLinkResourceFactory, kExtLinkODBCLinkResourceFactoryImpl)


//========================================================================================
//
// ExtLinkODBCLinkResourceFactory Public Implementation
//
//========================================================================================

//========================================================================================
// Constructor
//========================================================================================
ExtLinkODBCLinkResourceFactory::ExtLinkODBCLinkResourceFactory(IPMUnknown* boss)
: inherited(boss)
{
}

//========================================================================================
// Destructor
//========================================================================================
ExtLinkODBCLinkResourceFactory::~ExtLinkODBCLinkResourceFactory()
{
}

//========================================================================================
// ExtLinkODBCLinkResourceFactory::GetSchemes
//========================================================================================
void ExtLinkODBCLinkResourceFactory::GetSchemes(K2Vector<WideString>& schemes) const
{
	const WideString fileScheme(kExtLinkScheme);

	schemes.clear();
	schemes.push_back(fileScheme);
}

//========================================================================================
// ExtLinkODBCLinkResourceFactory::QueryHandler
//========================================================================================
ILinkResourceHandler* ExtLinkODBCLinkResourceFactory::QueryHandler(const URI& uri, IDataBase* db) const
{
	return ::CreateObject2<ILinkResourceHandler>(kExtLinkODBCLinkResourceHandlerBoss);
}

//========================================================================================
// FileLinkResourceFactory::QueryStateUpdater
//========================================================================================
ILinkResourceStateUpdater* ExtLinkODBCLinkResourceFactory::QueryStateUpdater(const URI& uri, IDataBase* db) const
{
	return ::CreateObject2<ILinkResourceStateUpdater>(kExtLinkODBCLinkResourceHandlerBoss);
}
