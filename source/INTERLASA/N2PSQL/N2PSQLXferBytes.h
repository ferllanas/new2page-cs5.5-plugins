//========================================================================================
//  
//  $File: //depot/indesign_4.0/highprofile/source/sdksamples/printmemorystream/N2PSQLXferBytes.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: jbond $
//  
//  $DateTime: 2005/07/08 10:44:00 $
//  
//  $Revision: #2 $
//  
//  $Change: 350974 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PSQLXferBytes_h__
#define __N2PSQLXferBytes_h__

// Interface/General includes:
#include "IXferBytes.h" /* This is NOT an boss interface. */


/** Implements IXferBytes over a memory buffer. 
 * 	We associate this with a stream (IPMStream) by calling
 * 	at some point IPMStream::SetXferBytes
 * 	@ingroup printmemorystream
*/
class  N2PSQLXferBytes : public IXferBytes 
{
public:
	/** Constructor
	*/
	N2PSQLXferBytes(void);

	/** Destructor
	*/
	virtual ~N2PSQLXferBytes(void);

	//--- The IXferBytes interface we must implement  ---

	/** See IXferBytes::Read
	*/
	virtual uint32 Read(void* buffer, uint32 num);

	/** See IXferBytes::Write
	*/
	virtual uint32 Write(void* buffer, uint32 num);

	/** See IXferBytes::Seek
	*/
	virtual uint32 Seek(int32 numberOfBytes, SeekFromWhere fromHere);

	/** See IXferBytes::Flush
	*/
	virtual void Flush(void);

	/** See IXferBytes::GetStreamState
	*/
	virtual StreamState GetStreamState(void);

	/** See IXferBytes::SetEndOfStream
	*/
	virtual void SetEndOfStream(void);

protected:
	const uint8*  getBufferPtr(void) const;
	void resizeBuffer(uint32 newSize);
	void dumpState(void) const;

private:

	StreamState fStreamState;
	uint32 fCountBytesStored;
	uint32 fAbsolutePositionInBuffer;
	uint8* fMyBuffer;
	uint32 fCurrentMaxBuf;

	enum
	{
		ePrtMemInitialMaxBuf = 1024*1024
	};
};

#endif // __N2PSQLXferBytes_h__

// End, N2PSQLXferBytes.h.