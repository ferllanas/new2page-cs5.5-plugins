//========================================================================================
//  DATE 10/09/08
//========================================================================================
#include "VCPluginHeaders.h"

#include "MenuDef.fh"
#include "ActionDef.fh"
#include "ActionDefs.h"
#include "AdobeMenuPositions.h"
#include "LocaleIndex.h"
#include "PMLocaleIds.h"
#include "StringTable.fh"
#include "ObjectModelTypes.fh"
#include "ShuksanID.h"
#include "ActionID.h"
#include "WorkspaceID.h"
#include "WidgetID.h"
#include "BuildNumber.h"
#include "PanelList.fh"
#include "Widgets.fh"
#include "InterfaceColorDefines.h"
#include "IControlViewDefs.h"
#include "SysControlIDs.h"

#include "N2PCalID.h"
//#include "..\..\Interlasa\N2PCalendaro\N2PCalID.h"
//#include "N2PCalDefines.h"

#ifdef __ODFRC__

//========================================================================================
// TYPES
//========================================================================================

type N2PCalDialogWidget(kViewRsrcType) : DialogBoss(ClassID = kN2PCalDialogBoss)
{
};
type TableCellEditBoxWidget (kViewRsrcType) : TextEditBoxWidget (ClassID = kN2PCalEditBoxEventHandlerImpl) {};

type TableCellWidget(kViewRsrcType)		 : Widget (ClassID = kN2PCalTableCellWidgetBoss )
{	
	CControlView;
};
type ICalRollOverIconButtonWidget (kViewRsrcType) : RollOverIconButtonWidget(ClassID = kICalRollOverIconButtonObserverBoss) 
{
};


//================================================================================
//MENU DEFINITION
//================================================================================

resource MenuDef (kSDKDefMenuResourceID)
{
	{
		// The About Plug-ins sub-menu item for this plug-in. TODA ESTA PARTE ESTABA COMENTADA
	/*	kN2PCalAboutActionID,			// ActionID (kInvalidActionID for positional entries)
		kN2PCalAboutMenuPath,			// Menu Path.
		kSDKDefAlphabeticPosition,			// Menu Position.
		kSDKDefIsNotDynamicMenuFlag,		// kSDKDefIsNotDynamicMenuFlag or kSDKDefIsDynamicMenuFlag


		// Separator for the popup menu on the panel
		kN2PCalSeparator1ActionID,
		kN2PCalInternalPopupMenuNameKey kSDKDefDelimiterAndSeparatorPath,	// :- to mark an item as a separator.
		kN2PCalSeparator1MenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,

		// About this plugin submenu for the popup menu on the panel
		kN2PCalPopupAboutThisActionID,
		kN2PCalTargetMenuPath
		kN2PCalAboutThisMenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,


	// The Plug-ins menu sub-menu items for this plug-in.
		kN2PCalDialogActionID,
		kN2PCalPluginsMenuPath,
		kN2PCalDialogMenuItemPosition,
		kSDKDefIsNotDynamicMenuFlag,*/
	}
};


//================================================================================
// ACTION DEFINITION
//================================================================================

resource ActionDef (kSDKDefActionResourceID)
{
	{
	    //TODA ESTA PARTE ESTABA COMENTADA
		/*kN2PCalActionComponentBoss, 	// ClassID of boss class that implements the ActionID.
		kN2PCalAboutActionID,	        // ActionID.
		kN2PCalAboutMenuKey,	        // Sub-menu string.
		kOtherActionArea,				// Area name (see ActionDefs.h).
		kNormalAction,					// Type of action (see ActionDefs.h).
		kDisableIfLowMem,				// Enabling type (see ActionDefs.h).
		kInvalidInterfaceID,			// Selection InterfaceID this action cares about or kInvalidInterfaceID.
		kSDKDefInvisibleInKBSCEditorFlag, // kSDKDefVisibleInKBSCEditorFlag or kSDKDefInvisibleInKBSCEditorFlag.


		kN2PCalActionComponentBoss,
		kN2PCalPopupAboutThisActionID,
		kSDKDefAboutThisPlugInMenuKey,	// Key to the name of this action
		kOtherActionArea,
		kNormalAction,
		kDisableIfLowMem,
		kInvalidInterfaceID,
		kSDKDefInvisibleInKBSCEditorFlag,


		kN2PCalActionComponentBoss,
		kN2PCalDialogActionID,		
		kN2PCalDialogMenuItemKey,		
		kOtherActionArea,			
		kNormalAction,				
		kDisableIfLowMem,	
		kInvalidInterfaceID,		
		kSDKDefVisibleInKBSCEditorFlag,*/
	}
};


//========================================================================================
// RESOURCES Locale Index
//========================================================================================

/*-----------------------Dialog LocaleIndex----------------------------------*/
resource LocaleIndex (kSDKDefDialogResourceID)
{
   kViewRsrcType,
	{
		kWildFS,	k_Wild, kSDKDefDialogResourceID + index_enUS
	}
};

#endif // __ODFRC__

#include "N2PCalViews_enUS.fr"

//End, N2PCalViews.fr.

