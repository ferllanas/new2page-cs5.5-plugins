/*
//	File:	N2PSQLPreferDlgController.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"
#include "N2PSQLListBoxHelper.h"
#include "IDocument.h"
#include "ILayoutUtils.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "IApplication.h"
#include "IPDFExptStyleListMgr.h"
#include "IWorkspace.h"



#include "ErrorUtils.h"
#include "ILayoutUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"
//#include "DocumentID.h"
#include "OpenPlaceID.h"


// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "SDKUtilities.h"
#include "SDKFileHelper.h"
#include "PlatformFileSystemIterator.h"
#include "IPalettePanelUtils.h"

//#include <direct.h>
#include <stdlib.h>
#include <stdio.h>




// Project includes:
#include "N2PsqlID.h"
#include "CAlert.h"
#include "UpdateStorysAndDBUtilis.h"
#include "N2PSQLUtilities.h"

/** N2PSQLPreferDlgController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class N2PSQLPreferDlgController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PSQLPreferDlgController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~N2PSQLPreferDlgController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);

private:

		void LlenarComboPreferenciasOnDlg(PMString nameConnection);
		
		void LlenarComboPreferenciasOnPanel();
		
		void LlenarPDFComboBox(PMString PresetToPDFeditorial,PMString PresetToPDFeditorialWEB);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(N2PSQLPreferDlgController, kN2PSQLPreferenciasDialogControllerImpl)

/* ApplyDialogFields
*/
void N2PSQLPreferDlgController::InitializeDialogFields(IActiveContext* context) 
{
	
	
	InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
	(
		kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
		IUpdateStorysAndDBUtils::kDefaultIID
	)));
	
	
	
	if(UpdateStorys!=nil)
	{
		PreferencesConnection PrefConections;
	
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
		
		//PrefConections.IPServerConnection.SetTranslatable(kFalse);
		//this->SetTextControlData(kN2PSQLIPServerOfDBEditWidgetID,PrefConections.IPServerConnection);
		
		PrefConections.NameConnection.SetTranslatable(kFalse);
		this->SetTextControlData(kN2PSQLNameConectionDBWidgetID,"SSSSS"/*PrefConections.NameConnection*/);
		
		PrefConections.DSNNameConnection.SetTranslatable(kFalse);
		this->SetTextControlData(kN2PSQLDSNNameEditWidgetID,PrefConections.DSNNameConnection);
		
		PrefConections.NameUserDBConnection.SetTranslatable(kFalse);
		this->SetTextControlData(kN2PSQLUsuarioOfDBEditWidgetID,PrefConections.NameUserDBConnection);
		
		
		this->SetTextControlData(kN2PSQLPassswordOfDBWidgetID,"");
		
		PrefConections.PathOfServerFile.SetTranslatable(kFalse);
		this->SetTextControlData(kN2PSQLPathOfServerFileEditWidgetID,PrefConections.PathOfServerFile);
		this->SetTextControlData(kN2PSQLPathOfServerFileImagenesEditWidgetID,PrefConections.PathOfServerFileImages);
		this->SetTextControlData(kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID,PrefConections.ImagCarpInServFImages);		
		this->SetTextControlData(kN2PSQLURLWebServicesEditWidgetID,PrefConections.URLWebServices);

		
		this->SetTextControlData(kN2PSQLRutaDServidorRespNotasEditWidgetID,PrefConections.RutaDServidorRespNotas);
		
		this->SetTextValue(KN2PSQLTimeToCheckUpdateElemsWidgetID,PrefConections.TimeToCheckUpdateElements);
		this->SetTextValue(kN2PResizeHeightWidgetID,PrefConections.N2PResizeHeight);
		this->SetTextValue(kN2PResizeWidthWidgetID,PrefConections.N2PResizeWidth);
		
		if(PrefConections.LlenarTablaWEB==1)
			this->SetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxExportNotasATablaWEBWidgetID,ITriStateControlData::kUnselected);
		
		this->SetTextControlData(KN2PSQLEstatusParaExportarATWEBWidgetID,PrefConections.EstadoParaExportarATWEB);
		
		this->LlenarComboPreferenciasOnDlg(PrefConections.NameConnection);
		
		
		if(PrefConections.EsCampoGuiaObligatorio==1)
			this->SetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,ITriStateControlData::kUnselected);
		
		if(PrefConections.N2PResizePreview)
		{
			this->SetTriStateControlData(kN2PResizePreviewViewWidgetID,ITriStateControlData::kSelected);
		}
		else
			this->SetTriStateControlData(kN2PResizePreviewViewWidgetID,ITriStateControlData::kUnselected);
		
		if(PrefConections.N2PUtilizarGuias)
		{
			this->SetTriStateControlData(kN2PUsarGuiasWidgetID,ITriStateControlData::kSelected);
		}
		else
			this->SetTriStateControlData(kN2PUsarGuiasWidgetID,ITriStateControlData::kUnselected);

		
		if(PrefConections.N2PHmteSendToHmca==1)
		{
			//CAlert::InformationAlert("D select");
			this->SetTriStateControlData(kN2PHmteSendToHmcaWidgetID,ITriStateControlData::kSelected);
		}
		else
		{
			//CAlert::InformationAlert("D kUnselected");
			this->SetTriStateControlData(kN2PHmteSendToHmcaWidgetID,ITriStateControlData::kUnselected);
		}
		
		if(PrefConections.ExportPDFPresetsEditWEB==1)
			this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,ITriStateControlData::kUnselected);
		
		
		if(PrefConections.ExportPDFPresets==1)
			this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxIsExportPDFPresetsWidgetID,ITriStateControlData::kUnselected);
		
		if(PrefConections.EnviaAWEBEnAltasPieFoto==1)
			this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,ITriStateControlData::kUnselected);
		
		
		if(PrefConections.EnviaAWEBSinRetornosTitulo==1)
			this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,ITriStateControlData::kUnselected);
		
		
		if(PrefConections.EsEnviarNotasHijasAWEB==1)
			this->SetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,ITriStateControlData::kUnselected);
		

		if(PrefConections.CreateBackUpPagina==1)
			this->SetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,ITriStateControlData::kUnselected);
		
		if(PrefConections.ExportarPaginaComoPDF==1)
			this->SetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,ITriStateControlData::kSelected);
		else
			this->SetTriStateControlData(kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,ITriStateControlData::kUnselected);
		
		this->SetTextControlData(KN2PSQLNombreEstatusParaNotasCompletasWidgetID,PrefConections.NombreEstatusParaNotasCompletas);
		this->SetTextControlData(KN2PSQLRutaDePaginaComoPDFWidgetID,PrefConections.RutaDePaginaComoPDF);
		this->SetTextControlData(KN2PSQLRutaDePaginaComoPDFWEBWidgetID,PrefConections.RutaDePaginaComoPDFWEB);
		this->SetTextControlData(kN2PSQLnameParaStyleCreditoWidgetID,PrefConections.nameParaStyleCredito);
		
		this->SetTextControlData(kN2PSQLnameParaStyleContenidoWidgetID, PrefConections.nameParaStyleContenido);
		this->SetTextControlData(kN2PSQLnameParaStyleBalazoWidgetID, PrefConections.nameParaStyleBalazo);
		this->SetTextControlData(kN2PSQLnameParaStyleTituloWidgetID, PrefConections.nameParaStyleTitulo);
		this->SetTextControlData(kN2PSQLnameParaStylePieFotoWidgetID, PrefConections.nameParaStylePieFoto);
		
		
		this->SetTextControlData(kN2PHmeDSNNameWidgetID,PrefConections.N2PHmeDSNName);
		this->SetTextControlData(kN2PHmeDSNUserLoginWidgetID,PrefConections.N2PHmeDSNUserLogin);
		this->SetTextControlData(kN2PHmeDSNPwdLoginWidgetID,PrefConections.N2PHmeDSNPwdLogin);
		this->SetTextControlData(kN2PHmeFTPServerWidgetID,PrefConections.N2PHmeFTPServer);
		this->SetTextControlData(kN2PHmeFTPUserWidgetID,PrefConections.N2PHmeFTPUser);
		this->SetTextControlData(kN2PHmeFTPPwdWidgetID,PrefConections.N2PHmeFTPPwd);
		this->SetTextControlData(kN2PHmeFTPFolderWidgetID,PrefConections.N2PHmeFTPFolder);
		
		if(PrefConections.N2PUseFototeca==1)
		{
			//CAlert::InformationAlert("D select");
			this->SetTriStateControlData(kN2PUseFototecaWidgetID,ITriStateControlData::kSelected);
		}
		else
		{
			//CAlert::InformationAlert("D kUnselected");
			this->SetTriStateControlData(kN2PUseFototecaWidgetID,ITriStateControlData::kUnselected);
		}
		
		this->SetTextControlData(kN2PPhotoServerFolderINWidgetID,PrefConections.N2PPhotoServerFolderIN);
		this->SetTextControlData(kPhotoServerFolderOUTWidgetID,PrefConections.PhotoServerFolderOUT);
	
		
		if(PrefConections.N2PCkBoxSendWordPress==1)
		{
			//CAlert::InformationAlert("D select");
			this->SetTriStateControlData(kN2PCkBoxSendWordPressWidgetID,ITriStateControlData::kSelected);
		}
		else
		{
			//CAlert::InformationAlert("D kUnselected");
			this->SetTriStateControlData(kN2PCkBoxSendWordPressWidgetID,ITriStateControlData::kUnselected);
		}
		
		this->SetTextControlData(kN2PLinkWebServicesWordPressWidgetID,PrefConections.N2PLinkWebServicesWordPress);
				
		
		LlenarPDFComboBox(PrefConections.PresetTOPDFEditorial,PrefConections.PresetTOPDFEditorialWEB);	
	}
	
	
}

/* ValidateDialogFields
*/
WidgetID N2PSQLPreferDlgController::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID result = kNoInvalidWidgets;

	// Put code to validate widget values here.

	return result;
}

/* ApplyDialogFields
*/
void N2PSQLPreferDlgController::ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId) 
{
	// Replace with code that gathers widget values and applies them.
	//this->LlenarComboPreferenciasOnPanel();
	
	PreferencesConnection PrefConections;
	InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
	(
		kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
		IUpdateStorysAndDBUtils::kDefaultIID
	)));
	
	
	
	if(UpdateStorys!=nil)
	{
	
	
		UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kTrue);
	}
	SystemBeep();  
}


void N2PSQLPreferDlgController::LlenarComboPreferenciasOnDlg(PMString NomPref)
{
	PMString Archivo;
	
	PMString Arc;
	int posicion;
		do
		{
			
			InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
			
			
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
				break;
			}

			
			
			InterfacePtr<IControlView>		CVComboBoxSelecPrefer(panel->FindWidget(kN2PsqlDataBaseComboWidgetID), UseDefaultIID() );
			if(CVComboBoxSelecPrefer==nil)
			{
				break;
			}
			
			
			InterfacePtr<IStringListControlData> dropListData(CVComboBoxSelecPrefer, UseDefaultIID());
			if (dropListData == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			///borrado de la lista al inicializar el combo
			
			dropListData->Clear(kFalse, kFalse);//lIMPIA LA LISTA ACTUAL
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////COMENTARIO ES MUY EXTRAÑO QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		    PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
		   
			
		 
			//remoeve los ultimos dos puntos
		    PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
		
  			
  			if(PathPMS.IsEmpty() == kTrue)
			{
				break;
			}
			SDKFileHelper rootFileHelper(PathPMS);
			IDFile rootSysFile = rootFileHelper.GetIDFile();
			PlatformFileSystemIterator iter;
		
			iter.SetStartingPath(rootSysFile);
					
			IDFile sysFile;
			bool16 hasNext= iter.FindFirstFile(sysFile,"//*.*");
			while(hasNext)
			{
				SDKFileHelper fileHelper(sysFile);
				PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
				
				if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
				{
					truncP=N2PSQLUtilities::TruncateExtencion(truncP);
					dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
				}
				hasNext= iter.FindNextFile(sysFile);
			}
			
			InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( CVComboBoxSelecPrefer, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				
				break;
			}
			
			if(dropListData->Length()>0)
			{
				posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
				if(posicion!=-1)
					dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
				posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
				if(posicion!=-1)
					dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			
			
				posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
			
				if(posicion<0)
				{
					IDDLDrComboBoxSelecPrefer->Select(0);
				}
				else
				{
					IDDLDrComboBoxSelecPrefer->Select(posicion);
				}
			}
			
		}while(false);}


/**
	Llena el combo por medio de las preferencias actuales
*/
void N2PSQLPreferDlgController::LlenarComboPreferenciasOnPanel()
{

	
	int32 posicion=-1;
	PMString NomPref="";
	do
	{
	
		InterfacePtr<IPanelControlData>	panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID));	
		// Don't assert, fail silently, the tree view panel may be closed.
		if(panelData == nil)
		{
			ASSERT_FAIL("LlenarComboPreferenciasOnPanel panelData");
			break;
		}
		IControlView* ComboCView = panelData->FindWidget(kN2PsqlDataBaseComboWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			ASSERT_FAIL("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		

		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("No pudo Obtener dropListData*");
			
			break;
		}
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				ASSERT_FAIL("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
				break;
			}
		
		int32 IndexSelected=0;
		if(dropListData->Length()>0)
		{
			
			
			IndexSelected=IDDLDrComboBoxSelecPrefer->GetSelected();
			
			
			
			//CAlert::InformationAlert("5.1");
			if(IndexSelected>=0)
			{
				
				
				PMString StringSelected=dropListData->GetString(IndexSelected);
				
				
				dropListData->Clear();//lIMPIA LA LISTA ACTUAL
				IndexSelected=-1;
				//Llena la lista
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		    	PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
		   
		    
		   		#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
		   		#endif
  			
  				if(PathPMS.IsEmpty() == kTrue)
				{
					break;
				}
				SDKFileHelper rootFileHelper(PathPMS);
				IDFile rootSysFile = rootFileHelper.GetIDFile();
				PlatformFileSystemIterator iter;
				/*if(!iter.IsDirectory(rootSysFile))
				{
					
					break;
				}*/
			

				
				iter.SetStartingPath(rootSysFile);
					
				PMString TypeFile="";
				#ifdef WINDOWS 
					TypeFile="\\*.pfa";
				#endif
				
				#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		TypeFile="*.pfa";
		   		#endif

				IDFile sysFile;
				bool16 hasNext= iter.FindFirstFile(sysFile,TypeFile);
				
				while(hasNext)
				{
					SDKFileHelper fileHelper(sysFile);
					PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
					
					if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
					{
						truncP=N2PSQLUtilities::TruncateExtencion(truncP);
						dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
					}
					hasNext= iter.FindNextFile(sysFile);
				}
			
				
			
				if(dropListData->Length()>0)
				{
					posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
					posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			
			
					posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
			
					if(posicion<0)
					{
						IDDLDrComboBoxSelecPrefer->Select(0);
					}
					else
					{
						IDDLDrComboBoxSelecPrefer->Select(posicion);
					}
				}
				////////////////////////////
				////////////////////////////
				
				if(dropListData->Length()>0)//si se lleno con almenos un path el combo
				{
					
					//Seleccion si existe del ultimo path seleccionado
					if(dropListData->Length()>0)//si hay elementos en la lista
					{
						//IndexSelected=dropListData->GetIndex(StringSelected);
						//si exite en la lista el anterior path
						if(IndexSelected>=0)
						{	
							
							IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
						}
						else
						{	//si existe alguno que contenga la leyenda default
							
							IndexSelected=dropListData->GetIndexOfPartialString("Default",kFalse);
							if(IndexSelected>=0)
							{	
								//mns="Si existe alguno que contenga la leyenda default";
								
								IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
							}
							else
							{//si no selecciona el primer elemento de la lista
								
								IDDLDrComboBoxSelecPrefer->Select(0);
							}
						}
					}
				}
			}
			else
			{
				//si no existe alguno seleccionado se limpia la lista se vuelve a llenar busca el que contenga el termino default
				IndexSelected=-1;
				dropListData->Clear();//lIMPIA LA LISTA ACTUAL
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		    	PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
		   
		    
		   		#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
		   		#endif
  			
  				if(PathPMS.IsEmpty() == kTrue)
				{
					break;
				}
				SDKFileHelper rootFileHelper(PathPMS);
				IDFile rootSysFile = rootFileHelper.GetIDFile();
				PlatformFileSystemIterator iter;
				/*if(!iter.IsDirectory(rootSysFile))
				{
					CAlert::InformationAlert("SAlio");
					break;
				}*/
			

				
				iter.SetStartingPath(rootSysFile);
					
				PMString TypeFile="";
				#ifdef WINDOWS 
					TypeFile="\\*.pfa";
				#endif
				
				#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		TypeFile="*.pfa";
		   		#endif

				IDFile sysFile;
				bool16 hasNext= iter.FindFirstFile(sysFile,TypeFile);
				while(hasNext)
				{
					SDKFileHelper fileHelper(sysFile);
					PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
					
					if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
					{
						truncP=N2PSQLUtilities::TruncateExtencion(truncP);
						dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
					}
					hasNext= iter.FindNextFile(sysFile);
				}
			
				
			
				if(dropListData->Length()>0)
				{
					posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
					posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			
			
					posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
			
					if(posicion<0)
					{
						IDDLDrComboBoxSelecPrefer->Select(0);
					}
					else
					{
						IDDLDrComboBoxSelecPrefer->Select(posicion);
					}
				}
				////////////////////////////
				////////////////////////////
				
				if(dropListData->Length()>0)//si se lleno con almenos un path el combo
				{
					if(IndexSelected>=0)
						IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
					else
						IDDLDrComboBoxSelecPrefer->Select(0);
				}
			}
		}
		else
		{
		
			//no existen Path en combo llenado del combo
			IndexSelected=-1;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
				////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		    	PMString PathPMS = N2PSQLUtilities::CrearFolderPreferencias();
		   
		    
		   		#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		PathPMS.Remove(PathPMS.NumUTF16TextChars()-1,1);
		   		#endif
  			
  				if(PathPMS.IsEmpty() == kTrue)
				{
					break;
				}
				SDKFileHelper rootFileHelper(PathPMS);
				IDFile rootSysFile = rootFileHelper.GetIDFile();
				PlatformFileSystemIterator iter;
				/*if(!iter.IsDirectory(rootSysFile))
				{
					
					break;
				}*/
			

				
				iter.SetStartingPath(rootSysFile);
					
				PMString TypeFile="";
				#ifdef WINDOWS 
					TypeFile="\\*.pfa";
				#endif
				
				#ifdef MACINTOSH
					//remoeve los ultimos dos puntos
		    		TypeFile="*.pfa";
		   		#endif

				IDFile sysFile;
				bool16 hasNext= iter.FindFirstFile(sysFile,TypeFile);
				while(hasNext)
				{
					SDKFileHelper fileHelper(sysFile);
					PMString truncP = N2PSQLUtilities::TruncatePath(fileHelper.GetPath());
				
					if(N2PSQLUtilities::validPath(truncP) && truncP.Contains(".pfa") && truncP.NumUTF16TextChars()>4)
					{
						truncP=N2PSQLUtilities::TruncateExtencion(truncP);
						dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
					}
					hasNext= iter.FindNextFile(sysFile);
				}
			
				
			
				if(dropListData->Length()>0)
				{
					posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
					posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
					if(posicion!=-1)
						dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			
			
					posicion = dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo
			
					if(posicion<0)
					{
						IDDLDrComboBoxSelecPrefer->Select(0);
					}
					else
					{
						IDDLDrComboBoxSelecPrefer->Select(posicion);
					}
				}
				////////////////////////////
				////////////////////////////
			
			
			//si existe alguno con la leyenda default
			if(dropListData->Length()>0)//si se lleno con almenos un path el combo
			{
				if(IndexSelected>=0)
					IDDLDrComboBoxSelecPrefer->Select(IndexSelected);
				else
					IDDLDrComboBoxSelecPrefer->Select(0);
			}
		}
	}while(false);
}



void N2PSQLPreferDlgController::LlenarPDFComboBox(PMString PresetToPDFeditorial,PMString PresetToPDFeditorialWEB)
{
	
	do
	{
		
	
	
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
			
			
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panelData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panelData==nil)
			{
				break;
			}	
		
		IControlView* ComboCView = panelData->FindWidget(kN2PsqlExportPDFPresetsDropDownWidgetID);
		ASSERT(ComboCView);
		if(ComboCView == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCView");
			break;
		}
		
		
		IControlView* ComboCViewEditWEB = panelData->FindWidget(kN2PsqlExportPDFPresetsEditWEBDropDownWidgetID);
		ASSERT(ComboCViewEditWEB);
		if(ComboCViewEditWEB == nil)
		{
			CAlert::InformationAlert("LlenarComboPreferenciasOnPanel ComboCViewEditWEB");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListDataWEB(ComboCViewEditWEB,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataWEB == nil)
		{
			CAlert::InformationAlert("No pudo Obtener dropListDataWEB*");
	
			break;
		}
		
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPreferWEB( ComboCViewEditWEB, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPreferWEB==nil)
		{
			CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPreferWEB*");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			CAlert::InformationAlert("No pudo Obtener dropListData*");
			
			break;
		}
		///borrado de la lista al inicializar el combo
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			CAlert::InformationAlert("No pudo Obtener IDDLDrComboBoxSelecPrefer*");
			break;
		}
		
		dropListDataWEB->Clear(kFalse, kFalse);
		dropListData->Clear(kFalse, kFalse);
		
		InterfacePtr<IWorkspace> piWorkspace(GetExecutionContextSession()->QueryWorkspace());
		if(piWorkspace==nil)
		{
			CAlert::InformationAlert("No pudo Obtener piWorkspace*");
			break;
		}
			
		InterfacePtr<IPDFExptStyleListMgr> piPDFExptStyleListMgr((IPDFExptStyleListMgr *) piWorkspace->QueryInterface(IID_IPDFEXPORTSTYLELISTMGR));
		if(piPDFExptStyleListMgr==nil)
		{
			CAlert::InformationAlert("No pudo Obtener piPDFExptStyleListMgr*");
			break;
		}
		
		int32 indexFromEditorial=0;
		int32 indexFromEditorialWEB=0;
		for(int32 i=0;i<piPDFExptStyleListMgr->GetNumStyles(); i++)
		{
			PMString pName="";
			ErrorCode er=piPDFExptStyleListMgr->GetNthStyleName (i, &pName);
		 	if(er==kSuccess)
		 	{
		 		dropListData->AddString(pName, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
				dropListDataWEB->AddString(pName, IStringListControlData::kEnd, kFalse, kFalse);
		 	}
			
			if(pName==PresetToPDFeditorial)
			{
				indexFromEditorial=i;
			}
			
			if(pName==PresetToPDFeditorialWEB)
			{
				indexFromEditorialWEB=i;
			}
		}
		
		if(dropListData->Length()>0)//si se lleno con almenos un path el combo
		{
			if(indexFromEditorial>=0)
				IDDLDrComboBoxSelecPrefer->Select(indexFromEditorial);
			else
				IDDLDrComboBoxSelecPrefer->Select(0);
		}
		
		
		
		if(dropListDataWEB->Length()>0)//si se lleno con almenos un path el combo
		{
			if(indexFromEditorial>=0)
				IDDLDrComboBoxSelecPreferWEB->Select(indexFromEditorialWEB);
			else
				IDDLDrComboBoxSelecPreferWEB->Select(0);
		}
	}while(false);
	
}