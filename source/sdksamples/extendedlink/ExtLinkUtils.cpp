//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkUtils.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interfaces:
#include "ITextModel.h"
#include "XMLReference.h"
#include "ILinkFacade.h"
#include "ILinkManager.h"
#include "INewUIDCmdData.h"
#include "IPMUnknownData.h"
#include "IPersistUIDData.h"
#include "IIDXMLElement.h"
#include "IXMLReferenceData.h"
#include "ITextModelCmds.h"
#include "IWorkspace.h"
#include "IXMLTagCommands.h"
#include "IXMLElementCommands.h"
#include "IXMLAttributeCommands.h"
#include "IActiveContext.h"
#include "IBoolData.h"
#include "IDocument.h"

// General:
#include "CmdUtils.h"
#include "URI.h"

// Project:
#include "ExtLinkUtils.h"
#include "IExtLinkOptions.h"
#include "ExtLinkConst.h"
#include "ExtLinkID.h"

/*
// ReplaceText
// Replace the given range of text with the WideString supplied.
*/
ErrorCode ExtLinkUtils::ReplaceText(ITextModel* iTextModel, const TextIndex& textIndex, int32 length, const boost::shared_ptr<WideString>& wideVal)
{

	// Never ever try to replace the last character in a story.
	// It is always a carriage return so leave it well alone.
	if (textIndex + length == iTextModel->TotalLength())
		length--;

	ErrorCode err = kFailure;

	// Create a command to replace the string
	// Ownership of the buffer wideVal is passed to the command. It 
	// will delete it when it is no longer required
    InterfacePtr<ITextModelCmds>textModelCmds(iTextModel,UseDefaultIID());
	InterfacePtr<ICommand> iCmd(textModelCmds->ReplaceCmd(textIndex, length, wideVal, kFalse));
	ASSERT_MSG( iCmd != nil, "cmd is invalid.");
	err = (iCmd != nil) ? kSuccess : kFailure;
	if (err == kSuccess)
		err = CmdUtils::ProcessCommand(iCmd);

	return err;
}



// Acquire XMLExtLink XML Tags. Create new tag if they have not been created before.
UIDRef ExtLinkUtils::AcquireOneTag(IXMLTagList* tagList,const WideString& tagName )
{
	UIDRef retval;
	WideString tagNameWide(tagName);
	do {
		if(!tagList) 
			break;
		
		UID existingTagUID  = tagList->GetTag(tagNameWide);
		if(existingTagUID == kInvalidUID) 
		{
			UID createdTagUID = kInvalidUID;
			ErrorCode err = Utils<IXMLTagCommands>()->CreateTag (::GetUIDRef(tagList), 
															tagNameWide,
															kInvalidUID, 
															&createdTagUID);

			ASSERT(err == kSuccess);
			ASSERT(createdTagUID != kInvalidUID);
			retval = UIDRef(::GetDataBase(tagList), createdTagUID);

		} 
		else {
			retval = UIDRef(::GetDataBase(tagList), existingTagUID);
		}

	} while(kFalse);

	return retval;
}

ErrorCode ExtLinkUtils::TagTextWithAttributeValue(UID tagUID, 
									UIDRef story, TextIndex startIndex, TextIndex endIndex, 
									const WideString attrName, const WideString attrValue, const URI &uri)
{
	ErrorCode status = kFailure;

	do
	{		
		XMLReference createdElement;
		status = Utils<IXMLElementCommands>()->CreateElement(tagUID, story, startIndex, endIndex, kInvalidXMLReference, &createdElement);
		if(status != kSuccess) 
			break;

		Utils<IXMLAttributeCommands> attributeFacade;
		if(!attributeFacade) 
			break;

		status = attributeFacade->CreateAttribute(createdElement,(WideString)attrName, (WideString)attrValue);
		if(status != kSuccess) 
			break;

		IDataBase * dataBase = createdElement.GetDataBase();
		InterfacePtr<IIDXMLElement>xmlElement( createdElement.Instantiate() );
		InterfacePtr<IPersistUIDData> xmlObjectRef(xmlElement, IID_ILINKOBJECTPERSISTUIDDATA);
		// Create a reference object kExtLinkODBCXMLLinkObjectReferenceBoss, which is what the links mgr sees (xml element not being uid-based)
		InterfacePtr<ICommand> newReferenceCmd(CmdUtils::CreateCommand(kNewUIDCmdBoss));
		InterfacePtr<INewUIDCmdData> newReferenceData(newReferenceCmd, IID_INEWUIDCMDDATA);
		newReferenceData->Set(dataBase, kExtLinkODBCXMLLinkObjectReferenceBoss);
		status = CmdUtils::ProcessCommand(newReferenceCmd);
		if (status != kSuccess)
			break;

		// Make the reference boss point to xmlElement.
		UIDRef reference =  newReferenceCmd->GetItemList()->GetRef(0);
		InterfacePtr<IXMLReferenceData> referenceData(reference, IID_IXMLREFERENCEDATA);
		referenceData->Set(createdElement);

		// And the xmlelement point to it
		xmlObjectRef->SetUID(reference.GetUID());

		// Add new resource and link
		UID resourceUID = kInvalidUID;
		Utils<Facade::ILinkFacade> iFacade;
		status = iFacade->CreateResource(dataBase, uri, kIDLinkClientID, PMString(), resourceUID);
		ASSERT(resourceUID != kInvalidUID);
		if (status != kSuccess)
			break;
		UID linkUID = kInvalidUID;
		status = iFacade->CreateLink(dataBase, kBidirectionalLinkBoss, kIDLinkClientID, reference.GetUID(), resourceUID, false, linkUID);
		ASSERT(linkUID != kInvalidUID);
		if (status != kSuccess)
			break;
		
		InterfacePtr<ILink> newLink(dataBase, linkUID, UseDefaultIID());
		if (newLink)
		{
			newLink->SetCanPackage(kFalse);
		}
		
		// Set the format type
		PMString formatName( "XML" );
        InterfacePtr<ILinkResource> iLinkResource(dataBase, resourceUID,UseDefaultIID());
        iLinkResource->SetFormatType(formatName);
	} while (false);

	return status;
}


PMString ExtLinkUtils::GetSelectedFolderPath()
{
	PMString retval="";
	do
	{
		InterfacePtr<IWorkspace> sessionWorkspace(GetExecutionContextSession()->QueryWorkspace());
		ASSERT(sessionWorkspace);
		if(sessionWorkspace == nil)
		{
			break;
		}
		InterfacePtr<IExtLinkOptions> extLinkOptions(sessionWorkspace, UseDefaultIID());
		ASSERT(extLinkOptions);
		if(!extLinkOptions)
		{
			break;
		}
		ExtLinkOptionsData option = extLinkOptions->GetNthOption(0);
		retval = option.GetImageFolderPath();
		
	} while(kFalse);
	return retval;
}


PMString ExtLinkUtils::GetSelectedDataSource()
{
	PMString retval="";
	do
	{
		InterfacePtr<IWorkspace> sessionWorkspace(GetExecutionContextSession()->QueryWorkspace());
		ASSERT(sessionWorkspace);
		if(sessionWorkspace == nil)
		{
			break;
		}
		InterfacePtr<IExtLinkOptions> extLinkOptions(sessionWorkspace, UseDefaultIID());
		ASSERT(extLinkOptions);
		if(!extLinkOptions)
		{
			break;
		}
		ExtLinkOptionsData option = extLinkOptions->GetNthOption(0);
		retval = option.GetDataSourceName();
		
	} while(kFalse);
	return retval;
}


PMString ExtLinkUtils::GetSelectedTableName()
{
	PMString retval="";
	do
	{
		InterfacePtr<IWorkspace> sessionWorkspace(GetExecutionContextSession()->QueryWorkspace());
		ASSERT(sessionWorkspace);
		if(sessionWorkspace == nil)
		{
			break;
		}
		InterfacePtr<IExtLinkOptions> extLinkOptions(sessionWorkspace, UseDefaultIID());
		ASSERT(extLinkOptions);
		if(!extLinkOptions)
		{
			break;
		}
		ExtLinkOptionsData option = extLinkOptions->GetNthOption(0);
		retval = option.GetDBTableName();
		
	} while(kFalse);
	return retval;
}

bool16 ExtLinkUtils::IsDisplayExtLinksInLinksPanel()
{
	IActiveContext* context = GetExecutionContextSession()->GetActiveContext();
	if (context)
	{	
		IDocument* document = context->GetContextDocument();
		if(document)
		{
			InterfacePtr<IBoolData> showInUI(document->GetDocWorkSpace(), IID_ISHOWINLINKSUI);
			if(showInUI)
			{
				return showInUI->Get();
			}
		}
	}

	InterfacePtr<IWorkspace> sessionWorkspace(GetExecutionContextSession()->QueryWorkspace());
	ASSERT(sessionWorkspace);
	if(sessionWorkspace != nil)
	{
		InterfacePtr<IBoolData> showInUI(sessionWorkspace, IID_ISHOWINLINKSUI);
		if(showInUI)
		{
			return showInUI->Get();
		}
	}
	
	return kTrue;
}

bool16 ExtLinkUtils::IsAutoRefreshStatus()
{
	bool16 retval = kTrue;
	do
	{
		InterfacePtr<IWorkspace> sessionWorkspace(GetExecutionContextSession()->QueryWorkspace());
		ASSERT(sessionWorkspace);
		if(sessionWorkspace == nil)
		{
			break;
		}
		InterfacePtr<IExtLinkOptions> extLinkOptions(sessionWorkspace, UseDefaultIID());
		ASSERT(extLinkOptions);
		if(!extLinkOptions)
		{
			break;
		}
		
		retval = extLinkOptions->IsAutoRefreshStatus();
		
	} while(kFalse);
	
	return retval;
}

// End ExtLinkUtils.cpp

