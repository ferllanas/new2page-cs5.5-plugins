/*
 *  N2PDebugLog.h
 *  News2Page
 *
 *  Created by Fernando Llanas on 05/01/09.
 *  Copyright 2009 Interlasa.com. All rights reserved.
 *
 */

#ifndef __IN2PSQLDebugLog_h__
#define __IN2PSQLDebugLog_h__

#include"IPMUnknown.h"
#include"IDocument.h"

#include"N2PsqlID.h"



class IN2PSQLDebugLog:public IPMUnknown
{
	public:
		enum { kDefaultIID = IID_IN2PSQLDebugLogINTERFACE };
		
		virtual void IniciaDebug(const PMString &nombreArchivo="Prueba")=0;
		
		virtual void printmsg(const PMString& msn)=0;
		
		virtual void TerminaDebug()=0;
			
};


#endif // __IN2PSQLDebugLog_h__

