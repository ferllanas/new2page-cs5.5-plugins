//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActionManager.h"
#include "IActiveContext.h"
#include "IApplication.h"
#include "IDocument.h"

#include "ActionFilterHelper.h"
#include "CActionComponent.h"
#include "CAlert.h"
#include "LocaleSetting.h"
#include "IActionStateList.h" // not an interface!
//#include "UpdateManagerID.h" // for actionIDs and boss classes
#include "IActionFilter.h"
#include "IEnvironment.h"
#include "ILayoutUIUtils.h"
#include "IInCopyDocUtils.h"

// General includes:
#include "ActionDefs.h" // for kCustomEnabling
#include "ActionFilterHelper.h"
#include "CPMUnknown.h"
//#include "DocumentID.h" // for kDocumentComponentBoss
#include "ShuksanID.h" // for actionIDs and bossclasses
#include "PrintID.h" // for actionIDs and bossclasses
#include "ImportExportUIID.h" // for actionIDs and bossclasses
#include "Utils.h"
#include "InCopyFileActionsID.h"
// General includes:
#include "CActionComponent.h"
#include "CAlert.h"

// Project includes:
#include "N2P_MAFID.h"
#include "N2P_MAFActionFilter.h"



#ifdef MACINTOSH

	#include "N2PsqlID.h"
	#include "N2PInOutID.h"
	#include "N2PCheckInOutID.h"
	
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "ICheckInOutSuite.h"
#endif

#ifdef WINDOWS
	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"
	
	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	
	#include "..\N2PCheckInOut\N2PCheckInOutID.h"
	#include "..\N2PCheckInOut\ICheckInOutSuite.h"
#endif

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

	
	@ingroup n2p_menuactionfilter

*/
class N2P_MAFActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		N2P_MAFActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);
		
		/** Called when the action component is asked to update a list of actions to the proper state.
		This method is required to be overridden by subclasses only if there are action components with 
		kCustomEnabling specified.

		The list of actions is intially disabled and unselected, so it only has to be
		changed if this state is not correct. Note that action components are never asked to update
		actions that don't belong to them, or actions they have not specified kCustomEnabling for.
		
		@see CActionComponent::UpdateActionStates
	*/
	virtual void UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint = kInvalidMousePoint, IPMUnknown* widget = nil);


	private:
		/** Encapsulates functionality for the about menu item. */
		void DoAbout();
		
		/** Checks if action can/should be carried out.
	 * 	@param actionID (in) The ActionID to examine...
	 * 	@return kTrue if action can be carried out.
	 */
	bool16 CanDoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint = kInvalidMousePoint, IPMUnknown* widget = nil);

	/** Performs the actual action. The includes a "PreProcess()" step before
	 * 	calling the original action component
	 * 	@param actionID (in) The ActionID to perform.
	 */
	void DoPerformAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint = kInvalidMousePoint, IPMUnknown* widget = nil);

	/** Does a bit of preprocessing.
	 * 	Change this to suit your needs.
	 *	@param msg (in) A PMString to display.
	 */
	void PreProcess(const PMString& msg);
	

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2P_MAFActionComponent, kN2P_MAFActionComponentImpl)

/* N2P_MAFActionComponent Constructor
*/
N2P_MAFActionComponent::N2P_MAFActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void N2P_MAFActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{

	if (actionID.Get() == kN2P_MAFAboutActionID)
	{
		this->DoAbout();

		
	}
	else
	{
		if (this->CanDoAction(ac, actionID, mousePoint, widget))
		{
			this->DoPerformAction(ac, actionID, mousePoint, widget);
		}
	}
	/*switch (actionID.Get())
	{

		case kN2P_MAFAboutActionID:
		{
			this->DoAbout();
			break;
		}
					


		default:
		{
			break;
		}
	}*/
}

/* UpdateActionStates
*/
void N2P_MAFActionComponent::UpdateActionStates(IActiveContext* ac, IActionStateList* listToUpdate, GSysPoint mousePoint, IPMUnknown* widget)
{
	
	
	do
	{
		if (ac == nil || listToUpdate == nil)
		{
			break;
		}

		int32 len = listToUpdate->Length();

		TRACEFLOW(kN2P_MAFPluginName, "[CstAFltActionComponent::UpdateActionStates()]\n");

		for (int32 i = 0 ; i < len ; i++)
		{
			//ActionID myUpdateDesignActionID(94721);
			ActionID actionID = listToUpdate->GetNthAction(i);
			int16 actionState = listToUpdate->GetNthActionState(i);
			PMString actionName = listToUpdate->GetNthActionName(i);

			TRACEFLOW(kN2P_MAFPluginName, "[%d] BEFORE: ActionID = 0x%X ActionState = %d, ActionName = \"%s\"\n", 
					  i, actionID.Get(), actionState, actionName.GrabCString()); 

			// HERE'S THE CHANGE in the action behavior...!
			// Though it looks ridiculous just to always enable
			// START
			switch (actionID.Get())
			{
				
				case kCloseActionID:
				case kSaveAsActionID:
				case kSaveActionID:
				case kPrintActionID:
				case kSaveACopyActionID:
				case kExportActionID:
				case kQuitActionID:
				case 94721:
				
				// always enabled
				// kEnabledAction is in IActionStateList.h
				listToUpdate->SetNthActionState(i, kEnabledAction);
				break;

				default:
					break;

			}
			
			PMString mm="";
			mm.Append("ActionID=");
			mm.AppendNumber(actionID.Get());
			mm.Append("ActionState=");
			mm.AppendNumber(actionState);
			mm.Append("ActionName=");
			mm.Append( actionName.GrabCString());
			//CAlert::InformationAlert(mm);
			// END
			TRACEFLOW(kN2P_MAFPluginName, "[%d] AFTER:  ActionID = 0x%X ActionState = %d, ActionName = \"%s\"\n", 
					  i, actionID.Get(), actionState, actionName.GrabCString()); 
		}
	} while (kFalse);
}

/* CanDoAction
*/
bool16 N2P_MAFActionComponent::CanDoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	// 3rd PARTY TODO: change this to suit your needs
	return kTrue;
}


/* DoPerformAction
	* Find new actionID match from actionFilterHelpers and
	* Get the Orig IActionComponent so we can call it
*/
void N2P_MAFActionComponent::DoPerformAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{
	
	bool16 IsN2PUserLoged=kFalse;
	do
	{
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			break;
		}		
		
		PMString ID_Usuario="";	
		PMString Applicacion="";	
		ID_Usuario.AppendNumber(wrkSpcPrefs->GetID_Usuario());
		Applicacion.Append(wrkSpcPrefs->GetNameApp());
		
		
		if(Applicacion.NumUTF16TextChars()<1)
		{
			IsN2PUserLoged=kFalse;
		}
		else
		{
			IsN2PUserLoged=kTrue;
		}
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
						
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
		(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
		if(checkIn==nil)
			break;
	
		// local copy of action component
		IActionComponent* origActionComponent = nil;

		// Need to make sure the global gActionFilterHelpers is properly initialized.
		if (gActionFilterHelpers.size() == 0)
			N2P_MAFActionFilter::ConfigureFilters();
	
		// find the action to perform from the global action helper list
		K2Vector<ActionFilterHelper>::iterator iter = gActionFilterHelpers.begin();
		K2Vector<ActionFilterHelper>::iterator last = gActionFilterHelpers.end();

		ActionID origActionID, newActionID;
		ClassID origCompID, newCompID;
		PMString origActionName, newActionName;

		/* NOTE: Since we are going to iterate through a K2Vector to find the correct 
			ActionFilterHelper class, the following is an O(n) algorithm. 
			If you have lots of actions to process, this may take a little while, so
			using a K2Vector may not be the best solution for you.  
		*/
		while (iter != last)
		{
			ActionFilterHelper helper = *iter;
			// get the current action ID and name
			helper.GetFilterActionID(origActionID, newActionID);
			helper.GetFilterActionName(origActionName, newActionName);

			// see if the action filter matches the passed in actionID
			if (newActionID == actionID)
			{
				// match!  Go find the IActionComponent (refCount added)
				// so we don't get into an endless loop!
				origActionComponent = helper.QueryOrigActionComponent();
				if (origActionComponent)
				{
					// do something before the actionComponent
					//this->PreProcess(newActionName);

					// call original actionComponent
					//ActionID myUpdateDesignActionID(94721);
					switch (actionID.Get())
					{
						case 94721://Update Design
						{
							
							if(IsN2PUserLoged==kTrue)
							{
								PreferencesConnection PrefConections;
								UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
								UpdateStorys->ActualizaTodasLNotasYDisenoPagina("N2PActualizacion", PrefConections);
								/*UpdateStorys->CheckInNotasEnEdicion();
								//UpdateStorys->MakeUpdate();
								//checkIn->AutomaticSaveRevi();
								if(Utils<IInCopyDocUtils>()->CanDoUpdateDesign (::GetUIDRef(Utils<ILayoutUIUtils>()->GetFrontDocument())))
								{
									///antes que nada hay que guardar el id de las 
									//notas que se estan editando para que no se pierda el hido de ellas
									Utils<IInCopyDocUtils>()->DoUpdateDesign(::GetUIDRef(Utils<ILayoutUIUtils>()->GetFrontDocument()));
				
									
									//Hace un update de las notas
									UpdateStorys->Update();
									
									//Pone todos los elementos en estado de no edicion
									PMString CadenaCompleta=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate");
									if(CadenaCompleta.NumUTF16TextChars()>0)
									{
									
										int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
			
										ClassRegEnXMP *ListUpdate = new ClassRegEnXMP[contUpdate];
	
										N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
									
										for(int32 numElem=0;numElem<contUpdate;numElem++)
										{
											PMString IDFrameEleNota="";
											IDFrameEleNota.AppendNumber(ListUpdate[numElem].IDFrame);
											UpdateStorys->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kTrue);
										}
									}
						
									K2Vector<int32> Notas;
									if(UpdateStorys->ObtenerIDNotasPUpdateDesign(Notas))
									{
										//Cambia modo de bloquedo de cada una de los textframe que se encontraban editando
										for(int32 i=0;i<Notas.Length();i++)
										{
											PMString IDFrameEleNota="";
											IDFrameEleNota.AppendNumber(Notas[i]);
											//CAlert::InformationAlert(IDNota);
											UpdateStorys->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kFalse);
										}
									}
								}*/
							}
							else
							{
								if(Utils<IInCopyDocUtils>()->CanDoUpdateDesign (::GetUIDRef(Utils<ILayoutUIUtils>()->GetFrontDocument())))
								{
									///antes que nada hay que guardar el id de las 
									//notas que se estan editando para que no se pierda el hido de ellas
									Utils<IInCopyDocUtils>()->DoUpdateDesign(::GetUIDRef(Utils<ILayoutUIUtils>()->GetFrontDocument()));
				
									//CAlert::InformationAlert("Update Design");
									
								}
							}
							break;
						}
						
						
						case kQuitActionID:
						{
							//CAlert::InformationAlert("Salio");
							break;
						}
						
						
						case kCloseActionID:
						{
							if(IsN2PUserLoged==kTrue)
							{
								//*Pendiente hacer checkin de todas los documentos que se encuentran en uso
								/*
								//UpdateStorys->ActualizaTodasLNotasYDisenoPagina();
								UpdateStorys->ActualizaNotasDesdeBD(document);
								UpdateStorys->MakeUpdate();
								//Para Gurdar Revision
								if(Applicacion.Contains("InDesign"))
									checkIn->CheckIn();
								else
									origActionComponent->DoAction(ac, actionID, mousePoint, widget);
									*/
							}
							else
							{
								origActionComponent->DoAction(ac, actionID, mousePoint, widget);
							}
							
							break;
						}
						case kSaveAsActionID:
						{
							//CAlert::InformationAlert("ZZZZZ");
							if(IsN2PUserLoged==kTrue)
							{
								PreferencesConnection PrefConections;
								UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
								UpdateStorys->ActualizaTodasLNotasYDisenoPagina("Save As", PrefConections);
								
							}
							else
							{
								//CAlert::InformationAlert("DDDDD");
								origActionComponent->DoAction(ac, actionID, mousePoint, widget);
							}
							
							break;
						}
		
		
						case kSaveActionID:
						{
							//CAlert::InformationAlert("YYYYY");
							if(IsN2PUserLoged==kTrue)
							{
								//CAlert::InformationAlert("XXX");
								PreferencesConnection PrefConections;
								UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse);
								UpdateStorys->ActualizaTodasLNotasYDisenoPagina("Save", PrefConections);
							}
							else
							{
								//CAlert::InformationAlert("BBBB");
								origActionComponent->DoAction(ac, actionID, mousePoint, widget);
							}
							break;
						}

						case kPrintActionID:
						{
							origActionComponent->DoAction(ac, actionID, mousePoint, widget);
							break;
						}
		
						case kSaveACopyActionID:
						{
							origActionComponent->DoAction(ac, actionID, mousePoint, widget);
							break;
						}
		
	
						case kExportActionID:
						{
							origActionComponent->DoAction(ac, actionID, mousePoint, widget);
							break;
						}
						
						
						
						//#include "..\..\..\source\Interlasa\N2PCheckInOut\N2PCheckInOutID.h"
						default:
						{
							break;
						}
					}
					//origActionComponent->DoAction(ac, actionID, mousePoint, widget);

					// out of while loop
					break; 

				} // if (origActionComponent)...
				// otherwise, we will fall out..

			} // if (newID == actionID)...

			// get next ActionFilterHelper
			++iter; 

		} // while (iter != last)...

		/* Check if any action was performed...
			In the case where the ActionFilterHelper was configured without
			a ComponentClass, the helper.QueryOrigActionComponent() will return nil.
			If the helper.QueryOrigActionComponent() above did return nil, 
			no actions would be performed in the above loop because origActionComponent
			would be nil. 
		*/
		if (origActionComponent == nil)
		{
			// we still have an obligation to process the action, 
			// so let's ask the ActionManager for the current action component.
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			if (app)
			{
				InterfacePtr<IActionManager> actionManager(app->QueryActionManager());
				if (actionManager)
				{
					actionManager->PerformAction(ac, actionID, mousePoint, widget);
				}
			}	
		}
		else
		{
			// IActionComponent is non-nil, so we must clean up 
			origActionComponent->Release();
			origActionComponent = nil;
		}
	}while(false);
}

/* PreProcess
*/
void N2P_MAFActionComponent::PreProcess(const PMString& msg)
{
	// this routine does preprcessing... 
	// Modify this to suit your needs

	// display a simple message indicating that we have taken over 
	int16 uiScript = LocaleSetting::GetLocale().GetUIScript();
	PMString displayMsg("Processing Action: ",  PMString::kDontTranslateDuringCall);
	PMString msgCopy(msg);
	msgCopy.Translate();
	displayMsg += msgCopy;

	CAlert::InformationAlert(displayMsg);
}


/* DoAbout
*/
void N2P_MAFActionComponent::DoAbout()
{
	CAlert::ModalAlert
	(
		kN2P_MAFAboutBoxStringKey,				// Alert string
		kOKString, 						// OK button
		kNullString, 						// No second button
		kNullString, 						// No third button
		1,							// Set OK button to default
		CAlert::eInformationIcon				// Information icon.
	);
}



//  Code generated by DollyXs code generator
