//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/common/SDKODBCCache.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"
#include "K2Vector.tpp"
#include "K2Vector.h"
#include "CAlert.h"

// Interface includes
#include "URIList.h"

// Impl includes
#include <map>
// Project includes
#include "SDKODBCCache.h"
#include "SDKODBCWrapper.h"
#include "SDKODBCConst.h"
#include <boost/thread/mutex.hpp>

#include <boost/thread/once.hpp>
#include <boost/thread/mutex.hpp>

std::auto_ptr<SDKODBCCache> SDKODBCCache::ts_SDKCacheSingleInstance;
boost::once_flag ts_SDKCacheOnceFlag = BOOST_ONCE_INIT;
boost::mutex ts_SDKODBCCache_Mutex;

/* Private method used to create the single instance of SDKODBCCache
*/
void SDKODBCCache::InitSDKODBCCacheOnce()
{
	SDKODBCCache::ts_SDKCacheSingleInstance.reset(new SDKODBCCache);
	TRACEFLOW("SDKODBCWrapper", "New cache created\n");
}

/* Returns the one instance of SDKODBCCache
*/
SDKODBCCache& SDKODBCCache::Instance()
{
	boost::call_once(&InitSDKODBCCacheOnce, ts_SDKCacheOnceFlag);
	return *ts_SDKCacheSingleInstance;
}
	
/* Destructor
*/
SDKODBCCache::~SDKODBCCache()
{
	deleteTree();
}


/* deleteTree
*/
void SDKODBCCache::deleteTree() 
{
//	boost::mutex::scoped_lock lock (fMutex);
	fODBCCache.clear();
	fODBCCache2.clear();
}



/* Rebuild
*/
void SDKODBCCache::Rebuild(const URI& tableURI, PMString primaryKeyFieldName, K2Vector<WideString> &keyValues, K2Vector<PMString> &columnNames)
{
	//CAlert::InformationAlert("ZA1");
	boost::mutex::scoped_lock lock (ts_SDKODBCCache_Mutex);
	fODBCCache.clear();
	fODBCCache2.clear();
	SDKODBCWrapper odbc;
	//CAlert::InformationAlert("AA1");
	if (ISDKODBCWrapper::ODBCNormal != odbc.Query(tableURI))
	{
		CAlert::InformationAlert("ISDKODBCWrapper::ODBCNormal != odbc.Query(tableURI)");
		return;
	}

	//CAlert::InformationAlert("AA2");
	// first figure out the column names first
	int32 columnCounts = odbc.GetNumberOfColumns();
	for (int32 colIndex = 0; colIndex < columnCounts; colIndex++) 
	{
		std::string columnStr;
		odbc.GetMthColumnInfo(colIndex, columnStr);
		columnNames.push_back(PMString(columnStr.c_str()));		
	}
	
	//CAlert::InformationAlert("AA3");
	URI fieldURI;
	int32 numRecords = odbc.GetNumberOfRecords();	
	// traverse the record set and create a drop-down list representing the data
	for (int32 rNum = 0; rNum < numRecords; rNum++) 
	{
		//CAlert::InformationAlert("B 1");
		// get the value of the first field to use as a key value for query URI for this record
		WideString keyValue = odbc.GetNthRecordMthColumn(rNum, 0);
		PMString CDW(keyValue);
		CAlert::InformationAlert(CDW);
		
		keyValues.push_back(keyValue);

		// get the URI for every field in the record like this:  
		//		"odbc://MySQL/?table=inventory&sku='AB-219'#price");
		WideString fieldQueryPrefix(tableURI.GetURIW() + "&" + primaryKeyFieldName + "='" + keyValue + "'");
		fieldURI.SetURIW(fieldQueryPrefix);
		//CAlert::InformationAlert("B 2");
		// figure out the field names from the fragment part of the URI
		WideString uriFragment(kExtLinkDBAllColumns);
		if (tableURI.HasComponent(URI::kFragment))
		{
			uriFragment= tableURI.GetComponent(URI::kFragment);
		}
		//CAlert::InformationAlert("B 3");
		WideString delimiterComma(",");
		for(int32 i = 1; ; i++)
		{
			//CAlert::InformationAlert("C 1");
			K2::scoped_ptr<WideString> field(uriFragment.GetItem(delimiterComma, i));
			if (!field)
			{
				//CAlert::InformationAlert("!field");
				break;
			}
			//CAlert::InformationAlert("C 2");
			fieldURI.RemoveComponent(URI::kFragment);
			fieldURI.SetComponent(URI::kFragment, *field); // create an URI that points to the field
			WideString fieldData = odbc.GetNthRecordMthColumn(rNum, i-1);	// fieldData is the real data the current field contains that we want to cache
			std::string fieldName;
			//CAlert::InformationAlert("C 3");
			ISDKODBCWrapper::ItemType	fieldType = odbc.GetMthColumnInfo(0, fieldName);
			std::map<URI, WideString >::const_iterator result = fODBCCache.find(fieldURI);	// check if the URI (key) is already stored in the map
			if (result == fODBCCache.end() && !fieldData.IsNull())	// we don't find a cache with such key, and the key has a valid value
			{
				fODBCCache.insert(std::pair<URI, WideString>(fieldURI, fieldData));	// add the cache then
				ODBCCacheData	cd;
				cd.fFieldName = fieldName;
				cd.fFieldData = fieldData;
				cd.fFieldType = fieldType;
				fODBCCache2.insert(std::pair<URI, ODBCCacheData>(fieldURI, cd));	// add the cache 
			}
			//CAlert::InformationAlert("C 4");
		}
		//CAlert::InformationAlert("B 4");
	}
}

K2Vector<ODBCCacheData> SDKODBCCache::GetCacheData(const URI& uri, bool updateCache)
{
	// This routine right now does not handle all uri permitted in SDKODBCWrapper, here are the legal ones:
	// odbc://MySQL?table=inventory&sku='AB-219'||sku='AB-220'#sku,name,price,quantity,image,description,version
	// where query conditions (sku='AB-220') is optional and it always follows the table query with an &, and when multiple sku conditions are specified, they are separated by "||"
	// fragment is optional, when no fragment is specified, it will be treated as all fields query.
	// one scenario this does not handle is multiple tables queries, separated by a comma.  The sample uses one table only, which is "inventory"
	boost::mutex::scoped_lock lock (ts_SDKODBCCache_Mutex);
	URI myURI = uri;
	K2Vector<ODBCCacheData> retval;
    if (!uri.VerifyURI())
		return retval;
	
	if (!myURI.HasComponent(URI::kFragment))
	{
		myURI.SetComponent(URI::kFragment, kExtLinkDBAllColumns);
	}
	
	WideString uriQuery(uri.GetComponent(URI::kQuery));
	WideString delimiterAmpersand("&");
	K2::scoped_ptr<WideString> tableQuery(uriQuery.GetItem(delimiterAmpersand, 1));
	K2::scoped_ptr<WideString> skuQuery(uriQuery.GetItem(delimiterAmpersand, 2));
	URIList	recordURIs;
	// decompose an URI that may represents multiple fields for multipe records into an array of URIs that represents one field per record
	if (!skuQuery) // the uri does not specify which SKU to query. get every records, you should probably not pass in an URI that represents every records
	{	
		SDKODBCWrapper odbc1;
		
		if (ISDKODBCWrapper::ODBCNormal == odbc1.Query(uri))
		{
			int32 numRecords = odbc1.GetNumberOfRecords();	
			// traverse the record set and create one URI for each record
			for (int32 rNum = 0; rNum < numRecords; rNum++) 
			{
				// get the value of the first field to use as a key value for query URI for this record
				WideString keyValue = odbc1.GetNthRecordMthColumn(rNum, 0);
				WideString newQuery(*tableQuery+"&sku='"+keyValue+"'");
				myURI.RemoveComponent(URI::kQuery);
				myURI.SetComponent(URI::kQuery, newQuery);
				recordURIs.AddURI(myURI);
			}
		}
	}
	else
	{
		WideString delimiterOr("||");
		for (int32 skuCount = 1; ; skuCount++)
		{
			K2::scoped_ptr<WideString> sku(skuQuery->GetItem(delimiterOr, skuCount));
			if(!sku || sku->IsNull())
				break;

			// GetItem does not seem to see the "||" as a whole delimiter, it will include "|" in the result, so we need to strip it off
			WideString junk("|");
			WideString skuStr;
			int32 index = sku->IndexOf(junk);
			if (index == 0)
			{
				K2::scoped_ptr<WideString> tempWS(sku->Substring(1));
				skuStr = *tempWS;
			}
			else
			{
				skuStr = *sku;
			}

			WideString newQuery(*tableQuery+"&"+skuStr);
			myURI.RemoveComponent(URI::kQuery);
			myURI.SetComponent(URI::kQuery, newQuery);
			recordURIs.AddURI(myURI);
		}
	}

	// process each URI in recordURIs
	int32 uriCount = recordURIs.GetURICount();
	for (int32 i = 0 ; i < uriCount; i++)
	{
		URI theURI = recordURIs.GetNthURI(i);
		
		WideString uriFragment(theURI.GetComponent(URI::kFragment));
		theURI.RemoveComponent(URI::kFragment);
		WideString delimiterComma(",");
		for(int32 j = 1; ; j++)
		{
			K2::scoped_ptr<WideString> field(uriFragment.GetItem(delimiterComma, j));
			if (!field)
				break;

			// at this point, the URI should look like odbc://MySQL?table=inventory&sku='AB-223'#price and we are ready to do single SKU query based on the fragment field
			theURI.SetComponent(URI::kFragment, *field);
			std::map<URI, ODBCCacheData >::iterator result = fODBCCache2.find(theURI);	// check if the URI (key) is already stored in the map
			if (result == fODBCCache2.end())	// we don't find a cache with such key
			{
				SDKODBCWrapper odbc;
				if (ISDKODBCWrapper::ODBCNormal == odbc.Query(theURI))
				{
					WideString fieldData = odbc.GetNthRecordMthColumn(0, 0);
					if (!fieldData.IsNull())
					{
						std::string fieldName;
						ISDKODBCWrapper::ItemType fieldType = odbc.GetMthColumnInfo(0, fieldName);
						ODBCCacheData	cd;
						cd.fFieldName = fieldName;
						cd.fFieldData = fieldData;
						cd.fFieldType = fieldType;
						fODBCCache2.insert(std::pair<URI, ODBCCacheData>(theURI, cd));	// add the cache 
						retval.push_back(cd);
					}
				}
			}
			else
			{
				if (updateCache)
				{
					SDKODBCWrapper odbc;
					if (ISDKODBCWrapper::ODBCNormal == odbc.Query(theURI))
					{
						WideString fieldData = odbc.GetNthRecordMthColumn(0, 0);
						if (!fieldData.IsNull()) {
							std::string fieldName;
							ISDKODBCWrapper::ItemType	fieldType = odbc.GetMthColumnInfo(0, fieldName);
							result->second.fFieldName = fieldName;
							result->second.fFieldData = fieldData;
							result->second.fFieldType = fieldType;
						}
					}
				}
				retval.push_back(result->second);
			}
		}
	}
	return retval;
}

bool SDKODBCCache::IsCacheExisted(const URI& uri) const
{
	boost::mutex::scoped_lock lock (ts_SDKODBCCache_Mutex);
	std::map<URI, WideString >::const_iterator result = fODBCCache.find(uri);	// check if the URI (key) is already stored in the map
	return !(result == fODBCCache.end());
}