//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/IExtLinkUIDataModel.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IExtLinkUIDataModel_H_DEFINED__
#define __IExtLinkUIDataModel_H_DEFINED__

#include "IPMUnknown.h"
#include "URI.h"
#include "ExtLinkUIID.h"

class ExtLinkUINodeID;
class ExtLinkUIDataNode;

/**  @ingroup extendedlinkui
*/
class IExtLinkUIDataModel : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IEXTLINKUIDATAMODEL };


	/**	Get the database the model is based upon
		@return Returns the database that the model is based upon
	 */
	virtual IDataBase* GetCurrentDocDB() const = 0;

	/**	Set the database the model is based upon
		@param newDocDB [IN] specifies specifies the database
	 */
	virtual void SetCurrentDocDB(IDataBase* newDocDB) = 0;


	/**	Determine the number of children given a data
		@param uri [IN] specifies the URI of the parent node 
		@param uid [IN] specifies the UID of the parent node
		@return Returns the number of children belonging to the specified parent
	 */
	virtual int32 GetNumChildren(const URI& uri, const UID uid) const = 0; 

	/**	Get the node pointer for the specified child number
		@param parentNode [IN] specifies the parent node
		@param nth [IN] specifies the child's node index
		@return Returns the specified node if found, nil otherwise
	 */
	virtual const ExtLinkUIDataNode* GetNthChild(const ExtLinkUIDataNode* parentNode, int32 nth) const = 0;
	
	/**	Get the node pointer for the specified node data
		@param uri [IN] specifies the URI of the node to find
		@param uid [IN] specifies the UID of the node to find
		@return Returns the specified node if found, nil otherwise
	 */
	virtual const ExtLinkUIDataNode* GetNode(const URI& uri, const UID uid) const = 0;

	/**	Get the data associated with the specified child
		@param parentURI [IN] specifies parent's URI
		@param parentUID [IN] specifies parent's UID
		@param nth [IN] specifies child's index within parent's list of children
		@param childURI [OUT] set to child's URI upon return
		@param childUID [OUT] set to child's UID upon return
		@return Returns kTrue if child exists 
	 */
	virtual bool16 GetNthChildData(const URI& parentURI, const UID parentUID, int32 nth, URI& childURI, UID& childUID) const = 0; 

	/**	Determine the index of the child within the parent's list of children
		@param parentNodeURI [IN] specifies parent's URI
		@param parentLinkUID [IN] specifies parent's UID
		@param childNodeURI [IN] specifies child's URI
		@param childLinkUID [IN] specifies child's UID
		@return Returns index 
	 */
	virtual int32 GetChildIndexFor(const URI& parentNodeURI, const UID parentLinkUID, const URI& childNodeURI, const UID childLinkUID) const = 0;

	/**	Call when you want to force the tree data to be rebuilt
	 */
	virtual void Rebuild() = 0;
	
	/** Get the default root URI
	*/
	virtual URI GetRootURI() const = 0;

};


#endif // __IExtLinkUIDataModel_H_DEFINED__

