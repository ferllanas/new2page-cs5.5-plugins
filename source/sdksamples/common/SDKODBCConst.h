//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/common/SDKODBCConst.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __ExtLinkConst_h__
#define __ExtLinkConst_h__

// General:
#include "WideString.h"

const WideString kExtLinkTagNamePrefix ("_ExtLink_");
const WideString kExtLinkTableTag("_ExtLink_TABLE_");
const WideString kExtLinkKeyTag ("_ExtLink_RECORD_");
const WideString kExtLinkFieldTag ("_ExtLink_FIELD_");
const WideString kExtLinkValueTag ("_ExtLink_VALUE_");

const WideString kExtLinkDBColumnSKU("sku");
const WideString kExtLinkCatalogSKUTitle("Model: ");
const WideString kExtLinkDBColumnName("name");
const WideString kExtLinkCatalogNameTitle("Name: ");
const WideString kExtLinkDBColumnPrice("price");
const WideString kExtLinkCatalogPriceTitle("Price: $");
const WideString kExtLinkDBColumnQuantity("quantity");
const WideString kExtLinkCatalogQuantityTitle("Quantity Available: ");
const WideString kExtLinkDBColumnImage("image");
const WideString kExtLinkDBColumnDescription("description");
const WideString kExtLinkCatalogDescriptionTitle("Description: ");
const WideString kExtLinkDBColumnVersion("version");
const WideString kExtLinkDBAllColumns("sku,name,price,quantity,image,description,version");

const WideString kExtLinkTableNameAttribute("table");
const WideString kExtLinkRecordKeyAttribute ("key");
const WideString kExtLinkFieldNameAttribute ("field");

const WideString kWideString_ExtLinkSeparatorSpace("_");
const WideString kWideString_ExtLinkCarriageReturn ("\r");
const WideString kWideString_ExtLinkNull ("");
const WideString kWideString_ExtLinkValueNotFound("####");

// Number of zero width character added when tag text with XML 
const int32 numZWCharsAdded = 2;

// File control characters
const uchar kuchar_Separator = ',';
const uchar kuchar_CR = 0x0d;
const uchar kuchar_LF = 0x0a;

const WideString k_DataUpdatekeyString ("SKU");
const WideString k_DataUpdatepriceString ("Price");
const WideString k_DataUpdatedescriptionString ("Description");

#ifdef WINDOWS
const PMString kPlatformPathDelimiter("\\");
#endif
#ifdef MACINTOSH
const PMString kPlatformPathDelimiter(":");
#endif
#endif

// End, ExtLinkConst.h


