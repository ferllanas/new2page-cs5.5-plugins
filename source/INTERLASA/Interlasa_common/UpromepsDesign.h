/*[#text:            **********************************************************************************************]
 [#text:        * Name        : ] UpromepsDesign.h
 [#text:      * Design      : ] SwInterlasa
 [#text:   * Created By  : ] Sentinel UltraPro v1.0.0
 [#text: * Description : Include File for Toolkit design strategy.]
 [#text:     * Purpose     : This file is required to include in program used to design strategy created by Toolkit.]

 [#text:   * Generated   : ] 10/22/2009 (MM/DD/YYYY)

 [#text:     * WARNING - this file is generated, any edits might be overwritten!]
 [#text:               *********************************************************************************************]**/

#include "Upromeps.h"

#define DESIGNID                                                             0xBED0
#define SP_A_INTEGER                                                         0x20008
#define SP_N2PLIMIT_FULLLICENSE                                              0x60014
#define SP_N2PLIMIT_FULLLICENSE_USER_LIMIT                                   0x9000A
#define SP_N2PLIMIT_FULLLICENSE_TIME_TAMPER                                  0x80012
#define SP_N2PLIMIT_FULLLICENSE_EXECUTION_CONTROL                            0x80013
#define SP_N2PLIMIT_FULLLICENSE_EXPIRATION_DATE                              0x90011
#define SP_N2PLIMIT_FULLLICENSE_TRIAL_PERIOD                                 0x90010

 /*[#text: Total number of query response: ]    501*/
#define SP_NUM_OF_QR                                                  501

 /*[#text: Length of query data: ]    4*/
#define SP_LEN_OF_QR                                                  4

 /*[#text: This query response is generated for element : ]  N2PLIMIT */
unsigned char QUERY_SP_N2PLIMIT_FULLLICENSE[SP_NUM_OF_QR]  [SP_LEN_OF_QR] = {
{0x40,0x6E,0x0D,0x74},
{0x47,0x55,0x41,0x49},
{0x45,0x47,0x4F,0x32},
{0x23,0x40,0x73,0x1B},
{0x1C,0x15,0x3D,0x62},
{0x42,0x33,0x37,0x23},
{0x2D,0x06,0x61,0x23},
{0x2C,0x43,0x5B,0x56},
{0x65,0x0B,0x23,0x07},
{0x4B,0x65,0x5A,0x09},
{0x19,0x65,0x0C,0x0C},
{0x17,0x77,0x19,0x51},
{0x7D,0x35,0x3A,0x4E},
{0x1B,0x6D,0x7D,0x1A},
{0x5F,0x77,0x10,0x79},
{0x44,0x2A,0x03,0x7E},
{0x17,0x08,0x78,0x3E},
{0x6D,0x47,0x10,0x73},
{0x11,0x3D,0x04,0x7D},
{0x47,0x4D,0x25,0x39},
{0x36,0x4E,0x1A,0x37},
{0x4C,0x03,0x67,0x04},
{0x11,0x50,0x12,0x28},
{0x31,0x4A,0x23,0x60},
{0x38,0x6C,0x3A,0x69},
{0x1A,0x0C,0x2F,0x17},
{0x5F,0x68,0x68,0x23},
{0x0D,0x03,0x67,0x46},
{0x13,0x4B,0x79,0x2B},
{0x7D,0x3A,0x4C,0x2B},
{0x30,0x21,0x6F,0x01},
{0x3E,0x0D,0x12,0x01},
{0x1B,0x32,0x79,0x74},
{0x0F,0x7B,0x79,0x5D},
{0x5F,0x07,0x3A,0x19},
{0x44,0x3A,0x17,0x39},
{0x40,0x4F,0x53,0x78},
{0x1D,0x04,0x08,0x40},
{0x1D,0x2B,0x01,0x5F},
{0x05,0x22,0x28,0x68},
{0x19,0x0D,0x1B,0x34},
{0x37,0x7E,0x74,0x71},
{0x02,0x46,0x62,0x5D},
{0x04,0x5F,0x71,0x4C},
{0x0D,0x64,0x6F,0x39},
{0x12,0x0A,0x33,0x23},
{0x01,0x2E,0x38,0x66},
{0x4B,0x3C,0x36,0x69},
{0x03,0x79,0x41,0x55},
{0x7B,0x0E,0x79,0x63},
{0x18,0x48,0x5C,0x04},
{0x32,0x45,0x6D,0x7B},
{0x43,0x1F,0x0D,0x54},
{0x5D,0x3B,0x5C,0x76},
{0x1F,0x26,0x26,0x61},
{0x24,0x46,0x3F,0x18},
{0x38,0x22,0x6A,0x21},
{0x43,0x15,0x7D,0x76},
{0x12,0x6A,0x10,0x61},
{0x26,0x6F,0x20,0x61},
{0x10,0x28,0x4B,0x42},
{0x77,0x0F,0x30,0x72},
{0x62,0x41,0x5B,0x09},
{0x39,0x23,0x14,0x75},
{0x55,0x42,0x5A,0x5B},
{0x3C,0x32,0x41,0x1E},
{0x11,0x56,0x4F,0x35},
{0x7E,0x00,0x1E,0x74},
{0x38,0x62,0x1A,0x34},
{0x6D,0x25,0x72,0x5A},
{0x12,0x66,0x01,0x41},
{0x63,0x2B,0x3C,0x78},
{0x61,0x0F,0x44,0x43},
{0x23,0x40,0x27,0x2A},
{0x4B,0x6A,0x3E,0x35},
{0x62,0x68,0x15,0x78},
{0x53,0x3F,0x74,0x19},
{0x65,0x44,0x41,0x39},
{0x0C,0x3A,0x22,0x56},
{0x00,0x13,0x62,0x00},
{0x79,0x35,0x39,0x1E},
{0x2B,0x2F,0x56,0x37},
{0x16,0x15,0x79,0x2D},
{0x4E,0x73,0x55,0x08},
{0x37,0x6E,0x50,0x3B},
{0x6F,0x0E,0x17,0x16},
{0x3B,0x54,0x70,0x7B},
{0x41,0x05,0x28,0x3B},
{0x3D,0x22,0x64,0x76},
{0x44,0x3E,0x1B,0x55},
{0x1E,0x28,0x6F,0x52},
{0x0C,0x04,0x34,0x41},
{0x06,0x57,0x47,0x0D},
{0x7D,0x2D,0x15,0x44},
{0x27,0x64,0x78,0x22},
{0x3C,0x4B,0x2A,0x26},
{0x3B,0x5B,0x58,0x12},
{0x26,0x7B,0x0D,0x3F},
{0x62,0x01,0x08,0x0C},
{0x32,0x68,0x55,0x03},
{0x54,0x2B,0x1F,0x55},
{0x6E,0x47,0x0F,0x13},
{0x4B,0x34,0x7B,0x42},
{0x11,0x60,0x4B,0x4C},
{0x2E,0x42,0x01,0x05},
{0x4F,0x1C,0x11,0x00},
{0x7E,0x7E,0x0B,0x70},
{0x15,0x20,0x70,0x52},
{0x13,0x29,0x5B,0x7A},
{0x49,0x2A,0x4F,0x43},
{0x54,0x28,0x49,0x1A},
{0x2E,0x73,0x4A,0x25},
{0x04,0x63,0x06,0x45},
{0x09,0x35,0x04,0x0D},
{0x7E,0x56,0x53,0x65},
{0x69,0x03,0x20,0x13},
{0x77,0x1D,0x06,0x39},
{0x72,0x0A,0x57,0x4A},
{0x1C,0x34,0x62,0x7A},
{0x62,0x79,0x15,0x3F},
{0x2F,0x50,0x64,0x06},
{0x1C,0x33,0x21,0x42},
{0x25,0x2D,0x4D,0x6B},
{0x6A,0x00,0x4E,0x4C},
{0x15,0x4B,0x23,0x2A},
{0x6F,0x3F,0x75,0x3C},
{0x09,0x7B,0x6E,0x35},
{0x3F,0x76,0x4B,0x7D},
{0x3D,0x50,0x58,0x21},
{0x04,0x42,0x59,0x78},
{0x37,0x0E,0x18,0x42},
{0x0E,0x73,0x70,0x3F},
{0x27,0x74,0x70,0x63},
{0x49,0x33,0x1A,0x46},
{0x03,0x2C,0x26,0x13},
{0x0F,0x2B,0x65,0x44},
{0x76,0x43,0x41,0x7E},
{0x7B,0x5A,0x21,0x75},
{0x0C,0x42,0x11,0x39},
{0x31,0x4F,0x10,0x3C},
{0x77,0x6B,0x1A,0x26},
{0x59,0x5E,0x0B,0x34},
{0x2A,0x17,0x74,0x03},
{0x78,0x02,0x6A,0x0D},
{0x6E,0x64,0x62,0x46},
{0x0F,0x5F,0x79,0x4C},
{0x1E,0x42,0x0C,0x4B},
{0x11,0x36,0x0E,0x65},
{0x5E,0x66,0x60,0x12},
{0x5A,0x29,0x3E,0x2E},
{0x2F,0x17,0x20,0x79},
{0x38,0x61,0x38,0x04},
{0x1A,0x76,0x30,0x53},
{0x49,0x53,0x3C,0x3A},
{0x58,0x68,0x55,0x42},
{0x6E,0x78,0x0F,0x12},
{0x71,0x1F,0x39,0x47},
{0x46,0x43,0x78,0x34},
{0x46,0x2B,0x76,0x14},
{0x16,0x50,0x53,0x7D},
{0x68,0x29,0x63,0x39},
{0x22,0x1E,0x69,0x06},
{0x5F,0x66,0x6E,0x64},
{0x30,0x10,0x67,0x0B},
{0x4C,0x48,0x2C,0x78},
{0x22,0x10,0x51,0x5D},
{0x3A,0x3A,0x4F,0x5C},
{0x4F,0x79,0x36,0x61},
{0x7C,0x5C,0x4F,0x79},
{0x03,0x5C,0x60,0x33},
{0x55,0x3B,0x48,0x45},
{0x52,0x57,0x21,0x29},
{0x29,0x71,0x73,0x52},
{0x57,0x30,0x4D,0x7B},
{0x30,0x49,0x6F,0x60},
{0x75,0x4D,0x25,0x43},
{0x27,0x42,0x76,0x2F},
{0x58,0x70,0x0D,0x40},
{0x4B,0x3F,0x33,0x7A},
{0x65,0x6E,0x46,0x03},
{0x42,0x02,0x42,0x06},
{0x17,0x1B,0x4D,0x01},
{0x09,0x52,0x0C,0x41},
{0x0E,0x43,0x71,0x05},
{0x6A,0x43,0x04,0x27},
{0x6D,0x4E,0x01,0x5E},
{0x51,0x3E,0x67,0x4B},
{0x20,0x28,0x7A,0x0F},
{0x36,0x5A,0x0A,0x52},
{0x77,0x40,0x6D,0x76},
{0x64,0x31,0x2D,0x41},
{0x27,0x3F,0x65,0x5F},
{0x17,0x38,0x65,0x4B},
{0x75,0x4C,0x60,0x70},
{0x33,0x3F,0x7E,0x6E},
{0x2C,0x7A,0x65,0x31},
{0x62,0x7A,0x3E,0x30},
{0x5C,0x66,0x5B,0x2E},
{0x40,0x23,0x1D,0x45},
{0x64,0x58,0x12,0x21},
{0x43,0x0A,0x28,0x3D},
{0x56,0x1D,0x69,0x59},
{0x47,0x31,0x3A,0x6B},
{0x23,0x06,0x35,0x2D},
{0x59,0x43,0x34,0x45},
{0x2E,0x45,0x1C,0x57},
{0x69,0x4E,0x05,0x36},
{0x57,0x6A,0x48,0x3E},
{0x05,0x14,0x38,0x0E},
{0x26,0x21,0x6A,0x46},
{0x7C,0x5E,0x55,0x63},
{0x20,0x4E,0x1C,0x35},
{0x13,0x36,0x50,0x1C},
{0x6E,0x2E,0x1D,0x69},
{0x39,0x04,0x59,0x09},
{0x26,0x22,0x5B,0x3B},
{0x5C,0x65,0x57,0x79},
{0x6C,0x4C,0x60,0x5A},
{0x71,0x56,0x2D,0x61},
{0x47,0x5B,0x71,0x29},
{0x47,0x75,0x78,0x04},
{0x6A,0x69,0x7B,0x3C},
{0x52,0x5C,0x5F,0x69},
{0x6B,0x78,0x3A,0x31},
{0x2E,0x4A,0x31,0x4C},
{0x25,0x6F,0x4D,0x06},
{0x51,0x73,0x29,0x5F},
{0x51,0x19,0x1F,0x29},
{0x3A,0x38,0x5A,0x13},
{0x28,0x43,0x7E,0x53},
{0x61,0x23,0x2B,0x26},
{0x72,0x20,0x15,0x58},
{0x47,0x24,0x05,0x29},
{0x3C,0x43,0x1E,0x75},
{0x1B,0x68,0x5B,0x3C},
{0x7C,0x28,0x3A,0x3F},
{0x08,0x35,0x51,0x63},
{0x1E,0x38,0x02,0x40},
{0x3B,0x12,0x51,0x67},
{0x77,0x31,0x32,0x3E},
{0x5D,0x41,0x36,0x4E},
{0x2B,0x60,0x53,0x54},
{0x5A,0x13,0x03,0x66},
{0x21,0x5F,0x0F,0x3A},
{0x18,0x07,0x31,0x1D},
{0x76,0x29,0x75,0x19},
{0x41,0x7A,0x50,0x79},
{0x2F,0x10,0x70,0x17},
{0x36,0x49,0x6C,0x1E},
{0x0B,0x02,0x75,0x54},
{0x43,0x03,0x21,0x72},
{0x59,0x37,0x5C,0x64},
{0x3A,0x1C,0x4E,0x1A},
{0x56,0x2F,0x40,0x03},
{0x1B,0x31,0x31,0x43},
{0x35,0x37,0x2D,0x59},
{0x7C,0x37,0x4E,0x2E},
{0x51,0x26,0x68,0x19},
{0x09,0x79,0x06,0x13},
{0x3B,0x01,0x30,0x1E},
{0x06,0x38,0x4A,0x55},
{0x0C,0x78,0x48,0x2B},
{0x68,0x20,0x2C,0x1F},
{0x01,0x15,0x69,0x06},
{0x28,0x7B,0x03,0x44},
{0x02,0x6B,0x3D,0x3A},
{0x59,0x46,0x17,0x20},
{0x0F,0x1D,0x47,0x23},
{0x7A,0x22,0x7D,0x5C},
{0x5A,0x35,0x04,0x41},
{0x48,0x1D,0x3A,0x3E},
{0x14,0x33,0x0C,0x13},
{0x1E,0x7D,0x1B,0x0F},
{0x7D,0x51,0x32,0x3C},
{0x0F,0x4D,0x1E,0x62},
{0x2B,0x1B,0x05,0x77},
{0x64,0x4D,0x7E,0x3C},
{0x51,0x31,0x68,0x41},
{0x52,0x1D,0x3A,0x50},
{0x07,0x5B,0x32,0x47},
{0x6B,0x13,0x7E,0x09},
{0x75,0x45,0x6E,0x46},
{0x51,0x7D,0x4E,0x0D},
{0x44,0x38,0x32,0x0C},
{0x21,0x26,0x07,0x63},
{0x61,0x5C,0x63,0x72},
{0x68,0x07,0x19,0x52},
{0x2D,0x61,0x73,0x5A},
{0x10,0x21,0x63,0x7E},
{0x21,0x19,0x12,0x17},
{0x38,0x1D,0x03,0x1F},
{0x5E,0x14,0x13,0x41},
{0x56,0x58,0x56,0x64},
{0x32,0x0D,0x1A,0x78},
{0x67,0x40,0x5F,0x3D},
{0x7E,0x6E,0x26,0x54},
{0x2F,0x04,0x42,0x7E},
{0x60,0x2E,0x13,0x31},
{0x00,0x52,0x4F,0x79},
{0x6E,0x00,0x27,0x35},
{0x0C,0x61,0x62,0x4B},
{0x21,0x2A,0x4B,0x57},
{0x59,0x10,0x19,0x0F},
{0x50,0x7D,0x44,0x17},
{0x7A,0x24,0x66,0x0E},
{0x51,0x16,0x32,0x28},
{0x1E,0x20,0x5F,0x50},
{0x0D,0x3F,0x5B,0x62},
{0x19,0x5C,0x1A,0x47},
{0x25,0x2B,0x71,0x60},
{0x5E,0x39,0x3B,0x04},
{0x1E,0x23,0x31,0x15},
{0x2A,0x66,0x61,0x3F},
{0x16,0x4F,0x71,0x3F},
{0x28,0x1D,0x54,0x1C},
{0x62,0x74,0x4B,0x39},
{0x0D,0x5A,0x21,0x61},
{0x02,0x59,0x5C,0x22},
{0x30,0x3F,0x1A,0x7A},
{0x04,0x11,0x0B,0x6A},
{0x51,0x59,0x00,0x3A},
{0x60,0x2C,0x17,0x17},
{0x37,0x22,0x67,0x12},
{0x49,0x29,0x4D,0x1B},
{0x59,0x50,0x29,0x0B},
{0x61,0x7C,0x58,0x1B},
{0x44,0x10,0x7B,0x10},
{0x5C,0x1F,0x2C,0x40},
{0x08,0x69,0x00,0x1D},
{0x62,0x46,0x06,0x21},
{0x35,0x73,0x3A,0x61},
{0x49,0x3D,0x6E,0x08},
{0x19,0x7D,0x1A,0x4D},
{0x01,0x45,0x3D,0x53},
{0x1D,0x5D,0x67,0x76},
{0x01,0x13,0x57,0x7C},
{0x08,0x11,0x02,0x0F},
{0x0F,0x43,0x7E,0x5A},
{0x3E,0x52,0x77,0x7C},
{0x0A,0x2C,0x15,0x64},
{0x7A,0x1F,0x1D,0x3F},
{0x63,0x15,0x1D,0x62},
{0x47,0x72,0x48,0x6E},
{0x12,0x0E,0x4F,0x73},
{0x17,0x61,0x04,0x04},
{0x10,0x40,0x70,0x15},
{0x74,0x4C,0x2D,0x76},
{0x40,0x33,0x4D,0x07},
{0x3A,0x75,0x27,0x7B},
{0x3E,0x1D,0x5B,0x22},
{0x62,0x4A,0x6A,0x22},
{0x1E,0x34,0x1E,0x07},
{0x05,0x49,0x2C,0x25},
{0x1C,0x2D,0x29,0x1D},
{0x23,0x38,0x14,0x17},
{0x2A,0x2E,0x18,0x72},
{0x6E,0x24,0x20,0x1E},
{0x2E,0x55,0x03,0x1C},
{0x4E,0x18,0x10,0x55},
{0x51,0x51,0x1A,0x5B},
{0x4C,0x2D,0x3E,0x16},
{0x46,0x22,0x39,0x6C},
{0x47,0x04,0x49,0x0E},
{0x0D,0x47,0x2C,0x65},
{0x4C,0x52,0x69,0x2F},
{0x37,0x3D,0x38,0x4A},
{0x36,0x5D,0x4A,0x48},
{0x0E,0x4C,0x44,0x27},
{0x51,0x76,0x43,0x20},
{0x79,0x33,0x35,0x23},
{0x70,0x09,0x29,0x17},
{0x44,0x37,0x61,0x3E},
{0x0E,0x2D,0x12,0x63},
{0x50,0x60,0x2D,0x06},
{0x5B,0x24,0x5A,0x62},
{0x1D,0x46,0x41,0x07},
{0x53,0x51,0x32,0x14},
{0x59,0x54,0x1C,0x4C},
{0x36,0x30,0x25,0x7B},
{0x0F,0x79,0x76,0x24},
{0x40,0x54,0x6C,0x06},
{0x31,0x6A,0x27,0x46},
{0x64,0x51,0x7B,0x4B},
{0x19,0x49,0x64,0x18},
{0x05,0x52,0x79,0x36},
{0x5A,0x42,0x2C,0x16},
{0x54,0x6A,0x09,0x04},
{0x7C,0x32,0x73,0x55},
{0x4D,0x6F,0x3F,0x04},
{0x1D,0x4F,0x5B,0x42},
{0x7B,0x00,0x3E,0x19},
{0x42,0x2A,0x14,0x25},
{0x2B,0x52,0x59,0x7E},
{0x43,0x1A,0x3E,0x08},
{0x54,0x6D,0x6D,0x33},
{0x5D,0x2E,0x06,0x0E},
{0x0D,0x26,0x64,0x7A},
{0x0D,0x47,0x1C,0x4F},
{0x79,0x7C,0x53,0x62},
{0x4E,0x2C,0x4C,0x42},
{0x7C,0x68,0x5D,0x27},
{0x54,0x66,0x21,0x42},
{0x00,0x75,0x03,0x5A},
{0x28,0x29,0x58,0x60},
{0x31,0x04,0x1B,0x7B},
{0x30,0x73,0x30,0x53},
{0x2C,0x2B,0x09,0x41},
{0x72,0x2C,0x4B,0x7D},
{0x27,0x58,0x32,0x15},
{0x7A,0x68,0x20,0x73},
{0x4D,0x64,0x0A,0x2F},
{0x02,0x35,0x1A,0x09},
{0x01,0x17,0x03,0x6C},
{0x50,0x6D,0x7E,0x02},
{0x5F,0x5A,0x29,0x39},
{0x76,0x60,0x60,0x22},
{0x5A,0x13,0x4E,0x65},
{0x1A,0x49,0x2C,0x14},
{0x2C,0x6F,0x13,0x17},
{0x1F,0x36,0x50,0x1C},
{0x12,0x6B,0x02,0x77},
{0x38,0x5C,0x02,0x52},
{0x62,0x3C,0x56,0x47},
{0x21,0x3D,0x2B,0x66},
{0x16,0x79,0x30,0x4D},
{0x2E,0x0C,0x24,0x3A},
{0x18,0x63,0x29,0x64},
{0x5E,0x2B,0x43,0x41},
{0x66,0x51,0x7D,0x28},
{0x5A,0x5F,0x63,0x7C},
{0x4F,0x3D,0x6F,0x28},
{0x0A,0x3F,0x06,0x4D},
{0x05,0x76,0x1A,0x76},
{0x6D,0x5B,0x66,0x46},
{0x55,0x50,0x63,0x13},
{0x07,0x71,0x02,0x4E},
{0x5C,0x07,0x78,0x46},
{0x5F,0x57,0x1A,0x6B},
{0x73,0x73,0x2C,0x2B},
{0x2B,0x7D,0x40,0x65},
{0x2C,0x70,0x48,0x62},
{0x00,0x2F,0x2B,0x23},
{0x2D,0x03,0x56,0x00},
{0x42,0x68,0x51,0x72},
{0x08,0x3C,0x76,0x5B},
{0x50,0x49,0x66,0x4A},
{0x39,0x63,0x58,0x20},
{0x3A,0x51,0x0D,0x49},
{0x5D,0x00,0x52,0x16},
{0x7A,0x43,0x1C,0x58},
{0x7B,0x7B,0x74,0x70},
{0x06,0x0C,0x23,0x33},
{0x7C,0x23,0x49,0x77},
{0x3A,0x77,0x09,0x10},
{0x7D,0x0D,0x77,0x5C},
{0x53,0x4E,0x4C,0x4F},
{0x42,0x6F,0x4D,0x62},
{0x7C,0x64,0x11,0x10},
{0x40,0x53,0x21,0x6F},
{0x4D,0x5E,0x17,0x7B},
{0x63,0x64,0x2A,0x35},
{0x20,0x4E,0x61,0x25},
{0x2F,0x00,0x3C,0x28},
{0x0D,0x08,0x08,0x3B},
{0x28,0x64,0x4F,0x1C},
{0x47,0x10,0x13,0x4B},
{0x57,0x23,0x30,0x69},
{0x74,0x54,0x0B,0x7A},
{0x15,0x4C,0x45,0x42},
{0x0A,0x3F,0x6C,0x68},
{0x66,0x13,0x6C,0x24},
{0x20,0x7A,0x3E,0x13},
{0x45,0x40,0x01,0x1F},
{0x51,0x4C,0x33,0x26},
{0x62,0x58,0x39,0x2C},
{0x1C,0x34,0x1C,0x2D},
{0x00,0x35,0x79,0x49},
{0x25,0x58,0x34,0x06},
{0x54,0x7E,0x09,0x21},
{0x25,0x4F,0x5B,0x15},
{0x4B,0x04,0x2A,0x10},
{0x78,0x19,0x53,0x7D},
{0x66,0x3C,0x78,0x56},
{0x3D,0x26,0x0C,0x2D},
{0x72,0x02,0x25,0x1A},
{0x2F,0x13,0x36,0x7D},
{0x33,0x43,0x3B,0x01},
{0x02,0x66,0x17,0x69},
{0x69,0x77,0x6F,0x3D},
{0x72,0x71,0x04,0x34},
{0x43,0x3A,0x70,0x45},
{0x0E,0x6A,0x26,0x6D},
{0x47,0x36,0x7A,0x4D},
{0x3C,0x55,0x32,0x05},
{0x67,0x7B,0x79,0x4D},
{0x6C,0x4D,0x7C,0x3B},
{0x35,0x1F,0x3E,0x3B},
{0x4E,0x33,0x1E,0x15},
{0x3E,0x48,0x53,0x6A},
{0x1C,0x66,0x69,0x2F},
{0x2E,0x07,0x07,0x33}
};
unsigned char RESPONSE_SP_N2PLIMIT_FULLLICENSE[SP_NUM_OF_QR] [SP_LEN_OF_QR] = {
{0xC4,0x8F,0x47,0x00},
{0xEC,0xF6,0xF5,0x6D},
{0x3F,0x48,0xCB,0x0D},
{0xD5,0xAD,0xFC,0xA7},
{0x30,0xD6,0xAE,0xAE},
{0x6A,0x58,0x04,0x25},
{0x79,0xB7,0x03,0xC4},
{0x0B,0xBE,0x67,0x24},
{0x87,0x30,0x7C,0x44},
{0x9A,0x08,0xAE,0x63},
{0x9F,0x43,0xF3,0x80},
{0x72,0xFA,0x47,0x11},
{0x4A,0x09,0xE3,0xF8},
{0x6C,0x27,0x09,0xDD},
{0x24,0xA8,0x46,0x25},
{0x4B,0xAB,0x48,0xD9},
{0xE6,0xFA,0xA5,0x55},
{0x1F,0x90,0xA0,0x54},
{0xB3,0x60,0x0D,0x3D},
{0x3B,0x7C,0x47,0x1E},
{0x6B,0x47,0x36,0x9E},
{0xE6,0x3A,0x0E,0x94},
{0x03,0x0E,0x44,0x75},
{0xCB,0xD9,0x83,0x27},
{0x9F,0xC0,0xB0,0xF6},
{0x5C,0x15,0x8B,0xF8},
{0x76,0xAC,0x6C,0x96},
{0xDB,0xCB,0x09,0x1C},
{0x95,0x7D,0x9B,0x37},
{0x8D,0xAE,0xB5,0xEA},
{0x31,0x0B,0xFD,0x0C},
{0xEA,0xD2,0x74,0x1D},
{0xDD,0xB8,0x42,0xB5},
{0x2A,0x34,0x14,0x78},
{0xC0,0xC4,0xD9,0x90},
{0x49,0x7F,0x63,0x1D},
{0x27,0xAE,0xC8,0xA4},
{0xAE,0x87,0x02,0x0F},
{0xEA,0x8F,0xAA,0x2E},
{0x11,0xC5,0x39,0x69},
{0x08,0xC4,0x47,0x4C},
{0xB4,0xFD,0x31,0x56},
{0x31,0xDD,0xE7,0x0C},
{0x2F,0x13,0xE5,0xBB},
{0xBD,0xB3,0x5C,0x92},
{0x31,0x1E,0x9A,0xE1},
{0xE7,0x43,0x3E,0x55},
{0x29,0xA7,0x8C,0x46},
{0xDC,0x20,0x64,0x6C},
{0x30,0x8D,0x5C,0x48},
{0x81,0xD6,0xBE,0x30},
{0xF8,0x81,0x2D,0xEA},
{0xFB,0x87,0x1D,0xEC},
{0x57,0x4A,0x1E,0x2E},
{0xC1,0xD2,0xC9,0x30},
{0x50,0xE6,0xC1,0xC2},
{0xB5,0xDE,0xA3,0x73},
{0x55,0x76,0xA0,0x61},
{0xC3,0x02,0x91,0xB6},
{0x7D,0x22,0xFB,0xE1},
{0x79,0x67,0xAC,0x96},
{0x8D,0x1A,0x96,0xFF},
{0x9D,0x62,0x3A,0x55},
{0xE1,0x52,0xEE,0x80},
{0xE2,0x3E,0x69,0x77},
{0x50,0x41,0x28,0xD6},
{0xAD,0x71,0x16,0x85},
{0xB2,0x50,0xE9,0x78},
{0x07,0x56,0x3E,0xF5},
{0x84,0x1B,0xD4,0x4F},
{0x32,0x2F,0x91,0x76},
{0x6B,0x56,0xBC,0xD4},
{0x2E,0x76,0x13,0x50},
{0xD2,0x5F,0x27,0xF8},
{0x8A,0x7E,0x22,0xE5},
{0xDD,0xF5,0x5B,0xA2},
{0xDF,0xB3,0x8E,0x08},
{0xBA,0xED,0xCC,0x96},
{0x8B,0xC2,0xAE,0xDA},
{0xDE,0x31,0xCA,0x5A},
{0x36,0xD6,0x4A,0x31},
{0xD2,0xD8,0xDF,0x7C},
{0x7A,0xB6,0x15,0x30},
{0x69,0x34,0xE8,0xD2},
{0x17,0xFB,0xE8,0xD3},
{0x54,0x89,0x89,0xBA},
{0x3B,0x9C,0x37,0xEC},
{0x52,0x12,0x6D,0xA6},
{0x7B,0xB1,0xC3,0x7F},
{0xC4,0x27,0xFA,0x22},
{0x2F,0x58,0x88,0xF4},
{0x67,0x6A,0xCA,0x0A},
{0xBE,0x06,0xD8,0x2E},
{0x6E,0xB1,0xDC,0xA8},
{0x4D,0x31,0x96,0xBF},
{0xC9,0x60,0x5A,0x0B},
{0xC7,0x35,0x13,0xFF},
{0xD7,0x55,0x7C,0x3F},
{0xB6,0x6D,0x8F,0xD4},
{0x8F,0xE2,0x12,0x46},
{0xA6,0x94,0xB4,0x1E},
{0x17,0x2F,0x52,0xAD},
{0xEB,0x5F,0xA0,0x53},
{0x98,0x93,0x59,0x90},
{0x65,0x46,0x67,0x7C},
{0x58,0xB1,0xB0,0xF8},
{0x66,0x9D,0xAF,0x48},
{0xB0,0xD7,0xCD,0xBD},
{0x53,0x2F,0x95,0xEA},
{0x33,0x76,0x5E,0x2D},
{0x28,0xAB,0x6C,0xFF},
{0x23,0xC8,0x76,0xFB},
{0x22,0x96,0x69,0x2F},
{0x18,0xC7,0xE5,0x4D},
{0xE4,0xA4,0xCC,0x74},
{0x11,0xF7,0xC5,0x21},
{0xB9,0x88,0x97,0x9F},
{0xD1,0x8C,0x19,0x1F},
{0x2D,0x18,0x2A,0xEE},
{0x79,0xF0,0xA8,0x45},
{0xFF,0x79,0x37,0x01},
{0xF7,0x8A,0xDF,0x16},
{0xC0,0xF8,0x8A,0x42},
{0x2C,0x62,0x8C,0x99},
{0x36,0x8B,0xEA,0x92},
{0x4A,0xFC,0xEB,0xEF},
{0xC2,0x38,0x75,0xB2},
{0x6A,0xE4,0x4B,0x68},
{0xBB,0x71,0xBF,0x1D},
{0xDD,0x23,0x6D,0xE8},
{0x97,0x3D,0xBB,0xB5},
{0x74,0x02,0xE2,0x18},
{0xB7,0xF3,0xF8,0x44},
{0xDF,0x5B,0x2E,0x46},
{0xF6,0x32,0x08,0xCE},
{0x56,0x58,0x72,0x42},
{0x8F,0xB9,0x30,0x61},
{0xCF,0x62,0xD0,0x29},
{0x51,0xCA,0x3C,0x1C},
{0x3F,0x81,0x81,0xA7},
{0x17,0x4A,0xEF,0x5B},
{0xD1,0x5C,0x2E,0x62},
{0x51,0xD7,0x3A,0x59},
{0x69,0x0B,0xAE,0x35},
{0x7E,0xF9,0x6F,0xD1},
{0x86,0x6A,0x73,0x77},
{0xA1,0x22,0x2E,0xAF},
{0x65,0xF4,0x94,0x67},
{0x6C,0x4A,0xBE,0xC2},
{0xDB,0xB3,0xCB,0x9B},
{0x55,0xAA,0x0D,0xA7},
{0x53,0x5E,0x6C,0x1D},
{0x7B,0x4C,0x25,0xBA},
{0x54,0x68,0x14,0x66},
{0x3E,0x8B,0x47,0x8F},
{0x45,0x6A,0x40,0x1E},
{0x4E,0xC4,0x99,0xE2},
{0xB5,0xC8,0x16,0x0C},
{0x10,0xA3,0xFA,0x28},
{0x50,0x1C,0x2A,0x4F},
{0xD9,0x6E,0x8E,0x60},
{0x9B,0x86,0x03,0x41},
{0xD8,0x57,0x65,0x0E},
{0xFC,0x96,0xF2,0xAC},
{0x70,0x8F,0x0C,0xA2},
{0x33,0x50,0x40,0x00},
{0x89,0xD6,0x76,0xDF},
{0x6A,0x6B,0x58,0xCD},
{0x19,0x3D,0xF6,0x02},
{0x68,0x8D,0xA7,0x57},
{0x4D,0x15,0xAD,0x83},
{0xD8,0x10,0x35,0xB8},
{0x44,0xA7,0xF6,0x5F},
{0x34,0xEF,0xEF,0x9A},
{0x45,0x78,0xFD,0xDF},
{0x8A,0x54,0xBA,0x53},
{0xA9,0xF4,0x70,0x0D},
{0x3B,0xDA,0x9E,0x3C},
{0xAB,0x99,0x6D,0xD6},
{0x00,0x80,0x73,0xBA},
{0x8C,0x83,0x20,0xAE},
{0x04,0x4B,0x0C,0x47},
{0x24,0xC9,0xF9,0xF3},
{0x40,0x26,0xB5,0xE9},
{0x28,0xBF,0xB8,0x1C},
{0x26,0x97,0x7A,0xE4},
{0x9F,0x96,0xF3,0x24},
{0x9E,0xC3,0xBD,0x48},
{0x7F,0xCE,0xAD,0xA3},
{0x27,0x3B,0xB4,0x8F},
{0x88,0x86,0xC4,0x25},
{0x88,0x8C,0x42,0xA9},
{0x0D,0x1F,0xD7,0xAE},
{0x4B,0xDC,0x78,0xFC},
{0x86,0x75,0x8E,0xD0},
{0x6F,0x85,0x00,0x0C},
{0xF1,0x67,0x88,0xD9},
{0xEF,0x4C,0x77,0x10},
{0xE2,0xFB,0xFC,0x45},
{0x3C,0x13,0x83,0x01},
{0x80,0xAD,0xC2,0x55},
{0x45,0xD7,0x07,0xED},
{0x90,0x0F,0x9B,0xC5},
{0xEC,0xA2,0x1E,0xED},
{0xD6,0xEB,0x67,0x28},
{0x2F,0xA9,0x70,0x43},
{0x94,0xCD,0x75,0x9C},
{0xA0,0x83,0x90,0xF1},
{0x19,0x06,0x1A,0xF3},
{0xF5,0x3B,0xDF,0xAA},
{0x30,0x4F,0x34,0x8B},
{0xF7,0x06,0x2E,0x0A},
{0xCF,0x33,0xD5,0x15},
{0xC0,0xC1,0xBF,0xF9},
{0xA0,0x81,0x31,0x54},
{0xEC,0x80,0x82,0x99},
{0xC2,0x56,0x7C,0x48},
{0x4B,0xA8,0x1E,0x4A},
{0xBE,0xBB,0xF0,0xE5},
{0x1B,0xB0,0xD5,0x37},
{0x9D,0x18,0xDD,0x7B},
{0xC0,0xD0,0x49,0xCC},
{0xC9,0x18,0xC0,0x42},
{0xB7,0x09,0x8A,0x48},
{0x55,0x71,0x55,0xBF},
{0x64,0x74,0x62,0x26},
{0x1E,0x6F,0xAD,0x3C},
{0x11,0x9B,0xE5,0xE8},
{0xB5,0x26,0x68,0x59},
{0x31,0x10,0x68,0x78},
{0x34,0x49,0x15,0xED},
{0xB7,0x0E,0x7F,0x21},
{0x9C,0x1C,0xDE,0x31},
{0x00,0x67,0x5C,0x7D},
{0x85,0x0C,0x95,0x10},
{0x75,0xCA,0x19,0x66},
{0x00,0x08,0x32,0x38},
{0x41,0x1D,0x2D,0x19},
{0x2F,0x96,0x49,0xDF},
{0xC3,0xE6,0xD2,0x97},
{0x75,0xE2,0x43,0x01},
{0xD2,0x68,0xD8,0x17},
{0xB3,0x10,0x4D,0x3C},
{0x83,0x55,0x43,0xBA},
{0xFF,0xA4,0x14,0xAE},
{0xA6,0x07,0x49,0x26},
{0xC4,0xCF,0x84,0x38},
{0xFA,0x67,0x3A,0x28},
{0xA9,0x80,0x7C,0x88},
{0x22,0x06,0x60,0x2F},
{0x42,0x75,0x56,0xF3},
{0x54,0x8A,0xDE,0x30},
{0xE7,0x5C,0xFC,0xA1},
{0xB7,0xB9,0xD1,0xA4},
{0x93,0x7D,0x44,0x6E},
{0x5E,0x0F,0x08,0x5A},
{0x2A,0x00,0xC6,0x88},
{0xBC,0xBB,0xBE,0x63},
{0x34,0xAE,0xC9,0x07},
{0x31,0xA8,0x98,0xAA},
{0x08,0xC3,0xBB,0x8D},
{0xF7,0x6C,0xE0,0x89},
{0x17,0x31,0x43,0x09},
{0xD2,0x42,0x69,0xBA},
{0x30,0x91,0xDE,0xC6},
{0x56,0xBC,0xC1,0xDA},
{0x54,0xC0,0x84,0x0D},
{0x42,0x6E,0xD8,0xB8},
{0x6F,0xD5,0xEC,0x4C},
{0x08,0xA6,0x24,0xEC},
{0xCA,0x13,0x31,0xDA},
{0xAB,0xA4,0xA1,0x27},
{0x92,0xC3,0x21,0x09},
{0xA2,0x6F,0xCD,0xA3},
{0xA7,0x0C,0x10,0x49},
{0xF2,0x4B,0xFE,0xD0},
{0x59,0x61,0x99,0x18},
{0x68,0x21,0xEB,0x6E},
{0x36,0x74,0x23,0x60},
{0xCB,0xAC,0xB3,0x8C},
{0x8F,0x08,0xC7,0x56},
{0xAE,0xAB,0xEC,0x4A},
{0x9E,0x06,0x8E,0x8C},
{0x5B,0xED,0xC9,0x8E},
{0x59,0xB4,0x27,0xC0},
{0xDA,0xDC,0x40,0x55},
{0x36,0xCD,0xED,0x17},
{0xCE,0x54,0x87,0x30},
{0xCB,0xFE,0x04,0xE8},
{0x5C,0x29,0xA1,0x51},
{0x17,0xA3,0x77,0x8F},
{0x22,0x64,0xC2,0xA8},
{0xEF,0x0C,0x3A,0x86},
{0xB9,0x1F,0x93,0x3B},
{0x8E,0xF6,0x85,0xEF},
{0xB6,0xBA,0x1C,0x0B},
{0x7D,0x7E,0x37,0x1C},
{0xA8,0x19,0x18,0x2E},
{0xC1,0xA5,0x5B,0xFB},
{0xDA,0xD7,0x8F,0x70},
{0x77,0x16,0xEF,0xAA},
{0x0F,0xE0,0x0B,0xC9},
{0x55,0xCF,0xC8,0x25},
{0xF0,0x37,0x84,0xB5},
{0xD6,0x7E,0xD7,0xCD},
{0x1F,0x6F,0x08,0xC5},
{0xCD,0xFB,0x97,0x32},
{0x77,0x5A,0xD5,0xD4},
{0xFC,0xCD,0xC6,0x93},
{0x42,0x2B,0xB3,0x6A},
{0xD3,0xAC,0xC4,0xEC},
{0xA1,0x88,0x2D,0x92},
{0xEF,0xC1,0xCA,0xC6},
{0x50,0xDB,0xAF,0x7D},
{0x8C,0x7C,0xAE,0x5D},
{0x77,0x8A,0x9D,0xA6},
{0x6F,0x27,0x63,0x48},
{0x60,0x94,0xAE,0x75},
{0xC6,0xB0,0x8B,0xCC},
{0x53,0xE2,0xF8,0xC9},
{0x28,0xA8,0x60,0xC6},
{0xDA,0x65,0xE8,0x91},
{0xA3,0xDE,0x2B,0x13},
{0xBE,0x8C,0x41,0xE2},
{0x5B,0x4E,0x98,0x1A},
{0xE4,0x59,0x7F,0xBB},
{0xCE,0x3B,0xBB,0x2E},
{0xFD,0x0D,0x88,0x7E},
{0xE7,0xCB,0x0E,0x5A},
{0xC3,0x7E,0xFD,0xEF},
{0xB3,0x70,0x35,0x67},
{0x73,0x48,0x5D,0xDC},
{0xFD,0x38,0x7C,0x63},
{0xDA,0xC9,0x74,0x07},
{0xF7,0x56,0x12,0x80},
{0xD0,0xAA,0xF7,0xD0},
{0x3F,0xF9,0x29,0x23},
{0x5E,0x5A,0x17,0xFE},
{0xD1,0x3D,0xA7,0xEB},
{0x96,0xD2,0x78,0x79},
{0xA2,0x54,0x8A,0xC4},
{0x63,0x35,0x15,0x06},
{0x9B,0x0F,0xE0,0x65},
{0x21,0x6C,0xFE,0x08},
{0xE3,0x97,0xCB,0x9C},
{0x32,0x28,0x96,0xCD},
{0xD6,0xB9,0x3C,0xD6},
{0x4B,0x28,0xD1,0x3B},
{0xF3,0xAB,0x92,0xD1},
{0x96,0xC0,0x5F,0x18},
{0x29,0x6E,0xFC,0x80},
{0x94,0x19,0xC5,0x1F},
{0x7C,0x0F,0xDA,0xDC},
{0x71,0xE5,0x3D,0x1B},
{0xF7,0x6C,0xDC,0x17},
{0x0E,0x7C,0x19,0xDB},
{0x25,0x5B,0x0B,0x40},
{0x7A,0xE1,0x41,0x96},
{0x9E,0x7E,0x70,0xC0},
{0x10,0x42,0xB5,0x94},
{0x97,0x39,0xBC,0x73},
{0xEA,0x3D,0x2A,0x36},
{0xE9,0x56,0x1C,0x3A},
{0x1E,0x1E,0x72,0x9D},
{0xA7,0xA0,0xE6,0xD4},
{0x8D,0x80,0xB1,0x5D},
{0x63,0x9E,0xBF,0xE3},
{0x4F,0x21,0x56,0xC0},
{0xE1,0x55,0x7A,0xE9},
{0x13,0xCF,0x2B,0x72},
{0x34,0x6E,0xF1,0x04},
{0xC5,0x43,0xBA,0xC4},
{0x67,0x5C,0x3A,0x76},
{0xCC,0x10,0x77,0x5B},
{0x85,0xD4,0x04,0x2E},
{0x5B,0x1C,0x11,0x01},
{0x1C,0xA6,0x61,0x43},
{0x52,0x61,0x53,0x64},
{0x21,0x69,0xD7,0xE0},
{0x45,0xC1,0xE7,0x04},
{0x7F,0xFE,0x54,0xDC},
{0xFA,0x7D,0xA1,0xB6},
{0xB9,0x9E,0x55,0xC6},
{0x4B,0x3A,0x4A,0xB5},
{0x2A,0xB4,0x15,0x92},
{0x0D,0x2A,0xAA,0xEC},
{0x67,0x34,0x02,0x9D},
{0xEA,0x32,0x26,0x51},
{0x1C,0xBF,0x33,0x4F},
{0xC1,0xBD,0x1A,0xBF},
{0x96,0x4E,0x26,0x96},
{0x96,0x90,0xFC,0xFE},
{0xE1,0x88,0xBF,0x56},
{0xF2,0x23,0x8F,0x6B},
{0x7B,0xBE,0x6D,0x57},
{0xC7,0xD0,0x04,0x4C},
{0xC9,0xF5,0x3A,0xFB},
{0xFE,0x72,0x80,0x29},
{0x4C,0x71,0x64,0x9D},
{0xED,0x57,0x84,0x5F},
{0xD7,0xBF,0xF6,0x96},
{0x46,0xC8,0xA0,0x7F},
{0xE6,0x1D,0x49,0xBA},
{0x91,0x38,0x82,0x56},
{0xCA,0xDB,0x9A,0x73},
{0xE4,0x29,0xFA,0xB4},
{0x02,0x80,0x7B,0x52},
{0xF7,0x4C,0xA2,0x50},
{0x14,0x18,0x71,0x71},
{0x99,0x55,0xF6,0xB9},
{0x2B,0xBC,0xB7,0xE7},
{0xB2,0xFD,0x9F,0xD4},
{0x94,0x56,0xD2,0x93},
{0xDE,0x52,0x3A,0x27},
{0xB9,0xBA,0x4D,0x0E},
{0x03,0x9A,0xF6,0x87},
{0x0E,0x56,0x6F,0xDD},
{0xE2,0x60,0x67,0xA8},
{0x13,0x4A,0x19,0xF3},
{0xFE,0x05,0x08,0xF1},
{0xD5,0x81,0xE5,0x0B},
{0x3D,0xD8,0x3E,0x36},
{0x31,0x0F,0xC9,0xC5},
{0xDD,0x00,0x04,0xAF},
{0x75,0xD4,0xA5,0x1C},
{0xD3,0x0D,0x67,0x9A},
{0xBA,0x6E,0xDA,0xDB},
{0xB1,0xAD,0x12,0x7C},
{0xE9,0x88,0xDE,0x35},
{0x57,0xB2,0xF8,0xA7},
{0xA0,0x64,0xDC,0x09},
{0x21,0xEE,0xEC,0x25},
{0x14,0xBC,0x89,0x5F},
{0x8B,0x4E,0x2E,0xA9},
{0xED,0x0B,0x0C,0x02},
{0x42,0x22,0x11,0x80},
{0xB0,0x6F,0xBE,0xD6},
{0x92,0x48,0x2E,0xBD},
{0xA4,0xCA,0x20,0x94},
{0xFA,0x3C,0xAC,0xD2},
{0x79,0x9E,0x74,0xE3},
{0x32,0xA8,0x3A,0xF1},
{0x1D,0x90,0xB0,0xD4},
{0x0E,0xBC,0x65,0xE3},
{0x51,0x30,0xB8,0x6B},
{0x32,0x03,0xA9,0x67},
{0xFA,0x3E,0xCF,0x26},
{0x51,0x70,0xE3,0xD0},
{0xFD,0x47,0xF0,0x2D},
{0x48,0xC6,0xB7,0xC7},
{0x27,0xE1,0xF7,0xBC},
{0x7B,0xD4,0x06,0x6D},
{0xD2,0xA5,0x27,0x0D},
{0x6A,0x0C,0xDF,0x42},
{0x80,0xB9,0x72,0xF2},
{0x67,0xDA,0x08,0xFE},
{0x66,0xC9,0xFB,0x44},
{0x62,0x26,0xEA,0x5D},
{0x66,0x54,0x18,0xA7},
{0x1C,0x90,0xEA,0xF1},
{0x62,0x9D,0x02,0xA8},
{0x90,0x03,0xA4,0xB2},
{0xFD,0x83,0x7D,0x4C},
{0xC9,0xAF,0xDE,0x17},
{0x29,0xCD,0xEA,0xBF},
{0x35,0x23,0xAE,0x5B},
{0x80,0x89,0x2F,0x25},
{0x6B,0x1A,0xA8,0xD7},
{0xB2,0x65,0x1A,0x66},
{0x53,0xA5,0x33,0x96},
{0xF2,0x84,0x85,0x2D},
{0x76,0x00,0x67,0x9C},
{0x09,0x8A,0x77,0x6F},
{0x3F,0xC3,0x07,0xFF},
{0x75,0xD6,0xCA,0x26},
{0xF0,0x2A,0x9C,0xE3},
{0xCC,0xE3,0xB5,0xB3},
{0x97,0x08,0x4B,0x19},
{0x9D,0x51,0x7C,0x06},
{0x09,0xCF,0x79,0xE0},
{0x20,0x41,0x61,0x24},
{0xDE,0xDA,0x04,0x88},
{0xE3,0x7C,0xB0,0x0A},
{0x89,0x3F,0x67,0x7E},
{0xEC,0xBB,0x5A,0xCE},
{0x77,0xF8,0xA3,0x97},
{0xB3,0x03,0x7F,0x8B},
{0xD5,0xFA,0x30,0x1F},
{0x38,0x7D,0x65,0x05},
{0xAF,0xE8,0x31,0x3E},
{0x32,0xF4,0x79,0xE6},
{0x1E,0x74,0x6E,0x5E},
{0x5F,0xFE,0xF1,0x11},
{0xD4,0x06,0xFE,0x28},
{0xBD,0x06,0x04,0x24},
{0xA6,0x41,0x5B,0x56},
{0xF6,0x24,0x5C,0x2C},
{0xB4,0x94,0x69,0x1B},
{0x48,0x32,0x69,0x01},
{0xF8,0x06,0x37,0x8E},
{0xEC,0x76,0xE6,0x68}
};

