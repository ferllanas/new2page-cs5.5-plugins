//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/snippetrunner/SnipRunControlView.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#include "ErasablePanelView.h"

// Plug-in includes
#include "SnipRunID.h"
#include "SnipRunLog.h"


/** Overrides the ConstrainDimensions to control the maximum and minimum width 
	and height of panel when it is resized.

	@ingroup snippetrunner
	
*/
class  SnipRunControlView : public ErasablePanelView
{
	public:
		/** 
			Constructor.
		*/
		SnipRunControlView(IPMUnknown* boss) 
			: ErasablePanelView(boss) {}

		/** 
			Destructor.
		*/
		virtual ~SnipRunControlView() {}

		/** Allows the panel size to be constrained.  
			@param dimensions OUT specifies the maximum and minimum width and height of the panel
				when it is resized.
		*/
		virtual PMPoint	ConstrainDimensions(const PMPoint& dimensions) const;

		/**	Clear the SnippetRunner framework log when resizing. 
			The multi line widget log gives some odd behaviour if we don't. 
			Means you lose the log contents when you resize.
			@param newDimensions
			@param invalidate
		*/
		virtual  void Resize(const PMPoint& newDimensions, bool16 invalidate);

	private:

		static const int kMinimumPanelWidth;
		static const int kMaximumPanelWidth;
		static const int kMinimumPanelHeight;
		static const int kMaximumPanelHeight;
};

// define the max/min width and height for the panel
const int	SnipRunControlView::kMinimumPanelWidth	=	207;
const int 	SnipRunControlView::kMaximumPanelWidth	=	500;
const int 	SnipRunControlView::kMinimumPanelHeight	=	290;
const int 	SnipRunControlView::kMaximumPanelHeight	=	600;


/* Make the implementation available to the application.
*/
CREATE_PERSIST_PMINTERFACE(SnipRunControlView, kSnipRunControlViewImpl)


/* Allows the panel size to be constrained.  
*/
PMPoint SnipRunControlView::ConstrainDimensions(const PMPoint& dimensions) const
{
	PMPoint constrainedDim = dimensions;

	// Width can vary if not above maximum or below minimum
	if(constrainedDim.X() > kMaximumPanelWidth)
	{
		constrainedDim.X(kMaximumPanelWidth);
	}
	else if(constrainedDim.X() < kMinimumPanelWidth)
	{
		constrainedDim.X(kMinimumPanelWidth);
	}

	// Height can vary if not above maximum or below minimum
	if(constrainedDim.Y() > kMaximumPanelHeight)
	{
		constrainedDim.Y(kMaximumPanelHeight);
	}
	else if(constrainedDim.Y() < kMinimumPanelHeight)
	{
		constrainedDim.Y(kMinimumPanelHeight);
	}

	return constrainedDim;
}

/*
*/
void SnipRunControlView::Resize(const PMPoint& newDimensions, bool16 invalidate)
{
	// Clear the log during resize to workaround the fact that
	// if we don't the static multiline widget gets messed up.
	// SnipRunLog will arrange that the contents of the log get
	// restored once resize is complete.
	if (SnipRunLog::PeekAtSnipRunLog() != nil) {
		SnipRunLog::Instance()->Resize();
	}

	ErasablePanelView::Resize(newDimensions, invalidate);

}

// End SnipRunControlView.cpp

