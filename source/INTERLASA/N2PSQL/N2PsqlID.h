//========================================================================================
//  
//  $File: $
//  
//  Owner: Fernando Llanas
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PsqlID_h__
#define __N2PsqlID_h__

#include "SDKDef.h"

// Company:
#define kN2PsqlCompanyKey	"INTERLASA"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2PsqlCompanyValue	"INTERLASA"	// Company name displayed externally.

// Plug-in:
#define kN2PsqlPluginName	"News2Page"			// Name of this plug-in.
#define kN2PsqlPrefixNumber	0x96F00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2PsqlVersion		"v1.0.8.6.2"						// Version of this plug-in (for the About Box).

/*
 Version v1.0.8.6
	Mejoramiento llamado a preferencias.
 Version v1.0.8.6.1
	Se gregaron dos nuevas preferencias.
		-N2PLinkWebServicesWordPress Variable que almacena el link del servicio web 
		-N2PCkBoxSendWordPress Variable para indicar si debe o no utilizar el webservices de WordPress
 */
#define kN2PsqlAuthor		"Interlasa.com S.A. de C.V."					// Author of this plug-in (for the About Box).
#define kNews2PageActionArea	"News2Page"

// Plug-in Prefix: (please change kN2PsqlPrefixNumber above to modify the prefix.)
#define kN2PsqlPrefix		RezLong(kN2PsqlPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2PsqlStringPrefix	SDK_DEF_STRINGIZE(kN2PsqlPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2PsqlMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2PsqlMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2PSQLPluginID, kN2PsqlPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kN2PsqlActionComponentBoss,						kN2PsqlPrefix + 0)
DECLARE_PMID(kClassIDSpace, kN2PsqlStringRegisterBoss,						kN2PsqlPrefix + 1)
DECLARE_PMID(kClassIDSpace, kN2PsqlMenuRegisterBoss,						kN2PsqlPrefix + 2)
DECLARE_PMID(kClassIDSpace, kN2PsqlActionRegisterBoss,						kN2PsqlPrefix + 3)
DECLARE_PMID(kClassIDSpace, kN2PsqlPanelWidgetBoss,							kN2PsqlPrefix + 4)
DECLARE_PMID(kClassIDSpace, kN2PsqlPanelRegisterBoss,						kN2PsqlPrefix + 5)

DECLARE_PMID(kClassIDSpace, kN2PsqlDNDTgtDropTargetTextWidgetBoss,			kN2PsqlPrefix + 6)
DECLARE_PMID(kClassIDSpace, kN2PsqlDNDTgtDropTextEditWidgetBoss,			kN2PsqlPrefix + 7)

DECLARE_PMID(kClassIDSpace, kN2PsqlListaResultadosWidgetBoss,				kN2PsqlPrefix + 8)
DECLARE_PMID(kClassIDSpace, kN2PsqlBusquedaDialogBoss,						kN2PsqlPrefix + 9)

DECLARE_PMID(kClassIDSpace, kN2PsqlCFTHScrapHandlerBoss,					kN2PsqlPrefix + 10)
DECLARE_PMID(kClassIDSpace, kN2PsqlCFTHCustFlavHlprBoss,					kN2PsqlPrefix + 11)
DECLARE_PMID(kClassIDSpace, kN2PsqlDocWchResponderServiceBoss,				kN2PsqlPrefix + 12)

DECLARE_PMID(kClassIDSpace, kN2PsqlUpdateStorysAndDBUtilitiesBoss,			kN2PsqlPrefix + 13)
DECLARE_PMID(kClassIDSpace, kUpdateStorysAndDBUtilitiesBoss,				kN2PsqlPrefix + 14)
DECLARE_PMID(kClassIDSpace, kN2PSQLPreferenciasDialogBoss,					kN2PsqlPrefix + 15)
DECLARE_PMID(kClassIDSpace, kN2PSQLNonBrokenPasswordEditBoxWidgetBoss,		kN2PsqlPrefix + 16)
DECLARE_PMID(kClassIDSpace, kN2PSQLSetLockAttributeStoryCmdBoss,			kN2PsqlPrefix + 17)
DECLARE_PMID(kClassIDSpace, kN2PsqlCheckInArticleDialogBoss,				kN2PsqlPrefix + 18)
DECLARE_PMID(kClassIDSpace, kN2PSQLDNDCustomFlavorHelperBoss,				kN2PsqlPrefix + 19)
DECLARE_PMID(kClassIDSpace, kN2PsqlListaResultadosPasesWidgetBoss,			kN2PsqlPrefix + 20)


//
DECLARE_PMID(kClassIDSpace, kN2PsqlListPhotoPanelWidgetBoss,				kN2PsqlPrefix + 21)
DECLARE_PMID(kClassIDSpace, kN2PPhotoCViewPanelWidgetBoss,					kN2PsqlPrefix + 22)
DECLARE_PMID(kClassIDSpace, kN2PsqlPhotoListWidgetBoss,						kN2PsqlPrefix + 23)

DECLARE_PMID(kClassIDSpace, kN2PSQLRollOverIconCalButtonObserverBoss,		kN2PsqlPrefix + 24)

DECLARE_PMID(kClassIDSpace, kN2PSQLDNDStaticMultiLineTextWidgetBoss,		kN2PsqlPrefix + 25)
DECLARE_PMID(kClassIDSpace, kN2PSQCheckBoxObserverBoss,						kN2PsqlPrefix + 26)
DECLARE_PMID(kClassIDSpace, kN2PsqlDetachArticleDialogBoss,						kN2PsqlPrefix + 27)
DECLARE_PMID(kClassIDSpace, kN2PsqlCrearNotaHijaDialogBoss,						kN2PsqlPrefix + 28)
DECLARE_PMID(kClassIDSpace, kN2PSQLDocResponderBoss,						kN2PsqlPrefix + 29)
DECLARE_PMID(kClassIDSpace, kN2PSQLCommandInterceptorBoss,						kN2PsqlPrefix + 30)
DECLARE_PMID(kClassIDSpace, kN2PSQLDebugLogBoss,						kN2PsqlPrefix + 31)
DECLARE_PMID(kClassIDSpace, kN2PSQLRollOverIconPieFotoButtonObserverBoss,		kN2PsqlPrefix + 32)

DECLARE_PMID(kClassIDSpace, kN2PSQLSlDlgDialogBoss,		kN2PsqlPrefix + 33)
DECLARE_PMID(kClassIDSpace, kBscSlDlgTreeViewWidgetBoss,		kN2PsqlPrefix + 34)
DECLARE_PMID(kClassIDSpace, kYinPanelBoss,		kN2PsqlPrefix + 35)
DECLARE_PMID(kClassIDSpace, kYangPanelBoss,		kN2PsqlPrefix + 36)
DECLARE_PMID(kClassIDSpace, kN2PHemerotecaPanelBoss,		kN2PsqlPrefix + 37)
DECLARE_PMID(kClassIDSpace, kN2PGeneral2PanelBoss,		kN2PsqlPrefix + 38)
DECLARE_PMID(kClassIDSpace, kN2PDirectSeleccionWidgetBoss,		kN2PsqlPrefix + 39)

// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PsqlWIDGETOBSERVER,						kN2PsqlPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PsqlSELECTIONOBSERVER,					kN2PsqlPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PsqlUpdateStorysAndDBUtilities,			kN2PsqlPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PSQLInspeccionaNotasTask,				kN2PsqlPrefix + 3)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PSQLFotoPageTask,						kN2PsqlPrefix + 4)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PPhotoWIDGETOBSERVER,					kN2PsqlPrefix + 5)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PSQLLOGOFFTASK,						kN2PsqlPrefix + 6)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PNOTESUTILS,						kN2PsqlPrefix + 7)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PSQL_DOCUMENT_OBSERVER,						kN2PsqlPrefix + 8)
DECLARE_PMID(kInterfaceIDSpace, IID_IGTTXTEDTSTORYCREATEDELETEOBSERVER,						kN2PsqlPrefix + 9)
DECLARE_PMID(kInterfaceIDSpace, IID_IGTTXTEDTSNAPSHOTINTERFACE,						kN2PsqlPrefix + 10)
DECLARE_PMID(kInterfaceIDSpace, IID_IN2PSQLDebugLogINTERFACE,						kN2PsqlPrefix + 11)

// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kN2PsqlActionComponentImpl,				kN2PsqlPrefix + 0)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlSelectionObserverImpl,				kN2PsqlPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlWidgetObserverImpl,					kN2PsqlPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlDNDTgtDrawTargetImpl,				kN2PsqlPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlListaResultadoBoxObserverImpl,		kN2PsqlPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlBusquedaDialogControllerImpl,		kN2PsqlPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlBusquedaDialogObserverImpl,			kN2PsqlPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlExPDFCustFlavHlprImpl,				kN2PsqlPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlExPDFDEHandlerImpl,					kN2PsqlPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlExPDFEHImpl,						kN2PsqlPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlDocWchServiceProviderImpl,			kN2PsqlPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlDocWchResponderImpl,				kN2PsqlPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlUpdateStorysAndDBUtilisImpl,		kN2PsqlPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLPreferenciasDialogControllerImpl,	kN2PsqlPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLPreferenciasDialogObserverImpl,		kN2PsqlPrefix + 16)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLNonBrokenPasswordEventHandlerImpl,	kN2PsqlPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kN2PSQLSetLockAttributeCmdImpl,			kN2PsqlPrefix + 18)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLSetLockDataImpl,					kN2PsqlPrefix + 19)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLInspeccionaNotasTaskImpl,			kN2PsqlPrefix + 20)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLFotoPageTaskImpl,					kN2PsqlPrefix + 21)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlCheckInArticleDialogControllerImpl,	kN2PsqlPrefix + 22)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlCheckInArticleDialogObserverImpl,	kN2PsqlPrefix + 23)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlListaResultadoPasesBoxObserverImpl,	kN2PsqlPrefix + 24)
DECLARE_PMID(kImplementationIDSpace, kN2PPhotoWidgetObserverImpl,				kN2PsqlPrefix + 25)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlPhotoListBoxObserverImpl,			kN2PsqlPrefix + 26)
DECLARE_PMID(kImplementationIDSpace, kN2PPhotoCustomViewImpl,					kN2PsqlPrefix + 27)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLDNDDragSourcePreviewImpl,			kN2PsqlPrefix + 28)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLCustomFlavorHelperImpl,				kN2PsqlPrefix + 29)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLDNDSourceSourceImpl,				kN2PsqlPrefix + 30)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLRollOverCalButtonObserverImpl,		kN2PsqlPrefix + 31)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLDNDDropTargetImpl,					kN2PsqlPrefix + 32)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLLogOffTaskImpl,						kN2PsqlPrefix + 33)
DECLARE_PMID(kImplementationIDSpace, kN2PNotesUtilsImpl,						kN2PsqlPrefix + 34)
DECLARE_PMID(kImplementationIDSpace, kN2PSQCheckBoxObserverImpl,				kN2PsqlPrefix + 35)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlDetachArticleDialogControllerImpl,	kN2PsqlPrefix + 36)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlDetachArticleDialogObserverImpl,	kN2PsqlPrefix + 37)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlCrearNotaHijaDialogControllerImpl,	kN2PsqlPrefix + 38)
DECLARE_PMID(kImplementationIDSpace, kN2PsqlCrearNotaHijaDialogObserverImpl,	kN2PsqlPrefix + 39)
DECLARE_PMID(kImplementationIDSpace, kN2PSQL_Document_ObserverImpl,				kN2PsqlPrefix + 40)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLDocServiceProviderImpl,				kN2PsqlPrefix + 41)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLDocResponderImpl,					kN2PsqlPrefix + 42)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLCommandInterceptorImpl,					kN2PsqlPrefix + 43)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLStartupShutdownImpl,					kN2PsqlPrefix + 44)
DECLARE_PMID(kImplementationIDSpace, kGTTxtEdtNewDeleteStoryObserverImpl,					kN2PsqlPrefix + 45)
DECLARE_PMID(kImplementationIDSpace, kGTTxtEdtSnapshotInterfaceImpl,					kN2PsqlPrefix + 46)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLDebugLogImpl,					kN2PsqlPrefix + 47)
DECLARE_PMID(kImplementationIDSpace, kN2PSQLRollOverIconPieFotoButtonObserverImpl,		kN2PsqlPrefix + 48)

DECLARE_PMID(kImplementationIDSpace, kBscSlDlgDialogCreatorImpl,		kN2PsqlPrefix + 49)
DECLARE_PMID(kImplementationIDSpace, kBscSlDlgDialogObserverImpl,		kN2PsqlPrefix + 50)
DECLARE_PMID(kImplementationIDSpace, kBscSlDlgTreeViewDlgSwitcherImpl,		kN2PsqlPrefix + 51)
DECLARE_PMID(kImplementationIDSpace, kBscSlDlgTreeViewWidgetMgrImpl,		kN2PsqlPrefix + 52)
DECLARE_PMID(kImplementationIDSpace, kBscSlDlgTreeViewAdapterImpl,		kN2PsqlPrefix + 53)
DECLARE_PMID(kImplementationIDSpace, kYinPanelCreatorImpl,		kN2PsqlPrefix + 54)
DECLARE_PMID(kImplementationIDSpace, kYangPanelCreatorImpl,		kN2PsqlPrefix + 55)
DECLARE_PMID(kImplementationIDSpace, kN2PHemerotecaPanelCreatorImpl,		kN2PsqlPrefix + 56)
DECLARE_PMID(kImplementationIDSpace, kN2PGeneral2PanelCreatorImpl,		kN2PsqlPrefix + 57)
DECLARE_PMID(kImplementationIDSpace, kN2PDirectSelectionObserverSuiteImpl,		kN2PsqlPrefix + 58)


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kN2PsqlLogInActionID,						kN2PsqlPrefix + 0)
DECLARE_PMID(kActionIDSpace, kN2PsqlLogOutActionID,						kN2PsqlPrefix + 1)
DECLARE_PMID(kActionIDSpace, kN2PsqlAboutActionID,						kN2PsqlPrefix + 2)
DECLARE_PMID(kActionIDSpace, kN2PsqlDBPanelWidgetActionID,				kN2PsqlPrefix + 3)
DECLARE_PMID(kActionIDSpace, kN2PsqlDBSeparator1ActionID,				kN2PsqlPrefix + 4)
DECLARE_PMID(kActionIDSpace, kN2PsqlDBPopupAboutThisActionID,			kN2PsqlPrefix + 5)
DECLARE_PMID(kActionIDSpace, kN2PsqlDBItemOneActionID,					kN2PsqlPrefix + 11)
DECLARE_PMID(kActionIDSpace, kN2PsqlDepositarPaginaActionID,					kN2PsqlPrefix + 12)
DECLARE_PMID(kActionIDSpace, kN2PsqlRetirarPaginaActionID,					kN2PsqlPrefix + 13)
DECLARE_PMID(kActionIDSpace, kN2PsqlSalvarActionID,						kN2PsqlPrefix + 14)
DECLARE_PMID(kActionIDSpace, kN2PsqlDepInCopyActionID,					kN2PsqlPrefix + 15)
DECLARE_PMID(kActionIDSpace, kN2PSQLPreferenciasActionID,				kN2PsqlPrefix + 16)
DECLARE_PMID(kActionIDSpace, kN2PSQLCheckOutNotaActionID,				kN2PsqlPrefix + 17)
DECLARE_PMID(kActionIDSpace, kN2PSQLCheckInNotaActionID,				kN2PsqlPrefix + 18)
DECLARE_PMID(kActionIDSpace, kN2PSQLImportNotaToActServerActionID,		kN2PsqlPrefix + 19)
DECLARE_PMID(kActionIDSpace, kN2PSQLDetachNotaActionID,					kN2PsqlPrefix + 20)
DECLARE_PMID(kActionIDSpace, kN2PSQLCrearSpaceAndNotaActionID,			kN2PsqlPrefix + 21)
DECLARE_PMID(kActionIDSpace, kN2PSQLCrearPaseDeNotaActionID,			kN2PsqlPrefix + 22)
DECLARE_PMID(kActionIDSpace, kN2PsqlDBSeparator3ActionID,				kN2PsqlPrefix + 31)
DECLARE_PMID(kActionIDSpace, kN2PsqlDBSeparator2ActionID,				kN2PsqlPrefix + 41)
DECLARE_PMID(kActionIDSpace, kN2PsqlDBSeparator4ActionID,				kN2PsqlPrefix + 51)
DECLARE_PMID(kActionIDSpace, kN2PsqlListPhotoPanelWidgetActionID,		kN2PsqlPrefix + 45)
DECLARE_PMID(kActionIDSpace, kN2PsqlDetachPhotoActionID,		kN2PsqlPrefix + 46)
DECLARE_PMID(kActionIDSpace, kN2PsqlBuscarNotasDPaginaActionID,		kN2PsqlPrefix + 47)
DECLARE_PMID(kActionIDSpace, kN2PsqlBuscarNotasActionID,		kN2PsqlPrefix + 48)
DECLARE_PMID(kActionIDSpace, kN2PsqlActualizarTodoActionID,		kN2PsqlPrefix + 49)
DECLARE_PMID(kActionIDSpace, kN2PSQLLigarImagensANotaActionID,		kN2PsqlPrefix + 50)
DECLARE_PMID(kActionIDSpace, kN2PSQLAceptarImagenesDNotaActionID,		kN2PsqlPrefix + 51)
DECLARE_PMID(kActionIDSpace, kN2PSQLDuplicarPaginaActionID,		kN2PsqlPrefix + 52)
DECLARE_PMID(kActionIDSpace, kN2PsqlDetachPaginaActionID,		kN2PsqlPrefix + 53)
DECLARE_PMID(kActionIDSpace, kN2PsqlCreatePieDFotoActionID,		kN2PsqlPrefix + 54)
DECLARE_PMID(kActionIDSpace, kN2PSQLOpenDialogIssuesActionID,		kN2PsqlPrefix + 55)




// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kN2PsqlPanelWidgetID,							kN2PsqlPrefix + 0) 
// TextEdits
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextPageWidgetID,						kN2PsqlPrefix + 1) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextTituloWidgetID,						kN2PsqlPrefix + 2) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextBalazoWidgetID,						kN2PsqlPrefix + 3) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextNotaWidgetID,						kN2PsqlPrefix + 4) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextPieWidgetID,						kN2PsqlPrefix + 5) 

DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextFechaWidgetID,						kN2PsqlPrefix + 6) 
//ComboBox
DECLARE_PMID(kWidgetIDSpace, kN2PsqlComboPubliWidgetID,						kN2PsqlPrefix + 7) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlComboSeccionWidgetID,					kN2PsqlPrefix + 8) 

//botones
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonBDWidgetID,						kN2PsqlPrefix + 9) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonPDFWidgetID,						kN2PsqlPrefix + 10) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonBorrarTextoWidgetID,				kN2PsqlPrefix + 11) 

//staticsTexts
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextTituloWidgetID,					kN2PsqlPrefix + 12) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextNotaWidgetID,						kN2PsqlPrefix + 13) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextPieWidgetID,						kN2PsqlPrefix + 14) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextBalazoWidgetID,					kN2PsqlPrefix + 15) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStcTextPageWidgetID,					kN2PsqlPrefix + 16) 

DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextFechaWidgetID,					kN2PsqlPrefix + 17) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextSeccionWidgetID,					kN2PsqlPrefix + 18) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextPubliWidgetID,					kN2PsqlPrefix + 19) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlLogoInterlasaWidgetID,					kN2PsqlPrefix + 20) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextStatusWidgetID,					kN2PsqlPrefix + 21) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextStatusWidgetID,						kN2PsqlPrefix + 22) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonBuscarWidgetID,					kN2PsqlPrefix + 23) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBscDNDSrcStaticTextWidgetID,			kN2PsqlPrefix + 24) 

DECLARE_PMID(kWidgetIDSpace, kN2PsqlNotasListBoxWidgetID,				kN2PsqlPrefix + 25) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBusquedaParentWidgetID,					kN2PsqlPrefix + 26) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlNotaLabelWidgetID,						kN2PsqlPrefix + 27) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlSeccionLabelWidgetID,					kN2PsqlPrefix + 28) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlEstadoLabelWidgetID,					kN2PsqlPrefix + 29) 
DECLARE_PMID(kWidgetIDSpace, kN2PsqlDialogBusquedaWidgetID,					kN2PsqlPrefix + 30) 

DECLARE_PMID(kWidgetIDSpace, kN2PsqlCmpListElementPenWidgetID,				kN2PsqlPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlPanelVistaPreviaNotaWidgetID,			kN2PsqlPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStaticMultiLineNotaWidgetID,			kN2PsqlPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStMultiLineNotaScrollBarWidgetID,		kN2PsqlPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlLogoWidgetID,							kN2PsqlPrefix + 35)

DECLARE_PMID(kWidgetIDSpace, kN2PsqlText_ID_Elem_PadreNotaWidgetID,						kN2PsqlPrefix + 36)

DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextPruebaWidgetID,					kN2PsqlPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextPrueba2WidgetID,					kN2PsqlPrefix + 38)

DECLARE_PMID(kWidgetIDSpace, kN2PsqlListUpdateWidgetID,						kN2PsqlPrefix + 39)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlIDNotaLabelWidgetID,					kN2PsqlPrefix + 40)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlItemToSearchLabelWidgetID,				kN2PsqlPrefix + 41)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlUIDTextModelLabelWidgetID,				kN2PsqlPrefix + 42)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonUpdateDBToTextFrameWidgetID,		kN2PsqlPrefix + 43)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonUpdateTextToDBFrameWidgetID,		kN2PsqlPrefix + 44)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextRoutedToWidgetID,					kN2PsqlPrefix + 45)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlComboRoutedToWidgetID,					kN2PsqlPrefix + 46)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBusquedaTextWidgetID,					kN2PsqlPrefix + 47)
DECLARE_PMID(kWidgetIDSpace, kBotonRefreshSatusNotaWidgetID,				kN2PsqlPrefix + 48)


DECLARE_PMID(kWidgetIDSpace, kN2PSQLPreferenciasDialogWidgetID,				kN2PsqlPrefix + 49)
//DECLARE_PMID(kWidgetIDSpace, kN2PSQLIPServerOfDBEditWidgetID,				kN2PsqlPrefix + 50)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkSQLOptionEditWidgetID,				kN2PsqlPrefix + 51)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLFileMAkerOptionEditWidgetID,			kN2PsqlPrefix + 52)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLNameOfDBEditWidgetID,					kN2PsqlPrefix + 53)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLDSNNameEditWidgetID,					kN2PsqlPrefix + 54)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLUsuarioOfDBEditWidgetID,				kN2PsqlPrefix + 56)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLPassswordOfDBWidgetID,					kN2PsqlPrefix + 57)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLGropPanelDBOptionsWidgetID,				kN2PsqlPrefix + 58)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlDataBaseComboWidgetID,					kN2PsqlPrefix + 59)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlNewDBConexionButtonWidgetID,			kN2PsqlPrefix + 60)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlDeleteDBConexionButtonWidgetID,			kN2PsqlPrefix + 61)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlSetLocalDBConexionButtonWidgetID,		kN2PsqlPrefix + 62)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonRefreshStatusNotaWidgetID,			kN2PsqlPrefix + 63)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlGuardarDBConexionButtonWidgetID,		kN2PsqlPrefix + 64)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlCancelarDBConexionButtonWidgetID,		kN2PsqlPrefix + 65)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLNameConectionDBWidgetID,				kN2PsqlPrefix + 66)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTestConectionButtonWidgetID,			kN2PsqlPrefix + 67)
DECLARE_PMID(kWidgetIDSpace, kN2PSqlLabelNameOfConectionWidgetID,			kN2PsqlPrefix + 68)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlEditDBConexionButtonWidgetID,			kN2PsqlPrefix + 69)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlNoteIsLocketTextEditWidgetID,			kN2PsqlPrefix + 70)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxShowEdgesEditWidgetID,				kN2PsqlPrefix + 71)

DECLARE_PMID(kWidgetIDSpace, kN2PSQLUsernameLogedWidgetID,					kN2PsqlPrefix + 72)

DECLARE_PMID(kWidgetIDSpace, kN2PsqlLastCheckInNoteTextEditWidgetID,		kN2PsqlPrefix + 73)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonUpdateMakeDBNotasTextFrameWidgetID,kN2PsqlPrefix + 74)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlAlertActualizarNotasWidgetID,			kN2PsqlPrefix + 75)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlCheckInArticleDlgWidgetID,				kN2PsqlPrefix + 76)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlDlgChekInNotaCancelWidgetID,				kN2PsqlPrefix + 77)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStcTextInCopyInDesingWidgetID,				kN2PsqlPrefix + 78)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStcTextDBConectionWidgetID,				kN2PsqlPrefix + 79)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlInCopyPctureWidgetID,				kN2PsqlPrefix + 80)

DECLARE_PMID(kWidgetIDSpace, kN2PsqlText_ID_Elem_Pie_WidgetID,				kN2PsqlPrefix + 81)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlText_ID_Elem_ContentNota_WidgetID,		kN2PsqlPrefix + 82)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlText_ID_Elem_Balazo_WidgetID,		kN2PsqlPrefix + 83)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlText_ID_Elem_Titulo_WidgetID,		kN2PsqlPrefix + 84)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxShowTxtOversetEditWidgetID,		kN2PsqlPrefix + 85)



DECLARE_PMID(kWidgetIDSpace, kN2PsqlPhotoListBoxWidgetID,		kN2PsqlPrefix + 86)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlPhotoParentWidgetID,		kN2PsqlPrefix + 87)
DECLARE_PMID(kWidgetIDSpace, kN2PPhotoCustomPanelViewWidgetID,		kN2PsqlPrefix + 88)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlPhotoPanelWidgetID,		kN2PsqlPrefix + 89)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlIdElePhotoLabelWidgetID,		kN2PsqlPrefix + 90)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlNombrePhotoLabelWidgetID,		kN2PsqlPrefix + 91)

DECLARE_PMID(kWidgetIDSpace, kN2PsqlTituloDeLaPhotoLabelWidgetID,		kN2PsqlPrefix + 92)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlDescrAutorPhotoLabelWidgetID,		kN2PsqlPrefix + 93)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlAutorPhotoLabelWidgetID,		kN2PsqlPrefix + 94)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlDescripcionPhotoLabelWidgetID,		kN2PsqlPrefix + 95)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTituloPhotoLabelWidgetID,		kN2PsqlPrefix + 96)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlAlertNotaConFotoWidgetID,		kN2PsqlPrefix + 97)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlIdPadreElePhotoLabelWidgetID,		kN2PsqlPrefix + 98)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStatusPhotoLabelWidgetID,		kN2PsqlPrefix + 99)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlFechaUltModPhotoLabelWidgetID,		kN2PsqlPrefix + 100)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLPathOfServerFileEditWidgetID,		kN2PsqlPrefix + 101)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlSelectPathFileServerButtonWidgetID,		kN2PsqlPrefix + 102)
//DECLARE_PMID(kWidgetIDSpace, kN2PsqlDetachNotaWidgetID,		kN2PsqlPrefix + 103)

DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextTituloTextWidgetID,		kN2PsqlPrefix + 104)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextBalazoTextWidgetID,		kN2PsqlPrefix + 105)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextNotaTextWidgetID,		kN2PsqlPrefix + 106)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextPieTextWidgetID,		kN2PsqlPrefix + 107)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonCancelNewNoteWidgetID,		kN2PsqlPrefix + 108)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonSubirNewNoteWidgetID,		kN2PsqlPrefix + 109)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextArticlesTexWidgetID,		kN2PsqlPrefix + 110)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextStatusComboWidgetID,		kN2PsqlPrefix + 111)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStcThisArticleTextWidgetID,		kN2PsqlPrefix + 112)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxOnlyPageJumpsWidgetID,		kN2PsqlPrefix + 113)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlPasesListBoxWidgetID,					kN2PsqlPrefix + 114)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlPasesListParentWidgetID,				kN2PsqlPrefix + 115)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlIDPaseLabelWidgetID,					kN2PsqlPrefix + 116)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlContenidoPaselLabelWidgetID,			kN2PsqlPrefix + 117)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlIDPadrePaseLabelWidgetID,				kN2PsqlPrefix + 118)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlUIDRefTexModelDeTitulo_WidgetID,		kN2PsqlPrefix + 119)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlUIDRefTexModelDePieFoto_WidgetID,		kN2PsqlPrefix + 120)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlUIDRefTexModelDeNota_WidgetID,			kN2PsqlPrefix + 121)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlUIDRefTexModelDeBalazo_WidgetID,		kN2PsqlPrefix + 122)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStTextCantFotosDeNotaWidgetID,			kN2PsqlPrefix + 123)
DECLARE_PMID(kWidgetIDSpace, KN2PSQLTimeToCheckUpdateElemsWidgetID,			kN2PsqlPrefix + 124)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxPhotoActivateWidgetID,				kN2PsqlPrefix + 125)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxPhotoListaWidgetID,				kN2PsqlPrefix + 126)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLPathOfServerFileImagenesEditWidgetID,	kN2PsqlPrefix + 127)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLRutaDServidorRespNotasEditWidgetID,		kN2PsqlPrefix + 128)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlDetachArticleDlgWidgetID,				kN2PsqlPrefix + 129)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlTextEnGuiaWidgetID,						kN2PsqlPrefix + 130)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlCrearNotaHijaDlgWidgetID,				kN2PsqlPrefix + 131)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxExportNotasATablaWEBWidgetID,		kN2PsqlPrefix + 132)
DECLARE_PMID(kWidgetIDSpace, KN2PSQLEstatusParaExportarATWEBWidgetID,			kN2PsqlPrefix + 133)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlUltimoUsuarioQModNotaWidgetID,			kN2PsqlPrefix + 134)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxMuestraDirigidoAEnCajasDNotasWidgetID,			kN2PsqlPrefix + 135)

DECLARE_PMID(kWidgetIDSpace, KN2PSQLNombreEstatusParaNotasCompletasWidgetID,			kN2PsqlPrefix + 136)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxExportarPaginaComoPDFWidgetID,			kN2PsqlPrefix + 137)
DECLARE_PMID(kWidgetIDSpace, KN2PSQLRutaDePaginaComoPDFWidgetID,			kN2PsqlPrefix + 138)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxBackUpPaginasEnCheckInWidgetID,			kN2PsqlPrefix + 139)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxEsCampoGuiaObligatorioWidgetID,			kN2PsqlPrefix + 140)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxEsEnviarNotasHijasAWEBWidgetID,			kN2PsqlPrefix + 141)

DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoWidgetID,			kN2PsqlPrefix + 142)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxEnviaAWEBSinRetornosTituloWidgetID,			kN2PsqlPrefix + 143)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlExportPDFPresetsDropDownWidgetID,			kN2PsqlPrefix + 144)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlExportPDFPresetsEditWEBDropDownWidgetID,			kN2PsqlPrefix + 145)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxIsExportPDFPresetsWidgetID,			kN2PsqlPrefix + 146)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLCkBoxIsExportPDFPresetsEditWEBWidgetID,			kN2PsqlPrefix + 147)
DECLARE_PMID(kWidgetIDSpace, KN2PSQLRutaDePaginaComoPDFWEBWidgetID,			kN2PsqlPrefix + 148)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlStcTextActualizaPagsWidgetID,			kN2PsqlPrefix + 149)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLnameParaStyleCreditoWidgetID,			kN2PsqlPrefix + 150)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLImagesFldrInSrvrFileImagesEditWidgetID,			kN2PsqlPrefix + 151)


DECLARE_PMID(kWidgetIDSpace, kBscSlDlgDialogWidgetID,								kN2PsqlPrefix + 152)
DECLARE_PMID(kWidgetIDSpace, kBscSlDlgIconSuiteWidgetID,							kN2PsqlPrefix + 153)
DECLARE_PMID(kWidgetIDSpace, kYinPanelWidgetID,										kN2PsqlPrefix + 159)
DECLARE_PMID(kWidgetIDSpace, kYinPanelStaticTextWidgetID,							kN2PsqlPrefix + 154)
DECLARE_PMID(kWidgetIDSpace, kYangPanelWidgetID,									kN2PsqlPrefix + 155)
DECLARE_PMID(kWidgetIDSpace, kYangPanelStaticTextWidgetID,							kN2PsqlPrefix + 156)
DECLARE_PMID(kWidgetIDSpace, kBscSlDlgTreeNodeWidgetID,								kN2PsqlPrefix + 157)
DECLARE_PMID(kWidgetIDSpace, kBscSlDlgTreeNodeNameWidgetID,							kN2PsqlPrefix + 158)
DECLARE_PMID(kWidgetIDSpace, kN2PHemerotecaPanelWidgetID,							kN2PsqlPrefix + 160)
DECLARE_PMID(kWidgetIDSpace, kN2PHmeDSNUserLoginWidgetID,							kN2PsqlPrefix + 161)
DECLARE_PMID(kWidgetIDSpace, kN2PHmeDSNPwdLoginWidgetID,								kN2PsqlPrefix + 162)
DECLARE_PMID(kWidgetIDSpace, kN2PHmeFTPServerWidgetID,								kN2PsqlPrefix + 163)
DECLARE_PMID(kWidgetIDSpace, kN2PHmeFTPUserWidgetID,								kN2PsqlPrefix + 164)
DECLARE_PMID(kWidgetIDSpace, kN2PHmeFTPPwdWidgetID,									kN2PsqlPrefix + 165)
DECLARE_PMID(kWidgetIDSpace, kN2PHmeFTPFolderWidgetID,								kN2PsqlPrefix + 166)
DECLARE_PMID(kWidgetIDSpace, kN2PHmeDSNNameWidgetID,								kN2PsqlPrefix + 167)
DECLARE_PMID(kWidgetIDSpace, kN2PHmteSendToHmcaWidgetID,								kN2PsqlPrefix + 168)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLURLWebServicesEditWidgetID,								kN2PsqlPrefix + 169)
DECLARE_PMID(kWidgetIDSpace, kN2PGeneral2PanelWidgetID,								kN2PsqlPrefix + 170)
DECLARE_PMID(kWidgetIDSpace, kN2PResizePreviewViewWidgetID,								kN2PsqlPrefix + 171)
DECLARE_PMID(kWidgetIDSpace, kN2PResizeHeightWidgetID,								kN2PsqlPrefix + 172)
DECLARE_PMID(kWidgetIDSpace, kN2PResizeWidthWidgetID,								kN2PsqlPrefix + 173)
DECLARE_PMID(kWidgetIDSpace, kN2PUsarGuiasWidgetID,								kN2PsqlPrefix + 174)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlComboguiaWidgetID,								kN2PsqlPrefix + 175)
DECLARE_PMID(kWidgetIDSpace, kN2PDirectButtonSeleccionWidgetID,						kN2PsqlPrefix + 176)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlEsActualizacionInCopyStatusWidgetID,			kN2PsqlPrefix + 177)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlBotonActualizarDisenioWidgetID,			kN2PsqlPrefix + 178)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlAlertActualizarDisenioWidgetID,			kN2PsqlPrefix + 179)
DECLARE_PMID(kWidgetIDSpace, kN2PUseFototecaWidgetID,			kN2PsqlPrefix + 180)
DECLARE_PMID(kWidgetIDSpace, kN2PPhotoServerFolderINWidgetID,			kN2PsqlPrefix + 181)
DECLARE_PMID(kWidgetIDSpace, kPhotoServerFolderOUTWidgetID,				kN2PsqlPrefix + 182)

DECLARE_PMID(kWidgetIDSpace, kN2PSQLnameParaStyleContenidoWidgetID,		kN2PsqlPrefix + 183)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLnameParaStyleBalazoWidgetID,		kN2PsqlPrefix + 184)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLnameParaStyleTituloWidgetID,		kN2PsqlPrefix + 185)
DECLARE_PMID(kWidgetIDSpace, kN2PSQLnameParaStylePieFotoWidgetID,		kN2PsqlPrefix + 186)


DECLARE_PMID(kWidgetIDSpace, kN2PCkBoxSendWordPressWidgetID,			kN2PsqlPrefix + 187)
DECLARE_PMID(kWidgetIDSpace, kN2PLinkWebServicesWordPressWidgetID,		kN2PsqlPrefix + 188)



// Service IDs
DECLARE_PMID(kServiceIDSpace, kBscSlDlgService, kN2PsqlPrefix + 0)

// "About Plug-ins" sub-menu:
#define kN2PsqlAboutMenuKey			kN2PsqlStringPrefix "kN2PsqlAboutMenuKey"
#define kN2PsqlAboutMenuPath			kSDKDefStandardAboutMenuPath kN2PsqlCompanyKey

// "Plug-ins" sub-menu:
#define kN2PsqlPluginsMenuKey 		kN2PsqlStringPrefix "kN2PsqlPluginsMenuKey"
#define kN2PsqlPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2PsqlCompanyKey kSDKDefDelimitMenuPath kN2PsqlPluginsMenuKey
#define kN2PsqlRigthMouseMenuPath	"Main:"  "RtMouseText" kSDKDefDelimitMenuPath kN2PsqlPluginsMenuKey//kSDKDefDelimiterAndSeparatorPath 


// Menu item keys:
#define kN2PsqlItemOneMenuItemKey		kN2PsqlStringPrefix "kN2PsqlItemOneMenuItemKey"
#define kN2PsqlDepositarMenuItemKey		kN2PsqlStringPrefix "kN2PsqlDepositarMenuItemKey"
#define kN2PsqlRetirarMenuItemKey		kN2PsqlStringPrefix "kN2PsqlRetirarMenuItemKey"
#define kN2PsqlSalvarMenuItemKey		kN2PsqlStringPrefix "kN2PsqlSalvarMenuItemKey"
#define kN2PsqlDepInCopyMenuItemKey		kN2PsqlStringPrefix "kN2PsqlDepInCopyMenuItemKey"
#define kN2PsqlLogInMenuItemKey			kN2PsqlStringPrefix "kN2PsqlLogInMenuItemKey"
#define kN2PsqlLogOutMenuItemKey		kN2PsqlStringPrefix "kN2PsqlLogOutMenuItemKey"

// Other StringKeys:
#define kN2PsqlAboutBoxStringKey				kN2PsqlStringPrefix "kN2PsqlAboutBoxStringKey"
#define kN2PsqlItemOneStringKey			kN2PsqlStringPrefix "kN2PsqlItemOneStringKey"
#define kN2PsqlPanelTitleKey					kN2PsqlStringPrefix	"kN2PsqlPanelTitleKey"
#define kN2PsqlStaticTextKey					kN2PsqlStringPrefix	"kN2PsqlStaticTextKey"
#define kN2PsqlInternalPopupMenuNameKey		kN2PsqlStringPrefix	"kN2PsqlInternalPopupMenuNameKey"
#define kN2PsqlTargetMenuPath kN2PsqlInternalPopupMenuNameKey

// Pop Up menu interno del paleta de fotos
#define kN2PsqlPhotoInternalPopupMenuNameKey		kN2PsqlStringPrefix	"kN2PsqlPhotoInternalPopupMenuNameKey"
#define kN2PsqlPhotoTargetMenuPath kN2PsqlPhotoInternalPopupMenuNameKey


#define kN2PsqlStcTextPageTextKey				kN2PsqlStringPrefix "kN2PsqlStcTextPageTextKey"
#define kN2PsqlStTextTituloTextKey			kN2PsqlStringPrefix "kN2PsqlStTextTituloTextKey"
#define kN2PsqlStTextBalazoTextKey			kN2PsqlStringPrefix "kN2PsqlStTextBalazoTextKey"
#define kN2PsqlStTextNotaTextKey				kN2PsqlStringPrefix "kN2PsqlStTextNotaTextKey"
#define kN2PsqlStTextPieTextKey				kN2PsqlStringPrefix "kN2PsqlStTextPieTextKey"
#define kN2PsqlStTextPubliTextKey				kN2PsqlStringPrefix "kN2PsqlStTextPubliTextKey"
#define kN2PsqlStTextSeccionTextKey			kN2PsqlStringPrefix "kN2PsqlStTextSeccionTextKey"
#define kN2PsqlStTextFechaTextKey				kN2PsqlStringPrefix "kN2PsqlStTextFechaTextKey"
#define kN2PsqlStTextStatusTextKey			kN2PsqlStringPrefix "kN2PsqlStTextStatusTextKey"
#define kN2PsqlBotonPDFFechaTextKey			kN2PsqlStringPrefix "kN2PsqlBotonPDFFechaTextKey"
#define kN2PsqlBotonSubmitFechaTextKey		kN2PsqlStringPrefix "kN2PsqlBotonSubmitFechaTextKey"
#define kN2PsqlBotonLimpiarCamposTextKey		kN2PsqlStringPrefix "kN2PsqlBotonLimpiarCamposTextKey"
#define kN2PsqlBotonPDFBusquedaTextKey		kN2PsqlStringPrefix "kN2PsqlBotonPDFBusquedaTextKey"
#define kN2PsqlDlgBusquedaTitleKey	kN2PsqlStringPrefix "kN2PsqlDlgBusquedaTitleKey"
#define kN2PsqlBotonBuscarNotaTextKey			kN2PsqlStringPrefix "kN2PsqlBotonBuscarNotaTextKey"
#define kN2PsqlVistaPreviaTextKey				kN2PsqlStringPrefix "kN2PsqlVistaPreviaTextKey"
#define kN2PsqlStTextArticlesTextKey				kN2PsqlStringPrefix "kN2PsqlStTextArticlesTextKey"

#define kN2PsqlStcThisArticleTextKey				kN2PsqlStringPrefix "kN2PsqlStcThisArticleTextKey"
#define kN2PsqlStTextRoutedToTextKey				kN2PsqlStringPrefix "kN2PsqlStTextRoutedToTextKey"
#define kN2PsqlStTextMAkeUpdateTextKey				kN2PsqlStringPrefix "kN2PsqlStTextMAkeUpdateTextKey"
#define kN2PsqlStTextReciveUpdateTextKey				kN2PsqlStringPrefix "kN2PsqlStTextReciveUpdateTextKey"
#define kN2PsqlMsgAlertHacerSeleccionTextKey				kN2PsqlStringPrefix "kN2PsqlMsgAlertHacerSeleccionTextKey"


#define kN2PsqlNoExisteDocumentoStringKey				kN2PsqlStringPrefix "kN2PsqlNoExisteDocumentoStringKey"
#define kN2PsqlUpdateFinishedStringKey				kN2PsqlStringPrefix "kN2PsqlUpdateFinishedStringKey"
#define kN2PsqlPDFExportadoStringKey				kN2PsqlStringPrefix "kN2PsqlPDFExportadoStringKey"
#define kN2PsqlNewRegisterOnDBStringKey				kN2PsqlStringPrefix "kN2PsqlNewRegisterOnDBStringKey"

#define kN2PSQLServerIPLabelKey				kN2PsqlStringPrefix "kN2PSQLServerIPLabelKey"
#define kN2PSQLOptionStringKey				kN2PsqlStringPrefix "kN2PSQLOptionStringKey"
#define kN2PSQLFileMAkerStringKey				kN2PsqlStringPrefix "kN2PSQLFileMAkerStringKey"
#define kN2PSQLDBOpcionesStringKey				kN2PsqlStringPrefix "kN2PSQLDBOpcionesStringKey"
#define kN2PSQLNameDBLabelKey				kN2PsqlStringPrefix "kN2PSQLNameDBLabelKey"
#define kN2PSQLDSNNameLabelKey				kN2PsqlStringPrefix "kN2PSQLDSNNameLabelKey"
#define kN2PSQLPasswordDBLabelKey				kN2PsqlStringPrefix "kN2PSQLPasswordDBLabelKey"
#define kN2PSQLNameUsuarioDBLabelKey				kN2PsqlStringPrefix "kN2PSQLNameUsuarioDBLabelKey"

#define kN2PSQLPreferenciasMenuItemStringKey				kN2PsqlStringPrefix "kN2PSQLPreferenciasMenuItemStringKey"

#define kN2PSQLPreferDlgTitleKey				kN2PsqlStringPrefix "kN2PSQLPreferDlgTitleKey"
#define kN2PsqlNewDBConexionStringKey				kN2PsqlStringPrefix "kN2PsqlNewDBConexionStringKey"
#define kN2PsqlDeleteDBConexionStringKey				kN2PsqlStringPrefix "kN2PsqlDeleteDBConexionStringKey"
#define kN2PsqlSetLocalDBConexionStringKey				kN2PsqlStringPrefix "kN2PsqlSetLocalDBConexionStringKey"
#define kN2PsqlRefreshStatusNotaStringKey				kN2PsqlStringPrefix "kN2PsqlRefreshStatusNotaStringKey"
#define kN2PSQLNameConecctionDBLabelKey				kN2PsqlStringPrefix "kN2PSQLNameConecctionDBLabelKey"
#define kN2PsqlTestDBConexionStringKey				kN2PsqlStringPrefix "kN2PsqlTestDBConexionStringKey"
#define kN2PsqlCancelDBConexionStringKey				kN2PsqlStringPrefix "kN2PsqlCancelDBConexionStringKey"
#define kN2PsqlGuardarDBConexionStringKey				kN2PsqlStringPrefix "kN2PsqlGuardarDBConexionStringKey"
#define kN2PSQLSeguroBorrarPrefereStringKey				kN2PsqlStringPrefix "kN2PSQLSeguroBorrarPrefereStringKey"
#define kN2PSQLAlmenosUnaSeleccionStringKey				kN2PsqlStringPrefix "kN2PSQLAlmenosUnaSeleccionStringKey"
#define kN2PSQLErrorDuranteBorradoStringKey				kN2PsqlStringPrefix "kN2PSQLErrorDuranteBorradoStringKey"
#define kN2PSQLPreferenciaBorradaStringKey				kN2PsqlStringPrefix "kN2PSQLPreferenciaBorradaStringKey"
#define kN2PsqlEditDBConexionStringKey				kN2PsqlStringPrefix "kN2PsqlEditDBConexionStringKey"
#define kN2PSQLSeguroBorrarPrefereDefaultStringKey				kN2PsqlStringPrefix "kN2PSQLSeguroBorrarPrefereDefaultStringKey"
#define kN2PInOutNoServerPrefSelectedStringKey				kN2PsqlStringPrefix "kN2PInOutNoServerPrefSelectedStringKey"

#define kN2PsqlCheckInNotaMenuItemKey				kN2PsqlStringPrefix "kN2PsqlCheckInNotaMenuItemKey"
#define kN2PsqlCheckOutNotaMenuItemKey				kN2PsqlStringPrefix "kN2PsqlCheckOutNotaMenuItemKey"
#define kN2PsqlImportNotaToActServerMenuItemKey				kN2PsqlStringPrefix "kN2PsqlImportNotaToActServerMenuItemKey"
#define kN2PsqlDetachNotaMenuItemKey				kN2PsqlStringPrefix "kN2PsqlDetachNotaMenuItemKey"
#define kN2PsqlCrearSpaceAndNotaMenuItemKey				kN2PsqlStringPrefix "kN2PsqlCrearSpaceAndNotaMenuItemKey"


#define kN2PSQLShowEdgesStringKey				kN2PsqlStringPrefix "kN2PSQLShowEdgesStringKey"
#define kN2PSQLSetLblCmdUndoKey				kN2PsqlStringPrefix "kN2PSQLSetLblCmdUndoKey"
#define kN2PSQLSetLblCmdRedoKey				kN2PsqlStringPrefix "kN2PSQLSetLblCmdRedoKey"

#define kN2PSQLNoExistenNotasSobreDocumentoStringKey				kN2PsqlStringPrefix "kN2PSQLNoExistenNotasSobreDocumentoStringKey"
#define kN2PSQLAlertSeleccionarCajaDeTextoStringKey				kN2PsqlStringPrefix "kN2PSQLAlertSeleccionarCajaDeTextoStringKey"
#define kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey				kN2PsqlStringPrefix "kN2PSQLAlertCajaDeTextoNoNotaN2PStringKey"

#define kN2PsqlStTextUpdateMakeNotasTextKey				kN2PsqlStringPrefix "kN2PsqlStTextUpdateMakeNotasTextKey"
#define kN2PsqlMakeUpdateFinishedStringKey				kN2PsqlStringPrefix "kN2PsqlMakeUpdateFinishedStringKey"
#define kInfoAlertNotaEditPorOtroUsuarioStringKey				kN2PsqlStringPrefix "kInfoAlertNotaEditPorOtroUsuarioStringKey"
#define kInfoAlertCheckOutArticleCorrectStringKey				kN2PsqlStringPrefix "kInfoAlertCheckOutArticleCorrectStringKey"
#define kInfoAlertNotaDeOtraBDStringKey				kN2PsqlStringPrefix "kInfoAlertNotaDeOtraBDStringKey"
#define kInfoAlertCheckInNotaStringKey				kN2PsqlStringPrefix "kInfoAlertCheckInNotaStringKey"
#define kN2PSQLOkConecctionDSNStringKey				kN2PsqlStringPrefix "kN2PSQLOkConecctionDSNStringKey"
#define kN2PSQLErrorConecctionDSNStringKey				kN2PsqlStringPrefix "kN2PSQLErrorConecctionDSNStringKey"
#define kN2PSQLInspeccionaNotasTaskKey				kN2PsqlStringPrefix "kN2PSQLInspeccionaNotasTaskKey"
#define kN2PSQLFotoPageTaskKey				kN2PsqlStringPrefix "kN2PSQLFotoPageTaskKey"
#define kN2PsqlDlgCheckInArticleTitleKey				kN2PsqlStringPrefix "kN2PsqlDlgCheckInArticleTitleKey"
#define kN2PsqlStcTextInCopyInDesingTextKey				kN2PsqlStringPrefix "kN2PsqlStcTextInCopyInDesingTextKey"
#define kN2PsqlStcTextDBConectionStringKey			kN2PsqlStringPrefix "kN2PsqlStcTextDBConectionStringKey"
#define kN2PImpNomPagInvalidoStringKey			kN2PsqlStringPrefix "kN2PImpNomPagInvalidoStringKey"

#define kN2PsqlNotaDeOtroServidorStringKey			kN2PsqlStringPrefix "kN2PsqlNotaDeOtroServidorStringKey"
#define kN2PsqlServidorLocalStringKey			kN2PsqlStringPrefix "kN2PsqlServidorLocalStringKey"
#define kN2PSqlNotaPerteneceaOtroGrupoStringKey			kN2PsqlStringPrefix "kN2PSqlNotaPerteneceaOtroGrupoStringKey"


#define kN2PsqlListPhotoPanelTitleKey			kN2PsqlStringPrefix "kN2PsqlListPhotoPanelTitleKey"
#define kN2PSQLDeseaImportNotaStringKey			kN2PsqlStringPrefix "kN2PSQLDeseaImportNotaStringKey"
#define kCantCheckOutserverRemoteStringKey			kN2PsqlStringPrefix "kCantCheckOutserverRemoteStringKey"
#define kFotoYaFueColocadaStringKey			kN2PsqlStringPrefix "kFotoYaFueColocadaStringKey"

#define kN2PSQLPathOfServerFileLabelKey			kN2PsqlStringPrefix "kN2PSQLPathOfServerFileLabelKey"
#define kN2PsqlSelectPathStringKey			kN2PsqlStringPrefix "kN2PsqlSelectPathStringKey"
#define kN2PSQLDeseaFluirNotaCIdSecDifteAPageStringKey			kN2PsqlStringPrefix "kN2PSQLDeseaFluirNotaCIdSecDifteAPageStringKey"
#define kN2PSQLAllowString			kN2PsqlStringPrefix "kN2PSQLAllowString"

#define kN2PSQLYouCouldSaveThisPageString			kN2PsqlStringPrefix "kN2PSQLYouCouldSaveThisPageString"
#define kN2PSQLThisIsElemDNotaString			kN2PsqlStringPrefix "kN2PSQLThisIsElemDNotaString"

#define kN2PsqlAlertCNSinTextoStringKey			kN2PsqlStringPrefix "kN2PsqlAlertCNSinTextoStringKey"
#define kN2PsqlAlertPFSinTextoStringKey			kN2PsqlStringPrefix "kN2PsqlAlertPFSinTextoStringKey"
#define kN2PsqlAlertBaSinTextoStringKey			kN2PsqlStringPrefix "kN2PsqlAlertBaSinTextoStringKey"
#define kN2PsqlAlertTiSinTextoStringKey			kN2PsqlStringPrefix "kN2PsqlAlertTiSinTextoStringKey"
#define kN2PsqlCantCheckInArticleStringKey			kN2PsqlStringPrefix "kN2PsqlCantCheckInArticleStringKey"
#define kN2PSQLUserCantLigarNotaAPagStringKey			kN2PsqlStringPrefix "kN2PSQLUserCantLigarNotaAPagStringKey"
#define kN2PSQLUserCantDesligarNotaAPagStringKey			kN2PsqlStringPrefix "kN2PSQLUserCantDesligarNotaAPagStringKey"
#define kN2PSQLCantCrearNotaInDStringKey			kN2PsqlStringPrefix "kN2PSQLCantCrearNotaInDStringKey"
#define kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey			kN2PsqlStringPrefix "kN2PSQLSinPrivilegiosPaCheckOutNotaStringKey"

#define kN2PsqlCrearPaseDeNotaMenuItemKey			kN2PsqlStringPrefix "kN2PsqlCrearPaseDeNotaMenuItemKey"
#define kN2PSQLOnlyPageJumpsStringKey			kN2PsqlStringPrefix "kN2PSQLOnlyPageJumpsStringKey"
#define kN2PSQLCantFotoAsignadasStringKey			kN2PsqlStringPrefix "kN2PSQLCantFotoAsignadasStringKey"

#define kN2PSQLLogOffTaskKey			kN2PsqlStringPrefix "kN2PSQLLogOffTaskKey"
#define kN2PSQLTimeTaskLogOffStringKey			kN2PsqlStringPrefix "kN2PSQLTimeTaskLogOffStringKey"
#define kN2PSQLFailedWhenSavePrefStringKey			kN2PsqlStringPrefix "kN2PSQLFailedWhenSavePrefStringKey"
#define kN2PSQLPhotoActivarStringKey			kN2PsqlStringPrefix "kN2PSQLPhotoActivarStringKey"
#define kN2PSQLElementosMostradosEstanEnUsoStringKey			kN2PsqlStringPrefix "kN2PSQLElementosMostradosEstanEnUsoStringKey"
#define kN2PsqlDetachPhotoStringMenuKey			kN2PsqlStringPrefix "kN2PsqlDetachPhotoStringMenuKey"
#define kN2PSQLPathOfServerFileImagenesLabelKey			kN2PsqlStringPrefix "kN2PSQLPathOfServerFileImagenesLabelKey"
#define kN2PSQLRutaDServidorRespNotasLabelKey			kN2PsqlStringPrefix "kN2PSQLRutaDServidorRespNotasLabelKey"
#define kN2PSQLDebeSerChecauteadaStringKey			kN2PsqlStringPrefix "kN2PSQLDebeSerChecauteadaStringKey"
#define kN2PDeseasDesligarEstaNotaStringKey			kN2PsqlStringPrefix "kN2PDeseasDesligarEstaNotaStringKey"
#define kN2PDesarReemplazarelElementoStringKey			kN2PsqlStringPrefix "kN2PDesarReemplazarelElementoStringKey"
#define kN2PSQLShowOverTextStringKey			kN2PsqlStringPrefix "kN2PSQLShowOverTextStringKey"
#define kN2PsqlDlgDetachArticleTitleKey			kN2PsqlStringPrefix "kN2PsqlDlgDetachArticleTitleKey"
#define kN2PsqlIdiamaActualStringKey			kN2PsqlStringPrefix "kN2PsqlIdiamaActualStringKey"
#define kN2PsqlStcTextEnGuiaTextKey			kN2PsqlStringPrefix "kN2PsqlStcTextEnGuiaTextKey"
#define kN2PsqlDlgCrearNotaHijaTitleKey			kN2PsqlStringPrefix "kN2PsqlDlgCrearNotaHijaTitleKey"
#define kN2PMensagCrearComoNotaHijaStringKey			kN2PsqlStringPrefix "kN2PMensagCrearComoNotaHijaStringKey"

#define kN2PBuscarNotaDPaginaStringKey			kN2PsqlStringPrefix "kN2PBuscarNotaDPaginaStringKey"
#define kN2PBuscarNotasStringKey			kN2PsqlStringPrefix "kN2PBuscarNotasStringKey"
#define kN2PActualizarTodoStringKey			kN2PsqlStringPrefix "kN2PActualizarTodoStringKey"
#define kN2PSQLDebeDepositarNotaPadreStringKey			kN2PsqlStringPrefix "kN2PSQLDebeDepositarNotaPadreStringKey"
#define kInfoAlertNotaASidoDirigidoAUsuarioStringKey			kN2PsqlStringPrefix "kInfoAlertNotaASidoDirigidoAUsuarioStringKey"
#define kN2PSQLExportNotasATablaWEBStringKey			kN2PsqlStringPrefix "kN2PSQLExportNotasATablaWEBStringKey"
#define kN2PSQLEstatusParaExportarStringKey			kN2PsqlStringPrefix "kN2PSQLEstatusParaExportarStringKey"
#define kN2PsqlUltimoUsuarioQModNotaStringKey			kN2PsqlStringPrefix "kN2PsqlUltimoUsuarioQModNotaStringKey"
#define kN2PSQLMuestraDirigidoAEnCajasDNotasStringKey			kN2PsqlStringPrefix "kN2PSQLMuestraDirigidoAEnCajasDNotasStringKey"
#define kN2PsqlLigarImagensANotaStringMenuKey			kN2PsqlStringPrefix "kN2PsqlLigarImagensANotaStringMenuKey"
#define kN2PsqlAceptarImagenesDNotaMenuItemKey			kN2PsqlStringPrefix "kN2PsqlAceptarImagenesDNotaMenuItemKey"

#define kN2PsqlNoexistenCajasDImagenSelecStringKey			kN2PsqlStringPrefix "kN2PsqlNoexistenCajasDImagenSelecStringKey"
#define kN2PsqlImegenesLigadasANotasStringKey			kN2PsqlStringPrefix "kN2PsqlImegenesLigadasANotasStringKey"
#define kN2PsqlDuplicarPaginaMenuItemKey			kN2PsqlStringPrefix "kN2PsqlDuplicarPaginaMenuItemKey"
#define kN2PsqlAboutThisPlugInMenuKey			kN2PsqlStringPrefix "kN2PsqlAboutThisPlugInMenuKey"
#define kN2PsqlNoSeEncontraronNotasPlugInMenuKey			kN2PsqlStringPrefix "kN2PsqlNoSeEncontraronNotasPlugInMenuKey"
#define kN2PSQNombreEstatusParaNotasCompletasStringKey			kN2PsqlStringPrefix "kN2PSQNombreEstatusParaNotasCompletasStringKey"
#define kN2PSQLExportarPaginaComoPDFStringKey			kN2PsqlStringPrefix "kN2PSQLExportarPaginaComoPDFStringKey"
#define kN2PSQRutaDePaginaComoPDFStringKey			kN2PsqlStringPrefix "kN2PSQRutaDePaginaComoPDFStringKey"


#define kN2PsqlErrorAlertPaginaBorradaEnBDString			kN2PsqlStringPrefix "kN2PsqlErrorAlertPaginaBorradaEnBDString"
#define kN2PSQLBackUpPaginasEnCheckInStringKey			kN2PsqlStringPrefix "kN2PSQLBackUpPaginasEnCheckInStringKey"
#define kN2PsqlDetachPaginaMenuItemKey			kN2PsqlStringPrefix "kN2PsqlDetachPaginaMenuItemKey"
#define kN2PDeseasDesligarPaginaStringKey			kN2PsqlStringPrefix "kN2PDeseasDesligarPaginaStringKey"
#define kN2PPaginaDesligadaStringKey			kN2PsqlStringPrefix "kN2PPaginaDesligadaStringKey"
#define kN2PHacerUpdateAntesDContinuarStringKey			kN2PsqlStringPrefix "kN2PHacerUpdateAntesDContinuarStringKey"
#define kN2PSQLValidaComilasYDComillasStringKey			kN2PsqlStringPrefix "kN2PSQLValidaComilasYDComillasStringKey"
#define kN2PSQLEsCampoGuiaObligatorioStringKey			kN2PsqlStringPrefix "kN2PSQLEsCampoGuiaObligatorioStringKey"
#define kN2PSQLGuiaNoVaciaStringKey						kN2PsqlStringPrefix "kN2PSQLGuiaNoVaciaStringKey"
#define kN2PSQLEsEnviarNotasHijasAWEBoStringKey						kN2PsqlStringPrefix "kN2PSQLEsEnviarNotasHijasAWEBoStringKey"

#define kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoStringKey	kN2PsqlStringPrefix "kN2PSQLCkBoxEnviaAWEBEnAltasPieFotoStringKey"
#define kN2PSQLCkBoxEnviaAWEBSinRetornosTituloStringKey	kN2PsqlStringPrefix "kN2PSQLCkBoxEnviaAWEBSinRetornosTituloStringKey"
#define kN2PSQLCkExportPDFPresetsStringKey	kN2PsqlStringPrefix "kN2PSQLCkExportPDFPresetsStringKey"
#define kN2PSQLCkExportPDFPresetsEditWEBStringKey	kN2PsqlStringPrefix "kN2PSQLCkExportPDFPresetsEditWEBStringKey"
#define kN2PSQLAcabasDBorrarUnElementoDLaNotaPendienteStringKey	kN2PsqlStringPrefix "kN2PSQLAcabasDBorrarUnElementoDLaNotaPendienteStringKey"

#define kN2PSQLNotaRetiradaPorOtroUsuarioDepositoVisualStringKey	kN2PsqlStringPrefix "kN2PSQLNotaRetiradaPorOtroUsuarioDepositoVisualStringKey"

#define kN2PSQLAlertCajaDeTextoConNotaN2PDesligarNotaStringKey	kN2PsqlStringPrefix "kN2PSQLAlertCajaDeTextoConNotaN2PDesligarNotaStringKey"
#define kN2PSQLNoSeFechaValidaKey	kN2PsqlStringPrefix "kN2PSQLNoSeFechaValidaKey"

#define kN2PsqlCreatePieDFotoStringMenuKey	kN2PsqlStringPrefix "kN2PsqlCreatePieDFotoStringMenuKey"
#define kN2PsqlPieDFotoCreadoStringKey	kN2PsqlStringPrefix "kN2PsqlPieDFotoCreadoStringKey"
#define kN2PsqlSoloSelect1CajaDImagenSelecStringKey	kN2PsqlStringPrefix "kN2PsqlSoloSelect1CajaDImagenSelecStringKey"
#define kN2Psql_NoEsFotoLigadaAPagina_LigarPrimeroSelecStringKey	kN2PsqlStringPrefix "kN2Psql_NoEsFotoLigadaAPagina_LigarPrimeroSelecStringKey"
#define kN2Psql_Yatiene_UnPiedFotoLigado_FluirSelecStringKey	kN2PsqlStringPrefix "kN2Psql_Yatiene_UnPiedFotoLigado_FluirSelecStringKey"
#define kN2PsqlPieDFotoCreadoStringKey	kN2PsqlStringPrefix "kN2PsqlPieDFotoCreadoStringKey"
#define kN2PSQLAlertCajaDeTextoYaEsElemNotaN2PStringKey	kN2PsqlStringPrefix "kN2PSQLAlertCajaDeTextoYaEsElemNotaN2PStringKey"
#define kN2PSQnameParaStyleCreditoStringKey	kN2PsqlStringPrefix "kN2PSQnameParaStyleCreditoStringKey"
#define kN2PSQLImagesFolderInServerFileImagesLabelKey	kN2PsqlStringPrefix "kN2PSQLImagesFolderInServerFileImagesLabelKey"



#define kYinPanelTitleKey	kN2PsqlStringPrefix "kYinPanelTitleKey"
#define kYangPanelTitleKey	kN2PsqlStringPrefix "kYangPanelTitleKey"
#define kYangPanelStaticTextKey	kN2PsqlStringPrefix "kYangPanelStaticTextKey"
#define kYinPanelStaticTextKey	kN2PsqlStringPrefix "kYinPanelStaticTextKey"
#define kBscSlDlgDialogTitleKey	kN2PsqlStringPrefix "kBscSlDlgDialogTitleKey"
#define kN2PHemerotecaPanelTitleKey	kN2PsqlStringPrefix "kN2PHemerotecaPanelTitleKey"
#define kN2PHmeDSNNameStringKey	kN2PsqlStringPrefix "kN2PHmeDSNNameStringKey"

#define kN2PHmeDSNUserLoginStringKey	kN2PsqlStringPrefix "kN2PHmeDSNUserLoginStringKey"
#define kN2PHmeDSNPwdLoginStringKey		kN2PsqlStringPrefix "kN2PHmeDSNPwdLoginStringKey"
#define kN2PHmeFTPServerStringKey		kN2PsqlStringPrefix "kN2PHmeFTPServerStringKey"
#define kN2PHmeFTPUserStringKey		kN2PsqlStringPrefix "kN2PHmeFTPUserStringKey"
#define kN2PHmeFTPPwdStringKey		kN2PsqlStringPrefix "kN2PHmeFTPPwdStringKey"
#define kN2PHmeFTPFolderStringKey		kN2PsqlStringPrefix "kN2PHmeFTPFolderStringKey"
#define kN2PHmteSendToHmcaStringKey		kN2PsqlStringPrefix "kN2PHmteSendToHmcaStringKey"
#define kN2PSQLURLWebServicesLabelKey		kN2PsqlStringPrefix "kN2PSQLURLWebServicesLabelKey"
#define kN2PResizePreviewViewStringKey		kN2PsqlStringPrefix "kN2PResizePreviewViewStringKey"
#define kN2PHeightSizeStringKey		kN2PsqlStringPrefix "kN2PHeightSizeStringKey"
#define kN2PWidthSizeStringKey		kN2PsqlStringPrefix "kN2PWidthSizeStringKey"
#define kN2PUsarGuiasStringKey		kN2PsqlStringPrefix "kN2PUsarGuiasStringKey"
#define kN2PGenerals2PanelTitleKey		kN2PsqlStringPrefix "kN2PGenerals2PanelTitleKey"
#define kN2PsqlActulizarDisenioStrKey		kN2PsqlStringPrefix "kN2PsqlActulizarDisenioStrKey"

#define kN2PFototecaFolderStringKey		kN2PsqlStringPrefix "kN2PFototecaFolderStringKey"
#define kN2PUsePhotoServerStringKey		kN2PsqlStringPrefix "kN2PUsePhotoServerStringKey"
#define kN2PFototecaFolderINStringKey		kN2PsqlStringPrefix "kN2PFototecaFolderINStringKey"
#define kN2PFototecaFolderOUTStringKey		kN2PsqlStringPrefix "kN2PFototecaFolderOUTStringKey"
#define kN2PFinishedStringKey		kN2PsqlStringPrefix "kN2PFinishedStringKey"
#define kN2PSelectEstatusStringKey		kN2PsqlStringPrefix "kN2PSelectEstatusStringKey"
#define kN2PsqlOpenDialogIssuesStringMenuKey		kN2PsqlStringPrefix "kN2PsqlOpenDialogIssuesStringMenuKey"

#define kN2PSQnameParaStyleBalazoStringKey		kN2PsqlStringPrefix "kN2PSQnameParaStyleBalazoStringKey"
#define kN2PSQnameParaStyleTituloStringKey		kN2PsqlStringPrefix "kN2PSQnameParaStyleTituloStringKey"
#define kN2PSQnameParaStyleContenidoStringKey		kN2PsqlStringPrefix "kN2PSQnameParaStyleContenidoStringKey"
#define kN2PSQnameParaStylePieFotoStringKey		kN2PsqlStringPrefix "kN2PSQnameParaStylePieFotoStringKey"

#define kN2PCkBoxSendWordPressStringKey		kN2PsqlStringPrefix "kN2PCkBoxSendWordPressStringKey"
#define kN2PLinkWebServicesWordPressKey		kN2PsqlStringPrefix "kN2PLinkWebServicesWordPressKey"



// Menu item positions:

#define kN2PsqlLogInMenuItemPosition					0.5
#define kN2PsqlLogOutMenuItemPosition					0.6

#define	kN2PsqlSeparator4MenuItemPosition				1.0

#define kN2PsqlLogOuMenuItemPosition					2.0
#define kN2PsqlRetirarMenuItemPosition					3.0
#define kN2PsqlDepositarMenuItemPosition				4.0
#define kN2PsqlGuardarMenuItemPosition					5.0
#define kN2PsqlDuplicarPaginaMenuItemPosition			5.1
#define kN2PsqlDetachPaginaMenuItemPosition             5.2

#define	kN2PsqlSeparator3MenuItemPosition				6.0

#define kN2PsqlCrearSpaceAndNotaMenuItemPosition        7.0
#define kN2PsqlImportarNotaMenuItemPosition				8.0
#define kN2PsqlCheckOutNotaMenuItemPosition				10.0
#define kN2PsqlCheckInNotaMenuItemPosition				11.0

#define	kN2PsqlSeparator1MenuItemPosition				12.0

#define kN2PsqlCrearPaseDeNotaMenuItemPosition			12.1
#define kN2PsqlBuscarNotasDPaginaMenuItemPosition		12.2
#define kN2PsqlBuscarNotasMenuItemPosition				12.3
#define kN2PsqlActualizarTodoMenuItemPosition			12.4
#define kN2PsqlDetachNotaMenuItemPosition				12.5
#define kN2PsqlDetachPhotoMenuItemPosition				12.6
#define kN2PsqlLigarImagensANotaMenuItemPosition		12.7
#define kN2PsqlAceptarImagenesDNotaMenuItemPosition		12.8
#define kN2PsqlCreatePieDFotoMenuItemPosition			12.9

#define kN2PsqlOpenDialogIssuesMenuItemPosition			12.91

#define	kN2PsqlSeparator2MenuItemPosition				13.0

#define kN2PSQLPreferenciasMenuItemPosition				14.0
#define kN2PsqlAboutThisMenuItemPosition				16.0







#define kN2PsqlLogoInterlasaRsrcID						1520

#define kN2PsqlCmpPenIcon								1512
#define kN2PsqlFluirIconResourceID						1700
#define kN2Psql_CSRsrcID								128
#define kN2PsqlAlertActualizarNotasIconResourceID		129
#define kN2PsqlInCopyPctureIconResourceID				130

#define kN2PSQLPanelPrincipalResourceID					1400
#define kN2PsqlCheckInArticleDlgResourceID				1600


#define kN2PSQLDefPanelResourceID						1700
#define kN2PSQLDefFactoryListResourceID					1750
#define kN2PsqlPhotoListElementRsrcID  					1800
#define kN2PsqlAlertNotaConFotoIconResourceID			1900
#define kN2PsqlPlacedFotoIconResourceID					1950
#define kN2PsqlDetachNotaIconResourceID					2001
#define kN2PsqlFotoListaIconResourceID					2050
#define kN2PsqlDetachArticleDlgResourceID				2060
#define kN2PsqlCrearNotaHijaDlgResourceID				2070

////Selectable dialog
#define kBscSlDlgPanelOrderingResourceID				700
#define kBscSlDlgDialogResourceID						710
#define kBscSlDlgTabDialogResourceID					711
#define kYinPanelResourceID								720
#define kYinPanelCreatorResourceID						730
#define kYangPanelResourceID							740
#define kYangPanelCreatorResourceID						750

#define kBscSlDlgTreeNodeTreeStyle						800
#define kBscSlDlgTreeNodeRsrcID							810
////////////////////////
#define kN2PsqlResultadoListElementRsrcID				820
#define kN2PsqlPasesListElementRsrcID					830
#define kN2PsqlBusquedaDialogResourceID					840
#define kN2PHemerotecaPanelCreatorResourceID			850
#define kN2PHemerotecaPanelResourceID					860

#define kN2PGeneral2PanelCreatorResourceID			870
#define kN2PGeneral2PanelResourceID					880


//#define N2PsqlourFlavor PMFlavor('AOL1')
#define N2PSQLCustomFlavorFile PMFlavor(kPMSysFileFlavor)

#define N2PSQLTituloTextFlavor PMFlavor(kPMTextFlavor)
#define N2PSQLBalazoTextFlavor PMFlavor(kPMTextFlavor)
#define N2PSQLPieFotoTextFlavor PMFlavor(kPMTextFlavor)
#define N2PSQLContenidoNetoTextFlavor PMFlavor(kPMTextFlavor)



// Initial data format version numbers
#define kN2PsqlFirstMajorFormatNumber  RezLong(1)
#define kN2PsqlFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kN2PsqlCurrentMajorFormatNumber kN2PsqlFirstMajorFormatNumber
#define kN2PsqlCurrentMinorFormatNumber kN2PsqlFirstMinorFormatNumber

#endif // __N2PsqlID_h__

//  Code generated by DollyXs code generator
