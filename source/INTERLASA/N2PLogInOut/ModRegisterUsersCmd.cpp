//========================================================================================
//  
//  $File: //depot/indesign_3.x/dragonfly/source/sdksamples/customprefs/ModRegisterUsersCmd.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: sstudley $
//  
//  $DateTime: 2003/12/18 11:20:39 $
//  
//  $Revision: #1 $
//  
//  $Change: 237988 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes
#include "IBoolData.h"
#include "IIntData.h"
#include "ISubject.h"
#include "IStringData.h"
// General includes
#include "ErrorUtils.h"
#include "Command.h"
#include "CAlert.h"

// ModRegisterUsersCmd includes
#include "N2PRegisterUsers.h"


/** ModRegisterUsersCmd
	simple command to change the preferences for this plug-in.  The implementation
	is assumed to be an interface on ModCstPrfCmdBoss, and it needs to have the
	data interfaces on that boss.  

	@ingroup customprefs
  	@author John Hake
*/

class ModRegisterUsersCmd : public Command
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		ModRegisterUsersCmd(IPMUnknown* boss);

		/**
			Destructor.
		*/
		~ModRegisterUsersCmd() {}

	protected:
		/**
			Set the application workspace custom preference and the front document 
			preference value.
		*/
		void Do();

		/**
			Restores the preference to its original value for both the workspace
			and the front document.
		*/
		void Undo();

		/**
			Sets the preference values by calling Do().
		*/
		void Redo();

		/** 
			Broadcasts notification of change. Acquires the subject interface
			on the same boss of the command target, and calls the change method.
		*/
		void DoNotify();

		/**
			Gives the name that shows up under the Edit menu and
			allows the user to undo or redo the command.

			@return pointer to a PMString allocated using the new operator.
			Deleted by caller.
		*/
		PMString* CreateName();

	private:
		/** Old preference value, used for Undo().
		 */
		bool16		fOldPref;
		
		int32		IDUsuario;
		
		PMString NameApp;
		PMString FechaIn;
		PMString HoraIn;
		PMString IDUserLogedString;
		
		PMString Estado_To_PageIn;
		PMString SeccionNota;
		PMString PaginaNota;
		PMString IssueNota;
		PMString DateNota;
		PMString DirididoANota;
	
	
		PMString EstatusPageOut;
		PMString SeccionPageOut;
		PMString PaginaPageOut;
		PMString IssuePageOut;
		PMString DatePageOut;
		PMString DirididoAPageOut;
		
		PMString Datos;
		/** Called by Do() and Undo();
		 * 	fOldPref value will change after this call.
		 * 	@param value IN The value to set.
		 */
		void DoSetValue(bool16 value,int32 IDUser,PMString Applicacion,PMString FechaInUser, PMString HoraInUser
														,PMString EstatusN,PMString SeccionN,PMString PaginaN,PMString IssueN,
														PMString DateN, PMString DirigidoAN,PMString IDUserLogedStringN,
														PMString EstatusNotaPageOutN,
														PMString SeccionNotaPageOutN,
														PMString PaginaNotaPageOutN,
														PMString IssueNotaPageOutN,
														PMString DateNotaPageOutN,
														PMString DirididoANotaPageOutN);
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(ModRegisterUsersCmd, kModRegisterUsersCmdImpl)


/* Constructor
*/
ModRegisterUsersCmd::ModRegisterUsersCmd(IPMUnknown* boss) :
	Command(boss), fOldPref(kFalse), IDUsuario(), NameApp(), FechaIn(), HoraIn()
{
}

/* Do
*/
void ModRegisterUsersCmd::Do()
{
	do	// false loop start
	{
		// Get a pointer to the command's (boolean) data interface
		
		
		InterfacePtr<IBoolData> cmdBoolData (this, UseDefaultIID());
		if (cmdBoolData == nil)
		{
			ASSERT_FAIL("Couldn't get cmd IBOOLDATA interface in ModRegisterUsersCmd::Do()");
			break;
		}
		
		InterfacePtr<IIntData> cmdIntData(this, UseDefaultIID());
		if(cmdIntData==nil)
		{
			ASSERT_FAIL("Couldn't get cmd IIntData interface in ModRegisterUsersCmd::Do()");
			break;
		}
		
		InterfacePtr<IStringData> cmdStringData(this, UseDefaultIID());
		if(cmdStringData==nil)
		{
			ASSERT_FAIL("Couldn't get cmd IStringData interface in ModRegisterUsersCmd::Do()");
			break;
		}
		
		
		PMString ApplicacionUser="";
		PMString FechaInUser="";
		PMString HoraInUser="";
		PMString IDUserLogedStringUse="";
		PMString EstatusN="";
		PMString SeccionN="";
		PMString PaginaN="";
		PMString IssueN="";
		PMString DateN="";
		 DirididoANota="";
		PMString IDUserLogedStringUser="";
		PMString EstatusPageOutN="";
		PMString SeccionPageOutN="";
		PMString PaginaPageOutN="";
		PMString IssuePageOutN="";
		PMString DatePageOutN="";
		PMString DirididoANotaPageOutN="";
	
		PMString PreferenciasStrings=cmdStringData->Get();
		
		//Obtiene el nombre de la aplicacion
		int32 index=PreferenciasStrings.IndexOfString("Aplicacion:");
		int32 LengthStrPref=0;
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+11)-(index+11));
			if(LengthStrPref>0)
				ApplicacionUser=(PreferenciasStrings.Substring(index+11,LengthStrPref))->GrabCString();
			else
				ApplicacionUser="";
		}
		
		
		//obtiene la Fecha de entrada
		index=PreferenciasStrings.IndexOfString("FechaIn:");
		if(index>=0)
		{
			LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+8)-(index+8));
			if(LengthStrPref>0)
				FechaInUser=(PreferenciasStrings.Substring(index+8,LengthStrPref))->GrabCString();
			else
				FechaInUser="";
		}
		
		
		//obtiene la Hora de entrada
		index=PreferenciasStrings.IndexOfString("HoraIn:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+7)-(index+7));
		if(LengthStrPref>0)
			HoraInUser=(PreferenciasStrings.Substring(index+7,LengthStrPref))->GrabCString();
		else
			HoraInUser="";
		
		index=PreferenciasStrings.IndexOfString("IDUserLogedString:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+18)-(index+18));
		if(LengthStrPref>0)
			IDUserLogedStringUser=(PreferenciasStrings.Substring(index+18,LengthStrPref))->GrabCString();
		else
			IDUserLogedStringUser="";
		
		index=PreferenciasStrings.IndexOfString("Estado_To_PageIn:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+17)-(index+17));
		if(LengthStrPref>0)
			EstatusN=(PreferenciasStrings.Substring(index+17,LengthStrPref))->GrabCString();
		else
			EstatusN="";
		
		index=PreferenciasStrings.IndexOfString("SeccionNota:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+12)-(index+12));
		if(LengthStrPref>0)
			SeccionN=(PreferenciasStrings.Substring(index+12,LengthStrPref))->GrabCString();
		else
			SeccionN="";
		
		index=PreferenciasStrings.IndexOfString("PaginaNota:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+11)-(index+11));
		if(LengthStrPref>0)
			PaginaN=(PreferenciasStrings.Substring(index+11,LengthStrPref))->GrabCString();
		else
			PaginaN="";
		
		index=PreferenciasStrings.IndexOfString("IssueNota:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+10)-(index+10));
		if(LengthStrPref>0)
			IssueN=(PreferenciasStrings.Substring(index+10,LengthStrPref))->GrabCString();
		else
			IssueN="";
		
		index=PreferenciasStrings.IndexOfString("DateNota:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+9)-(index+9));
		if(LengthStrPref>0)
			DateN=(PreferenciasStrings.Substring(index+9,LengthStrPref))->GrabCString();
		else
			DateN="";
		
		index=PreferenciasStrings.IndexOfString("DirigidoA:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+10)-(index+10));
		if(LengthStrPref>0)
			DirididoANota=(PreferenciasStrings.Substring(index+10,LengthStrPref))->GrabCString();
		else
			DirididoANota="";
		
		
		
		index=PreferenciasStrings.IndexOfString("StatusNotaPageOut:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+18)-(index+18));
		if(LengthStrPref>0)
			EstatusPageOutN=(PreferenciasStrings.Substring(index+18,LengthStrPref))->GrabCString();
		else
			EstatusPageOutN="";
		
		
		index=PreferenciasStrings.IndexOfString("SeccionNotaPageOut:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+19)-(index+19));
		if(LengthStrPref>0)
			SeccionPageOutN=(PreferenciasStrings.Substring(index+19,LengthStrPref))->GrabCString();
		else
			SeccionPageOutN="";
		
		
		index=PreferenciasStrings.IndexOfString("PaginaNotaPageOut:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+18)-(index+18));
		if(LengthStrPref>0)
			PaginaPageOutN=(PreferenciasStrings.Substring(index+18,LengthStrPref))->GrabCString();
		else
			PaginaPageOutN="";
		
		
		index=PreferenciasStrings.IndexOfString("IssueNotaPageOut:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+17)-(index+17));
		if(LengthStrPref>0)
			IssuePageOutN=(PreferenciasStrings.Substring(index+17,LengthStrPref))->GrabCString();
		else
			IssuePageOutN="";
		
		
		index=PreferenciasStrings.IndexOfString("DateNotaPageOut:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+16)-(index+16));
		if(LengthStrPref>0)
			DatePageOutN=(PreferenciasStrings.Substring(index+16,LengthStrPref))->GrabCString();
		else
			DatePageOutN="";
			
		
		index=PreferenciasStrings.IndexOfString("DirigidoAPageOut:");
		LengthStrPref=(PreferenciasStrings.IndexOfWChar(10,index+17)-(index+17));
		if(LengthStrPref>0)
			DirididoANotaPageOutN=(PreferenciasStrings.Substring(index+17,LengthStrPref))->GrabCString();
		else
			DirididoANotaPageOutN="";
		
		
		this->DoSetValue(cmdBoolData->Get(),cmdIntData->Get(),ApplicacionUser,FechaInUser,HoraInUser,EstatusN,SeccionN,PaginaN,IssueN,DateN,DirididoANota,IDUserLogedStringUser,
							EstatusPageOutN,
							SeccionPageOutN,
							PaginaPageOutN,
							IssuePageOutN,
							DatePageOutN,
							DirididoANotaPageOutN);
	} while(false);
}

/* Undo
*/
void ModRegisterUsersCmd::Undo()
{
	this->DoSetValue(fOldPref,IDUsuario,NameApp,FechaIn,HoraIn,
		 Estado_To_PageIn,
		 SeccionNota,
		 PaginaNota,
		 IssueNota,
		 DateNota,
		 DirididoANota,
		 IDUserLogedString,
		 EstatusPageOut,
							SeccionPageOut,
							PaginaPageOut,
							IssuePageOut,
							DatePageOut,
							DirididoAPageOut);
}

/* Redo
*/
void ModRegisterUsersCmd::Redo()
{
	ModRegisterUsersCmd::Do();
}


/* DoNotify()
*/
void ModRegisterUsersCmd::DoNotify()
{

	do
	{
		//Get the ItemList
		const UIDList* itemList = this->GetItemList();
		if (itemList == nil || itemList->IsEmpty()) 
		{
			ASSERT_FAIL("The itemList is nil or empty in ModRegisterUsersCmd::DoNotify()");
			break;
		}
		
		// There is only one UIDRef in this list
		UIDRef uidRef = itemList->GetRef(0);
		if (uidRef.ExistsInDB() == kFalse) 
		{	
			ASSERT_FAIL("Invalid UIDRef in ModRegisterUsersCmd::DoNotify()");
			break;
		}

		InterfacePtr<ISubject> subject(uidRef, UseDefaultIID());

		if (subject == nil)
		{
			ASSERT_FAIL("Couldn't get subject interface in ModRegisterUsersCmd::DoNotify()");
			break;
		}

		// Notify the subject of the change
		subject->Change(kModRegisterUsersCmdBoss, IID_IN2PREGISTERUSERS, this);

	} while (false);
}


/* CreateName
*/
PMString* ModRegisterUsersCmd :: CreateName()
{
	PMString* str = new PMString(kN2PModRegisterUsersCmdNameKey);
	return str;
}

/* DoSetValue
*/
void ModRegisterUsersCmd::DoSetValue(bool16 value,int32 IDUser,PMString Applicacion,PMString FechaInUser, PMString HoraInUser
														,PMString EstatusN,PMString SeccionN,PMString PaginaN,PMString IssueN,
														PMString DateN, PMString DirigidoAN,PMString IDUserLogedStringN,
														PMString EstatusNotaPageOutN,
														PMString SeccionNotaPageOutN,
														PMString PaginaNotaPageOutN,
														PMString IssueNotaPageOutN,
														PMString DateNotaPageOutN,
														PMString DirididoANotaPageOutN)
{
	ErrorCode status = kFailure;

	do	// false loop start
	{
		// Get a pointer to the command's (boolean) data interface
		InterfacePtr<IBoolData> cmdBoolData (this, UseDefaultIID());
		if (cmdBoolData == nil)
		{
			ASSERT_FAIL("Couldn't get cmd IBOOLDATA interface in ModRegisterUsersCmd::Do()");
			break;
		}

		//Get the ItemList
		const UIDList* itemList = this->GetItemList();
		if (itemList == nil || itemList->IsEmpty()) 
		{
			ASSERT_FAIL("The itemList is nil or empty in ModRegisterUsersCmd::Do()");
			break;
		}
		
		// There is only one UIDRef in this list
		UIDRef uidRef = itemList->GetRef(0);
		if (uidRef.ExistsInDB() == kFalse) 
		{	
			ASSERT_FAIL("Invalid UIDRef in ModRegisterUsersCmd::Do()");
			break;
		}

		// Use the UIDRef to get the simple prefs interface for the workspace we're changing
		InterfacePtr<IN2PRegisterUsers> cstPrefs(uidRef, UseDefaultIID());

		if (cstPrefs == nil)
		{
			ASSERT_FAIL("Couldn't get custom prefs interface in ModRegisterUsersCmd::Do()");
			break;
		}

		// Copy the old preferences value for undo
		fOldPref = cstPrefs->GetValue();

		IDUsuario = cstPrefs->GetID_Usuario();
		
		NameApp = cstPrefs->GetNameApp();
		
		FechaIn = cstPrefs->GetFechaIn();
		
		HoraIn  = cstPrefs->GetHoraIn();
		
		Estado_To_PageIn = cstPrefs->GetEstatusNota();
		SeccionNota = cstPrefs->GetSeccionNota();
		PaginaNota = cstPrefs->GetPaginaNota();
		IssueNota = cstPrefs->GetIssueNota();
		DateNota = cstPrefs->GetDateNota();
		DirididoANota = cstPrefs->GetDirigidoANota();
		
		EstatusPageOut = cstPrefs->GetEstatusNotaPageOut();
		SeccionPageOut = cstPrefs->GetSeccionNotaPageOut();
		PaginaPageOut = cstPrefs->GetPaginaNotaPageOut();
		IssuePageOut = cstPrefs->GetIssueNotaPageOut();
		DatePageOut = cstPrefs->GetDateNotaPageOut();
		DirididoAPageOut = cstPrefs->GetDirigidoANotaPageOut();
		
		IDUserLogedString = cstPrefs->GetIDUserLogedString();
		// Set the new preference value
		cstPrefs->SetUserValues(value,IDUser,Applicacion,FechaInUser,HoraInUser, 
								EstatusN, SeccionN, PaginaN, IssueN, DateN,DirigidoAN,IDUserLogedStringN,
								EstatusNotaPageOutN,
								SeccionNotaPageOutN,
								PaginaNotaPageOutN,
								IssueNotaPageOutN,
								DateNotaPageOutN,
								DirididoANotaPageOutN);
		
		
		// Indicate success
		status = kSuccess;

	} while (false);		// false loop end

	// Handle any errors
	if (status != kSuccess)
	{
		ASSERT_FAIL("Something wrong happened in ModRegisterUsersCmd::DoSetValue()");
		ErrorUtils::PMSetGlobalErrorCode(status);
	}

}


// End, ModRegisterUsersCmd.cpp.
