//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/ExtLinkUITreeNodeList.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes
#include "URIList.h"

// Impl includes
#include <map>
// Project includes
#include "IExtLinkUITreeNodeList.h"
#include "ISDKODBCWrapper.h"
#include "ExtLinkConst.h"
#include "NodeID.h"


/**  Class representing a a cache of database data. 
	@ingroup extendedlink
*/
class ExtLinkUITreeNodeList : CPMUnknown<IExtLinkUITreeNodeList>
{
public:
	/** Default constructor */
	ExtLinkUITreeNodeList(IPMUnknown* boss);

	/**	Destructor
	 */
	virtual ~ExtLinkUITreeNodeList();

	/** 	See IExtLinkUITreeNodeList::GetData
	*/
	virtual void AddTreeNodeForLink(UID link, NodeID node);

	/** 	See IExtLinkUITreeNodeList::GetData
	*/
	virtual NodeID GetTreeNodeForLink(const UID& link);

protected:

	/**	Destroy the tree represented in this 
	 */
	void deleteTree() ;


private:
	
	std::map<UID, NodeID> fTreeNodeList;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(ExtLinkUITreeNodeList, kExtLinkUITreeNodeListImpl)


/* Constructor
*/
ExtLinkUITreeNodeList::ExtLinkUITreeNodeList(IPMUnknown* boss) : 
	CPMUnknown<IExtLinkUITreeNodeList>(boss)
{
	
}



	
/* Destructor
*/
ExtLinkUITreeNodeList::~ExtLinkUITreeNodeList()
{
	deleteTree();
}


/* deleteTree
*/
void ExtLinkUITreeNodeList::deleteTree() 
{
	fTreeNodeList.clear();
}



/* Rebuild
*/
NodeID ExtLinkUITreeNodeList::GetTreeNodeForLink(const UID& linkUID)
{
	NodeID retNode;
	std::map<UID, NodeID >::iterator result = fTreeNodeList.find(linkUID);
	if (!(result == fTreeNodeList.end()))
		retNode = result->second;
	return retNode;
}

void ExtLinkUITreeNodeList::AddTreeNodeForLink(UID linkUID, NodeID node)
{
	std::map<UID, NodeID >::iterator result = fTreeNodeList.find(linkUID);
	if (result == fTreeNodeList.end())	// we don't find a node with such UID
	{
		fTreeNodeList.insert(std::pair<UID, NodeID>(linkUID, node));	// add the cache 
	}
	else
	{
		result->second = node;
	}
}

//	end, File: ExtLinkUITreeNodeList.cpp
