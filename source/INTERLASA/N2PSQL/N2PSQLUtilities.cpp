//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/FileTreeUtils.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================




#include "VCPluginHeaders.h"

// Interface includes

#include "IWidgetParent.h"
//#include "IImportFileCmdData.h"
#include "IDocument.h"
#include "IPlaceGun.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "IDialogController.h"
#include "ITriStateControlData.h"
#include "IMetaDataAccess.h"
#include "ILayoutUtils.h"
#include "IDropDownListController.h"
#include "IUnitOfMeasureSettings.h"
#include "IMeasurementSystem.h"
#include "IUnitOfMeasure.h"
#include "ITextModel.h"
#include "ITextModelCmds.h"
#include "ITextControlData.h"
#include "IPanelControlData.h"
#include "IItemLockData.h"
#include "ISelectionUtils.h"
#include "IPageList.h"
#include "IHierarchy.h"
#include "ITLAdornmentUtils.h"
#include "IApplication.h"
#include "IPageItemTypeUtils.h"
#include "IScrapItem.h"
#include "ISpread.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "TaggedTextFiltersID.h"
#include "AttributeBossList.h"
#include "ITextAttrUtils.h"
#include "TextID.h"
//
#include "ISelectionUtils.h"
#include "ISelectionManager.h"
#include "ILayoutSelectionSuite.h"
#include "ITextSelectionSuite.h"
#include "IConcreteSelection.h"
#include "IFrameList.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "ITriStateControlData.h"
#include "IGraphicFrameData.h"
#include "IStandOffData.h"
#include "ITextColumnSizer.h"

#include "IParcelList.h"
#include "ITextParcelListComposer.h"
#include "ITOPSplineData.h"
#include "IWaxIterator.h"
#include "IWaxLine.h"
#include "IWaxStrand.h"
#include "IGeometry.h"
#include "IGraphicMetaDataObject.h"
#include "IGraphicStateRenderObjects.h"
#include "IGraphicStateUtils.h"
#include "IGraphicStyleDescriptor.h"////////para cambiar el tamaÒo del marco del rectangulo////////////
//
#include "IConcreteSelection.h"
#include "ITextTarget.h"
#include "ITextFocus.h"
#include "IFrameList.h"
#include "IMultiColumnTextFrame.h"
#include "IComposeScanner.h"
#include "IPlacePIData.h"
#include "IRangeData.h"
#include "IExportProvider.h"
#include "IXferBytes.h"
#include "IAttributeStrand.h"

#include "IListBoxController.h"
#include "ITaggedTextExportPreferences.h"
#include "ITaggedTextImportPreferences.h"
#include "TaggedTextFiltersUIID.h"
#include "TaggedTextTypes.h"
#include "IWorkspace.h"
#include "ISnippetExport.h"

#include "HyperlinkID.h"
#include "IHyperlinkTable.h"

#include "ILayoutUtils.h"
#include "ILayoutUIUtils.h"
#include "IFrameListComposer.h"

// Other API includes
#include "K2SmartPtr.h"
#include "SDKFileHelper.h"
#include "IPalettePanelUtils.h"
#include "TextChar.h"
#include "CAlert.h"
#include "utils.h"
#include "IUIDData.h"

#include "SDKUtilities.h"
#include "SDKLayoutHelper.h"

#include "TransformUtils.h"	
	

#include "IDialogMgr.h"
#include "IDialog.h"
#include "IDialogController.h"
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"

#include "ITextAttributeSuite.h"
#include "IStyleNameTable.h"
#include "IStyleInfo.h"
#include "IPageSetupPrefs.h"

#include "ISelectUtils.h"
#include "FileUtils.h"
#include "IPalettePanelUtils.h"

#include "StreamUtil.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "PlatformFileSystemIterator.h"

#include "IPDFExptStyleListMgr.h"
#include "IPrintContentPrefs.h"
#include "IPDFSecurityPrefs.h"
#include "IPDFExportPrefs.h"
#include "IUIFlagData.h"
#include "IBoolData.h"
#include "IOutputPages.h"
#include "BookID.h"



#include "ISysFileData.h"
#include "IDocumentList.h"
#include "IFileList.h"
#include "ITextFrameColumn.h"
#include "ITextAttrUID.h"
#include "IAttrReport.h"
#include "ITextAttrFont.h"
#include "IFontFamily.h"
#include "IStyleGroupManager.h"
#include "N2PDebugLog.h"
#include "SelectionObserver.h"


#include <stdlib.h>
#include <stdio.h>



//#include <winreg.h>
#ifdef WINDOWS
	#include <sys/types.h> 
	#include <sys/stat.h> 
#endif
#ifdef MACINTOSH
	#include <types.h> 
	#include <sys/stat.h> 
#endif
//#include <io.h> 

#include <time.h> 
#include "ICompositeFont.h"
#include "IImportResourceCmdData.h"
#include "IURIUtils.h"

#include "TextStoryThreadHelper.h"


#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
	#include "N2PSQLUtilities.h"
	#include "IN2PSQLUtils.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "PreferenceUtils.h"
	#include "N2PSQLListBoxHelper.h"
	#include "N2PSQLPhotoListBoxHelper.h"
	#include "N2PSQLXferBytes.h"
	#include "N2PNotesStorage.h"
	#include "IN2PNotesUtils.h"

	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PsqlLogInLogOutUtilities.h"
	#include "IN2PAdIssueUtils.h"



	#include "N2PwsdlcltID.h"
	#include "N2PWSDLCltUtils.h"
#endif

#ifdef WINDOWS
	#include <sys/stat.h>
	#include "PreferenceUtils.h"
	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"
	#include "IN2PNotesUtils.h"
	#include "N2PSQLListBoxHelper.h"
	#include "N2PSQLPhotoListBoxHelper.h"
	#include "N2PSQLXferBytes.h"
	#include "N2PNotesStorage.h"
	
	#include "..\Interlasa_common\InterlasaUtilities.h"
	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
	#include "..\N2PCheckInOut\ICheckInOutSuite.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PsqlLogInLogOutUtilities.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PAdornmentIssue\IN2PAdIssueUtils.h"
	#include "..\N2PAdornmentIssue\N2PAdIssueID.h"
	#include "..\InterlasaUltraProKeys\InterUltraProKyID.h"
	#include "..\InterlasaUltraProKeys\IInterUltraProKy.h"
	#include "..\InterlasaUltraProKeys\IInterUltraObjectProKy.h"
#endif


// Project includes
#
#ifdef WINDOWS
#define PLATFORM_PATH_DELIMITER kTextChar_ReverseSolidus
#endif
#ifdef MACINTOSH
#define PLATFORM_PATH_DELIMITER kTextChar_Colon
#endif




PMString N2PSQLUtilities::ProporcionaFechaArchivo(CString Archivo)
{
	struct stat p;
	stat(Archivo,&p);
	return(ctime(&p.st_atime));
}

PMString N2PSQLUtilities::DescryptSerial(CString CadenaEncriptada)
{
	PMString Temp="";
	char *caracter;		//caracter leido
	int ValorAscii=0;	//Valor ascii del caracter
	char *Llave;		//llave para la conversion de de los caracteres
	Llave="INTERLASA";	
	int Tamano;			//tamaÒo de la cadena a encriptar
	int NumCarLlave;
	int numCarDeCadADesencrip;
	Tamano=strlen(CadenaEncriptada);
	//char CaracterDesencriptada;		//Caracter desencriptado
	PMString CadenaDesencriptada;			//Cadena completa y desencriptada
	NumCarLlave=0;
	for(numCarDeCadADesencrip=0;numCarDeCadADesencrip<Tamano;numCarDeCadADesencrip++)//hacer hasta el ultimo caracter de la cadena a desencriptar
	{
		ValorAscii=(int)CadenaEncriptada[numCarDeCadADesencrip];//obtiene el codigo ascii del caracter leido de la cadena a desencriptar
		//Temp.AppendNumber(ValorAscii);
		
		//Temp.Append(",");
		ValorAscii=(ValorAscii-(int)Llave[NumCarLlave])+19;//codigo asccii del caracter leido de la llave y adicion a valos asscii
		//Temp.AppendNumber(ValorAscii);
		
		//Temp.Append(",");
		//CaracterDesencriptada=(char)ValorAscii;//conversion del numero resultante a caracter
		
		CadenaDesencriptada.AppendW(ValorAscii);//adidion a cadena desencriptada
		
		
		caracter=" ";
		ValorAscii=0;
		NumCarLlave++;//siguiente caracter
		if(NumCarLlave>7)//si caracter en llave es mayor de 7 comenzar en 0
			NumCarLlave=0;
	}
//CadenaDesencriptada.SetTranslated();
	
return(CadenaDesencriptada.GrabCString());
}










/**
			Metodo que busca el archivo correspondiente si no lo encuentra lo buscara por otro tipo de
			extencion.
*/
bool16 N2PSQLUtilities::ExisteArchivoParaLectura(PMString *filePath)
{
	PMString Path;//Variable que almacenara el path original y para poder hacer las preguntas si existen el
					//archivo con determinada extencion
	Path=filePath->GrabCString();
	bool16 value=kFalse;//
	do
	{
		//Busca si existe el archivo con la extencion inicial
		if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
		{
			value=kTrue;
			break;
		}
		else//si no lo encontro
		{
			//Busca si existe el archivo con la extencion jpg
			Path=N2PSQLUtilities::TruncateExtencion(Path);
			Path.Append(".jpg");//adiciona la nueva extencion al path o ruta.
			if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
			{
				value=kTrue;
				break;
			}
			else
			{
				//Busca si existe el archivo con la extencion gif
				Path=N2PSQLUtilities::TruncateExtencion(Path);
				Path.Append(".gif");
				if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
				{
					value=kTrue;
					break;
				}
				else
				{
					//Busca si existe el archivo con la extencion pdf
					Path=N2PSQLUtilities::TruncateExtencion(Path);
					Path.Append(".pdf");
					if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
					{
						value=kTrue;
						break;
					}
					else
					{
						//Busca si existe el archivo con la extencion psd
						Path=N2PSQLUtilities::TruncateExtencion(Path);
						Path.Append(".psd");
						if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
						{
							value=kTrue;
							break;
						}
						else
						{
							//Busca si existe el archivo con la extencion eps
							Path=N2PSQLUtilities::TruncateExtencion(Path);
							Path.Append(".eps");
							if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
							{
								value=kTrue;
								break;
							}
							else
							{
								//Busca si existe el archivo con la extencion tif
								Path=N2PSQLUtilities::TruncateExtencion(Path);
								Path.Append(".tif");
								if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
								{
									value=kTrue;
									break;
								}
								else
								{
									//Busca si existe el archivo con la extencion bmp
									Path=N2PSQLUtilities::TruncateExtencion(Path);
									Path.Append(".bmp");
									if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
									{
										value=kTrue;
										break;
									}

									else
									{
										//Busca si existe el archivo con la extencion jepg
										Path=N2PSQLUtilities::TruncateExtencion(Path);
										Path.Append(".jpeg");
										if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
										{
											value=kTrue;
											break;
										}
									}
								}
							}
						}
					}
				}
			}			
		}
	}while(false);
filePath->Clear();//limpia la cadena el path original
filePath->Append(Path.GrabCString());
//TimeCreacionArchivo=ProporcionaFechaArchivo(Path.GrabCString());

return(value);
}


/* Truncateextencion
*/
PMString N2PSQLUtilities::TruncateExtencion(const PMString& fromthis)
{
	if(fromthis.IsEmpty() == kTrue) return PMString("Empty!");

	PMString retval = fromthis;
	int32 lastpos = (-1);
	lastpos=fromthis.IndexOfString("."); 

	if(lastpos >= 0)
	{
		// Suppose we have ../X/Y.gif
		// Then, lastpos would be 4, and we'd want 5 chars from 5 onwards
		int32 countChars = lastpos;
		int32 startIndex = 0;
		int32 endIndex = (startIndex + countChars);
		if((endIndex > startIndex) && (endIndex <= fromthis.CharCount()))
		{
			K2::scoped_ptr<PMString> ptrRightStr(fromthis.Substring(startIndex, countChars));
			if(ptrRightStr)
			{
				retval = *ptrRightStr;
			}
		}
	} 
	return retval;
}




/**
			Funcion utilizada para cambiar de unidades seleccionadas a puntos.
			
			  @param Cantidad, variable del tipo PMReal la cual contiene la cantidad que se desea convertir a puntos.
*/
PMReal N2PSQLUtilities::ConversionPuntosAUnidades(PMReal Cantidad,PMString &NameIOM)
{	
	do
	{
		//obtiene la interfaz IUnitOfMeasureSettings de las preferencias del documento (kGetFrontmostPrefs)
		InterfacePtr<IUnitOfMeasureSettings>	iUOMPref((IUnitOfMeasureSettings*)
			::QueryPreferences(IID_IUNITOFMEASURESETTINGS, GetExecutionContextSession()));
		if (iUOMPref == nil)
		{
			break;
		}
		// Get the measurement system interface from the session boss
		InterfacePtr<IMeasurementSystem>	iMeasureSystem(GetExecutionContextSession(), UseDefaultIID());
		if (iMeasureSystem == nil)
		{
			break;
		}
		// Get the XUnitOfMeasure index
		int16 unitIndex = iUOMPref->GetXUnitOfMeasureIndex();
		// Use the index to get the unit of measurement
		InterfacePtr<IUnitOfMeasure> iUOM(iMeasureSystem->QueryUnitOfMeasure(unitIndex));
		if (iUOM == nil)
		{
			break;
		}
		// Converting 1 unit of this unit of measurement to points
		
		iUOM->GetName(&NameIOM);
		if(NameIOM=="CENTIMETERS" || NameIOM=="CENTIMETROS" || NameIOM=="Centimetros" || NameIOM=="Centimeters")
			NameIOM="cm";
		else
			if(NameIOM=="POINTS" || NameIOM=="PUNTOS" || NameIOM=="Puntos" || NameIOM=="Points" || NameIOM=="puntos")
				NameIOM="pts";
			else
				if(NameIOM=="Inches" || NameIOM=="INCHES" || NameIOM=="Pulgadas" || NameIOM=="PULGADAS")
					NameIOM="¥¥";
				else
					if(NameIOM=="MILIMITERS" || NameIOM=="MILIMITROS" || NameIOM=="Milimiters" || NameIOM=="Milimetros")
						NameIOM="mm";
					else
                        if(NameIOM=="Ciceros" || NameIOM=="CICEROS" )
							NameIOM="ciceros";


		
		PMReal pointValue = iUOM->PointsToUnits(Cantidad);
		return(pointValue);
	}while(false);
	return(0);
}

/*bool16 N2PSQLUtilities::LlaveDongleCorrecta()
{
	bool16 retval=kFalse;
	if(LlaveCorrecta())
		retval=kTrue;
	return(retval);
}*/

 
PMString N2PSQLUtilities::CrearFolderPreferencias()
{
	PMString Archivo = "";
	PMString SeparatorPath="";
	 #if defined(MACINTOSH)
		SDKUtilities::GetApplicationFolder(Archivo);
		//Archivo=InterlasaUtilities::GetFolderLibraryOfShared();
		SeparatorPath=":";
	#elif defined(WINDOWS)
		Archivo="C:\\Windows";	
		SeparatorPath="\\";
	#endif,
	
	
	Archivo.Append(SeparatorPath + "asalretni" + SeparatorPath);
	IDFile fileFolder;
		
		FileUtils::PMStringToIDFile(Archivo,fileFolder);
		
		if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
		{//Fue creado  
			//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
			Archivo.Append("Software" + SeparatorPath);
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{//fue creado
				//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
				Archivo.Append("News2PageSQL" + SeparatorPath);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					}
					else
					{
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//Archivo.Append("C2PPref.pfa");
					}
				}

			}
			else
			{
				//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
				Archivo.Append("News2PageSQL" + SeparatorPath);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					}
				}
				else
				{
					//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					}
				}
			}
		}
		else//ya habia sido creado
		{
			//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
			Archivo.Append("Software" + SeparatorPath);
		//	Archivo.Append("News2PageSQL:");
		//	Archivo.Append("Preferencias:");
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{//fue creado
				//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
				Archivo.Append("News2PageSQL" + SeparatorPath);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
			else
			{
				//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
				Archivo.Append("News2PageSQL" + SeparatorPath);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					}
					else
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					}
				}
				else
				{
					//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
					Archivo.Append("Preferencias" + SeparatorPath);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{//fue Creado
						//chmod(InterlasaUtilities::MacToUnix(Archivo).GrabCString(),0777);
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
		}
	
	PMString Element=Archivo;
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	SDKUtilities::RemoveLastElement(Element);
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	SDKUtilities::RemoveLastElement(Element);
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	SDKUtilities::RemoveLastElement(Element);
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	SDKUtilities::RemoveLastElement(Element);
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	return(Archivo);
}


PMString N2PSQLUtilities::CrearFolderRespaldoNotas(PMString Servidor,PMString Fecha,PMString Publicacion,PMString Seccion, PMString Usuario)
{
	PMString Archivo = Servidor;
	
	SDKUtilities::AppendPathSeparator(Archivo);
	Archivo.Append("respaldo_notas");
	IDFile fileFolder;
		
		FileUtils::PMStringToIDFile(Archivo,fileFolder);
	
		if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
		{
			
		//Fue creado  
			SDKUtilities::AppendPathSeparator(Archivo);
			Archivo.Append(Fecha);
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{
				
				//fue creado
				SDKUtilities::AppendPathSeparator(Archivo);
				Archivo.Append(Publicacion);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{
					
					//fue creado
					SDKUtilities::AppendPathSeparator(Archivo);
					Archivo.Append(Seccion);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{
						
						//fue Creado
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					else
					{
						
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					
				}
				else
				{
					SDKUtilities::AppendPathSeparator(Archivo);
					Archivo.Append(Seccion);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					else
					{
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
				}

			}
			else
			{
				SDKUtilities::AppendPathSeparator(Archivo);
				Archivo.Append(Publicacion);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					SDKUtilities::AppendPathSeparator(Archivo);
					Archivo.Append(Seccion);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					else
					{
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					
				}
				else
				{
					SDKUtilities::AppendPathSeparator(Archivo);
					Archivo.Append(Seccion);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					else
					{
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
				}
			}
		}
		else//ya habia sido creado
		{//Fue creado  
			SDKUtilities::AppendPathSeparator(Archivo);
			Archivo.Append(Fecha);
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{//fue creado
				SDKUtilities::AppendPathSeparator(Archivo);
				Archivo.Append(Publicacion);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					SDKUtilities::AppendPathSeparator(Archivo);
					Archivo.Append(Seccion);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					else
					{
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					
				}
				else
				{
					SDKUtilities::AppendPathSeparator(Archivo);
					Archivo.Append(Seccion);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					else
					{
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
				}

			}
			else
			{
				
				SDKUtilities::AppendPathSeparator(Archivo);
				Archivo.Append(Publicacion);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					SDKUtilities::AppendPathSeparator(Archivo);
					Archivo.Append(Seccion);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					else
					{
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					
				}
				else
				{
					
					SDKUtilities::AppendPathSeparator(Archivo);
					Archivo.Append(Seccion);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
					else
					{
						
						SDKUtilities::AppendPathSeparator(Archivo);
						Archivo.Append(Usuario);
						
						FileUtils::PMStringToIDFile(Archivo,fileFolder);
						if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
						{
							
						}
					}
				}
			}
		}
	PMString Element=Archivo;
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	SDKUtilities::RemoveLastElement(Element);
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	SDKUtilities::RemoveLastElement(Element);
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	SDKUtilities::RemoveLastElement(Element);
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	SDKUtilities::RemoveLastElement(Element);
	chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
	
	return(Archivo);
}

void N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL(PMString Buscar, 
													 bool16 isOnPasesNotas, 
													 PreferencesConnection PrefConections)
{

	do
	{	
		bool16 BusquedaDeNotasSobrePagina=kTrue;
		//CAlert::InformationAlert(Buscar);
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			N2PSQLUtilities::CleanPalette();
			break;
		}
	
		if(Buscar=="simple")
		{
			PMString N2PSQLFolioPag = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
			
			if(N2PSQLFolioPag.NumUTF16TextChars()<=0)
			{
				N2PSQLUtilities::CleanPalette();
				break;
			}
			
			Buscar = "SELECT Id_elemento_Padre AS elemento, Estatus_Colocado AS Estatus_Colocado, Id_Elemento AS Id_ElementoContenido  FROM Elemento WHERE Id_Tipo_ele=8 AND Id_elemento_padre IN(SELECT ID_Elemento FROM Elemento_Por_pagina WHERE Pagina_ID= "+ N2PSQLFolioPag + ")  AND Id_elemento_padre IN ( SELECT Id_elemento FROM Elemento WHERE  Id_elemento_padre =  '0' OR Id_elemento_padre =  NULL  OR Id_elemento_padre =  ' '  AND Id_tipo_ele=10)";
			//Buscar = "SELECT Id_elemento_Padre AS elemento,(SELECT E.Nombre FROM Elemento E WHERE E.Id_Elemento=Id_elemento_Padre) as Nombre Estatus_Colocado AS Estatus_Colocado, Id_Elemento AS Id_ElementoContenido  FROM Elemento WHERE Id_Tipo_ele=8 AND Id_elemento_padre IN(SELECT ID_Elemento FROM Elemento_Por_pagina WHERE Pagina_ID= "+ N2PSQLFolioPag + ")";
		}
		else
		{
			BusquedaDeNotasSobrePagina=kFalse;
		}
		//CAlert::InformationAlert(Buscar);
		WidgetID ListALlenaWidgetID=nil;
	
		
		//Para indicar cual lista es la que se debe de llenar
		if(isOnPasesNotas)
		{
			
			ListALlenaWidgetID = kN2PsqlPasesListBoxWidgetID;
		}
		else
		{
			
			ListALlenaWidgetID = kN2PsqlNotasListBoxWidgetID;
		}
			
		
		int32 herencia=0;
		
		PMString Contenido="";
		PMString Secc="";
		PMString Estado="";
		PMString NotaFluida="";
		PMString IDElementoPadre="";
		PMString IDElemento="";
		
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		InterfacePtr<IControlView>		iControViewList( panel->FindWidget(ListALlenaWidgetID), UseDefaultIID() );
		if(iControViewList==nil)
		{
			CAlert::ErrorAlert("No se encontro el editbox");
			break;
		}

		
		N2PSQLListBoxHelper listHelper(iControViewList, kN2PSQLPluginID,ListALlenaWidgetID);
		listHelper.EmptyCurrentListBox();

		PMString cadena;

	
	
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		
/*		
		PMString NamePreferencia="Default"; //N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
*/		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}
			
		K2Vector<PMString> QueryVector;
		K2Vector<PMString> QueryContenido;
		
		
		
		 
		SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryVector);
		
		if(QueryVector.Length()<=0)
		{
			
			//No se encontraron notas para este criterio de busqueda
			if(!BusquedaDeNotasSobrePagina)
			{
				PMString NoNotas=PMString(kN2PsqlNoSeEncontraronNotasPlugInMenuKey);
				NoNotas.Translate();
  				NoNotas.SetTranslatable(kTrue);
				CAlert::InformationAlert(NoNotas);
			}
				
			break;
		}
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			IDElementoPadre = SQLInterface->ReturnItemContentbyNameColumn("elemento",QueryVector[i]);
			IDElementoPadre.SetTranslatable(kFalse);
			
			
			
			IDElemento = SQLInterface->ReturnItemContentbyNameColumn("Id_ElementoContenido",QueryVector[i]);
			IDElemento.SetTranslatable(kFalse);
			
			//Si es solo notas pases entonces no se hace la busqueda de notas hijas
			if(!isOnPasesNotas)
			{
					//CAlert::ErrorAlert("XXXX11111");
					LLena_Subnaotas_D_Nota(IDElementoPadre, listHelper, isOnPasesNotas, herencia,PrefConections);
					//CAlert::ErrorAlert("XXXX22222");
			}
			
			
			QueryContenido.clear();
			Buscar = "SELECT Nombre AS Contenido FROM Elemento WHERE Id_Elemento=" + IDElementoPadre;//Buscar= "CALL Return_Contenido_De_Id_Elemento (" + IDElemento + ")";//
			SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
			
			
			for(int32 j=0;j<QueryContenido.Length();j++)
			{
				
				Contenido = SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j]);
				
				K2Vector<N2PNotesStorage*> fBNotesStore;
				//Importa TextoTageado si es que el texto viene en formato tageado
				UIDRef RefTagetTextImported = ImportTaggedTextOfPMString(Contenido, fBNotesStore);
				
				if(RefTagetTextImported != UIDRef::gNull)
				{	//convierte el texto tageado a texto plano
					Contenido = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);
				}
				
				
				
				Contenido.SetTranslatable(kFalse);
				
				
			
				if(isOnPasesNotas)
				{
					//CAlert::InformationAlert("Es solo pases");
					listHelper.AddElement(Contenido,kN2PsqlContenidoPaselLabelWidgetID,0);//UIDModel
					listHelper.SetTextoEnSeleccionActual(kN2PsqlIDPadrePaseLabelWidgetID,IDElementoPadre,0);//Item para realizar la busqueda
					listHelper.SetTextoEnSeleccionActual(kN2PsqlIDPaseLabelWidgetID,IDElemento,0);
					
				}
				else
				{
					listHelper.AddElement(Contenido,kN2PsqlNotaLabelWidgetID,0);//UIDModel
					listHelper.SetTextoEnSeleccionActual(kN2PsqlIDNotaLabelWidgetID,IDElementoPadre,0);//Item para realizar la busqueda
					
				}
			}
			
			
			
			
			NotaFluida="";
			NotaFluida=SQLInterface->ReturnItemContentbyNameColumn("Estatus_Colocado",QueryVector[i]);
			NotaFluida.SetTranslatable(kFalse);
			
			if(NotaFluida.GetAsNumber()==1)
			{
				listHelper.Mostrar_IconFluido(0);	
			}
			
			
			//CAlert::InformationAlert("EXIT Busqueda");
			
		
		}
	}while(false);
}



void N2PSQLUtilities::LLena_Subnaotas_D_Nota(PMString Id_Nota_Padre, 
											 N2PSQLListBoxHelper listHelper, 
											 bool16 isOnPasesNotas, 
											 int32 Herencia, 
											 PreferencesConnection PrefConections)
{
	do
	{
		int32 herencia = Herencia + 1;
		//Busqueda de notas hijas
		K2Vector<PMString> QueryVectorNotasHijas;
		K2Vector<PMString> QueryContenido;
		PMString Contenido="";
		PMString Secc="";
		PMString Estado="";
		PMString Buscar="";
		PMString NotaFluida="";
		PMString IDElementoPadre="";
		PMString IDElemento="";
		
		QueryVectorNotasHijas.clear();
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
/*		PreferencesConnection PrefConections;
		
		PMString NamePreferencia="Default"; //N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
*/		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}
			
		Buscar = "CALL Select_SubNotas_DeNotaPrimaria(" + Id_Nota_Padre + ")";
		SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryVectorNotasHijas);
			
			for(int32 k=0;k<QueryVectorNotasHijas.Length();k++)
			{
				//CAlert::InformationAlert("Notas hijas " + QueryVectorNotasHijas[k]);
				IDElementoPadre = SQLInterface->ReturnItemContentbyNameColumn("elemento",QueryVectorNotasHijas[k]);
				IDElementoPadre.SetTranslatable(kFalse);
			
			
			
				IDElemento = SQLInterface->ReturnItemContentbyNameColumn("Id_ElementoContenido",QueryVectorNotasHijas[k]);
				IDElemento.SetTranslatable(kFalse);
				
				if(!isOnPasesNotas)
				{
					if(IDElementoPadre.NumUTF16TextChars()>0)
						LLena_Subnaotas_D_Nota(IDElementoPadre, listHelper, isOnPasesNotas, herencia, PrefConections);
				}
			
				QueryContenido.clear();
				Buscar = "SELECT Nombre AS Contenido FROM Elemento WHERE Id_Elemento=" + IDElementoPadre;//Buscar= "CALL Return_Contenido_De_Id_Elemento (" + IDElemento + ")";//
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
				
			
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					
					Contenido="";
					Contenido.Append(  SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j])  );
					
					K2Vector<N2PNotesStorage*> fBNotesStore;
					//Importa TextoTageado si es que el texto viene en formato tageado
					UIDRef RefTagetTextImported = ImportTaggedTextOfPMString(Contenido, fBNotesStore);
				
					if(RefTagetTextImported != UIDRef::gNull)
					{	//convierte el texto tageado a texto plano
						Contenido = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);
					}
					
					switch(Herencia)
					{
						case 0:
							Contenido="¨ "+Contenido;
							break;
							
						case 1:
							Contenido=" -¨ "+Contenido;
							break;
							
						case 2:
							Contenido=" --¨ "+Contenido;
							break;
							
						case 3:
							Contenido="	---¨ "+Contenido;
							break;
					}
					Contenido.SetTranslatable(kFalse);
			
					if(isOnPasesNotas)
					{
						//CAlert::InformationAlert("Es solo pases");
						listHelper.AddElement(Contenido,kN2PsqlContenidoPaselLabelWidgetID,0);//UIDModel
						listHelper.SetTextoEnSeleccionActual(kN2PsqlIDPadrePaseLabelWidgetID,IDElementoPadre,0);//Item para realizar la busqueda
						listHelper.SetTextoEnSeleccionActual(kN2PsqlIDPaseLabelWidgetID,IDElemento,0);
					
					}
					else
					{
						listHelper.AddElement(Contenido,kN2PsqlNotaLabelWidgetID,0);//UIDModel
						listHelper.SetTextoEnSeleccionActual(kN2PsqlIDNotaLabelWidgetID,IDElementoPadre,0);//Item para realizar la busqueda
					
					}
				}
			
				NotaFluida="";
				NotaFluida=SQLInterface->ReturnItemContentbyNameColumn("Estatus_Colocado",QueryVectorNotasHijas[k]);
				NotaFluida.SetTranslatable(kFalse);
			
				if(NotaFluida.GetAsNumber()==1)
				{
					listHelper.Mostrar_IconFluido(0);	
				}
			}
			/////////
	}while(false);
	
}



void N2PSQLUtilities::RealizarBusqueda_Y_llenado(PMString Busqueda, bool16 IsOnlyPases, PreferencesConnection PrefConections)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::RealizarBusqueda_Y_llenado ini");
	do
	{
		//CAlert::InformationAlert(Busqueda);
		PMString Nota="";
		PMString Fecha="";
		PMString NomPublicacion="";
		PMString Publicacion="";
		PMString Seccion="";
		PMString NomSeccion="";
		PMString Titulo="";
		PMString Balazo="";
		PMString Pie="";
		PMString Pagina="";
		PMString Ruta="";
		PMString Estatus="";
		PMString Serial="";
		PMString GuiaNota="";
		PMString EditandoPorUsuario="";
		PMString FYHUCheckinNota="";
		
		PMString ID_Ele_NotaContent="";
		PMString ID_Ele_TituContent="";
		PMString ID_Ele_BalaContent="";
		PMString ID_Ele_PieContent="";
		bool16 TraesElementosConPases=kTrue;
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		ASSERT(app);
		if (app == nil) {	
			break;
		}
		
		PMString Aplicacion=app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		if(Aplicacion.Contains("InCopy"))
			break;
	
	
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
/*		PreferencesConnection PrefConections;
		
		PMString NamePreferencia="Default"; //N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
*/		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}	
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//CAlert::InformationAlert(Busqueda);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			//CAlert::InformationAlert(QueryVector[i]);
			
			
		
			PMString Id_tipo_ele = SQLInterface->ReturnItemContentbyNameColumn("Id_tipo_ele",QueryVector[i]);
			
			
			if(Id_tipo_ele=="5" || Id_tipo_ele=="1")////Id tipo de elemento es igual a Pase o nota o Contenido de nota
			{
				if(!IsOnlyPases)
				{
					ID_Ele_NotaContent = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",QueryVector[i]);
					ID_Ele_NotaContent.SetTranslatable(kFalse);
					
					Nota = N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, ID_Ele_NotaContent, TraesElementosConPases);
				}
				
				
				
				
				Serial=SQLInterface->ReturnItemContentbyNameColumn("Id_elemento_padre",QueryVector[i]);
				Serial.SetTranslatable(kFalse);
				
				Fecha = SQLInterface->ReturnItemContentbyNameColumn("DATE_EDITION",QueryVector[i]);
				Fecha.SetTranslatable(kFalse);
				
				Publicacion = SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
				Publicacion.SetTranslatable(kFalse);
				
				Seccion = SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
				Seccion.SetTranslatable(kFalse);
				
				Pagina = SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",QueryVector[i]);
				Pagina.SetTranslatable(kFalse);
				
				Estatus = SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
				Estatus.SetTranslatable(kFalse);
				
				FYHUCheckinNota = SQLInterface->ReturnItemContentbyNameColumn("Fecha_ult_mod",QueryVector[i]);
				FYHUCheckinNota.SetTranslatable(kFalse);
				
				NomPublicacion= SQLInterface->ReturnItemContentbyNameColumn("NomPublicacion",QueryVector[i]);
				NomPublicacion.SetTranslatable(kFalse);
				
				NomSeccion= SQLInterface->ReturnItemContentbyNameColumn("NomSeccion",QueryVector[i]);
				NomSeccion.SetTranslatable(kFalse);
				
				GuiaNota = SQLInterface->ReturnItemContentbyNameColumn("Nombre",QueryVector[i]);
				GuiaNota.SetTranslatable(kFalse);
			}
			
			
			if(Id_tipo_ele=="8")//Id tipo de elemento es igual a Titulo de una nota
			{	ID_Ele_TituContent=SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",QueryVector[i]);
				ID_Ele_TituContent.SetTranslatable(kFalse);
				
				Titulo =   N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, ID_Ele_TituContent, TraesElementosConPases);
				Titulo.SetTranslatable(kFalse);
				
				
			}
			
			if(Id_tipo_ele=="7")//Id tipo de elemento es igual a Balazo de una nota
			{	ID_Ele_BalaContent=SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",QueryVector[i]);
				ID_Ele_BalaContent.SetTranslatable(kFalse);
				
				Balazo =  N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, ID_Ele_BalaContent, TraesElementosConPases);
				Balazo.SetTranslatable(kFalse);
				
				
			}
		
			
			if(Id_tipo_ele=="6")//Id tipo de elemento es igual a Pie de Foto de una nota
			{	
				ID_Ele_PieContent=SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",QueryVector[i]);
				ID_Ele_PieContent.SetTranslatable(kFalse);
				
				Pie =  N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, ID_Ele_PieContent, TraesElementosConPases);
				Pie.SetTranslatable(kFalse);
			}
			
						
			//EditandoPorUsuario = SQLInterface->ReturnItemContentbyNameColumn("LockByUset",QueryVector[i]);
			
		}
		
			
			
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlStTextPubliWidgetID,NomPublicacion);
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlSeccionLabelWidgetID,NomSeccion);
			
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPageWidgetID,Pagina);
		
		//CAlert::InformationAlert("N2PSQLUtilities::RealizarBusqueda_Y");
		N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(Seccion);
		
		if(PrefConections.N2PUtilizarGuias==1)
			N2PSQLUtilities::SetInComboSobrePaletaN2P(kN2PsqlComboguiaWidgetID,GuiaNota);
		else
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextEnGuiaWidgetID,GuiaNota);
		
			//CAlert::InformationAlert(" N2PSQLUtilities::RealizarBusqueda_Y_llenado");
			/////
			//Importa TextoTageado si es que el texto viene en formato tageado
			K2Vector<N2PNotesStorage*> fBNotesStore1;
			UIDRef RefTagetTextImported = ImportTaggedTextOfPMString(Titulo, fBNotesStore1);
			UIDRef CopiaRef= RefTagetTextImported;
			UID uid=CopiaRef.GetUID();
			PMString UIDString=""; 
			UIDString.AppendNumber(uid.Get()); 
			//Titulo="";
			//Titulo.AppendNumber();
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlUIDRefTexModelDeTitulo_WidgetID,UIDString);
			
			
			/*
				Contenido = SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j]);
				
				//Importa TextoTageado si es que el texto viene en formato tageado
				UIDRef RefTagetTextImported = ImportTaggedTextOfPMString(Contenido);
				
				if(RefTagetTextImported != UIDRef::gNull)
				{	//convierte el texto tageado a texto plano
					Contenido = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);
				}
			*/	
			if(RefTagetTextImported != UIDRef::gNull)
			{	//convierte el texto tageado a texto plano
				Titulo = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported); //convierte el texto tageado a texto plano
			}
			
			
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextTituloWidgetID,Titulo);
			
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_Titulo_WidgetID,ID_Ele_TituContent);
			
			/////
			//Importa TextoTageado si es que el texto viene en formato tageado
			K2Vector<N2PNotesStorage*> fBNotesStoreBalazo;
			RefTagetTextImported = ImportTaggedTextOfPMString(Balazo, fBNotesStoreBalazo);
		
			CopiaRef= RefTagetTextImported;
			uid=CopiaRef.GetUID();
			UIDString=""; 
			UIDString.AppendNumber(uid.Get()); 
			
			//Balazo="";
			//Balazo.AppendNumber(uid.Get());
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlUIDRefTexModelDeBalazo_WidgetID,UIDString);
			
			if(RefTagetTextImported != UIDRef::gNull)
			{	//convierte el texto tageado a texto plano
				Balazo = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);//convierte el texto tageado a texto plano
			}			
			
		
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextBalazoWidgetID,Balazo);
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_Balazo_WidgetID,ID_Ele_BalaContent);
			
			
			/////
			if(!IsOnlyPases)
			{
				//Importa TextoTageado si es que el texto viene en formato tageado
				K2Vector<N2PNotesStorage*> fBNotesStore;
				RefTagetTextImported = ImportTaggedTextOfPMString(Nota, fBNotesStore);
				CopiaRef= RefTagetTextImported;
				uid=CopiaRef.GetUID();
				UIDString=""; 
				UIDString.AppendNumber(uid.Get()); 
				//Nota="";
				//Nota.AppendNumber(uid.Get());
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlUIDRefTexModelDeNota_WidgetID,UIDString);
				if(RefTagetTextImported != UIDRef::gNull)
				{	//convierte el texto tageado a texto plano
					Nota = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);//convierte el texto tageado a texto plano
				}
					
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextNotaWidgetID,Nota);
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_ContentNota_WidgetID,ID_Ele_NotaContent);
			}
			
			
			/////
			//Importa TextoTageado si es que el texto viene en formato tageado
			K2Vector<N2PNotesStorage*> fBNotesStorePie;
			RefTagetTextImported = ImportTaggedTextOfPMString(Pie, fBNotesStorePie);
			CopiaRef= RefTagetTextImported;
			uid=CopiaRef.GetUID();
			UIDString=""; 
			UIDString.AppendNumber(uid.Get()); 
			//Pie="";
			//Pie.AppendNumber(uid.Get());
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlUIDRefTexModelDePieFoto_WidgetID,UIDString);
			if(RefTagetTextImported != UIDRef::gNull)
			{	//convierte el texto tageado a texto plano
				Pie = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported);//convierte el texto tageado a texto plano
			}
			
			
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPieWidgetID,Pie);
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_Pie_WidgetID,ID_Ele_PieContent);
			
			/////
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextFechaWidgetID,Fecha);
			
			/////
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextStatusWidgetID,Estatus);
			
			/////
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlComboPubliWidgetID,Publicacion);
			

			/////
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlComboSeccionWidgetID,Seccion);
			
			/////
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_PadreNotaWidgetID,Serial);
			/////
			N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlLastCheckInNoteTextEditWidgetID,FYHUCheckinNota);
		
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::RealizarBusqueda_Y_llenado fin");
}


/* TruncatePath
*/
PMString N2PSQLUtilities::TruncatePath(const PMString& fromthis)
{
	if(fromthis.IsEmpty() == kTrue) return PMString("Empty!");

	PMString retval = fromthis;
	int32 lastpos = (-1);
	for (int32 i = 0 ; i < fromthis.CharCount();i++)
	{
		bool16 predicate = (fromthis[i] == PLATFORM_PATH_DELIMITER);
		if (predicate)
		{
			lastpos = i;
		}
	}

	if(lastpos >= 0)
	{
		// Suppose we have ../X/Y.gif
		// Then, lastpos would be 4, and we'd want 5 chars from 5 onwards
		int32 countChars = fromthis.CharCount() - (lastpos+1);
		int32 startIndex = lastpos+1;
		int32 endIndex = (startIndex + countChars);
		if((endIndex > startIndex) && (endIndex <= fromthis.CharCount()))
		{
			K2::scoped_ptr<PMString> ptrRightStr(fromthis.Substring(startIndex, countChars));
			if(ptrRightStr)
			{
				retval = *ptrRightStr;
			}
		}
	} 
	return retval;
}


bool16 N2PSQLUtilities::validPath(const PMString& p)
{
	const PMString thisDir(".");
	const PMString parentDir("..");
	return p != thisDir && p != parentDir;
}




bool16 N2PSQLUtilities::CambiaEstadoEnCheckBoxWidgetEnN2PSQLPaleta(WidgetID widget,bool16 selected)
{
	bool16 retval=kFalse;
	do
	{
		
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::CambiaEstadoEnCheckBoxWidgetEnN2PSQLPaleta panel pointer nil");
			break;
		}
		
		IControlView *iControlView = panel->FindWidget(widget);
		if(iControlView==nil)
		{
			ASSERT_FAIL("N2PSQLUtilities::CambiaEstadoEnCheckBoxWidgetEnN2PSQLPaleta iControlView invalid");
			break;
		}
		
		InterfacePtr<ITriStateControlData> StateControl(iControlView,ITextControlData::kDefaultIID);
		if (StateControl == nil)
		{
			ASSERT_FAIL("N2PSQLUtilities::CambiaEstadoEnCheckBoxWidgetEnN2PSQLPaleta ITriStateControlData invalid");
			
			break;
		}
		
		
		if(selected==kTrue)
		{
			const ITriStateControlData::TriState trystate=ITriStateControlData::kSelected;
			const bool16 invalidate=kTrue;
			const bool16 notify=kTrue;
			StateControl->SetState(trystate,invalidate, notify);
			//StateControl->SetState(trystate );
			
			//CAlert::InformationAlert("select");
		}
		else
		{
			const ITriStateControlData::TriState trystate=ITriStateControlData::kUnselected;
			const bool16 invalidate=kTrue;
			const bool16 notify=kTrue;
			StateControl->SetState(trystate,invalidate, notify);
			//StateControl->Select();
			//CAlert::InformationAlert("Unselect");
		}
		
		 retval=kTrue;
	}while(false);
	return(kTrue);
}


bool16 N2PSQLUtilities::GetEstateCkBoxOfWidget(WidgetID Widget)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::CambiaEstadoEnCheckBoxWidgetEnN2PSQLPaleta panel pointer nil");
			break;
		}
		
		IControlView*	textWid = panel->FindWidget(Widget);
		if (textWid == nil)
		{
			ASSERT_FAIL("Control view for text widget is nil?");
			break;
		}

		InterfacePtr<ITriStateControlData> theCheckBox(textWid,UseDefaultIID());
		if (theCheckBox == nil)
		{
			ASSERT_FAIL("No theCheckBox control data interface for widget?");
			break;
		}

		if(theCheckBox->IsSelected())
		{
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
		
	}while(false);
return(retval);
}


int32 N2PSQLUtilities::GetUIDOfTextFrameSelect(int32& NumPageOfTextModel)
{
	int32 UIDTexModelOfMyFrame=0;
	
	do
	{
		UID UIDTextModel = kInvalidUID;
		
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}

		
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionUtils pointer nil");
			break;
		}

	
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionManager pointer nil");
			break;
		}

		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kTextSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel!=nil)
		{
			InterfacePtr<ITextTarget> pTextTarget(pTextSel, UseDefaultIID());
			if(pTextTarget==nil)
			{
				ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect pTextTarget pointer nil");
				break;
			}

			InterfacePtr<ITextFocus> pFocus(pTextTarget->QueryTextFocus());
			if(pFocus!=nil)
			{
				InterfacePtr<ITextModel> iTextModel(pFocus->QueryModel());
				if(iTextModel==nil)
				{
					ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iTextModel pointer nil");
					break;
				}
			
				InterfacePtr<IFrameList> frameList(iTextModel->QueryFrameList());
				if(!frameList)
				{
					 ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect frameList pointer nil");
					break;
				}
			
				UIDTextModel = frameList->GetTextModelUID();
			}
			else
			{
				InterfacePtr<IConcreteSelection> pLayoutSele(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
				if(pTextSel==nil)
				{
					ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect pLayoutSele pointer nil");
					break;
				}


				UIDRef selectedItemRef = UIDRef::gNull;
				UID selectedItem = kInvalidUID;
			
				InterfacePtr<ILayoutTarget> pLayoutTarget(pLayoutSele, UseDefaultIID());    
				if (pLayoutTarget)
				{
					UIDList itemList = pLayoutTarget->GetUIDList(kStripStandoffs);
					if (itemList.Length() == 1) 
					{
						// get the database
						db = itemList.GetDataBase();
						// get the UID of the selected item
						selectedItem = itemList.At(0);

						selectedItemRef=UIDRef(db,selectedItem);
					}
					else
					{
						//Tienes mas de un frame seleccionado
						 ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect (itemList.Length() >1 ");
						break;
					}
				}
				else
				{
					//CAlert::InformationAlert("False");
					ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect pLayoutTarget ponter is nil ");
					break;
				}
			
			
			
			
				//Para asegurarnos que es un text frame
				Utils<IPageItemTypeUtils> pageItemTypeUtils;
			
				if(!pageItemTypeUtils &&  pageItemTypeUtils->IsTextFrame(selectedItemRef))
				{
					ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect pageItemTypeUtils ponter is nil ");
					break;	
				}
			
				UID UIDTextContent=kInvalidUID;
				InterfacePtr<IGraphicFrameData> graphicFrameData(selectedItemRef, UseDefaultIID());
					if(!graphicFrameData)
				{
					ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect graphicFrameData ponter is nil ");
					break;
				}
				
			
				UIDTextContent = graphicFrameData->GetTextContentUID();
			
				InterfacePtr<ITextColumnSizer> textColumnSizer(selectedItemRef.GetDataBase(), UIDTextContent , UseDefaultIID());
				if(!textColumnSizer)
				{
					ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect textColumnSizer ponter is nil ");
					break;
				}
				
			
			
				InterfacePtr<IMultiColumnTextFrame > textColumnFrame(textColumnSizer , UseDefaultIID());
				if(!textColumnFrame)
				{
					ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect textColumnFrame ponter is nil ");
					break;
				}
				
			
				InterfacePtr<IFrameList> frameList(textColumnFrame->QueryFrameList());
				if(!frameList)
				{
					ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect frameList ponter is nil ");
					break;
				}
				
			
				UIDTextModel = frameList->GetTextModelUID();
				
			}
	
	
			
		}
		
		UIDTexModelOfMyFrame=UIDTextModel.Get();
		
	}while(false);
	return(UIDTexModelOfMyFrame);
}


bool16 N2PSQLUtilities::GetIDNota_ConIDTextFrame_SobreXMPNotasFluidas(int32 IDFrame, PMString& IDNota,bool16& PuedeModificarGuia)
{
	bool16 encontroFrame=kFalse;
	
	do
	{
		IDocument* document =  Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString DatosEnXMP = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		//Si encontro la Cadena	
		if(DatosEnXMP.NumUTF16TextChars()<=0)
		{
			
			break;
		}
		
		//Obtiene la cantidad de notas que se encuentren en la pagina 
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		if(CantDatosEnXMP<=0)
		{
			
			break;
		}
		
		ClassRegEnXMP *RegistrosEnXMPTOUpdate=new ClassRegEnXMP[CantDatosEnXMP];
		
		N2PSQLUtilities::LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMPTOUpdate);
		
		int32 NumReg=0;
		int32 numregRep=0;
		
		//Busca el ID del Frame seleccionado para obtener el ID de la nota que fue fluida sobre el
		
		for(NumReg	=0 ; NumReg < CantDatosEnXMP  &&  encontroFrame==kFalse; NumReg++)
		{
			PMString jh="ID ";
			jh.Append( RegistrosEnXMPTOUpdate[NumReg].IDElemPadre);
			jh.Append(" en registro ");
			jh.AppendNumber( NumReg);
			
			if(IDFrame == RegistrosEnXMPTOUpdate[NumReg].IDFrame)
			{
				
				if(RegistrosEnXMPTOUpdate[NumReg].TipoDato=="PieFoto")
				{
					IDNota = RegistrosEnXMPTOUpdate[NumReg].IDElemento;
					PuedeModificarGuia=kFalse;
				}
				else
				{
					IDNota = RegistrosEnXMPTOUpdate[NumReg].IDElemPadre;
					PuedeModificarGuia=kTrue;
				}
				
				encontroFrame = kTrue;
			}
		}
		//delete RegistrosEnXMPTOUpdate;
	}while(false);
	
	return(encontroFrame);
}


bool16 N2PSQLUtilities::EnableWidgetEnN2PSQLPaleta(WidgetID widget,bool16 Enable)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		IControlView *iControlView = panel->FindWidget(widget);
		if(iControlView==nil)
		{
			break;
		}
		
		if(Enable==kTrue)
		{
			iControlView->Enable();
		}
		else
		{
			iControlView->Disable();
		}
		
		retval=kTrue;
	}while(false);
	return(retval);
}

bool16 N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(WidgetID widget,bool16 Mostrar)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		IControlView *iControlView = panel->FindWidget(widget);
		if(iControlView==nil)
		{
			break;
		}
		
		if(Mostrar==kTrue)
		{
			iControlView->Show(Mostrar);
		}
		else
		{
			iControlView->Hide();
		}
		
		retval=kTrue;
	}while(false);
	return(retval);
}

bool16 N2PSQLUtilities::IsShowWidgetEnN2PSQLPaleta(WidgetID widget)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		IControlView *iControlView = panel->FindWidget(widget);
		if(iControlView==nil)
		{
			break;
		}
		
		if(iControlView->GetVisibleState())
		{
			retval=kTrue;
		}
		else
		{
			retval=kFalse;
		}
		
	}while(false);
	return(retval);
}



PMString N2PSQLUtilities::GetTextPlainOfUIDRefImportTextTagged(UIDRef refTextTaggedImported)
{
	PMString texto="";
	do
	{
		
		InterfacePtr<ITextModel> mytextmodel(refTextTaggedImported , UseDefaultIID());
		if(mytextmodel==nil)
		{
			//CAlert::ErrorAlert("mytextmodel possible"); 
			break;
		}

					
		InterfacePtr<IComposeScanner> cs(mytextmodel, UseDefaultIID()); 
		// Setup a variable to retrieve the length of the text 
		if(cs==nil)
		{
			CAlert::ErrorAlert("cs possible"); 
			break;
		}
		// Setup a variable to retrieve the length of the text 
		int32 length = mytextmodel->TotalLength();//obtengo el total de caracteres de la cadena en el textframe
		int32 index=0;//numero de caracter leido
		WideString Temp;
		cs->CopyText(index, length-1, &Temp);
		// Setup a PMString to hold the text (depending on what you want to do with the text, the textchar may be ok) 
		// Load the text into the buf variable 
		//buf.SetXString(buffer, length); 
		texto="";
		texto.Append(Temp);
		
	}while(false);
	return(texto);
}


PMString N2PSQLUtilities::GetPMStringOfTextFrame(int32 UIDModelString)	
{	
	PMString texto="";
	do
	{
		////////obtener Uid del TextModel
					
					PMString Consulta="";
					int32 intthis=UIDModelString;
					UID UIDTextModel=intthis;
					
				//////////////////////Obtener Base de taso para el Textmodel
					IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
					if (document == nil)
					{
						break;
					}
					IDataBase* db = ::GetDataBase(document);
					if (db == nil)
					{
						ASSERT_FAIL("db is invalid");
						break;
					}
				////////////////////Obtener texto de Texframe
					UIDRef TextModelUIDRef(db,UIDTextModel);
					InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
					if(mytextmodel==nil)
					{
						break;
					}
					InterfacePtr<IComposeScanner> cs(mytextmodel, UseDefaultIID()); 
					// Setup a variable to retrieve the length of the text 
					if(cs==nil)
					{
						break;
					}
					// Setup a variable to retrieve the length of the text 
					int32 length = mytextmodel->TotalLength();//obtengo el total de caracteres de la cadena en el textframe
					int32 index=0;//numero de caracter leido
					WideString Temp;
					cs->CopyText(index, length-1, &Temp);
					// Setup a PMString to hold the text (depending on what you want to do with the text, the textchar may be ok) 
					// Load the text into the buf variable 
					//buf.SetXString(buffer, length); 
					texto="";
					texto.Append(Temp);
		}while (false); // only do once
		return(texto);
}



bool16 N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(WidgetID PanelWidgetID, WidgetID widget,PMString cadena)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		IControlView *iControlView = panel->FindWidget(widget);
		InterfacePtr<ITextControlData> TextControl(iControlView,ITextControlData::kDefaultIID);
		if (TextControl == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		TextControl->SetString(cadena);
		 retval=kTrue;
	}while(false);
	return(kTrue);
}


PMString N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(WidgetID widget)
{
	PMString Cadena;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		IControlView *iControlView = panel->FindWidget(widget);
		InterfacePtr<ITextControlData> TextControl(iControlView,ITextControlData::kDefaultIID);
		if (TextControl == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		Cadena=TextControl->GetString();
	}while(false);
	return(Cadena);
}







void N2PSQLUtilities::RealizarBusqueda_Y_llenadoDeListaPhoto(PMString Busqueda, PreferencesConnection PrefConections)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::RealizarBusqueda_Y_llenadoDeListaPhoto ini");
	do
	{
		PMString Titulo="";
		PMString TituloDeFoto="";
		PMString Descripcion="";
		PMString Autor="";
		PMString DescripcioDelEscritor="";
		PMString Ruta="";
		PMString IdElePhoto="";
		PMString StatusPhoto="";
		PMString FechaUltMod="";
		PMString IdPadreElePhoto="";
		PMString FotoColocada="";
		PMString MostrarInDes="";
		int32 fotosAsignadas=0;
		PMString Id_PieDFoto= "";
		PMString PieDFoto= "";
		//CAlert::ErrorAlert("1");
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		ASSERT(app);
		if (app == nil) {	
			break;
		}
		
		PMString nameApp = app->GetApplicationName();
		
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPhotoPanelWidgetID)) ;
		if(panel==nil)
		{
			//CAlert::ErrorAlert("No se encontro el editbox");
			break;
		}
		
		//CAlert::ErrorAlert("2");
		InterfacePtr<IControlView>		iControViewList( panel->FindWidget(kN2PsqlPhotoListBoxWidgetID), UseDefaultIID() );
		if(iControViewList==nil)
		{
			//CAlert::ErrorAlert("No se encontro el editbox");
			break;
		}
		
		N2PSQLPhotoListBoxHelper N2PSQLPhotoListHelper(iControViewList, kN2PSQLPluginID,kN2PsqlPhotoListBoxWidgetID, kN2PsqlPhotoPanelWidgetID);
		
		//CAlert::ErrorAlert("4");
		N2PSQLPhotoListHelper.EmptyCurrentListBox();
		
		
		//CAlert::ErrorAlert("5");
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
		
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		//CAlert::ErrorAlert("6");
/*		PreferencesConnection PrefConections;
		
		
		PMString NamePreferencia="Default"; //N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		//CAlert::ErrorAlert("7");
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
*/		
		//CAlert::ErrorAlert("8");
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}
		
		//CAlert::ErrorAlert("9");
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//CAlert::ErrorAlert("10");
		if(QueryVector.Length()>0)
		{
			for(int32 j=0;j<QueryVector.Length();j++)
			{
				
				Ruta = SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[j]);
				
				
				//SDKUtilities::RemoveFirstElement(Ruta);
			#ifdef MACINTOSH
				SDKUtilities::convertToMacPath(Ruta);
			#endif
				//CAlert::InformationAlert(Ruta);
				
				//CAlert::InformationAlert(Ruta);
				PMString CopyaRuta=PrefConections.PathOfServerFileImages;
				SDKUtilities::AppendPathSeparator(CopyaRuta);
				if(CopyaRuta.NumUTF16TextChars()<=1 || CopyaRuta!="null" )
				{
					Ruta=Ruta;
				}
				else
				{
					Ruta = CopyaRuta +""+ Ruta;
				}
				
				
				//CAlert::InformationAlert(Ruta);
				
				
				Id_PieDFoto = SQLInterface->ReturnItemContentbyNameColumn("id_piefoto",QueryVector[j]);
				PieDFoto = SQLInterface->ReturnItemContentbyNameColumn("pie_foto",QueryVector[j]);
				
				IdElePhoto = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",QueryVector[j]);
				IdPadreElePhoto = SQLInterface->ReturnItemContentbyNameColumn("Id_elemento_padre",QueryVector[j]);
				StatusPhoto =  SQLInterface->ReturnItemContentbyNameColumn("Id_Status",QueryVector[j]);
				FechaUltMod =  SQLInterface->ReturnItemContentbyNameColumn("Fecha_ult_mod",QueryVector[j]);
				Titulo = SQLInterface->ReturnItemContentbyNameColumn("Nombre",QueryVector[j]);
				TituloDeFoto = SQLInterface->ReturnItemContentbyNameColumn("Nombre_Archivo",QueryVector[j]);
				Autor = SQLInterface->ReturnItemContentbyNameColumn("Proveniente_de",QueryVector[j]);
				FotoColocada = SQLInterface->ReturnItemContentbyNameColumn("Estatus_Colocado",QueryVector[j]);
				MostrarInDes = SQLInterface->ReturnItemContentbyNameColumn("Mostrar_Indesign",QueryVector[j]);	
				
				//if(Id_PieDFoto=="")
				//	continue;

				
				if(nameApp.Contains("InDesign"))
				{
					if(MostrarInDes.GetAsNumber()==0)
					{
						continue;	//se brinca a la siguente foto cuando se nos encontramos en indesign y se indica que no se debe mostrar esta foto aqui
					}
					
					N2PSQLPhotoListHelper.Mostrar_IconFotoColocada(0, kN2PSQLCkBoxPhotoActivateWidgetID, kFalse);		
				}
				
				N2PSQLPhotoListHelper.AddElement(Ruta, Titulo,TituloDeFoto, TituloDeFoto, Autor,IdElePhoto, 0);
				
			
				N2PSQLPhotoListHelper.SetTextoEnSeleccionActual(kN2PsqlIdElePhotoLabelWidgetID,IdElePhoto, 0);
				N2PSQLPhotoListHelper.SetTextoEnSeleccionActual(kN2PsqlIdPadreElePhotoLabelWidgetID,IdPadreElePhoto, 0);
				N2PSQLPhotoListHelper.SetTextoEnSeleccionActual(kN2PsqlStatusPhotoLabelWidgetID,StatusPhoto, 0);
				N2PSQLPhotoListHelper.SetTextoEnSeleccionActual(kN2PsqlFechaUltModPhotoLabelWidgetID,FechaUltMod, 0);	
				
				K2Vector<N2PNotesStorage*> fBNotesStore;
				UIDRef RefTagetTextImported = ImportTaggedTextOfPMString(PieDFoto, fBNotesStore);
				Titulo="";
				
				if(RefTagetTextImported == UIDRef::gNull)
				{
					PieDFoto =  N2PSQLUtilities::ReturnContenidoOfId_Elemento(StringConection, Id_PieDFoto, kFalse);
					RefTagetTextImported = ImportTaggedTextOfPMString(PieDFoto, fBNotesStore);
					if(RefTagetTextImported != UIDRef::gNull)
					{
						Titulo = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported); //convierte el texto tageado a texto plano
					}
				}
				else
				{
					Titulo = GetTextPlainOfUIDRefImportTextTagged(RefTagetTextImported); //convierte el texto tageado a texto plano
				}
				
				
				
				
				UIDRef CopiaRef= RefTagetTextImported;
				UID uid=CopiaRef.GetUID();
				PMString UIDString=""; 
				UIDString.AppendNumber(uid.Get()); 
				
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlUIDRefTexModelDePieFoto_WidgetID,UIDString);
				N2PSQLPhotoListHelper.SetTextoEnSeleccionActual(kN2PsqlTextPieWidgetID,Titulo, 0);
				N2PSQLPhotoListHelper.SetTextoEnSeleccionActual(kN2PsqlText_ID_Elem_Pie_WidgetID,Id_PieDFoto , 0);	
				
				
				//Para mostrar icono de Foto colocada
				if(FotoColocada.GetAsNumber()==1)
				{
					N2PSQLPhotoListHelper.Mostrar_IconFotoColocada(0,kN2PsqlCmpListElementPenWidgetID,kTrue);
				}
							
				
				
				
				//Para poner como seleccionada en para mostrar en InDesign
				if(!nameApp.Contains("InDesign"))
					if(MostrarInDes.GetAsNumber()==1)
					{
						N2PSQLPhotoListHelper.ChechedCheckBox(0,kN2PSQLCkBoxPhotoActivateWidgetID,kTrue);
					}
					else
					{
						N2PSQLPhotoListHelper.ChechedCheckBox(0,kN2PSQLCkBoxPhotoActivateWidgetID,kFalse);
					}
				else
				{
					N2PSQLPhotoListHelper.Mostrar_IconFotoColocada(0, kN2PSQLCkBoxPhotoActivateWidgetID, kFalse);
				}
						
				
				//Para Mostar Foto como lista
				PMString FullNameOfLink=Ruta;
						
				PMString Separator="";
				SDKUtilities::AppendPathSeparator(Separator);
						
				int32 LastSeparator=FullNameOfLink.LastIndexOfCharacter(Separator.GetChar(0));
				PMString numebre="";
				numebre.AppendNumber(LastSeparator);
		
				if(LastSeparator>0)
				{
					PMString FullNameOfLinkListo=FullNameOfLink;
					FullNameOfLinkListo.Insert("@",1,LastSeparator+1);
						
					IDFile FileList;
					FileUtils::PMStringToIDFile(FullNameOfLinkListo,FileList);
						
					SDKFileHelper fileHelper(FileList);
					if(fileHelper.IsExisting())
					{
						//PathCorrect=FullNameOfLinkListo;
						//ExisteListo=kTrue;
						//CAlert::InformationAlert("Se sopme que existe2");
						N2PSQLPhotoListHelper.Mostrar_IconFotoColocada(0, kN2PSQLCkBoxPhotoListaWidgetID, kTrue);	
					}
				}
				////////
				fotosAsignadas=fotosAsignadas+1; //Incrementa el numero de fotos asignadas
			}	
		}
		
		
		PMString cadena="";
		cadena.AppendNumber(fotosAsignadas);
		PMString SAS(kN2PSQLCantFotoAsignadasStringKey);
		SAS.Translate();
  		SAS.SetTranslatable(kTrue);
  		cadena.Append(SAS);
		ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPhotoPanelWidgetID,kN2PsqlStTextCantFotosDeNotaWidgetID,cadena);
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::RealizarBusqueda_Y_llenadoDeListaPhoto fin");
}




void N2PSQLUtilities::MuestraIconodeCamaraSiNotaTieneFotos(PMString Busqueda, PreferencesConnection PrefConections)
{
	do
	{
		
		
		
		
		
		InterfacePtr<IPanelControlData>	panelNota(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panelNota==nil)
		{
			
			break;
		}
		
		InterfacePtr<IControlView>		iControViewiconCamara( panelNota->FindWidget(kN2PsqlAlertNotaConFotoWidgetID), UseDefaultIID() );
		if(iControViewiconCamara==nil)
		{
			
			break;
		}
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
		
		
		if(UpdateStorys==nil)
		{
			
			break;
		}
		
	
/*		PreferencesConnection PrefConections;
		
		
		PMString NamePreferencia="Default"; //N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			
			break;
		}
*/		
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		if(QueryVector.Length()>0)
		{
			PMString cadena="";
			cadena.AppendNumber(QueryVector.Length());
			PMString SAS(kN2PSQLCantFotoAsignadasStringKey);
			SAS.Translate();
  			SAS.SetTranslatable(kTrue);
  			cadena.Append(SAS);
			ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPhotoPanelWidgetID,kN2PsqlStTextCantFotosDeNotaWidgetID,cadena);
			iControViewiconCamara->Show(kTrue);
		}
		else
		{
			PMString cadena="";
			cadena.AppendNumber(QueryVector.Length());
			PMString SAS(kN2PSQLCantFotoAsignadasStringKey);
			SAS.Translate();
  			SAS.SetTranslatable(kTrue);
  			cadena.Append(SAS);
			ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPhotoPanelWidgetID,kN2PsqlStTextCantFotosDeNotaWidgetID,cadena);
			iControViewiconCamara->Hide();
			//N2PsqlLogInLogOutUtilities::ShowHidePaleteByWidgetID (kN2PsqlPhotoPanelWidgetID, kFalse ) ;
		}
				
	}while(false);
}


/*
	do
	{
		PMString Pagina="";
		PMString Secc="";
		PMString Estado="";
		PMString NotaFluida="";
		PMString IDNota="";
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		InterfacePtr<IControlView>		iControViewList( panel->FindWidget(kN2PsqlNotasListBoxWidgetID), UseDefaultIID() );
		if(iControViewList==nil)
		{
			CAlert::ErrorAlert("No se encontro el editbox");
			break;
		}

		N2PSQLListBoxHelper listHelper(iControViewList, kN2PSQLPluginID,kN2PsqlNotasListBoxWidgetID);
		listHelper.EmptyCurrentListBox();

		PMString cadena;

	
	
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		PMString NamePreferencia= N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetPreferencesConnectionForNameConection( NamePreferencia , PrefConections ))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			//CAlert::ErrorAlert(QueryVector[i]);
			IDNota=SQLInterface->ReturnItemContentbyNameColumn("Id_elemento_padre",QueryVector[i]);
			
			Pagina=SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryVector[i]);
		
			//Secc=SQLInterface->ReturnItemContentbyNameColumn("Seccion",QueryVector[i]);
			
			//Estado=SQLInterface->ReturnItemContentbyNameColumn("Estatus",QueryVector[i]);
		
			
			
			listHelper.AddElement(Pagina,kN2PsqlNotaLabelWidgetID,0);//UIDModel
		
		//	listHelper.SetTextoEnSeleccionActual(kN2PsqlSeccionLabelWidgetID,Secc,0);//ID de nota
		
		//	listHelper.SetTextoEnSeleccionActual(kN2PsqlEstadoLabelWidgetID,Estado,0);//Item para realizar la busqueda
			
			listHelper.SetTextoEnSeleccionActual(kN2PsqlIDNotaLabelWidgetID,IDNota,0);//Item para realizar la busqueda
			
			NotaFluida="";
			NotaFluida=SQLInterface->ReturnItemContentbyNameColumn("Estatus_Colocado",QueryVector[i]);
		
			if(NotaFluida.GetAsNumber()==1)
			{
				
				listHelper.Mostrar_IconFluido(0);	
				
			}
		}
		
		
	}while(false);
*/


/* ImportImageAndLoadPlaceGun
*/
UIDRef N2PSQLUtilities::ImportImageAndLoadPlaceGun(	const UIDRef& docUIDRef, const IDFile& idFile)
{
	// Precondition: active document
	// Precondition: file exists
	// 
	UIDRef retval = UIDRef::gNull;
	do
	{
		
		SDKFileHelper fileHelper(idFile);
		bool16 fileExists = fileHelper.IsExisting();
		// Fail quietly... ASSERT(fileExists);
		if(!fileExists)
		{
			break;
		}
		InterfacePtr<IDocument> 
			iDocument(docUIDRef, UseDefaultIID());
		ASSERT(iDocument);
		if(!iDocument)
		{
			break;
		}

		// Wrap the import in a command sequence.
		//PMString seqName("Import");
		//CmdUtils::SequenceContext seq(&seqName);
		//seq=kFailure;

		// If the place gun is already loaded abort it so we can re-load it. 
		InterfacePtr<IPlaceGun> placeGun(iDocument, UseDefaultIID());
		ASSERT(placeGun);
		if (!placeGun) 
		{
			break;
		}
		if (placeGun->IsLoaded())
		{
			InterfacePtr<ICommand> abortPlaceGunCmd( CmdUtils::CreateCommand(kAbortPlaceGunCmdBoss));
			ASSERT(abortPlaceGunCmd);
			if (!abortPlaceGunCmd)
			{
				break;
			}
			InterfacePtr<IUIDData> uidData(abortPlaceGunCmd, UseDefaultIID());
			ASSERT(uidData);
			if (!uidData) 
			{
				break;
			}
			uidData->Set(placeGun);
			if (CmdUtils::ProcessCommand(abortPlaceGunCmd) != kSuccess) 
			{
				ASSERT_FAIL("kAbortPlaceGunCmdBoss failed");
				break;
			}
		}
		
		IDataBase* db = docUIDRef.GetDataBase();
		ASSERT(db);
		if(!db)
		{
			break;
		}
		
		// Now import the selected file and load it's UID into the place gun.
		InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
		ASSERT(importCmd);
		if (!importCmd) {
			break;
		}
		
		InterfacePtr<IImportResourceCmdData> data(importCmd, IID_IIMPORTRESOURCECMDDATA);
		if (data == nil)
			break;

		//IDFile myfile(fromPath);
		URI fileURI;
		Utils<IURIUtils>()->IDFileToURI(idFile, fileURI);

		data->Set(db, fileURI, K2::kSuppressUI);  
		
		ErrorCode err = CmdUtils::ProcessCommand(importCmd);
		
		ASSERT(err == kSuccess);
		if(err != kSuccess)
		{
			break;
		}	
		// Get the contents of the place gun as our return value
		UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
		retval = placedItem; 
		//seq.SetState(kSuccess);
	} while(kFalse);
	
	return retval;
}





/*
 	Obtiene el las cooordenadas reales del frame primario que contiene el textmodel de un elemento 
 	de la nota a partir del Uid del TextFrame.
 	
 */
 bool16 N2PSQLUtilities::GETStoryParams_OfUIDTextModel( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumLinesFaltantes,
 												int32& NumLinesRestantes,
 												int32& NumCarFaltantes,
 												int32& NumCarRestantes,
 												int32& NumCarOfTextModel)
 {
 	bool16 retval=kFalse;
 	do
 	{
 			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
						(
							kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
							IUpdateStorysAndDBUtils::kDefaultIID
						)));
			
						if(UpdateStorys==nil)
						{
							break;
						}
						
			//
			UIDRef UIDRefOfElementoNota(db,UIDTextModelofElementoNota);
			
			//Obtiene el Texmodel apartir del numero de UID
			InterfacePtr<ITextModel> mytextmodel(db,UIDTextModelofElementoNota, UseDefaultIID()); 
			if(mytextmodel==nil)
			{
				
				continue;
			}
			
			TextIndex sourceStart=0;
			int32 sourceLength=0;
			
			sourceLength=mytextmodel->GetPrimaryStoryThreadSpan()-1;
		
			int32 sourceStoryLength = mytextmodel->TotalLength();
			TextIndex sourceEnd = sourceStart + sourceLength;
			if (sourceLength > 1) 
			{
				
				if (sourceEnd < sourceStoryLength - 1) 
				{
					NumCarOfTextModel=mytextmodel->TotalLength();
					NumCarOfTextModel--;//Se le resta uno por ultimo caracter que indica que es fin del texto
					//break;
				}
			}
			
			
			
			
			InterfacePtr<IFrameList> frameList(mytextmodel->QueryFrameList());
			ASSERT(frameList != nil);
			if (frameList == nil) {
				break;
			}
		
			InterfacePtr<IParcelList> parcelList(frameList, UseDefaultIID());
			ASSERT(parcelList != nil);
			if (parcelList == nil) {
				break;
			}
			InterfacePtr<ITextParcelList> textParcelList(frameList, UseDefaultIID());
			ASSERT(textParcelList != nil);
			if (textParcelList == nil) {
				break;
			}
		
						
			//Obtiene el Iparcel del Texmodel 
			InterfacePtr<IParcel> parcel(N2PSQLUtilities::QueryParcelContaining(mytextmodel,0));
			if (!parcel)
			{
				continue;
			}	
			
			//Obtiene el TextFrame que contiene el caracter numero 1
			InterfacePtr<ITextFrameColumn > FrameContentModel;
			
			FrameContentModel.reset(parcel->QueryFrame());
			if (!FrameContentModel)
			{
				continue;
			}	
			
			//Verifica que este texto sea un texto multicolumna
			if(!Utils<ITextUtils>()->IsMultiColumnFrame(FrameContentModel))
			{
			
				InterfacePtr<IMultiColumnTextFrame > frameConteiner(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel),UseDefaultIID());
				
				if(!Utils<ITextUtils>()->IsMultiColumnFrame(frameConteiner))
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}
	
				//Obtiene el texto Multicolumna que contiene el Texmodel
				InterfacePtr<ITextColumnSizer> textColumnSizerFlow(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel), UseDefaultIID());
				if (textColumnSizerFlow == nil)
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}	
				
				
				//Obtiene el UIDRef del GraphicFrame(Frame Principal del Textstory)
				UIDRef refFrame = N2PSQLUtilities::GetGraphicFrameRef(frameConteiner, kTrue);
				
				PMReal estimatedDepth=0;
  			    int32 UltimoCaracterMostrado=0;
				
				
				if (sourceEnd > sourceStoryLength - 1) 
				{
					N2PSQLUtilities::CountWordsAndLines(UIDTextModelofElementoNota, textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumLinesFaltantes, UltimoCaracterMostrado, NumCarFaltantes);
				}
				//this->EstimateTextDepth(textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumLinesFaltantes, UltimoCaracterMostrado);
				
				
				
				NumCarRestantes = NumCarOfTextModel - UltimoCaracterMostrado;
				//Para sacar segun las coordenadas de la pagina
				int32 numPagDeFrame;
				boundsInParentCoo = N2PSQLUtilities::Get_Coordinates_Reales(refFrame, numPagDeFrame);
				
				NumLinesRestantes = Utils<ITextUtils>()->CountOversetLines(UIDRefOfElementoNota);
				
				
				retval=kTrue;
			}	
 	}while(false);
 	return(retval);	
 }
 
 
 
  bool16 N2PSQLUtilities::AAAAXCSXCSED( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumLinesFaltantes,
 												int32& NumLinesRestantes,
 												int32& NumCarFaltantes,
 												int32& NumCarRestantes,
 												int32& NumCarOfTextModel, int32& UltimoCaracterMostrado)
 {
 	bool16 retval=kFalse;
 	do
 	{
 			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			//
			UIDRef UIDRefOfElementoNota(db,UIDTextModelofElementoNota);
			
			//Obtiene el Texmodel apartir del numero de UID
			InterfacePtr<ITextModel> mytextmodel(db,UIDTextModelofElementoNota, UseDefaultIID()); 
			if(mytextmodel==nil)
			{
				
				continue;
			}
			
			NumCarOfTextModel=mytextmodel->TotalLength();
			NumCarOfTextModel--;//Se le resta uno por ultimo caracter que indica que es fin del texto
			
			InterfacePtr<IFrameList> frameList(mytextmodel->QueryFrameList());
			ASSERT(frameList != nil);
			if (frameList == nil) {
				break;
			}
		
			InterfacePtr<IParcelList> parcelList(frameList, UseDefaultIID());
			ASSERT(parcelList != nil);
			if (parcelList == nil) {
				break;
			}
			InterfacePtr<ITextParcelList> textParcelList(frameList, UseDefaultIID());
			ASSERT(textParcelList != nil);
			if (textParcelList == nil) {
				break;
			}
		
						
			//Obtiene el Iparcel del Texmodel 
			InterfacePtr<IParcel> parcel(N2PSQLUtilities::QueryParcelContaining(mytextmodel,0));
			if (!parcel)
			{
				continue;
			}	
			
			//Obtiene el TextFrame que contiene el caracter numero 1
			InterfacePtr<ITextFrameColumn> FrameContentModel;
			
			FrameContentModel.reset(parcel->QueryFrame());
			if (!FrameContentModel)
			{
				continue;
			}	
			
			//Verifica que este texto sea un texto multicolumna
			if(!Utils<ITextUtils>()->IsMultiColumnFrame(FrameContentModel))
			{
			
				InterfacePtr<IMultiColumnTextFrame > frameConteiner(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel),UseDefaultIID());
				
				if(!Utils<ITextUtils>()->IsMultiColumnFrame(frameConteiner))
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}
	
				//Obtiene el texto Multicolumna que contiene el Texmodel
				InterfacePtr<ITextColumnSizer> textColumnSizerFlow(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel), UseDefaultIID());
				if (textColumnSizerFlow == nil)
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}	
				
				
				//Obtiene el UIDRef del GraphicFrame(Frame Principal del Textstory)
				UIDRef refFrame = N2PSQLUtilities::GetGraphicFrameRef(frameConteiner, kTrue);
				
				PMReal estimatedDepth=0;
  			    UltimoCaracterMostrado=0;
  			    N2PSQLUtilities::CountWordsAndLines(UIDTextModelofElementoNota, textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumLinesFaltantes, UltimoCaracterMostrado, NumCarFaltantes);
				//this->EstimateTextDepth(textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumLinesFaltantes, UltimoCaracterMostrado);
				
				
				
				NumCarRestantes = NumCarOfTextModel - UltimoCaracterMostrado;
				//Para sacar segun las coordenadas de la pagina
				int32 numPagDeFrame;
				boundsInParentCoo = N2PSQLUtilities::Get_Coordinates_Reales(refFrame, numPagDeFrame);
				
				NumLinesRestantes = Utils<ITextUtils>()->CountOversetLines(UIDRefOfElementoNota);
								
				retval=kTrue;
			}	
 	}while(false);
 	return(retval);	
 }
 




 /*
*/
IParcel* N2PSQLUtilities::QueryParcelContaining(ITextModel* textModel, const TextIndex at)
{
	InterfacePtr<IParcel> result;
	do {
		// Find the parcel list that displays the TextIndex.
		InterfacePtr<ITextParcelList> textParcelList(textModel->QueryTextParcelList(at));
		if (!textParcelList) {
			// Likely it belongs to a note or deleted text or 
			// another story thread that does not use parcels.
			// Or perhaps we are not in the layout view (i.e.
			// we are in a story or galley view).
			break;
		}
		ParcelKey firstDamagedParcelKey = textParcelList->GetFirstDamagedParcel();
		if (firstDamagedParcelKey.IsValid() == kTrue){
			// Recompose the text in this parcel list.
			InterfacePtr<ITextParcelListComposer> textParcelListComposer(textParcelList, UseDefaultIID());
			ASSERT(textParcelListComposer);
			if (!textParcelListComposer) {
				break;
			}
			textParcelListComposer->RecomposeThruTextIndex(kInvalidParcelIndex/*recompose all*/);
		}

		// Find the parcel that displays the TextIndex.
		ParcelKey parcelKey = textParcelList->GetParcelContaining(at);
		if (parcelKey.IsValid() == kFalse) {
			// Parcel is not displayed. Could be that the parcel
			// that displays this TextIndex is an overset text
			// cell in a table for example.
			break;
		}
		InterfacePtr<IParcelList> parcelList(textParcelList, UseDefaultIID());
		ASSERT(parcelList);
		if (!parcelList) {
			break;
		}
		InterfacePtr<IParcel> parcel(parcelList->QueryParcel(parcelKey));
		ASSERT(parcel);
		if (!parcel) {
			break;
		}
		result = parcel;
	} while(false);
	return result.forget();
}



/*
*/
UIDRef N2PSQLUtilities::GetGraphicFrameRef(const InterfacePtr<IMultiColumnTextFrame >& textFrame, const bool16 isTOPFrameAllowed)
{
	UIDRef result = UIDRef::gNull;

	do {
		ASSERT(textFrame);
		if (!textFrame) {
			break;
		}

		// Check for a regular text frame by going up
		// the hierarchy till we find a parent object
		// that aggregates IGraphicFrameData. This is
		// the graphic frame that contains the text content.
		UIDRef graphicFrameDataUIDRef = UIDRef::gNull;
		InterfacePtr<IHierarchy> child(textFrame, UseDefaultIID());
		if (child == nil) {
			break;
		}
		do 
		{
			InterfacePtr<IHierarchy> parent(child->QueryParent());
			if (parent == nil) {
				break;
			}
			InterfacePtr<IGraphicFrameData> graphicFrameData(parent, UseDefaultIID());
			if (graphicFrameData != nil)
			{
				// We have a text frame.
				graphicFrameDataUIDRef = ::GetUIDRef(graphicFrameData);
				break;
			}
			child = parent;
		} while(child != nil);

		if (graphicFrameDataUIDRef == UIDRef::gNull) 
		{
			break;
		}
                 
		InterfacePtr<ITOPSplineData> topSplineData(graphicFrameDataUIDRef, UseDefaultIID());
		if (topSplineData) 
		{
			// We have a text on a path frame.
			if (isTOPFrameAllowed == kTrue) 
			{
				// Return the text on a path frame
				result = graphicFrameDataUIDRef;
			}
			else          
			{
				// Return the graphic frame associated with the text on a path frame.
				UID mainSplineItemUID = topSplineData->GetMainSplineItemUID();
				ASSERT(mainSplineItemUID != kInvalidUID);
				result = UIDRef(graphicFrameDataUIDRef.GetDataBase(), mainSplineItemUID);
			}
		}
		else 
		{
			// We have a normal graphic frame.
			result = graphicFrameDataUIDRef;
		}

	} while(false);

	return result;

}


 /*
*/
ErrorCode N2PSQLUtilities::CountWordsAndLines(UID UIDTextModelofElementoNota, const InterfacePtr<ITextParcelList>& textParcelList, ParcelKey fromParcelKey, 
											int32 numberOfParcels, PMReal& estimatedDepth,
											int32& NumeroDeLineaAntesDelOverset,int32& UltimoCaracterMostrado, int32& CaracFaltAprox)
{

	int32 SpanProm=0;
	int32   i = 0;
	ErrorCode status = kFailure;
	do {
	
		
		InterfacePtr<ILayoutControlData> layoutA(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutA == nil)
		{
			ASSERT_FAIL("layout pointer nil");
			break;
		}


		//Obtien la jerarquia de la capa
		InterfacePtr<IHierarchy> layerHier(layoutA->QueryActiveLayer());
		if (layerHier == nil)
		{
			ASSERT_FAIL("IHierarchy pointer nil");
			break;
		}

		// Get the UIDRef of the layer:
		UIDRef layerRef(::GetUIDRef(layerHier));
								
		// Get the bounding box for the current page
		IDataBase* db1 = layerRef.GetDataBase();
		UIDRef pageUIDRefA = UIDRef(db1,layoutA->GetPage());
		InterfacePtr<IGeometry> pageGeometry(pageUIDRefA, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry pointer nil");
			break;
		}
		
		////////////////////////////////
	
		// Validate parameters.
		ASSERT(textParcelList != nil);
		if (textParcelList == nil) {
			break;
		}
		
		/*InterfacePtr<ITiler>  myTiler(textParcelList, UseDefaultIID());
		//ASSERT(myTiler != nil);
		if (myTiler == nil) 
		{
			CAlert::InformationAlert("myTiler == nil");
			break;
		}*/
		
		
		
		InterfacePtr<IParcelList> parcelList(textParcelList, UseDefaultIID());
		ASSERT(parcelList != nil);
		if (parcelList == nil) 
		{
			break;
		}
	
		
		
		int32 fromParcelIndex = parcelList->GetParcelIndex(fromParcelKey);
		ASSERT(fromParcelIndex >= 0);
		if (fromParcelIndex < 0) {
			break;
		}
		ASSERT(numberOfParcels > 0 && numberOfParcels <= parcelList->GetParcelCount());
		if(numberOfParcels <= 0 || numberOfParcels > parcelList->GetParcelCount()) {
			break;
		}

		// Make sure the text is composed.
		ParcelKey firstDamagedParcelKey = textParcelList->GetFirstDamagedParcel();
		
		if (firstDamagedParcelKey.IsValid() == kTrue){
			InterfacePtr<ITextParcelListComposer> textParcelListComposer(textParcelList, UseDefaultIID());
			ASSERT(textParcelListComposer);
			if (!textParcelListComposer) {
				break;
			}
			textParcelListComposer->RecomposeThruTextIndex(fromParcelIndex + numberOfParcels - 1);
		}
		
		// Scan the wax and use the position of the first and last line
		// in each parcel that displays text to estimate the overall depth.
		InterfacePtr<IWaxStrand> waxStrand(textParcelList->GetWaxStrandRef(), UseDefaultIID());
		if (waxStrand == nil) {
			break;
		}
		
		
		K2Vector<int32> ParcelKeyToAdd;
		int32 UltimoParcelWithText=-1;
		bool16 isParcelSinTexto=kFalse;
		PMReal YLastLine=0;
		PMReal YHeightLastLine=0;
		PMReal Alto, YSigLine,  YSigHeightLine;
		Alto=0.0;
		PMString kk="";
		for (int32 j = fromParcelIndex; j < fromParcelIndex + numberOfParcels; j++) 
		{
			
			
		 	
    	    
    	    ParcelKey key = parcelList->GetNthParcelKey(j);
    	    PMRect BoundparcelLastParcel=parcelList->GetParcelBounds(key);
    	    
    	    
    	    
    	    //Toma las coordenadas del TextFrame
			TextIndex start = textParcelList->GetTextStart(key);
			int32 span = textParcelList->GetTextSpan(key);
			if (span <= 0) 
			{
				// Some parcels may not be able to display any text.
				// But we can't assume that the story is displayed
				// in its entirety by preceeding parcels. It is possible
				// that a parcel was not big enough to accept text and
				// so text flowed into a subsequent and larger parcel.
				// To handle this continue and consider the depth
				// of other parcels in the list.
				//CAlert::InformationAlert("No se fluyo sobre este span<0");
				ParcelKeyToAdd.push_back(j);
				isParcelSinTexto=kTrue;
				continue;
			}
			else
			{
				UltimoParcelWithText=j;
				//Borra los anteriores pues no se van a contar para estimar 
				//la cantidad de caracteres que falatn para el CopyFit
				if(isParcelSinTexto==kTrue)	
				{
					if(ParcelKeyToAdd.Length()==1)
					{
						ParcelKeyToAdd.pop_back();
					}
					else
					{
						
						if(ParcelKeyToAdd.Length()>1)
						{
							ParcelKeyToAdd.clear();
						}
					}
					isParcelSinTexto=kFalse;
				}
			}
			
			TextIndex end = start + span - 1;
			UltimoCaracterMostrado = end;
			
			K2::scoped_ptr<IWaxIterator> waxIterator(waxStrand->NewWaxIterator());
    	    if (waxIterator == nil) 
    	    {
    	         break;
    	    }
    	    IWaxLine* waxLine = waxIterator->GetFirstWaxLine(start);
    	    
    	    
    	    i = 0;
    	    
    	    //SNIPLOG("#, TextOrigin, GetTextSpan, Leading(pts)");
   	     	while (waxLine != nil && waxLine->TextOrigin() < end) 
    	     {
    	     
    	     	
    	     	if(i==0)
    	     	{
    	     		
    	     		YLastLine = waxLine->GetYPosition();
    	     		YHeightLastLine= waxLine->GetLineHeight();
    	     		if( YLastLine > (BoundparcelLastParcel.Top() + YHeightLastLine))
    	     		{
    	     			Alto = Alto + ((-1)*(BoundparcelLastParcel.Top() - YSigLine));
    	     		}
    	     	}
    	     	else
    	     	{
    	     		YSigLine = waxLine->GetYPosition();
    	     		YSigHeightLine= waxLine->GetLineHeight();
    	     		
    	     		if( YSigLine == (YLastLine + YSigHeightLine))
    	     		{
    	     			YLastLine=YSigLine;
    	     			YHeightLastLine=	YSigHeightLine;
    	     			//ok
    	     		}
    	     		else
    	     		{
    	     			
    	     			Alto = Alto + ((-1)*(YLastLine - YSigLine));
    	     			YLastLine=YSigLine;
    	     			YHeightLastLine=	YSigHeightLine;
    	     		}
    	     		
    	     		
    	     	}
    	     	SpanProm = waxLine->GetTextSpan();
            	waxLine = waxIterator->GetNextWaxLine();
             	i++;
        	 }
		}
		
		SpanProm=SpanProm/i;
		
		
		
		PMReal LastPosicionLineOnParcelReal=0;
		
		
		ParcelKey LastParcelkeyWhithText = parcelList->GetNthParcelKey(UltimoParcelWithText);
    	PMRect BoundparcelLastParcelWithText=parcelList->GetParcelBounds(LastParcelkeyWhithText);
	
		
		
		
		PMReal CantidadDeLineasParaCompletarElUltimoParcelConTexto = 0;
		if(YLastLine>0)
		{
			CantidadDeLineasParaCompletarElUltimoParcelConTexto =  (BoundparcelLastParcelWithText.Height() - ((BoundparcelLastParcelWithText.Height()/2) + YLastLine))/YHeightLastLine;
			LastPosicionLineOnParcelReal=(BoundparcelLastParcelWithText.Height()/2) + YLastLine;
			//::Round(CantidadDeLineasParaCompletarElUltimoParcelConTexto);
		}
		else
		{
			CantidadDeLineasParaCompletarElUltimoParcelConTexto =  ((BoundparcelLastParcelWithText.Height()/2) - YLastLine)/YHeightLastLine;
			LastPosicionLineOnParcelReal=(BoundparcelLastParcelWithText.Height()/2) + YLastLine;
			//::Round(CantidadDeLineasParaCompletarElUltimoParcelConTexto);
		}
		
		
		//Adiciona el numero de Parcel que se enconttro la ultima linea leida
		ParcelKeyToAdd.push_back(UltimoParcelWithText);
		
		//Ciclo para observar si se encuentra en los Parcel faltates y en el ultimo Parcel que se encontro texto,
		//algun Pageitem con TextWrap
		for(int32 zz=0;zz<ParcelKeyToAdd.Length();zz++)
		{
			PMString coordenadasvbv="";
			ParcelKey key = parcelList->GetNthParcelKey(ParcelKeyToAdd[zz]);
			PMRect Boundparcel=parcelList->GetParcelBounds(key);
			
			
			 
			// Se suma la natidad de Lineas que caben en el siguiente Parcel siempre y cuando 
			// sea diferente al numero del  ultimo Parcel que tenia Texto
			if(UltimoParcelWithText!=ParcelKeyToAdd[zz])
				CantidadDeLineasParaCompletarElUltimoParcelConTexto = CantidadDeLineasParaCompletarElUltimoParcelConTexto  +  (Boundparcel.Height()/YHeightLastLine);
			
			
			//
			
			
			
			/////
			InterfacePtr<IGeometry> geometry(db1,parcelList->GetParcelFrameUID(key),IID_IGEOMETRY);
			if(geometry==nil)
			{ 
				
				ASSERT_FAIL("geometry pointer nil");
				break;
			}
			
			
			PMMatrix identity;
			
			PMRect bBoxInner = geometry->GetStrokeBoundingBox(identity);
		
			
			PBPMRect bBoxPasteboard = geometry->GetStrokeBoundingBox(::InnerToPasteboardMatrix(geometry));
			
			
			//PasteboardToInner(pageGeometry, &bBoxPasteboard);
			
			
			////////////////////
			
			InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
			if (layout == nil)
			{
				ASSERT_FAIL("ILayoutControlData pointer nil");
				break;
			}
			
			InterfacePtr<ISpread> spread(layout->GetSpreadRef(), UseDefaultIID());
			if(spread==nil)
			{
				break;
			}
			
			// Iterate over all the pages on the spread.
			int32 numberOfPages = spread->GetNumPages();
			for ( int32 j = 0; j < numberOfPages; j++ )
			{
				//contpag++;//contador de paginas
				UIDList pageItemList(db1);
				spread->GetItemsOnPage
				(
					j, 
					&pageItemList, 
					kFalse, // don't include the page object itself
					kFalse // include items that lie on the pasteboard
				);
				
				UID pagerUID=spread->GetNthPageUID(i);
				UIDRef pageUIDRef(db1,pagerUID);

				
					
				// Iterate the page items and save off the UIDs of frames.
				int32 pageItemListLength = pageItemList.Length();
				for ( int32 k = 0; k < pageItemListLength; k++ )
				{
					UIDRef pageItemRef = pageItemList.GetRef(k);
					
					
					InterfacePtr<IStandOffData> myStandOffData(pageItemRef, IID_ISTANDOFFDATA);
					if(myStandOffData==nil)
					{
						continue;
					}
					
					IStandOff::mode modoTextWrap = myStandOffData->GetMode();
					if(modoTextWrap==IStandOff::kNone)
					{
						continue;
					}
				
					InterfacePtr<IGraphicFrameData> graphicFrameData(pageItemRef, UseDefaultIID());
					if(graphicFrameData==nil)
					{
						continue;
					}
					else
					{
						if(!graphicFrameData->IsGraphicFrame())
						{
							//UIDTextModelofElementoNota
							UID fromMutiColumnItemUID = graphicFrameData->GetTextContentUID();
							if(fromMutiColumnItemUID==kInvalidUID)
							{
								continue;
							}
							
							InterfacePtr<ITextColumnSizer> fromTextColumnSizer(db1, fromMutiColumnItemUID, UseDefaultIID());
							if(fromTextColumnSizer==nil)
							{
								continue;
							}
							InterfacePtr<IMultiColumnTextFrame > fromTextFrame(fromTextColumnSizer, UseDefaultIID());
							if(fromTextFrame==nil)
							{
								continue;
							}
							
							InterfacePtr<ITextModel> fromTextModel(fromTextFrame->QueryTextModel());
							if(fromTextModel==nil)
							{
								continue;
							}
							
							IFrameList* Listffame = fromTextModel->QueryFrameList();
							UID UIDTextModel = Listffame->GetTextModelUID();
							if(UIDTextModel==UIDTextModelofElementoNota)
								continue;
						}
						
					}
					/*if (Utils<IPageItemTypeUtils>()->IsGraphicFrame(pageItemRef) == kTrue || 
							(graphicFrameData != nil && 
							 graphicFrameData->IsGraphicFrame() == kTrue && 
							 graphicFrameData->HasContent() != kTrue))
					{*/
						
						InterfacePtr<IGeometry> geometryPageItem(pageItemRef , IID_IGEOMETRY);
						if(geometryPageItem==nil)
						{ 
							ASSERT_FAIL("geometry pointer nil");
							break;
						}
						
						//Obtiene la geometria Real del GraphicFrame
						PMMatrix identityPageItem;
						PMRect bBoxInnerPageItem = geometry->GetStrokeBoundingBox(identityPageItem);
						PBPMRect bBoxPasteboardPageItem = geometryPageItem->GetStrokeBoundingBox(::InnerToPasteboardMatrix(geometryPageItem));
						//PasteboardToInner(pageGeometry, &bBoxPasteboardPageItem);
						
							//Si se existe interseccion entre los dos GraficsFrames
						if(Intersect(bBoxPasteboard, bBoxPasteboardPageItem))
						{
												
							
							if( (UltimoParcelWithText==ParcelKeyToAdd[zz]) && ((bBoxPasteboard.Top() + LastPosicionLineOnParcelReal) < bBoxPasteboardPageItem.Top())  )
							{
								PMRect IntersectPMRect = Intersection(bBoxPasteboard, bBoxPasteboardPageItem);
						
								//si el largo de la interseccion es igual al largo del Parcel
					
								if(IntersectPMRect.Width()==bBoxPasteboard.Width() || ( (IntersectPMRect.Width()-bBoxPasteboard.Width()) < 12))
								{
									//Entonces ocupa varias lineas completas
									//Se restan las lineas que ocupa el graphicframe sobre el actual Parcel
									CantidadDeLineasParaCompletarElUltimoParcelConTexto=CantidadDeLineasParaCompletarElUltimoParcelConTexto - (IntersectPMRect.Height()/YHeightLastLine);
									
								}
								else
								{
									//Si no, Entonces parte de algunas lineas no estaran completas
									//Se debe de determinar cuantos caracteres caben en esa parte de linea sobrante
								}
							}
							else
							{
								if(UltimoParcelWithText!=ParcelKeyToAdd[zz])
								{
									PMRect IntersectPMRect = Intersection(bBoxPasteboard, bBoxPasteboardPageItem);
						
									//si el largo de la interseccion es igual al largo del Parcel
					
									if(IntersectPMRect.Width()==bBoxPasteboard.Width() || ( (IntersectPMRect.Width()-bBoxPasteboard.Width()) < 12))
									{
										//Entonces ocupa varias lineas completas
										//Se restan las lineas que ocupa el graphicframe sobre el actual Parcel
										CantidadDeLineasParaCompletarElUltimoParcelConTexto=CantidadDeLineasParaCompletarElUltimoParcelConTexto - (IntersectPMRect.Height()/YHeightLastLine);
										
									}
									else
									{
										//Si no, Entonces parte de algunas lineas no estaran completas
											//Se debe de determinar cuantos caracteres caben en esa parte de linea sobrante
									}
								}
								
							}
							
						}
					//}
				}
			} 
		}
		
		//Verificar si  despues de la ultima linea observada
		PMString nn="";
		/*nn.Append("UltimoParcelWithText= ");
		nn.AppendNumber(UltimoParcelWithText);
		
		nn.Append("\n BoundparcelLastParcelWithText= ");
		nn.AppendNumber(BoundparcelLastParcelWithText.Height());
		nn.Append("\n YLastLine= ");
    	nn.AppendNumber(YLastLine);
    	
    	nn.Append("\n YHeightLastLine= ");
    	nn.AppendNumber(YHeightLastLine);*/
		
		nn.AppendNumber(CantidadDeLineasParaCompletarElUltimoParcelConTexto);
		NumeroDeLineaAntesDelOverset = nn.GetAsNumber(); 
		//CAlert::InformationAlert(nn);
		SpanProm = SpanProm * nn.GetAsNumber(); 
		CaracFaltAprox = SpanProm;
		status = kSuccess;

	} while (false);
	return status;
}


PMRect N2PSQLUtilities::Get_Coordinates_Reales(const UIDRef& pageItemRef, int32& numpagina)
{
	
	PMRect coor;
	do
	{
		coor=InterlasaUtilities::GetPageItemBoundingBox(pageItemRef, 0,0 ) ;
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}

		
		InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
		ASSERT(iPageList);
		if(!iPageList) 
		{
		 	ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
			break;
		}
				
		InterfacePtr<IHierarchy> itemHier(pageItemRef, IID_IHIERARCHY);
		if (itemHier == NULL )
			break;
		
		UID pageUID = Utils<ILayoutUtils>()->GetOwnerPageUID(itemHier);
		
		numpagina= iPageList->GetPageIndex(pageUID);
		numpagina = numpagina+1;


/*		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionUtils pointer nil");
			break;
		}
		
		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionManager pointer nil");
			break;
		}

		
		InterfacePtr<ILayoutSelectionSuite> lint_LayoutSelectionSuite(iSelectionManager, UseDefaultIID() );
		if( !lint_LayoutSelectionSuite )
			break;
		
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
				
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}
		
		
		UIDList selectPageItems(db);
		
		selectPageItems.Append(pageItemRef.GetUID());
		
		lint_LayoutSelectionSuite->SelectPageItems( selectPageItems,
												   Selection::kReplace,
												   Selection::kAlwaysCenterInView );
		
		
		 InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
		 ASSERT(iPageList);
		 if(!iPageList) 
		 {
		 	ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
			  break;
		 }
		 
		 InterfacePtr<IPageSetupPrefs> iPageSetupPrefs(static_cast<IPageSetupPrefs *>(::QueryPreferences(IID_IPAGEPREFERENCES, document)));
		if(iPageSetupPrefs==nil)
		{
			break;
		}
		
		PMRect PageTam= iPageSetupPrefs->GetPageSizePref();
		   		
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("layout pointer nil");
			break;
		}


		//Obtien la jerarquia de la capa
		InterfacePtr<IHierarchy> layerHier(layout->QueryActiveLayer());
		if (layerHier == nil)
		{
			ASSERT_FAIL("IHierarchy pointer nil");
			break;
		}

		// Get the UIDRef of the layer:
		UIDRef layerRef(::GetUIDRef(layerHier));
								
		// Get the bounding box for the current page
		IDataBase* db1 = layerRef.GetDataBase();
		UIDRef pageUIDRef = UIDRef(db1,layout->GetPage());
		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
		if (pageGeometry == nil)
		{
			ASSERT_FAIL("pageGeometry pointer nil");
			break;
		}


		/////////////////////////////

		//frameList.Append(pageItemRef.GetUID());	
							
		InterfacePtr<IGeometry> geometry(pageItemRef,IID_IGEOMETRY);
		if(geometry==nil)
		{ 
			ASSERT_FAIL("geometry pointer nil");
			//CAlert::InformationAlert("geometry pointer nil");
			break;
		}

		PMMatrix identity;
		PMRect bBoxInner = geometry->GetStrokeBoundingBox(identity);
		
		PBPMRect bBoxPasteboard = geometry->GetStrokeBoundingBox(::InnerToPasteboardMatrix(geometry));
		TransformPasteboardRectToInner(pageGeometry, &bBoxPasteboard);
		//PasteboardToInner(pageGeometry, &bBoxPasteboard);
		
		coor=bBoxPasteboard;
		numpagina= iPageList->GetPageIndex(pageUIDRef.GetUID());
		numpagina = numpagina+1;
*/
/*		numpagina= iPageList->GetPageIndex(pageUIDRef.GetUID());
		PMString NumPagStr="Numero de Pagina:";
		NumPagStr.AppendNumber(numpagina);
		NumPagStr.Append(", UID");
		NumPagStr.AppendNumber(pageItemRef.GetUID().Get());
		CAlert::InformationAlert(NumPagStr);
		if(numpagina==0)//actualmente nos encontramnos en la primera pagina
		{
			coor=coor;
			//if(coor.Left()>PageTam.Width())
			//{//se encuentra en la siguinete pagina o en la segunda pagina
			//	 coor=PMRect((coor.Left()+PageTam.Width()), coor.Top(), (coor.Right()+PageTam.Width()),coor.Bottom());
			//}
		}
		else
		{
			coor=coor;
			if(coor.Left()<0)
			{//se encuentra en la primera pagina
				
				coor=PMRect((coor.Left()+PageTam.Width()), coor.Top(), (coor.Right()+PageTam.Width()),coor.Bottom());
			}
			else
			{
				
				coor=PMRect( (coor.Left()+PageTam.Width()), coor.Top(), (coor.Right()+PageTam.Width()),coor.Bottom());
			}
		}
*/
		
		
						
	}while(false);
	return coor;	
}


PMRect N2PSQLUtilities::Get_Coordinates_On_Spread(const UIDRef& pageItemRef)
{
	PMRect coor;
	do
	{
			
							
		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("layout pointer nil");
			break;
		}
		
		InterfacePtr<IGeometry> spreadGeometry(layout->GetSpreadRef(), UseDefaultIID());
		if (spreadGeometry == nil)
		{
			ASSERT_FAIL("spreadGeometry pointer nil");
			break;
		}
		//Obtien la jerarquia de la capa
//		InterfacePtr<IHierarchy> layerHier(layout->QueryActiveLayer());
//		if (layerHier == nil)
//		{
//			ASSERT_FAIL("IHierarchy pointer nil");
//			break;
//		}

		// Get the UIDRef of the layer:
//		UIDRef layerRef(::GetUIDRef(layerHier));
								
		// Get the bounding box for the current page
//		IDataBase* db1 = layerRef.GetDataBase();
//		UIDRef pageUIDRef = UIDRef(db1,layout->GetPage());
//		InterfacePtr<IGeometry> pageGeometry(pageUIDRef, UseDefaultIID());
//		if (pageGeometry == nil)
//		{
//			ASSERT_FAIL("pageGeometry pointer nil");
//			break;
//		}


		/////////////////////////////

		//frameList.Append(pageItemRef.GetUID());	
							
		InterfacePtr<IGeometry> geometry(pageItemRef,IID_IGEOMETRY);
		if(geometry==nil)
		{ 
			ASSERT_FAIL("geometry pointer nil");
			//CAlert::InformationAlert("geometry pointer nil");
			break;
		}

		PMMatrix identity;
		PMRect bBoxInner = geometry->GetStrokeBoundingBox(identity);
		
		PBPMRect bBoxPasteboard = geometry->GetStrokeBoundingBox(::InnerToPasteboardMatrix(geometry));
		//PasteboardToInner(spreadGeometry, &bBoxPasteboard);
		coor=bBoxPasteboard;
						
	}while(false);
	return coor;	
}





ErrorCode N2PSQLUtilities::DeleteText(ITextModel* textModelz,const TextIndex position,const int32 length)
{
  	ErrorCode status = kFailure;
  	do 
  	{
  		ASSERT(textModelz);
  		if (!textModelz)
  		{
  			break;
  		}
  		
  		/*if (position < 0 || position >= textModelz->TotalLength()) 
  		{
  			CAlert::InformationAlert("position invalid");
  		    ASSERT_FAIL("position invalid");
            break;
		}*/
		
        if (length < 1 || length >= textModelz->TotalLength()) 
        {
        	//CAlert::InformationAlert("position invalid");
            ASSERT_FAIL("length invalid");
        	break;
        }
        InterfacePtr<ITextModelCmds> textModelCmds(textModelz, UseDefaultIID());
        ASSERT(textModelCmds);
        if (!textModelCmds) 
        {
        	//CAlert::InformationAlert("textModelCmds invalid");
            break;
        }
        InterfacePtr<ICommand> deleteCmd(textModelCmds->DeleteCmd(position, length));
        ASSERT(deleteCmd);
        if (!deleteCmd) 
        {
        	//CAlert::InformationAlert("deleteCmd invalid");
            break;
        }
        status = CmdUtils::ProcessCommand(deleteCmd);
     }while(false);
    return status;
}


ErrorCode N2PSQLUtilities::InsertTextMiee(ITextModel* mytextmodel, const TextIndex position, const boost::shared_ptr<WideString>& mytext)
{
     ErrorCode status = kFailure;
     do 
     {
     	IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}
		
         ASSERT(mytextmodel);
         if (!mytextmodel) {
         		CAlert::InformationAlert("mytextmodel");
             break;
         }
         if (position < 0 || position >= mytextmodel->TotalLength()) {
            CAlert::InformationAlert("position invalid");
             ASSERT_FAIL("position invalid");
             break;
         }
         InterfacePtr<ITextModelCmds> textModelCmds(mytextmodel, UseDefaultIID());
         ASSERT(textModelCmds);
         if (!textModelCmds) {
         	 CAlert::InformationAlert("textModelCmds invalid");
             break;
         }
         InterfacePtr<ICommand> insertCmd(textModelCmds->InsertCmd(position, mytext));
         ASSERT(insertCmd);
         if (!insertCmd) {
         	 CAlert::InformationAlert("insertCmd invalid");
             break;
         }
        // db->BeginTransaction();
         status = CmdUtils::ProcessCommand(insertCmd);
         //db->EndTransaction();
         if(status!=kSuccess)
         {
         	 CAlert::InformationAlert("status!=kSuccess");
             break;
         }
         
      	N2PSQLUtilities::ObtenListaDeAtributosDRangoDTexto(mytextmodel, position,  mytextmodel->TotalLength());
         
     } while(false);
     return status;
}




ErrorCode N2PSQLUtilities::ObtenListaDeAtributosDRangoDTexto(ITextModel* textModel, const TextIndex start, const TextIndex length)
{
	ErrorCode error=kFailure;
	do
	{
	
		InterfacePtr<IAttributeStrand> attributeStrand((IAttributeStrand*)textModel->QueryStrand(kParaAttrStrandBoss,IAttributeStrand::kDefaultIID));
		if(attributeStrand==nil)
		{
			break;
		}
		
		int32 length2= 0;
		UID styleUID = attributeStrand->GetStyleUID(start-1, &length2);	
		
		
		const DataWrapper<AttributeBossList> charListOverrides = attributeStrand->GetLocalOverrides(start-1, &length2);
		
		if((charListOverrides!=nil) && (*charListOverrides).CountBosses()>0)
		{
			 InterfacePtr<ITextModelCmds> textModelCmds(textModel, UseDefaultIID());
        	 ASSERT(textModelCmds);
        	 if (textModelCmds==nil) {
         		 CAlert::InformationAlert("textModelCmds invalid");
         	    break;
        	 }
        	 
        	/* const AttributeBossList* theOverrides = charListOverrides;
        	 K2:scoped_ptr<AttributeBossList>  attr(theOverrides->Duplicate());
        	 
        	 if(theOverrides==nil)
        	 {
        	 	break;
        	 }
        	 */
        	 TextIndex a=0;
        	 int32 b=1;
        	 InterfacePtr<ICommand> insertCmd(textModelCmds->ApplyStyleCmd(a, b, styleUID, kCharAttrStrandBoss));
        	 ASSERT(insertCmd);
        	 if (!insertCmd) {
        	 	 CAlert::InformationAlert("insertCmd invalid");
        	     break;
        	 }
        
        	 error = CmdUtils::ProcessCommand(insertCmd);
         
			/*///Aplicar Anuevo Texto Atributos del Ultimo Caracter
			InterfacePtr<ICommand> applyCmd(Utils<ITextAttrUtils>()->BuildApplyTextAttrCmd(textModel,start,length,kCharAttrStrandBoss,charListOverrides) );
			if(applyCmd==nil)
			{	
				break;
			}
		
			//procesa comando
			 error = CmdUtils::ProcessCommand(applyCmd);*/
		}
		  
   
	}while(false);
	return(error);
}


bool16 N2PSQLUtilities::LlenarComboUsuarioSobrePaletaN2P()
{
	bool16 retval=kFalse;	
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		IControlView *ControlView = panel->FindWidget(kN2PsqlComboRoutedToWidgetID);
		if(ControlView==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListDataDirigidoa(ControlView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataDirigidoa == nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos dropListDataDirigidoa");
			break;
		}
		dropListDataDirigidoa->Clear(kTrue);

		PMString Busqueda="SELECT SQL_CACHE  Id_Usuario From Usuario ORDER By Id_Usuario ASC";
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ControlView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}
				
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		
		//llenado del combo Dirigido A
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Usuario",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListDataDirigidoa->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}
		
		cadena = "";
		cadena.SetTranslatable(kFalse);
		dropListDataDirigidoa->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
	}while(false);
	return(retval);
}


bool16 N2PSQLUtilities::LlenarComboEstatusSobrePaletaN2P()
{
	bool16 retval=kFalse;	
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		IControlView *ControlView = panel->FindWidget(kN2PsqlTextStatusComboWidgetID);
		if(ControlView==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListDataDirigidoa(ControlView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataDirigidoa == nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos dropListDataDirigidoa");
			break;
		}
		dropListDataDirigidoa->Clear(kTrue);

		PMString Busqueda="SELECT SQL_CACHE  Nombre_Estatus From estatus_elemento WHERE Id_tipo_ele=10 ORDER BY Nombre_Estatus ASC";
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ControlView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}
				
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		
		//llenado del combo Dirigido A
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListDataDirigidoa->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}
		
		cadena = "";
		cadena.SetTranslatable(kFalse);
		dropListDataDirigidoa->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
	}while(false);
	return(retval);
}

PMString N2PSQLUtilities::ReturnContenidoOfId_Elemento(PMString StringConection, PMString Id_ElementoDContenido,const bool16& ReturnWhitPases)
{
	
	PMString CopiaIdElemento=Id_ElementoDContenido;
	PMString Contenido="";
	PMString Contenido2="";
	
	
	
	do
	{	
		if(Id_ElementoDContenido=="" || Id_ElementoDContenido.NumUTF16TextChars()<=0)
			break;
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			ASSERT(SQLInterface);
			if (SQLInterface == nil) {	
				break;
			}
			
		K2Vector<PMString> QueryContenido;
		K2Vector<PMString> QueryIdElementos;
		
		
		
		PMString Buscar= "CALL Return_Length_Contenido_De_Id_Elemento(" + CopiaIdElemento + ")";

		SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
		for(int32 j=0;j<QueryContenido.Length();j++)
		{
			Contenido.Append( SQLInterface->ReturnItemContentbyNameColumn("TamanoTexto",QueryContenido[j]));
		}
		
		//CAlert::InformationAlert(Contenido);
		int32 CantidadDeCracteres=Contenido.GetAsNumber();
		int32 inicio=1;
		int32 length=29999;
		
		int32 final=length;
		int32 vecespasadas=0;
		Contenido="";
		
		bool16 pasarDNuevo=kTrue;
		
		if(CantidadDeCracteres>0 )
		{
			if(CantidadDeCracteres>30000)
			{
				
				while(pasarDNuevo==kTrue)
				{
					QueryContenido.clear();
					
					Buscar= "CALL Return_Contenido_De_Id_ElementoSUBString(" + CopiaIdElemento + ",";
					Buscar.AppendNumber(Contenido.NumUTF16TextChars()+1);
					Buscar.Append(",");
					Buscar.AppendNumber(length);
					Buscar.Append(")");
					//CAlert::InformationAlert(Buscar);
					
					SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
					for(int32 j=0;j<QueryContenido.Length();j++)
					{
						PMString Temp= SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j]);

						
							
						
						
						Contenido.Append(Temp);
					}
					
					
					inicio = final+1;
					final = inicio+length;
					
					if(final>CantidadDeCracteres)
					{
						pasarDNuevo=kFalse;
						length=inicio+(CantidadDeCracteres-inicio);
						
						QueryContenido.clear();
					
						Buscar= "CALL Return_Contenido_De_Id_ElementoSUBString(" + CopiaIdElemento + ",";
						Buscar.AppendNumber(Contenido.NumUTF16TextChars()+1);
						Buscar.Append(",");
						Buscar.AppendNumber(length);
						Buscar.Append(")");
						//CAlert::InformationAlert(Buscar);
						
						SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
						for(int32 j=0;j<QueryContenido.Length();j++)
						{
							PMString Temp= SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j]);
							
						
							Contenido.Append(Temp);
						}
					}
				}
			}
			else
			{
				Buscar= "CALL Return_Contenido_De_Id_Elemento(" + CopiaIdElemento + ")";
				QueryContenido.clear();
				
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
				//CAlert::InformationAlert("Queryyyyyyy = " + Buscar);
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					//CAlert::InformationAlert("Queryyyyyyy = " + QueryContenido[j]);
					Contenido.Append( SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j]));
				}
			}
		}
		
		
		
		
	/*	do
		{
		
		
		
			
			PMString Buscar= "CALL Return_Length_Contenido_De_Id_Elemento(" + CopiaIdElemento + ")";
			
			SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
			for(int32 j=0;j<QueryContenido.Length();j++)
			{
				Contenido.Append( SQLInterface->ReturnItemContentbyNameColumn("TamanoTexto",QueryContenido[j]));
			}
			
			int32 CantidadDeCracteres=Contenido.GetAsNumber();
			int32 inicio=1;
			int32 final=29999;
			int32 vecespasadas=0;
			Contenido="";
			
			PMString CSE="";
			CSE.AppendNumber(CantidadDeCracteres);
			
			//CAlert::InformationAlert("Cantidad de caracteres== "+CSE);
			QueryContenido.clear();
			if(CantidadDeCracteres>30000)
			{
				
				do
				{
					Buscar= "CALL Return_Contenido_De_Id_ElementoSUBString(" + CopiaIdElemento + ",";
					Buscar.AppendNumber(inicio);
					Buscar.Append(",");
					Buscar.AppendNumber(final);
					Buscar.Append(")");
					
					//CAlert::InformationAlert(Buscar);
					
					
					QueryContenido.clear();
					
					SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
					for(int32 j=0;j<QueryContenido.Length();j++)
					{
						Contenido.Append( SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j]));
					}
					
					inicio = final;
					final = inicio+29999;
					if(final>CantidadDeCracteres)
					{
						final=CantidadDeCracteres;
						vecespasadas++;
					}
					CAlert::InformationAlert("Pasa por aca  "+Contenido);
				}while(vecespasadas<2);
				
				
				
			}
			else
			{
				Buscar= "CALL Return_Contenido_De_Id_Elemento(" + CopiaIdElemento + ")";
				QueryContenido.clear();
				
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
				//CAlert::InformationAlert("Queryyyyyyy = " + Buscar);
				for(int32 j=0;j<QueryContenido.Length();j++)
				{
					//CAlert::InformationAlert("Queryyyyyyy = " + QueryContenido[j]);
					Contenido.Append( SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j]));
				}
				
			}
		*/	
			/*Buscar= "CALL Return_Contenido_De_Id_Elemento2(" + CopiaIdElemento + ")";
			QueryContenido.clear();
			
			SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryContenido);
		
			for(int32 j=0;j<QueryContenido.Length();j++)
			{
				Contenido.Append( SQLInterface->ReturnItemContentbyNameColumn("Contenido",QueryContenido[j]));
				Contenido2.Append( SQLInterface->ReturnItemContentbyNameColumn("Contenido2",QueryContenido[j]));
			}
			
			Contenido.Append(Contenido2);*/
			//CAlert::InformationAlert("ContenidoTEXTOe = " + Contenido);
			
			Buscar = "SELECT Id_Elemento FROM Elemento WHERE Id_tipo_ele=1 AND Id_elemento_padre= " + CopiaIdElemento ;
			CopiaIdElemento="";
			if(ReturnWhitPases==kTrue)
			{
				
			
			
				QueryIdElementos.clear();
				SQLInterface->SQLQueryDataBase(StringConection,Buscar,QueryIdElementos);
			
			
			
				for(int32 i=0;i<QueryIdElementos.Length();i++)
				{
					CopiaIdElemento = SQLInterface->ReturnItemContentbyNameColumn("Id_Elemento",QueryIdElementos[i]);
				}
			}
			
			
			
		//}while(CopiaIdElemento.NumUTF16TextChars()>0);
		
	}while(false);
			
	return(Contenido);
}


bool16 N2PSQLUtilities::LlenarElementosDePasesEnVentanaN2PSQL(PreferencesConnection PrefConections)
{
	
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		

		InterfacePtr<IControlView>		ListCView( panel->FindWidget(kN2PsqlPasesListBoxWidgetID), UseDefaultIID() );
		if(ListCView==nil)
		{
			ASSERT_FAIL("No se encontro el editbox");
			break;
		}
					
		InterfacePtr<IListBoxController> listCntl(ListCView,IID_ILISTBOXCONTROLLER); // kDefaultIID not def'd
		ASSERT_MSG(listCntl != nil, "listCntl nil");
		if(listCntl == nil) {
			break;
		}
					
		K2Vector<int32> multipleSelection ;
		listCntl->GetSelected( multipleSelection ) ;
		const int kSelectionLength =  multipleSelection.Length() ;
		if (kSelectionLength> 0 )
		{
			int indexSelected = multipleSelection[0];
			//se declara crea una lista 
			PMString otro="";
			PMString IDNotaStri="";
			N2PSQLListBoxHelper listHelper(ListCView, kN2PSQLPluginID,kN2PsqlPasesListBoxWidgetID);
			listHelper.ObtenerTextoDeSeleccionActual(kN2PsqlIDPaseLabelWidgetID,IDNotaStri,kN2PsqlContenidoPaselLabelWidgetID,otro,indexSelected);
				
	 				
	 		
	 		PMString Busqueda= "CALL QueryElementsOfPases '" + IDNotaStri +"'";
	 		
	 		N2PSQLUtilities::RealizarBusqueda_Y_llenado(Busqueda, kTrue, PrefConections);
	 		retval=kTrue;
		}
		
	}while(false);
	return(retval);
}

PMString N2PSQLUtilities::ExportTaggedTextOfPMString(UIDRef textModelRef,const PMString& ElementoSS)
{
	//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::ExportTaggedTextOfPMString ini");
	PMString taggedText = ""; 
	
	//Obtiene las notas del Elemento del Articulo
	K2Vector<N2PNotesStorage*> fN2PNotesStore;
	do
	{
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		
		
		InterfacePtr<IWorkspace> piWorkspace(GetExecutionContextSession()->QueryWorkspace());
		if(piWorkspace==nil)
			break;
			
		InterfacePtr<ITaggedTextExportPreferences>TATEXTExportPref((ITaggedTextExportPreferences *) piWorkspace->QueryInterface(IID_ITAGGEDTEXTEXPORTPREFERENCES));
		if(TATEXTExportPref==nil)
			break;
		
		TATEXTExportPref->SetExportTagForm(kVerbose);
		TATEXTExportPref->SetExportEncodingPref(kEncodingAscii);
			
		/*Utils<IN2PNotesUtils>()->GetNotesOnElementArticle(fN2PNotesStore, textModelRef);
		
		for(int32 i=0;i<fN2PNotesStore.Length();i++)
		{
			PMString aa= fN2PNotesStore[i]->flastmodified;
			CAlert::InformationAlert( aa);
		}*/
		///
		InterfacePtr<ITextModel> mytextmodel(textModelRef , UseDefaultIID());
		if(mytextmodel==nil)
		{
			//CAlert::ErrorAlert("mytextmodel possible"); 
			break;
		}
		InterfacePtr<ITextTarget> targetboss ((ITextTarget*)::CreateObject(kTextSuiteBoss, IID_ITEXTTARGET)); 
		if(!targetboss)
		{
			//CAlert::ErrorAlert("targetboss possible"); 
			break;
		}
		
		int32 storyLength = mytextmodel->GetPrimaryStoryThreadSpan() - 1;
		targetboss->SetTextUnmanaged(textModelRef, RangeData(0, storyLength)); 
		InterfacePtr<IK2ServiceRegistry> registry(GetExecutionContextSession(), UseDefaultIID()); 
		if(!registry)
		{
			CAlert::ErrorAlert("registry possible"); 
			break;
		}
		
		
		InterfacePtr<IK2ServiceProvider> service(registry->QueryServiceProviderByClassID(kExportProviderService, kTaggedTextExportFilterBoss)); 
		if(!service)
		{
			CAlert::ErrorAlert("service possible"); 
			break;
		}
		
		
		InterfacePtr<IExportProvider> exportPro(service, UseDefaultIID()); 
		if(!service)
		{
			break;
		}
		
		
		
		N2PSQLXferBytes irXferBytes; 
		
		InterfacePtr<IPMStream> stream(StreamUtil::CreateMemoryStreamWrite(&irXferBytes)); 
		if(!stream)
		{
			break;
		}
		
		stream->Open(); 
		
	
		exportPro->ExportToStream(stream, document, targetboss, "InDesign Tagged Text", kSuppressUI); 
	
	
		stream->Flush(); 
		
		PMString taggedTemp=""; 
		
		
		
		irXferBytes.Seek(0, kSeekFromStart); 
		
		char temp[256]; 
		bool crlf = false; 
		while (irXferBytes.GetStreamState() == kStreamStateGood) 
		{ 
			uint32 anzahl = irXferBytes.Read(temp, 255);
			temp[anzahl] = 0;
			taggedTemp.SetCString(temp, PMString::kUnknownEncoding);
			taggedText.Append(taggedTemp);
		}
		stream->Close();
	}while(false);
	
	InterfacePtr<IN2PSQLDebugLog> N2PSDebug(static_cast<IN2PSQLDebugLog*> (CreateObject
																		   (
																			kN2PSQLDebugLogBoss,										   
																			IN2PSQLDebugLog::kDefaultIID
																			)));
	

	
	N2PSDebug->IniciaDebug("ExportTaggedTextOfPMString_1_"+ElementoSS);
	N2PSDebug->printmsg(taggedText);
	N2PSDebug->TerminaDebug();
	
	if(taggedText.NumUTF16TextChars())
	{
		N2PSQLUtilities::convertTaggetTextToTaggedTextToXML(taggedText, fN2PNotesStore);
	}
	N2PSDebug->IniciaDebug("ExportTaggedTextOfPMString_2_"+ElementoSS);
	N2PSDebug->printmsg(taggedText);
	N2PSDebug->TerminaDebug();
	//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::ExportTaggedTextOfPMString fin");
	return(taggedText);
}


UIDRef N2PSQLUtilities::ImportTaggedTextOfPMString(PMString Texto, K2Vector<N2PNotesStorage*>& fBNotesStore)
{	UIDRef newRef=UIDRef::gNull;
	
	//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::ImportTaggedTextOfPMString ini");
	do
	{
		
		ExtarctOfTextXML_Notes(Texto, fBNotesStore);
		
		if(!N2PSQLUtilities::ExractOfTextXML_TaggetText(Texto))
		{
			break;
		}
		
		//CAlert::InformationAlert(Texto);
		
		
		
		
		/*FILE *ArchivoPref;
		
		PMString PathFile=N2PSQLUtilities::CrearFolderPreferencias();
		PathFile.Append("texto_tageado.pfd");
		if((ArchivoPref=FileUtils::OpenFile(PathFile.GrabCString(),"w"))!=NULL)
		{
			
			
			fprintf(ArchivoPref,"%s",(Texto.Substring(0,Texto.NumUTF16TextChars()))->GrabCString());
	
		}
		fclose(ArchivoPref);*/
		
		if(!Texto.Contains("ASCII-"))
		{ 
			//CAlert::InformationAlert("DEBIO SQLIR POR AQUII!!!!!!!!");
			break; 
		}
		
		InterfacePtr<IWorkspace> piWorkspace(GetExecutionContextSession()->QueryWorkspace());
		if(piWorkspace==nil)
			break;
		
		InterfacePtr<ITaggedTextImportPreferences>TATEXTImporttPref((ITaggedTextImportPreferences *) piWorkspace->QueryInterface(IID_ITAGGEDTEXTIMPORTPREFERENCES));
		if(TATEXTImporttPref==nil)
			break;
		
		TATEXTImporttPref->SetStyleConflictOption(kApplyPubStyle);//kApplyPubStyle 
		TATEXTImporttPref->SetRemoveFormatting(kFalse);
		TATEXTImporttPref->SetUseTypographersQuotes(kTrue);
		//TATEXTImporttPref->SetImportEncodingPref(kTrue);
		TATEXTImporttPref->SetWarningOption(kSuppressAllWarnings);
		
		
			
			
			
		IApplication *gApp = GetExecutionContextSession()->QueryApplication(); 
		if(gApp==nil)
			break;
		
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		if(doc==nil)
			break;
		
		InterfacePtr<IK2ServiceRegistry> registry(GetExecutionContextSession(), UseDefaultIID()); 
		if (registry == 0) 
		{ 
			break; 
		} 
		// Query the service registry for the service provider 
		InterfacePtr<IK2ServiceProvider> service(registry->QueryServiceProviderByClassID( kImportProviderService,kTaggedTextImportFilterBoss)); 
		if (service == 0) 	
		{ 
			break; 
		} 

		// Query the service provider for an import provider 
		InterfacePtr<IImportProvider> import(service, IID_IIMPORTPROVIDER); 
		if (import == 0) 
		{ 
			break; 
		} 
		if (import)
		{ 
			
			IPMStream * MyTexTagged = StreamUtil::CreatePointerStreamRead((char *)Texto.GrabCString(), Texto.CharCount() );
			
			
			if (MyTexTagged) 
			{ 
				if (IImportProvider::kFullImport == import->CanImportThisStream(MyTexTagged)) 
				{ 
					const K2::UIFlags uiFlags = K2::kSuppressUI;// kFullUI;//
					
					import->ImportThis(::GetDataBase(doc), MyTexTagged, uiFlags, &newRef); 
				} 
				
			} 
		}
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::ImportTaggedTextOfPMString fin");
	return(newRef);
}






bool16 N2PSQLUtilities::ProcessCopyStoryCmd(const UIDRef& sourceStory,  const UIDRef& destStory, IDocument* document)
{
	bool16 retval=kFalse;
	do
	{
		TextIndex sourceStart=0;
		int32 sourceLength=0;
		TextIndex destStart=0;
		int32 destLength=0;
		
		// Validate the range of text to be copied from the source story.
		InterfacePtr<ITextModel> sourceTextModel(sourceStory, UseDefaultIID());
		if (sourceTextModel == nil) {
			break;
		}
		
		sourceLength=sourceTextModel->GetPrimaryStoryThreadSpan()-1;
		
		int32 sourceStoryLength = sourceTextModel->TotalLength();
		if (sourceStart > sourceStoryLength - 1) {
			break;
		}
		if (sourceLength < 1) {
			break;
		}
		TextIndex sourceEnd = sourceStart + sourceLength;
		if (sourceEnd > sourceStoryLength - 1) {
			break;
		}

		// Validate the range of text to be replaced in the destination story.
		InterfacePtr<ITextModel> destTextModel(destStory, UseDefaultIID());
		if (destTextModel == nil) {
			break;
		}
		
		destLength=destTextModel->GetPrimaryStoryThreadSpan()-1;
		int32 destStoryLength = destTextModel->TotalLength();
		if (destStart> destStoryLength - 1) {
			break;
		}
		if (destLength < 0) {
			break;
		}
		TextIndex destEnd = destStart + destLength;
		if (destEnd > destStoryLength - 1) {
			break;
		}

		// Create kCopyStoryRangeCmdBoss.
		InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
		if (copyStoryRangeCmd == nil) {
			break;
		}

		// Refer the command to the source story and range to be copied.
		InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
		if (sourceUIDData == nil) {
			break;
		}
		sourceUIDData->Set(sourceStory);
		InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
		if (sourceRangeData == nil) {
			break;
		}
		sourceRangeData->Set(sourceStart, sourceEnd);

		// Refer the command to the destination story and the range to be replaced.
		UIDList itemList(destStory);
		copyStoryRangeCmd->SetItemList(itemList);
		InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);
		destRangeData->Set(destStart, destEnd);

		// Process CopyStoryRangeCmd
		if(CmdUtils::ProcessCommand(copyStoryRangeCmd)==kSuccess)
			retval=kTrue;
		//status = CmdUtils::ProcessCommand(copyStoryRangeCmd);
		
		
		/*	// Validate the range of text to be copied from the source story.
		InterfacePtr<ITextModel> sourceTextModel(sourceStory, UseDefaultIID());
		if (sourceTextModel == nil) 
		{
			//CAlert::InformationAlert("sourceTextModel= nil");
			break;
		}
		int32 sourceStoryLength = sourceTextModel->TotalLength();
		int32	sourceLength=sourceStoryLength-1;
		if (sourceStart > sourceStoryLength - 1) 
		{
			CAlert::InformationAlert("sourceStart > sourceStoryLength - 1");
			break;
		}
		if (sourceLength < 1) 
		{
			//CAlert::InformationAlert("sourceLength < 1");
			break;
		}
		
		int32 sourceEnd = sourceStart + sourceLength;
	
		
		if (sourceEnd > sourceStoryLength - 1) 
		{
			CAlert::InformationAlert("sourceEnd > sourceStoryLength - 1");
			break;
		}

		// Validate the range of text to be replaced in the destination story.
		InterfacePtr<ITextModel> destTextModel(destStory, UseDefaultIID());
		if (destTextModel == nil) 
		{
			CAlert::InformationAlert("destTextModel == nil");
			break;
		}
		int32 destStoryLength = destTextModel->TotalLength();
		int32 destLength=destStoryLength-1;
		if (destStart> destStoryLength - 1) 
		{
			CAlert::InformationAlert("destStart> destStoryLength - 1");
			break;
		}
		if (destLength < 0) 
		{
			CAlert::InformationAlert("destLength < 0");
			break;
		}
		TextIndex destEnd = destStart + destLength;
		if (destEnd > destStoryLength - 1) 
		{
			CAlert::InformationAlert("destEnd > destStoryLength - 1");
			break;
		}
		
		CAlert::InformationAlert("FERMNANDO LOCOCOOCOCOOCOC");
		// Create kCopyStoryRangeCmdBoss.
		InterfacePtr<ICommand> copyStoryRangeCmd(CmdUtils::CreateCommand(kCopyStoryRangeCmdBoss));
		if (copyStoryRangeCmd == nil) 
		{
			CAlert::InformationAlert("copyStoryRangeCmd");
			break;
		}

		// Refer the command to the source story and range to be copied.
		InterfacePtr<IUIDData> sourceUIDData(copyStoryRangeCmd, UseDefaultIID());
		if (sourceUIDData == nil) 
		{
			CAlert::InformationAlert("sourceUIDData");
			break;
		}
		sourceUIDData->Set(sourceStory);
		InterfacePtr<IRangeData> sourceRangeData(copyStoryRangeCmd, UseDefaultIID());
		if (sourceRangeData == nil) 
		{
			 CAlert::InformationAlert("sourceRangeData");
			break;
		}
		sourceRangeData->Set(sourceStart, sourceEnd);

		// Refer the command to the destination story and the range to be replaced.
		UIDList itemList(destStory);
		copyStoryRangeCmd->SetItemList(itemList);
		InterfacePtr<IRangeData> destRangeData(copyStoryRangeCmd, IID_IRANGEDATA2);
		destRangeData->Set(destStart, destEnd);

		// Process CopyStoryRangeCmd
		if(CmdUtils::ProcessCommand(copyStoryRangeCmd)==kSuccess)
			retval=kTrue;
	
		*/
		
	}while(false);
	return(retval);
}





bool16 N2PSQLUtilities::convertTaggetTextToTaggedTextToXML(PMString& thissestring, const K2Vector<N2PNotesStorage*>& fN2PNotesStore)
{
	
	PMString taggedText=thissestring;
	bool16 retval=kTrue;
	
	if(taggedText.NumUTF16TextChars()>0)
	{
		UTF32TextChar comillas=34;
		
		
		int32 posiniremove=-1;
		int32 porToremove=-1;
		
		////Borrar definiciones de estilos parrafos y definiciones de Caracteres
/*
		Por que los elimina?: pienso que es para el manejo de notas en web. para evitar el reemplazo de estas.
 
		int32 positionPStyle = taggedText.IndexOfString("<DefineParaStyle:");
		int32 positionCStyle  = taggedText.IndexOfString("<DefineCharStyle:");
		int32 positionFIRSTPStyle  = taggedText.IndexOfString("<ParaStyle:");
		int32 positionFIRSTCStyle  = taggedText.IndexOfString("<CharStyle:");
		
		
		if(positionPStyle>0 || positionCStyle>0)
		{
			if(positionPStyle>=0)
			{
				if(positionCStyle>=0)
				{
					if(positionPStyle<positionCStyle)
						posiniremove=positionPStyle;
					else
						posiniremove=positionCStyle;
				}
				else
				{
					posiniremove=positionPStyle;
				}
			}
			else
			{	//positionCStyle>=0
				posiniremove=positionCStyle;
			}
		}
		
		if(positionFIRSTPStyle>0 || positionFIRSTCStyle>0)
		{
			if(positionFIRSTPStyle>=0)
			{
				if(positionFIRSTCStyle>=0)
				{
					if(positionFIRSTPStyle<positionFIRSTCStyle)
						porToremove=positionFIRSTPStyle;
					else
						porToremove=positionCStyle;
				}
				else
				{
					porToremove=positionPStyle;
				}
			}
			else
			{	//positionCStyle>=0
				porToremove=positionCStyle;
			}
		}
		
		if(posiniremove>=0 && porToremove>=0)
			taggedText.Remove(posiniremove, (porToremove-posiniremove));
	
			//Al parecer esto funciona en CS3 no se por que pero tambie deberia mandar errores
			//Es un buen punto de verifivacion
			//if(positionPStyle>positionCStyle)
			//{
			//	if(positionFIRSTPStyle>0)
			//		taggedText.Remove(positionCStyle, (positionFIRSTPStyle-positionCStyle));
			//	else
			//		taggedText.Remove(positionCStyle, (taggedText.NumUTF16TextChars()-positionCStyle));
			//}
			//else
			//{
			//	if(positionFIRSTPStyle>0)
			//		taggedText.Remove(positionPStyle, (positionFIRSTPStyle-positionPStyle));
			//	else
			//		taggedText.Remove(positionPStyle, (taggedText.NumUTF16TextChars()-positionPStyle));
			//}
			
		//}
		
	*/	
		
		PMString TabToreplace="<pTabRuler";
		
		//N2PSQLUtilities::BorrarTagDTextoTageado(taggedText,TabToreplace);
		N2PSQLUtilities::ReemplazarSlashesPorCommayPunto(taggedText,TabToreplace);
		
		
		PMString Diagonal="";
		Diagonal.AppendW(92);
		//Diagonal.AppendW(92);
		PMString DDiagonal="";
		DDiagonal.Append("\\\\");
		N2PSQLUtilities::ReplaceAllOcurrencias(taggedText,Diagonal,DDiagonal);
		
	/*	
		
		int32 position = taggedText.IndexOfString("\\");
		while(position!=-1)
		{
			//CAlert::InformationAlert("XXX");
			//CAlert::InformationAlert(taggedText);
			taggedText.Remove(position, 1);
			taggedText.Insert("\\\\",4,position);
			position = taggedText.IndexOfString("\\",position+4);
		}
		
		
		position = taggedText.IndexOfString("\\\\_");
		while(position!=-1)
		{
			//CAlert::InformationAlert("XXX");
			CAlert::InformationAlert(taggedText);
			taggedText.Remove(position, 1);
			taggedText.Insert("\\\\ _",3,position);
			position = taggedText.IndexOfString("\\\\_",position+6);
		}
		
		CAlert::InformationAlert(taggedText);
	*/	
		/*int32 position = taggedText.IndexOfString("<");
		while(position!=-1)
		{
			//CAlert::InformationAlert("XXX");
			//CAlert::InformationAlert(taggedText);
			taggedText.Remove(position, 1);
			taggedText.Insert("&lt;",4,position);
			position = taggedText.IndexOfString("<");
		}
		
		position = taggedText.IndexOfString(">");
		while(position!=-1)
		{
			//CAlert::InformationAlert("WWWWWWWWW");
			//CAlert::InformationAlert(taggedText);
			taggedText.Remove(position, 1);
			taggedText.Insert("&gt;",4,position);
			position = taggedText.IndexOfString(">");
		}*/
		
		//CAlert::InformationAlert("AAAAAAAAA");
		PMString XMLString="<?xml version=";
		XMLString.AppendW(comillas);
		XMLString.Append("1.0");
		XMLString.AppendW(comillas);
		XMLString.Append(" encoding=");
		XMLString.AppendW(comillas);
		XMLString.Append("UTF-8");
		XMLString.AppendW(comillas);
		XMLString.Append(" stanlone=");
		XMLString.AppendW(comillas);
		XMLString.Append("yes");
		XMLString.AppendW(comillas);
		XMLString.Append("?>");
		XMLString.Append("\n");
		XMLString.Append("<Story>");
		XMLString.Append("\n");
		XMLString.Append("<TaggedText>");
		
		//CAlert::InformationAlert(taggedText);
		//Borra ultimo caracter que se genera al cambiar a texto tageado
		//PMString* myLasString=taggedText.Substring(taggedText.NumUTF16TextChars()-1,1);
		//if(myLasString->GrabCString()!=";")
		//	taggedText.Remove(taggedText.NumUTF16TextChars()-1,1);
		
		//CAlert::InformationAlert(taggedText);
		
		
		//Reemplazo de comillas simples con caracter en hexadecimal
		PMString ComillaSimple="";
		ComillaSimple.AppendW(27);
		
		PMString ComSimHex="";
		ComSimHex.Append("<0x2019>");
		N2PSQLUtilities::ReplaceAllOcurrencias(taggedText,ComillaSimple,ComSimHex);
		
		//Por si las dudas volvemos a reemplazar pero con comilla en caracter 
		 ComillaSimple="'";
		 ComSimHex="";
		ComSimHex.Append("<0x2019>");
		N2PSQLUtilities::ReplaceAllOcurrencias(taggedText,ComillaSimple,ComSimHex);
		
		
		
		XMLString.Append(taggedText);
		XMLString.Append("</TaggedText>");
		XMLString.Append("\n");
		
		
		
		for(int32 i=0;i<fN2PNotesStore.Length();i++)
		{
			PMString Texto = fN2PNotesStore[i]->ftext;
			PMString author= fN2PNotesStore[i]->fauthor;
			PMString DateCreated= fN2PNotesStore[i]->fcreated;
			PMString DateMod= fN2PNotesStore[i]->flastmodified;
			int32 index= fN2PNotesStore[i]->findex;
			UIDRef StoryRef= fN2PNotesStore[i]->fstoryref;
			UID UIDNote= fN2PNotesStore[i]->fnoteUID;
			
			XMLString.Append("<Note ascd='" + DateCreated + "' asmo='" + DateMod + "' UsrN='" + author +"' index='" );
			XMLString.AppendNumber(index); 	
			XMLString.Append("'>"); 	
			XMLString.Append("<txtsr>" );
			XMLString.Append(Texto);
			XMLString.Append("</txtsr>" );
			XMLString.Append("</Note>" );
			
		}
		XMLString.Append("</Story>");
		taggedText = XMLString;
		
		
		
		//}while(false);
		thissestring=taggedText;
	}
		
	return(retval);
}

bool16 N2PSQLUtilities::ExractOfTextXML_TaggetText(PMString& thissestring)
{
	PMString retval="";
	
	bool16 retvalbbol=kFalse;
	
	UTF32TextChar comillas=34;
		
	int32 position = thissestring.IndexOfString("<TaggedText>");
		
	int32 Lenght = thissestring.IndexOfString("</TaggedText>") ;
	
	
	
	/*PMString AASsa="position=";
	AASsa.AppendNumber(position);
	AASsa.Append(", lenght=");
	AASsa.AppendNumber(Lenght);
	CAlert::InformationAlert(AASsa);
	*/
	
	if(position!=-1 && (Lenght-position)>1 && thissestring.Contains("ASCII-"))
	{
		position=position+12;
		Lenght=Lenght-position;
		
		K2::scoped_ptr<PMString> ptrdf(thissestring.Substring(position, Lenght));
		if(ptrdf)
			retval.Append(*ptrdf);
	
		
		
		position = retval.IndexOfString("&lt;");
		while(position!=-1)
		{
			
			retval.Remove(position, 4);
			retval.Insert("<",1,position);
				
			position = retval.IndexOfString("&lt;");
		}
	
		position = retval.IndexOfString("&gt;");
		while(position!=-1)
		{
			retval.Remove(position, 4);
			retval.Insert(">",1,position);
			position = retval.IndexOfString("&gt;");
		}
		
		retvalbbol=kTrue;
	}
	
	//CAlert::InformationAlert(thissestring);
	//
	//<HyperlinkDestDefn:
	PMString AS="HyperlinkDestDefn";
	N2PSQLUtilities::RemoveTags(retval, AS);
/*	
	////Borrar definiciones de estilos parrafos y definiciones de Caracteres
	int32 positionPStyle = retval.IndexOfString("<DefineParaStyle:");
	int32 positionCStyle  = retval.IndexOfString("<DefineCharStyle:");
	int32 positionFIRSTPStyle  = retval.IndexOfString("<ParaStyle:");
	if(positionPStyle!=-1)
	{
		if(positionFIRSTPStyle!=-1)
			retval.Remove(positionPStyle, (positionFIRSTPStyle-positionPStyle));
		else
			retval.Remove(positionPStyle, (retval.NumUTF16TextChars()-positionPStyle));
			
	}
*/	
	thissestring = retval;
	//CAlert::InformationAlert(thissestring);
	return(retvalbbol);
}


bool16 N2PSQLUtilities::RemoveTags(PMString& CadenaOriginal, PMString& TagABorrar)
{
	
	if(CadenaOriginal.IndexOfString("<"+TagABorrar,0)>0)
	{
		//CAlert::InformationAlert("Si se encontro");
	}
	
	int32 posIniTag = -1;
	int32 posFintagTag=-1;
	int32 posSegIniTag = -1;
	
	
	do
	{
		posIniTag = CadenaOriginal.IndexOfString("<"+TagABorrar,0); 
		
		if(posIniTag>0)
		{
			
			posFintagTag = CadenaOriginal.IndexOfString(">>",posIniTag); 
			if(posFintagTag>0)
			{
				CadenaOriginal.Remove(posIniTag, (posFintagTag-posIniTag)+2);
			}
		}
		
	}while(CadenaOriginal.IndexOfString("<"+TagABorrar,0) > 0);
	
	//CAlert::InformationAlert("Salio");
	return(kTrue);
}


bool16 N2PSQLUtilities::ExtarctOfTextXML_Notes(const PMString& thissestring, K2Vector<N2PNotesStorage*>& fBNotesStore)
{
	PMString retval="";
	
	bool16 retvalbbol=kFalse;
	
	int32 indexOfInicio=0; 
	
	UTF32TextChar comillas=34;
		
	int32 position = thissestring.IndexOfString("<Note ascd='", indexOfInicio);
		
	int32 Lenght = thissestring.IndexOfString(">", position);
	int32 lenghtZ=-1;
	while(position!=-1 && Lenght>0)
	{
	
		N2PNotesStorage* store = new N2PNotesStorage();
		
		position=position+6;
		lenghtZ=Lenght-position;
		
		K2::scoped_ptr<PMString> ptrdf(thissestring.Substring(position, lenghtZ));
		if(ptrdf)
			retval.Append(*ptrdf);
	
		
		
		position = retval.IndexOfString("ascd='");
		if(position!=-1)
		{
			K2::scoped_ptr<PMString> stringC(thissestring.Substring( position+6,  retval.IndexOfString("'", position+6)));
			if(ptrdf)
				store->fcreated= stringC->GrabCString();
			
		}
	
		position = retval.IndexOfString("asmo='");
		if(position!=-1)
		{
			K2::scoped_ptr<PMString> stringC(thissestring.Substring( position+6,  retval.IndexOfString("'", position+6)));
			if(ptrdf)
				store->flastmodified= stringC->GrabCString();
			
		}
		
		position = retval.IndexOfString("UsrN='");
		if(position!=-1)
		{
			K2::scoped_ptr<PMString> stringC(thissestring.Substring( position+6,  retval.IndexOfString("'", position+6)));
			if(ptrdf)
				store->fauthor= stringC->GrabCString();
			
		}
		
		position = retval.IndexOfString("index='");
		if(position!=-1)
		{
			K2::scoped_ptr<PMString> stringC(thissestring.Substring( position+7,  retval.IndexOfString("'", position+7)));
			if(ptrdf)
				store->findex= stringC->GetAsNumber();
			
		}
		
		position = retval.IndexOfString("<txtsr>")+7;
		Lenght = retval.IndexOfString("</txtsr>", position);
		
		lenghtZ=Lenght-position;
		
		if(position!=-1 && lenghtZ>0)
		{
			K2::scoped_ptr<PMString> ptrdfAchis(thissestring.Substring(position, lenghtZ));
			if(ptrdfAchis)
				store->ftext =ptrdfAchis->GrabCString();
		}
		
		fBNotesStore.push_back(store);
		
		//
		Lenght=Lenght+8;
		position = retval.IndexOfString("<Note ascd='", Lenght);
		Lenght = thissestring.IndexOfString(">", position);
		
		retvalbbol=kTrue;
	}
	
	
	//CAlert::InformationAlert(thissestring);
	return(retvalbbol);
}





int32 N2PSQLUtilities::GetUIDOfGraphicFrameSelect()
{
	int32 UIDGraphicFrame=0;
	
	do
	{
		UID UIDTextModel = kInvalidUID;
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}

		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionUtils pointer nil");
			break;
		}

		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionManager pointer nil");
			break;
		}
		
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			break;
		}

		UIDRef selectedItemRef = UIDRef::gNull;
		UID selectedItem = kInvalidUID;
		InterfacePtr<ILayoutTarget> pLayoutTarget(pTextSel, UseDefaultIID());    
		if (pLayoutTarget)
		{
			UIDList itemList = pLayoutTarget->GetUIDList(kStripStandoffs);
			if (itemList.Length() == 1) 
			{
				// get the database
				db = itemList.GetDataBase();
				// get the UID of the selected item
				selectedItem = itemList.At(0);

				selectedItemRef=UIDRef(db,selectedItem);
			}
		}

		UIDGraphicFrame=selectedItem.Get();
		
		InterfacePtr<IGraphicFrameData> graphicFrame(selectedItemRef, UseDefaultIID());
		ASSERT(graphicFrame);
		if (!graphicFrame)
		{
			ASSERT_FAIL("graphicFrame = nil");
			break;
		}
						
		InterfacePtr<IGeometry> myGeometry(selectedItemRef, UseDefaultIID());
		if(myGeometry==nil)
		{
			ASSERT_FAIL("myGeometry = nil");
			break;
		}
						
	}while(false);
	return(UIDGraphicFrame);
}




bool16  N2PSQLUtilities::BuscaYElimina_IDFrameFotoOnXMP(PMString variableXMP, PMString ID_Elemento, PMString FrameID)
{
	bool16 retval=kFalse;
 	bool16 encontro=kFalse;
	do
	{
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		PMString DatosEnXMP=GetXMPVar(variableXMP, document);
		
		int32 CantDatosEnXMP=GetCantDatosEnXMP(DatosEnXMP);
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	=0 ; NumReg < CantDatosEnXMP && encontro==kFalse ; NumReg++)
		{
			PMString jh="MiFrameID=";
			jh.AppendNumber(FrameID.GetAsNumber());
			jh.Append("\n == \n FrameStruct=");
			jh.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
			
			if(ID_Elemento == RegistrosEnXMP[NumReg].IDElemento && RegistrosEnXMP[NumReg].TipoDato=="Foto" )
			{
				encontro=kTrue;
				numregRep=NumReg;
				//PMString kl="se encontro en num=";
				//kl.AppendNumber(numregRep);
				
			}
		}
		
		
		//si se encontro se crean los nuevos registros del XMP
		if(encontro==kTrue)
		{
			DatosEnXMP="";
			for(NumReg=0; NumReg < CantDatosEnXMP ;NumReg++)
			{
				if(NumReg==numregRep)
				{
					//CAlert::InformationAlert("Si se encontro");
					/*DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemPadre);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemento);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(FrameID);			///reemplaza el ID del Frame
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].TipoDato);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].NameDSN);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].LastCheckInNote);
					DatosEnXMP.Append("^");*/
				}
				else
				{
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemPadre);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemento);
					DatosEnXMP.Append(",");
					DatosEnXMP.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].TipoDato);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].NameDSN);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].LastCheckInNote);
					DatosEnXMP.Append("^");
				}
				
				
			}
		}

		
		//salva los nuevos registros
		if(DatosEnXMP.NumUTF16TextChars()<=0)
		{
			
			RemoveXMPVar("N2P_ListaUpdate", document);
			
		}
		else
		{
			//salva los nuevos registros
			SaveXMPVar("N2P_ListaUpdate",DatosEnXMP, document);
		}
		
		retval=kTrue;
		
	}while(false);
	return(retval);
}



 bool16 N2PSQLUtilities::GETCoordenadasDePrimerFrameOfTextModel( UID UIDTextModelofElementoNota,
 												PMRect&  boundsInParentCoo,
 												int32& NumCarOfTextModel,
												K2Vector<PMString>& multiplesFrames,
																int32& NumLinesFaltantes,
																int32& NumLinesRestantes,
																int32& NumCarFaltantes,
																int32& NumCarRestantes)
 {
 	bool16 retval=kFalse;
 	do
 	{
 			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
						(
							kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
							IUpdateStorysAndDBUtils::kDefaultIID
						)));
			
			if(UpdateStorys==nil)
			{
				break;
			}
						
			//
			UIDRef UIDRefOfElementoNota(db,UIDTextModelofElementoNota);
		
			if(UIDRefOfElementoNota==UIDRef::gNull)
			{
				break;
			}
		
		
			//Obtiene el Texmodel apartir del numero de UID
			InterfacePtr<ITextModel> mytextmodel(db,UIDTextModelofElementoNota, UseDefaultIID()); 
			if(mytextmodel==nil)
			{
				
				break;
			}
		
			NumCarOfTextModel=mytextmodel->TotalLength();
			NumCarOfTextModel--;//Se le resta uno por ultimo caracter que indica que es fin del texto
			
			InterfacePtr<IFrameList> frameList(mytextmodel->QueryFrameList());
			ASSERT(frameList != nil);
			if (frameList == nil) {
				break;
			}
		
		InterfacePtr<IFrameListComposer> frameListComposer(frameList, UseDefaultIID());
		if(!frameListComposer) {
			break;
		}
		frameListComposer->RecomposeThruLastFrame();
		
	/*	bool16 wordStarted;
		uint32 characters;
		uint32 words;
		uint32 paragraphs;
		uint32 lines;
		uint32 oversetCharacters;
		uint32 oversetWords;
		uint32 oversetParagraphs;
		uint32 oversetLines;
		bool16 bWordAcrossOverset;
		
		TextStoryThreadHelper helper(mytextmodel);
		
		
		Utils<ITextUtils>()->GetWordCountInfo(mytextmodel,
		0,
		 mytextmodel->TotalLength(),
		ITextUtils::kUpdateAll,
		ITextUtils::kEndCounting,
		wordStarted,
		characters,
		words,
		paragraphs,
		lines,
		oversetCharacters,
		oversetWords,
						 oversetParagraphs,
		 oversetLines,
		 bWordAcrossOverset,
		 helper
		) ;*/
			NumLinesRestantes =0;
			if(Utils<ITextUtils>()->IsOverset(frameList))
				NumLinesRestantes = Utils<ITextUtils>()->CountOversetLines(UIDRefOfElementoNota);
			
				
			//////
			InterfacePtr<IParcelList> parcelList(frameList, UseDefaultIID());
			ASSERT(parcelList != nil);
			if (parcelList == nil) {
				break;
			}
		
			InterfacePtr<ITextParcelList> textParcelList(frameList, UseDefaultIID());
			ASSERT(textParcelList != nil);
			if (textParcelList == nil) {
				break;
			}
		

			PMReal estimatedDepth=0;
			NumLinesFaltantes=0;
			int32 UltimoCaracterMostrado=0;
			NumCarFaltantes=0;
			N2PSQLUtilities::CountWordsAndLines(UIDTextModelofElementoNota, textParcelList, parcelList->GetNthParcelKey(0), parcelList->GetParcelCount(), estimatedDepth, NumLinesFaltantes, UltimoCaracterMostrado, NumCarFaltantes);

			NumCarRestantes = NumCarOfTextModel - UltimoCaracterMostrado;
			//////
			for(int32 indexFrame=0;indexFrame<frameList->GetFrameCount();indexFrame++)
			{
			
				int32 UIDFrame=  frameList->GetNthFrameUID(indexFrame).Get();
				InterfacePtr<ITextFrameColumn> FrameContentModel( frameList->QueryNthFrame(indexFrame), UseDefaultIID());;
				if(FrameContentModel==nil){
					continue;
				}
				
				InterfacePtr<IMultiColumnTextFrame > frameConteiner(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel),UseDefaultIID());
				
				if(!Utils<ITextUtils>()->IsMultiColumnFrame(frameConteiner))
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}
				
				//Obtiene el texto Multicolumna que contiene el Texmodel
				InterfacePtr<ITextColumnSizer> textColumnSizerFlow(Utils<ITextUtils>()->QueryMultiColumnFrame(FrameContentModel), UseDefaultIID());
				if (textColumnSizerFlow == nil)
				{
					ASSERT_FAIL("no es un frame multi container");
					continue;
				}	
				
				//Obtiene el UIDRef del GraphicFrame(Frame Principal del Textstory)
				UIDRef refFrame = N2PSQLUtilities::GetGraphicFrameRef(frameConteiner, kTrue);
				int32 numPagina=0;
				PMString strCoordenadasA="";
				if(indexFrame==0){
					boundsInParentCoo = N2PSQLUtilities::Get_Coordinates_Reales(refFrame, numPagina);
					strCoordenadasA.AppendNumber(boundsInParentCoo.Left());
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(boundsInParentCoo.Top());
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(boundsInParentCoo.Right());
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(boundsInParentCoo.Bottom());
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(numPagina);
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(UIDFrame);
					//CAlert::InformationAlert(strCoordenadasA);
					multiplesFrames.push_back(strCoordenadasA);
				}
				else {
					PMRect boundsInParentCooZ = N2PSQLUtilities::Get_Coordinates_Reales(refFrame, numPagina);
					
					strCoordenadasA.AppendNumber(boundsInParentCooZ.Left());
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(boundsInParentCooZ.Top());
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(boundsInParentCooZ.Right());
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(boundsInParentCooZ.Bottom());
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(numPagina);
					strCoordenadasA.Append(",");
					strCoordenadasA.AppendNumber(UIDFrame);
					//CAlert::InformationAlert(strCoordenadasA);
					multiplesFrames.push_back(strCoordenadasA);
				}		
				//ITextFrameColumn * QueryNthFrame(int32 n)
			}	
		
	}while(false);
 	return(retval);	
 }
 
 
void N2PSQLUtilities::CleanPalette()
{
 	do
 	{
 		
 	
		
		
 		//Para indicar cual lista es la que se debe de llenar
 		WidgetID ListALlenaWidgetID=nil;
 		
		ListALlenaWidgetID = kN2PsqlNotasListBoxWidgetID;
		
 		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}

		InterfacePtr<IControlView>		iControViewList( panel->FindWidget(ListALlenaWidgetID), UseDefaultIID() );
		if(iControViewList==nil)
		{
			CAlert::ErrorAlert("No se encontro el editbox");
			break;
		}
		
 		N2PSQLListBoxHelper listHelper(iControViewList, kN2PSQLPluginID,ListALlenaWidgetID);
		listHelper.EmptyCurrentListBox();
 		
 		
 		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlStTextPubliWidgetID,"");
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlSeccionLabelWidgetID,"");
			
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPageWidgetID,"");
		
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextEnGuiaWidgetID,"");
		
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlUIDRefTexModelDeTitulo_WidgetID,"");
			
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextTituloWidgetID,"");
			
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_Titulo_WidgetID,"");

		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlUIDRefTexModelDeBalazo_WidgetID,"");
		
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextBalazoWidgetID,"");
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_Balazo_WidgetID,"");
			
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextNotaWidgetID,"");
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_ContentNota_WidgetID,"");
					
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextPieWidgetID,"");
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_Pie_WidgetID,"");
			
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextFechaWidgetID,"");
			
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlTextStatusWidgetID,"");
			
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlComboPubliWidgetID,"");
			
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlComboSeccionWidgetID,"");
		
		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlText_ID_Elem_PadreNotaWidgetID,"");

		N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlLastCheckInNoteTextEditWidgetID,"");
 	}while(false);
}

bool16 N2PSQLUtilities::DELETE_Pinches_HyperlinksYBookMarks()
{
	bool16 retval=kFalse;
	do
	{
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
		{
			break;
		}
		
		InterfacePtr<IHyperlinkTable> hyperTable(document, IID_IHYPERLINKTABLE);
		if(hyperTable==nil)
		{
			break;
		}
		
	//	PMString seqName("DELETE Hyperlinks");
	//	CmdUtils::SequenceContext seq(&seqName);
	//	seq.SetState(kFailure);
		
		
		///Source
		int32 i=0;
		int32 HyperlinkSourceCount = hyperTable->GetHyperlinkSourceCount();
		if(HyperlinkSourceCount>0)
		{
			retval=kTrue;
		}
		
	
		/*for(i=0;i < HyperlinkSourceCount; i++)
		{
			UID iHyperlinkSource=hyperTable->GetNthHyperlinkSource(i);
			hyperTable->RemoveHyperlinkSource(iHyperlinkSource);
		}*/
		
		///Destination
		
		int32 linkDestinationCount = hyperTable->GetHyperlinkDestinationCount();
		if(linkDestinationCount>0)
		{
			retval=kTrue;
		}
		
		
		/*for(i=0;i < linkDestinationCount; i++)
		{
			UID iHyperlinkSource=hyperTable->GetNthHyperlinkDestination(i);
			hyperTable->RemoveHyperlinkDestination(iHyperlinkSource);
		}*/
		
		/////////
		int32 linkCount = hyperTable->GetHyperlinkCount();
		if(linkCount>0)
		{
			retval=kTrue;
		}
		/*for(i=0;i < linkCount; i++)
		{
			UID HyperlinkDestination=hyperTable->GetNthHyperlink(i);
			hyperTable->RemoveHyperlink(HyperlinkDestination);
		}*/
		
		////
		int32 BookmarkCount = hyperTable->GetBookmarkCount();
		if(BookmarkCount>0)
		{
			retval=kTrue;	
		}
		
		/*for(i=0;i < BookmarkCount; i++)
		{
			UID Bookmark=hyperTable->GetNthBookmark(i);
			hyperTable->RemoveBookmark(Bookmark);
		}*/
		
		if(retval==kTrue)
		{
			CAlert::InformationAlert("Remove Hyperlink");
		}
		//CAlert::InformationAlert("OKKKK");
		
		//seq.SetState(kSuccess);
	}while(false);
	return retval;
}



void N2PSQLUtilities::SetRootStyleADefaultCharStyle()
{
	do
	{
		
					
		//////////////////////Obtener Base de taso para el Textmodel
	
		InterfacePtr<IWorkspace> theWS(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
		if(theWS==nil)
		{
			break;
		}

	
		InterfacePtr<IStyleNameTable> charStyleNameTable(theWS,IID_ICOMPOSITEFONTLIST);
		if(charStyleNameTable==nil)
		{
			break;
		}
		
		InterfacePtr<IStyleNameTable> ParaStyleNameTable(theWS,IID_ICOMPOSITEFONTLIST);
		if(ParaStyleNameTable==nil)
		{
			break;
		}
		
		UID ParaDefStyleUID = ParaStyleNameTable->GetDefaultStyleUID();
		UID ParaRootStyleUID = ParaStyleNameTable->GetRootStyleUID();
		UID BasicParaStyleUID = charStyleNameTable->FindByName("[No Paragraph Style]");
		
		if(ParaDefStyleUID!=ParaRootStyleUID)
		{
			
			if(BasicParaStyleUID!=nil && BasicParaStyleUID!=kInvalidUID)
			{
				//CAlert::InformationAlert("Pone estilo de parafo");
				ParaStyleNameTable->SetDefaultStyleUID(BasicParaStyleUID);
			}
			else
			{
				//CAlert::InformationAlert("Pone estilo de parafo 2");
				ParaStyleNameTable->SetDefaultStyleUID(ParaRootStyleUID);
			}
				
		}
		
		UID DefaultUIDStyle=charStyleNameTable->GetDefaultStyleUID();
		
		UID rootCharStyleUID = charStyleNameTable->GetRootStyleUID();
		
		UID NoneCharStyleUID = charStyleNameTable->FindByName("[None]");
		
		if(DefaultUIDStyle!=rootCharStyleUID)
		{
			//CAlert::InformationAlert("Pone estilo de caracter");
			charStyleNameTable->SetDefaultStyleUID(NoneCharStyleUID);
		}
		
		//CAlert::InformationAlert("AAAS");
		/*InterfacePtr<IStyleInfo> parentStyleInfo(::GetDataBase(charStyleNameTable),NoneCharStyleUID,UseDefaultIID());
		ASSERT(parentStyleInfo);
		if(parentStyleInfo == nil) 
		{
			
			break;
		}	
	
		parentStyleInfo->SetBasedOn(kInvalidUID);
		parentStyleInfo->SetCharStyle(kInvalidUID);
		parentStyleInfo->SetIsParagraphStyle(kFalse);
		parentStyleInfo->SetIsStyleImported(kFalse);
		//PMString parentName("Root");
	
		
		/*CAlert::InformationAlert("A16");
		// get our name
		PMString styleName = parentStyleInfo->GetName();
		
		
		CAlert::InformationAlert("parentName="+parentName+",styleName="+styleName);
		
		
		
		CAlert::InformationAlert("Se supone que se puso el root como default");
		
		bool16 clearOverrides = kFalse;
		
		ITextAttributeSuite::CharStyleParam removeCharacterStyles = ITextAttributeSuite::kIgnoreCharacterStyles;
		
		ITextAttributeSuite::NextStyleParam autoNextStyle = ITextAttributeSuite::kApplySingleStyle;
		
		ITextAttributeSuite::StyleToAttrParam oldStyleAttrs = ITextAttributeSuite::kRemoveOldStyle; //changed from default

		textAttributeSuite->ApplyStyle( rootCharStyleUID, clearOverrides,removeCharacterStyles, autoNextStyle, oldStyleAttrs ); 
		*/
				
	}while(false);
}



void N2PSQLUtilities::BusquedaPorDialogo(PreferencesConnection PrefConections)
{
	do
	{
		if(N2PsqlLogInLogOutUtilities::ExistsLoggedUsers())
		{
			/******DONGLE VERDE RED
			//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::BusquedaPorDialogo() Sentinel check");
			InterfacePtr<IInterUltraObjectProKy> UltraProCheck(static_cast<IInterUltraObjectProKy*> (CreateObject
			(
				kInterUltraObjetProKyBoss,	// Object boss/class
				IInterUltraObjectProKy::kDefaultIID
			)));
			
			if(UltraProCheck==nil)
			{
				// retval=kFalse;
				break;
			}
		
			PMString ResultString="";
			bool16 retval=UltraProCheck->RedValueN2PPlugin(ResultString);
			if(!retval)
			{

				CAlert::ErrorAlert(ResultString);
				// retval=kFalse;
				break;
			}*/
			//N2PSQLUtilities::ImprimeMensaje("N2PSQLUtilities::BusquedaPorDialogo() Sentinel check fin");
			/********* DONGLE ROJO LOCAL *************
			InterfacePtr<IInterUltraProKy> UltraProCheck(static_cast<IInterUltraProKy*> (CreateObject
			(
				kInterUltraProKyBoss,	// Object boss/class
				IInterUltraProKy::kDefaultIID
			)));
			if(UltraProCheck==nil)
			{
				
				//retval=kFalse;
				break;
			}
			PMString ResultString="";
			bool16 retval=UltraProCheck->CheckDongleN2PPlugins(ResultString);
			if(!retval)
			{
				CAlert::ErrorAlert(ResultString);
				// retval=kFalse;
				break;
			}
			*/
			
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																									  (
																									   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																									   IUpdateStorysAndDBUtils::kDefaultIID
																									   )));
			
			if(UpdateStorys==nil)
			{
				break;
			}
			
/*			PreferencesConnection PrefConections;
			
			PMString NamePreferencia="Default"; //N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
			
			if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
			{
				break;
			}
*/
			
			InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
			if (wrkSpcPrefs == nil)
			{	
				break;
			}	
			
			PMString ID_Usuario = wrkSpcPrefs->GetIDUserLogedString();
			
			InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
			ASSERT(app);
			if (app == nil) {	
				break;
			}
			
			PMString Aplicacion=app->GetApplicationName();
			
			InterfacePtr<IN2PWSDLCltUtils> N2PWSDLCltUtil(static_cast<IN2PWSDLCltUtils*> (CreateObject
																						  (
																						   kN2PwsdlcltUtilsBoss,	// Object boss/class
																						   IN2PWSDLCltUtils::kDefaultIID
																						   )));
			
			if(!	N2PWSDLCltUtil)
			{
				CAlert::InformationAlert("salio");
				break;
			}
			PMString ResultString="";
			bool16 retval=N2PWSDLCltUtil->realizaConneccion(ID_Usuario,
															"192.6.2.2", 
															"",
															Aplicacion, 
															PrefConections.URLWebServices);
			if(!retval)
			{
				break;
			}
			
			bool16 FindOnlyPases=kFalse;
			//CAlert::InformationAlert("AbrirDialogoBusqueda");
			PMString Buscar= AbrirDialogoBusqueda(FindOnlyPases);
			//CAlert::InformationAlert(Buscar);
				
			if(Buscar.NumUTF16TextChars()>0)
			{
				N2PSQLUtilities::ColocarTextoDeWidgetEnN2PSQLPaleta(kN2PsqlPanelWidgetID,kN2PsqlBusquedaTextWidgetID,Buscar);
				N2PSQLUtilities::LLenarListaNotasEnPaletaN2PSQL(Buscar, FindOnlyPases, PrefConections);
			}	
		}
		else
		{
			CAlert::InformationAlert(kN2PInOutNoUsersLogedStringKey);
		}
	}while(false);
}


PMString N2PSQLUtilities::AbrirDialogoBusqueda(bool16& FindOnlyPases)
{
	PMString Busqueda="";
	PMString TablasBusqueda="";
	PMString CondicionesTerminales="";
	
	do
	{
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		if (application == nil)
		{
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: application invalid"); 
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		if (dialogMgr == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: dialogMgr invalid"); 
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kN2PSQLPluginID,			// Our Plug-in ID from MyfDlgID.h. 
			kViewRsrcType,				// This is the kViewRsrcType.
			kN2PsqlBusquedaDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		if (dialog == nil)
		{ 
			ASSERT_FAIL("MyfDlgActionComponent::DoAction: can't create dialog"); 
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		IControlView *CVDialogNuevaPref=dialog->GetDialogPanel();
				
		InterfacePtr<IDialogController> dialogController(CVDialogNuevaPref,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			CAlert::ErrorAlert("N2PsqlWidgetObserver::AbrirDialogoBusqueda: dialogController invalid");
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		dialog->WaitForDialog();
		//Consulta Parametros
		
		PMString Pagina=dialogController->GetTextControlData(kN2PsqlTextPageWidgetID);
		PMString Fecha=dialogController->GetTextControlData(kN2PsqlTextFechaWidgetID);
		PMString GuiaNota=dialogController->GetTextControlData(kN2PsqlTextEnGuiaWidgetID);
		
		PMString MySQLFecha=InterlasaUtilities::ChangeInDesignDateStringToMySQLDateString(Fecha);
		PMString Estado=dialogController->GetTextControlData(kN2PsqlTextStatusWidgetID);
		PMString Seccion=dialogController->GetTextControlData(kN2PsqlComboSeccionWidgetID);
		PMString Publicacion=dialogController->GetTextControlData(kN2PsqlComboPubliWidgetID);
		PMString RoutedTo=dialogController->GetTextControlData(kN2PsqlComboRoutedToWidgetID);
		ITriStateControlData::TriState stateCheckBox =	dialogController->GetTriStateControlData(kN2PSQLCkBoxOnlyPageJumpsWidgetID);
		if(Pagina=="Cancel" || Pagina=="Cancelar")
		{
			Busqueda="";
			break;
		}
		
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			break;
		}	
		
		PMString ID_Usuario = wrkSpcPrefs->GetIDUserLogedString();
		
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if (app == nil)
		{	
			break;
		}	
		PMString nameApp=app->GetApplicationName();
		nameApp.SetTranslatable(kFalse);
		
		if(stateCheckBox==ITriStateControlData::kSelected)		
		{
			FindOnlyPases=kTrue;
			if(nameApp.Contains("InDesign"))
			{
				Busqueda="EXEC BuscaNota2OnlyPases '" + Pagina + "' , '" + Fecha + "' , '" + Seccion  + "' , '" + Publicacion  + "' , '"  + RoutedTo  + "' , '"  + Estado + "', '" + ID_Usuario +"',''";
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlNotasListBoxWidgetID, kFalse);
				N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID, kTrue);
			}
		}
		else
		{
			FindOnlyPases = kFalse;
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlNotasListBoxWidgetID, kTrue);
			N2PSQLUtilities::MuestraWidgetEnN2PSQLPaleta(kN2PsqlPasesListBoxWidgetID, kFalse);
			if(nameApp.Contains("InDesign"))
			{
				Busqueda="CALL Busca_Notas_Por_Parametros3 ('" + Pagina + "' , '" + MySQLFecha + "' , '" + Seccion  + "' , '" + Publicacion  + "' , '"  + RoutedTo  + "' , '"  + Estado + "', '" + ID_Usuario +"',4,'"+GuiaNota+"')";
				//CAlert::InformationAlert(Busqueda);
			}
			else
			{
				Busqueda="CALL Busca_Notas_Por_Parametros3 ('" + Pagina + "' , '" + MySQLFecha + "' , '" + Seccion  + "' , '" + Publicacion  + "' , '"  + RoutedTo  + "' , '"  + Estado + "', '" + ID_Usuario +"',12,'"+GuiaNota+"')";
			}
			//CAlert::InformationAlert(Busqueda);		
				
		}
		
	} while (false);	
	//	
	
	return(Busqueda);
}


/*PMString N2PSQLUtilities::ExportOnlyTextOfUIDTextModelRef(UIDRef textModelRef)
{
	PMString taggedText = ""; 
	
	//Obtiene las notas del Elemento del Articulo
	K2Vector<N2PNotesStorage*> fN2PNotesStore;
	do
	{
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		
		
		IDataBase* db = ::GetDataBase(document);
			if (db == nil)
			{
				ASSERT_FAIL("db is invalid");
				break;
			}
		
		
		UIDList uidList(db);
		
		uidList.Append(textModelRef.GetUID());
		//InterfacePtr<IWorkspace> piWorkspace(GetExecutionContextSession()->QueryWorkspace());
		//if(piWorkspace==nil)
		//	break;
			
		//InterfacePtr<ITaggedTextExportPreferences>TATEXTExportPref((ITaggedTextExportPreferences *) piWorkspace->QueryInterface(IID_ITAGGEDTEXTEXPORTPREFERENCES));
		//if(TATEXTExportPref==nil)
		//	break;
		
		//TATEXTExportPref->SetExportTagForm(kVerbose);
		//TATEXTExportPref->SetExportEncodingPref(kEncodingAscii);
			
		//Utils<IN2PNotesUtils>()->GetNotesOnElementArticle(fN2PNotesStore, textModelRef);
		
		//for(int32 i=0;i<fN2PNotesStore.Length();i++)
		//{
		//	PMString aa= fN2PNotesStore[i]->flastmodified;
		//	CAlert::InformationAlert( aa);
		//}
		///
		
		InterfacePtr<ITextModel> mytextmodel(textModelRef , UseDefaultIID());
		if(mytextmodel==nil)
		{
			//CAlert::ErrorAlert("mytextmodel possible"); 
			break;
		}
		
		InterfacePtr<ITextTarget> targetboss ((ITextTarget*)::CreateObject(kTextSuiteBoss, IID_ITEXTTARGET)); 
		if(!targetboss)
		{
			//CAlert::ErrorAlert("targetboss possible"); 
			break;
		}
		
		int32 storyLength = mytextmodel->GetPrimaryStoryThreadSpan() - 1;
		targetboss->SetTextUnmanaged(textModelRef, RangeData(0, storyLength)); 
		
		//InterfacePtr<IK2ServiceRegistry> registry(GetExecutionContextSession(), UseDefaultIID()); 
		//if(!registry)
		//{
		//	CAlert::ErrorAlert("registry possible"); 
		//	break;
		//}
		//InterfacePtr<IK2ServiceProvider> service(registry->QueryServiceProviderByClassID(kExportProviderService, kTxtExpFilterBoss)); 
		//if(!service)
		//{
		//	CAlert::ErrorAlert("service possible"); 
		//	break;
		//}
		//InterfacePtr<IExportProvider> exportPro(service, UseDefaultIID()); 
		//if(!service)
		//{
		//	break;
		//}
		
		
		
		N2PSQLXferBytes irXferBytes; 
		
		InterfacePtr<IPMStream> stream(StreamUtil::CreateMemoryStreamWrite(&irXferBytes)); 
		if(!stream)
		{
			break;
		}
		
		
		stream->Open(); 
		
		//ErrorCode result = Utils<ISnippetExport>()->ExportToStream(stream, uidList); //CS4
		//exportPro->ExportToStream(stream, document, targetboss, "Text Only", kSuppressUI); 
	
	
			
		stream->Flush(); 
		
		PMString taggedTemp=""; 
		
		
		
		irXferBytes.Seek(0, kSeekFromStart); 
		
		char temp[256]; 
		bool crlf = false; 
		while (irXferBytes.GetStreamState() == kStreamStateGood) 
		{ 
			uint32 anzahl = irXferBytes.Read(temp, 255);
			temp[anzahl] = 0;
			taggedTemp.SetCString(temp, PMString::kUnknownEncoding);
			taggedText.Append(taggedTemp);
		}
		stream->Close();
		
	}while(false);
	return(taggedText);
}
*/


bool16 N2PSQLUtilities::N2PExportPDFPages(PMString& RutaToSaveFile,PMString& RutaTOJpg,PMString Pagina_To_SavePage)
{
	bool16 retval=kFalse;
	do
	{
	
		bool16 status=kFalse;
		const UIFlags uiFlags = kSuppressUI; 

		PMString nombre;
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF document is invalid");
			break;
		}

		document->GetName(nombre);
		if(nombre.Contains(".indd"))
		{
			nombre.Remove(nombre.NumUTF16TextChars() -5,nombre.NumUTF16TextChars() );
		}

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF db is invalid");
			break;
		}

			//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		// Identify pages to export
		   UIDList pageUIDs = UIDList(::GetDataBase(document));
		   InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
		   ASSERT(iPageList);
		   if(!iPageList) 
		   {
		   		ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
			   break;
		   }
		   // Setup the page UID list to include
		   // the pages you want to export
		   int32 cPages = iPageList->GetPageCount();
		   for (int32 iPage = 0;  iPage < cPages || iPage<2; iPage++ )
		   {
		   		
		   		PMString NumberPagesass="";
		   		NumberPagesass.AppendNumber(iPage);
		   		CAlert::InformationAlert(NumberPagesass);
		   		
			   UID uidPage = iPageList->GetNthPageUID(iPage);
			   pageUIDs.Append(uidPage);
			   
			   
			 
			   PMString FotoVersionPagina=RutaToSaveFile;
			   PMString	FotoLastPagina=RutaTOJpg;
			   
			  // CAlert::InformationAlert("1 "+FotoVersionPagina);
			   
			   //FotoVersionPagina.Append(Pagina_To_SavePage );
			   SDKUtilities::AppendPathSeparator(FotoVersionPagina);
				
				//CAlert::InformationAlert("2 "+FotoVersionPagina);
				
				
			   //FotoLastPagina.Append(Pagina_To_SavePage );
			   SDKUtilities::AppendPathSeparator(FotoLastPagina);
			   
			   //CAlert::InformationAlert("3 "+FotoVersionPagina);

			   	//Adiciona el Folder que corresponde al numero de pagina
			   	FotoVersionPagina.Append("Pagina_");
			    FotoVersionPagina.AppendNumber(iPage+1);
			    
			   // CAlert::InformationAlert("4 "+FotoVersionPagina);
			    
			    FotoLastPagina.Append("Pagina_");
			    FotoLastPagina.AppendNumber(iPage+1);
			   
			  // CAlert::InformationAlert("5 "+FotoVersionPagina);
			  
			
			     
			   	if(CreateFolder(FotoVersionPagina,Pagina_To_SavePage))
				{
					PMString PathPrueba=FotoVersionPagina;
					SDKUtilities::AppendPathSeparator(PathPrueba);
					
					//FotoLastPagina.Append("\\");
					PathPrueba.Append(Pagina_To_SavePage);
					//FotoLastPagina.Append(Pagina);
					PathPrueba.Append("_last.pdf");
					//FotoLastPagina.Append("_1.jpg");
					
					IDFile filePrueba;
					if(SDKUtilities::AbsolutePathToSysFile(PathPrueba, filePrueba)!=kSuccess)
					{
						//PMString p("Error in conversion to IDFile");
						break;
					}
					else
					{	
						if (SDKUtilities::FileExistsForRead(FileUtils::SysFileToPMString(filePrueba)) != kSuccess)
						{
							
							SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoLastPagina);
						
							FotoVersionPagina.Append(Pagina_To_SavePage);
							FotoLastPagina.Append(Pagina_To_SavePage);
							FotoVersionPagina.Append("_last.pdf");
							FotoLastPagina.Append("_last.pdf");
							
							//CAlert::InformationAlert("1");
							//CAlert::InformationAlert(FotoVersionPagina);
							//CAlert::InformationAlert(FotoLastPagina);
						}
						else
						{
							int32 count=Contador_Archivos_Carpeta(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoVersionPagina);
							SDKUtilities::AppendPathSeparator(FotoLastPagina);
							
							FotoVersionPagina.Append(Pagina_To_SavePage);
							FotoLastPagina.Append(Pagina_To_SavePage);
							
							FotoVersionPagina.Append("_");
							FotoLastPagina.Append("_");
							
							//system(FotoVersionPagina.GrabCString());
							FotoVersionPagina.Append("last");
							FotoLastPagina.Append("last");
							
							FotoVersionPagina.Append(".pdf");
							FotoLastPagina.Append(".pdf");
							
							
							
							//CAlert::InformationAlert("2");
						}
					}
				}
				UIDRef pageUIDRef = UIDRef(db,uidPage);
				
				CAlert::InformationAlert(FotoVersionPagina);
				CAlert::InformationAlert(FotoLastPagina);
				N2PSQLUtilities::ExportPaginaPDF( pageUIDRef,FotoVersionPagina);
				N2PSQLUtilities::ExportPaginaPDF( pageUIDRef,FotoLastPagina);
				
				/*///////////////////////////////////
				//Para tomar Fot Version de Pagina
				///////////////////////////////////
				UIDRef pageUIDRef = UIDRef(db,uidPage);
				
				//P88iPadshotUtils P88iPadshot(pageUIDRef,0.75,0.75,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse);
				P88iPadshotUtils P88iPadshot(pageUIDRef,1,1,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kTrue,P88iPadshotUtils::kXPHigh);
				ErrorCode MyErr = P88iPadshot.GetStatus();
				if (MyErr == kFailure)
					break;
				//return false;
				
				IDFile sysFile;
				
				
				sysFile = FileUtils::PMStringToSysFile(FotoVersionPagina);
				InterfacePtr<IPMStream> Stream(StreamUtil::CreateFileStreamWrite(sysFile, kOpenOut | kOpenTrunc));
				if (Stream == NULL)
					break;
				//return false;
				
				P88iPadshot.ExportImageToJPEG(Stream);
				
				///////////////////////////////////
				//Para Tomar Foto Last de Pagina
				///////////////////////////////////
				
				//P88iPadshotUtils P88iPadshotLast(pageUIDRef,0.75,0.75,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kFalse);
				P88iPadshotUtils P88iPadshotLast(pageUIDRef,1,1,72.0,72.0,0.0,IShape::kPrinting,kFalse,kFalse,kTrue,P88iPadshotUtils::kXPHigh);
				MyErr = P88iPadshotLast.GetStatus();
				if (MyErr == kFailure)
					break;
				//return false;
				
				//IDFile sysFile;
				
				
				sysFile = FileUtils::PMStringToSysFile(FotoLastPagina);
				InterfacePtr<IPMStream> StreamLast(StreamUtil::CreateFileStreamWrite(sysFile, kOpenOut | kOpenTrunc));
				if (StreamLast == NULL)
					break;
				//return false;
				
				P88iPadshotLast.ExportImageToJPEG(StreamLast);
				*/
				#ifdef WINDOWS
					// bUSCAR EL SIMILAR PARA WINDOWS
					
				#endif
				#ifdef MACINTOSH
					chmod(InterlasaUtilities::MacToUnix(FotoLastPagina).GrabCString(),0777);
					chmod(InterlasaUtilities::MacToUnix(FotoVersionPagina).GrabCString(),0777);
				#endif
		   }
		   
		   
		   
		   if(cPages<2)
		   {
				
				

		   		PMString  ParaBorrar=Pagina_To_SavePage;
				SDKUtilities::AppendPathSeparator(ParaBorrar);
		   		ParaBorrar.Append("Pagina_2");
				SDKUtilities::AppendPathSeparator(ParaBorrar);
				ParaBorrar.Append(Pagina_To_SavePage+"_last.pdf");
		   		remove(InterlasaUtilities::MacToUnix(ParaBorrar).GrabCString());
		   }
		 //  RutaToSaveFile = FotoVersionPagina;
		//	   	RutaTOJpg = FotoLastPagina;
	}while(false);	   
		 //  RutaToSaveFile = FotoVersionPagina;
		//	   	RutaTOJpg = FotoLastPagina;
	return retval;
}


bool16 N2PSQLUtilities::ExportPaginaPDF(UIDRef pageUIDRef,PMString PathFile)
{
	bool16 retval=kFalse;
	do
	{
		K2Vector<PMString> StringListPresetsPDFExport = N2PSQLUtilities::GetPDFExportStyles();
		
		int32 pdfStylesIndex=-1;
		
		for (int32 i = 0 ; i < StringListPresetsPDFExport.Length() ; i++) 
		{
			//CAlert::InformationAlert(StringListPresetsPDFExport[i]);
			if(StringListPresetsPDFExport[i]=="[Smallest File Size]")
			{
				pdfStylesIndex=i;
				//CAlert::InformationAlert(StringListPresetsPDFExport[i]);
			}
		}
		
		if(pdfStylesIndex==-1)
		{
			break;
		}
		//ok ahora exportar el pdf
		
		// get the PDF export style list
        InterfacePtr<IPDFExptStyleListMgr> styleMgr((IPDFExptStyleListMgr*)::QuerySessionPreferences(IPDFExptStyleListMgr::kDefaultIID));
        if (styleMgr == nil)
		{
			ASSERT_FAIL("Failed to get IPDFExptStyleListMgr");
			break;
		}

		UIDRef pdfExportStyleRef = styleMgr->GetNthStyleRef(pdfStylesIndex);
        InterfacePtr<IPDFExportPrefs> pdfExportStyleToSet(pdfExportStyleRef, UseDefaultIID());
		if (pdfExportStyleToSet == nil) 
		{
			ASSERT_FAIL("Failed to get IPDFExportPrefs/pdfExportStyleToSet");
			break;
		}

		// process kSetPDFExportPrefsCmdBoss to change current settings
		InterfacePtr<ICommand> setPDFExportPrefsCmd(CmdUtils::CreateCommand(kSetPDFExportPrefsCmdBoss));
		if (setPDFExportPrefsCmd == nil) 
		{
			ASSERT_FAIL("Failed to get setPDFExportPrefsCmd");
			break;
		}
		InterfacePtr<IPDFExportPrefs> pdfExportPrefs(setPDFExportPrefsCmd, UseDefaultIID());
		if (pdfExportPrefs == nil) 
		{
			ASSERT_FAIL("Failed to get IPDFExportPrefs/pdfExportPrefs");
			break;
		}
		pdfExportPrefs->CopyPrefs(pdfExportStyleToSet);
		
		if(CmdUtils::ProcessCommand(setPDFExportPrefsCmd)!=kSuccess)
		{
			break;
		}
		
		////////////
		
		
		
		
		
		bool16 status=kFalse;
		const UIFlags uiFlags = kSuppressUI; 

		PMString nombre;
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF document is invalid");
			break;
		}

		document->GetName(nombre);
		if(nombre.Contains(".indd"))
		{
			nombre.Remove(nombre.NumUTF16TextChars() -5,nombre.NumUTF16TextChars() );
		}

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF db is invalid");
			break;
		}

		//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		// Identify pages to export
		   UIDList pageUIDs = UIDList(::GetDataBase(document));
		   InterfacePtr<IPageList> iPageList((IPMUnknown*)document, IID_IPAGELIST);
		   ASSERT(iPageList);
		   if(!iPageList) 
		   {
		   		ASSERT_FAIL("CheckInOutDialogController::crearPDF iPageList is invalid");
			   break;
		   }
		   // Setup the page UID list to include
		   // the pages you want to export
		   int32 cPages = iPageList->GetPageCount();
		   for (int32 iPage = 0; iPage < cPages; iPage++ )
		   {
			   UID uidPage = iPageList->GetNthPageUID(iPage);
			   pageUIDs.Append(uidPage);
		   }


		//PMString filePath=PrefConections.PathOfServerFile + ":apaginas:Archivos PDF:";
		//	filePath.Append(nombre);
		//	filePath.Append(".pdf");
		/********/
		// obtain command boss
        InterfacePtr<ICommand> pdfExportCmd(CmdUtils::CreateCommand(kPDFExportCmdBoss));
		if(pdfExportCmd==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF pdfExportCmd is invalid");
			break;
		}
		// set export prefs
		InterfacePtr<IPDFExportPrefs> pdfExportPrefsToExp(pdfExportCmd, UseDefaultIID());
		if(pdfExportPrefs==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF pdfExportPrefs is invalid");
			break;
		}
		
		// set IDFileData
		PMString pmFilePath(PathFile);
		pmFilePath.SetTranslatable(kFalse);
		IDFile sysFilePath = FileUtils::PMStringToSysFile(pmFilePath);
		
		InterfacePtr<ISysFileData> trgIDFileData(pdfExportCmd, IID_ISYSFILEDATA);
		if(trgIDFileData==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF trgIDFileData is invalid");
			break;
		}
		trgIDFileData->Set(sysFilePath);
		
		// set security preferences
		InterfacePtr<IPDFSecurityPrefs> securityPrefs(pdfExportCmd, UseDefaultIID());
		if(securityPrefs==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF securityPrefs is invalid");
			break;
		}
		// todo: add calls to specific methods...

		// set print content prefs
		InterfacePtr<IPrintContentPrefs> printContentPrefs(pdfExportCmd, IID_IPRINTCONTENTPREFS);
		if(printContentPrefs==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF printContentPrefs is invalid");
			break;
		}
		// todo: add calls to specific methods...

		// set to true if outputting a book file 
		InterfacePtr<IBoolData> bookExport(pdfExportCmd, IID_IBOOKEXPORT);
		if(bookExport==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF bookExport is invalid");
			break;
		}
		bookExport->Set(kFalse); 

		// set UI flags
		InterfacePtr<IUIFlagData> uiFlagData(pdfExportCmd, IID_IUIFLAGDATA);
		if(uiFlagData==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF uiFlagData is invalid");
			break;
		}
		uiFlagData->Set(uiFlags );
		
		// set UIDList containing pages to output (item list of command)
		UIDList theList(pageUIDs);
		pdfExportCmd->SetItemList(theList);
		
		// set UIDRefs containing pages to output (IOutputPages)
		InterfacePtr<IOutputPages> outputPages(pdfExportCmd, UseDefaultIID());
		if(outputPages==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF outputPages is invalid");
			break;
		}
		outputPages->Clear();
	//	IDataBase* db = theList.GetDataBase();
		outputPages->SetMasterDataBase(db);
		int32 nProgressItems = theList.Length(); 
		for (int32 index = 0 ; index < nProgressItems ; index++) 
		{
			//theList.GetRef(index)
        	outputPages->AppendOutputPageUIDRef(pageUIDRef);
		}

		// set PDF document name (get name from IDocument)
		InterfacePtr<IDocument> doc ((IDocument*)db->QueryInstance(db->GetRootUID(), IID_IDOCUMENT));
		if(doc==nil)
		{
			ASSERT_FAIL("CheckInOutDialogController::crearPDF doc is invalid");
			break;
		}
		PMString documentName; 
		doc->GetName(documentName);
		outputPages->SetName(documentName);
		
		// setup progress bar, if not suppressing the UI.
		/*K2::scoped_ptr<RangeProgressBar> deleteProgressBar;
		bool16 bShowImmediate = kTrue;
		if( uiFlags != kSuppressUI )
		{
			RangeProgressBar *progress = new RangeProgressBar( "Generating PDF", 0, nProgressItems, bShowImmediate, kTrue, nil, kTrue); 
			pdfExportPrefs->SetProgress(progress);
			deleteProgressBar.reset( progress );
		}*/
		
		// finally process command
		status = CmdUtils::ProcessCommand(pdfExportCmd);
		
	
		
	}while(false);
	
	return retval;
}


/* GetPDFExportStyles
*/
K2Vector<PMString> N2PSQLUtilities::GetPDFExportStyles(void)
{
	K2Vector<PMString> styles;
	styles.clear();
	do 
	{
        InterfacePtr<IPDFExptStyleListMgr> styleMgr	((IPDFExptStyleListMgr*)::QuerySessionPreferences(IPDFExptStyleListMgr::kDefaultIID));
        if (styleMgr == nil)
		{
			ASSERT_FAIL("Failed to load IPDFExptStyleListMgr");
			break;
		}
		int32 numStyles = styleMgr->GetNumStyles();
		for (int32 i = 0 ; i < numStyles ; i++) 
		{
			PMString name;
			ErrorCode status = kSuccess;
			status = styleMgr->GetNthStyleName(i, &name);
			if (status != kSuccess) 
			{
				ASSERT_FAIL(FORMAT_ARGS("Could not get %dth PDF Export style name", i));
				continue;
			}
			else
			{
				styles.push_back(name);
			}
		}
	} while (false);
	return styles;
}

bool16 N2PSQLUtilities::CreateFolder(PMString path,PMString foldername) 
{ 
	//CAlert::InformationAlert("Antes");
	IDFile file;
	FileUtils::PMStringToIDFile(path,file);
	FileUtils::CreateFolderIfNeeded(file,kTrue);
	
	PMString Element=path;
	#ifdef WINDOWS
		//hay que buscar el similar para Windows
	#endif
	#ifdef MACINTOSH
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777);
		SDKUtilities::RemoveLastElement(Element);
		chmod(InterlasaUtilities::MacToUnix(Element).GrabCString(),0777); 
	#endif
	
	//CAlert::InformationAlert("Despues"+Element);
	return kTrue; 
}



int N2PSQLUtilities::Contador_Archivos_Carpeta(PMString Dir)
{
	
	int contador=0;
	
	PMString PathPMS = Dir;
	
	do
	{
				  
  		if(PathPMS.IsEmpty() == kTrue)
		{
			
			break;
		}
	
		SDKFileHelper rootFileHelper(PathPMS);
		IDFile rootIDFile = rootFileHelper.GetIDFile();
		PlatformFileSystemIterator iter;
		if(!iter.IsDirectory(rootIDFile))
		{
			break;
		}

		/*#ifdef WINDOWS
			// Windows dir iteration a little diff to Mac
			rootIDFile.Append(fFilter);
		#endif
		*/

		iter.SetStartingPath(rootIDFile);
		
		PMString TypeFile;
#ifdef MACINTOSH 
		TypeFile="*.jpg";
#endif
#ifdef WINDOWS 
		TypeFile="\\*.*";
#endif
		IDFile IDFile;
		bool16 hasNext= iter.FindFirstFile(IDFile,TypeFile);
		while(hasNext)
		{
			SDKFileHelper fileHelper(IDFile);
			PMString truncP = fileHelper.GetPath();
			if(truncP.Contains(".jpg") && truncP.NumUTF16TextChars()>=4)
			{
				 contador++;
			}
			hasNext= iter.FindNextFile(IDFile);
		}
	}while(false);
	return(contador);////retorna la posicion del laultima preferencia con que se trabajo
}



UID N2PSQLUtilities::GetUIDTextModelFromUIDRefTextFrame(UIDRef RefOfTextFrame)
{
	UID result = kInvalidUID;
	do
	{
		InterfacePtr<IGraphicFrameData> graphicFrameData(RefOfTextFrame, UseDefaultIID());
		if(graphicFrameData==nil)
		{
			
			break;
		}
		else
		{
			
				//UIDTextModelofElementoNota
				UID fromMutiColumnItemUID = graphicFrameData->GetTextContentUID();
				if(fromMutiColumnItemUID==kInvalidUID)
				{
					
					break;
				}
							
				InterfacePtr<ITextColumnSizer> fromTextColumnSizer(RefOfTextFrame.GetDataBase(), fromMutiColumnItemUID, UseDefaultIID());
				if(fromTextColumnSizer==nil)
				{
					
					break;
				}
				InterfacePtr<IMultiColumnTextFrame > fromTextFrame(fromTextColumnSizer, UseDefaultIID());
				if(fromTextFrame==nil)
				{
					
					break;
				}
						
				InterfacePtr<ITextModel> fromTextModel(fromTextFrame->QueryTextModel());
				if(fromTextModel==nil)
				{
					
					break;
				}
					
				IFrameList* Listffame = fromTextModel->QueryFrameList();
				 result = Listffame->GetTextModelUID();
				
			
					
		}
	}while(false);
	
	return result;
}




void N2PSQLUtilities::ReplaceAllOcurrencias(PMString& origen,PMString &Target, PMString& replace)
{
	PMString taggedTextXX=origen;
	//CAlert::InformationAlert(Target);
	//CAlert::InformationAlert(replace);
	int32 position = taggedTextXX.IndexOfString(Target);
	while(position>=0)
	{
		//CAlert::InformationAlert("XXX");
		//CAlert::InformationAlert(taggedText);
		//PMString AS="";
		//AS.AppendNumber(position);
		//CAlert::InformationAlert(AS);
		taggedTextXX.Remove(position, Target.CharCount());
		taggedTextXX.Insert(replace,position,replace.CharCount());
		//CAlert::InformationAlert(taggedTextXX);
		position = taggedTextXX.IndexOfString(Target,position+replace.CharCount());
	}
	origen=taggedTextXX;
	//CAlert::InformationAlert(taggedTextXX);
}


//enviar inicio de tag ejemplo: <tapruler
void N2PSQLUtilities::BorrarTagDTextoTageado(PMString& origen,PMString &Target)
{
	PMString taggedTextXX=origen;
	PMString mayorque= ">";
	
	int32 position = taggedTextXX.IndexOfString(Target);
	while(position>0)
	{
		int32 positionmenorque = taggedTextXX.IndexOfString(mayorque,position);
		
		taggedTextXX.Remove(position, (positionmenorque-position)+1);
		//taggedTextXX.Insert(replace,position,replace.CharCount());
		position = taggedTextXX.IndexOfString(Target,position);
	}
	origen=taggedTextXX;
}

//enviar inicio de tag ejemplo:
void N2PSQLUtilities::ReemplazarSlashesPorCommayPunto(PMString& origen,PMString &Target)
{
	PMString taggedTextXX=origen;
	PMString mayorque= ">";
	
	PMString Buscar="\\,\\,\\";
	PMString Remplazar="\\,.\\";
	int32 position = taggedTextXX.IndexOfString(Target);
	while(position>0)
	{
		int32 positionmenorque = taggedTextXX.IndexOfString(mayorque,position);
		
		int32 posicionBuscar=taggedTextXX.IndexOfString(Buscar,position);
		while(posicionBuscar<positionmenorque && posicionBuscar>0)
		{
			//reemplazar
			taggedTextXX.Remove(posicionBuscar, Buscar.CharCount());
			taggedTextXX.Insert(Remplazar,posicionBuscar,Remplazar.CharCount());
			posicionBuscar=taggedTextXX.IndexOfString(Buscar,position);
		}
		//taggedTextXX.Remove(position, (positionmenorque-position)+1);
		
		//taggedTextXX.Insert(replace,position,replace.CharCount());
		position = taggedTextXX.IndexOfString(Target,positionmenorque);
	}
	
	origen=taggedTextXX;
}
/*ErrorCode BuscaNotasOnElemDArticulo(K2Vector<N2PNotesStorage*> fN2PNotesStore;,const UIDRef& StoryElementOfArticle)
{
	ErrorCode error=kFailure;
	do
	{
		Utils<IN2PNotesUItils>()->GetNotesOnElementArticle(fN2PNotesStore, StoryElementOfArticle);
		
	}while(false);
	return error;
}*/




bool16 N2PSQLUtilities::VerificarSiExisteDocumentoSobrePathInicial(IDocument * document)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IApplication> theApp(GetExecutionContextSession()->QueryApplication());
		if(theApp==nil)
		{
			CAlert::InformationAlert("theApp==nil");
			break;
		}
			
		InterfacePtr<IDocumentList> docList(theApp->QueryDocumentList());
		if(docList==nil)
		{
			CAlert::InformationAlert("docList==nil");
			break;
		}
			
		if(docList->GetDocCount()<=0){
			
			CAlert::InformationAlert("docList->GetDocCount()<=0");
			break;
		}
			
		int32 numDocOfFront=-1;
		for(int32 i=0;i<docList->GetDocCount();i++)
		{
			IDocument *DocOfList =docList->GetNthDoc(i);
			if(document==DocOfList)
			{
				numDocOfFront=i;
			}
		}
			
		InterfacePtr<IFileList> fileList(docList, IID_IFILELIST);
		if(fileList==nil)
		{
			CAlert::InformationAlert("fileList==nil");
			break;
		}
				
		//const PMString* PathDocument=fileList->GetNthPathName(numDocOfFront);
		//IDFile* IDFileOfDocument = fileList->GetNthFile(numDocOfFront); 
		const IDFile *IDFileOfDocument= fileList->GetNthFile(numDocOfFront); 
		//FileUtils::PMStringToIDFile(PathDocument->GrabCString(),IDFileOfDocument);
					
		/*if(SDKUtilities::FileExistsForRead(PathDocument->GrabCString())!=kSuccess)
		{
			PMString NameDoc=PathDocument->GrabCString();
			CAlert::InformationAlert("Verifica si existe este archivo en la ruta indicada. "+NameDoc);
			break;
		}*/
		if(!FileUtils::DoesFileExist(*IDFileOfDocument))
		{
			break;
		}
			
		retval=kTrue;
	}while(false);
	return retval;
}



bool16 N2PSQLUtilities::SetInComboSobrePaletaN2P(WidgetID idCombo,PMString ItemToSelect)
{
	bool16 retval=kFalse;	
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{

			break;
		}

		IControlView *ControlView = panel->FindWidget(idCombo);
		if(ControlView==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos ComboCView");
			CAlert::InformationAlert("ControlView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListDataDirigidoa(ControlView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataDirigidoa == nil)
		{
			CAlert::InformationAlert("dropListDataDirigidoa");
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos dropListDataDirigidoa");
			break;
		}
		

		int32 indexToSelect=dropListDataDirigidoa->GetIndex(ItemToSelect);
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ControlView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			CAlert::InformationAlert("IDDLDrComboBoxSelecPrefer");
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos IDDLDrComboBoxSelecPrefer");
			break;
		}


		if(indexToSelect>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(indexToSelect);
		}
		
	}while(false);
	return(retval);
}



void N2PSQLUtilities::ReplaceRetornBy(PMString& origen, PMString& replace)
{
	PMString taggedTextXX=origen;
	//CAlert::InformationAlert(Target);
	//CAlert::InformationAlert(replace);
	int32 position = taggedTextXX.LastIndexOfWChar(13);
	while(position>=0)
	{
		//CAlert::InformationAlert("XXX");
		//CAlert::InformationAlert(taggedText);
		//PMString AS="";
		//AS.AppendNumber(position);
		//CAlert::InformationAlert(AS);
		#ifdef WINDOWS
			taggedTextXX.Remove(position, 2);//Target.CharCount()
			taggedTextXX.Insert(replace,position,replace.CharCount());
		#endif
		#ifdef MACINTOSH
			taggedTextXX.Remove(position, 1);//Target.CharCount()
			taggedTextXX.Insert(replace,position,replace.CharCount());
		#endif	
		//CAlert::InformationAlert(taggedTextXX);
		position = taggedTextXX.LastIndexOfWChar(13);
	}
	origen=taggedTextXX;
	//CAlert::InformationAlert(taggedTextXX);
}


void  N2PSQLUtilities::ExportPDFAsPresetName( PMString Path,PMString& NombrePref)
{
	do
	{	
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		// Get a pointer to the document database
		IDataBase *piDataBase = ::GetDataBase(document);
		
		////
		PMString filePathToExport = Path;
		//SDKUtilities::AppendPathSeparator(filePathToExport);
		//filePathToExport.Append(NombreAdd);
		//filePathToExport.Append("");
		 
		InterfacePtr<ICommand> pageExportCmd(CmdUtils::CreateCommand(kPDFExportCmdBoss));
		if (pageExportCmd == nil) 
		{ 
			CAlert::InformationAlert("pageExportCmd");
			ASSERT_FAIL("pageExportCmd==nil");
			break; 
		}
	
		// Get the IBookOutputActionCmdData data interface
		InterfacePtr<IOutputPages>  data(pageExportCmd, UseDefaultIID());
		if (data == nil) 
		{ 
			CAlert::InformationAlert("data");
			break; 
		}

		
		
		UIDRef docUIDRef = ::GetUIDRef(document);
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			CAlert::InformationAlert("db");
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		
		// Create a PDF Export Command
		InterfacePtr<ICommand> piPDFExportCmd(CmdUtils::CreateCommand(kPDFExportCmdBoss));
		// Get the PDF Export Preferences interface
		InterfacePtr<IPDFExportPrefs> piPDFExportPrefs(piPDFExportCmd, UseDefaultIID());
			
			
		// Get the PDF styles list manager
		InterfacePtr<IWorkspace> piWorkspace(GetExecutionContextSession()->QueryWorkspace());
		if(piWorkspace==nil)
		{
			CAlert::InformationAlert("piWorkspace");
			break;
		}
			
		
		InterfacePtr<IPDFExptStyleListMgr> piPDFExptStyleListMgr((IPDFExptStyleListMgr *) piWorkspace->QueryInterface(IID_IPDFEXPORTSTYLELISTMGR));
		if(piPDFExptStyleListMgr==nil)
		{
			CAlert::InformationAlert("piPDFExptStyleListMgr");
			break;
		}
			
		
		
		int32 nStyleIndex = -1;
			for(int32 i=0;i<piPDFExptStyleListMgr->GetNumStyles(); i++)
			{
				PMString NomStyleActual="";
				piPDFExptStyleListMgr->GetNthStyleName(i,&NomStyleActual);
			//	CAlert::InformationAlert(NomStyleActual);
				
				if(NombrePref==NomStyleActual)
				{
					nStyleIndex=i;
					i=piPDFExptStyleListMgr->GetNumStyles();
				}
				
			}
			
		if(nStyleIndex<0)
		{
			//CAlert::InformationAlert("nStyleIndex");
			break;
		}
		
		// Get the UIDRef for the style
			UIDRef styleRef = piPDFExptStyleListMgr->GetNthStyleRef(nStyleIndex);
			// Get the export preferences interface for our style and copy it into the command's interface
			InterfacePtr<IPDFExportPrefs> piMyPDFStylePrefs(styleRef, UseDefaultIID());
			piPDFExportPrefs->CopyPrefs(piMyPDFStylePrefs);
			//
			//piPDFExportPrefs->SetVisibleGuidesGrids(kTrue);
			piPDFExportPrefs->SetPDFExEmbedFonts(kTrue);
			piPDFExportPrefs->SetPDFExSubsetFonts(kTrue);
			
		// outputFile is the file to which the PDF will be exported - You have to set its value
			IDFile outputFile = SDKUtilities::PMStringToSysFile( &filePathToExport);		
			// Specify the output file
			InterfacePtr<ISysFileData> piFileData(piPDFExportCmd, UseDefaultIID());
			piFileData->Set(outputFile);

			// Get the pages to be exported and set them on the command's output pages interface
			InterfacePtr<IPageList> piPageList(document, UseDefaultIID());
			int32 nPages = piPageList->GetPageCount();
			UIDList pageUIDList(piDataBase);
			for(int32 i=0; i<nPages; i++)
			{
				UID uidPage = piPageList->GetNthPageUID(i);
				pageUIDList.Append(uidPage);
			}
			InterfacePtr<IOutputPages> piOutputPages(piPDFExportCmd, UseDefaultIID());
			piOutputPages->InitializeFrom(pageUIDList, kFalse);

			// Process the command
			CmdUtils::ProcessCommand(piPDFExportCmd);
			
			//CAlert::InformationAlert("Exporto o salio de la exportacion");
			
	}while(false);
}



bool16  N2PSQLUtilities::Busca_y_Elimina_En_XMP_IDlementosNotas( PMString ID_ElementoPadre)
{
	bool16 encontro=kFalse;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString DatosEnXMP = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		//Si encontro la Cadena	
		if(DatosEnXMP.NumUTF16TextChars()<=0)
		{
			break;
		}
		
		//Obtiene la cantidad de notas que se encuentren en la pagina 
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		if(CantDatosEnXMP<=0)
		{
			
			break;
		}
		
		
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			
			if(ID_ElementoPadre == RegistrosEnXMP[NumReg].IDElemPadre)
			{
				
				RegistrosEnXMP[NumReg].IDElemPadre="";
				
				RegistrosEnXMP[NumReg].NameDSN = "";
				RegistrosEnXMP[NumReg].LastCheckInNote = "";
				RegistrosEnXMP[NumReg].IDElemPadre="";
				
				if(RegistrosEnXMP[NumReg].TipoDato=="Titulo")
				{	
					RegistrosEnXMP[NumReg].IDElemento="";
				}
				
				if(RegistrosEnXMP[NumReg].TipoDato=="Balazo")
				{
					RegistrosEnXMP[NumReg].IDElemento="";
				}
				
				if(RegistrosEnXMP[NumReg].TipoDato=="PieFoto")
				{
					RegistrosEnXMP[NumReg].IDElemento="";
				}
				
				if(RegistrosEnXMP[NumReg].TipoDato=="ContenidoNota")
				{
					RegistrosEnXMP[NumReg].IDElemento="";
				}
			}
		}
		
		DatosEnXMP="";
		//Se vuelve a guardar los registros en el XMP ahora con los nuevos datos
		for(NumReg=0; NumReg < CantDatosEnXMP ; NumReg++)
		{
			if(RegistrosEnXMP[NumReg].IDElemPadre!="")
			{
				//CAlert::InformationAlert(RegistrosEnXMP[NumReg].IDElemPadre);
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemPadre);
				DatosEnXMP.Append(",");
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemento);
				DatosEnXMP.Append(",");
				DatosEnXMP.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
				DatosEnXMP.Append(",");
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].TipoDato);
				DatosEnXMP.Append(",");
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].NameDSN);
				DatosEnXMP.Append(",");
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].LastCheckInNote);
				DatosEnXMP.Append("^");
			}
		}
		
		if(DatosEnXMP.NumUTF16TextChars()<=0)
		{
			
			RemoveXMPVar("N2P_ListaUpdate", document);
			
		}
		else
		{
			//salva los nuevos registros
			SaveXMPVar("N2P_ListaUpdate",DatosEnXMP, document);
		}
		
		
	}while(false);
	return(kTrue);
}


bool16  N2PSQLUtilities::BuscaYReemplaza_ElementoOnPaginaXMP(PMString variableXMP,PMString ID_Elem_Padre, PMString ID_Elemento,
												 PMString FrameID, PMString TipoDato,
												 PMString NombrePreferDSN,PMString StatusNota,
												 bool16 UnLockStory,
												 PMString FYHUCheckinNota, bool16 EsElementoFoto)
{
	
	bool16 retval=kFalse;
	bool16 encontro=kFalse;
	do
	{
		
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> 
																(CreateObject
																	(
																	    kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																		IUpdateStorysAndDBUtils::kDefaultIID
														)));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PMString DatosEnXMP=GetXMPVar(variableXMP, document);
		
		int32 CantDatosEnXMP=GetCantDatosEnXMP(DatosEnXMP);
		if(CantDatosEnXMP<=0)
		{
			//si no se encontro solo se adiciona el nuevo registro en el texto del xmp
			DatosEnXMP.Append(ID_Elem_Padre);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(ID_Elemento);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(FrameID);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(TipoDato);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(NombrePreferDSN);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(FYHUCheckinNota);
			DatosEnXMP.Append("^");
			//salva los nuevos registros
			SaveXMPVar(variableXMP,DatosEnXMP, document);
			retval=kTrue;
			
			UpdateStorys->CambiaTipoDEstadoDElementoNota(FrameID,StatusNota,kTrue);
			
			//Coloca el Lock estado del anterior TextFrame
			UpdateStorys->CambiaModoDeEscrituraDElementoNota(FrameID,kTrue);
			
			break;
		}


		
		
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros os datos
		for(NumReg	=0 ; NumReg < CantDatosEnXMP && encontro==kFalse ; NumReg++)
		{
			PMString jh="MiFrameID=";
			jh.AppendNumber(FrameID.GetAsNumber());
			jh.Append("\n == \n FrameStruct=");
			jh.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
			
			if(ID_Elemento == RegistrosEnXMP[NumReg].IDElemento && RegistrosEnXMP[NumReg].TipoDato==TipoDato)// && RegistrosEnXMP[NumReg].NameDSN==NombrePreferDSN
			{
				encontro=kTrue;
				numregRep=NumReg;
				//PMString kl="se encontro en num=";
				//kl.AppendNumber(numregRep);
				
			}
		}
		
		const int ActionAgreedValue=1;
		
		if(encontro==kTrue && EsElementoFoto==kTrue  )
		{
			
			retval=kFalse;
			break;
		}
		else
		{
			
			if(encontro==kTrue && EsElementoFoto==kFalse)
			{
				int32 result=CAlert::ModalAlert
				 (
					kN2PDesarReemplazarelElementoStringKey,                       // Alert string
					kYesString,                           // OK button
					kCancelString,                       // Cancel button
					kNullString,                         // No third button
					ActionAgreedValue,                                   // Set OK button to default
					CAlert::eQuestionIcon             // Information icon.
				);
			
				if(result==ActionAgreedValue)
				{
					
					//hay que borrar el texto que se encuentra en el IDModelActual øPreginatr si se desea borrar el texto o desea dejarlo como esta?
					//PMString kl="se borrara texto de=";
					//kl.AppendNumber(numregRep);
					//kl.Append(" UID=");
					//kl.AppendNumber(RegistrosEnXMP[numregRep].IDFrame);
				
				
					//Obtiene el Id del Frame donde fue colocado el elemento anterior
					PMString idTextModelString="";
					idTextModelString.AppendNumber(RegistrosEnXMP[NumReg-1].IDFrame); 
					int32 intthis=RegistrosEnXMP[numregRep].IDFrame;
					UID UIDTextModel=intthis;
				
					
						
						
						bool16 VeniaBlockeado=kFalse;//bandera si indica si no se estaba editando la nota
					
						////////////////////Obtener texto de Texframe
						UIDRef TextModelUIDRef(db,UIDTextModel);
						InterfacePtr<ITextModel> mytextmodel(TextModelUIDRef, UseDefaultIID()); 
						if(mytextmodel!=nil)
						{
							//CAlert::ErrorAlert("mytextmodel");
							//break;/// ItextLockDat para obtener el estado del Lock story y ver si de puede borra el texto del textframe 
							InterfacePtr<IItemLockData> textLoctData(TextModelUIDRef, UseDefaultIID()); 
							if(textLoctData==nil)
							{
								CAlert::ErrorAlert("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota textLoctData is invalid");
								ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota textLoctData is invalid");	
								break;
							}
		
			
							
				
							InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
							ASSERT(app);
							if (app == nil) {	
								break;
							}
			
							PMString nameApp=app->GetApplicationName();
							nameApp.SetTranslatable(kFalse);
						
							//Pregunta el nombre de la aplicacion para conecer por que tipo de bloqueo se debe de pregintar
							if(nameApp.Contains("InDesign"))
							{
								if(textLoctData->GetInsertLock())
								{
									UpdateStorys->CambiaModoDeEscrituraDElementoNota(idTextModelString,kFalse);
									VeniaBlockeado=kTrue;
									//CAlert::ErrorAlert("Lockeado");
								}
						
							}
							else
							{
								if(textLoctData->GetAttributeLock())
								{
									UpdateStorys->CambiaModoDeEscrituraDElementoNota(idTextModelString,kFalse);
									VeniaBlockeado=kTrue;
								}
							}
				
							/////////Bora Texto del frame Actual
							TextIndex index=0;		
				
							InterfacePtr<ITextModelCmds> textModelCmds(mytextmodel, UseDefaultIID());
							ASSERT(textModelCmds);
							if (!textModelCmds) 
							{
								CAlert::ErrorAlert("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota textModelCmds is invalid");
								ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota textModelCmds is invalid");	
								break;
							}
							InterfacePtr<ICommand> deleteCmd(textModelCmds->DeleteCmd(index, mytextmodel->TotalLength()-1));
							ASSERT(deleteCmd);
							if (deleteCmd) 
							{
								CmdUtils::ProcessCommand(deleteCmd);
							}
							else
							{
								//CAlert::ErrorAlert("N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota deleteCmdΩ is invalid");
								ASSERT_FAIL(" N2PsqlUpdateStorysAndDBUtils::ActualizaElementoNota deleteCmd is invalid");
							}	
					
							//Quitar el adorno del estatus del TextFrame
							UpdateStorys->CambiaTipoDEstadoDElementoNota(idTextModelString,"",kFalse);
				
							//Quitar el adorno del lapiz del TextFrame
							UpdateStorys->CambiaEstadoLapizUnLock(idTextModelString, 0, kFalse);
				
						}
					
						
			
						//borra lo que se encuentta en el registro
						//RegistrosEnXMP[NumReg].IDElemPadre=-1;
						//RegistrosEnXMP[NumReg].TipoDato="";
						RegistrosEnXMP[NumReg-1].IDFrame=FrameID.GetAsNumber();
				
				
						//Poner el estatus en el nuevoi textframe
						//idTextModelString="";
						///idTextModelString.AppendNumber(FrameID);
						UpdateStorys->CambiaTipoDEstadoDElementoNota(FrameID,StatusNota,kTrue);
				
						//Coloca el Lock estado del anterior TextFrame
						UpdateStorys->CambiaModoDeEscrituraDElementoNota(FrameID,VeniaBlockeado);
						encontro=kTrue;
				}
				else
				{
					retval=kFalse;
					break;
				}
			}else
			{
				if(encontro==kFalse && EsElementoFoto==kFalse)	
				{
					//Poner el estatus en el nuevoi textframe
					
				
					//PMString idTextModelString="";
					//idTextModelString.AppendNumber(RegistrosEnXMP[NumReg-1].IDFrame);
					UpdateStorys->CambiaTipoDEstadoDElementoNota(FrameID,StatusNota,kTrue);
			
					UpdateStorys->CambiaModoDeEscrituraDElementoNota(FrameID,UnLockStory);
				}
			}
		}		
		
	
		encontro=kFalse;
		for(NumReg=0; NumReg < CantDatosEnXMP && encontro==kFalse;NumReg++)
		{
			PMString jh="MiFrameID=";
			jh.AppendNumber(FrameID.GetAsNumber());
			jh.Append("\n == \n FrameStruct=");
			jh.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
			
			if(FrameID.GetAsNumber()==RegistrosEnXMP[NumReg].IDFrame)
			{
				RegistrosEnXMP[NumReg].IDFrame=FrameID.GetAsNumber();
				RegistrosEnXMP[NumReg].IDElemPadre=ID_Elem_Padre;
				RegistrosEnXMP[NumReg].IDElemento=ID_Elemento;
				RegistrosEnXMP[NumReg].TipoDato=TipoDato;
				RegistrosEnXMP[NumReg].NameDSN=NombrePreferDSN;
				RegistrosEnXMP[NumReg].LastCheckInNote=FYHUCheckinNota;
				encontro=kTrue;
				
			}
		}
		
		
		//si se encontro se crean los nuevos registros del XMP
		if(encontro==kTrue)
		{
			DatosEnXMP="";
			for(NumReg=0; NumReg < CantDatosEnXMP ;NumReg++)
			{
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemPadre);
				DatosEnXMP.Append(",");
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemento);
				DatosEnXMP.Append(",");
				DatosEnXMP.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
				DatosEnXMP.Append(",");
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].TipoDato);
				DatosEnXMP.Append(",");
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].NameDSN);
				DatosEnXMP.Append(",");
				DatosEnXMP.Append(RegistrosEnXMP[NumReg].LastCheckInNote);
				DatosEnXMP.Append("^");
				
			}
		}
		else
		{	
			
			//si no se encontro solo se adiciona el nuevo registro en el texto del xmp
			DatosEnXMP.Append(ID_Elem_Padre);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(ID_Elemento);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(FrameID);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(TipoDato);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(NombrePreferDSN);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(FYHUCheckinNota);
			DatosEnXMP.Append("^");
			
			
		}

		
		
		//salva los nuevos registros
		SaveXMPVar(variableXMP,DatosEnXMP, document);
		
		
		
		retval=kTrue;
		
	}while(false);
	return(retval);
}




bool16  N2PSQLUtilities::BuscaYElimina_ElementoOnPaginaXMP(PMString variableXMP, int32 FrameID)
{
	
	bool16 retval=kFalse;
	bool16 encontro=kFalse;
	do
	{
		
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		PMString DatosEnXMP=GetXMPVar(variableXMP, document);
		
		int32 CantDatosEnXMP=GetCantDatosEnXMP(DatosEnXMP);
		


		
		
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	=0 ; NumReg < CantDatosEnXMP && encontro==kFalse ; NumReg++)
		{
			
			
			if(FrameID == RegistrosEnXMP[NumReg].IDFrame)// && RegistrosEnXMP[NumReg].NameDSN==NombrePreferDSN
			{
				encontro=kTrue;
				numregRep=NumReg;
				//PMString kl="se encontro en num=";
				//kl.AppendNumber(numregRep);
				
			}
		}
		
			
		
						
		
	
		//si se encontro se crean los nuevos registros del XMP
		if(encontro==kTrue)
		{
			DatosEnXMP="";
			for(NumReg=0; NumReg < CantDatosEnXMP ;NumReg++)
			{
				if(numregRep!=NumReg)
				{
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemPadre);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemento);
					DatosEnXMP.Append(",");
					DatosEnXMP.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].TipoDato);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].NameDSN);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].LastCheckInNote);
					DatosEnXMP.Append("^");
				}
			}
		}
		
		//salva los nuevos registros
		SaveXMPVar(variableXMP,DatosEnXMP, document);
		
		retval=kTrue;
		
	}while(false);
	return(retval);
}

bool16  N2PSQLUtilities::BuscaYReemplaza_IDFrameFotoOnXMP(PMString variableXMP, PMString ID_Elemento, PMString FrameID)
{
	bool16 retval=kFalse;
 	bool16 encontro=kFalse;
	do
	{
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		PMString DatosEnXMP=GetXMPVar(variableXMP, document);
		
		int32 CantDatosEnXMP=GetCantDatosEnXMP(DatosEnXMP);
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	=0 ; NumReg < CantDatosEnXMP && encontro==kFalse ; NumReg++)
		{
			PMString jh="MiFrameID=";
			jh.AppendNumber(FrameID.GetAsNumber());
			jh.Append("\n == \n FrameStruct=");
			jh.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
			
			if(ID_Elemento == RegistrosEnXMP[NumReg].IDElemento && RegistrosEnXMP[NumReg].TipoDato=="Foto" )
			{
				encontro=kTrue;
				numregRep=NumReg;
				//PMString kl="se encontro en num=";
				//kl.AppendNumber(numregRep);
				
			}
		}
		
		
		//si se encontro se crean los nuevos registros del XMP
		if(encontro==kTrue)
		{
			DatosEnXMP="";
			for(NumReg=0; NumReg < CantDatosEnXMP ;NumReg++)
			{
				if(NumReg==numregRep)
				{
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemPadre);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemento);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(FrameID);			///reemplaza el ID del Frame
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].TipoDato);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].NameDSN);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].LastCheckInNote);
					DatosEnXMP.Append("^");
				}
				else
				{
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemPadre);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemento);
					DatosEnXMP.Append(",");
					DatosEnXMP.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].TipoDato);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].NameDSN);
					DatosEnXMP.Append(",");
					DatosEnXMP.Append(RegistrosEnXMP[NumReg].LastCheckInNote);
					DatosEnXMP.Append("^");
				}
				
				
			}
		}

		
		//salva los nuevos registros
		SaveXMPVar(variableXMP,DatosEnXMP, document);
		
		retval=kTrue;
		
	}while(false);
	return(retval);
}




bool16  N2PSQLUtilities::Busca_y_Reemplaza_En_XMP_IDlementosNotas( PMString ID_ElementoPadreAnterior , PMString ID_ElementoPadreNuevo,
												 PMString Id_Titulo,PMString Id_PieFoto, 
												 PMString Id_Balazo,PMString Id_Content,
												 PMString FYHUCheckinNota, PMString DSNNuevo,
												 PMString FechaNueva)
{
	bool16 encontro=kFalse;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString DatosEnXMP = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		//Si encontro la Cadena	
		if(DatosEnXMP.NumUTF16TextChars()<=0)
		{
			
			break;
		}
		
		//Obtiene la cantidad de notas que se encuentren en la pagina 
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		if(CantDatosEnXMP<=0)
		{
			
			break;
		}
		
		
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	= 0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			
			if(ID_ElementoPadreAnterior == RegistrosEnXMP[NumReg].IDElemPadre)
			{
				RegistrosEnXMP[NumReg].NameDSN = DSNNuevo;
				RegistrosEnXMP[NumReg].LastCheckInNote = FechaNueva;
				RegistrosEnXMP[NumReg].IDElemPadre=ID_ElementoPadreNuevo;
				
				if(RegistrosEnXMP[NumReg].TipoDato=="Titulo")
				{	
					RegistrosEnXMP[NumReg].IDElemento=Id_Titulo;
				}
				
				if(RegistrosEnXMP[NumReg].TipoDato=="Balazo")
				{
					RegistrosEnXMP[NumReg].IDElemento=Id_Balazo;
				}
				
				if(RegistrosEnXMP[NumReg].TipoDato=="PieFoto")
				{
					RegistrosEnXMP[NumReg].IDElemento=Id_PieFoto;
				}
				
				if(RegistrosEnXMP[NumReg].TipoDato=="ContenidoNota")
				{
					RegistrosEnXMP[NumReg].IDElemento=Id_Content;
				}
			}
		}
		
		DatosEnXMP="";
		//Se vuelve a guardar los registros en el XMP ahora con los nuevos datos
		for(NumReg=0; NumReg < CantDatosEnXMP ; NumReg++)
		{
			DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemPadre);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(RegistrosEnXMP[NumReg].IDElemento);
			DatosEnXMP.Append(",");
			DatosEnXMP.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(RegistrosEnXMP[NumReg].TipoDato);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(RegistrosEnXMP[NumReg].NameDSN);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(RegistrosEnXMP[NumReg].LastCheckInNote);
			DatosEnXMP.Append("^");
			
		}
		
		//salva los nuevos registros
		SaveXMPVar("N2P_ListaUpdate",DatosEnXMP, document);
		
	}while(false);
	return(kTrue);
}

/*
	
*/
bool16 N2PSQLUtilities::ColocaDatosDePaginaOnXMP(int32 numeroPaginaActual,PMString Seccion,PMString Publicacion,PMString FechaPubli)
{
	
	bool16 encontro=kFalse;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString DatosEnXMP = GetXMPVar("N2PSAQL_To_N2PCS", document);
		
		int32 CantDatosEnXMP=GetCantDatosEnXMP(DatosEnXMP);
		
		ClassDatosDePagEnXMP *RegistrosDDatosDePagEnXMP=new ClassDatosDePagEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDDatosDePagXMP(DatosEnXMP,RegistrosDDatosDePagEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	=0 ; NumReg < CantDatosEnXMP && encontro==kFalse ; NumReg++)
		{
			
			if(numeroPaginaActual == RegistrosDDatosDePagEnXMP[NumReg].NumPagina)
			{
				encontro=kTrue;
				numregRep=NumReg;
				//PMString kl="se encontro en num=";
				//kl.AppendNumber(numregRep);
				
			}
		}
		
		//si se encontro se crean los nuevos registros del XMP
		if(encontro==kFalse)
		{
			
			//si no se encontro solo se adiciona el nuevo registro en el texto del xmp
			DatosEnXMP.AppendNumber(numeroPaginaActual);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(Seccion);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(Publicacion);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(FechaPubli);
			DatosEnXMP.Append("^");
		}


		
		//salva los nuevos registros
		SaveXMPVar("N2PSAQL_To_N2PCS",DatosEnXMP, document);
		
	}while(false);
	return(encontro);

}

bool16 N2PSQLUtilities::ReemplazaLastCheckInNotaEnXMP(PMString IDNote,PMString UltimoCheckIn, IDocument* document)
{
	
	bool16 encontro=kFalse;
	do
	{
		
		
		if (document == nil)
		{
			break;
		}
		
		
		PMString DatosEnXMP = N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
		
		//Si encontro la Cadena	
		if(DatosEnXMP.NumUTF16TextChars()<=0)
		{
			
			break;
		}
		
		//Obtiene la cantidad de notas que se encuentren en la pagina 
		int32 CantDatosEnXMP=N2PSQLUtilities::GetCantDatosEnXMP(DatosEnXMP);
		if(CantDatosEnXMP<=0)
		{
			
			break;
		}
	
		ClassRegEnXMP *RegistrosEnXMPTOUpdate=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMPTOUpdate);
	
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el ID de la nota sobre los registros del XMP para sustituir la ultima fecha en que fue guardada una nota
		for(NumReg	=0 ; NumReg < CantDatosEnXMP ; NumReg++)
		{
			PMString jh="ID ";
			jh.Append( RegistrosEnXMPTOUpdate[NumReg].IDElemPadre);
			jh.Append(" en registro ");
			jh.AppendNumber( NumReg);
			
			if(IDNote == RegistrosEnXMPTOUpdate[NumReg].IDElemPadre)
			{
			
				RegistrosEnXMPTOUpdate[NumReg].LastCheckInNote=UltimoCheckIn;
			}
		}
		
		
		
		
		//si se encontro se crean los nuevos registros del XMP
		
		DatosEnXMP="";
		PMString NumberStr="";
		for(NumReg=0; NumReg < CantDatosEnXMP ;NumReg++)
		{
			NumberStr="";
			NumberStr.Append(RegistrosEnXMPTOUpdate[NumReg].IDElemPadre);
			DatosEnXMP.Append(NumberStr);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(RegistrosEnXMPTOUpdate[NumReg].IDElemento);
			DatosEnXMP.Append(",");
			NumberStr="";
			NumberStr.AppendNumber(RegistrosEnXMPTOUpdate[NumReg].IDFrame);
			DatosEnXMP.Append(NumberStr);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(RegistrosEnXMPTOUpdate[NumReg].TipoDato);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(RegistrosEnXMPTOUpdate[NumReg].NameDSN);
			DatosEnXMP.Append(",");
			DatosEnXMP.Append(RegistrosEnXMPTOUpdate[NumReg].LastCheckInNote);
			DatosEnXMP.Append("^");
		
		}
		
		//salva los nuevos registros
		SaveXMPVar("N2P_ListaUpdate" ,DatosEnXMP, document);
		
	}while(false);
	return(kTrue);

}

int32 N2PSQLUtilities::GetCantDatosEnXMP(PMString DatosEnXMP)
{
	int32 Cant=0;
	int32 PosEndLine=0;

	if(DatosEnXMP.NumUTF16TextChars()>0)
	{
		while(DatosEnXMP.NumUTF16TextChars()>0)
		{
			PosEndLine=DatosEnXMP.IndexOfString("^");
			if(PosEndLine>0)
			{
				DatosEnXMP.Remove(0,PosEndLine+1);
			}
			Cant++;
		}
	}
	
	PMString nm="";
	nm.AppendNumber(Cant);
	
	return(Cant);
}

bool16 N2PSQLUtilities::LlenaStructRegistrosDeXMP(PMString DatosEnXMP,ClassRegEnXMP *RegistrosEnXM)
{
	PMString Linea="";
	PMString Campo="";
	int32 Cant=0;
	int32 PosEndLine=0;
	int32 PosComa=0;
	int32 numCampo=0;

	if(DatosEnXMP.NumUTF16TextChars()>0)
	{
		while(DatosEnXMP.NumUTF16TextChars()>0)
		{
			Linea.Clear();
			PosEndLine=DatosEnXMP.IndexOfString("^");
			if(PosEndLine == -1)
			{
				continue;
			}
			
			Linea.Append(DatosEnXMP,PosEndLine);
			
			numCampo=0;
			do
			{
				PosComa=Linea.IndexOfWChar(44);
				if(PosComa != -1)
				{
					Campo.Clear();
					Campo.Append(Linea,PosComa);
				
					switch(numCampo)
					{
					case 0:
						{
							RegistrosEnXM[Cant].IDElemPadre=Campo;
							//PMString nl="IDNota=";
							//nl.AppendNumber(RegistrosEnXM[Cant].IDElemPadre);
							
							break;
						}
					case 1:
						{
							RegistrosEnXM[Cant].IDElemento=Campo;
							//PMString nl="IDNota=";
							//nl.AppendNumber(RegistrosEnXM[Cant].IDElemPadre);
							
							break;
						}
					case 2:
						{
							RegistrosEnXM[Cant].IDFrame = Campo.GetAsNumber();
							//PMString nl="IDFrame=";
							//nl.AppendNumber(RegistrosEnXM[Cant].IDFrame);
							
							break;
						}
					case 3:
						{
							RegistrosEnXM[Cant].TipoDato=Campo;
							
							break;
						}
					case 4:
						{
							RegistrosEnXM[Cant].NameDSN=Campo;
							
							break;
						}
						
					case 5:
						{
							RegistrosEnXM[Cant].LastCheckInNote=Campo;
							
							break;
						}
						
					}
					
					Linea.Remove(0,PosComa+1);
				}
				
				numCampo++;	
				
			}while(Linea.NumUTF16TextChars()>0 && numCampo<6);
		
			if(numCampo==6)
			{
			
				RegistrosEnXM[Cant].LastCheckInNote=Linea;
			}
			
			
			Cant++;
			
			
			DatosEnXMP.Remove(0,PosEndLine+1);
			
			
		}
	}
	return(kTrue);
}


bool16 N2PSQLUtilities::LlenaStructRegistrosDDatosDePagXMP(PMString DatosEnXMP,ClassDatosDePagEnXMP* RegistrosEnXM)
{
	PMString Linea="";
	PMString Campo="";
	int32 Cant=0;
	int32 PosEndLine=0;
	int32 PosComa=0;
	int32 numCampo=0;

	if(DatosEnXMP.NumUTF16TextChars()>0)
	{
		while(DatosEnXMP.NumUTF16TextChars()>0)
		{
			Linea="";
			PosEndLine=DatosEnXMP.IndexOfString("^");
			if(PosEndLine==-1)
			 continue;
			Linea.Append(DatosEnXMP,PosEndLine);
			
			numCampo=0;
			do
			{
				PosComa=Linea.IndexOfWChar(44);
					
				if(PosEndLine!=-1)
				{
					//
					Campo="";
					Campo.Append(Linea,PosComa);
				
					switch(numCampo)
					{
					case 0:
						{
							RegistrosEnXM[Cant].NumPagina=Campo.GetAsNumber();
							//PMString nl="IDNota=";
							//nl.AppendNumber(RegistrosEnXM[Cant].IDElemPadre);
							
							break;
						}
					case 1:
						{
							RegistrosEnXM[Cant].Seccion=Campo;
							//PMString nl="IDFrame=";
							//nl.AppendNumber(RegistrosEnXM[Cant].IDFrame);
							
							break;
						}
					case 2:
						{
							RegistrosEnXM[Cant].Pubicacion=Campo;
							
							break;
						}
					case 3:
						{
							RegistrosEnXM[Cant].FechaPublicacion=Campo;
							
							break;
						}
						
					}
					
					Linea.Remove(0,PosComa+1);
				}
				
				numCampo++;
				
			}while(Linea.NumUTF16TextChars()>0 && numCampo<5);
		
			if(numCampo==4)
			{
			
				RegistrosEnXM[Cant].FechaPublicacion=Linea;
			}
			
			
			Cant++;
			DatosEnXMP.Remove(0,PosEndLine+1);
			
		}
	}
	return(kTrue);
}

bool16 N2PSQLUtilities::RemoveXMPVar(PMString Variable, IDocument* doc)
{
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open. This snippet requires a front document.");
			CAlert::ErrorAlert(kN2PsqlNoExisteDocumentoStringKey);
			break;
		}
		IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			CAlert::ErrorAlert("metaDataAccess is nil!");
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		metaDataAccess->Remove("http://ns.adobe.com/xap/1.0/", Variable);
		//db->EndTransaction();
	} while(false);
	return(kTrue);
}


bool16 N2PSQLUtilities::SaveXMPVar(PMString Variable,PMString value,  IDocument* doc)
{
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open. This snippet requires a front document.");
			CAlert::ErrorAlert(kN2PsqlNoExisteDocumentoStringKey);
			break;
		}
		
		IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			CAlert::ErrorAlert("metaDataAccess is nil!");
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		metaDataAccess->Set("http://ns.adobe.com/xap/1.0/", Variable, value/*, metaDataFeatures*/);
		//db->EndTransaction();
	} while(false);
	return(kTrue);
}


PMString N2PSQLUtilities::GetXMPVar(PMString Variable, IDocument* doc)
{
	PMString value;
	do 
	{
		
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		IDataBase* db = ::GetDataBase(doc);
		
		//db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		if(!metaDataAccess->Get("http://ns.adobe.com/xap/1.0/", Variable, value/*, metaDataFeatures*/))
		{
			break;
		}
		//db->EndTransaction();
	} while(false);
	return(value);
}




void N2PSQLUtilities::InicializaDebugeo()
{
	//Inicia
	do{
	InterfacePtr<IN2PSQLDebugLog> N2PSDebug(static_cast<IN2PSQLDebugLog*> (CreateObject
		(
		 kN2PSQLDebugLogBoss,										   
		 IN2PSQLDebugLog::kDefaultIID
		 )));
	
	if(N2PSDebug==nil)
	{
		break;
	}
	
	N2PSDebug->IniciaDebug();
	}while(false);
}

void N2PSQLUtilities::FinalizaDebugeo()

{
	//Finaliza
	do{
	InterfacePtr<IN2PSQLDebugLog> N2PSDebug(static_cast<IN2PSQLDebugLog*> (CreateObject
																		   (
																			kN2PSQLDebugLogBoss,										   
																			IN2PSQLDebugLog::kDefaultIID
																			)));
	
	if(N2PSDebug==nil)
	{
		break;
	}
	
	N2PSDebug->TerminaDebug();
	}while(false);
}

void N2PSQLUtilities::ImprimeMensaje(const PMString& msgDebug)
{
	//Inicia
	do{
	InterfacePtr<IN2PSQLDebugLog> N2PSDebug(static_cast<IN2PSQLDebugLog*> (CreateObject
																		   (
																			kN2PSQLDebugLogBoss,										   
																			IN2PSQLDebugLog::kDefaultIID
																			)));
	
	if(N2PSDebug==nil)
	{
		break;
	}
	
	N2PSDebug->printmsg(msgDebug);
	}while(false);
}



bool16  N2PSQLUtilities::Existe_IDFrameFotoOnXMP(PMString variableXMP, PMString FrameID)
{
	
 	bool16 encontro=kFalse;
	do
	{
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();;
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		PMString DatosEnXMP=GetXMPVar(variableXMP, document);
		
		int32 CantDatosEnXMP=GetCantDatosEnXMP(DatosEnXMP);
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	=0 ; NumReg < CantDatosEnXMP && encontro==kFalse ; NumReg++)
		{
			PMString jh="MiFrameID=";
			jh.AppendNumber(FrameID.GetAsNumber());
			jh.Append("\n == \n FrameStruct=");
			jh.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
			//CAlert::InformationAlert(jh);
			
			if( RegistrosEnXMP[NumReg].IDFrame==FrameID.GetAsNumber() )//ID_Elemento == RegistrosEnXMP[NumReg].IDElemento &&
			{
				encontro=kTrue;
				
			}
		}
		
	}while(false);
	return(encontro);
}

/*
 Busca sobre XMP el Frame con el ID FrameID y regresa si existe o se encuentra como elemento de nota
 ademas regresa en ID_Elemento el id del elemento de nota o foto
 */
bool16  N2PSQLUtilities::Busca_IDFrameFotoOnXMP(PMString variableXMP, PMString& ID_Elemento, PMString FrameID)
{
	bool16 encontro=kFalse;
	do
	{
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();;
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		PMString DatosEnXMP=GetXMPVar(variableXMP, document);
		
		int32 CantDatosEnXMP=GetCantDatosEnXMP(DatosEnXMP);
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	=0 ; NumReg < CantDatosEnXMP && encontro==kFalse ; NumReg++)
		{
			PMString jh="MiFrameID=";
			jh.AppendNumber(FrameID.GetAsNumber());
			jh.Append("\n == \n FrameStruct=");
			jh.AppendNumber(RegistrosEnXMP[NumReg].IDFrame);
			
			if(FrameID.GetAsNumber() == RegistrosEnXMP[NumReg].IDFrame && RegistrosEnXMP[NumReg].TipoDato=="Foto" )
			{
				encontro=kTrue;
				numregRep=NumReg;
				ID_Elemento= RegistrosEnXMP[NumReg].IDElemento;
			}
		}
		
		
	}while(false);
	return(encontro);
}


/*
 */
bool16  N2PSQLUtilities::GetIdFrameDElementoNotaANDEtiquetaDNota(PMString variableXMP,int32& int32FrameElementnota ,PMString& Etiqueta, PMString ID_ElementoFoto)
{
	bool16 encontro=kFalse;
	do
	{
		//////////////////////Obtener Base de taso para el Textmodel
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();;
		if (document == nil)
		{
			break;
		}
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		PMString DatosEnXMP=GetXMPVar(variableXMP, document);
		
		int32 CantDatosEnXMP=GetCantDatosEnXMP(DatosEnXMP);
		
		ClassRegEnXMP *RegistrosEnXMP=new ClassRegEnXMP[CantDatosEnXMP];
		
		LlenaStructRegistrosDeXMP(DatosEnXMP,RegistrosEnXMP);
		
		int32 NumReg=0;
		int32 numregRep=0;
		//Busca el FrameID dentro de los registros del XMP para sustituir en caso de que se encuentren por los nuevos datos
		for(NumReg	=0 ; NumReg < CantDatosEnXMP && encontro==kFalse ; NumReg++)
		{
			
			
			if(ID_ElementoFoto == RegistrosEnXMP[NumReg].IDElemento && RegistrosEnXMP[NumReg].TipoDato=="Foto" )
			{
				//Se se encontro elemento foto
				PMString Id_Elemento_PadreDFoto=RegistrosEnXMP[NumReg].IDElemPadre;
				for(int32 i=0 ; i < CantDatosEnXMP && encontro==kFalse ; i++)
				{
					if(Id_Elemento_PadreDFoto == RegistrosEnXMP[i].IDElemPadre && RegistrosEnXMP[i].TipoDato!="Foto" )
					{
						//CAlert::InformationAlert("A1");
						int32FrameElementnota=RegistrosEnXMP[i].IDFrame;
						//Se encontro un elemento de nota , ya se titulo, balazo, notacontenido
						
						//CAlert::InformationAlert("A2");
						UID IDFrameElemenNota=RegistrosEnXMP[i].IDFrame;
						
						//CAlert::InformationAlert("A3");
						UIDRef UIDRefIDFrameElemenNota(db,IDFrameElemenNota);
						
						//CAlert::InformationAlert("A4");
						InterfacePtr<IN2PAdIssueUtils> N2PAdIssueUtil(static_cast<IN2PAdIssueUtils*> (CreateObject
																									  (
																									   kN2PAdIssueUtilsBoss,	// Object boss/class
																									   IN2PAdIssueUtils::kDefaultIID
																									   )));
						
						//CAlert::InformationAlert("A5");
						if(N2PAdIssueUtil==nil)
						{
							
							break;
						}
						
						//CAlert::InformationAlert("A6");
						UIDList ListFrame=UIDList(UIDRefIDFrameElemenNota);
						//CAlert::InformationAlert("A7");
						bool16 visibility=kFalse;
						//CAlert::InformationAlert("A8");
						N2PAdIssueUtil->GetFrameLabelAndVisibilityUIDListLabelfor(ListFrame, Etiqueta, visibility);
						// CAlert::InformationAlert(Etiqueta);
						//CAlert::InformationAlert("A9");
						
						encontro=kTrue;
					}
				}
			}
		}
		
		
	}while(false);
	return(encontro);
}

UIDRef N2PSQLUtilities::PreparaImportImageAndLoadPlaceGun(const UIDRef& docUIDRef, const PMString& fromPath)
{
	UIDRef retval = UIDRef::gNull;
	do
	{
		InterfacePtr<IDocument> 
		iDocument(docUIDRef, UseDefaultIID());
		ASSERT(iDocument);
		if(!iDocument)
		{
			break;
		}
		
		IDataBase* db = ::GetDataBase(iDocument); 
		
		IDFile idFile;
		FileUtils::PMStringToIDFile(fromPath,idFile);
		InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
		ASSERT(importCmd);
		if (!importCmd) {
			break;
		}
		
		InterfacePtr<IImportResourceCmdData> data(importCmd, IID_IIMPORTRESOURCECMDDATA);
		if (data == nil)
			break;
		
		IDFile myfile(fromPath);
		URI fileURI;
		Utils<IURIUtils>()->IDFileToURI(myfile, fileURI);
		
		data->Set(db, fileURI, K2::kSuppressUI); 
		/* //CS3
		 InterfacePtr<IImportFileCmdData> importCmdData(importCmd, IID_IIMPORTFILECMDDATA);
		 ASSERT(importCmd);
		 if (!importCmd) {
		 break;
		 }
		 importCmdData->Set(db, idFile, kSuppressUI);*/
		CmdUtils::ProcessCommand(importCmd);
		//the UIDRef of the imported item
		retval = importCmd->GetItemListReference().GetRef(0); 
	}while(false);
	// Precondition: active document
	// Precondition: file exists
	// 
	
	return retval;
}


void N2PSQLUtilities::CreateAndProcessPlaceItemInGraphicFrameCmd(const UIDRef& docUIDRef,const UIDRef& itemToPlace, const UIDRef& graphicFrame)
{
	do
	{
		// Create a PlaceItemInGraphicFrameCmd:
		InterfacePtr<ICommand>
		placeCmd(CmdUtils::CreateCommand(kPlaceItemInGraphicFrameCmdBoss));
		// Set the PlaceItemInGraphicFrameCmd's ItemList:
		placeCmd->SetItemList(UIDList(itemToPlace));
		// Get an IPlacePIData Interface for the PlaceItemInGraphicFrameCmd:
		InterfacePtr<IPlacePIData> placeData(placeCmd, IID_IPLACEPIDATA);
		// Set the IPlacePIData Interface's data (don't use PlaceGun in thisexample):
		placeData->Set(graphicFrame, nil, kTrue);
		// Process the PlaceItemInGraphicFrameCmd:
		if (CmdUtils::ProcessCommand(placeCmd) != kSuccess)
		{
			CAlert::ErrorAlert("Error al intentar importar la imagen");
		}
		else
		{
			InterfacePtr<ICommand> clearPlaceGun(CmdUtils::CreateCommand(kClearPlaceGunCmdBoss));
			ASSERT(clearPlaceGun);
			InterfacePtr<IUIDData> docUIDData(clearPlaceGun, UseDefaultIID());
            ASSERT(docUIDData);
			if(!docUIDData) {
				break;
			}
			docUIDData->Set(docUIDRef);
			
			if(CmdUtils::ProcessCommand(clearPlaceGun)!=kSuccess)
			{
             	break;
			}
		}
	}while(false);
}
/*****************************/

/* QueryFont
 */
IPMFont* N2PSQLUtilities::QueryFont(const InDesign::TextRange& textRange, 
								   PMString& fontFamilyName,
								   PMString& fontStyleName)
{
	InterfacePtr<IPMFont> font(nil);
	do {
		
		// get info from text range
		RangeData range = textRange.GetRange();
		InterfacePtr<ITextModel> textModel(textRange.QueryModel());
		if (textModel == nil) 
		{
			ASSERT(textModel); break;
		}
		
		
		// query the compose scanner so we can query attributes
		InterfacePtr<IComposeScanner> composeScanner(textModel, UseDefaultIID());
		if (composeScanner == nil) 
		{
			ASSERT(composeScanner); break;
		}
		
		// get the font family UID
		InterfacePtr<const IAttrReport> fontUIDAttrReport(composeScanner->QueryAttributeAt(range, kTextAttrFontUIDBoss));
		if (fontUIDAttrReport == nil)
		{
			ASSERT(fontUIDAttrReport); break;
		}
		InterfacePtr<ITextAttrUID> fontUIDAttr(fontUIDAttrReport, UseDefaultIID());
		if (fontUIDAttr == nil)
		{
			ASSERT_MSG(fontUIDAttr, "Missing ITextAttrUID in QueryFont()");
			break;
		}
		UID fontUID = fontUIDAttr->Get();
		ASSERT_MSG(fontUID != kInvalidUID, "fontUID is invalid");
		
		// get the font style name
		InterfacePtr<const IAttrReport> fontStyleAttrReport(composeScanner->QueryAttributeAt(range, kTextAttrFontStyleBoss));
		if (fontStyleAttrReport == nil)
		{
			ASSERT(fontStyleAttrReport); break;
		}
		InterfacePtr<ITextAttrFont> fontStyleAttr(fontStyleAttrReport, UseDefaultIID());
		if (fontStyleAttr == nil)
		{
			ASSERT_MSG(fontStyleAttr, "Missing ITextAttrFont in QueryFont()");
			break;
		}
		
		// get the font family
        InterfacePtr<IFontFamily> fontFamily(::GetDataBase(textModel), fontUID, UseDefaultIID());
		if (fontFamily == nil)
		{
			ASSERT(fontFamily); break;
		}
		
		fontStyleName = fontStyleAttr->GetFontName();
		if (fontStyleName.empty()) 
		{
			// Font style name is empty.
			// You fall into this case if you haven't set the font face on the selected text.
			// Suggestion - set the font face on the selected text.
			IDrawingStyle* drawingStyle = nil;
			TextIterator textIter = composeScanner->QueryDataAt(range.Start(nil), &drawingStyle, nil);
			if (textIter.IsNull() == kFalse && 
				drawingStyle) 
			{
				// get the font
				font.reset(drawingStyle->QueryFont());
				// release drawingStyle - it was addref'ed in QueryDataAt()
				drawingStyle->Release();
				
				// get the font family name 
				fontFamilyName.Clear();
				font->AppendFamilyName(fontFamilyName);
				
				// get the font style name 
				fontStyleName.Clear();
				font->AppendStyleName(fontStyleName);
			}
		}
		else
		{
			// get the font
			font.reset(fontFamily->QueryFace(fontStyleName));
			
			// got the font family name
			fontFamilyName = fontFamily->GetFamilyName();
			ASSERT_MSG(fontFamilyName.empty() == kFalse, "Font family name is empty");
		}
		ASSERT_MSG(font, "Could not query the font");
		
		
	} while (false);
	return font.forget();
}


/*
 SNIPLOG("%s", choices[choice].GetPlatformString().c_str());
 InterfacePtr<ITextModel> textModel(textTarget->QueryTextModel());
 RangeData rangeData = textTarget->GetRange();
 TextIndex start = rangeData.Start(nil);
 TextIndex end = rangeData.End();
 if (rangeData.Length() == 0) {
 start = 0;
 end = textModel->TotalLength();
 }
 InDesign::TextRange range(textModel, start, end - start);
 
 
 // Run the requested report.
 SnpInspectTextModel instance(range);
 */

/*
 */
ErrorCode N2PSQLUtilities::FindParaStyleOnStory(const InDesign::TextRange& fTextRange, const PMString &nameStyToFind,
												  TextIndex &position,int32 &length, const bool16& equalNameParaStyle)
{
	ErrorCode status = kFailure;
	do
	{
		// Acquire the text model
		InterfacePtr<ITextModel> textModel(fTextRange.QueryModel());
		if (textModel == nil) {
			break;
		}
		
		// Navigate to the paragraph attribute strand for this story.
		InterfacePtr<IAttributeStrand> attributeStrand
		(
		 (IAttributeStrand*)textModel->QueryStrand
		 (
		  kParaAttrStrandBoss, 
		  IAttributeStrand::kDefaultIID
		  )
		 );		
		if (attributeStrand == nil)
		{
			break;
		}
		
		// Get the UID of the root paragraph style from the document's 
		// paragraph style table. This let's us report if the root style
		// or some other style applies.
		IDataBase* db = ::GetDataBase(textModel);
		InterfacePtr<IDocument> document(db, db->GetRootUID(), UseDefaultIID());
		if (document == nil) 
		{
			break;
		}
		InterfacePtr<IStyleGroupManager> paraStyleNameTable(document->GetDocWorkSpace(), IID_IPARASTYLEGROUPMANAGER);
		if (paraStyleNameTable == nil)
		{
			break;
		}
		UID rootParaStyleUID = paraStyleNameTable->GetRootStyleUID();
		
		// Initialise loop control variables.
		/*TextIndex*/ position = fTextRange.Start();
		/*int32*/ length = 0;
		int32 numberOfRuns = 0;
		IDataBase* database = ::GetDataBase(textModel);
		
		// Loop round reporting runs on the paragraph attribute strand
		// and count up the total number of runs.
		//SNIPLOG(" position, length, styleName, rootStyle");
		while (position < fTextRange.End())
		{
			// The length returned by GetStyleUID indicates the boundary
			// of the run to which the style applies.
			UID styleUID = attributeStrand->GetStyleUID(position,&length);
			
			// Get the name of the style.
			InterfacePtr<IStyleInfo> styleInfo(database, styleUID, UseDefaultIID());
			if (styleInfo == nil)
			{
				break;
			}
			const PMString& styleName = styleInfo->GetName();
			
			// Check for root style.
			PMString rootStyle("No");
			if (styleUID == rootParaStyleUID) {
				rootStyle = "Yes";
			}
			
			/*PMString banas;
			banas.Append("posicion= ");
			banas.AppendNumber(position);
			banas.Append("\n tamaño=");
			banas.AppendNumber(length);
			banas.Append("\n Nombre de Estilo: "+styleName);
			banas.Append("\n Nombre de Root: "+nameStyToFind);
			
			CAlert::InformationAlert(banas);*/
			/*SNIPLOG(" %d, %d, %s, %s", 
					position, 
					length,
					styleName.GetPlatformString().c_str(),
					rootStyle.GetPlatformString().c_str()
					);*/
			
			//CAlert::InformationAlert("!"+nameStyToFind+"=="+styleName+"!");
			if(equalNameParaStyle)
			{
				if(nameStyToFind==styleName)
				{
					status = kSuccess;
					return status;
					break;
				}
			}
			else {
				
				PMString sname1=styleName;
				sname1.ToLower();
				PMString sname2=nameStyToFind;
				sname2.ToLower();
				if( sname1.Contains(sname2))
				{
					status = kSuccess;
					return status;
					break;
				}
			}

			
			position += length;
			numberOfRuns++;
		}
		//PMString ailert="";
		//ailert="number of runs=";
		//ailert.AppendNumber( numberOfRuns);
		//CAlert::InformationAlert(ailert);
		//SNIPLOG("number of runs=%d", numberOfRuns);
		
		
	} while (false);
	return status;
} 

/*
 
ErrorCode SnpInspectTextModel::ReportParagraphs()
{
	ErrorCode status = kFailure;
	do {
		// Acquire the text model for the story from the text focus:
		InterfacePtr<ITextModel> textModel(fTextRange.QueryModel());
		if (textModel == nil) {
			break;
		}
		
		// Navigate to the paragraph strand for this story:
		InterfacePtr<IStrand> strand
		(
		 (IStrand*)textModel->QueryStrand
		 (
		  kParaAttrStrandBoss, 
		  IStrand::kDefaultIID
		  )
		 );		
		if (strand == nil) {
			break;
		}
		
		// Initialise loop control variables
		TextIndex position = fTextRange.Start();
		int32 length = fTextRange.Length();
		int32 numberOfParagraphs = 0;
		
		// Loop round reporting runs on the strand & totting up the number of paragraphs
		SNIPLOG(" position, length, characters");
		while (position < fTextRange.End())
		{
			// The length returned by GetRunLength indicates the boundary
			// of the paragraph.
			length = strand->GetRunLength(position);
			
			// Build a string displaying the first and last
			// characters in the paragraph. Show kTextChar_CR 
			// as "<CR>".
			TextIterator firstChar(textModel, position);
			TextIterator lastChar(textModel, position + length - 1);
			ASSERT(*lastChar==kTextChar_CR);
			PMString paraText;
			paraText.AppendW(*firstChar);
			if (length > 2)
			{
				lastChar--;
				paraText += "...";
				paraText.AppendW(*lastChar);
			}
			paraText += "<CR>";
			
			// Report the details of this run.
			SNIPLOG(" %d, %d, %s", position, length, paraText.GetPlatformString().c_str());
			position += length;
			numberOfParagraphs++;
		}
		SNIPLOG("number of paragraphs=%d", numberOfParagraphs);
		
		status = kSuccess;
	} while (false);
	return status;
}*/


bool16 N2PSQLUtilities::LlenarComboGuiasNotasSobrePaletaN2P(PMString Seccion)
{
	bool16 retval=kFalse;	
	do
	{
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kN2PsqlPanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}
		
		IControlView *ControlView = panel->FindWidget(kN2PsqlComboguiaWidgetID);
		if(ControlView==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos ComboCView");
			break;
		}
		
		
		
		InterfacePtr<IStringListControlData> dropListDataDirigidoa(ControlView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListDataDirigidoa == nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos dropListDataDirigidoa");
			break;
		}
		dropListDataDirigidoa->Clear(kTrue);
		
		if(Seccion.NumUTF16TextChars()<=0)
			Seccion = N2PSQLUtilities::GetXMPVar("N2PSeccion", document);
		PMString Busqueda="SELECT SQL_CACHE a.id, a.nombre FROM dguias a INNER JOIN mguias b ON a.id_mguias=b.id WHERE b.id_seccion="+Seccion+"  ORDER by a.nombre";
		///borrado de la lista al inicializar el combo
		
		
		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ControlView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogController::LLenadoDeCombos IDDLDrComboBoxSelecPrefer");
			break;
		}
		
		
		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
																			(
																			 kN2PSQLUtilsBoss,	// Object boss/class
																			 IN2PSQLUtils::kDefaultIID
																			 )));
		ASSERT(SQLInterface);
		if (SQLInterface == nil) {	
			break;
		}
		
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		
		//llenado del combo Dirigido A
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("nombre",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListDataDirigidoa->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}
		
		cadena = "";
		cadena.SetTranslatable(kFalse);
		dropListDataDirigidoa->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
	}while(false);
	return(retval);
}




/*
 Metodo que devuelve solo los rectangulos de imagenes que se encuentran seleccionados
 */
UIDList N2PSQLUtilities::GetUIDListOfGraphicFramesSelected(UIDRef &RefOfTextFrame)
{
	
	UIDList itemList;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionUtils pointer nil");
			break;
		}
		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionManager pointer nil");
			break;
		}
		
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			break;
		}
		
		UIDRef selectedItemRef = UIDRef::gNull;
		UID selectedItem = kInvalidUID;
		InterfacePtr<ILayoutTarget> pLayoutTarget(pTextSel, UseDefaultIID());    
		if (pLayoutTarget)
		{
			itemList = pLayoutTarget->GetUIDList(kStripStandoffs);
			if (itemList.Length() >= 2) //Se deben tener al menos dos frames seleccionados un grafic frame y el de la nota
			{
				
				for(int32 i=0;i<itemList.Length();i++)
				{
					// get the database
					db = itemList.GetDataBase();
					// get the UID of the selected item
					selectedItem = itemList.At(i);
					
					selectedItemRef=UIDRef(db,selectedItem);
					
					if(Utils<IPageItemTypeUtils>()->IsTextFrame(selectedItemRef))
					{
						RefOfTextFrame=selectedItemRef;
						//CAlert::InformationAlert("Removio por que no es graphicFrame");
						itemList.Remove(i);
						i=-1;
					}
					else
					{
						InterfacePtr<IGraphicFrameData> graphicFrame(selectedItemRef, UseDefaultIID());
						ASSERT(graphicFrame);
						if (!graphicFrame)
						{
							if(RefOfTextFrame==UIDRef::gNull)
							{
								if(Utils<IPageItemTypeUtils>()->IsTextFrame(selectedItemRef))
								{
									RefOfTextFrame=selectedItemRef;
								}
							}
							
							
							//CAlert::InformationAlert("Removio por que no es graphicFrame");
							itemList.Remove(i);
							i=-1;
							
						}
						else
						{
							InterfacePtr<IGeometry> myGeometry(selectedItemRef, UseDefaultIID());
							if(myGeometry==nil)
							{
								//CAlert::InformationAlert("Removio por que no es myGeometry");
								itemList.Remove(i);
								i=-1;
							}
							//else
							//CAlert::InformationAlert("ESTA OK");
						}
					}
					
					
					
				}
				
			}
			else
			{
				CAlert::InformationAlert("Debes seleccionar dos frames");
			}
		}
		else
		{
			CAlert::InformationAlert("No existen frame selecionados");
		}	
	}while(false);
	return(itemList);
}



/*
 Metodo que devuelve solo los rectangulos de Texto que se encuentran seleccionados
 */
UIDList N2PSQLUtilities::GetUIDListOfTextFramesSelected(UIDRef &RefOfTextFrame)
{
	
	UIDList itemList;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect document pointer nil");
			break;
		}
		
		UIDRef ur(GetUIDRef(document));
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect db pointer nil");
			break;
		}
		
		Utils<ISelectionUtils> iSelectionUtils;
		if (iSelectionUtils == nil) 
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionUtils pointer nil");
			break;
		}
		
		ISelectionManager* iSelectionManager = iSelectionUtils->GetActiveSelection();
		if(iSelectionManager==nil)
		{
			ASSERT_FAIL(" N2PSQLUtilities::GetUIDOfTextFrameSelect iSelectionManager pointer nil");
			break;
		}
		
		InterfacePtr<IConcreteSelection> pTextSel(iSelectionManager->QueryConcreteSelectionBoss(kNewLayoutSelectionBoss)); // deprecated but universal (CS/2.0.2) 
		if(pTextSel==nil)
		{
			break;
		}
		
		UIDRef selectedItemRef = UIDRef::gNull;
		UID selectedItem = kInvalidUID;
		InterfacePtr<ILayoutTarget> pLayoutTarget(pTextSel, UseDefaultIID());    
		if (pLayoutTarget)
		{
			itemList = pLayoutTarget->GetUIDList(kStripStandoffs);
								
			for(int32 i=0;i<itemList.Length();i++)
			{
				// get the database
				db = itemList.GetDataBase();
				// get the UID of the selected item
				selectedItem = itemList.At(i);
					
				selectedItemRef=UIDRef(db,selectedItem);
					
				PMString pageItemType="";
				InterlasaUtilities::GetPageItemType(selectedItemRef, pageItemType);
					
				if( pageItemType!= "TextFrame")
				{
					RefOfTextFrame=selectedItemRef;
					itemList.Remove(i);
					i=-1;
				}
			}
		}
		else
		{
			CAlert::InformationAlert("No existen frame selecionados");
		}	
	}while(false);
	return(itemList);
}


bool16 N2PSQLUtilities::IniciaDetieneObserver(WidgetID PanelWidgetID, WidgetID widget,bool16 startObserber)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PanelWidgetID)) ;
		if(panel==nil)
		{
			break;
		}
		
		IControlView *iControlView = panel->FindWidget(widget);
		InterfacePtr<ActiveSelectionObserver> activeSelectionObserver(iControlView, UseDefaultIID());
		
		if(startObserber)
			activeSelectionObserver->AutoAttach();
		else {
			activeSelectionObserver->AutoDetach();
		}

		/*InterfacePtr<ITextControlData> TextControl(iControlView,ITextControlData::kDefaultIID);
		if (TextControl == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		TextControl->SetString(cadena);
		 */
		retval=kTrue;
	}while(false);
	return(kTrue);
}
