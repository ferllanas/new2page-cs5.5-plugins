//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlink/ExtLinkErrorStringService.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
// none.

// General includes:
#include "CErrorStringService.h"

// Project includes:
#include "ExtLinkID.h"

/** Provides error string service for ExtLink that 
	maps this plug-in's ErrorCode's to error strings defined in the 
	UserErrorTable ODFRez statement.

	@ingroup extendedlink
*/
class ExtLinkErrorStringService : public CErrorStringService
{
public:
	/** Constructor 
	 * 	This is where we specify the pluginID and the resourceID for the 
	 * 	UserErrorTable resource that is associated with this implementation.
	 */
	ExtLinkErrorStringService(IPMUnknown* boss):
		CErrorStringService(boss, kExtLinkPluginID, kSDKDefErrorStringResourceID) {}

	/** Destructor 
	 */
	virtual ~ExtLinkErrorStringService(void) {}
};

/* Make the implementation available to the application.
*/
CREATE_PMINTERFACE(ExtLinkErrorStringService, kExtLinkErrorStringServiceImpl)

// End, ExtLinkErrorStringService.cpp.
