/*
//	File:	CheckInOutAsignaIssueController.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:

#include "IApplication.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"
#include "N2PCheckInOutListBoxHelper.h"
#include "IDocument.h"
#include "ILayoutUtils.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "ImportExportUIID.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IOpenManager.h"
#include "ITriStateData.h"
#include "IInCopyDocUtils.h"

#include "ILayoutUtils.h"
#include "ILayoutUIUtils.h"
#include "IInCopyStoryList.h"

#include "Utils.h"
#include "FileUtils.h"


#include "ErrorUtils.h"
#include "ILayoutUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"
//#include "DocumentID.h"
#include "OpenPlaceID.h"


// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "CAlert.h"
#include "SDKFileHelper.h"
#include "SDKUtilities.h"
#include "SDKLayoutHelper.h"

#include "PlatformFileSystemIterator.h"

//#include <direct.h>
#include <stdlib.h>
#include <stdio.h>




// Project includes
#include "N2PCheckInOutID.h"
#include "ICheckInOutSuite.h"
#ifdef MACINTOSH
	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif

//#include "A2PPrefID.h"
//#include "N2PsqlID.h"


////////////////


//#include <winreg.h>    //If you are using MS Visual C++ you do not need this
//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")
/** CheckInOutAsignaIssueController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class CheckInOutAsignaIssueController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CheckInOutAsignaIssueController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CheckInOutAsignaIssueController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);
		
		
		K2Vector<PMString> VectorIdSeccion;
		K2Vector<PMString> VectorIdPublicacion;
	private:


		




		

		
		void SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget);
		
		bool16 LLenar_Combo_Publicacion(PMString Id_PublicacionText);
		
		bool16 LLenar_Combo_Seccion(PMString Id_SeccionText);	
		
		bool16 LlenarListaDeIssueDePagina();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(CheckInOutAsignaIssueController, kN2PCheckInOutAsignaIssueControllerImpl)

/* ApplyDialogFields
*/
void CheckInOutAsignaIssueController::InitializeDialogFields(IActiveContext* context) 
{
	////Inicializa Fields CheckIn
	SetTextControlData(kDatePubliWidgetID,"");
	SetTextControlData(kPaginaWidgetID,"");
	
	
	
	
	ClassDialogCheckOutPaginaSets PreferencesPara;
		
		
	ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
	(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
	if(checkIn!=nil)
	{
			
		checkIn->ObtenerDatosDeCheckOutDPaginas(PreferencesPara);
	}
	

		
		//Selecciona la seccion Anterior
		
		this->LLenar_Combo_Publicacion(PreferencesPara.Issue_PageOut);		
		
		this->LlenarListaDeIssueDePagina();
		//Selecciona la seccion Anterior
		
		this->LLenar_Combo_Seccion(PreferencesPara.Seccion_PageOut);	
		
		// Put code to initialize widget values here.
}

/* ValidateDialogFields
*/
WidgetID CheckInOutAsignaIssueController::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID result = kNoInvalidWidgets;
	// Put code to validate widget values here.
	do
	{
			
	}while(false);
	return result;
}

/* ApplyDialogFields
*/
void CheckInOutAsignaIssueController::ApplyDialogFields(IActiveContext* context, const WidgetID& widgetId) 
{
	// Replace with code that gathers widget values and applies them.
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutAsignaIssueController::ApplyDialogFields ini");
	do{
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("CheckInOutAsignaIssueController::ApplyDialogFields fin");
	SystemBeep();  
}
//  Generated by Dolly build 17: template "Dialog".
// End, CheckInOutAsignaIssueController.cpp.








bool16 CheckInOutAsignaIssueController::LLenar_Combo_Seccion(PMString Id_SeccionText)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion=(SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + GetTextControlData(kComboBoxIssueWidgetID) + "' ) ORDER BY Nombre_de_Seccion ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_SeccionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//Selecciona el item deseado o la seccion deseada

		//CAlert::InformationAlert(Id_SeccionText);
		int32 Index=dropListData->GetIndex(Id_SeccionText);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
			
	}while(false);
	return(kTrue);
}


bool16 CheckInOutAsignaIssueController::LLenar_Combo_Publicacion(PMString Id_PublicacionText)
{
	//kComboBoxIssueWidgetID
	
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Publicacion, Nombre_Publicacion From Publicacion";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia la lista de la consulta
		VectorIdPublicacion = QueryVector;
		
		//Variable para indicar que Publicacion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			if(Id_PublicacionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//CAlert::InformationAlert(Id_PublicacionText);
		int32 Index=dropListData->GetIndex(Id_PublicacionText);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}

		
			
	}while(false);
	return(kTrue);
}


bool16 CheckInOutAsignaIssueController::LlenarListaDeIssueDePagina()
{
	do{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		PMString IDPaginaENXMP = N2PSQLUtilities::GetXMPVar("N2PSQLIDPagina", document);
		
		InterfacePtr<IPanelControlData> panel(this, UseDefaultIID());
		if (panel == nil)
		{
			
			break;
		}		
		
		
		IControlView *iControViewList = panel->FindWidget(kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		if(iControViewList==nil)
		{
			CAlert::InformationAlert("ControlView");
			break;
		}
		
		
		
		
		SDKListBoxHelper listHelper(iControViewList, kN2PCheckInOutPluginID,kN2PCheckInOutIssueDPaginasListBoxWidgetID);
		listHelper.EmptyCurrentListBox();	
		
		
		PMString Busqueda="SELECT a.id as id, p.Nombre_Publicacion as publicacion, s.Nombre_de_Seccion as seccion ";
		Busqueda.Append(	  "	FROM edicionesdpagina a LEFT JOIN publicacion p ON p.Id_Publicacion=a.publicacion_idpublicacion");
		Busqueda.Append(	  " LEFT JOIN seccion s ON s.Id_Seccion=a.seccion_id_seccion WHERE pagina_id="+IDPaginaENXMP+ " AND a.estatus<90 ");
		
		//CAlert::InformationAlert(Busqueda);
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
																								  (
																								   kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
																								   IUpdateStorysAndDBUtils::kDefaultIID
																								   )));
		
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		
		K2Vector<PMString> QueryVector;
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject( kN2PSQLUtilsBoss,	IN2PSQLUtils::kDefaultIID )));
		if(SQLInterface==nil)
		{
			ASSERT_FAIL("SQLInterface = nil");
			break;
		}
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		if(QueryVector.Length()<=0)
		{
			PMString NoNotas=PMString(kN2PCheckInOutNoSeEncontraronPaginasPlugInMenuKey);
			NoNotas.Translate();
			NoNotas.SetTranslatable(kTrue);
			CAlert::InformationAlert(NoNotas);
			break;
		}
		
		// cuando regresa solo un registro
		if(QueryVector.Length()==1)
		{
			
			//CAlert::InformationAlert(QueryVector[0]);
			PMString Privilegio = SQLInterface->ReturnItemContentbyNameColumn("Privilegio",QueryVector[0]);
			if(Privilegio.NumUTF16TextChars()>0)//Pregunta el rtegitro trae el campo con el nombre 'Privilegio'
			{
				//entonces no tiene privilegios para ver las paginas de esta seccion
				CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
			}
			else
			{// si no entonces es una regirtro con datos de una pagina
				for(int32 i=0;i<QueryVector.Length();i++)
				{
					PMString Nombre_Archivo = SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryVector[i]);
					PMString Secc = SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryVector[i]);
					PMString Estado="";// SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
					PMString Folio_Pagina = SQLInterface->ReturnItemContentbyNameColumn("id",QueryVector[i]);
					PMString Id_SeccionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
					PMString Id_PublicacionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
					listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
				}
			}
			
			//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogObserver::Update call(despues del llenado de lista de paginas)");
			
		}
		else
		{//si no entonces trae muchas registeros de paginas.
			for(int32 i=0;i<QueryVector.Length();i++)
			{
				PMString Nombre_Archivo = SQLInterface->ReturnItemContentbyNameColumn("publicacion",QueryVector[i]);
				PMString Secc = SQLInterface->ReturnItemContentbyNameColumn("seccion",QueryVector[i]);
				PMString Estado="";// SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
				PMString Folio_Pagina = SQLInterface->ReturnItemContentbyNameColumn("id",QueryVector[i]);
				PMString Id_SeccionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]);
				PMString Id_PublicacionText ="";// SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]);
				listHelper.AddElementListPag(Nombre_Archivo,Secc,Estado,Folio_Pagina,Id_PublicacionText,Id_SeccionText,kPaginaLabelWidgetID,0);
			}
			
			
		}
		
	}while (false);
	return kTrue;
}
