/*
//	File:	CheckOutDialogController.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:

#include "IApplication.h"
#include "IPanelControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "ISelectableDialogSwitcher.h"
#include "N2PCheckInOutListBoxHelper.h"
#include "IDocument.h"
#include "ILayoutUtils.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IOpenFileCmdData.h"
#include "ICommand.h"
#include "ILayoutControlData.h"
#include "IHierarchy.h"
#include "IDataBase.h"
#include "IPMStream.h"
#include "IWindow.h"
#include "ICloseWinCmdData.h"
#include "ImportExportUIID.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IOpenManager.h"
#include "ITriStateData.h"
#include "IInCopyDocUtils.h"

#include "ILayoutUtils.h"
#include "ILayoutUIUtils.h"
#include "IInCopyStoryList.h"

#include "Utils.h"
#include "FileUtils.h"


#include "ErrorUtils.h"
#include "ILayoutUtils.h"
#include "IWindowUtils.h"
#include "StreamUtil.h"
#include "SnapshotUtils.h"
//#include "DocumentID.h"
#include "OpenPlaceID.h"


// none.

// General includes:
#include "CDialogController.h"
#include "SystemUtils.h"
#include "CmdUtils.h"
#include "CAlert.h"
#include "SDKFileHelper.h"
#include "SDKUtilities.h"
#include "SDKLayoutHelper.h"

#include "PlatformFileSystemIterator.h"

//#include <direct.h>
#include <stdlib.h>
#include <stdio.h>




// Project includes
#include "N2PCheckInOutID.h"
#include "ICheckInOutSuite.h"
#ifdef MACINTOSH
	#include "InterlasaUtilities.h"

	#include "N2PInOutID.h"
	#include "IN2PSQLUtils.h"
	#include "IRegisterUsersUtils.h"
	#include "N2PRegisterUsers.h"

	#include "N2PsqlID.h"
	#include "UpdateStorysAndDBUtilis.h"
	#include "N2PSQLUtilities.h"

	#include "IN2PCTUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"

	#include "..\N2PLogInOut\N2PInOutID.h"
	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"

	#include "..\N2PSQL\N2PsqlID.h"
	#include "..\N2PSQL\UpdateStorysAndDBUtilis.h"
	#include "..\N2PSQL\N2PSQLUtilities.h"

	#include "..\N2PFrameOverset\IN2PCTUtilities.h"
#endif

//#include "A2PPrefID.h"
//#include "N2PsqlID.h"


////////////////


//#include <winreg.h>    //If you are using MS Visual C++ you do not need this
//#import "c:\Archivos de programa\Archivos comunes\system\ado\msado15.dll" no_namespace rename("EOF","adoEOF")
/** CheckOutDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Juan Fernando Llanas Rdz
*/
class CheckOutDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CheckOutDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CheckOutDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext* context);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext* context);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext* context,const WidgetID& widgetId);
		
		
		K2Vector<PMString> VectorIdSeccion;
		K2Vector<PMString> VectorIdPublicacion;
	private:


		


		PMString  Aplicar_Usuario_Actual();

		PMString BuscarEnUsuarios(PMString &Busqueda,PMString Campo);

		void Limpiar_Lista_Paginas(PMString ID_Usuario);

		bool16 CreateAndProcessOpenDocCmd(const IDFile& targetFile);


		

		void Llenar_ComboUsuarios();
		
		void SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget);
		
		bool16 LLenar_Combo_Publicacion(PMString Id_PublicacionText);
		
		bool16 LLenar_Combo_Seccion(PMString Id_SeccionText);	
		
		int32 GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem);
		
		bool16 LLenar_Combo_EstatusNota(PMString EstatusNota);
		
		bool16 ObtenerFolioDePaginaSeleccionada(PMString& Id_PaginaSel, PMString& Seccion);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(CheckOutDialogController, kCheckOutDialogControllerImpl)

/* ApplyDialogFields
*/
void CheckOutDialogController::InitializeDialogFields(IActiveContext* context) 
{
	////Inicializa Fields CheckIn
	SetTextControlData(kDatePubliWidgetID,"");
	SetTextControlData(kPaginaWidgetID,"");
	PMString Usuario=this->Aplicar_Usuario_Actual();
	this->Limpiar_Lista_Paginas(Usuario);
	this->Llenar_ComboUsuarios();
	
	
	
	
	ClassDialogCheckOutPaginaSets PreferencesPara;
		
		
	ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
	(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
	if(checkIn!=nil)
	{
			
		checkIn->ObtenerDatosDeCheckOutDPaginas(PreferencesPara);
	}
	
		Usuario.SetTranslatable(kFalse);
		this->SeleccionarCadenaEnComboBox(Usuario, kComboBoxUsuarioWidgetID);

		
			
		//Selecciona el Estado Anterior		
		this->LLenar_Combo_EstatusNota(PreferencesPara.Status_PageOut);
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Estado_To_SavePage, kComboBoxStatusWidgetID);
	
		//Selecciona la seccion Anterior
		
		this->LLenar_Combo_Publicacion(PreferencesPara.Issue_PageOut);		
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Issue_To_SavePage, kComboBoxIssueWidgetID);
		
		
		//Selecciona la seccion Anterior
		
		this->LLenar_Combo_Seccion(PreferencesPara.Seccion_PageOut);	
		//this->SeleccionarCadenaEnComboBox(PreferencesPara.Seccion_To_SavePage, kComboBoxSeccionWidgetID);
		
		
		
	PMString Date = InterlasaUtilities::FormatFechaYHoraManana("%m/%d/%Y");
	
	PreferencesPara.Date_PageOut.SetTranslatable(kFalse);
	if(PreferencesPara.Date_PageOut.NumUTF16TextChars()<=0)
		SetTextControlData(kDatePubliWidgetID , Date);
	else
	
		if(PreferencesPara.Date_PageOut=="(null)")
			SetTextControlData(kDatePubliWidgetID , Date);
		else
			SetTextControlData(kDatePubliWidgetID , PreferencesPara.Date_PageOut);
	
	
	if(PreferencesPara.Pagina_PageOut=="(null)")
		PreferencesPara.Pagina_PageOut.SetTranslatable(kFalse);
	else
		SetTextControlData(kPaginaWidgetID , PreferencesPara.Pagina_PageOut);
	// Put code to initialize widget values here.
}

/* ValidateDialogFields
*/
WidgetID CheckOutDialogController::ValidateDialogFields(IActiveContext* context) 
{
	WidgetID result = kNoInvalidWidgets;
	// Put code to validate widget values here.
	do
	{
		PMString EditarPriv="";
		PMString RetirarPriv="";
		PMString Id_PaginaSel="";
		PMString N2PSeccion="";
		PMString Pag_Dirigido_A="";
		PMString Consulta="";
		PMString Usuario="";
		
		//Obtiene el nombre de la aplicacion
				InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
				if(app==nil)
				{
					break;
				}
		
				PMString Aplicacion=app->GetApplicationName();
				Aplicacion.SetTranslatable(kFalse);

		//Obtiene el nombre del usuario logeado actualmenete
				InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
				if (wrkSpcPrefs == nil)
				{	
					ASSERT_FAIL("Invalid workspace prefs in CheckInOutDialogController::ApplyDialogFields()");
					break;
				}
		
				Usuario=wrkSpcPrefs->GetIDUserLogedString();
				Usuario.SetTranslatable(kFalse);
		
		//
				if(this->ObtenerFolioDePaginaSeleccionada( Id_PaginaSel, N2PSeccion)==kFalse)
				{
					ASSERT_FAIL("Error tiene elelemntos en la lista seleccionados");
					result=kPaginasListBoxWidgetID;
					break;
				}
	
		
	
		
		
		
		//Obtiene Preferencias para conexion 
				InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
				(
					kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
					IUpdateStorysAndDBUtils::kDefaultIID
				)));
				
				if(UpdateStorys==nil)
				{
					ASSERT_FAIL("Error no puede hacer conexion");
					result=kPaginasListBoxWidgetID;
					break;
				}
		
				PreferencesConnection PrefConections;
				if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
				{
					ASSERT_FAIL("Error no encontro preferencias de conexion");
					result=kPaginasListBoxWidgetID;
					break;
				}
		
			//Arma Cadena de coneccion DSN
				PMString StringConection="";
		
				StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
				InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
				(
					kN2PSQLUtilsBoss,	// Object boss/class
					IN2PSQLUtils::kDefaultIID
				)));
			
			
			//Arma Cadena Query para Basee de datos
				Consulta="SELECT Dirigido_a FROM Pagina WHERE ID=";
				Consulta.Append(Id_PaginaSel);
				
			
			
				K2Vector<PMString> QueryVectorUsario;
			
				
				//Realiza Query
				SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVectorUsario);
			
			
				//Obtiene el usuario a quien fue dirigido la ultima vez esta Pagina
				if(QueryVectorUsario.Length()>0)
				{
					Pag_Dirigido_A=SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVectorUsario[QueryVectorUsario.Length()-1]);
				}
//				else
//				{
//					result=kPaginasListBoxWidgetID;
//					break;
//				}
			
			
//				if(Pag_Dirigido_A.NumUTF16TextChars()<1)
//				{
//					result=kPaginasListBoxWidgetID;
					//break;
//				}
			
				//Para verificar si puede hacer check out de la pgina
				
//				if(Usuario==Pag_Dirigido_A)
//				{
//					
//					break;
//				}
//				else
//				{
				
					
				
					//Consulta="DECLARE @IDGRUPO VARCHAR(20) SET @IDGRUPO = (SELECT Id_Grupo FROM UsuarioXGrupo WHERE Id_Usuario='";
//					Consulta.Append(Usuario + "') ");
//		
//					Consulta.Append("SELECT Editar, Retirar FROM Privilegio WHERE EXISTS (SELECT Id_Seccion,Id_Publicacion, Id_Grupo FROM Grupo WHERE Id_Grupo = @IDGRUPO)");
//					Consulta.Append(" AND Id_tipo_ele = 'Pagina' AND Id_Aplicacion='");
//					if(Aplicacion.Contains("InDesign"))
//					{
//						Consulta.Append("InDesign CS2'");
//					}
//					else
//					{
//						Consulta.Append("InCopy CS2'");
//					}
//				
				
				
//					K2Vector<PMString> QueryVectorPrivilegios;
//					SQLInterface->SQLQueryDataBase(StringConection,Consulta,QueryVectorPrivilegios);
//			
//					if(QueryVectorPrivilegios.Length()>0)
//					{
///				
	//					EditarPriv=SQLInterface->ReturnItemContentbyNameColumn("Editar",QueryVectorPrivilegios[QueryVectorPrivilegios.Length()-1]);
//		/				RetirarPriv=SQLInterface->ReturnItemContentbyNameColumn("Retirar",QueryVectorPrivilegios[QueryVectorPrivilegios.Length()-1]);
				
//					}
//					else
//					{
//						PMString msgMensaje(kN2PCheckInOutNoPrivilegiosPGrupoDUsuarioStringkey);
//						msgMensaje.Translate();
 //						msgMensaje.SetTranslatable(kTrue);
	//					
		//				CAlert::InformationAlert(msgMensaje);
			//			result=kPaginasListBoxWidgetID;
				//		break;
					//}
			
//					if(RetirarPriv.GetAsNumber()>0 && EditarPriv.GetAsNumber()>0)
//					{
//						PMString msgMensaje(kN2PCheckInOutNoPrivilegiosPGrupoDUsuarioStringkey);
//						msgMensaje.Translate();
 //						msgMensaje.SetTranslatable(kTrue);
 						
//						CAlert::InformationAlert(msgMensaje);
//						result=kPaginasListBoxWidgetID;
//						break;
//					}
//					else
//					{
						//Este usuario puede retirar la Pagina pues contiene los privilegios necesarios
//						break;
//					}*/
//			}
		
	}while(false);
	return result;
}

/* ApplyDialogFields
*/
void CheckOutDialogController::ApplyDialogFields(IActiveContext* context, const WidgetID& widgetId) 
{
	// Replace with code that gathers widget values and applies them.
	//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogController::ApplyDialogFields ini");
	do
	{
		PMString Date="";
		PMString Issue="";
		PMString DirigidoA="";
		PMString Seccion="";
		PMString Id_PaginaSel="";
		
		PMString HoraIn="";
		PMString FechaIn="";
		PMString fecha="";
		PMString Aplicacion="";
		
		//Obtiene el nombre de la aplicacion actual
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		if (app == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs in CheckOutDialogController::ApplyDialogFields()");
			break;
		}
		
		Aplicacion = app->GetApplicationName();
		Aplicacion.SetTranslatable(kFalse);
		
		//
		InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
		if (wrkSpcPrefs == nil)
		{	
			ASSERT_FAIL("Invalid workspace prefs inCheckOutDialogController::ApplyDialogFields()");
			break;
		}
		
		
		if(this->ObtenerFolioDePaginaSeleccionada( Id_PaginaSel, Seccion)==kFalse)
		{
			break;
		}
		
			
		PMString Usuario=this->Aplicar_Usuario_Actual();
			
		/**ID Usuario**/
		PMString UsuarioActual=GetTextControlData(kComboBoxUsuarioWidgetID);;
		PMString Bus="SELECT Id_Empleado From Usuario Where Id_Usuario='";
		Bus.Append(UsuarioActual);
		Bus.Append("'");
		PMString IDUsuario=this->BuscarEnUsuarios(Bus,"Id_Empleado");
			
		////////////
		/**Estatus**/
		PMString Estatus=GetTextControlData(kComboBoxStatusWidgetID);

		/**Fecha y hora Out**/
			
		if(IDUsuario.NumUTF16TextChars ()<1)
		{
			IDUsuario = wrkSpcPrefs->GetIDUserLogedString();
		}
		

		PMString Busqueda="SELECT Ruta_Elemento, Nombre_Archivo, Id_Seccion, Id_Estatus, Id_Publicacion, DATE_FORMAT(Fecha_Edicion,'%m/%d/%Y') AS SoloDia, Dirigido_a  FROM Pagina WHERE ID=";
		Busqueda.Append(Id_PaginaSel);
		Busqueda.Append(" And Id_Seccion=");
		Busqueda.Append(Seccion);
		//	And Aplicacion='");
		//	Busqueda.Append(Aplicacion);
		//	Busqueda.Append("' And UltimoCheck='1'");
			
		PMString Path;
			
			
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
				
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
		(
			kN2PSQLUtilsBoss,	// Object boss/class
			IN2PSQLUtils::kDefaultIID
		)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
			
		if(QueryVector.Length()>0)
		{
			
			Estatus=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[QueryVector.Length()-1]);
				
			Path=SQLInterface->ReturnItemContentbyNameColumn("Ruta_Elemento",QueryVector[QueryVector.Length()-1]);
				
			Date=SQLInterface->ReturnItemContentbyNameColumn("SoloDia",QueryVector[QueryVector.Length()-1]);
				
			Issue=SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[QueryVector.Length()-1]);
				
			DirigidoA=SQLInterface->ReturnItemContentbyNameColumn("Dirigido_a",QueryVector[QueryVector.Length()-1]);
				
			Seccion=SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[QueryVector.Length()-1]);
				
			//Id_PaginaSel=SQLInterface->ReturnItemContentbyNameColumn("Id_PaginaSel",QueryVector[QueryVector.Length()-1]);
				
		}
			
		Path.StripWhiteSpace(PMString::kTrailingWhiteSpace);
			
		#if defined(MACINTOSH)
			Path=Path;
		#elif defined(WINDOWS)
			Path=Path;
		#endif

		//CAlert::InformationAlert(Path);
		IDFile targetFile;
		FileUtils::PMStringToIDFile(Path,targetFile);
			
		ICheckInOutSuite *checkIn=static_cast<ICheckInOutSuite*>(CreateObject
			(kCheckInOutSuiteBoss,IID_ICheckInOutSUITE));
			if(checkIn==nil)
				break;
			
			
			
		QueryVector.clear();
		Busqueda="CALL PaginaCheckOutStoreProc("+Id_PaginaSel+",'"+Usuario+"',";
		if(Aplicacion.Contains("InDesign"))
			Busqueda.Append("8");
		else
			Busqueda.Append("14");
			Busqueda.Append(",@A);");
			
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
			
		PMString Resultado="";
		if(QueryVector.Length()>0)
		{
			PMString Privilegio = SQLInterface->ReturnItemContentbyNameColumn("Privilegio",QueryVector[0]);
			if(Privilegio.NumUTF16TextChars()>0 && Privilegio=="1")//Pregunta el rtegitro trae el campo con el nombre 'Privilegio'
			{
				//entonces no tiene privilegios para ver las paginas de esta seccion
				CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
					
			}
			else
			{// si no entonces es una regirtro con datos de una pagina
				for(int32 i=0;i<QueryVector.Length();i++)
				{
					//CAlert::InformationAlert(QueryVector[i]);
					Resultado=SQLInterface->ReturnItemContentbyNameColumn("Resultado",QueryVector[i]);
					if(Resultado!="0")
					{
						//CAlert::InformationAlert("Resultado diferente a Cero");
						break;
					}
						
				}
			}
		}	
		else
		{
			//CAlert::InformationAlert("Ocurrio algun Error");
			break;
		}
			
		if(Resultado!="0")
		{
			CAlert::InformationAlert(kN2PCheckInOutUserSinPriviPBuscarPageStringKey);
			break;
		}
			
		/*------------MSSQL----------------------**/
		//K2Vector<PMString> PARAMVector;
			
		//PARAMVector.Append(Issue);
		//PARAMVector.Append(Date);
		//PARAMVector.Append(Seccion);
		//PARAMVector.Append(Id_PaginaSel);
		//PARAMVector.Append(Usuario);	
		//PARAMVector.Append(fecha);	//Fecha_Ult_Mod 
				
		//if(Aplicacion.Contains("InDesign"))
		//	PARAMVector.Append("6");	//ID_Evento
		//else
		//	PARAMVector.Append("14");	//ID_Event
		//PARAMVector.Append("Check Out InDesign");	//Descripcion
			
		//if(checkIn->StoreProcCheckOutPage(PARAMVector)==kFalse)
		// break;
		/*------------MSSQL----------------------**/
			
		//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogController::ApplyDialogFields call(CreateAndProcessOpenDocCmd)");
		if(this->CreateAndProcessOpenDocCmd(targetFile))
		{
			//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogController::ApplyDialogFields call(CreateAndProcessOpenDocCmd end)");
			//Obtierne Preferencias de CheckOut		
			PMString StatusNotaPageOut = GetTextControlData(kComboBoxStatusWidgetID);
			PMString SeccionNotaPageOut = GetTextControlData(kComboBoxSeccionWidgetID);
			PMString PaginaNotaPageOut = GetTextControlData(kPaginaWidgetID);
			PMString IssueNotaPageOut = GetTextControlData(kComboBoxIssueWidgetID);
			PMString DateNotaPageOut = GetTextControlData(kDatePubliWidgetID);
			PMString DirigidoANotaPageOut = GetTextControlData(kComboBoxUsuarioWidgetID);
			
			ClassDialogCheckOutPaginaSets PreferencesPara;
			checkIn->ObtenerDatosDeCheckOutDPaginas(PreferencesPara);
			
			PreferencesPara.Status_PageOut=		StatusNotaPageOut;
			PreferencesPara.Seccion_PageOut=	SeccionNotaPageOut;
			PreferencesPara.Pagina_PageOut=		PaginaNotaPageOut;
			PreferencesPara.Issue_PageOut=		IssueNotaPageOut;
			PreferencesPara.Date_PageOut=		DateNotaPageOut;
			PreferencesPara.DirididoA_PageOut=	DirigidoANotaPageOut;
		
			checkIn->GuardaDatosDeCheckOutDPaginas(PreferencesPara);
			/////////////////////////////////////////////
			//Recieve Update
			InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
				
				
			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				break;
			}
			UpdateStorys->ShowFrameEdgesDocument( document, kTrue);	
			UpdateStorys->ActualizaNotasDesdeBD(document, PrefConections);
				
			//Cambia el estado de edicion de la nota(lapiz)					
			if(Aplicacion.Contains("InCopy"))
			{	
				//quiere decir que en InCopy no se encontraba ninguna noda en Edicion
				//por lo tanto si se encontraba en edicion notas en InDesign apareceran con apareceran lapiz de edicion las notas.
				//por lo tanto hay que cambiar el estado de todos lo frames de las notas en InCopy
				//Pone todos los elementos en estado de no edicion
				PMString CadenaCompleta=N2PSQLUtilities::GetXMPVar("N2P_ListaUpdate", document);
				if(CadenaCompleta.NumUTF16TextChars()>0)
				{
					UIDRef ur(GetUIDRef(document));
		
					IDataBase* db = ::GetDataBase(document);
					if (db == nil)
					{
						ASSERT_FAIL("db is invalid");
						break;
					}
		
		
					int32 contUpdate = N2PSQLUtilities::GetCantDatosEnXMP(CadenaCompleta);
					ClassRegEnXMP *ListUpdate = new ClassRegEnXMP[contUpdate];
					N2PSQLUtilities::LlenaStructRegistrosDeXMP(CadenaCompleta,ListUpdate);
									
					for(int32 numElem=0;numElem<contUpdate;numElem++)
					{
							
						PMString IDFrameEleNota="";
						IDFrameEleNota.AppendNumber(ListUpdate[numElem].IDFrame);
						UpdateStorys->CambiaModoDeEscrituraDElementoNota(IDFrameEleNota,kTrue);
							
						UID UIDStory=UID(ListUpdate[numElem].IDFrame);
						UIDRef UIDRefStory=UIDRef(db,UIDStory);
							
						InterfacePtr<IInCopyStoryList> icStoryList(document,UseDefaultIID());
						if(icStoryList==nil)
							break;
		
		
						icStoryList->ProcessAddStory(UIDRefStory.GetUID());
					}
				}
				//Guarda la ruta de donde se abrio la pagina esto con el motivo de verificar la actualizacion 
				//de Documento si es que no fue renombrado por el diseñador
				InterlasaUtilities::SaveXMPVar("N2PPaginaPath",Path,document);
				//CAlert::InformationAlert("Se supone guardo el path"+Path);
			}
									
		}	
	}while(false);
	
	//N2PSQLUtilities::ImprimeMensaje("CheckOutDialogController::ApplyDialogFields fin");
	SystemBeep();  
}
//  Generated by Dolly build 17: template "Dialog".
// End, CheckOutDialogController.cpp.

/**
	Obtiene ID-Usuario del ultimo usuario que a InDesign desde el regEdit 
	para buscar en la base de datos el Nombre del usuario correspondiente al ID que se obtuvo
	y poner el nombre subre su TextEdit
*/
PMString CheckOutDialogController::Aplicar_Usuario_Actual()
{
	PMString retval="";

	InterfacePtr<IN2PRegisterUsers> wrkSpcPrefs(Utils<IRegisterUsersUtils>()->QueryCustomPrefs(GetExecutionContextSession()));
	if (wrkSpcPrefs != nil)
	{	
		retval=wrkSpcPrefs->GetIDUserLogedString();
		retval.SetTranslatable(kFalse);
		SetTextControlData(kComboBoxUsuarioWidgetID,retval);
	}
	return(retval);
}








PMString CheckOutDialogController::BuscarEnUsuarios(PMString &Busqueda,PMString Campo)
{

	PMString cadena="";

	do
	{
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
				
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Usuario",QueryVector[i]);
		}
	}while(false);
		
	
	return(cadena);
}


void CheckOutDialogController::Limpiar_Lista_Paginas(PMString Usuario)
{
	do
	{
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			ASSERT_FAIL("parent");
			break;
		}

		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
		
			ASSERT_FAIL("panel");
			break;
		}

		InterfacePtr<IControlView>		ListCView( panel->FindWidget(kPaginasListBoxWidgetID), UseDefaultIID() );
		if(ListCView==nil)
		{
			ASSERT_FAIL("No se encontro el editbox");
			break;
		}
		
		//se declara crea una lista 
		SDKListBoxHelper listHelper(ListCView, kN2PCheckInOutPluginID,kPaginasListBoxWidgetID);
		listHelper.EmptyCurrentListBox();
	/*******************************/
	}while(false);
}


bool16 CheckOutDialogController::CreateAndProcessOpenDocCmd(const IDFile& theFile)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		PMString Aplicacion=app->GetApplicationName();
				
		Aplicacion.SetTranslatable(kFalse);
				
		
			
		if(Aplicacion.Contains("InDesign"))
		{
			// Create an OpenFileWithWindowCmd:
			InterfacePtr<ICommand>	openFileCmd(CmdUtils::CreateCommand(kOpenFileWithWindowCmdBoss));
			if(openFileCmd==nil)
				break;
			// Get an IOpenFileCmdData Interface for the OpenFileWithWindowCmd:
			InterfacePtr<IOpenFileCmdData> openFileData(openFileCmd, IID_IOPENFILECMDDATA);
			if(openFileData==nil)
				break;
			// Set the IOpenFileCmdData Interface's data:
			openFileData->Set(theFile);
			// Process the OpenFileWithWindowCmd:
			if (CmdUtils::ProcessCommand(openFileCmd) != kSuccess)
			{
				retval=kFalse;
				break;
			}
			retval=kTrue;
		}
		else
		{
			Utils<IInCopyDocUtils>()->DoOpen(theFile,IOpenFileCmdData::kOpenCopy);
			
			retval=kTrue;
			/*InterfacePtr<ICommand> iOpenDocCmd(CmdUtils::CreateCommand(kTheInCopyOpenDocCmdBoss));
			if(iOpenDocCmd==nil)
			{
				break;
			}
			
			InterfacePtr<IOpenFileCmdData> iOpenFileCmdData(iOpenDocCmd, IID_IOPENFILECMDDATA);
			if(iOpenFileCmdData==nil){
				break;
			}
			
			iOpenFileCmdData->Set(theFile, kSuppressUI);
			CmdUtils::ProcessCommand(iOpenDocCmd);
			
			*/
			/*//openFileData->Set(theFile,kMinimalUI,IOpenFileCmdData::kOpenCopy,IOpenFileCmdData::kUseLockFile);
			InterfacePtr<IK2ServiceRegistry>		registry(GetExecutionContextSession(), IID_IK2SERVICEREGISTRY);
			InterfacePtr<IK2ServiceProvider>	openMgrService(registry->QueryDefaultServiceProvider(kOpenManagerService));
			InterfacePtr<IOpenManager>		openMgr(openMgrService, IID_IOPENMANAGER);
			InterfacePtr<ITriStateData> openMgrOptions(openMgr, IID_ITRISTATEDATA);		
			
			IOpenFileCmdData::OpenFlags flags = openMgrOptions->IsUnknown() ? IOpenFileCmdData::kOpenDefault : openMgrOptions->IsSelected() ?
												IOpenFileCmdData::kOpenOriginal : IOpenFileCmdData::kOpenCopy;

				ErrorCode err = kSuccess;
				
				
					//const IDFile *ithFile = filesToOpen.GetNthFile(i);

					Utils<IInCopyDocUtils>()->DoOpen(theFile, flags);

					err = ErrorUtils::PMGetGlobalErrorCode();
					
					retval=kTrue;
			*/	
		}
		
		
	}while(false);
	return(retval);
}





void CheckOutDialogController::Llenar_ComboUsuarios()
{
	do
	{


		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("PreferenciasObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxUsuarioWidgetID);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("no se encontro combo");
			break;
		}

		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		dropListData->Clear(kTrue);

		
		dropListData->Clear(kTrue);

		
		PMString Busqueda="SELECT   Id_Usuario From Usuario ORDER BY Id_Usuario ASC";
		///borrado de la lista al inicializar el combo

		PMString cadena;

		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
				
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Usuario",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}
		
		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
	}while(false);
}


void CheckOutDialogController::SeleccionarCadenaEnComboBox(PMString CadenaASeleccionar, WidgetID widget)
{
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("panelControlData");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			ASSERT_FAIL("ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			break;
		}

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL("IDDLDrComboBoxSelecPrefer");
			break;
		}

		
		int32 Index=dropListData->GetIndex(CadenaASeleccionar);
		
		if(Index < 0)
		{
			Index=dropListData->Length();
		}
		
		IDDLDrComboBoxSelecPrefer->Select(Index);
		
	}while(false);
}


bool16 CheckOutDialogController::LLenar_Combo_Seccion(PMString Id_SeccionText)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxSeccionWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion=(SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + GetTextControlData(kComboBoxIssueWidgetID) + "' ) ORDER BY Nombre_de_Seccion ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia el vetor de seccion
		VectorIdSeccion = QueryVector;
		
		//Variable para indicar que seccion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			if(Id_SeccionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Seccion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//Selecciona el item deseado o la seccion deseada

		//CAlert::InformationAlert(Id_SeccionText);
		int32 Index=dropListData->GetIndex(Id_SeccionText);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
		
			
	}while(false);
	return(kTrue);
}


bool16 CheckOutDialogController::LLenar_Combo_Publicacion(PMString Id_PublicacionText)
{
	//kComboBoxIssueWidgetID
	
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxIssueWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Publicacion, Nombre_Publicacion From Publicacion";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		//Copia la lista de la consulta
		VectorIdPublicacion = QueryVector;
		
		//Variable para indicar que Publicacion se debe seleccionar
		int32 IndexToSelect=QueryVector.Length();
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			if(Id_PublicacionText==SQLInterface->ReturnItemContentbyNameColumn("Id_Publicacion",QueryVector[i]))
			{
				IndexToSelect = i;
			}
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Publicacion",QueryVector[i]);
			cadena.SetTranslatable(kFalse);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		//CAlert::InformationAlert(Id_PublicacionText);
		int32 Index=dropListData->GetIndex(Id_PublicacionText);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}

		
			
	}while(false);
	return(kTrue);
}


bool16 CheckOutDialogController::ObtenerFolioDePaginaSeleccionada(PMString& Id_PaginaSel, PMString& Seccion)
{
	bool16 retval=kFalse;
	
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
		
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			break;
		}

		InterfacePtr<IControlView>		ListCView( panel->FindWidget(kPaginasListBoxWidgetID), UseDefaultIID() );
		if(ListCView==nil)
		{
			ASSERT_FAIL("No se encontro el editbox");
			break;
		}
			
		//se declara crea una lista 
		SDKListBoxHelper listBox(ListCView, kN2PCheckInOutPluginID,kPaginasListBoxWidgetID);
		//obtencion del controladsor de la lista
		InterfacePtr<IListBoxController> listCntl(ListCView,IID_ILISTBOXCONTROLLER);	// useDefaultIID() not defined for this interface
		ASSERT_MSG(listCntl != nil, "listCntl nil");
		if(listCntl == nil) 
		{
			break;
		}
		//  Get the first item in the selection (should be only one if the list box
		// has been set up correctly in the framework resource file)
		int32 indexSelected;
		K2Vector<int32> multipleSelection ;
		listCntl->GetSelected( multipleSelection ) ;//obtencion de la selecciones sobre la lista
		const int kSelectionLength =  multipleSelection.Length() ;//tamaÒo de la seleccion
		if (kSelectionLength> 0 )//si la cantidad de campos seleccionados es nmayor a 0
		{
			PMString dbgInfoString("Selected item(s): ");
			for(int i=0; i < multipleSelection.Length() ; i++) 
			{
				indexSelected = multipleSelection[i];
				dbgInfoString.AppendNumber(indexSelected);
				if(i < kSelectionLength - 1)
				{
					dbgInfoString += ", ";
				}
			}
			
			listBox.ObtenerTextoDeSeleccionActual(kFolio_PaginaLabelWidgetID,Id_PaginaSel,kLabelId_SeccionLabelWidgetID,Seccion,indexSelected);
			retval=kTrue;
		}
	}while(false);
	
	return(retval);
}


bool16 CheckOutDialogController::LLenar_Combo_EstatusNota(PMString Id_EstatusActual)
{
	
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kComboBoxStatusWidgetID);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
		
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
			(
				kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
				IUpdateStorysAndDBUtils::kDefaultIID
			)));
			
		if(UpdateStorys==nil)
		{
			break;
		}
		
		PreferencesConnection PrefConections;
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		

		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
			
		K2Vector<PMString> QueryVector;
		
		
		
		//Consulta
		PMString Busqueda="SELECT   Id_Estatus,Nombre_Estatus From Estatus_Elemento WHERE Id_tipo_ele='2' ORDER BY Nombre_Estatus ASC";
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			
			PMString cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_Estatus",QueryVector[i]);
			PMString IdEstatusQuery=SQLInterface->ReturnItemContentbyNameColumn("Id_Estatus",QueryVector[i]);
			
			
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
			
		}


		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		
		
		int32 Index=dropListData->GetIndex(Id_EstatusActual);
		
		if(Index>=0)
		{
			IDDLDrComboBoxSelecPrefer->Select(Index);
		}
			
	}while(false);
	return(kTrue);
}


int32 CheckOutDialogController::GetIdexSelectedOfComboBoxWidgetID(WidgetID widget,PMString& StringOfSelectedItem)
{
	int32 retvalIndex=-1;
	do
	{
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(widget);
		if(ComboCView==nil)
		{
			
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}

		retvalIndex = dropListData->GetIndex(StringOfSelectedItem) ;
	
		/*InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			
			break;
		}
			
		retvalIndex = IDDLDrComboBoxSelecPrefer->GetSelected() 
		*/
	}while(false);
	return(retvalIndex);
}