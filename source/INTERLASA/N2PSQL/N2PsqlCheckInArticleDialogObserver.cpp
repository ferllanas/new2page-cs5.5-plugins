/*
//	File:	N2PsqlCheckInArticleDialogObserver.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ITextControlData.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"

// Project includes:
#include "N2PsqlID.h"

#include "N2PsqlID.h"
#include "UpdateStorysAndDBUtilis.h"
#include "N2PSQLUtilities.h"

#ifdef WINDOWS
	#include "..\N2PCheckInOut\N2PCheckInOutID.h"

	#include "..\N2PLogInOut\IN2PSQLUtils.h"
	#include "..\N2PLogInOut\N2PRegisterUsers.h"
	#include "..\N2PLogInOut\IRegisterUsersUtils.h"
#endif

#ifdef MACINTOSH
	#include "N2PCheckInOutID.h"

	#include "IN2PSQLUtils.h"
	#include "N2PRegisterUsers.h"
	#include "IRegisterUsersUtils.h"
#endif

/** N2PsqlCheckInArticleDialogObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author Juan Fernando Llanas Rdz
*/
class N2PsqlCheckInArticleDialogObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		N2PsqlCheckInArticleDialogObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~N2PsqlCheckInArticleDialogObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		
	private: void LLenar_Combo_Seccion();
	
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(N2PsqlCheckInArticleDialogObserver, kN2PsqlCheckInArticleDialogObserverImpl)

/* AutoAttach
*/
void N2PsqlCheckInArticleDialogObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to N2PCheckInOut's info button widget.
		//AttachToWidget(kCheckInOutIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kN2PsqlComboPubliWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void N2PsqlCheckInArticleDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from N2PCheckInOut's info button widget.
		//DetachFromWidget(kCheckInOutIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kN2PsqlComboPubliWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void N2PsqlCheckInArticleDialogObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("N2PsqlCheckInArticleDialogObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();

		if (theSelectedWidget == kN2PsqlComboPubliWidgetID && theChange == kPopupChangeStateMessage)
		{
			// Bring up the About box.
			this->LLenar_Combo_Seccion();
		}

	} while (false);
}
//  Generated by Dolly build 17: template "Dialog".
// End, N2PsqlCheckInArticleDialogObserver.cpp.



void N2PsqlCheckInArticleDialogObserver::LLenar_Combo_Seccion()
{
	do
	{
	
		
		
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL(" BusquedaDialogObserver::LLenar_Combo_Seccion  panelControlData");
			break;
		}
		
		
						
		
		
		
		IControlView * ComboCViewPubli=panelControlData->FindWidget(kN2PsqlComboPubliWidgetID);
		if (ComboCViewPubli == nil)
		{
			ASSERT_FAIL(" BusquedaDialogObserver::LLenar_Combo_Seccion  ComboCViewPubli*");
			break;
		}
		
		InterfacePtr<ITextControlData> TextControlPubli(ComboCViewPubli,ITextControlData::kDefaultIID);
		if (TextControlPubli == nil)
		{
			ASSERT_FAIL(" BusquedaDialogObserver::LLenar_Combo_Seccion  TextControlPubli*");
			break;
		}
		
		IControlView * ComboCView=panelControlData->FindWidget(kN2PsqlComboSeccionWidgetID);
		if(ComboCView==nil)
		{
			ASSERT_FAIL(" BusquedaDialogObserver::LLenar_Combo_Seccion   ComboCView");
			break;
		}
		
		InterfacePtr<IStringListControlData> dropListData(ComboCView,IID_ISTRINGLISTCONTROLDATA);
		if (dropListData == nil)
		{
			ASSERT_FAIL("dropListData");
			
			break;
		}
		dropListData->Clear(kTrue);

		PMString Busqueda="SELECT SQL_CACHE  Id_Seccion, Nombre_de_Seccion From Seccion WHERE Id_Publicacion=(SELECT Id_Publicacion FROM Publicacion WHERE Nombre_Publicacion='" + TextControlPubli->GetString() + "' ) ORDER BY Nombre_de_Seccion ASC";
		///borrado de la lista al inicializar el combo
		

		InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( ComboCView, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLDrComboBoxSelecPrefer==nil)
		{
			ASSERT_FAIL(" BusquedaDialogObserver::LLenar_Combo_Seccion   IDDLDrComboBoxSelecPrefer");
			break;
		}


		PMString cadena;
		
		////// Obtiene Preferencias para conexion /////////////
		InterfacePtr<IUpdateStorysAndDBUtils> UpdateStorys(static_cast<IUpdateStorysAndDBUtils*> (CreateObject
		(
			kN2PsqlUpdateStorysAndDBUtilitiesBoss,	// Object boss/class
			IUpdateStorysAndDBUtils::kDefaultIID
		)));
			
		if(UpdateStorys==nil)
		{
			ASSERT_FAIL(" BusquedaDialogObserver::LLenar_Combo_Seccion  UpdateStorys");
			break;
		}
		
		PreferencesConnection PrefConections;
		PMString DSNName="Default";// N2PSQLUtilities::ObtenerTextoDeWidgetEnN2PSQLPaleta(kN2PsqlDataBaseComboWidgetID);
		
		if(!UpdateStorys->GetDefaultPreferencesConnection(PrefConections,kFalse))
		{
			break;
		}
		
		PMString StringConection="";
		
		StringConection.Append("DSN=" + PrefConections.DSNNameConnection + ";UID=" + PrefConections.NameUserDBConnection + ";PWD=" + PrefConections.PwdUserDBConnection);
		
		
		InterfacePtr<IN2PSQLUtils> SQLInterface(static_cast<IN2PSQLUtils*> (CreateObject
			(
				kN2PSQLUtilsBoss,	// Object boss/class
				IN2PSQLUtils::kDefaultIID
			)));
		if(SQLInterface==nil)
		{
			break;
		}
		
		K2Vector<PMString> QueryVector;
		
		SQLInterface->SQLQueryDataBase(StringConection,Busqueda,QueryVector);
		
		for(int32 i=0;i<QueryVector.Length();i++)
		{
			cadena=SQLInterface->ReturnItemContentbyNameColumn("Nombre_de_Seccion",QueryVector[i]);
			dropListData->AddString(cadena, IStringListControlData::kEnd, kFalse, kFalse);
		}

		
		dropListData->AddString("", IStringListControlData::kEnd, kFalse, kFalse);
		

	}while(false);
}
