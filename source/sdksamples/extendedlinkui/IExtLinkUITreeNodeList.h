//========================================================================================
//  
//  $File: //depot/indesign_7.x/highprofile/source/sdksamples/extendedlinkui/IExtLinkUITreeNodeList.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: alokumar $
//  
//  $DateTime: 2011/04/01 11:22:15 $
//  
//  $Revision: #1 $
//  
//  $Change: 781912 $
//  
//  Copyright 1997-2010 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IExtLinkUITreeNodeList_H_DEFINED__
#define __IExtLinkUITreeNodeList_H_DEFINED__

#include "IPMUnknown.h"
#include "K2Vector.h"
#include "WideString.h"
#include "NodeID.h"
#include "URI.h"
#include "ExtLinkUIID.h"

/**  @ingroup extendedlinkui
*/
class IExtLinkUITreeNodeList : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IEXTLINKUITREENODELIST };


	/**	Given an URI, return the cache for it
	 */
	virtual void AddTreeNodeForLink(UID link, NodeID node)=0;

	/**	Check if a key value pair keyed by uri is already exisited in the stored map
	 */
	virtual NodeID GetTreeNodeForLink(const UID& link)=0;
};


#endif // __IExtLinkUITreeNodeList_H_DEFINED__

