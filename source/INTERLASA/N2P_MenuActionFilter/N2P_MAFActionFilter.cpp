//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/customactionfilter/N2P_MAFActionFilter.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: rgano $
//  
//  $DateTime: 2005/01/09 20:46:10 $
//  
//  $Revision: #2 $
//  
//  $Change: 309245 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IActionFilter.h"
#include "IEnvironment.h"
#include "IApplication.h"

// General includes:
#include "ActionDefs.h" // for kCustomEnabling
#include "ActionFilterHelper.h"
#include "CPMUnknown.h"
#include "DocumentID.h" // for kDocumentComponentBoss
#include "ShuksanID.h" // for actionIDs and bossclasses
#include "PrintID.h" // for actionIDs and bossclasses
#include "ImportExportUIID.h" // for actionIDs and bossclasses
#include "TextEditorActionID.h" // for actionIDs and bossclasses
#include "TextEditorID.h" // for actionIDs and bossclasses
#include "InCopyFileActionsID.h"
#include "CAlert.h" // for actionIDs and bossclasses
#include "TextAttrID.h"
#include "ActionMapper.h"

#include "PDFID.h"

#include "Utils.h"
#include "ILayoutUIUtils.h"

#include "PMString.h"

#include "IDocument.h"
#include "ActionFilterHelper.h"

// Project includes:
#include "N2P_MAFID.h"
#include "N2P_MAFActionFilter.h"

#ifdef MACINTOSH
	#include "N2PCheckInOutID.h"
#endif

#ifdef WINDOWS
	#include "..\N2PCheckInOut\N2PCheckInOutID.h"
#endif

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(N2P_MAFActionFilter, kN2P_MAFActionFilterImpl)


/* Constructor 
*/
N2P_MAFActionFilter::N2P_MAFActionFilter(IPMUnknown* boss)
	: CPMUnknown<IActionFilter>(boss) 
{
	// does nothing. 
}


/* FilterAction
*/
void N2P_MAFActionFilter::FilterAction(ClassID* componentClass, ActionID* actionID, 
									   PMString* actionName, PMString* actionArea, 
									   int16* actionType, uint32* enablingType, 
									   bool16* userEditable)
{
	
	/*if(actionID->Get()==kOpenStoryEditorActionID)
	 {
	 PMString Update="Name:";
	 Update.Append(actionName->GrabCString());
	 Update.Append("\n Area:");
	 Update.Append(actionArea->GrabCString());
	 Update.Append("\n ActionID:");
	 Update.AppendNumber(actionID->Get());
	 CAlert::InformationAlert(Update);
	 }*/
	
	
	
	
	
	// check if the filters are configured
	if (gActionFilterHelpers.size() == 0)
	{
		// configure only when needed...
		this->ConfigureFilters();
	}
	
	// do the filtering!
	
	// iterate through all entries
	K2Vector<ActionFilterHelper>::const_iterator iter = gActionFilterHelpers.begin();
	K2Vector<ActionFilterHelper>::const_iterator last = gActionFilterHelpers.end();
	while (iter != last)
	{
		ActionFilterHelper helper = *iter;
		
		// check if there is match
		if (helper.IsMatch(*componentClass, *actionID, 
						   *actionName, *actionArea,
						   *actionType, *enablingType, 
						   *userEditable))
		{
			TRACEFLOW(kN2P_MAFPluginName, "[N2P_MAFActionFilter::FilterAction]\n");
			/*TRACEFLOW(kN2P_MAFPluginName, ">>> FilterAction (before): \"%s\", ClassID 0x%X, ActionID 0x%X, ActionArea:\"%s\" ActionType: %d, EnablingType %d, UserEditable %d\n", 
					  (actionName ? actionName->GrabCString() : "<unknown>"), 
					  *componentClass, 
					  *actionID, 
					  (actionArea ? actionArea->GrabCString() : "<unknown>"),
					  *actionType, 
					  *enablingType,*userEditable);*/
						//
			
			// match made - do the filtering
			helper.DoFilter(componentClass, actionID, 
							actionName,actionArea, actionType, 
							enablingType, userEditable);
			
			/*TRACEFLOW(kN2P_MAFPluginName, ">>> FilterAction (after): \"%s\", ClassID 0x%X, ActionID 0x%X, ActionArea:\"%s\" ActionType: %d, EnablingType %d, UserEditable %d\n", 
					  (actionName ? actionName->GrabCString() : "<unknown>"), 
					  *componentClass, 
					  *actionID, 
					  (actionArea ? actionArea->GrabCString() : "<unknown>"),
					  *actionType, 
					  *enablingType, 
					  *userEditable);*/
			
			break;	// allow only the first match
		}
		iter++;
	}
	/*IDocument* myDoc = Utils<ILayoutUIUtils>()->GetFrontDocument();
	// configure only when there is a document open needed...
	if (myDoc)
	{
		PMString test;
		myDoc->GetName(test);
		if (*actionID>=kFirstPDFStyleActionID && *actionID<= kLastPDFStyleActionID)
		{
			// heavyweight logic here, do you want the action filtered at all?
			if (test == "ross.indd") 
			{
				ActionMapper * actionMapperPtr = ActionMapper::ActionMapperFactory();
				// add action will only add the action if it is missing.
				actionMapperPtr->AddAction(*actionID,*componentClass);
				*componentClass = kN2P_MAFActionComponentBoss;
			}
			
		}
	}*/
}


/* ConfigureFilters
*/
void N2P_MAFActionFilter::ConfigureFilters(void)
{

	
	//CAlert::InformationAlert("ConfigureFilters");
	// 3rd PARTY TODO: change this to suit your needs

	/*
	here we are "intercepting" the following actions:
		File > New
		File > Open
		File > Close
		File > Exit/Quit
	By "intercepting", we mean that we are routing the registered action component to be ours, so that
	we can do stuff before the original action take place.
	In some cases, we may have to change some properties of the registered action, 
	which is what the Set***() methods in the ActionFilterHelper class are designed to do.
	*/

	gActionFilterHelpers.clear();




	
	InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
	
	InterfacePtr<IActionManager> actionmanager(app->QueryActionManager());
	
	
	ActionID myUpdateDesignActionID(94721);
	IActionComponent* mycomponenw=actionmanager->QueryActionComponent(myUpdateDesignActionID);
	//Obtine el nombre de la aplicacion sobre la que estamos corriendo (InDesign o InCopy)
	PMString Aplicacion=app->GetApplicationName();
	Aplicacion.SetTranslatable(kFalse);
	
	
	
	
	
	if(Aplicacion.Contains("InCopy"))
	{
		// (1) filter the "File >> New" menu
		ActionFilterHelper filterNewAction;
		// nothing to change, but just making this field a filter key
		filterNewAction.SetFilterActionID(kTrue, kNewActionID, kNewActionID, kTrue);
		// here's what we want to change
		//filterNewAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFNewActionNameStringKey)); 
		filterNewAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterNewAction);
		
		//Para inhabilitar los shortcuts
		// (1) filter the "File >> New" menu
		ActionFilterHelper filterAlignCenterAction;
		// nothing to change, but just making this field a filter key
		filterAlignCenterAction.SetFilterActionID(kTrue, kAlignCenterActionID, kAlignCenterActionID, kTrue);
		// here's what we want to change
		//filterNewAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFNewActionNameStringKey)); 
		filterAlignCenterAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterAlignCenterAction);
		
		
		//Para inhabilitar los shortcuts
		// (1) filter the "File >> New" menu
		ActionFilterHelper filterIncreasePointAction;
		// nothing to change, but just making this field a filter key
		filterIncreasePointAction.SetFilterActionID(kTrue, kIncreasePointSizeActionID, kIncreasePointSizeActionID, kTrue);
		// here's what we want to change
		//filterNewAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFNewActionNameStringKey)); 
		filterIncreasePointAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterIncreasePointAction);
	
		// (1) filter the "File >> New" menu
		ActionFilterHelper filterDecreasePointSizeAction;
		// nothing to change, but just making this field a filter key
		filterDecreasePointSizeAction.SetFilterActionID(kTrue, kDecreasePointSizeActionID, kDecreasePointSizeActionID, kTrue);
		// here's what we want to change
		//filterNewAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFNewActionNameStringKey)); 
		filterDecreasePointSizeAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterDecreasePointSizeAction);
		
		ActionFilterHelper filterIncreasePointSizeTimes5Action;
		// nothing to change, but just making this field a filter key
		filterIncreasePointSizeTimes5Action.SetFilterActionID(kTrue, kIncreasePointSizeTimes5ActionID, kIncreasePointSizeTimes5ActionID, kTrue);
		// here's what we want to change
		//filterNewAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFNewActionNameStringKey)); 
		filterIncreasePointSizeTimes5Action.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterIncreasePointSizeTimes5Action);
		
		ActionFilterHelper filterDecreasePointSizeTimes5Action;
		// nothing to change, but just making this field a filter key
		filterDecreasePointSizeTimes5Action.SetFilterActionID(kTrue, kDecreasePointSizeTimes5ActionID, kDecreasePointSizeTimes5ActionID, kTrue);
		// here's what we want to change
		//filterNewAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFNewActionNameStringKey)); 
		filterDecreasePointSizeTimes5Action.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterDecreasePointSizeTimes5Action);
		
		
		ActionFilterHelper filterUpdateDesignAction;
		// nothing to change, but just making this field a filter key
		filterUpdateDesignAction.SetFilterActionID(kTrue, myUpdateDesignActionID, myUpdateDesignActionID, kTrue);
		// here's what we want to change
		filterUpdateDesignAction.SetFilterActionName(kTrue, PMString("&Update Design"), PMString(kN2P_MAFUpdateDesigNameStringKey)); 
		filterUpdateDesignAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterUpdateDesignAction);
		
		/*//Este Filtro sera para evitar el error de Depositar en InCopy el Documento
		
	
		ActionFilterHelper filterN2PCheckInPageAction;
		// nothing to change, but just making this field a filter key
		filterN2PCheckInPageAction.SetFilterActionID(kTrue, kCheckInOutDialogActionID, kCheckInOutDialogActionID, kTrue);
		// here's what we want to change
		filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterN2PCheckInPageAction.SetFilterComponentClass(kTrue, kCheckInOutActionComponentBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterN2PCheckInPageAction);*/
		
	}
	else
	{
		
		
		/*ActionFilterHelper filterMoveToRightOneCharAction;
		// nothing to change, but just making this field a filter key
		filterMoveToRightOneCharAction.SetFilterActionID(kTrue, kMoveToRightOneCharActionID, kMoveToRightOneCharActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterMoveToRightOneCharAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterMoveToRightOneCharAction);
		
		
		ActionFilterHelper filterMoveToLeftOneCharAction;
		// nothing to change, but just making this field a filter key
		filterMoveToLeftOneCharAction.SetFilterActionID(kTrue, kMoveToLeftOneCharActionID, kMoveToLeftOneCharActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterMoveToLeftOneCharAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterMoveToLeftOneCharAction);
		
		ActionFilterHelper filterMoveUpOneLineAction;
		// nothing to change, but just making this field a filter key
		filterMoveUpOneLineAction.SetFilterActionID(kTrue, kMoveUpOneLineActionID, kMoveUpOneLineActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterMoveUpOneLineAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterMoveUpOneLineAction);
		
		
		ActionFilterHelper filterMoveDownOneLineAction;
		// nothing to change, but just making this field a filter key
		filterMoveDownOneLineAction.SetFilterActionID(kTrue, kMoveDownOneLineActionID, kMoveDownOneLineActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterMoveDownOneLineAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterMoveDownOneLineAction);
		
		
		
		ActionFilterHelper filterMoveToRightOneWordAction;
		// nothing to change, but just making this field a filter key
		filterMoveToRightOneWordAction.SetFilterActionID(kTrue, kMoveToRightOneWordActionID, kMoveToRightOneWordActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterMoveToRightOneWordAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterMoveToRightOneWordAction);
		
		
		ActionFilterHelper filterMoveToLeftOneWordAction;
		// nothing to change, but just making this field a filter key
		filterMoveToLeftOneWordAction.SetFilterActionID(kTrue, kMoveToLeftOneWordActionID, kMoveToLeftOneWordActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterMoveToLeftOneWordAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterMoveToLeftOneWordAction);
		
		
		
		ActionFilterHelper filterMoveToStartOfLineAction;
		// nothing to change, but just making this field a filter key
		filterMoveToStartOfLineAction.SetFilterActionID(kTrue, kMoveToStartOfLineActionID, kMoveToStartOfLineActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterMoveToStartOfLineAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterMoveToStartOfLineAction);
		
		
		ActionFilterHelper filterDeleteOneCharToRightAction;
		// nothing to change, but just making this field a filter key
		filterDeleteOneCharToRightAction.SetFilterActionID(kTrue, kDeleteOneCharToRightActionID, kDeleteOneCharToRightActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterDeleteOneCharToRightAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterDeleteOneCharToRightAction);
		
		
		ActionFilterHelper filterDeleteOneCharToLeftAction;
		// nothing to change, but just making this field a filter key
		filterDeleteOneCharToLeftAction.SetFilterActionID(kTrue, kDeleteOneCharToLeftActionID, kDeleteOneCharToLeftActionID, kTrue);
		// here's what we want to change
		//filterN2PCheckInPageAction.SetFilterActionName(kTrue, PMString(kCheckInOutDialogMenuItemKey), PMString(kCheckInOutDialogMenuItemKey)); 
		filterDeleteOneCharToLeftAction.SetFilterComponentClass(kTrue, kTextEditorMenuBoss, kN2P_MAFActionComponentBoss);
		// add it to the vector
		gActionFilterHelpers.push_back(filterDeleteOneCharToLeftAction);*/
		
	}

	
	
/*	// (2) filter the "File >> Open" menu
	ActionFilterHelper filterOpenAction;
	// nothing to change, but just making this field a filter key
	filterOpenAction.SetFilterActionID(kTrue, kOpenActionID, kOpenActionID, kTrue);
	// here's what we want to change
	filterOpenAction.SetFilterActionName(kTrue, PMString("&Open..."), PMString(kInterPLGDemOpenActionNameStringKey)); 
	filterOpenAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kInterPLGDemActionComponentBoss);
	// add it to the vector
	gActionFilterHelpers.push_back(filterOpenAction);

*/
	// (3) filter the "File >> Close" menu
	ActionFilterHelper filterCloseAction;
	// nothing to change, but just making this field a filter key
	filterCloseAction.SetFilterActionID(kTrue, kCloseActionID, kCloseActionID,kTrue);
	// here's what we want to change
	filterCloseAction.SetFilterActionName(kTrue, PMString("&Close"), PMString( kCheckInOutDialogMenuItemKey)); 
	filterCloseAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kN2P_MAFActionComponentBoss);
	// add it to the vector
	gActionFilterHelpers.push_back(filterCloseAction);

	/*
#if defined(MACINTOSH)
	// (4) filter the "File >> Exit/Quit" menu
	ActionFilterHelper filterQuitAction;
	// nothing to change, but just making this field a filter key
	filterQuitAction.SetFilterActionID(kTrue, kQuitActionID, kQuitActionID, kTrue);
	// here's what we want to change

	// the Exit/Quit menus are named differently between Mac and Win platforms.
	

	filterQuitAction.SetFilterActionName(kTrue, PMString("&Quit"), PMString(kN2P_MAFQuitActionNameStringKey)); 
	// check if we are running on OS X...
	//InterfacePtr<const IEnvironment> sysEnvironment(gSession, UseDefaultIID());
	//if (sysEnvironment && sysEnvironment->IsAquaInterface())
	//{
		// if OS X, add this into the filter too
	//	filterQuitAction.SetFilterUserEditable(kTrue, kTrue, kFalse);
//	}
#elif defined(WINDOWS)
//	filterQuitAction.SetFilterActionName(kTrue, PMString("E&xit"), PMString(kInterPLGDemQuitActionNameStringKey)); 
#endif
	filterQuitAction.SetFilterComponentClass(kTrue, kApplKBSCBoss, kN2P_MAFActionComponentBoss);
	
	filterQuitAction.SetFilterEnablingType(kTrue, kAlwaysDisabled, kCustomEnabling);
	// add it to the vector
	gActionFilterHelpers.push_back(filterQuitAction);
	*/
	
	// (5) filter the "File >> Save" menu
	ActionFilterHelper filterSaveAsAction;
	// nothing to change, but just making this field a filter key
	filterSaveAsAction.SetFilterActionID(kTrue, kSaveAsActionID, kSaveAsActionID, kTrue);
	// here's what we want to change
	filterSaveAsAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFSaveAsActionNameStringKey)); 
	filterSaveAsAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kN2P_MAFActionComponentBoss);
	// add it to the vector
	gActionFilterHelpers.push_back(filterSaveAsAction);
	
	// (6) filter the "File >> Save As" menu
	ActionFilterHelper filterSaveAction;
	// nothing to change, but just making this field a filter key
	filterSaveAction.SetFilterActionID(kTrue, kSaveActionID, kSaveActionID, kTrue);
	// here's what we want to change
	filterSaveAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFSaveActionNameStringKey)); 
	filterSaveAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kN2P_MAFActionComponentBoss);
	// add it to the vector
	gActionFilterHelpers.push_back(filterSaveAction);

	// (7) filter the "File >> Save A Copy" menu
	ActionFilterHelper filterSaveAsCopyAction;
	// nothing to change, but just making this field a filter key
	filterSaveAsCopyAction.SetFilterActionID(kTrue, kSaveACopyActionID, kSaveACopyActionID, kTrue);
	// here's what we want to change
	filterSaveAsCopyAction.SetFilterActionName(kTrue, PMString("Document..."), PMString(kN2P_MAFSaveAsCopyActionNameStringKey)); 
	filterSaveAsCopyAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kN2P_MAFActionComponentBoss);
	// add it to the vector
	gActionFilterHelpers.push_back(filterSaveAsCopyAction);
	
	
	/*// (8) filter the "File >> Print" menu
	ActionFilterHelper filterPrintCopyAction;
	// nothing to change, but just making this field a filter key
	filterPrintCopyAction.SetFilterActionID(kTrue, kPrintActionID, kPrintActionID, kTrue);
	// here's what we want to change
	filterPrintCopyAction.SetFilterActionName(kTrue, PMString("Print..."), PMString(kN2P_MAFPrintActionNameStringKey)); 
	filterPrintCopyAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kN2P_MAFActionComponentBoss);
	// add it to the vector
	gActionFilterHelpers.push_back(filterPrintCopyAction);
	

	// (8) filter the "File >> Export" menu
	ActionFilterHelper filterExportCopyAction;
	// nothing to change, but just making this field a filter key
	filterExportCopyAction.SetFilterActionID(kTrue, kExportActionID, kExportActionID, kTrue);
	// here's what we want to change
	filterExportCopyAction.SetFilterActionName(kTrue, PMString("Print..."), PMString(kN2P_MAFExportActionNameStringKey)); 
	filterExportCopyAction.SetFilterComponentClass(kTrue, kDocumentComponentBoss, kN2P_MAFActionComponentBoss);
	// add it to the vector
	gActionFilterHelpers.push_back(filterExportCopyAction);
	*/
	
	
}

// End, N2P_MAFActionFilter.cpp.
